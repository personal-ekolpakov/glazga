package kz.pompei.glazga.moc.class_meta.model.accessors;

import kz.pompei.glazga.moc.class_meta.Ignore;

public class TestSimpleClass {
  public String field1;
  public String field2;
  public String field3;
  public String field4;

  @SuppressWarnings("unused")
  @Ignore
  public String fieldLeft;

  @SuppressWarnings("unused")
  public String getField2() {
    return field2 + " A";
  }

  @SuppressWarnings("unused")
  public void setField2(String field2) {
    this.field2 = field2 + " X";
  }

  @SuppressWarnings("unused")
  public String getField3() {
    return field3 + " B";
  }

  @SuppressWarnings("unused")
  public void setField4(String field4) {
    this.field4 = field4 + " Y";
  }

  @SuppressWarnings("unused")
  @Ignore
  public String getFieldWow() {
    return "";
  }

  @SuppressWarnings("unused")
  @Ignore
  public void setFieldStatus(String tmp) {}

  @SuppressWarnings("unused")
  public void setFieldNoGetter(String fieldNoGetter) {}

  @SuppressWarnings("unused")
  public String getFieldNoSetter() {
    return "";
  }
}
