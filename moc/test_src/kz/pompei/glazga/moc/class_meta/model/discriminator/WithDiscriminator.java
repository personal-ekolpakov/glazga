package kz.pompei.glazga.moc.class_meta.model.discriminator;

import kz.pompei.glazga.ann.DiscriminatorClassSimpleName;

@DiscriminatorClassSimpleName("type63472864")
public abstract class WithDiscriminator {
  public String baseField;
}
