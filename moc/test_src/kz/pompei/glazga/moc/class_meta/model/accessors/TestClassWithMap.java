package kz.pompei.glazga.moc.class_meta.model.accessors;

import java.util.Map;

public class TestClassWithMap {
  @SuppressWarnings("unused")
  public Map<TestEnumForMap, TestClassValue> mapField;
}
