package kz.pompei.glazga.moc.class_meta.model.accessors;

public class TestClassWithErrors {

  @SuppressWarnings("unused")
  public String getFieldGetterError() {
    throw new TestErr_Az7i5yH2AL("hhmAGpMON4");
  }

  @SuppressWarnings("unused")
  public void setFieldSetterError(String fieldSetterError) {
    throw new TestErr_Az7i5yH2AL("tt2gv2qch4");
  }
}
