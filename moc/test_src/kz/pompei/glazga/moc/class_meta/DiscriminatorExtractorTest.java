package kz.pompei.glazga.moc.class_meta;

import kz.pompei.glazga.ann.DiscriminatorClassSimpleName;
import kz.pompei.glazga.moc.class_meta.model.children.TestBaseWithSame;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DiscriminatorExtractorTest {

  @DiscriminatorClassSimpleName("type_25zNlT6g2h")
  public static class BaseClass {}

  public static class ChildClass1 extends BaseClass {}

  public static class ChildClass2 extends BaseClass {}

  @Test
  public void extract__SimpleNameDiscriminator__extends() {

    //
    //
    DiscriminatorDef def = DiscriminatorExtractor.extract(BaseClass.class).orElseThrow();
    //
    //

    assertThat(def.fieldName()).isEqualTo("type_25zNlT6g2h");
    assertThat(def.getChildClass(ChildClass1.class.getSimpleName())).contains(ChildClass1.class);
    assertThat(def.getChildClass(ChildClass2.class.getSimpleName())).contains(ChildClass2.class);

    assertThat(def.getFieldValue(ChildClass1.class)).isEqualTo(ChildClass1.class.getSimpleName());
    assertThat(def.getFieldValue(ChildClass2.class)).isEqualTo(ChildClass2.class.getSimpleName());

  }


  @DiscriminatorClassSimpleName("type_rx28GfQuLh")
  public interface BaseInterface {}

  public static class ImplClass implements BaseInterface {}

  @Test
  public void extract__SimpleNameDiscriminator__implements() {

    //
    //
    DiscriminatorDef def = DiscriminatorExtractor.extract(BaseInterface.class).orElseThrow();
    //
    //

    assertThat(def.fieldName()).isEqualTo("type_rx28GfQuLh");
    assertThat(def.getChildClass(ImplClass.class.getSimpleName())).contains(ImplClass.class);
    assertThat(def.getFieldValue(ImplClass.class)).isEqualTo(ImplClass.class.getSimpleName());
  }

  public static class LeftClass4 {}

  @Test
  public void extract__leftClass() {

    //
    //
    DiscriminatorDef def = DiscriminatorExtractor.extract(LeftClass4.class).orElse(null);
    //
    //

    assertThat(def).isNull();
  }

  @DiscriminatorClassSimpleName("type_c4wAE9UXhv")
  public static class SomeClassParent2 {}

  public static class SomeClassParent1 extends SomeClassParent2 {}

  public static class SomeClass extends SomeClassParent1 {}

  @Test
  public void extractParent__class__currentNoDiscriminator() {

    //
    //
    DiscriminatorDef discriminator = DiscriminatorExtractor.extractParent(SomeClass.class).orElseThrow();
    //
    //

    assertThat(discriminator.fieldName()).isEqualTo("type_c4wAE9UXhv");

    assertThat(discriminator.getFieldValue(SomeClass.class)).isEqualTo(SomeClass.class.getSimpleName());
    assertThat(discriminator.getChildClass(SomeClassParent1.class.getSimpleName())).contains(SomeClassParent1.class);


  }

  @DiscriminatorClassSimpleName("type_xHDgm0TIqm")
  public static class Some2ClassParent2 {}

  public static class Some2ClassParent1 extends Some2ClassParent2 {}

  @DiscriminatorClassSimpleName("left")
  public static class Some2Class extends Some2ClassParent1 {}

  @Test
  public void extractParent__class__currentWithDiscriminator() {

    //
    //
    DiscriminatorDef discriminator = DiscriminatorExtractor.extractParent(Some2Class.class).orElseThrow();
    //
    //

    assertThat(discriminator.fieldName()).isEqualTo("type_xHDgm0TIqm");

    assertThat(discriminator.getFieldValue(Some2Class.class)).isEqualTo(Some2Class.class.getSimpleName());
    assertThat(discriminator.getChildClass(Some2ClassParent1.class.getSimpleName())).contains(Some2ClassParent1.class);


  }


  @DiscriminatorClassSimpleName("type_pkH1uY9WmY")
  public interface Some3ClassParent2 {}

  public static class Some3ClassParent1 implements Some3ClassParent2 {}

  @DiscriminatorClassSimpleName("left")
  public static class Some3Class extends Some3ClassParent1 {}

  @Test
  public void extractParent__interface__currentWithDiscriminator() {

    //
    //
    DiscriminatorDef discriminator = DiscriminatorExtractor.extractParent(Some3Class.class).orElseThrow();
    //
    //

    assertThat(discriminator.fieldName()).isEqualTo("type_pkH1uY9WmY");

    assertThat(discriminator.getFieldValue(Some3Class.class)).isEqualTo(Some3Class.class.getSimpleName());
    assertThat(discriminator.getChildClass(Some3ClassParent1.class.getSimpleName())).contains(Some3ClassParent1.class);
  }

  @Test(expectedExceptions = SameSimpleName.class)
  public void extract__SameSimpleName() {
    //
    //
    DiscriminatorExtractor.extract(TestBaseWithSame.class);
    //
    //
  }
}
