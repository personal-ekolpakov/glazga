package kz.pompei.glazga.moc.class_meta;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.moc.class_meta.model.accessors.TestClassValue;
import kz.pompei.glazga.moc.class_meta.model.accessors.TestClassWithErrors;
import kz.pompei.glazga.moc.class_meta.model.accessors.TestClassWithMap;
import kz.pompei.glazga.moc.class_meta.model.accessors.TestEnumForMap;
import kz.pompei.glazga.moc.class_meta.model.accessors.TestErr_Az7i5yH2AL;
import kz.pompei.glazga.moc.class_meta.model.accessors.TestSimpleClass;
import kz.pompei.glazga.moc.class_meta.model.children.TestBase;
import kz.pompei.glazga.moc.class_meta.model.children.TestBaseWithSame;
import kz.pompei.glazga.moc.class_meta.model.children.ch1.TestChild1;
import kz.pompei.glazga.moc.class_meta.model.children.ch2.TestChild2;
import kz.pompei.glazga.moc.class_meta.model.children_deep.TestBaseDeep;
import kz.pompei.glazga.moc.class_meta.model.children_deep.TestChildDeep1;
import kz.pompei.glazga.moc.class_meta.model.children_deep.TestChildDeep2;
import kz.pompei.glazga.moc.class_meta.model.children_deep.TestChildDeep3;
import kz.pompei.glazga.moc.class_meta.model.children_deep.TestChildDeep4;
import kz.pompei.glazga.moc.class_meta.model.discriminator.DiscriminatorChild;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ClassMetaParserTest {

  @Test
  public void parse__SimpleClass() {

    //
    //
    ClassMeta classMeta = ClassMetaParser.parse(TestSimpleClass.class);
    //
    //

    assertThat(classMeta.fieldNames()).isEqualTo(Set.of("field1", "field2", "field3", "field4", "fieldNoGetter", "fieldNoSetter"));

    try {
      classMeta.accessor("9vHjp1iWPk");
      Assertions.fail("2zi1O326ie :: Must be generate exception " + NoFieldAccessor.class.getSimpleName());
    } catch (NoFieldAccessor e) {
      assertThat(e.fieldName).isEqualTo("9vHjp1iWPk");
    }

    FieldAccessor field1 = classMeta.accessor("field1");
    FieldAccessor field2 = classMeta.accessor("field2");
    FieldAccessor field3 = classMeta.accessor("field3");
    FieldAccessor field4 = classMeta.accessor("field4");

    TestSimpleClass test = new TestSimpleClass();
    test.field1 = "o14Gt50mhp";
    test.field2 = "6y1lp4Vb3v";
    test.field3 = "Ka1cVEytIb";
    test.field4 = "bF762l1Wxx";

    // Getters

    assertThat(field1.get(test)).isEqualTo("o14Gt50mhp");
    assertThat(field2.get(test)).isEqualTo("6y1lp4Vb3v A");
    assertThat(field3.get(test)).isEqualTo("Ka1cVEytIb B");
    assertThat(field4.get(test)).isEqualTo("bF762l1Wxx");

    // Setters

    field1.set(test, "OK");
    field2.set(test, "OK");
    field3.set(test, "OK");
    field4.set(test, "OK");

    assertThat(test.field1).isEqualTo("OK");
    assertThat(test.field2).isEqualTo("OK X");
    assertThat(test.field3).isEqualTo("OK");
    assertThat(test.field4).isEqualTo("OK Y");

    try {
      classMeta.accessor("6248gz1kkY");
      Assertions.fail("e5kDB8vuz9 :: MUST throw exception if field is absent");
    } catch (NoFieldAccessor e) {
      assertThat(e.aClass).isSameAs(TestSimpleClass.class);
      assertThat(e.fieldName).isEqualTo("6248gz1kkY");
      assertThat(e.placeId).isNotNull();
    }

    FieldAccessor fieldNoGetter = classMeta.accessor("fieldNoGetter");
    FieldAccessor fieldNoSetter = classMeta.accessor("fieldNoSetter");

    try {
      fieldNoGetter.get(null);
    } catch (NoFieldGetter e) {
      assertThat(e.aClass()).isSameAs(TestSimpleClass.class);
      assertThat(e.fieldName()).isEqualTo("fieldNoGetter");
      assertThat(e.placeId()).isNotNull();
    }
    try {
      fieldNoSetter.set(null, null);
    } catch (NoFieldSetter e) {
      assertThat(e.aClass()).isSameAs(TestSimpleClass.class);
      assertThat(e.fieldName()).isEqualTo("fieldNoSetter");
      assertThat(e.placeId()).isNotNull();
    }

  }

  @Test
  public void parse__getterError() {

    //
    //
    ClassMeta classMeta = ClassMetaParser.parse(TestClassWithErrors.class);
    //
    //

    assertThat(classMeta.fieldNames()).isEqualTo(Set.of("fieldGetterError", "fieldSetterError"));

    TestClassWithErrors test = new TestClassWithErrors();

    try {
      classMeta.accessor("fieldGetterError").get(test);
      Assertions.fail("iYt6viRMxQ :: MUST throw " + TestErr_Az7i5yH2AL.class.getSimpleName());
    } catch (TestErr_Az7i5yH2AL e) {
      assertThat(e.getMessage()).isEqualTo("hhmAGpMON4");
    }

  }

  @Test
  public void parse__WithDiscriminator() {
    //
    //
    ClassMeta classMeta = ClassMetaParser.parse(DiscriminatorChild.class);
    //
    //

    assertThat(classMeta.fieldNames()).isEqualTo(Set.of("baseField", "childField", "type63472864"));

    DiscriminatorChild x = new DiscriminatorChild();
    x.baseField  = "KwsXA0VgKr";
    x.childField = "eXP0Ud01Mu";

    FieldAccessor accessorBaseField     = classMeta.accessor("baseField");
    FieldAccessor accessorChildField    = classMeta.accessor("childField");
    FieldAccessor accessorDiscriminator = classMeta.accessor("type63472864");

    assertThat(accessorBaseField.get(x)).isEqualTo("KwsXA0VgKr");
    assertThat(accessorChildField.get(x)).isEqualTo("eXP0Ud01Mu");
    assertThat(accessorDiscriminator.get(x)).isEqualTo(DiscriminatorChild.class.getSimpleName());

    accessorBaseField.set(x, "pD3Ad6Uu23");
    accessorChildField.set(x, "xR9amQ45Oy");
    accessorDiscriminator.set(x, "r2kTNpN3lA");

    assertThat(x.baseField).isEqualTo("pD3Ad6Uu23");
    assertThat(x.childField).isEqualTo("xR9amQ45Oy");
  }

  @Test
  public void parse__setterError() {

    //
    //
    ClassMeta classMeta = ClassMetaParser.parse(TestClassWithErrors.class);
    //
    //

    assertThat(classMeta.fieldNames()).isEqualTo(Set.of("fieldGetterError", "fieldSetterError"));

    TestClassWithErrors test = new TestClassWithErrors();

    try {
      classMeta.accessor("fieldSetterError").set(test, "6JNtTR694h");
      Assertions.fail("Qyq292055V :: MUST throw " + TestErr_Az7i5yH2AL.class.getSimpleName());
    } catch (TestErr_Az7i5yH2AL e) {
      assertThat(e.getMessage()).isEqualTo("tt2gv2qch4");
    }
  }

  @Test
  public void parse__discriminatorChildClasses() {
    //
    //
    ClassMeta classMeta = ClassMetaParser.parse(TestBase.class);
    //
    //

    DiscriminatorDef discriminator = classMeta.discriminator().orElseThrow();

    assertThat(discriminator.getChildClass(TestChild1.class.getSimpleName())).contains(TestChild1.class);
    assertThat(discriminator.getChildClass(TestChild2.class.getSimpleName())).contains(TestChild2.class);

  }

  @Test
  public void parse__children__deep() {
    //
    //
    ClassMeta classMeta = ClassMetaParser.parse(TestBaseDeep.class);
    //
    //

    DiscriminatorDef discriminator = classMeta.discriminator().orElseThrow();

    assertThat(discriminator.getChildClass(TestChildDeep1.class.getSimpleName())).contains(TestChildDeep1.class);
    assertThat(discriminator.getChildClass(TestChildDeep2.class.getSimpleName())).contains(TestChildDeep2.class);
    assertThat(discriminator.getChildClass(TestChildDeep3.class.getSimpleName())).contains(TestChildDeep3.class);
    assertThat(discriminator.getChildClass(TestChildDeep4.class.getSimpleName())).contains(TestChildDeep4.class);
  }

  @Test(expectedExceptions = SameSimpleName.class)
  public void parse__children_ERR() {
    //
    //
    ClassMetaParser.parse(TestBaseWithSame.class);
    //
    //
  }

  @Test
  public void parse__classWithFieldMap() {
    //
    //
    ClassMeta classMeta = ClassMetaParser.parse(TestClassWithMap.class);
    //
    //

    assertThat(classMeta).isNotNull();

    FieldAccessor mapField = classMeta.accessor("mapField");

    assertThat(mapField).isNotNull();

    Type fieldType = mapField.fieldType();
    assertThat(fieldType).isNotNull()
                         .isInstanceOf(ParameterizedType.class);

    ParameterizedType parameterizedType = (ParameterizedType) fieldType;

    assertThat(parameterizedType.getRawType()).isEqualTo(Map.class);

    assertThat(parameterizedType.getActualTypeArguments()).hasSize(2);

    assertThat(parameterizedType.getActualTypeArguments()[0]).isEqualTo(TestEnumForMap.class);
    assertThat(parameterizedType.getActualTypeArguments()[1]).isEqualTo(TestClassValue.class);

  }

}
