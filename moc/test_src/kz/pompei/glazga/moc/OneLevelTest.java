package kz.pompei.glazga.moc;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OneLevelTest {

  @Test
  public void appendToOneLevel() {

    Map<String, Object> deepMap = Map.of(
      "top1", Map.of(
        "inner1", "Hello",
        "inner2", 43215
      ),
      "top2", Map.of(
        "deepInner1", Map.of(
          "name1", "vc81Dqo1i4",
          "name2", true
        ),
        "deepInner2", Map.of(
          "name1", "0564gXjPOF",
          "name2", new BigDecimal("234")
        )
      ),
      "top3", Map.of()
    );

    MocUtils.printMap("H0kKz9kHCv", deepMap);


    OneLevel oneLevel = new OneLevel();

    //
    //
    oneLevel.append(deepMap);
    //
    //


    assertThat(oneLevel.target).containsEntry("top1.inner1", "Hello");
    assertThat(oneLevel.target).containsEntry("top1.inner2", "43215");
    assertThat(oneLevel.target).containsEntry("top2.deepInner1.name1", "vc81Dqo1i4");
    assertThat(oneLevel.target).containsEntry("top2.deepInner1.name2", "true");
    assertThat(oneLevel.target).containsEntry("top2.deepInner2.name1", "0564gXjPOF");
    assertThat(oneLevel.target).containsEntry("top2.deepInner2.name2", "234");
  }

  @Test
  public void set() {

    Map<String, Object> deepMap = new HashMap<>();

    //
    //
    OneLevel.set(deepMap, "top.second1.name1", "3i9Pu9rYLG");
    //
    //

    MocUtils.printMap("157LI1y4fv", deepMap);


    assertThat(deepMap).isEqualTo(Map.of("top", Map.of("second1", Map.of("name1", "3i9Pu9rYLG"))));

    //
    //
    OneLevel.set(deepMap, "top.second2.name2", "Rt1zHmyO1r");
    //
    //

    MocUtils.printMap("rMpBPe1fFS", deepMap);

    assertThat(deepMap).isEqualTo(Map.of(
      "top", Map.of("second1", Map.of("name1", "3i9Pu9rYLG"),
                    "second2", Map.of("name2", "Rt1zHmyO1r")))
    );

  }
}
