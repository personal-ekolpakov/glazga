package kz.pompei.glazga.moc;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.moc.model.discr.TestBaseClient;
import kz.pompei.glazga.moc.model.discr.TopModel;
import kz.pompei.glazga.moc.model.discr.sub.TestClientLegal;
import kz.pompei.glazga.moc.model.discr.sub.TestClientNatural;
import kz.pompei.glazga.moc.model.simple.TestClient;
import kz.pompei.glazga.moc.model.with_map.TestEnumKey;
import kz.pompei.glazga.moc.model.with_map.TestMapValue;
import kz.pompei.glazga.moc.model.with_map.TestWithMap;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MapObjectConverterTest {

  @Test
  public void convertFromMap_simple() {

    var mapData = Map.of(
      "surname", "Полищук",
      "name", "Геннадий",
      "regAddress", Map.of("street", "Пушкина",
                           "home", "34",
                           "flat", "51"),
      "liveAddress", Map.of("street", "Гагарина",
                            "home", "14",
                            "flat", "501")
    );

    MapObjectConverter converter = new MapObjectConverter();

    //
    //
    TestClient actual = converter.convertFromMap(mapData, TestClient.class);
    //
    //

    assertThat(actual).isNotNull();
    assertThat(actual.surname).isEqualTo("Полищук");
    assertThat(actual.name).isEqualTo("Геннадий");
    assertThat(actual.regAddress).isNotNull();
    assertThat(actual.regAddress.street).isEqualTo("Пушкина");
    assertThat(actual.regAddress.home).isEqualTo("34");
    assertThat(actual.regAddress.flat).isEqualTo("51");
    assertThat(actual.liveAddress).isNotNull();
    assertThat(actual.liveAddress.street).isEqualTo("Гагарина");
    assertThat(actual.liveAddress.home).isEqualTo("14");
    assertThat(actual.liveAddress.flat).isEqualTo("501");
  }

  @Test
  public void convertFromMap_mapFieldWithEnum() {

    var mapData = Map.of(
      "fieldMap", Map.of(
        "V1", Map.of("field1", "syP9YBf3kA",
                     "field2", "drg30Ik4l9"
        ),
        "V2", Map.of("field1", "1VygMkPO5S",
                     "field2", "5HG4m0IhcR"
        ),
        "V3", Map.of("field1", "UZ2HXlAjHg",
                     "field2", "Wc9su6UOcq"
        ))
    );

    MapObjectConverter converter = new MapObjectConverter();

    //
    //
    TestWithMap actual = converter.convertFromMap(mapData, TestWithMap.class);
    //
    //

    assertThat(actual).isNotNull();
    assertThat(actual.fieldMap).isNotNull();

    TestMapValue actualV1 = actual.fieldMap.get(TestEnumKey.V1);
    assertThat(actualV1).isNotNull();
    assertThat(actualV1.field1).isEqualTo("syP9YBf3kA");
    assertThat(actualV1.field2).isEqualTo("drg30Ik4l9");

    TestMapValue actualV2 = actual.fieldMap.get(TestEnumKey.V2);
    assertThat(actualV2).isNotNull();
    assertThat(actualV2.field1).isEqualTo("1VygMkPO5S");
    assertThat(actualV2.field2).isEqualTo("5HG4m0IhcR");

    assertThat(actual.fieldMap.get(TestEnumKey.V3)).isNotNull();
  }

  @Test
  public void convertFromMap_discriminator_01() {

    var mapData = Map.<String, Object>of(
      "baseField", "wOL5nT07SN",
      "type", TestClientNatural.class.getSimpleName(),
      "naturalField", "48717y1chX"
    );

    MapObjectConverter converter = new MapObjectConverter();

    //
    //
    TestBaseClient actual = converter.convertFromMap(mapData, TestBaseClient.class);
    //
    //

    assertThat(actual).isNotNull()
                      .isInstanceOf(TestClientNatural.class);

    TestClientNatural x = (TestClientNatural) actual;
    assertThat(x.baseField).isEqualTo("wOL5nT07SN");
    assertThat(x.naturalField).isEqualTo("48717y1chX");
  }

  @Test
  public void convertToMap__convertFromMap__withDiscriminator() {

    TopModel source = new TopModel();
    source.id = "bKr1slUe4h";
    TestClientNatural c1 = new TestClientNatural();
    c1.baseField    = "Q3382qq5mH";
    c1.naturalField = "3767WF6oft";
    source.client   = c1;

    MapObjectConverter converter = new MapObjectConverter();

    //
    //
    Map<String, Object> deepMap = converter.convertToMap(source);
    //
    //

    //
    //
    TopModel actual = converter.convertFromMap(deepMap, TopModel.class);
    //
    //

    assertThat(actual).isNotNull();

    assertThat(actual.id).isEqualTo("bKr1slUe4h");

    TestBaseClient actualClient = actual.client;
    assertThat(actualClient).isInstanceOf(TestClientNatural.class);

    TestClientNatural x = (TestClientNatural) actualClient;
    assertThat(x.baseField).isEqualTo("Q3382qq5mH");
    assertThat(x.naturalField).isEqualTo("3767WF6oft");
  }

  @Test
  public void convertFromMap_discriminator_02() {

    var mapData = Map.<String, Object>of(
      "baseField", "O9AbQSt1x7",
      "type", TestClientLegal.class.getSimpleName(),
      "legalField", "SAom6hCX4P"
    );

    MapObjectConverter converter = new MapObjectConverter();

    //
    //
    TestBaseClient actual = converter.convertFromMap(mapData, TestBaseClient.class);
    //
    //

    assertThat(actual).isNotNull()
                      .isInstanceOf(TestClientLegal.class);

    TestClientLegal x = (TestClientLegal) actual;
    assertThat(x.baseField).isEqualTo("O9AbQSt1x7");
    assertThat(x.legalField).isEqualTo("SAom6hCX4P");
  }

  @Test
  public void convertToMap__simple() {

    TestWithMap test = new TestWithMap();
    test.fieldMap = new HashMap<>();
    {
      TestMapValue v = new TestMapValue();
      v.field1 = "g8bz1nGX41";
      v.field2 = "68a0Zx05yU";
      test.fieldMap.put(TestEnumKey.V1, v);
    }
    {
      TestMapValue v = new TestMapValue();
      v.field1 = "Ew8g1srURd";
      v.field2 = "YzBwE9zE47";
      test.fieldMap.put(TestEnumKey.V2, v);
    }

    MapObjectConverter converter = new MapObjectConverter();

    //
    //
    Map<String, Object> map = converter.convertToMap(test);
    //
    //

    //noinspection unchecked
    Map<String, Object> fieldMap = (Map<String, Object>) map.get("fieldMap");

    assertThat(fieldMap).isNotNull();

    assertThat(fieldMap.get("V1")).isEqualTo(Map.of("field1", "g8bz1nGX41", "field2", "68a0Zx05yU"));
    assertThat(fieldMap.get("V2")).isEqualTo(Map.of("field1", "Ew8g1srURd", "field2", "YzBwE9zE47"));
  }
}
