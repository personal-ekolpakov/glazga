package kz.pompei.glazga.moc.probes.probe001;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Config001 {

  public static Path mapDbFile() {
    return Paths.get("build").resolve("probe-001/map-db");
  }
}
