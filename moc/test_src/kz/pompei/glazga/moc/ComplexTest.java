package kz.pompei.glazga.moc;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.moc.model.complex_text.DifferentThings;
import kz.pompei.glazga.moc.model.complex_text.block.BlockCircle;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ComplexTest {

  @Test
  public void createSaveAndLoad() {
    DifferentThings dth = new DifferentThings();
    dth.putBlockRect("r1")
       .name("Rect R1").comment("SL7Nko6JLh")
       .centerX(1.3).centerY(1.4)
       .height(4).width(10);
    dth.putBlockRect("r2")
       .name("Rect R2")
       .centerX(1.1).centerY(12.4)
       .width(11).height(5);
    dth.putBlockCircle("c1")
       .name("Circle C1").info("EJXl4eOg1a")
       .centerX(3.5).centerY(-1.2)
       .radius(3);
    dth.putBlockCircle("c2")
       .name("Circle C2")
       .radius(3.5)
       .centerX(-3.5).centerY(1.2);

    MapObjectConverter converter = new MapObjectConverter();

    Map<String, Object> map = converter.convertToMap(dth);

    OneLevel oneLevel = new OneLevel();

    Map<String, String> oneLevelMap = oneLevel.append(map);

    MocUtils.printMap("nQId4y3tIm", oneLevelMap);

    Map<String, Object> actualMap = new HashMap<>();

    oneLevelMap.forEach((k, v) -> OneLevel.set(actualMap, k, v));

    DifferentThings actual = converter.convertFromMap(actualMap, DifferentThings.class);

    assertThat(actual).isNotNull();

    BlockCircle c1 = (BlockCircle) actual.blocks.get("c1");

    assertThat(c1.name).isEqualTo("Circle C1");
    assertThat(c1.info).isEqualTo("EJXl4eOg1a");

  }

}
