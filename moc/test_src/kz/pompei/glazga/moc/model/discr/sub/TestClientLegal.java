package kz.pompei.glazga.moc.model.discr.sub;

import java.beans.ConstructorProperties;
import kz.pompei.glazga.moc.model.discr.TestBaseClient;

public class TestClientLegal extends TestBaseClient {

  public String legalField;

  @ConstructorProperties({"legalField", "baseField"})
  public TestClientLegal(String legalField, String baseField) {
    this.legalField = legalField;
    this.baseField  = baseField;
  }
}
