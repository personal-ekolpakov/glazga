package kz.pompei.glazga.moc.model.complex_text;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.moc.model.complex_text.block.Block;
import kz.pompei.glazga.moc.model.complex_text.block.BlockCircle;
import kz.pompei.glazga.moc.model.complex_text.block.BlockRect;

public class DifferentThings {
  public Map<String, Block> blocks = new HashMap<>();

  public BlockRect putBlockRect(String id) {
    var ret = new BlockRect();
    blocks.put(id, ret);
    return ret;
  }

  public BlockCircle putBlockCircle(String id) {
    var ret = new BlockCircle();
    blocks.put(id, ret);
    return ret;
  }
}
