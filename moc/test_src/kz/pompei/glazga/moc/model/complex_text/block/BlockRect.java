package kz.pompei.glazga.moc.model.complex_text.block;

public class BlockRect extends Block {
  public double centerX;
  public double centerY;
  public double width;
  public double height;
  public String comment;

  public BlockRect centerX(double centerX) {
    this.centerX = centerX;
    return this;
  }

  public BlockRect centerY(double centerY) {
    this.centerY = centerY;
    return this;
  }

  public BlockRect width(double width) {
    this.width = width;
    return this;
  }

  public BlockRect height(double height) {
    this.height = height;
    return this;
  }

  public BlockRect comment(String comment) {
    this.comment = comment;
    return this;
  }

  @Override
  public BlockRect name(String name) {
    return (BlockRect) super.name(name);
  }

  @Override
  public String toString() {
    return "BlockRect{" +
      "centerX=" + centerX +
      ", centerY=" + centerY +
      ", width=" + width +
      ", height=" + height +
      ", comment='" + comment + '\'' +
      ", name='" + name + '\'' +
      '}';
  }
}
