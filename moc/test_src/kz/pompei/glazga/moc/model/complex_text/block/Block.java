package kz.pompei.glazga.moc.model.complex_text.block;

import kz.pompei.glazga.ann.DiscriminatorClassSimpleName;

@DiscriminatorClassSimpleName("type")
public abstract class Block {

  public String name;

  public Block name(String name) {
    this.name = name;
    return this;
  }
}
