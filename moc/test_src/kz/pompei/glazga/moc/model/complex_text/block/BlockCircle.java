package kz.pompei.glazga.moc.model.complex_text.block;

public class BlockCircle extends Block {
  public double centerX;
  public double centerY;
  public double radius;
  public String info;

  public BlockCircle centerX(double centerX) {
    this.centerX = centerX;
    return this;
  }

  public BlockCircle centerY(double centerY) {
    this.centerY = centerY;
    return this;
  }

  public BlockCircle radius(double radius) {
    this.radius = radius;
    return this;
  }

  public BlockCircle info(String info) {
    this.info = info;
    return this;
  }

  @Override
  public BlockCircle name(String name) {
    return (BlockCircle) super.name(name);
  }

  @Override
  public String toString() {
    return "BlockCircle{" +
      "centerX=" + centerX +
      ", centerY=" + centerY +
      ", radius=" + radius +
      ", info='" + info + '\'' +
      ", name='" + name + '\'' +
      '}';
  }
}
