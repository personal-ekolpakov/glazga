package kz.pompei.glazga.moc;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class TypeRef<T> {
  public final Type type;

  public TypeRef() {
    Type superClass = getClass().getGenericSuperclass();
    if (superClass instanceof Class) {
      throw new RuntimeException("8GeP4Q0N0e :: Missing type parameter");
    }

    ParameterizedType parType = (ParameterizedType) superClass;
    type = parType.getActualTypeArguments()[0];
  }
}
