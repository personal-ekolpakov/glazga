package kz.pompei.glazga.moc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import kz.pompei.glazga.ann.ConvertToStr;
import kz.pompei.glazga.ann.FromString;
import lombok.NonNull;

public class StrConvertUtil {

  private static final Map<Class<?>, Function<Object, String>> toStrFunctions = new ConcurrentHashMap<>();

  static {

    {
      Function<Object, String> simple = object -> "" + object;
      toStrFunctions.put(int.class, simple);
      toStrFunctions.put(Integer.class, simple);
      toStrFunctions.put(long.class, simple);
      toStrFunctions.put(Long.class, simple);
      toStrFunctions.put(short.class, simple);
      toStrFunctions.put(Short.class, simple);
      toStrFunctions.put(double.class, simple);
      toStrFunctions.put(Double.class, simple);
      toStrFunctions.put(float.class, simple);
      toStrFunctions.put(Float.class, simple);
      toStrFunctions.put(byte.class, simple);
      toStrFunctions.put(Byte.class, simple);
      toStrFunctions.put(char.class, simple);
      toStrFunctions.put(Character.class, simple);
      toStrFunctions.put(boolean.class, simple);
      toStrFunctions.put(Boolean.class, simple);
      toStrFunctions.put(String.class, simple);
      toStrFunctions.put(BigDecimal.class, simple);
    }

    toStrFunctions.put(Date.class, new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")::format);

  }

  private static final Map<Class<?>, Function<String, Object>> fromStrFunctions = new ConcurrentHashMap<>();

  static {
    fromStrFunctions.put(float.class, s -> s.isEmpty() ? null : Float.parseFloat(s));
    fromStrFunctions.put(Float.class, s -> s.isEmpty() ? null : Float.parseFloat(s));
    fromStrFunctions.put(double.class, s -> s.isEmpty() ? null : Double.parseDouble(s));
    fromStrFunctions.put(Double.class, s -> s.isEmpty() ? null : Double.parseDouble(s));

    fromStrFunctions.put(int.class, s -> s.isEmpty() ? null : Integer.parseInt(s));
    fromStrFunctions.put(Integer.class, s -> s.isEmpty() ? null : Integer.parseInt(s));
    fromStrFunctions.put(long.class, s -> s.isEmpty() ? null : Long.parseLong(s));
    fromStrFunctions.put(Long.class, s -> s.isEmpty() ? null : Long.parseLong(s));
    fromStrFunctions.put(short.class, s -> s.isEmpty() ? null : Short.parseShort(s));
    fromStrFunctions.put(Short.class, s -> s.isEmpty() ? null : Short.parseShort(s));
    fromStrFunctions.put(byte.class, s -> s.isEmpty() ? null : Byte.parseByte(s));
    fromStrFunctions.put(Byte.class, s -> s.isEmpty() ? null : Byte.parseByte(s));
    fromStrFunctions.put(char.class, s -> s.isEmpty() ? null : s.charAt(0));
    fromStrFunctions.put(Character.class, s -> s.isEmpty() ? null : s.charAt(0));
    fromStrFunctions.put(boolean.class, "true"::equals);
    fromStrFunctions.put(Boolean.class, "true"::equals);
    fromStrFunctions.put(String.class, s -> s);
    fromStrFunctions.put(BigDecimal.class, BigDecimal::new);
  }

  public static <T> T convertFromStrOrCast(Object object, Class<T> targetClass) {
    if (object == null) {
      return null;
    }
    if (object instanceof CharSequence s) {
      String str = s.toString();
      return convertFromStr(str, targetClass);
    }

    return targetClass.cast(object);
  }

  private static <T> T convertFromStr(@NonNull String str, Class<?> targetClass) {
    {
      Function<String, Object> function = fromStrFunctions.get(targetClass);
      if (function != null) {
        //noinspection unchecked
        return (T) function.apply(str);
      }
    }

    for (final Method method : targetClass.getMethods()) {

      if (true

          && Modifier.isStatic(method.getModifiers())
          && method.getAnnotation(FromString.class) != null
          && method.getParameterTypes().length == 1
          && method.getParameterTypes()[0] == String.class

      ) {

        Function<String, Object> function = obj -> {
          try {
            return method.invoke(null, obj);
          } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
          } catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause instanceof RuntimeException x) {
              throw x;
            }
            throw new RuntimeException(cause);
          }
        };

        fromStrFunctions.put(targetClass, function);

        //noinspection unchecked
        return (T) function.apply(str);
      }

    }

    throw new RuntimeException("zwo05PylDe :: Cannot convert ot type: " + targetClass + " - not registered"
                               + " and no static method with annotation " + FromString.class);
  }

  public static String convertToStr(Object object) {

    if (object == null) {
      return null;
    }

    Class<?> objectClass = object.getClass();

    {
      Function<Object, String> function = toStrFunctions.get(objectClass);
      if (function != null) {
        return function.apply(object);
      }
    }


    for (final Method method : objectClass.getMethods()) {

      if (true

          && !Modifier.isStatic(method.getModifiers())
          && method.getAnnotation(ConvertToStr.class) != null
          && method.getParameterTypes().length == 0
          && method.getReturnType() == String.class

      ) {

        Function<Object, String> function = obj -> {
          try {
            return (String) method.invoke(obj);
          } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
          } catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause instanceof RuntimeException x) {
              throw x;
            }
            throw new RuntimeException(cause);
          }
        };

        toStrFunctions.put(objectClass, function);

        return function.apply(object);
      }

    }


    throw new RuntimeException("5p1dqFUTFz :: Cannot convert " + objectClass + " to string."
                               + " No method without arguments, returning String,"
                               + " and marked by annotation " + ConvertToStr.class);
  }

}
