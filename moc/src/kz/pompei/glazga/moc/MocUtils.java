package kz.pompei.glazga.moc;

import java.util.List;
import java.util.Map;
import lombok.NonNull;

public class MocUtils {
  public static void printMap(String placeId, Map<?, ?> map) {
    map.keySet().stream().sorted().forEach(key -> System.out.println(placeId + " :: " + key + " = " + map.get(key)));
  }


  public static double strToDouble(String str) {
    if (str == null) {
      return 0;
    }

    String correctedStr = str.replaceAll("[\\s_]+", "").replace(',', '.');

    return Double.parseDouble(correctedStr);
  }

  public static int strToInt(String str) {
    if (str == null) {
      return 0;
    }

    String correctedStr = str.replaceAll("[\\s_]+", "");

    return Integer.parseInt(correctedStr);
  }

  public static boolean strToBool(String str) {
    if (str == null) {
      return false;
    }

    return switch (str.trim().toLowerCase()) {
      case "true", "yes", "да", "1" -> true;
      default -> false;
    };
  }

  public static void append(@NonNull StringBuilder sb, Object object) {
    append1(sb, 0, object);
  }

  private static final int TAB = 2;

  private static void appendSpaces(StringBuilder sb, int spaces) {
    sb.append(" ".repeat(spaces * TAB));
  }

  private static class Null {
    @Override
    public String toString() {
      return "NULL";
    }
  }

  private static final Null NULL = new Null();

  private static void append1(@NonNull StringBuilder sb, int level, Object object) {

    if (object == null) {
      sb.append(NULL);
      return;
    }

    if (object instanceof List<?> list) {
      sb.append("[");

      boolean first = true;
      for (final Object element : list) {
        if (first) {
          first = false;
        } else {
          sb.append(", ");
        }
        append1(sb, level + 1, element);
      }

      sb.append("]");
      return;
    }

    if (object instanceof Map<?, ?> map) {
      List<? extends Map.Entry<String, ?>> list = map.entrySet()
                                                     .stream()
                                                     .map(e -> Map.entry("" + e.getKey(), e.getValue() == null ? NULL : e.getValue()))
                                                     .sorted(Map.Entry.comparingByKey())
                                                     .toList();

      sb.append("{\n");

      boolean first = true;
      for (final Map.Entry<String, ?> e : list) {
        if (first) {
          first = false;
        } else {
          sb.append(",\n");
        }

        appendSpaces(sb, level + 1);
        sb.append(e.getKey()).append(" => ");
        append1(sb, level + 2, e.getValue());
      }

      sb.append("\n");
      appendSpaces(sb, level);
      sb.append("}");

      return;
    }

    if (object instanceof String s) {
      sb.append('"').append(s).append('"');
      return;
    }

    sb.append("" + object);

  }

  public static String toPrettyStr(Object object) {
    StringBuilder sb = new StringBuilder();
    append(sb, object);
    return sb.toString();
  }

}
