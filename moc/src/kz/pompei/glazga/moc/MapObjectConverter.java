package kz.pompei.glazga.moc;

import java.beans.ConstructorProperties;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import kz.pompei.glazga.moc.class_meta.ClassMeta;
import kz.pompei.glazga.moc.class_meta.ClassMetaManager;
import kz.pompei.glazga.moc.class_meta.DiscriminatorDef;
import kz.pompei.glazga.moc.class_meta.FieldAccessor;
import lombok.RequiredArgsConstructor;

import static java.util.stream.Collectors.toUnmodifiableSet;
import static kz.pompei.glazga.moc.MocUtils.strToBool;
import static kz.pompei.glazga.moc.MocUtils.strToDouble;
import static kz.pompei.glazga.moc.MocUtils.strToInt;

public class MapObjectConverter {
  private static final ClassMetaManager cmm = new ClassMetaManager();

  private final Set<Class<?>> directClasses = new HashSet<>();

  {
    directClasses.add(CharSequence.class);
    directClasses.add(Date.class);
    directClasses.add(BigDecimal.class);
    directClasses.add(BigInteger.class);
    directClasses.add(Map.class);
  }

  private final Set<Class<?>> directConvertClasses = new HashSet<>();

  {
    directConvertClasses.add(Date.class);
    directConvertClasses.add(Integer.class);
    directConvertClasses.add(Long.class);
    directConvertClasses.add(Double.class);
    directConvertClasses.add(Float.class);
    directConvertClasses.add(Boolean.class);
    directConvertClasses.add(Short.class);
    directConvertClasses.add(Character.class);
    directConvertClasses.add(Byte.class);
    directConvertClasses.add(String.class);
    directConvertClasses.add(BigDecimal.class);
  }

  public void convertClasses_add(Class<?> convertClass) {
    directConvertClasses.add(convertClass);
    directClasses.add(convertClass);
  }

  private static <T, V> Map<T, V> createMapInstance(Class<?> mapClass) {
    if (mapClass == Map.class || mapClass == HashMap.class || AbstractMap.class.isAssignableFrom(mapClass)) {
      return new HashMap<>();
    }

    if (mapClass == TreeMap.class) {
      //noinspection SortedCollectionWithNonComparableKeys
      return new TreeMap<>();
    }

    throw new RuntimeException("rNYk1gzt3s :: Cannot create class instance for mapClass " + mapClass);
  }

  private static <T> T primitiveConvert(Object primitiveValue, Class<T> modelClass) {
    if (primitiveValue == null) {
      if (modelClass == int.class) {
        //noinspection unchecked
        return (T) (Integer) 0;
      }
      if (modelClass == long.class) {
        //noinspection unchecked
        return (T) (Long) 0L;
      }
      if (modelClass == float.class) {
        //noinspection unchecked
        return (T) (Float) 0f;
      }
      if (modelClass == double.class) {
        //noinspection unchecked
        return (T) (Double) 0d;
      }
      if (modelClass == boolean.class) {
        //noinspection unchecked
        return (T) (Boolean) false;
      }
      if (modelClass == short.class) {
        //noinspection unchecked
        return (T) (Short) (short) 0;
      }
      if (modelClass == byte.class) {
        //noinspection unchecked
        return (T) (Byte) (byte) 0;
      }
      if (modelClass == char.class) {
        //noinspection unchecked
        return (T) (Character) (char) 0;
      }
      return null;
    }

    if (primitiveValue instanceof Integer && (modelClass == Double.class || modelClass == double.class)) {
      //noinspection unchecked
      return (T) (Double) (double) (Integer) primitiveValue;
    }

    if (primitiveValue instanceof String) {

      if (modelClass == double.class || modelClass == Double.class) {
        //noinspection unchecked
        return (T) (Object) strToDouble((String) primitiveValue);
      }

      if (modelClass == int.class || modelClass == Integer.class) {
        //noinspection unchecked
        return (T) (Object) strToInt((String) primitiveValue);
      }

      if (modelClass == boolean.class || modelClass == Boolean.class) {
        //noinspection unchecked
        return (T) (Object) strToBool((String) primitiveValue);
      }

    }

    return modelClass.cast(primitiveValue);
  }

  public <T> T convertFromMap(Object data, Type modelType) {

    if (data == null) {
      return null;
    }

    if (modelType instanceof Class) {

      //noinspection unchecked
      Class<T> modelClass = (Class<T>) modelType;

      if (modelClass.isPrimitive()) {
        return primitiveConvert(data, modelClass);
      }

      for (final Class<?> directClass : directClasses) {
        if (directClass.isAssignableFrom(modelClass)) {
          return StrConvertUtil.convertFromStrOrCast(data, modelClass);
        }
      }

      ClassMeta classMeta = cmm.get(modelClass);

      var discriminatorOpt = classMeta.discriminator();

      if (discriminatorOpt.isEmpty()) {
        return createInstance(data, modelClass);
      }

      {
        DiscriminatorDef discriminator = discriminatorOpt.orElseThrow();

        String fieldValue = (String) ((Map<?, ?>) data).get(discriminator.fieldName());

        Class<?> childClass = discriminator.getChildClass(fieldValue)
                                           .orElseThrow(() -> new RuntimeException("JHLEwMUev0 :: No child class for field value"
                                                                                     + " = `" + fieldValue + "`"
                                                                                     + " of discriminating class"
                                                                                     + " `" + modelClass.getSimpleName() + "`"));

        return createInstance(data, childClass);
      }
    }

    if (modelType instanceof ParameterizedType parType) {

      if (parType.getRawType() instanceof Class<?> rowClass) {
        Type[] argTypes = parType.getActualTypeArguments();
        if (Map.class.isAssignableFrom(rowClass) && argTypes.length == 2) {

          if (data instanceof Map<?, ?> dataMap) {

            Map<Object, Object> mapInstance = createMapInstance(data.getClass());

            for (final Map.Entry<?, ?> e : dataMap.entrySet()) {
              Object convertedKey   = convertFromMap(e.getKey(), argTypes[0]);
              Object convertedValue = convertFromMap(e.getValue(), argTypes[1]);
              if (convertedKey != null && convertedValue != null) {
                mapInstance.put(convertedKey, convertedValue);
              }
            }

            //noinspection unchecked
            return (T) mapInstance;
          }

          throw new RuntimeException("i5wNluVBB4 :: Cannot convert type " + data.getClass().getSimpleName() + " to " + modelType);
        }
      }

    }

    throw new RuntimeException("83sc4eOq1f :: Cannot converting to type " + modelType);
  }

  private static String extractStringForEnumValue(Object data) {
    if (data == null) {
      return null;
    }
    if (data instanceof CharSequence) {
      return data.toString();
    }

    if (data instanceof Enum<?> e) {
      return e.name();
    }

    throw new RuntimeException("aME1R2X37r :: Cannot convert type " + data.getClass().getSimpleName() + " to string for Enum");
  }

  private <T> T createInstance(Object data, Class<?> aClass) {

    if (aClass.isEnum()) {
      String enumValue = extractStringForEnumValue(data);
      if (enumValue == null) {
        return null;
      }

      try {
        //noinspection unchecked,rawtypes
        return (T) Enum.valueOf((Class<? extends Enum>) aClass, enumValue);
      } catch (IllegalArgumentException e) {
        return null;
      }
    }

    @RequiredArgsConstructor
    class Constr {
      final Constructor<?> source;
      final String[]       argFieldNames;
      final Class<?>[]     argTypes;

      @Override
      public String toString() {
        return "CONSTRUCTOR (" + String.join(", ", argFieldNames) + ")";
      }
    }

    Constructor<?>[] constructors = aClass.getConstructors();

    List<Constr> list = new ArrayList<>();

    for (final Constructor<?> constructor : constructors) {
      if (constructor.getParameterTypes().length == 0) {
        list.add(new Constr(constructor, new String[]{}, new Class<?>[]{}));
        continue;
      }

      ConstructorProperties cProp = constructor.getAnnotation(ConstructorProperties.class);
      if (cProp == null) {
        continue;
      }

      list.add(new Constr(constructor, cProp.value(), constructor.getParameterTypes()));
    }

    if (list.isEmpty()) {
      throw new RuntimeException("yMET8P1JV4 :: Cannot find constructor for class " + aClass.getSimpleName());
    }

    list.sort(Comparator.comparing(x -> x.argFieldNames.length));

    Constr constr = list.get(list.size() - 1);

    int      argCount = constr.argFieldNames.length;
    Object[] args     = new Object[argCount];

    //noinspection unchecked
    Map<String, Object> dataMap = (Map<String, Object>) data;

    for (int i = 0; i < argCount; i++) {
      args[i] = convertFromMap(dataMap.get(constr.argFieldNames[i]), constr.argTypes[i]);
    }

    Object instance;

    try {
      instance = constr.source.newInstance(args);
    } catch (InstantiationException | IllegalAccessException e) {
      throw new RuntimeException(e);
    } catch (InvocationTargetException e) {
      throw new RuntimeException("1C75liXdY4 :: InvocationTargetException", e.getCause());
    }

    Set<String> argFieldNames = Arrays.stream(constr.argFieldNames).collect(toUnmodifiableSet());

    ClassMeta meta = cmm.get(aClass);

    for (final String fieldName : meta.fieldNames()) {
      if (argFieldNames.contains(fieldName)) {
        continue;
      }

      if (!dataMap.containsKey(fieldName)) {
        continue;
      }


      FieldAccessor accessor  = meta.accessor(fieldName);
      Object        comeValue = dataMap.get(fieldName);
      Type          fieldType = accessor.fieldType();

      Object convertedComeValue = convertFromMap(comeValue, fieldType);
      accessor.set(instance, convertedComeValue);
    }

    //noinspection unchecked
    return (T) instance;
  }


  private Object convertInner(Object object, Map<Integer, Object> cache) {

    if (object == null) {
      return null;
    }

    int objectId = System.identityHashCode(object);

    Object fromCache = cache.get(objectId);
    if (fromCache != null) {
      return fromCache;
    }

    Object ret = directConvert(object, cache);

    cache.put(objectId, ret);

    return ret;
  }

  private Object directConvert(Object object, Map<Integer, Object> cache) {
    Class<?> objectClass = object.getClass();

    if (directConvertClasses.contains(objectClass)) {
      return object;
    }

    if (object instanceof byte[]) {
      return Base64.getEncoder().encodeToString((byte[]) object);
    }

    if (objectClass.isArray()) {
      throw new RuntimeException("02Cnn4mAII :: Arrays cannot be used:  " + objectClass);
    }

    if (object instanceof Collection<?>) {
      throw new RuntimeException("bOI7DW4cF2 :: Collections cannot be used: " + objectClass);
    }

    if (object instanceof Enum) {
      return ((Enum<?>) object).name();
    }

    if (object instanceof Map) {
      Map<String, Object> ret = new HashMap<>();
      //noinspection unchecked
      for (final Map.Entry<Object, Object> e : ((Map<Object, Object>) object).entrySet()) {
        String key = convertMapKey(e.getKey());
        if (key != null) {
          ret.put(key, convertInner(e.getValue(), cache));
        }
      }
      return ret;
    }

    ClassMeta classMeta = cmm.get(objectClass);

    Map<String, Object> ret = new HashMap<>();

    for (final String fieldName : classMeta.fieldNames()) {
      FieldAccessor accessor = classMeta.accessor(fieldName);
      ret.put(fieldName, convertInner(accessor.get(object), cache));
    }

    return ret;
  }

  private static String convertMapKey(Object mapKey) {
    if (mapKey == null) {
      return null;
    }
    if (mapKey instanceof String) {
      return (String) mapKey;
    }
    if (mapKey instanceof Enum) {
      return ((Enum<?>) mapKey).name();
    }
    return "" + mapKey;
  }

  public Map<String, Object> convertToMap(Object object) {
    //noinspection unchecked
    return (Map<String, Object>) convertInner(object, new HashMap<>());
  }
}
