package kz.pompei.glazga.moc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

public class OneLevel {

  public final Map<String, String> target;

  public OneLevel(Map<String, String> target) {
    this.target = target;
  }

  public OneLevel() {
    this(new HashMap<>());
  }

  public Map<String, String> append(Map<String, Object> deepMap) {

    class State {
      final List<String> keys   = new ArrayList<>();
      final List<Object> values = new ArrayList<>();

      int currentIndex = 0;

      private void add(String key, Object value) {
        keys.add(key);
        values.add(value);
      }

    }

    Function<Map<String, Object>, State> toState = deepMapArg -> {
      State ret = new State();
      deepMapArg.forEach(ret::add);
      return ret;
    };

    List<State> stack = new ArrayList<>();
    stack.add(toState.apply(deepMap));

    while (stack.size() > 0) {
      State last = stack.get(stack.size() - 1);
      if (last.currentIndex >= last.keys.size()) {
        stack.remove(stack.size() - 1);
        if (stack.size() > 0) {
          stack.get(stack.size() - 1).currentIndex++;
        }
        continue;
      }

      Object lastValue = last.values.get(last.currentIndex);

      if (lastValue instanceof Map) {
        //noinspection unchecked
        stack.add(toState.apply((Map<String, Object>) lastValue));
        continue;
      }

      List<String> currentKey = new ArrayList<>();
      for (final State state : stack) {
        currentKey.add(state.keys.get(state.currentIndex));
      }

      target.put(String.join(".", currentKey), StrConvertUtil.convertToStr(lastValue));
      last.currentIndex++;

    }

    return target;
  }

  public static void set(Map<String, Object> deepMap, String pathName, Object value) {

    Map<String, Object> current = deepMap;

    List<String> left = Arrays.stream(pathName.split("\\.")).collect(toList());
    String       last = left.remove(left.size() - 1);

    for (final String name : left) {
      Object point = current.get(name);
      if (point instanceof Map) {
        //noinspection unchecked
        current = (Map<String, Object>) point;
        continue;
      }
      Map<String, Object> newMap = new HashMap<>();
      current.put(name, newMap);
      current = newMap;
    }

    current.put(last, value);
  }
}
