package kz.pompei.glazga.moc.class_meta;

public class NoFieldGetter extends RuntimeException implements NoFieldGetterOrSetter {
  private final String   placeId;
  private final Class<?> aClass;
  private final String   fieldName;

  public NoFieldGetter(String placeId, Class<?> aClass, String fieldName) {
    super(placeId + " :: No getter " + aClass.getSimpleName() + "." + fieldName + " : " + aClass.getSimpleName() + "=" + aClass.getName());
    this.placeId   = placeId;
    this.aClass    = aClass;
    this.fieldName = fieldName;
  }

  @Override
  public String placeId() {
    return placeId;
  }

  @Override
  public Class<?> aClass() {
    return aClass;
  }

  @Override
  public String fieldName() {
    return fieldName;
  }
}
