package kz.pompei.glazga.moc.class_meta;

import java.lang.reflect.Type;

public interface FieldGetter {

  Object get(Object self);

  Type fieldType();

}
