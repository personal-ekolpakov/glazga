package kz.pompei.glazga.moc.class_meta;

import java.util.Optional;
import java.util.Set;

public interface ClassMeta {

  Set<String> fieldNames();

  FieldAccessor accessor(String fieldName) throws NoFieldAccessor;

  Optional<DiscriminatorDef> discriminator();

}
