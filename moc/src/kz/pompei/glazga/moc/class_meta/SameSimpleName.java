package kz.pompei.glazga.moc.class_meta;

public class SameSimpleName extends RuntimeException {
  public SameSimpleName(String placeId, Class<?> baseClass, Class<?> childClass1, Class<?> childClass2) {
    super(placeId + " :: Same Simple names of classes " + childClass1.getName() + ", " + childClass2.getName()
            + " - base class: " + baseClass.getName());
  }
}
