package kz.pompei.glazga.moc.class_meta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import kz.pompei.glazga.ann.DiscriminatorClassSimpleName;
import org.reflections.Reflections;
import org.reflections.scanners.Scanners;

public class DiscriminatorExtractor {

  public static Optional<DiscriminatorDef> extract(Class<?> aClass) {
    String simpleClassDiscriminatorValue = Optional.ofNullable(aClass.getAnnotation(DiscriminatorClassSimpleName.class))
                                                   .map(DiscriminatorClassSimpleName::value)
                                                   .orElse(null);

    if (simpleClassDiscriminatorValue != null) {

      Map<String, Class<?>> childrenBySimpleName = loadChildren(aClass);

      return Optional.of(new DiscriminatorDef() {
        @Override
        public String fieldName() {
          return simpleClassDiscriminatorValue;
        }

        @Override
        public Optional<Class<?>> getChildClass(String fieldValue) {
          return Optional.ofNullable(childrenBySimpleName.get(fieldValue));
        }

        @Override
        public String getFieldValue(Class<?> childClass) {
          return childClass.getSimpleName();
        }
      });
    }

    return Optional.empty();
  }

  public static Optional<DiscriminatorDef> extractParent(Class<?> aClass) {

    Set<Class<?>> visited = new HashSet<>();
    visited.add(Object.class);
    visited.add(aClass);

    List<Class<?>> classes = new ArrayList<>();
    classes.add(aClass);

    while (classes.size() > 0) {
      Class<?> current = classes.remove(0);

      {
        Class<?> superclass = current.getSuperclass();
        if (superclass != null) {
          classes.add(superclass);
        }
        classes.addAll(Arrays.asList(current.getInterfaces()));
      }

      if (visited.contains(current)) {
        continue;
      }
      visited.add(current);

      Optional<DiscriminatorDef> extracted = extract(current);
      if (extracted.isPresent()) {
        return extracted;
      }

    }

    return Optional.empty();
  }

  private static Map<String, Class<?>> loadChildren(Class<?> topClass) {

    String packageName = topClass.getPackageName();

    Reflections reflections = new Reflections(packageName);

    Set<Class<?>> classes = reflections.get(Scanners.SubTypes.of(topClass).asClass());

    Map<String, Class<?>> simpleNames = new HashMap<>();

    for (final Class<?> cl : classes) {
      Class<?> sameClass = simpleNames.get(cl.getSimpleName());
      if (sameClass != null) {
        throw new SameSimpleName("k75a1uM1EM", cl, sameClass, topClass);
      }
      simpleNames.put(cl.getSimpleName(), cl);
    }

    return Map.copyOf(simpleNames);
  }
}
