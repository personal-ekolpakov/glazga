package kz.pompei.glazga.moc.class_meta;

public interface FieldAccessor extends FieldGetter, FieldSetter {}
