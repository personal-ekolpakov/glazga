package kz.pompei.glazga.moc.class_meta;

public interface NoFieldGetterOrSetter {

  String placeId();

  Class<?> aClass();

  String fieldName();

}
