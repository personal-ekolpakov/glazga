package kz.pompei.glazga.moc.class_meta;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import lombok.SneakyThrows;

import static java.util.stream.Collectors.joining;

public class ClassMetaParser {

  private final Class<?> aClass;

  private ClassMetaParser(Class<?> aClass) {this.aClass = aClass;}

  public static ClassMeta parse(Class<?> aClass) {
    ClassMetaParser classMetaParser = new ClassMetaParser(aClass);
    classMetaParser.doParse();
    return classMetaParser.meta();
  }

  private ClassMeta meta;

  private ClassMeta meta() {
    return meta;
  }

  private static String firstLow(String str) {
    if (str == null) {
      return null;
    }
    if (str.length() <= 1) {
      return str.toLowerCase();
    }
    return str.substring(0, 1).toLowerCase() + str.substring(1);
  }

  private static boolean isBoolType(Class<?> aClass) {
    return aClass == Boolean.class || aClass == boolean.class;
  }

  private static <T> Set<T> union(Collection<T> a, Collection<T> b) {
    if (a == null && b == null) {
      return Set.of();
    }
    if (a == null) {
      return Set.copyOf(b);
    }
    if (b == null) {
      return Set.copyOf(a);
    }
    Set<T> set = new HashSet<>(a);
    set.addAll(b);
    return Set.copyOf(set);
  }

  private void doParse() {

    Map<String, FieldGetter> getters = new HashMap<>();
    Map<String, FieldSetter> setters = new HashMap<>();

    for (final Field field : aClass.getFields()) {
      if (Modifier.isStatic(field.getModifiers())) {
        continue;
      }

      if (field.getAnnotation(Ignore.class) != null) {
        continue;
      }

      getters.put(field.getName(), new FieldGetter() {
        @Override
        @SneakyThrows
        public Object get(Object self) {
          return field.get(self);
        }

        @Override
        public Type fieldType() {
          return field.getGenericType();
        }
      });

      setters.put(field.getName(), new FieldSetter() {
        @Override
        @SneakyThrows
        public void set(Object self, Object value) {
          field.set(self, value);
        }

        @Override
        public Type fieldType() {
          return field.getGenericType();
        }
      });
    }

    for (final Method method : aClass.getMethods()) {
      if (Modifier.isStatic(method.getModifiers())) {
        continue;
      }

      if (method.getAnnotation(Ignore.class) != null) {
        continue;
      }

      String methodName = method.getName();

      if ("getClass".equals(methodName)) {
        continue;
      }

      int parametersCount = method.getParameterTypes().length;

      if (parametersCount == 0) {

        if (methodName.length() >= 4 && methodName.startsWith("get")) {
          String fieldName = firstLow(methodName.substring(3));

          getters.put(fieldName, new FieldGetter() {
            @Override
            public Object get(Object self) {
              try {
                return method.invoke(self);
              } catch (IllegalAccessException e) {
                throw new RuntimeException("JfLY8WC2WW :: IllegalAccessException", e);
              } catch (InvocationTargetException e) {
                if (e.getCause() instanceof RuntimeException) {
                  throw (RuntimeException) e.getCause();
                } else {
                  throw new RuntimeException("Lfv5eK4y8C :: Err in getter invoke", e.getCause());
                }
              }
            }

            @Override
            public Type fieldType() {
              return method.getGenericReturnType();
            }
          });

          continue;
        }

        if (methodName.length() >= 3 && methodName.startsWith("is") && isBoolType(method.getReturnType())) {
          String fieldName = firstLow(methodName.substring(2));

          getters.put(fieldName, new FieldGetter() {
            @Override
            public Object get(Object self) {
              try {
                return method.invoke(self);
              } catch (IllegalAccessException e) {
                throw new RuntimeException("I8smeG38Dg :: IllegalAccessException", e);
              } catch (InvocationTargetException e) {
                if (e.getCause() instanceof RuntimeException) {
                  throw (RuntimeException) e.getCause();
                } else {
                  throw new RuntimeException("vu1rAMaQmK :: Err in getter invoke", e.getCause());
                }
              }
            }

            @Override
            public Type fieldType() {
              return method.getGenericReturnType();
            }
          });

          continue;
        }
        continue;
      }

      if (parametersCount == 1 && methodName.length() >= 4 && methodName.startsWith("set") && method.getReturnType() == void.class) {
        String fieldName = firstLow(methodName.substring(3));

        setters.put(fieldName, new FieldSetter() {
          @Override
          public void set(Object self, Object value) {
            try {
              method.invoke(self, value);
            } catch (IllegalAccessException e) {
              throw new RuntimeException("Ro1cQ3krMd :: IllegalAccessException", e);
            } catch (InvocationTargetException e) {
              if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException) e.getCause();
              } else {
                throw new RuntimeException("TQ5Ka6u83D :: Err in getter invoke", e.getCause());
              }
            }
          }

          @Override
          public Type fieldType() {
            return method.getGenericParameterTypes()[0];
          }
        });

        continue;
      }

    }

    Set<String> fieldNames = union(getters.keySet(), setters.keySet());

    Map<String, FieldAccessor> accessors = new HashMap<>();

    for (final String fieldName : fieldNames) {
      accessors.put(fieldName, new FieldAccessor() {

        final FieldGetter getter = getters.get(fieldName);
        final FieldSetter setter = setters.get(fieldName);

        @Override
        public Object get(Object self) {
          if (getter == null) {
            throw new NoFieldGetter("h6F48elDZV", aClass, fieldName);
          }
          return getter.get(self);
        }

        @Override
        public void set(Object self, Object value) {
          if (setter == null) {
            throw new NoFieldSetter("FjR070wmBe", aClass, fieldName);
          }
          setter.set(self, value);
        }

        @Override
        public Type fieldType() {
          return getter != null ? getter.fieldType() : setter.fieldType();
        }
      });
    }

    Set<String> fieldNames2 = new HashSet<>(fieldNames);

    var discriminatorParent = DiscriminatorExtractor.extractParent(aClass);

    discriminatorParent.ifPresent(discriminatorDef -> {

      String fieldName = discriminatorDef.fieldName();

      fieldNames2.add(fieldName);

      accessors.put(fieldName, new FieldAccessor() {
        @Override
        @SneakyThrows
        public Object get(Object self) {
          return discriminatorDef.getFieldValue(self.getClass());
        }

        @Override
        public Type fieldType() {
          return String.class;
        }

        @Override
        public void set(Object self, Object value) {
          // do nothing
        }
      });


    });

    Set<String> fieldNames3 = Set.copyOf(fieldNames2);

    final Optional<DiscriminatorDef> discriminator = DiscriminatorExtractor.extract(aClass);

    meta = new ClassMeta() {

      @Override
      public Set<String> fieldNames() {
        return fieldNames3;
      }

      @Override
      public FieldAccessor accessor(String fieldName) throws NoFieldAccessor {
        FieldAccessor accessor = accessors.get(fieldName);
        if (accessor != null) {
          return accessor;
        }

        throw new NoFieldAccessor("5CNOvzXCpw", aClass, fieldName);
      }

      @Override
      public Optional<DiscriminatorDef> discriminator() {
        return discriminator;
      }

      @Override
      public String toString() {
        var dfn = discriminator.map(DiscriminatorDef::fieldName).orElse(null);
        return "ClassMeta(" + aClass.getSimpleName() + "){"
          + "fields: " + fieldNames().stream().sorted().collect(joining(",")) + (dfn == null ? "" : ", discriminatorField: " + dfn) + "}";
      }
    };
  }

}
