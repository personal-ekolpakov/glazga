package kz.pompei.glazga.moc.class_meta;

public class NoFieldAccessor extends RuntimeException {
  public final String   placeId;
  public final Class<?> aClass;
  public final String   fieldName;

  public NoFieldAccessor(String placeId, Class<?> aClass, String fieldName) {
    super(placeId + " :: No accessor " + aClass.getSimpleName() + "." + fieldName + " : " + aClass.getSimpleName() + "=" + aClass.getName());
    this.placeId = placeId;
    this.aClass    = aClass;
    this.fieldName = fieldName;
  }
}
