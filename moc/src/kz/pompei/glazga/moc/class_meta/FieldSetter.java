package kz.pompei.glazga.moc.class_meta;

import java.lang.reflect.Type;

public interface FieldSetter {

  void set(Object self, Object value);

  Type fieldType();

}
