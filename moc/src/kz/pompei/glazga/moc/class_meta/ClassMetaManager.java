package kz.pompei.glazga.moc.class_meta;

import java.util.concurrent.ConcurrentHashMap;

public class ClassMetaManager {
  private final ConcurrentHashMap<Class<?>, ClassMeta> store = new ConcurrentHashMap<>();

  public ClassMeta get(Class<?> aClass) {
    if (aClass == null) {
      throw new RuntimeException("p9PI1VW1V3 :: aClass == null");
    }
    ClassMeta classMeta = store.get(aClass);
    if (classMeta != null) {
      return classMeta;
    }
    //noinspection SynchronizationOnLocalVariableOrMethodParameter
    synchronized (aClass) {
      ClassMeta meta = createMeta(aClass);
      store.put(aClass, meta);
      return meta;
    }
  }

  private ClassMeta createMeta(Class<?> aClass) {
    {
      ClassMeta meta = store.get(aClass);
      if (meta != null) {
        return meta;
      }
    }

    {
      return ClassMetaParser.parse(aClass);
    }
  }

}
