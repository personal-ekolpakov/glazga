package kz.pompei.glazga.moc.class_meta;

import java.util.Optional;

public interface DiscriminatorDef {
  String fieldName();

  Optional<Class<?>> getChildClass(String fieldValue);

  String getFieldValue(Class<?> childClass);
}
