package kz.pompei.glazga.env;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Modules {

  public static Path prjRoot() {

    Path start = Paths.get(".").toFile().getAbsoluteFile().toPath().normalize();

    Path current = start;

    Path prev;

    while (true) {

      Path prjName = current.resolve(".greetgo").resolve("project-name.txt");
      if (Files.exists(prjName)) {
        return current;
      }

      prev = current;

      File parentFile = current.toFile().getParentFile();

      current = parentFile == null ? null : parentFile.toPath();

      String strPrev    = makeAbsoluteStr(prev);
      String strCurrent = makeAbsoluteStr(current);

      if (strCurrent == null || strCurrent.equals(strPrev)) {
        throw new RuntimeException("g2ssBBbVSI :: Cannot find project root from dir: " + makeAbsoluteStr(start));
      }
    }

  }

  private static String makeAbsoluteStr(Path path) {
    if (path == null) {
      return null;
    }

    return path.toFile().getAbsoluteFile().toPath().normalize().toString();
  }


  public static void main(String[] args) {
    System.out.println("dLpcJxo9Xv :: " + prjRoot());
  }
}
