package jars;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import static java.util.stream.Collectors.joining;

public class CreateJar extends DefaultTask {

  private final List<File> inputFiles = new ArrayList<>();

  private String mainClassName;
  private Path   outJarFile;

  public void mainClass(String mainClassName) {
    this.mainClassName = mainClassName;
  }

  public void outJarFile(Path outJarFile) {
    this.outJarFile = outJarFile;
  }

  public void addFileJarOrDir(File inputJarOrDir) {
    inputFiles.add(inputJarOrDir);
  }

  @TaskAction
  public void execute() throws Exception {

    String classPath = inputFiles.stream()
                                 .map(File::toPath)
                                 .map(Path::toString)
                                 .collect(joining(" "));

    Manifest manifest = new Manifest();

    manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
    manifest.getMainAttributes().put(Attributes.Name.CLASS_PATH, classPath);

    if (mainClassName != null && mainClassName.length() > 0) {
      manifest.getMainAttributes().put(Attributes.Name.MAIN_CLASS, mainClassName);
    }

    outJarFile.toFile().getParentFile().mkdirs();
    try (var jarStream = new JarOutputStream(new FileOutputStream(outJarFile.toFile()))) {
      jarStream.putNextEntry(new ZipEntry("META-INF/"));
      {
        jarStream.putNextEntry(new ZipEntry("META-INF/MANIFEST.MF"));
        manifest.write(jarStream);
        jarStream.closeEntry();
      }
      jarStream.closeEntry();
    }
  }
}
