package kz.pompei.store_map_db;

import java.nio.file.Path;
import java.util.Map;
import kz.pompei.glazga.store.Storage;
import lombok.RequiredArgsConstructor;
import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toMap;

@RequiredArgsConstructor
public class StorageMapDb implements Storage {
  private final Path root;

  private static final String ROOT = "root";

  private <Ret> Ret exec(String filePath,
                         @SuppressWarnings("SameParameterValue")
                         String innerTable,
                         HandleConnect<Ret> handler) {

    Path file = root.resolve(filePath);
    file.toFile().getParentFile().mkdirs();

    try (DB db = DBMaker.fileDB(file.toFile()).make()) {
      try (BTreeMap<byte[], String> tree = db.treeMap(innerTable, Serializer.BYTE_ARRAY, Serializer.STRING)
                                             .createOrOpen()) {
        return handler.handle(tree);
      }
    }
  }

  private void execDo(String filePath,
                      @SuppressWarnings("SameParameterValue")
                      String innerTable,
                      HandleConnectDo handleConnect) {
    exec(filePath, innerTable,
         (HandleConnect<Void>) tree -> {
           handleConnect.handle(tree);
           return null;
         });
  }

  @Override
  public Map<String, String> loadAll(String filePath) {
    return exec(filePath,
                ROOT,
                tree -> tree.entrySet()
                            .stream()
                            .collect(toMap(x -> new String(x.getKey(), UTF_8), Map.Entry::getValue)));
  }

  @Override
  public void save(String filePath, Map<String, String> updateData) {
    execDo(filePath, ROOT, tree -> updateData.forEach((key, value) -> {
      byte[] keyBytes = key.getBytes(UTF_8);
      if (value == null) {
        tree.remove(keyBytes);
      } else {
        tree.put(keyBytes, value);
      }
    }));
  }

  @Override
  public Map<String, String> loadPart(String filePath, String keyPrefix) {
    return exec(filePath, ROOT, tree -> tree.prefixSubMap(keyPrefix.getBytes(UTF_8), true)
                                            .entrySet()
                                            .stream()
                                            .collect(toMap(x -> new String(x.getKey(), UTF_8), Map.Entry::getValue)));

  }
}
