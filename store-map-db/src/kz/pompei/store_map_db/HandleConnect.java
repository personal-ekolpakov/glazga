package kz.pompei.store_map_db;

import org.mapdb.BTreeMap;

public interface HandleConnect<Ret> {
  Ret handle(BTreeMap<byte[], String> tree);
}
