package kz.pompei.store_map_db;

import org.mapdb.BTreeMap;

public interface HandleConnectDo {
  void handle(BTreeMap<byte[], String> tree);
}
