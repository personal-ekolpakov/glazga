package kz.pompei.store_map_db;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StorageMapDbTest {

  private Path root;

  @BeforeMethod
  public void prepareRootDir(Method testMethod) {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd__HH_mm_ss");

    root = Paths.get("build")
                .resolve("testData")
                .resolve(testMethod.getDeclaringClass().getSimpleName())
                .resolve(testMethod.getName())
                .resolve(sdf.format(new Date()));
  }

  @Test
  public void save__loadAll() {

    StorageMapDb storage = new StorageMapDb(root);

    Map<String, String> data = Map.of(
      "c1.name1", "value1",
      "c1.name2", "value2",
      "c1.name3", "value3",
      "c1.name4", "value4",
      "c2.name1", "left1",
      "c2.name2", "left3",
      "c2.name3", "left4",
      "c2.name4", "left5"
    );

    //
    //
    storage.save("test_dir/test_file", data);
    //
    //

    //
    //
    Map<String, String> actualData = storage.loadAll("test_dir/test_file");
    //
    //

    assertThat(actualData).isEqualTo(data);
  }

  @Test
  public void save__loadPart() {
    StorageMapDb storage = new StorageMapDb(root);

    Map<String, String> data = Map.of(
      "c1.name1", "value1",
      "c1.name2", "value2",
      "c1.name3", "value3",
      "c1.name4", "value4",
      "c2.name1", "left1",
      "c2.name2", "left3",
      "c2.name3", "left4",
      "c2.name4", "left5"
    );

    //
    //
    storage.save("test_dir/test_file", data);
    //
    //

    //
    //
    Map<String, String> actualData = storage.loadPart("test_dir/test_file", "c1");
    //
    //

    assertThat(actualData).isEqualTo(Map.of(
      "c1.name1", "value1",
      "c1.name2", "value2",
      "c1.name3", "value3",
      "c1.name4", "value4"
    ));


  }
}
