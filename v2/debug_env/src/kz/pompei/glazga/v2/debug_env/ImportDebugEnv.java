package kz.pompei.glazga.v2.debug_env;

import kz.pompei.glazga.v2.debug_conf.ImportDebugConf;
import kz.pompei.glazga.v2.pg.beans.ImportDb;
import org.springframework.context.annotation.Import;

@Import({
  ImportDb.class,
  ImportDebugConf.class,
})
public class ImportDebugEnv {}
