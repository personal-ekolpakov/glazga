package kz.pompei.glazga.v2.debug_env;

import kz.pompei.glazga.v2.pg.beans.DbConnectCheck;
import kz.pompei.glazga.v2.pg.beans.LiquibaseManager;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class LaunchUpdateLiquibase {
  public static void main(String[] args) {

    var context = new AnnotationConfigApplicationContext();

    context.register(ImportDebugEnv.class);

    for (String beanDefinitionName : context.getBeanDefinitionNames()) {
      BeanDefinition beanDefinition = context.getBeanDefinition(beanDefinitionName);
      beanDefinition.setLazyInit(true);
    }

    context.refresh();

    context.getBean(DbConnectCheck.class)
           .waitForDbConnect();

    LiquibaseManager liquibaseManager = context.getBean(LiquibaseManager.class);

    String operation = "probe";

    if (args.length > 0) {
      operation = args[0];
    }

    switch (operation) {
      case "update" -> liquibaseManager.update();
      case "probe" -> liquibaseManager.probe();
      default -> throw new RuntimeException("w1mjRd5XNX :: Unknown operation " + operation);
    }

  }
}
