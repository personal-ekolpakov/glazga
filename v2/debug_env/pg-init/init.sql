CREATE USER glazga WITH ENCRYPTED PASSWORD 'GTHzp0YXgWlQX5olLT0I';
ALTER USER glazga WITH CREATEROLE;
CREATE DATABASE glazga WITH OWNER glazga;
\c glazga
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION btree_gist;
