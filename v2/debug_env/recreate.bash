#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 131

#../../gradlew run_liquibase -Pop=probe

bash remove.bash

docker compose up -d

../../gradlew run_liquibase -Pop=update
