package kz.pompei.glazga.v2.debug_server.beans;

import java.lang.reflect.Method;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@Slf4j
@Component
public class RouteReporter implements CommandLineRunner {

  private final RequestMappingHandlerMapping handlerMapping;

  public RouteReporter(RequestMappingHandlerMapping handlerMapping) {
    this.handlerMapping = handlerMapping;
  }

  @Override
  public void run(String... args) {
    handlerMapping.getHandlerMethods().forEach((RequestMappingInfo key, HandlerMethod value) -> {
      if (log.isInfoEnabled()) {
        String pattern    = "" + key.getActivePatternsCondition();
        Method method     = value.getMethod();
        String methodName = method.getDeclaringClass().getSimpleName() + "." + method.getName() + "()";
        log.info("k5RT1ZAToa :: " + pattern + " -> " + methodName);
      }
    });
  }
}
