package kz.pompei.glazga.v2.debug_server.beans;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class ImportsDebugServerBeans {}
