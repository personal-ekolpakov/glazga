package kz.pompei.glazga.v2.debug_server.imports;

import kz.pompei.glazga.v2.api.controllers.ImportControllers;
import kz.pompei.glazga.v2.debug_conf.ImportDebugConf;
import kz.pompei.glazga.v2.debug_server.beans.ImportsDebugServerBeans;
import kz.pompei.glazga.v2.pg.beans.ImportDb;
import org.springframework.context.annotation.Import;

@Import({
  ImportsDebugServerBeans.class,
  ImportControllers.class,
  ImportDb.class,
  ImportDebugConf.class,
})
public class ImportsDebugServer {}
