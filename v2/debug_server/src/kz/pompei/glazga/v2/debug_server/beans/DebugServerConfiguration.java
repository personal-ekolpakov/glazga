package kz.pompei.glazga.v2.debug_server.beans;

import kz.pompei.glazga.v2.debug_server.etc.LocalDispatcherServlet;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletPath;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;

@Configuration
public class DebugServerConfiguration {

  @Bean
  public ServletRegistrationBean<DispatcherServlet> dispatcherServlet() {
    LocalDispatcherServlet servlet = new LocalDispatcherServlet();

    servlet.setContextClass(org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext.class);
    return new ServletRegistrationBean<>(servlet, "/");
  }

  @Bean
  public DispatcherServletPath dispatcherServletPath() {
    return () -> "/";
  }


}
