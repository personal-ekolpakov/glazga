package kz.pompei.glazga.v2.debug_server.etc;

import jakarta.servlet.DispatcherType;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.util.WebUtils;

@Slf4j
public class LocalDispatcherServlet extends DispatcherServlet {


  private static String getRequestUri(HttpServletRequest request) {
    String uri = (String) request.getAttribute(WebUtils.INCLUDE_REQUEST_URI_ATTRIBUTE);
    if (uri == null) {
      uri = request.getRequestURI();
    }
    return uri;
  }

  protected boolean traceOn() {
    return true;
  }

  @Override
  protected void doService(@NonNull HttpServletRequest request,
                           @NonNull HttpServletResponse response) throws Exception {

    if (log.isInfoEnabled()) {
      log.info("HaM4Fy9yEl :: " + requestLogMessage(request));
    }
    super.doService(request, response);
  }

  @SneakyThrows
  private String requestLogMessage(HttpServletRequest request) {
    String params;
    String contentType = request.getContentType();

    Object handler = getHandler(request).getHandler();
    if (handler instanceof HandlerMethod) {

    }

    if (StringUtils.startsWithIgnoreCase(contentType, "multipart/")) {
      params = "multipart";
    } else if (isEnableLoggingRequestDetails()) {
      params = request.getParameterMap().entrySet().stream()
                      .map(entry -> entry.getKey() + ":" + Arrays.toString(entry.getValue()))
                      .collect(Collectors.joining(", "));
    } else {
      // Avoid request body parsing for form data
      params = (StringUtils.startsWithIgnoreCase(contentType, MediaType.APPLICATION_FORM_URLENCODED_VALUE) ||
        !request.getParameterMap().isEmpty() ? "masked" : "");
    }

    String queryString = request.getQueryString();
    String queryClause = (StringUtils.hasLength(queryString) ? "?" + queryString : "");
    String dispatchType = (!DispatcherType.REQUEST.equals(request.getDispatcherType()) ?
      "\"" + request.getDispatcherType() + "\" dispatch for " : "");
    String message = (dispatchType + request.getMethod() + " \"" + getRequestUri(request) +
      queryClause + "\", parameters={" + params + "}");

    if (!traceOn()) {
      return message;
    }

    {
      List<String> values = Collections.list(request.getHeaderNames());
      String       headers;
      if (isEnableLoggingRequestDetails()) {
        headers = values.stream().map(name -> name + ":" + Collections.list(request.getHeaders(name)))
                        .collect(Collectors.joining(", "));
      } else {
        headers = (!values.isEmpty() ? "masked" : "");
      }
      return message + ", headers={" + headers + "} in " + getClass().getSimpleName() + " `" + getServletName() + "`";
    }
  }

}
