package kz.pompei.glazga.v2.debug_server.launcher;


import jakarta.annotation.PostConstruct;
import kz.pompei.glazga.v2.debug_server.imports.ImportsDebugServer;
import kz.pompei.glazga.v2.pg.beans.LiquibaseManager;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication(exclude = {
  //DispatcherServletAutoConfiguration.class,
})
@Import(ImportsDebugServer.class)
public class DebugServer {

  public static void main(String[] args) {
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();
    LoggerFactory.getLogger(DebugServer.class).info("keYu5GXwWh :: START APPLICATION Glazga");
    SpringApplication.run(DebugServer.class, args);
  }

  @Autowired
  private LiquibaseManager liquibaseManager;


  @PostConstruct
  public void initApplication() {
    liquibaseManager.update();
  }

}
