package kz.pompei.glazga.v2.pg.etc;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.utils.db.SqlWithParams;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SqlWithParamsTest {

  @Test
  public void of__001() {

    String              sql    = "tst {var1} stone {var2} mount {var1} status {var3} found";
    Map<String, Object> params = new HashMap<>();
    params.put("var1", "Simon");
    params.put("var2", 234567L);
    params.put("var3", "Main");

    //
    //
    SqlWithParams swp = SqlWithParams.of(sql, params);
    //
    //

    assertThat(swp.sql).isEqualTo("tst ? stone ? mount ? status ? found");
    assertThat(swp.paramList.get(0)).isEqualTo("Simon");
    assertThat(swp.paramList.get(1)).isEqualTo(234567L);
    assertThat(swp.paramList.get(2)).isEqualTo("Simon");
    assertThat(swp.paramList.get(3)).isEqualTo("Main");

  }

  @Test
  public void of__002() {

    String              sql    = "tst {var1} stone {var2} mount {.var1} status {var3} found";
    Map<String, Object> params = new HashMap<>();
    params.put("var1", "Simon");
    params.put("var2", 234567L);
    params.put("var3", "Main");

    //
    //
    SqlWithParams swp = SqlWithParams.of(sql, params);
    //
    //

    assertThat(swp.sql).isEqualTo("tst ? stone ? mount Simon status ? found");
    assertThat(swp.paramList.get(0)).isEqualTo("Simon");
    assertThat(swp.paramList.get(1)).isEqualTo(234567L);
    assertThat(swp.paramList.get(2)).isEqualTo("Main");

  }
}
