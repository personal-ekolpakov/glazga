package kz.pompei.glazga.v2.pg.beans;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import jakarta.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;
import kz.pompei.glazga.utils.db.ConnectionCreator;
import kz.pompei.glazga.utils.db.Db;
import kz.pompei.glazga.v2.pg.etc.DbConf;
import kz.pompei.glazga.v2.pg.etc.DbHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class DbFactory {

  @Autowired
  private DbConf dbConf;

  private Db                db;
  private ConnectionCreator connectionCreator;

  @Bean
  public Db db() {
    return db;
  }

  @Bean
  public ConnectionCreator getConnectionCreator() {
    return connectionCreator;
  }

  @PostConstruct
  private void init() {
    connectionCreator = new ConnectionCreator() {

      private final AtomicReference<HikariDataSource> dataSource = new AtomicReference<>(null);

      @Override
      public Connection create() throws SQLException {
        return getHikariDataSource().getConnection();
      }

      private HikariDataSource getHikariDataSource() {
        {
          HikariDataSource x = dataSource.get();
          if (x != null) {
            return x;
          }
        }
        synchronized (dataSource) {

          {
            HikariDataSource x = dataSource.get();
            if (x != null) {
              return x;
            }
          }

          HikariDataSource x = createHikariDataSource();
          dataSource.set(x);
          return x;
        }
      }

      private HikariDataSource createHikariDataSource() {
        HikariConfig config = new HikariConfig();

        String url = DbHelper.urlFromConf(dbConf);

        config.setJdbcUrl(url);
        config.setUsername(dbConf.username());
        config.setPassword(dbConf.password());

        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

        return new HikariDataSource(config);
      }
    };

    db = new Db(connectionCreator);
  }

}
