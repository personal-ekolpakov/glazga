package kz.pompei.glazga.v2.pg.etc;

public interface DbConf {

  String host();

  int port();

  String database();

  String username();

  String password();

}
