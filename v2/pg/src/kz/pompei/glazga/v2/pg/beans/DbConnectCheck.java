package kz.pompei.glazga.v2.pg.beans;

import java.sql.Connection;
import java.sql.SQLException;
import kz.pompei.glazga.v2.pg.etc.DbConf;
import kz.pompei.glazga.v2.pg.etc.DirectConnectionCreator;
import lombok.SneakyThrows;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DbConnectCheck {

  @Autowired
  private DbConf dbConf;

  @SneakyThrows
  public void waitForDbConnect() {
    DirectConnectionCreator creator = new DirectConnectionCreator(dbConf);


    while (true) try (Connection ignore = creator.create()) {
      return;
    } catch (SQLException e) {

      if (e instanceof PSQLException err) {
        String sqlState = err.getSQLState();

        if (false
          || "08001".equals(sqlState)
          || "57P03".equals(sqlState)) {

          Thread.sleep(300);
          continue;
        }

        System.out.println("Yb88xO1SGU :: *************************************************************");
        System.out.println("Yb88xO1SGU :: *************************************************************");
        System.out.println("Yb88xO1SGU :: *************************************************************");
        System.out.println("Yb88xO1SGU :: *************************************************************");
        System.out.println("ji9NpDcFLl :: sqlState = " + sqlState);
        System.out.println("Yb88xO1SGU :: *************************************************************");
        System.out.println("Yb88xO1SGU :: *************************************************************");
        System.out.println("Yb88xO1SGU :: *************************************************************");
        System.out.println("Yb88xO1SGU :: *************************************************************");
      }

      throw new RuntimeException(e);
    }
  }
}
