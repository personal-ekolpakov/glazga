package kz.pompei.glazga.v2.pg.etc;

import java.util.Arrays;

import static java.util.stream.Collectors.joining;

public class DbHelper {

  public static String urlFromConf(DbConf conf) {
    String hostPort = Arrays.stream(conf.host().split(","))
                            .map(String::trim)
                            .map(s -> s + ":" + conf.port())
                            .collect(joining(","));

    return "jdbc:postgresql://" + hostPort + "/" + conf.database();
  }

}
