package kz.pompei.glazga.v2.pg.beans;

import java.sql.Connection;
import kz.pompei.glazga.v2.pg.etc.DbConf;
import kz.pompei.glazga.v2.pg.etc.DirectConnectionCreator;
import liquibase.command.CommandScope;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LiquibaseManager {

  @Autowired
  private DbConf dbConf;

  @SneakyThrows
  public void update() {
    DirectConnectionCreator creator = new DirectConnectionCreator(dbConf);

    try (Connection connection = creator.create();
         JdbcConnection jdbcConnection = new JdbcConnection(connection);
         Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection)) {

      CommandScope updateScope = new CommandScope("update");
      updateScope.addArgumentValue("changeLogFile", "liquibase/changelog.xml");
      updateScope.addArgumentValue("database", database);

      updateScope.execute();

    }
  }

  public void probe() {
    System.out.println("PeyHpC4JmW :: [OK] Probe " + getClass().getSimpleName());
  }

}
