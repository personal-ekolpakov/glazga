package kz.pompei.glazga.v2.pg.etc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import kz.pompei.glazga.utils.db.ConnectionCreator;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DirectConnectionCreator implements ConnectionCreator {

  private final DbConf conf;

  @Override
  public Connection create() throws SQLException {
    String url = DbHelper.urlFromConf(conf);

    try {
      Class.forName("org.postgresql.Driver");
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }

    return DriverManager.getConnection(url, conf.username(), conf.password());
  }

}
