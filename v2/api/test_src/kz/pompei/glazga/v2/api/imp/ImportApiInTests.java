package kz.pompei.glazga.v2.api.imp;

import kz.pompei.glazga.v2.api.controllers.ImportControllers;
import kz.pompei.glazga.v2.debug_conf.ImportDebugConf;
import kz.pompei.glazga.v2.pg.beans.ImportDb;
import org.springframework.context.annotation.Import;

@Import({
  ImportControllers.class,
  ImportDb.class,
  ImportDebugConf.class,
})
public class ImportApiInTests {}
