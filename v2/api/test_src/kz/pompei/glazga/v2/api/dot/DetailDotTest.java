package kz.pompei.glazga.v2.api.dot;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.v2.api.util.ClassScanner;
import kz.pompei.glazga.v2.pg.unit_model.Billet;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DetailDotTest {

  @DataProvider
  public Object[][] dotPairDataProvider() {

    String dotSuffix = "Dot";


    List<Class<?>> dotClasses  = ClassScanner.findClasses(BilletDot.class.getPackageName(), BilletDot.class);
    List<Class<?>> dataClasses = ClassScanner.findClasses(Billet.class.getPackageName(), Billet.class);


    Map<String, Class<?>> dotClassMap = new HashMap<>();
    for (final Class<?> dotClass : dotClasses) {
      dotClassMap.put(dotClass.getSimpleName(), dotClass);
    }

    Set<Class<?>> restDotClassNames = new HashSet<>(dotClassMap.values());

    List<Object[]> list = new ArrayList<>();

    for (final Class<?> dataClass : dataClasses) {
      if (Modifier.isAbstract(dataClass.getModifiers())) {
        continue;
      }

      Class<?> dotClass = dotClassMap.get(dataClass.getSimpleName() + dotSuffix);

      if (dotClass == null) {
        list.add(new Object[]{dataClass, null});
        continue;
      }

      restDotClassNames.remove(dotClass);

      list.add(new Object[]{dataClass, dotClass});

    }

    for (final Class<?> restDotClassName : restDotClassNames) {
      if (Modifier.isAbstract(restDotClassName.getModifiers())) {
        continue;
      }
      list.add(new Object[]{null, restDotClassName});
    }

    for (final Object[] objects : list) {
      System.out.println("g3WvXuKD1P :: " + Arrays.toString(objects));
    }

    return list.toArray(Object[][]::new);

  }

  @Test(dataProvider = "dotPairDataProvider")
  public void coverWithDot__reflect(Class<?> dataClass, Class<?> dotClass) throws Exception {

    if (dataClass == null && dotClass == null) {
      throw new RuntimeException();
    }

    if (dataClass == null) {
      throw new RuntimeException("KQIh1NFnXT :: Absent data class fot dot class: " + dotClass);
    }

    if (dotClass == null) {
      throw new RuntimeException("hrCwJNjg8L :: Absent dot class for data class: " + dataClass);
    }

    DetailDot detailDot = new DetailDot();

    Object dataInstance = dataClass.getConstructor().newInstance();

    //
    //
    Object dotInstance = detailDot.coverWithDot("testId", (Billet) dataInstance);
    //
    //

    assertThat(dotInstance).isNotNull();

    assertThat(dotInstance).isInstanceOf(dotClass);

    {
      Field id = dotClass.getField("id");

      assertThat(id.getType()).isEqualTo(String.class);

      assertThat(id.get(dotInstance)).isEqualTo("testId");
    }

    {
      Field data = dotClass.getField("data");

      assertThat(data.getType()).isEqualTo(dataClass);

      assertThat(data.get(dotInstance)).isSameAs(dataInstance);
    }

    {
      Field owner = dotClass.getField("owner");

      assertThat(owner.getType()).isEqualTo(DetailDot.class);

      assertThat(owner.get(dotInstance)).isSameAs(detailDot);
    }

  }

}
