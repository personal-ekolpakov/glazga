package kz.pompei.glazga.v2.api.reg_impl;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.moc.MapObjectConverter;
import kz.pompei.glazga.moc.MocUtils;
import kz.pompei.glazga.moc.OneLevel;
import kz.pompei.glazga.moc.TypeRef;
import kz.pompei.glazga.v2.api.imp.ImportApiInTests;
import kz.pompei.glazga.v2.api.reg.DetailRegister;
import kz.pompei.glazga.v2.pg.unit_model.Billet;
import kz.pompei.glazga.v2.pg.unit_model.Detail;
import kz.pompei.glazga.v2.pg.unit_model.block.BlockAssign;
import kz.pompei.glazga.v2.pg.unit_model.block.BlockEntryPoint;
import kz.pompei.glazga.v2.pg.unit_model.block.BlockNewVar;
import kz.pompei.glazga.v2.pg.unit_model.block.BlockReturn;
import kz.pompei.glazga.v2.pg.unit_model.expr.ExprAct;
import kz.pompei.glazga.v2.pg.unit_model.expr.ExprConst;
import kz.pompei.glazga.v2.pg.unit_model.expr.ExprImportRef;
import kz.pompei.glazga.v2.pg.unit_model.expr.ExprOp;
import kz.pompei.glazga.v2.pg.unit_model.expr.ExprTop;
import kz.pompei.glazga.v2.pg.unit_model.expr.ExprVarRef;
import kz.pompei.glazga.v2.pg.unit_model.expr.TopType;
import kz.pompei.glazga.v2.pg.unit_model.import1.ImportDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@ContextConfiguration(classes = ImportApiInTests.class)
public class DetailRegisterImplTest extends AbstractTestNGSpringContextTests {

  @Autowired
  private DetailRegister detailRegister;

  @Test(enabled = false)
  public void updateBillet__loadBillets() {

    Detail d = new Detail();

    //
    // Imports
    //

    d.expr("MY_MODULE_001", ExprTop.of(TopType.MY_MODULE));
    d.expr("int_001", ExprAct.of("MY_MODULE_001", "IntTypeId_4E6uNcSSgC"));
    d.importDetail("INT_TYPE", ImportDetail.of("Целое", "int_001"));

    d.expr("MY_MODULE_002", ExprTop.of(TopType.MY_MODULE));
    d.expr("out_001", ExprAct.of("MY_MODULE_002", "Out_Ref_iHawkJ7VuX"));
    d.importDetail("OUT_STD", ImportDetail.of("Стандартный вывод", "out_001"));

    //
    // Entry Point
    //

    d.block("entryPoint", BlockEntryPoint.of(13.45, 10)).act("Prints_hi_world", "Печатает 'Привет мир'").down("varMessage");


    d.expr("refMessage", ExprConst.of("Привет мир"));
    d.block("varMessage", BlockNewVar.of("Сообщение", "refMessage")).down("callPrintToOut");

    d.expr("ref_OUT", ExprImportRef.of("OUT_STD"));
    d.expr("ref_varMessage", ExprVarRef.of("varMessage"));
    d.expr("act_print", ExprAct.of("ref_OUT", "print_ln").arg("data", "ref_varMessage"));
    d.block("callPrintToOut", BlockAssign.left("act_print").down("return"));


    d.block("return", BlockReturn.ofVoid());

    //
    // END Entry Point
    //

    d.expr("test_test_test", ExprOp.of("plus", "leftId", "rightId")
                                   .part("p1", "plus", "partExprId1")
                                   .part("p2", "plus", "partExprId1"));

    System.out.println("WP8XbCbZIM :: " + d);

    MapObjectConverter conv = new MapObjectConverter();
    conv.convertClasses_add(Vec.class);

    Billet billet = d.billets.get("entryPoint");

    Map<String, Object> map = conv.convertToMap(d);

    OneLevel oneLevel = new OneLevel();

    oneLevel.append(map);

    detailRegister.updateDetail("ovQ0sabRzR", oneLevel.target);

    Map<String, String> loadedOneLevel = detailRegister.loadBillets("ovQ0sabRzR");

    System.out.println("J3nCmm9bSe :: IN DEEP " + MocUtils.toPrettyStr(map));
    System.out.println("aMpTeZV09s :: IN ONE_LEVEL " + MocUtils.toPrettyStr(loadedOneLevel));

    Map<String, Object> deepMap = new HashMap<>();

    loadedOneLevel.forEach((key, value) -> OneLevel.set(deepMap, key, value));

    System.out.println("NGZ4TOa1up :: SECOND " + MocUtils.toPrettyStr(deepMap));

    Map<String, Billet> actual = conv.convertFromMap(deepMap, new TypeRef<Map<String, Billet>>() {}.type);

    System.out.println("l0fqGwDY8k :: actual = " + actual);
    System.out.println("ZbxJK7sUsa :: actual.entryPoint = " + actual.get("entryPoint"));

  }

}
