package kz.pompei.glazga.v2.api.reg_impl;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class ImportRegisters {}
