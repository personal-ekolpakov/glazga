package kz.pompei.glazga.v2.api.util;

import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.SimpleMetadataReaderFactory;

public class ClassScanner {

  @SneakyThrows
  public static List<Class<?>> findClasses(String packageName, Class<?> parentClass) {

    List<Class<?>> ret                   = new ArrayList<>();
    String         packageSearchPath     = "classpath*:" + packageName.replace('.', '/') + "/**/*.class";
    var            resolver              = new PathMatchingResourcePatternResolver();
    var            metadataReaderFactory = new SimpleMetadataReaderFactory();
    Resource[]     resources             = resolver.getResources(packageSearchPath);

    for (Resource resource : resources) {
      MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);
      String         className      = metadataReader.getClassMetadata().getClassName();
      Class<?>       aClass         = Class.forName(className);

      if (parentClass.isAssignableFrom(aClass)) {
        ret.add(aClass);
      }
    }

    return ret;

  }

}
