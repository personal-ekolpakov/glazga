package kz.pompei.glazga.v2.api.dot.expr;

import kz.pompei.glazga.v2.api.dot.DetailDot;
import kz.pompei.glazga.v2.pg.unit_model.expr.ExprImportRef;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ExprImportRefDot extends ExprDot {
  public final @NonNull String        id;
  public final @NonNull ExprImportRef data;
  public final @NonNull DetailDot     owner;
}
