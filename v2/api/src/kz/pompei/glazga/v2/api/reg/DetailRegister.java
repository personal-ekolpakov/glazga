package kz.pompei.glazga.v2.api.reg;

import java.util.Map;
import lombok.NonNull;

public interface DetailRegister {

  void updateDetail(@NonNull String detailId, @NonNull Map<String, String> keyPathValuePairs);

  @NonNull Map<String, String> loadBillets(@NonNull String detailId);

}
