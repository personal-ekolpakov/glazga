package kz.pompei.glazga.v2.api.reg;

import java.sql.Timestamp;
import java.util.Date;

public interface NowSource {

  Date now();

  Timestamp timestamp();

}
