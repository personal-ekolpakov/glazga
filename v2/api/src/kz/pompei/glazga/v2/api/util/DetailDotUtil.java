package kz.pompei.glazga.v2.api.util;

import java.beans.ConstructorProperties;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import kz.pompei.glazga.v2.api.dot.BilletDot;
import kz.pompei.glazga.v2.api.dot.DetailDot;
import kz.pompei.glazga.v2.pg.unit_model.Billet;
import lombok.NonNull;
import lombok.SneakyThrows;

public class DetailDotUtil {

  private static final Map<Class<?>, Class<?>> dataDotMap = new HashMap<>();

  static {

    List<Class<?>> dataClasses = ClassScanner.findClasses(Billet.class.getPackageName(), Billet.class);
    List<Class<?>> dotClasses  = ClassScanner.findClasses(BilletDot.class.getPackageName(), BilletDot.class);

    Map<String, Class<?>> dotClassMap = new HashMap<>();
    for (final Class<?> dotClass : dotClasses) {
      dotClassMap.put(dotClass.getSimpleName(), dotClass);
    }

    for (final Class<?> dataClass : dataClasses) {

      Class<?> dotClass = dotClassMap.get(dataClass.getSimpleName() + "Dot");

      if (dotClass == null) {
        throw new RuntimeException("UInzObj7sC :: No dot class for data class: " + dataClass);
      }

      dataDotMap.put(dataClass, dotClass);
    }
  }

  private interface DotCreator {
    <T> T create(@NonNull String billetId, @NonNull Billet billet, @NonNull DetailDot owner);
  }

  private static final Map<Class<?>, DotCreator> DotCreatorMap = new ConcurrentHashMap<>();

  @SneakyThrows
  public static <Dot extends BilletDot> Dot coverWithDot(@NonNull String billetId, @NonNull Billet billet, @NonNull DetailDot owner) {

    Class<?> dataClass = billet.getClass();
    Class<?> dotClass  = dataDotMap.get(dataClass);
    if (dotClass == null) {
      throw new RuntimeException("vnZZSJojS4 :: No dot class for data class: " + dataClass);
    }

    {
      DotCreator dotCreator = DotCreatorMap.get(dotClass);
      if (dotCreator != null) {
        return dotCreator.create(billetId, billet, owner);
      }
    }

    synchronized (DotCreatorMap) {
      {
        DotCreator dotCreator = DotCreatorMap.get(dotClass);
        if (dotCreator != null) {
          return dotCreator.create(billetId, billet, owner);
        }
      }

      {
        DotCreator dotCreator = creaetDotCreator(dotClass);
        DotCreatorMap.put(dotClass, dotCreator);
        return dotCreator.create(billetId, billet, owner);
      }
    }

  }

  private static @NonNull DotCreator creaetDotCreator(Class<?> dotClass) {
    Map<String, Integer> positions   = null;
    Constructor<?>       constructor = null;

    for (final Constructor<?> c : dotClass.getConstructors()) {
      ConstructorProperties cp = c.getAnnotation(ConstructorProperties.class);
      if (cp != null) {

        positions = new HashMap<>();
        int index = 0;
        for (final String fieldName : cp.value()) {
          positions.put(fieldName, index++);
        }

        if (positions.keySet().equals(Set.of("id", "data", "owner"))) {
          constructor = c;
          break;
        }

      }
    }

    if (constructor == null) {
      throw new RuntimeException("1wZ1pbC1XW :: Not found constructor with `id`, `data`, `owner`");
    }

    Map<String, Integer> finalPositions   = positions;
    Constructor<?>       finalConstructor = constructor;

    return new DotCreator() {


      @Override
      @SneakyThrows
      public <T> T create(@NonNull String billetId, @NonNull Billet billet, @NonNull DetailDot owner) {
        Object[] params = new Object[3];
        params[finalPositions.get("id")]    = billetId;
        params[finalPositions.get("data")]  = billet;
        params[finalPositions.get("owner")] = owner;

        //noinspection unchecked
        return (T) finalConstructor.newInstance(params);
      }
    };


  }

}
