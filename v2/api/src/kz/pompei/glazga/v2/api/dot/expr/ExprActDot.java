package kz.pompei.glazga.v2.api.dot.expr;

import kz.pompei.glazga.v2.api.dot.DetailDot;
import kz.pompei.glazga.v2.pg.unit_model.expr.ExprAct;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ExprActDot extends ExprDot {
  public final @NonNull String    id;
  public final @NonNull ExprAct   data;
  public final @NonNull DetailDot owner;
}
