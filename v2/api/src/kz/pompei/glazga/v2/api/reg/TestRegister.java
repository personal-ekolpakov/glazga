package kz.pompei.glazga.v2.api.reg;

import java.util.List;
import kz.pompei.glazga.v2.api.model.TestRecord;

public interface TestRegister {

  String get();

  List<TestRecord> list();

}
