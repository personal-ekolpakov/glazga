package kz.pompei.glazga.v2.api.reg_impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kz.pompei.glazga.utils.db.Db;
import kz.pompei.glazga.utils.db.UpdateBatched;
import kz.pompei.glazga.v2.api.model.BilletValue;
import kz.pompei.glazga.v2.api.reg.DetailRegister;
import kz.pompei.glazga.v2.api.reg.NowSource;
import kz.pompei.glazga.v2.pg.unit_model.Billet;
import kz.pompei.glazga.v2.pg.unit_model.Detail;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.util.stream.Collectors.toMap;

@Component
public class DetailRegisterImpl implements DetailRegister {

  @Autowired
  private Db db;

  @Autowired
  private NowSource nowSource;

  private static final Pattern LEFT_TOP = Pattern.compile(Detail.Fields.billets + "\\.(\\w+)\\." + Billet.Fields.left_top);

  private static final Pattern BILLET_KEY = Pattern.compile(Detail.Fields.billets + "\\.(\\w+)\\.(.+)");

  @Override
  public void updateDetail(@NonNull String detailId, @NonNull Map<String, String> keyPathValuePairs) {

    List<BilletValue> billetValueList = new ArrayList<>();

    for (final Map.Entry<String, String> e : keyPathValuePairs.entrySet()) {
      String key_path  = e.getKey();
      String value_str = e.getValue();
      {
        Matcher matcher = BILLET_KEY.matcher(key_path);
        if (matcher.matches()) {

          String billet_id   = matcher.group(1);
          String billet_path = matcher.group(2);

          billetValueList.add(new BilletValue(billet_id, billet_path, value_str));
          continue;
        }
      }

      throw new RuntimeException("mqjXpoI6fz :: Cannot update keyPath " + key_path);
    }

    db.update(qs -> {

      //noinspection resource
      UpdateBatched billet = qs.query()
                               .sql("""
                                      insert into billet ( detail_id ,  billet_id,   key_path ,  value_str , modified_at)
                                                  values ({detail_id}, {billet_id}, {key_path}, {value_str}, {now}      )
                                      on conflict (detail_id, billet_id, key_path)
                                      do update set value_str   = {value_str}
                                                  , modified_at = {now}
                                      """)
                               .updateBatched();

      //noinspection resource
      UpdateBatched delete = qs.query()
                               .sql("""
                                      delete from billet
                                      where detail_id = {detail_id}
                                      and   billet_id = {billet_id}
                                      and    key_path = {key_path}
                                      """)
                               .updateBatched();

      //noinspection resource
      UpdateBatched left_top = qs.query()
                                 .sql("""
                                        insert into billet_pos ( detail_id ,  billet_id,  left_top                          )
                                        values                 ({detail_id}, {billet_id}, ST_GeomFromText({left_top}, 4326) )
                                        on conflict (detail_id, billet_id)
                                        do update set left_top = ST_GeomFromText({left_top}, 4326)
                                        """)
                                 .updateBatched();

      //noinspection resource
      UpdateBatched left_top_del = qs.query()
                                     .sql("""
                                            delete from billet_pos
                                            where detail_id = {detail_id}
                                            and   billet_id = {billet_id}
                                            """)
                                     .updateBatched();

      left_top.param("detail_id", detailId);
      left_top_del.param("detail_id", detailId);
      delete.param("detail_id", detailId);
      billet.param("detail_id", detailId);

      billet.param("now", nowSource.timestamp());

      for (final BilletValue x : billetValueList) {

        if (x.value_str == null) {
          delete.param("billet_id", x.billet_id)
                .param("key_path", x.key_path)
                .addBatch();
        } else {
          billet.param("billet_id", x.billet_id)
                .param("key_path", x.key_path)
                .param("value_str", x.value_str)
                .addBatch();
        }

        if (Billet.Fields.left_top.equals(x.key_path)) {

          if (x.value_str == null) {
            left_top_del.param("billet_id", x.billet_id)
                        .addBatch();
          } else {
            left_top.param("billet_id", x.billet_id)
                    .param("left_top", "POINT (" + x.value_str.replace(';', ' ') + ")")
                    .addBatch();
          }

        }

      }

      delete.executeBatch();
      billet.executeBatch();
      left_top.executeBatch();
      left_top_del.executeBatch();

      billet.commit();

    });

  }

  @Override
  public @NonNull Map<String, String> loadBillets(@NonNull String detailId) {
    return db.query()
             .sql("select billet_id, key_path, value_str from billet where detail_id = {detail_id}")
             .param("detail_id", detailId)
             .list(BilletValue.class)
             .stream()
             .collect(toMap(BilletValue::billetPath, x -> x.value_str));
  }

}
