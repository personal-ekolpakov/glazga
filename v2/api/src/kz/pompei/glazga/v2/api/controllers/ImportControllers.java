package kz.pompei.glazga.v2.api.controllers;

import kz.pompei.glazga.v2.api.reg_impl.ImportRegisters;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@ComponentScan
@Import(ImportRegisters.class)
public class ImportControllers {}
