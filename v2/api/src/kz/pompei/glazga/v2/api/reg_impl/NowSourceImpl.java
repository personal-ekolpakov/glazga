package kz.pompei.glazga.v2.api.reg_impl;

import java.sql.Timestamp;
import java.util.Date;
import kz.pompei.glazga.v2.api.reg.NowSource;
import org.springframework.stereotype.Component;

@Component
public class NowSourceImpl implements NowSource {

  @Override
  public Date now() {
    return new Date();
  }

  @Override
  public Timestamp timestamp() {
    return new Timestamp(System.currentTimeMillis());
  }

}
