package kz.pompei.glazga.v2.api.dot;

import kz.pompei.glazga.v2.api.util.DetailDotUtil;
import kz.pompei.glazga.v2.pg.unit_model.Billet;
import lombok.NonNull;

public class DetailDot {

  public <Dot extends BilletDot> Dot coverWithDot(@NonNull String billetId, @NonNull Billet billet) {
    return DetailDotUtil.coverWithDot(billetId, billet, this);
  }

}
