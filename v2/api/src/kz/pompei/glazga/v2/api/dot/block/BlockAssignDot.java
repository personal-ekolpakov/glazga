package kz.pompei.glazga.v2.api.dot.block;

import kz.pompei.glazga.v2.api.dot.DetailDot;
import kz.pompei.glazga.v2.pg.unit_model.block.BlockAssign;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BlockAssignDot extends BlockDot {
  public final @NonNull String      id;
  public final @NonNull BlockAssign data;
  public final @NonNull DetailDot   owner;
}
