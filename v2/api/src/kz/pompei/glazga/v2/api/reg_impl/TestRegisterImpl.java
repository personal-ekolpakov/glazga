package kz.pompei.glazga.v2.api.reg_impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import kz.pompei.glazga.utils.db.Db;
import kz.pompei.glazga.v2.api.model.TestRecord;
import kz.pompei.glazga.v2.api.reg.TestRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestRegisterImpl implements TestRegister {

  @Autowired
  private Db db;

  @Override
  public String get() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    return "SUtJS7V3nZ :: test get from register: now = " + sdf.format(new Date());
  }

  @Override
  public List<TestRecord> list() {
    return db.query()
             .sql("""
                    select id, name, value1, value2, value1 + value2 as value_sum
                    from test_list
                    where {min} <= value1 and value1 <= {max}
                    and   {min} <= value2 and value2 <= {max}
                    """)
             .param("min", 1_000)
             .param("max", 10_000)
             .list(TestRecord.class);
  }

}
