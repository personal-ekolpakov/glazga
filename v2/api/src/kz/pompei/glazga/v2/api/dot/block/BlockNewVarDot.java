package kz.pompei.glazga.v2.api.dot.block;

import kz.pompei.glazga.v2.api.dot.DetailDot;
import kz.pompei.glazga.v2.pg.unit_model.block.BlockNewVar;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BlockNewVarDot extends BlockDot {
  public final @NonNull String      id;
  public final @NonNull BlockNewVar data;
  public final @NonNull DetailDot   owner;
}
