package kz.pompei.glazga.v2.api.controllers;


import java.util.List;
import kz.pompei.glazga.v2.api.model.TestRecord;
import kz.pompei.glazga.v2.api.reg.TestRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

  @Autowired
  private TestRegister testRegister;

  @GetMapping("/get")
  public String get() {
    return testRegister.get();
  }

  @GetMapping("/list")
  public List<TestRecord> list() {
    return testRegister.list();
  }

}
