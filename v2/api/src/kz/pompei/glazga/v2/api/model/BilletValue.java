package kz.pompei.glazga.v2.api.model;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BilletValue {
  public final String billet_id;
  public final String key_path;
  public final String value_str;

  public String billetPath() {
    return billet_id + '.' + key_path;
  }
}
