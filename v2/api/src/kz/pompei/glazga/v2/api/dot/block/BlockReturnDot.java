package kz.pompei.glazga.v2.api.dot.block;

import kz.pompei.glazga.v2.api.dot.DetailDot;
import kz.pompei.glazga.v2.pg.unit_model.block.BlockReturn;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BlockReturnDot extends BlockDot {
  public final @NonNull DetailDot   owner;
  public final @NonNull String      id;
  public final @NonNull BlockReturn data;
}
