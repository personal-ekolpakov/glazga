package kz.pompei.glazga.v2.api.dot.import1;

import kz.pompei.glazga.v2.api.dot.BilletDot;
import kz.pompei.glazga.v2.api.dot.DetailDot;
import kz.pompei.glazga.v2.pg.unit_model.import1.ImportDetail;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ImportDetailDot extends BilletDot {
  public final @NonNull String       id;
  public final @NonNull ImportDetail data;
  public final @NonNull DetailDot    owner;
}
