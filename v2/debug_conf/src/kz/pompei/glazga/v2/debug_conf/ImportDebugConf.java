package kz.pompei.glazga.v2.debug_conf;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class ImportDebugConf {}
