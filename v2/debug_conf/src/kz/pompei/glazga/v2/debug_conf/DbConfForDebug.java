package kz.pompei.glazga.v2.debug_conf;

import kz.pompei.glazga.v2.pg.etc.DbConf;
import org.springframework.stereotype.Component;

@Component
public class DbConfForDebug implements DbConf {
  @Override
  public String host() {
    return "localhost";
  }

  @Override
  public int port() {
    return 21111;
  }

  @Override
  public String database() {
    return "glazga";
  }

  @Override
  public String username() {
    return "glazga";
  }

  @Override
  public String password() {
    return "GTHzp0YXgWlQX5olLT0I";
  }
}
