package kz.pompei.glazga.v2.pg.unit_model.block;

import kz.pompei.glazga.v2.pg.unit_model.Billet;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public abstract class Block extends Billet {
  public String downBlockId;
}
