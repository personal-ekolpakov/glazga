package kz.pompei.glazga.v2.pg.unit_model.expr;

import kz.pompei.glazga.v2.pg.unit_model.Billet;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public abstract class Expr extends Billet {
}
