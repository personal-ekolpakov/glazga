package kz.pompei.glazga.v2.pg.unit_model.block;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class BlockReturn extends Block {
  public String returnExprId;

  public static BlockReturn ofVoid() {
    return of(null);
  }

  public static BlockReturn of(String returnExprId) {
    BlockReturn ret = new BlockReturn();
    ret.returnExprId = returnExprId;
    return ret;
  }

  public BlockReturn down(String downBlockId) {
    this.downBlockId = downBlockId;
    return this;
  }

}
