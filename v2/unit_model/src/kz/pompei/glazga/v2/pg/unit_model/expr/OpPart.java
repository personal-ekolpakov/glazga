package kz.pompei.glazga.v2.pg.unit_model.expr;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class OpPart {
  public String opCode;
  public String exprId;
  public double order;
}
