package kz.pompei.glazga.v2.pg.unit_model.block;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class BlockAssign extends Block {
  public String leftExprId;
  public String rightExprId;

  public BlockAssign down(String downBlockId) {
    this.downBlockId = downBlockId;
    return this;
  }

  public static BlockAssign left(String leftExprId) {
    BlockAssign ret = new BlockAssign();
    ret.leftExprId = leftExprId;
    return ret;
  }
}
