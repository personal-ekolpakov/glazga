package kz.pompei.glazga.v2.pg.unit_model.expr;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class ExprVarRef extends Expr {
  public String blockId;

  public static ExprVarRef of(String blockId) {
    ExprVarRef ret = new ExprVarRef();
    ret.blockId = blockId;
    return ret;
  }
}
