package kz.pompei.glazga.v2.pg.unit_model.expr;

import java.util.HashMap;
import java.util.Map;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class ExprAct extends Expr {

  public String              leftExprId;
  public String              actCode;
  public Map<String, String> argExprIds;

  public static ExprAct of(String leftExprId, String actCode) {
    ExprAct ret = new ExprAct();
    ret.leftExprId = leftExprId;
    ret.actCode    = actCode;
    return ret;
  }

  public ExprAct arg(String argId, String exprId) {
    if (argExprIds == null) {
      argExprIds = new HashMap<>();
    }
    argExprIds.put(argId, exprId);
    return this;
  }
}
