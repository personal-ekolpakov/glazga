package kz.pompei.glazga.v2.pg.unit_model;

import kz.pompei.glazga.ann.DiscriminatorClassSimpleName;
import kz.pompei.glazga.mat.Vec;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
@DiscriminatorClassSimpleName("type")
public abstract class Billet {

  public Vec left_top;

  @SuppressWarnings("unused")
  public final String getType() {
    return getClass().getSimpleName();
  }

}
