package kz.pompei.glazga.v2.pg.unit_model.block;

import kz.pompei.glazga.mat.Vec;
import lombok.NonNull;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class BlockEntryPoint extends Block {

  public String newActCode;
  public String displayName;

  public static BlockEntryPoint of(double x, double y) {
    return of(Vec.of(x, y));
  }

  public static BlockEntryPoint of(@NonNull Vec pos) {
    BlockEntryPoint ret = new BlockEntryPoint();
    ret.left_top = pos;
    return ret;
  }

  public BlockEntryPoint down(String downBlockId) {
    this.downBlockId = downBlockId;
    return this;
  }

  public BlockEntryPoint newActCode(String newActCode) {
    this.newActCode = newActCode;
    return this;
  }

  public BlockEntryPoint displayName(String displayName) {
    this.displayName = displayName;
    return this;
  }

  public BlockEntryPoint act(String newActCode, String displayName) {
    this.newActCode  = newActCode;
    this.displayName = displayName;
    return this;
  }
}
