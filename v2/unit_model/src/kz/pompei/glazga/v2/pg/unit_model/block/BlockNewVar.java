package kz.pompei.glazga.v2.pg.unit_model.block;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class BlockNewVar extends Block {
  public String varName;
  public String valueExprId;

  public static BlockNewVar of(String varName, String valueExprId) {
    BlockNewVar ret = new BlockNewVar();
    ret.varName     = varName;
    ret.valueExprId = valueExprId;
    return ret;
  }

  public BlockNewVar down(String downBlockId) {
    this.downBlockId = downBlockId;
    return this;
  }

}
