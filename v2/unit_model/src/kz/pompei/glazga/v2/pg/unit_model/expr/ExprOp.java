package kz.pompei.glazga.v2.pg.unit_model.expr;

import java.util.HashMap;
import java.util.Map;
import lombok.NonNull;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class ExprOp extends Expr {
  public String opCode;
  public String firstExprId;
  public String secondExprId;

  public Map<String, OpPart> parts;

  public static ExprOp of(@NonNull String opCode, String firstExprId, String secondExprId) {
    ExprOp ret = new ExprOp();

    ret.opCode       = opCode;
    ret.firstExprId  = firstExprId;
    ret.secondExprId = secondExprId;

    return ret;
  }

  public ExprOp part(@NonNull String part_id, @NonNull String opCode, String exprId) {

    if (parts == null) {
      parts = new HashMap<>();
    }

    double order = parts.values().stream().mapToDouble(x -> x.order).max().orElse(0) + 1;

    OpPart part = new OpPart();
    part.opCode = opCode;
    part.exprId = exprId;
    part.order  = order;

    parts.put(part_id, part);

    return this;
  }
}
