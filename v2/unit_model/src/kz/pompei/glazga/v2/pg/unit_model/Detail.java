package kz.pompei.glazga.v2.pg.unit_model;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.v2.pg.unit_model.block.Block;
import kz.pompei.glazga.v2.pg.unit_model.expr.Expr;
import kz.pompei.glazga.v2.pg.unit_model.import1.ImportDetail;
import lombok.experimental.FieldNameConstants;

/**
 * Деталь программы. Она может быть: Классом, Интерфейсом, Перечислением
 */
@FieldNameConstants
public class Detail {

  public Map<String, Billet> billets;

  public <T extends Billet> T billet(String id, T billet) {
    if (billets == null) {
      billets = new HashMap<>();
    }
    billets.put(id, billet);
    return billet;
  }

  public <T extends Block> T block(String id, T block) {
    return billet(id, block);
  }

  public <T extends ImportDetail> T importDetail(String id, T importDetail) {
    return billet(id, importDetail);
  }

  public <T extends Expr> T expr(String id, T expr) {
    return billet(id, expr);
  }

}
