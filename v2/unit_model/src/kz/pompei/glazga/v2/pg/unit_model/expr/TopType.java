package kz.pompei.glazga.v2.pg.unit_model.expr;

public enum TopType {
  ROOT,
  MY_MODULE,
  MY_CLASS,
  ME,
}
