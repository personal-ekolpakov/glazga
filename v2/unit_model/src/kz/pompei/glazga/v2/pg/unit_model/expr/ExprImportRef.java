package kz.pompei.glazga.v2.pg.unit_model.expr;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class ExprImportRef extends Expr {
  public String importId;

  public static ExprImportRef of(String importId) {
    ExprImportRef ret = new ExprImportRef();
    ret.importId = importId;
    return ret;
  }
}
