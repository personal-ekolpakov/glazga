package kz.pompei.glazga.v2.pg.unit_model.expr;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class ExprConst extends Expr {
  public String constText;

  public static ExprConst of(String constText) {
    ExprConst ret = new ExprConst();
    ret.constText = constText;
    return ret;
  }
}
