package kz.pompei.glazga.v2.pg.unit_model.import1;

import kz.pompei.glazga.v2.pg.unit_model.Billet;
import lombok.experimental.FieldNameConstants;

/**
 * Импортирует стороннюю деталь, внутрь этой детали
 */
@FieldNameConstants
public class ImportDetail extends Billet {

  /**
   * Наименование импорта
   */
  public String varName;

  /**
   * Идентификатор выражения, которое вычисляет путь к нужной детали
   */
  public String exprId;

  public static ImportDetail of(String varName, String exprId) {
    ImportDetail ret = new ImportDetail();
    ret.varName = varName;
    ret.exprId  = exprId;
    return ret;
  }

}
