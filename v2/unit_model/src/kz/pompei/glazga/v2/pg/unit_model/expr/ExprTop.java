package kz.pompei.glazga.v2.pg.unit_model.expr;

import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class ExprTop extends Expr {
  public TopType topType;

  public static ExprTop of(TopType topType) {
    ExprTop ret = new ExprTop();
    ret.topType = topType;
    return ret;
  }

}
