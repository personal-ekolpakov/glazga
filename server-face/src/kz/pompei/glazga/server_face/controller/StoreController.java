package kz.pompei.glazga.server_face.controller;

import java.util.List;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.server_face.model.eye.etc.EyeData;
import kz.pompei.glazga.server_face.model.eye.etc.UFocusRef;
import kz.pompei.glazga.server_face.model.update.MapUpdate;
import lombok.NonNull;

public interface StoreController {

  /**
   * Загружает сцену
   *
   * @param filePath пусть к файлу скрипта. Расширения нет. Оно подставляется автоматически на сервере
   * @return Сцена
   */
  EyeData loadEyeData(String filePath);

  /**
   * Обновляет сцену по указанному пути, также фиксирует изменения в буфере отмен.
   *
   * @param filePath    путь к сцене
   * @param changeName  имя внесённого изменения
   * @param changeGroup имя группы изменений. Изменения в одной группе отменяются (Ctrl+Z) и возобновляются (Ctrl+Shift+Z) одновременно.
   * @param fw          вносимые изменения
   * @param bk          изменения отменяющие вносимые изменения
   */
  void save(@NonNull String filePath, @NonNull String changeName, String changeGroup, @NonNull List<MapUpdate> fw, @NonNull List<MapUpdate> bk);

  /**
   * Сохраняет преобразования координат, сделанные пользователем для данного файла
   *
   * @param filePath    редактируемый файл скрипта
   * @param changeGroup группа изменений в буфере отмены. Команды в одной группе отменяются все одновременно
   * @param trans       сделанные в этом файле преобразования координат
   */
  void saveTrans(@NonNull String filePath, String changeGroup, @NonNull LinearTrans trans);

  /**
   * Сохраняет фокус у глаза для указанного файла.
   *
   * @param filePath    файл глаза
   * @param changeGroup группа изменений в буфере отмены. Команды в одной группе отменяются все одновременно
   * @param focusRef    ссылка на фокус
   */
  void saveFocusRef(@NonNull String filePath, String changeGroup, @NonNull UFocusRef focusRef);

}
