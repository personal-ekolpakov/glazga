package kz.pompei.glazga.server_face.model.eye.block;

import kz.pompei.glazga.ann.DiscriminatorClassSimpleName;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
@DiscriminatorClassSimpleName("type")
public abstract class Block {
  public String downBlockId;
}

