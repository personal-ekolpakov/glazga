package kz.pompei.glazga.server_face.model.eye.block;

import kz.pompei.glazga.server_face.model.eye.block.exit.ExitKind;

public class BlockExit extends Block {
  public ExitKind kind;
  public String   throwExprId;
  public String   returnExprId;
}
