package kz.pompei.glazga.server_face.model.eye.expr;

/**
 * Вертикальное деление одного выражения на другое.
 */
public class ExprDiv extends Expr {
  /**
   * Ссылка на числитель
   */
  public String upExprId;

  /**
   * Ссылка на знаменатель
   */
  public String downExprId;
}
