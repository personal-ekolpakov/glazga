package kz.pompei.glazga.server_face.model.eye.expr;

import kz.pompei.glazga.common_model.BracketKind;

public class ExprBracket extends Expr {
  public String      exprId;
  public BracketKind kind;
}
