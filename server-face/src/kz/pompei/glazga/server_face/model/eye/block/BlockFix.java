package kz.pompei.glazga.server_face.model.eye.block;

import kz.pompei.glazga.mat.Vec;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public abstract class BlockFix extends Block {
  public double left;
  public double top;

  public Vec leftTop() {
    return Vec.of(left, top);
  }
}
