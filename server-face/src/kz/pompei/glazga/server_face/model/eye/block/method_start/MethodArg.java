package kz.pompei.glazga.server_face.model.eye.block.method_start;

public class MethodArg {
  public String name;
  public int    orderIndex;

  /**
   * Ссылка на выражения значения по-умолчанию
   */
  public String defExprId;
}
