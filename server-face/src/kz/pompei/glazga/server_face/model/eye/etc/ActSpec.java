package kz.pompei.glazga.server_face.model.eye.etc;

import java.util.HashMap;
import java.util.Map;

/**
 * Спецификация акта: его имя; способы вызова; характеристики и документация
 */
public class ActSpec {

  /**
   * Метка акта (представиться человеку)
   */
  public String label;

  /**
   * Спецификация аргументов вызова акта
   * <p>
   * Ключ колоды - идентификатор аргумента, уникальный в рамках данного акта
   * <p>
   * Значение колоды - спецификация аргумента
   */
  public Map<String, ActSpecArg> args = new HashMap<>();

}
