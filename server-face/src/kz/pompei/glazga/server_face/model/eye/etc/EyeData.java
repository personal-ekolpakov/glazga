package kz.pompei.glazga.server_face.model.eye.etc;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.server_face.model.eye.Eye;

/**
 * Данные глаза для отображения
 */
public class EyeData {

  /**
   * Структура глаза
   */
  public Eye eye;

  /**
   * Параметры методов, которые встречаются в сцене
   */
  public Map<String, ActSpec> actSpecifications = new HashMap<>();

  /**
   * Масштабирование отображаемой картины в глазе
   */
  public LinearTrans trans = LinearTrans.one();

  /**
   * Ссылка на сфокусированный элемент в глазе
   */
  public UFocusRef focusRef = null;


}
