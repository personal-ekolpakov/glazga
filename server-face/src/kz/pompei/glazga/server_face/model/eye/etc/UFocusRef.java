package kz.pompei.glazga.server_face.model.eye.etc;

import kz.pompei.glazga.common_model.UType;
import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

/**
 * Ссылка на сфокусированный элемент и состояние фокуса внутри сфокусированного элемента
 */
@ToString
@FieldNameConstants
@AllArgsConstructor(staticName = "of")
public class UFocusRef {

  /**
   * Геометрическое расположение фокуса слева
   */
  public final Double left;

  /**
   * Геометрическое расположение фокуса сверху
   */
  public final Double top;

  /**
   * Тип элемента на котором сосредоточен фокус
   */
  public final UType uType;

  /**
   * Идентификатор блока или выражения на котором сосредоточен фокус пользователя.
   * <p>
   * Это идентификатор блока, если <code>{@link #uType} == {@link UType#BLOCK}</code>
   * <p>
   * Это идентификатор выражения, если <code>{@link #uType} == {@link UType#EXPR}</code>
   */
  public final String elementId;

  /**
   * Определяет имя коннектора (ограды выражения или коннектора блока)
   */
  public final String connectName;

  /**
   * Тип коннектора - это ограда выражения или коннектор блока
   */
  public final ConnectType connectType;

  /**
   * Внутреннее состояние фокуса.
   * <p>
   * Его смысл его содержимого определяется логикой элемента, на который ссылаются поле {@link #elementId}
   */
  public final String state;

  /**
   * Признак того, что выделение находиться на самом глазе в виде крестика
   */
  public boolean selectedEye;

}
