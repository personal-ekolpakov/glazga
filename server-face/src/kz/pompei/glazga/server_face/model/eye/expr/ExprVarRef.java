package kz.pompei.glazga.server_face.model.eye.expr;

import kz.pompei.glazga.server_face.model.eye.expr.var_ref.VarRefType;

/**
 * Ссылка на переменную
 */
public class ExprVarRef extends Expr {

  /**
   * Тип ссылки
   */
  public VarRefType refType;

  /**
   * Идентификатор блока, где объявлена переменная
   * <p>
   * Используется, если <code>{@link #refType} == {@link VarRefType#LOCAL_VAR}</code>
   */
  public String varDefBlockId;

}
