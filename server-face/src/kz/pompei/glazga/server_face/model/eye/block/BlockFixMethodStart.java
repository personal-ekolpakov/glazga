package kz.pompei.glazga.server_face.model.eye.block;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.server_face.model.eye.block.method_start.MethodArg;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class BlockFixMethodStart extends BlockFix {
  public String methodName;
  public String methodId;

  /**
   * Ключ колоды - идентификатор аргумента
   */
  public Map<String, MethodArg> args = new HashMap<>();

  public String methodNameDisplay() {
    String x = methodName;
    return x == null ? "" : x;
  }
}
