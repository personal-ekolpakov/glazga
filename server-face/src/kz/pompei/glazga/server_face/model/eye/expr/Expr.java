package kz.pompei.glazga.server_face.model.eye.expr;

import kz.pompei.glazga.ann.DiscriminatorClassSimpleName;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
@DiscriminatorClassSimpleName("type")
public abstract class Expr {}
