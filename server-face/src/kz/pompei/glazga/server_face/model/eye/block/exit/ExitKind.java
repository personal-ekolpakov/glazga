package kz.pompei.glazga.server_face.model.eye.block.exit;

public enum ExitKind {
  FROM_METHOD,
  FROM_CIRCLE,
  CIRCLE_CONTINUE,
  THROW,
}
