package kz.pompei.glazga.server_face.model.eye.expr;

import lombok.NonNull;

/**
 * Определитель константы.
 */
public class ExprConst extends Expr {
  /**
   * Определитель константы.
   * <p>
   * Для чисел - это текста чисел
   * <p>
   * Для строк - это строка в кавычках
   */
  public String def;

  public @NonNull String defDisplay() {
    var x = def;
    return x == null ? "" : x;
  }
}
