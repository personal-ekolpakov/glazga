package kz.pompei.glazga.server_face.model.eye.expr;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import kz.pompei.glazga.server_face.model.eye.expr.call.CallArg;

/**
 * Вызов метода у выражения слева
 */
public class ExprCall extends Expr {
  /**
   * Ссылка у выражения слева
   */
  public String leftExprId;

  /**
   * Идентификатор метода, доступного типу выражения слева
   */
  public String actId;

  /**
   * Список ссылок выражений для аргументов метода. Для необязательных аргументов список может не содержать ссылку на выражение.
   * <p>
   * Если ссылки для аргумента нет, а аргумент обязательный - это ошибка компиляции
   * <p>
   * Ключ элемента колоды - идентификатор аргумента метода
   * <p>
   * Значение элемента колоды - ссылка на выражение
   */
  public Map<String, CallArg> argExprIds = new HashMap<>();

  public Map<String, CallArg> argExprIds() {
    var x = argExprIds;
    return x != null ? x : (argExprIds = new HashMap<>());
  }

  public Optional<CallArg> callArg(String argId) {
    return Optional.ofNullable(argExprIds().get(argId));
  }
}
