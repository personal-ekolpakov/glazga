package kz.pompei.glazga.server_face.model.eye.expr;

import kz.pompei.glazga.mat.Vec;

/**
 * Это выражение для удержания в "пустоте" другого выражения.
 * <p>
 * У него есть чёткие координаты
 */
public class ExprFix extends Expr {
  public double left;
  public double top;
  public String holdingExprId;

  public Vec leftTop() {
    return Vec.of(left, top);
  }
}
