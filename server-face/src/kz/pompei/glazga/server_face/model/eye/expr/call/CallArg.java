package kz.pompei.glazga.server_face.model.eye.expr.call;

public class CallArg {
  public String exprId;

  public static CallArg withExprId(String exprId) {
    CallArg ret = new CallArg();
    ret.exprId = exprId;
    return ret;
  }
}
