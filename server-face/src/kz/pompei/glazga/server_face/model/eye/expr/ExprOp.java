package kz.pompei.glazga.server_face.model.eye.expr;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.server_face.model.eye.expr.op.Op;
import kz.pompei.glazga.server_face.model.eye.expr.op.OpArg;

public class ExprOp extends Expr {
  public String  firstExprId;
  public boolean useFirst;

  public Map<String, OpArg> args = new HashMap<>();

  public void arg(String argId, Op op, String argExprId) {
    OpArg a = new OpArg();
    a.order     = args.values().stream().mapToDouble(x -> x.order).max().orElse(0) + 1e6;
    a.op        = op;
    a.argExprId = argExprId;
    args.put(argId, a);
  }
}
