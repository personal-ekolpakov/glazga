package kz.pompei.glazga.server_face.model.eye.block;

import java.util.Optional;

/**
 * Создаёт новую переменную
 */
public class BlockNewVar extends Block {
  /**
   * Название новой переменной
   */
  public String varName;

  /**
   * Ссылка на выражение, инициирующее начальное значение данной переменной
   */
  public String valueExprId;

  public String varNameTxt() {
    return Optional.ofNullable(varName).orElse("");
  }
}
