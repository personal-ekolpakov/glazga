package kz.pompei.glazga.server_face.model.eye.expr.var_ref;

/**
 * Тип ссылки на переменную
 */
public enum VarRefType {
  /**
   * Ссылка на локальную переменную
   */
  LOCAL_VAR,

  /**
   * Указатель на этот экземпляр текущего класса
   */
  THIS,
}
