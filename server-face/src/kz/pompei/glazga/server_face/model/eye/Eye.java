package kz.pompei.glazga.server_face.model.eye;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.server_face.model.eye.block.Block;
import kz.pompei.glazga.server_face.model.eye.expr.Expr;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class Eye {

  public Map<String, Block> blocks = new HashMap<>();

  public Map<String, Expr> expressions = new HashMap<>();

}
