package kz.pompei.glazga.server_face.model.eye.expr.op;

public class OpArg {
  public Op     op;
  public String argExprId;
  public double order;
}
