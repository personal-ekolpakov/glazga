package kz.pompei.glazga.server_face.model.update;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Изменение содержимого базы данных по принципу точечной нотации (DOT-notation). Этот принцип взять из СУБД MongoDB.
 */
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class MapUpdate {

  /**
   * Путь к изменяемому параметру
   */
  public final String dotPath;

  /**
   * Новое значение изменяемого параметра. Если указать NULL, то данный параметр удалиться
   */
  public final String simpleValue;

  public static MapUpdate of(String dotPath, String simpleValue) {
    return new MapUpdate(dotPath, simpleValue);
  }
}
