package kz.pompei.glazga.ann;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Ставиться в классе у статического метода, которому на вход приходить строка, а на выходе этот класс.
 * <p>
 * Данный метод должен разбирать строку и возвращать инстанцию, соответствующую этой строке.
 *
 * @see ConvertToStr
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface FromString {}
