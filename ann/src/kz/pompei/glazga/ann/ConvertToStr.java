package kz.pompei.glazga.ann;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Ставиться у метода, который преобразует объект this в строку. Строка такая, что инстанцию этого класса можно восстановить в полном
 * соответствии исходной инстанцией, с помощью статического метода в этом класса, помеченного аннотацией {@link FromString}.
 *
 * @see FromString
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ConvertToStr {}
