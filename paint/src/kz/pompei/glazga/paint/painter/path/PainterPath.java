package kz.pompei.glazga.paint.painter.path;

import java.awt.Graphics2D;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.Painter;
import kz.pompei.glazga.paint.painter.pen_fill.Fill;
import kz.pompei.glazga.paint.painter.pen_fill.Pen;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static java.util.Objects.requireNonNull;

public class PainterPath extends Painter<PainterPath> implements PainterPathStyles {
  private PainterPath() {}

  private final Pen     pen        = new Pen();
  private final Fill    fill       = new Fill();
  private       boolean scaleWidth = true;

  private Vec current = Vec.zero();

  private interface Segment {
    void appendTo(Path2D.Double system);
  }

  @RequiredArgsConstructor
  private class SegmentMoveTo implements Segment {
    final @NonNull Vec pos;

    @Override
    public void appendTo(Path2D.Double system) {
      LinearTrans trans = trans();
      double      x     = trans.convX(pos.x);
      double      y     = trans.convY(pos.y);
      system.moveTo(x, y);
    }
  }

  @RequiredArgsConstructor
  private class SegmentLineTo implements Segment {
    final @NonNull Vec pos;

    @Override
    public void appendTo(Path2D.Double system) {
      LinearTrans trans = trans();
      double      x     = trans.convX(pos.x);
      double      y     = trans.convY(pos.y);
      system.lineTo(x, y);
    }
  }

  @RequiredArgsConstructor
  private class SegmentBezierTo implements Segment {
    final Vec pos2;
    final Vec pos3;
    final Vec pos4;

    @Override
    public void appendTo(Path2D.Double system) {
      LinearTrans trans = trans();
      double      x2    = trans.convX(pos2.x);
      double      y2    = trans.convY(pos2.y);
      double      x3    = trans.convX(pos3.x);
      double      y3    = trans.convY(pos3.y);
      double      x4    = trans.convX(pos4.x);
      double      y4    = trans.convY(pos4.y);
      system.curveTo(x2, y2, x3, y3, x4, y4);
    }
  }

  private static class SegmentClose implements Segment {
    @Override
    public void appendTo(Path2D.Double system) {
      system.closePath();
    }
  }

  final List<Segment> segments = new ArrayList<>();

  public static PainterPath from(Vec startPos) {
    PainterPath ret = PainterPath.of();
    ret.current = startPos;
    ret.moveTo(startPos);
    return ret;
  }

  public static PainterPath from(double left, double top) {
    return from(Vec.of(left, top));
  }

  public static PainterPath withTrans(LinearTrans trans) {
    return PainterPath.of().trans(trans);
  }

  public static PainterPath of() {
    return new PainterPath();
  }

  public PainterPath moveTo(Vec pos) {
    requireNonNull(pos, "2Yzq8Ny78K :: pos == null");
    current = pos;
    segments.add(new SegmentMoveTo(current));
    return this;
  }

  public PainterPath right(double width) {
    return lineDelta(width, 0);
  }

  public PainterPath left(double width) {
    return lineDelta(-width, 0);
  }

  public PainterPath down(double height) {
    return lineDelta(0, height);
  }

  public PainterPath up(double height) {
    return lineDelta(0, -height);
  }

  public PainterPath scaleWidth(boolean scaleWidth) {
    this.scaleWidth = scaleWidth;
    return this;
  }

  public PainterPath moveTo(double left, double top) {
    current = Vec.of(left, top);
    segments.add(new SegmentMoveTo(current));
    return this;
  }

  private static Stream<String> split1(String str) {
    return Arrays.stream(str.split(","));
  }

  static Stream<String> split2(String str) {

    int          index     = -1;
    int          prevIndex = -1;
    boolean      letter    = false;
    List<String> ret       = new ArrayList<>();

    int strLen = str.length();

    while (++index < strLen) {
      char ch = str.charAt(index);
      if (Character.isLetter(ch)) {
        if (!letter) {
          letter = true;
          if (index > prevIndex) {
            ret.add(str.substring(prevIndex + 1, index));
          }
        }
        ret.add("" + ch);
        prevIndex = index;
      } else {
        letter = false;
      }
    }

    if (prevIndex < index - 1) {
      ret.add(str.substring(prevIndex + 1));
    }

    return ret.stream().filter(s -> s.length() > 0);
  }

  public PainterPath svgPath(String strCommands) {

    if (strCommands == null) {
      return this;
    }

    List<String> cmdList = Arrays.stream(strCommands.split("\\s+"))
                                 .flatMap(PainterPath::split1)
                                 .flatMap(PainterPath::split2)
                                 .filter(s -> s.length() > 0)
                                 .toList();

    for (int i = 0; i < cmdList.size(); i++) {
      final String cmd = cmdList.get(i);

      String  cmdLow = cmd.toLowerCase();
      boolean delta  = cmd.equals(cmdLow);

      if ("m".equals(cmdLow)) {
        double arg1 = Double.parseDouble(cmdList.get(++i));
        double arg2 = Double.parseDouble(cmdList.get(++i));
        move(delta, arg1, arg2);
        continue;
      }
      if ("l".equals(cmdLow)) {
        double arg1 = Double.parseDouble(cmdList.get(++i));
        double arg2 = Double.parseDouble(cmdList.get(++i));
        line(delta, arg1, arg2);
        continue;
      }

      if ("c".equals(cmdLow)) {
        double arg1 = Double.parseDouble(cmdList.get(++i));
        double arg2 = Double.parseDouble(cmdList.get(++i));
        double arg3 = Double.parseDouble(cmdList.get(++i));
        double arg4 = Double.parseDouble(cmdList.get(++i));
        double arg5 = Double.parseDouble(cmdList.get(++i));
        double arg6 = Double.parseDouble(cmdList.get(++i));
        bezier(delta, arg1, arg2, arg3, arg4, arg5, arg6);
        continue;
      }

      if ("z".equals(cmdLow)) {
        close();
        continue;
      }

      throw new RuntimeException("0FM02rHQ3L :: Unknown command [" + cmd + "]"
                                   + " in str = \"" + String.join(" ", cmdList.subList(0, i))
                                   + " [" + cmd + "] "
                                   + String.join(" ", cmdList.subList(i + 1, cmdList.size())) + "\"");
    }

    return this;
  }

  public PainterPath moveDelta(Vec delta) {
    requireNonNull(delta, "Ew9WG1OIx6 :: delta == null");
    current = current.plus(delta);
    segments.add(new SegmentMoveTo(current));
    return this;
  }

  public PainterPath moveDelta(double deltaX, double deltaY) {
    current = current.plus(deltaX, deltaY);
    segments.add(new SegmentMoveTo(current));
    return this;
  }

  @SuppressWarnings("UnusedReturnValue")
  public PainterPath move(boolean delta, double x, double y) {
    return delta ? moveDelta(x, y) : moveTo(x, y);
  }

  @SuppressWarnings("UnusedReturnValue")
  public PainterPath line(boolean delta, double x, double y) {
    return delta ? lineDelta(x, y) : lineTo(x, y);
  }

  @SuppressWarnings("UnusedReturnValue")
  public PainterPath bezier(boolean delta, double x2, double y2, double x3, double y3, double x4, double y4) {
    return delta ? bezierDelta(x2, y2, x3, y3, x4, y4) : bezierTo(x2, y2, x3, y3, x4, y4);
  }

  public PainterPath lineTo(Vec pos) {
    requireNonNull(pos, "q5jJR1TNUf :: pos == null");
    current = pos;
    segments.add(new SegmentLineTo(pos));
    return this;
  }

  public PainterPath lineTo(double left, double top) {
    current = Vec.of(left, top);
    segments.add(new SegmentLineTo(current));
    return this;
  }

  public PainterPath lineDelta(double deltaX, double deltaY) {
    segments.add(new SegmentLineTo(current = current.plus(deltaX, deltaY)));
    return this;
  }

  public PainterPath lineDelta(Vec delta) {
    segments.add(new SegmentLineTo(current = current.plus(delta)));
    return this;
  }

  public PainterPath bezierTo(double left2, double top2, double left3, double top3, double left4, double top4) {
    return bezierTo(Vec.of(left2, top2), Vec.of(left3, top3), Vec.of(left4, top4));
  }

  public PainterPath bezierTo(Vec pos2, Vec pos3, Vec pos4) {
    segments.add(new SegmentBezierTo(pos2, pos3, current = pos4));
    return this;
  }

  public PainterPath bezierDelta(double deltaX_2, double deltaY_2,
                                 double deltaX_3, double deltaY_3,
                                 double deltaX_4, double deltaY_4) {

    Vec pos2 = current.plus(deltaX_2, deltaY_2);
    Vec pos3 = current.plus(deltaX_3, deltaY_3);
    Vec pos4 = current.plus(deltaX_4, deltaY_4);

    current = pos4;

    segments.add(new SegmentBezierTo(pos2, pos3, pos4));
    return this;
  }

  public PainterPath bezierDelta(Vec delta2, Vec delta3, Vec delta4) {

    Vec pos2 = current = current.plus(delta2);
    Vec pos3 = current = current.plus(delta3);
    Vec pos4 = current = current.plus(delta4);

    segments.add(new SegmentBezierTo(pos2, pos3, pos4));
    return this;
  }

  public PainterPath close() {
    segments.add(new SegmentClose());
    return this;
  }


  @Override
  public Pen pen() {
    return pen;
  }

  @Override
  public Fill fill() {
    return fill;
  }

  @Override
  public PainterPath paint(Graphics2D g) {

    Fill fill = this.fill.copy();
    Pen  pen  = this.pen.copy();

    if (scaleWidth) {
      pen.width(trans().convAVG(pen.width()));
    }

    if (!fill.visible() && !pen.visible()) {
      return this;
    }

    Path2D.Double system = new Path2D.Double();

    for (final Segment segment : segments) {
      segment.appendTo(system);
    }

    if (fill.visible()) {
      fill.applyTo(g);
      g.fill(system);
    }
    if (pen.visible()) {
      pen.applyTo(g);
      g.draw(system);
    }

    return this;
  }

  public PainterPath rect(Rect rect) {
    if (rect != null) {
      moveTo(rect.leftTop());
      lineTo(rect.rightTop());
      lineTo(rect.rightBottom());
      lineTo(rect.leftBottom());
      close();
    }
    return this;
  }
}
