package kz.pompei.glazga.paint.painter.path;

import kz.pompei.glazga.paint.painter.pen_fill.Fill;
import kz.pompei.glazga.paint.painter.pen_fill.Pen;

public interface PainterPathStyles {
  Pen pen();

  Fill fill();
}
