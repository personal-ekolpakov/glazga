package kz.pompei.glazga.paint.painter.pen_fill;

import java.awt.BasicStroke;
import lombok.RequiredArgsConstructor;

/**
 * Определяет конец линии
 * <p>
 * Значения констант нарисованы на странице: <a href="https://docs.oracle.com/javase/tutorial/2d/geometry/strokeandfill.html">документации Oracle</a>
 */
@RequiredArgsConstructor
public enum LineEnds {

  /**
   * Соответствует константе {@link BasicStroke#CAP_BUTT}
   */
  BUTT(BasicStroke.CAP_BUTT),

  /**
   * Соответствует константе {@link BasicStroke#CAP_ROUND}
   */
  ROUND(BasicStroke.CAP_ROUND),

  /**
   * Соответствует константе {@link BasicStroke#CAP_SQUARE}
   */
  SQUARE(BasicStroke.CAP_SQUARE),
  ;

  private final int intValue;

  public int intValue() {
    return intValue;
  }
}
