package kz.pompei.glazga.paint.painter.pen_fill;

import java.awt.BasicStroke;
import lombok.RequiredArgsConstructor;

/**
 * Как соединять линии под углом.
 * <p>
 * Значения констант нарисованы на странице: <a href="https://docs.oracle.com/javase/tutorial/2d/geometry/strokeandfill.html">документации Oracle</a>
 */
@RequiredArgsConstructor
public enum LineJoins {

  /**
   * Соответствует константе {@link BasicStroke#JOIN_BEVEL}
   */
  BEVEL(BasicStroke.JOIN_BEVEL),

  /**
   * Соответствует константе {@link BasicStroke#JOIN_MITER}
   */
  MITER(BasicStroke.JOIN_MITER),

  /**
   * Соответствует константе {@link BasicStroke#JOIN_ROUND}
   */
  ROUND(BasicStroke.JOIN_ROUND),
  ;

  private final int intValue;

  public int intValue() {
    return intValue;
  }
}
