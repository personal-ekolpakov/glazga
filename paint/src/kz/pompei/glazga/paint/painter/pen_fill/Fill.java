package kz.pompei.glazga.paint.painter.pen_fill;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;

public class Fill {

  private Color  color;
  private double alpha;

  public Fill() {
    reset();
  }

  public Fill reset() {
    color = null;
    alpha = 100;
    return this;
  }

  public Fill copy() {
    Fill ret = new Fill();
    ret.color = color;
    return ret;
  }

  public Fill color(Color color) {
    this.color = color;
    return this;
  }

  public Fill alpha(double alpha) {
    if (alpha < 0) {
      this.alpha = 0;
    } else if (alpha > 100) {
      this.alpha = 100;
    } else {
      this.alpha = alpha;
    }
    return this;
  }

  public boolean visible() {
    return color != null && alpha > 0.01;
  }

  public void applyTo(Graphics2D g) {
    if (color == null) {
      return;
    }
    g.setColor(color);
    g.setComposite(AlphaComposite.SrcOver.derive((float) (alpha / 100)));
  }
}
