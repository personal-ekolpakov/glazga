package kz.pompei.glazga.paint.painter.pen_fill;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import static kz.pompei.glazga.utils.MathUtil.doubleToFloat;

public class Pen {
  private Color     color;
  private double    alpha;
  private double    width;
  private LineEnds  ends;
  private LineJoins joins;
  private double    miterLimit;
  private double[]  dashPattern;
  private double    dashPhaseStart;


  public Pen() {
    reset();
  }

  public Pen reset() {
    width          = 0;
    ends           = LineEnds.BUTT;
    joins          = LineJoins.BEVEL;
    miterLimit     = 10;
    dashPattern    = null;
    dashPhaseStart = 0;
    color          = new Color(0, 0, 0);
    alpha          = 100;
    return this;
  }

  public Pen copy() {
    Pen pen = new Pen();
    pen.color          = color;
    pen.width          = width;
    pen.ends           = ends;
    pen.joins          = joins;
    pen.miterLimit     = miterLimit;
    pen.dashPattern    = dashPattern;
    pen.dashPhaseStart = dashPhaseStart;
    return pen;
  }


  public boolean visible() {
    return width > 0.01;
  }

  public Pen color(Color color) {
    this.color = color != null ? color : new Color(0, 0, 0);
    return this;
  }

  public Pen width(double width) {
    this.width = width;
    return this;
  }

  public double width() {
    return width;
  }

  public Pen ends(LineEnds ends) {

    this.ends = ends != null ? ends : LineEnds.BUTT;
    return this;
  }

  public Pen joins(LineJoins joins) {
    this.joins = joins != null ? joins : LineJoins.BEVEL;
    return this;
  }

  public Pen alpha(double alpha) {
    if (alpha < 0) {
      this.alpha = 0;
    } else if (alpha > 100) {
      this.alpha = 100;
    } else {
      this.alpha = alpha;
    }
    return this;
  }

  public Pen miterLimit(double miterLimit) {
    if (miterLimit < 1) {
      throw new IllegalArgumentException("aAa1vRTMq7 :: miterLimit = " + miterLimit + " but MUST be more or equal 1");
    }
    this.miterLimit = miterLimit;
    return this;
  }

  public Pen dashPattern(double... dashPattern) {
    this.dashPattern = dashPattern;
    return this;
  }

  public Pen dashPhaseStart(double dashPhaseStart) {
    this.dashPhaseStart = dashPhaseStart;
    return this;
  }

  public void applyTo(Graphics2D g) {
    //noinspection MagicConstant
    g.setStroke(new BasicStroke((float) width,
                                ends.intValue(),
                                joins.intValue(),
                                (float) miterLimit,
                                doubleToFloat(dashPattern),
                                (float) dashPhaseStart));

    g.setColor(color);
    g.setComposite(AlphaComposite.SrcOver.derive((float) (alpha / 100)));

  }

}
