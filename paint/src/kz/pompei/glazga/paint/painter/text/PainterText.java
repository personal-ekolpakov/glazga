package kz.pompei.glazga.paint.painter.text;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.Painter;
import lombok.RequiredArgsConstructor;

import static java.util.Objects.requireNonNull;

@RequiredArgsConstructor(staticName = "text")
public class PainterText extends Painter<PainterText> implements PaintTextStyles {
  private final String text;

  private PaintFontFamily fontFamily = PaintFontFamily.Play;
  private boolean         bold       = false;
  private boolean         italic     = false;

  private double        fontHeight = 100;
  private PaintTextBase paintBase  = PaintTextBase.LEFT_BASE_LINE;
  private Vec           pos       = Vec.of(0, 0);

  private Color color = new Color(0, 0, 0);

  @Override
  public PainterText fontFamily(PaintFontFamily fontFamily) {
    this.fontFamily = fontFamily;
    return this;
  }

  @Override
  public PainterText color(Color color) {
    requireNonNull(color, "p19Ab4K1PN :: color cannot be null");
    this.color = color;
    return this;
  }

  @Override
  public PainterText bond(boolean bond) {
    this.bold = bond;
    return this;
  }

  @Override
  public PainterText italic(boolean italic) {
    this.italic = italic;
    return this;
  }

  @Override
  public PainterText fontHeight(double fontHeight) {
    this.fontHeight = fontHeight;
    return this;
  }

  public ExprHeight height() {
    return FontManager.get(fontFamily).getVisibleTextHeight("{[()]}", fontHeight, bold, italic);
  }

  public PainterText paintBase(PaintTextBase paintBase) {
    this.paintBase = paintBase;
    return this;
  }

  public PainterText pos(Vec pos) {
    this.pos = pos;
    return this;
  }

  public PainterText pos(double x, double y) {
    this.pos = Vec.of(x, y);
    return this;
  }

  public Vec pos() {
    return pos;
  }

  public double width() {
    return noText() ? 0 : FontManager.get(fontFamily).getTextWidth(text, fontHeight, bold, italic);
  }

  public Vec basePosition() {
    return switch (paintBase) {
      case LEFT_BASE_LINE, LEFT_BOTTOM -> pos;
      case LEFT_TOP -> pos.plus(0, fontHeight);
      case RIGHT_BASE_LINE, RIGHT_BOTTOM -> pos.plus(-width(), 0);
      case RIGHT_TOP -> pos.plus(-width(), fontHeight);
      case CENTER -> pos.plus(-width() / 2, fontHeight / 2);
      case CENTER_TOP -> pos.plus(-width() / 2, fontHeight);
      case CENTER_BOTTOM -> pos.plus(-width() / 2, 0);
    };
  }

  private boolean noText() {
    return text == null || text.isEmpty();
  }

  @Override
  public PainterText paint(Graphics2D g) {

    if (noText()) {
      return this;
    }

    g.setFont(FontManager.get(fontFamily).getFont(bold, italic).deriveFont((float) (trans().scaleY(fontHeight))));

    Point point = trans().conv(basePosition()).toPoint();

    g.setColor(color);
    g.drawString(text, point.x, point.y);

    return this;
  }

  @SuppressWarnings("UnusedReturnValue")
  public PainterText paintIntoRect(Graphics2D g, Rect rect) {

    FontManager fontManager = FontManager.get(fontFamily);

    Rect textBounds = fontManager.getVisibleBounds(text, fontHeight, bold, italic);

    Font font = fontManager.getFont(bold, italic).deriveFont((float) (fontHeight));

    AffineTransform affineTransform = trans().combine(LinearTrans.diapasonTrans(textBounds, rect)).toAffineTransform();

    g.setFont(font.deriveFont(affineTransform));

    g.setColor(color);
    g.drawString(text, 0, 0);

    return this;
  }

  public Rect visibleBounds() {
    return FontManager.get(fontFamily).getVisibleBounds(text, fontHeight, bold, italic);
  }

  public double fontHeight() {
    return fontHeight;
  }
}
