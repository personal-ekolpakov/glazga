package kz.pompei.glazga.paint.painter.text.fonts;

import java.awt.Font;
import java.io.InputStream;
import lombok.SneakyThrows;

import static java.util.Objects.requireNonNull;

public class FontAnchor {
  @SneakyThrows
  public static Font loadFont(String fondResourceName) {
    try (InputStream fontStream = FontAnchor.class.getResourceAsStream(fondResourceName)) {
      requireNonNull(fontStream);
      return Font.createFont(Font.TRUETYPE_FONT, fontStream);
    }
  }
}
