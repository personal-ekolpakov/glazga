package kz.pompei.glazga.paint.painter.text;

import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.text.fonts.FontAnchor;

public class FontManager {

  public static final float BASE_HEIGHT = 100f;

  private FontRenderContext newFontRenderContext() {
    return new FontRenderContext(null, false, false);
  }

  public double getTextWidth(String text, double height, boolean bond, boolean italic) {

    Font font = getFont(bond, italic).deriveFont((float) height);

    FontRenderContext fc = newFontRenderContext();

    Rectangle2D stringBounds = font.getStringBounds(text, fc);

    return stringBounds.getWidth();
  }

  @SuppressWarnings("unused")
  public ExprHeight getTextHeight(String text, double height, boolean bond, boolean italic) {

    Font font = getFont(bond, italic).deriveFont((float) height);

    FontRenderContext fc     = newFontRenderContext();
    TextLayout        layout = new TextLayout(text, font, fc);

    float ascent  = layout.getAscent();
    float descent = layout.getDescent();
    float leading = layout.getLeading();

    return ExprHeight.of(ascent, descent + leading);
  }

  public ExprHeight getVisibleTextHeight(String text, double height, boolean bond, boolean italic) {

    Font font = getFont(bond, italic).deriveFont((float) height);

    FontRenderContext fc     = newFontRenderContext();

    GlyphVector glyphVector = font.createGlyphVector(fc, text);

    ExprHeight result = ExprHeight.zero();

    int numGlyphs = glyphVector.getNumGlyphs();
    for (int i = 0; i < numGlyphs; i++) {
      ExprHeight h = extractHeightFrom(glyphVector.getGlyphVisualBounds(i).getBounds2D());

      result = result.combine(h);
    }

    return result;
  }

  private ExprHeight extractHeightFrom(Rectangle2D bounds2D) {
    return ExprHeight.of(-bounds2D.getY(), bounds2D.getHeight() + bounds2D.getY());
  }

  public Rect getVisibleBounds(String text, double height, boolean bold, boolean italic) {
    Font font = getFont(bold, italic).deriveFont((float) height);

    FontRenderContext fc = newFontRenderContext();

    Rect result = null;

    GlyphVector glyphVector = font.createGlyphVector(fc, text);
    int         numGlyphs   = glyphVector.getNumGlyphs();
    for (int i = 0; i < numGlyphs; i++) {
      result = Rect.from(glyphVector.getGlyphVisualBounds(i).getBounds2D()).union(result);
    }

    return result != null ? result : Rect.from(Vec.zero());

  }

  private final Font fontRegular;
  private final Font fontBold;
  private final Font fontItalic;
  private final Font fontBoldItalic;

  private FontManager(PaintFontFamily fontFamily) {

    fontRegular = FontAnchor.loadFont(fontFamily.nameRegular).deriveFont(BASE_HEIGHT);

    if (fontFamily.nameBold != null) {
      fontBold = FontAnchor.loadFont(fontFamily.nameBold).deriveFont(BASE_HEIGHT);
    } else {
      fontBold = FontAnchor.loadFont(fontFamily.nameRegular).deriveFont(BASE_HEIGHT).deriveFont(Font.BOLD);
    }

    if (fontFamily.nameItalic != null) {
      this.fontItalic = FontAnchor.loadFont(fontFamily.nameItalic).deriveFont(BASE_HEIGHT);
    } else {
      this.fontItalic = fontRegular.deriveFont(Font.ITALIC);
    }

    if (fontFamily.nameBoldItalic != null) {
      this.fontBoldItalic = FontAnchor.loadFont(fontFamily.nameBoldItalic).deriveFont((float) 100);
    } else if (fontFamily.nameBold != null) {
      this.fontBoldItalic = fontBold.deriveFont(Font.ITALIC);
    } else {
      this.fontBoldItalic = fontRegular.deriveFont(Font.BOLD | Font.ITALIC);
    }
  }

  private static final Map<PaintFontFamily, FontManager> WIDTHS_MAP = new HashMap<>();

  static {
    for (final PaintFontFamily fontFamily : PaintFontFamily.values()) {
      WIDTHS_MAP.put(fontFamily, new FontManager(fontFamily));
    }
  }

  public static FontManager get(PaintFontFamily fontFamily) {
    return WIDTHS_MAP.get(fontFamily);
  }

  public Font getFont(boolean bold, boolean italic) {
    return bold ? (

      italic ? fontBoldItalic : fontBold

    ) : (

      italic ? this.fontItalic : this.fontRegular

    );
  }
}
