package kz.pompei.glazga.paint.painter.text;

import java.awt.Color;

public interface PaintTextStyles {
  PaintTextStyles fontFamily(PaintFontFamily fontFamily);

  PaintTextStyles bond(boolean bond);

  PaintTextStyles italic(boolean italic);

  PaintTextStyles fontHeight(double height);

  PaintTextStyles color(Color color);
}
