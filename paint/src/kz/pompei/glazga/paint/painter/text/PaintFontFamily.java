package kz.pompei.glazga.paint.painter.text;

public enum PaintFontFamily {
  Play("Play", "Play-Regular.ttf", "Play-Bold.ttf", null, null),
  Roboto("Roboto", "Roboto-Regular.ttf", "Roboto-Bold.ttf", "Roboto-Italic.ttf", "Roboto-BoldItalic.ttf"),
  ;

  public final String name;
  public final String nameRegular;
  public final String nameBold;
  public final String nameItalic;
  public final String nameBoldItalic;

  PaintFontFamily(String name,
                  String nameRegular, String nameBold, String nameItalic, String nameBoldItalic) {
    this.name           = name;
    this.nameRegular    = nameRegular;
    this.nameBold       = nameBold;
    this.nameItalic     = nameItalic;
    this.nameBoldItalic = nameBoldItalic;
  }
}
