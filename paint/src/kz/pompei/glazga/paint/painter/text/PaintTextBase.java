package kz.pompei.glazga.paint.painter.text;

public enum PaintTextBase {
  LEFT_BASE_LINE,
  LEFT_BOTTOM,
  CENTER_BOTTOM,
  RIGHT_BOTTOM,
  RIGHT_BASE_LINE,
  RIGHT_TOP,
  CENTER_TOP,
  LEFT_TOP,
  CENTER,
}
