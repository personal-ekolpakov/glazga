package kz.pompei.glazga.paint.painter;

import java.awt.Graphics2D;
import java.util.function.Consumer;
import kz.pompei.glazga.mat.LinearTrans;

public abstract class Painter<T extends Painter<?>> {

  public abstract T paint(Graphics2D g);

  private LinearTrans trans = LinearTrans.one();

  @SuppressWarnings("unchecked")
  public T over(Consumer<T> consumer) {
    consumer.accept((T) this);
    return (T) this;
  }

  @SuppressWarnings("unchecked")
  public T trans(LinearTrans trans) {
    this.trans = trans != null ? trans : LinearTrans.one();
    return (T) this;
  }

  protected LinearTrans trans() {
    return trans;
  }
}
