package kz.pompei.glazga.paint.painter.bracket;

import java.awt.Color;
import java.awt.Graphics2D;
import kz.pompei.glazga.common_model.BracketKind;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.Painter;
import kz.pompei.glazga.paint.painter.text.PaintFontFamily;
import kz.pompei.glazga.paint.painter.text.PainterText;
import lombok.NonNull;

public class PainterBracket extends Painter<PainterBracket> implements PainterBracketStyles {

  private BracketKind     kind       = BracketKind.ROUND;
  private ExprHeight      height     = ExprHeight.zero();
  private Color           color      = new Color(0, 0, 0);
  private Vec             pos        = Vec.zero();
  private boolean         open       = true;
  private boolean         bond       = false;
  private boolean         italic     = false;
  private PaintFontFamily fontFamily = PaintFontFamily.Play;

  private PainterBracket() {}

  public static PainterBracket of() {
    return new PainterBracket();
  }

  public static PainterBracket withKind(BracketKind kind) {
    return of().kind(kind);
  }

  public static PainterBracket withTrans(LinearTrans trans) {
    return of().trans(trans);
  }

  public PainterBracket kind(BracketKind kind) {
    this.kind = kind;
    return this;
  }

  @Override
  public PainterBracket color(Color color) {
    this.color = color;
    return this;
  }

  @Override
  public PainterBracket bond(boolean bond) {
    this.bond = bond;
    return this;
  }

  @Override
  public PainterBracket italic(boolean italic) {
    this.italic = italic;
    return this;
  }

  public PainterBracket height(ExprHeight height) {
    this.height = height;
    return this;
  }

  public PainterBracket pos(Vec pos) {
    this.pos = pos;
    return this;
  }

  public PainterBracket open(boolean open) {
    this.open = open;
    return this;
  }

  @Override
  public PainterBracket fontFamily(PaintFontFamily fontFamily) {
    this.fontFamily = fontFamily;
    return this;
  }


  private PainterText painterText() {
    return PainterText.text(bracketText())
                      .color(color)
                      .fontFamily(fontFamily)
                      .bond(bond).italic(italic)
                      .trans(trans());
  }

  public double width() {
    return painterText().visibleBounds().proportionalSetHeight(height.total()).width();
  }

  public ExprHeight height() {
    return height;
  }

  @Override
  public PainterBracket paint(Graphics2D g) {

    PainterText pt = painterText();

    pt.paintIntoRect(g, Rect.from(pos.up(height.up)).to(width(), height.total()));

    return this;
  }

  private @NonNull String bracketText() {
    return switch (kind) {
      case ROUND -> open ? "(" : ")";
      case SQUARE -> open ? "[" : "]";
      case BRACES -> open ? "{" : "}";
    };
  }
}
