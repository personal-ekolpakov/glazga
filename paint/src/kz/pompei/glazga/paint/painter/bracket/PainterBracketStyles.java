package kz.pompei.glazga.paint.painter.bracket;

import java.awt.Color;
import kz.pompei.glazga.paint.painter.text.PaintFontFamily;

public interface PainterBracketStyles {
  PainterBracketStyles bond(boolean bond);

  PainterBracketStyles italic(boolean italic);

  PainterBracketStyles fontFamily(PaintFontFamily fontFamily);

  PainterBracketStyles color(Color color);
}
