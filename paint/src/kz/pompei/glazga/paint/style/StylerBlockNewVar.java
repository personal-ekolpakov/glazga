package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.paint.painter.path.PainterPathStyles;
import kz.pompei.glazga.paint.painter.text.PaintTextStyles;
import kz.pompei.glazga.paint.style.focus.Focusing;

public interface StylerBlockNewVar {
  /**
   * Задать стили главного текста внутри блока
   */
  void applyVarNameStyle(PaintTextStyles pts);

  /**
   * Вернуть отступы от границ блока
   */
  SideSpaces aroundRectPadding();

  /**
   * Задать стиль прямоугольника, который формирует каркас блока
   */
  void applyAroundRectStyle(PainterPathStyles target);

  /**
   * Возвращает расстояние межу текстовой строкой и выражением за ней следующим.
   */
  double text_expr_dist();

  /**
   * Применить стили для рамки фокуса
   *
   * @param target   куда нужно применять стили
   * @param focusing тип фокусировки
   */
  void applyFocusRect(PainterPathStyles target, Focusing focusing);

  /**
   * Применить стили для знака присваивания
   *
   * @param target куда нужно применять стили
   */
  void applyAssignTxtStyle(PaintTextStyles target);

  /**
   * Позволяет увеличивать прямоугольник фокуса
   *
   * @return размеры для увеличения прямоугольника фокуса
   */
  SideSpaces focusRectMargin();
}
