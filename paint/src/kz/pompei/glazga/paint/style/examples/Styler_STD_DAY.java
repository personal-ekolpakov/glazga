package kz.pompei.glazga.paint.style.examples;

import java.awt.Color;
import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.paint.painter.bracket.PainterBracketStyles;
import kz.pompei.glazga.paint.painter.path.PainterPathStyles;
import kz.pompei.glazga.paint.painter.text.PaintFontFamily;
import kz.pompei.glazga.paint.painter.text.PaintTextStyles;
import kz.pompei.glazga.paint.painter.text.PainterText;
import kz.pompei.glazga.paint.style.Styler;
import kz.pompei.glazga.paint.style.StylerBlockExit;
import kz.pompei.glazga.paint.style.StylerBlockFixMethodStart;
import kz.pompei.glazga.paint.style.StylerBlockNewVar;
import kz.pompei.glazga.paint.style.StylerBond;
import kz.pompei.glazga.paint.style.StylerDown;
import kz.pompei.glazga.paint.style.StylerExprBracket;
import kz.pompei.glazga.paint.style.StylerExprCall;
import kz.pompei.glazga.paint.style.StylerExprConst;
import kz.pompei.glazga.paint.style.StylerExprDiv;
import kz.pompei.glazga.paint.style.StylerExprOp;
import kz.pompei.glazga.paint.style.StylerEye;
import kz.pompei.glazga.paint.style.expr_call.ExprCallArgStyleProps;
import kz.pompei.glazga.paint.style.focus.Focusing;

public class Styler_STD_DAY implements Styler {

  private double stdHeight() {
    return 14;
  }

  private PaintFontFamily stdFont() {
    return PaintFontFamily.Play;
  }

  @Override
  public StylerBlockExit blockExit() {
    return blockExit;
  }

  private final StylerBlockExit blockExit = new StylerBlockExit() {
    @Override
    public void applyDisplayLabel(PaintTextStyles pts) {
      pts.fontFamily(stdFont())
         .bond(false)
         .italic(false)
         .fontHeight(stdHeight())
         .color(new Color(105, 105, 105))
      ;
    }

    @Override
    public SideSpaces padding() {
      return SideSpaces.of(5, 5, 5, 8);
    }

    @Override
    public void applyAroundBorder(PainterPathStyles prs) {
      prs.pen().color(new Color(35, 35, 33)).width(1);
      prs.fill().color(new Color(231, 231, 168));
    }

    @Override
    public SideSpaces displayLabelFocusMargin() {
      return SideSpaces.of(1, -1, 1, 4);
    }

    @Override
    public void applyFocusRect(PainterPathStyles target) {
      target.pen().dashPattern(2, 2).width(1).color(new Color(161, 161, 161));
    }
  };

  @Override
  public StylerBlockNewVar blockNewVar() {
    return blockNewVar;
  }

  private final StylerBlockNewVar blockNewVar = new StylerBlockNewVar() {
    @Override
    public void applyVarNameStyle(PaintTextStyles target) {
      target.fontFamily(stdFont())
            .bond(false)
            .italic(false)
            .fontHeight(stdHeight())
            .color(new Color(105, 105, 105))
      ;
    }

    @Override
    public void applyAssignTxtStyle(PaintTextStyles target) {
      applyVarNameStyle(target);
    }

    @Override
    public SideSpaces focusRectMargin() {
      return SideSpaces.of(1, 2, 1, 1);
    }

    @Override
    public SideSpaces aroundRectPadding() {
      return SideSpaces.of(3, 5, 5, 3);
    }

    @Override
    public void applyAroundRectStyle(PainterPathStyles prs) {
      prs.pen().color(new Color(35, 35, 33)).width(1);
      prs.fill().color(new Color(231, 231, 168));
    }


    @Override
    public double text_expr_dist() {
      return 3;
    }

    @Override
    public void applyFocusRect(PainterPathStyles target, Focusing focusing) {
      if (focusing == Focusing.IS) {
        target.pen().dashPattern(2, 2).width(1).color(new Color(161, 161, 161));
      }
    }
  };


  @Override
  public StylerBlockFixMethodStart blockFixMethodStart() {
    return blockFixMethodStart;
  }

  private final StylerBlockFixMethodStart blockFixMethodStart = new StylerBlockFixMethodStart() {
    @Override
    public void applyMethodName(PaintTextStyles pts) {
      pts.fontFamily(stdFont())
         .bond(false)
         .italic(false)
         .fontHeight(stdHeight())
         .color(new Color(105, 105, 105))
      ;
    }

    @Override
    public SideSpaces aroundBorderPadding() {
      return SideSpaces.of(5, 5, 5, 8);
    }

    @Override
    public void applyAroundBorder(PainterPathStyles prs) {
      prs.pen().color(new Color(35, 35, 33)).width(1);
      prs.fill().color(new Color(231, 231, 168));
    }

    @Override
    public SideSpaces displayNameFocusRectMargin() {
      return SideSpaces.of(1, 0, 3, 3);
    }

    @Override
    public void applyFocusRect(PainterPathStyles target) {
      target.pen().dashPattern(2, 2).width(1).color(new Color(161, 161, 161));
    }
  };

  @Override
  public StylerExprConst exprConst() {
    return exprConst;
  }

  private final StylerExprConst exprConst = new StylerExprConst() {
    @Override
    public void applyTextStyle(PaintTextStyles pts, Focusing focusing) {
      pts.fontFamily(stdFont())
         .bond(false)
         .italic(false)
         .fontHeight(stdHeight())
         .color(new Color(0, 31, 192))
      ;
    }

    @Override
    public SideSpaces focusRectMargin() {
      return SideSpaces.of(0, 0, 0, 0);
    }

    @Override
    public void applyFocusRect(PainterPathStyles target, Focusing focusing) {
      if (focusing == Focusing.IS) {
        target.pen().dashPattern(2, 2).width(1).color(new Color(161, 161, 161));
      }
    }
  };


  @Override
  public StylerBond bond() {
    return bond;
  }

  private final StylerBond bond = new StylerBond() {
    @Override
    public void applyEmptyRectStyle(PainterPathStyles prs) {
      prs.pen().width(1).color(new Color(1, 1, 1));
      prs.fill().color(new Color(248, 248, 248));
    }

    @Override
    public void applyEmptyHiddenText(PaintTextStyles pts) {
      pts.fontFamily(stdFont())
         .bond(false)
         .italic(false)
         .fontHeight(stdHeight())
      ;
    }
  };

  @Override
  public StylerExprDiv exprDiv() {
    return exprDiv;
  }

  private final StylerExprDiv exprDiv = new StylerExprDiv() {
    @Override
    public SideSpaces paddingUp() {
      return SideSpaces.of(3, 0, 3, 3);
    }

    @Override
    public SideSpaces paddingDown() {
      return SideSpaces.of(3, 3, 3, 0);
    }

    @Override
    public double baseLineShift() {
      return stdHeight() / 3;
    }

    @Override
    public void applyBaseLineStyle(PainterPathStyles pps) {
      pps.pen().width(1).color(new Color(1, 1, 1));
    }

    @Override
    public SideSpaces divLineFocusMargin() {
      return SideSpaces.of(2, 2, 2, 2);
    }

    @Override
    public void divLineFocusStyle(PainterPathStyles target, Focusing focusing) {
      if (focusing == Focusing.IS) {
        target.pen().dashPattern(2, 2).width(1).color(new Color(161, 161, 161));
      }
    }
  };

  @Override
  public StylerExprOp exprOp() {
    return exprOp;
  }

  private final StylerExprOp exprOp = new StylerExprOp() {
    @Override
    public void applySignStyle(PaintTextStyles pts) {
      pts.fontFamily(stdFont())
         .bond(false)
         .italic(false)
         .fontHeight(stdHeight())
         .color(new Color(146, 0, 180))
      ;
    }

    @Override
    public void applyFocusUnderline(PainterPathStyles target, Focusing focusing) {
      if (focusing == Focusing.IS) {
        target.pen().dashPattern(2, 2).width(1).color(new Color(161, 161, 161));
      }
    }

    @Override
    public void applyFocusCursor(PainterPathStyles target, Focusing focusing) {
      if (focusing == Focusing.IS) {
        target.pen().dashPattern(2, 2).width(1).color(new Color(161, 161, 161));
      }
    }
  };

  @Override
  public StylerExprCall exprCall() {
    return exprCall;
  }

  private final StylerExprCall exprCall = new StylerExprCall() {
    @Override
    public void actLabelApply(PaintTextStyles pts, boolean notFound) {
      pts.fontFamily(stdFont())
         .bond(false)
         .italic(false)
         .fontHeight(stdHeight())
         .color(new Color(146, 0, 180))
      ;
    }

    @Override
    public void actRectApply(PainterPathStyles pps) {
      pps.pen().width(1).color(new Color(213, 192, 192));
    }

    @Override
    public void applyLabelBeforeText(PaintTextStyles pts) {
      pts.fontFamily(stdFont())
         .bond(false)
         .italic(false)
         .fontHeight(stdHeight())
         .color(new Color(146, 0, 180))
      ;
    }

    @Override
    public void applyLabelAfterText(PainterText pts) {
      pts.fontFamily(stdFont())
         .bond(false)
         .italic(false)
         .fontHeight(stdHeight())
         .color(new Color(146, 0, 180))
      ;
    }

    @Override
    public SideSpaces argPadding(ExprCallArgStyleProps asp) {
      return SideSpaces.of(2, 2, 2, 2);
    }

    @Override
    public void applyFocusRect(PainterPathStyles target) {
      target.pen().dashPattern(2, 2).width(1).color(new Color(161, 161, 161));
    }

    @Override
    public SideSpaces focusMargin() {
      return SideSpaces.of(2, 2, 2, 2);
    }
  };

  @Override
  public StylerExprBracket exprBracket() {
    return exprBracket;
  }

  @SuppressWarnings("Convert2Lambda")
  private final StylerExprBracket exprBracket = new StylerExprBracket() {
    @Override
    public void apply(PainterBracketStyles pbs) {
      pbs.fontFamily(stdFont());
      pbs.color(new Color(1, 1, 1));
    }
  };

  @Override
  public StylerDown down() {
    return down;
  }


  private final StylerDown down = new StylerDown() {
    @Override
    public void applyFocusedLine(PainterPathStyles target) {
      target.pen().color(new Color(133, 0, 238)).width(2);
    }

    @Override
    public SideSpaces expandForTakeFocus() {
      return SideSpaces.of(0, 2, 0, 2);
    }
  };

  @Override
  public StylerEye eye() {
    return eye;
  }

  private final StylerEye eye = new StylerEye() {
    @Override
    public double focusCrossLen() {
      return 5;
    }

    @Override
    public void applyFocusCross(PainterPathStyles pps) {
      pps.pen().width(2).color(new Color(255, 0, 0));
    }

    @Override
    public void applyMarkedElement(PainterPathStyles pps, UType uType) {
      pps.pen().width(0).color(new Color(141, 6, 218));
    }

    @Override
    public void applyFocusRect(PainterPathStyles pps, UType uType, int number) {
      if (number % 2 == 0) {
        pps.pen().width(1).color(new Color(58, 176, 0));
      } else {
        pps.pen().width(1).color(new Color(141, 6, 218));
      }
    }

    @Override
    public double focusStep() {
      return 10;
    }
  };

}
