package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.paint.painter.path.PainterPathStyles;

public interface StylerEye {

  double focusCrossLen();

  void applyFocusCross(PainterPathStyles pps);

  void applyMarkedElement(PainterPathStyles pps, UType uType);

  void applyFocusRect(PainterPathStyles pps, UType uType, int number);

  double focusStep();

}
