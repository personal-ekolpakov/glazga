package kz.pompei.glazga.paint.style;

public interface Styler {

  StylerBlockExit blockExit();

  StylerBlockNewVar blockNewVar();

  StylerBlockFixMethodStart blockFixMethodStart();

  StylerBond bond();

  StylerExprConst exprConst();

  StylerExprDiv exprDiv();

  StylerExprOp exprOp();

  StylerExprCall exprCall();

  StylerExprBracket exprBracket();

  StylerDown down();

  StylerEye eye();

}
