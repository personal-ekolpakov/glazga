package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.paint.painter.path.PainterPathStyles;
import kz.pompei.glazga.paint.painter.text.PaintTextStyles;

public interface StylerBlockExit {
  /**
   * Задать стили текста сообщения на блоке
   */
  void applyDisplayLabel(PaintTextStyles pts);

  /**
   * Вернуть отступы от границ блока
   */
  SideSpaces padding();

  /**
   * Задать стиль прямоугольника, окантовывающего блок
   */
  void applyAroundBorder(PainterPathStyles prs);

  /**
   * Возвращает информацию о размерах, на которые нужно расширить место,
   * где прорисовалась сообщение блока, чтобы нарисовать прямоугольник фокуса
   */
  SideSpaces displayLabelFocusMargin();

  /**
   * Применить стили к прямоугольнику фокуса
   *
   * @param target куда нужно применять стили
   */
  void applyFocusRect(PainterPathStyles target);
}
