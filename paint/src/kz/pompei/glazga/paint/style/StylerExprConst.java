package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.paint.painter.path.PainterPathStyles;
import kz.pompei.glazga.paint.painter.text.PaintTextStyles;
import kz.pompei.glazga.paint.style.focus.Focusing;

public interface StylerExprConst {
  /**
   * Задать стиль выражения константы
   *
   * @param target   место, где нужно настроить стили
   * @param focusing состояние фокуса на элементе
   */
  void applyTextStyle(PaintTextStyles target, Focusing focusing);

  /**
   * Возвращает расстояния на которые увеличивается прямоугольник отображения фокуса по сравнению с видимым местом текста
   */
  SideSpaces focusRectMargin();

  /**
   * Настраивает прямоугольник, который обозначает, что элемент в фокусе
   *
   * @param target   место, где нужно настроить стили
   * @param focusing состояние фокуса на элементе
   */
  void applyFocusRect(PainterPathStyles target, Focusing focusing);
}
