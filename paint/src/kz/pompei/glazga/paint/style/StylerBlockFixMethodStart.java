package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.paint.painter.path.PainterPathStyles;
import kz.pompei.glazga.paint.painter.text.PaintTextStyles;

public interface StylerBlockFixMethodStart {
  /**
   * Задать стили главного текста внутри блока
   */
  void applyMethodName(PaintTextStyles pts);

  /**
   * Вернуть отступы от границ блока
   */
  SideSpaces aroundBorderPadding();

  /**
   * Задать стиль прямоугольника, который формирует каркас блока
   */
  void applyAroundBorder(PainterPathStyles prs);

  /**
   * На сколько нужно расширить прямоугольник, где нарисовано имя метода, чтобы нарисовать фокус
   *
   * @return размеры расширения
   */
  SideSpaces displayNameFocusRectMargin();

  /**
   * Применяет стили для прямоугольника фокуса
   *
   * @param target куда нужно применить стиль
   */
  void applyFocusRect(PainterPathStyles target);
}
