package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.paint.painter.bracket.PainterBracketStyles;

public interface StylerExprBracket {
  void apply(PainterBracketStyles pbs);
}
