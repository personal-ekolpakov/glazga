package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.paint.painter.path.PainterPathStyles;
import kz.pompei.glazga.paint.painter.text.PaintTextStyles;
import kz.pompei.glazga.paint.painter.text.PainterText;
import kz.pompei.glazga.paint.style.expr_call.ExprCallArgStyleProps;

public interface StylerExprCall {
  /**
   * Задать стиль метки акта
   *
   * @param pts      куда задать
   * @param notFound признак, что акт не найден (в этом случае стиль должен отличаться блеклостью)
   */
  void actLabelApply(PaintTextStyles pts, boolean notFound);

  /**
   * Применяет стили для окантовки аргумента метода
   *
   * @param pps куда нужно применить стили
   */
  void actRectApply(PainterPathStyles pps);

  /**
   * Применить стили для метки аргумента, которая ставиться перед значением
   *
   * @param pts куда нужно применять стили
   */
  void applyLabelBeforeText(PaintTextStyles pts);

  /**
   * Применить стили для метки аргумента, которая ставиться после значения
   *
   * @param pts куда нужно применять стили
   */
  void applyLabelAfterText(PainterText pts);

  /**
   * Возвращает отступы у аргументов актов
   */
  SideSpaces argPadding(ExprCallArgStyleProps asp);

  /**
   * Устанавливает стиль для фокусного прямоугольника
   *
   * @param target куда устанавливать стили
   */
  void applyFocusRect(PainterPathStyles target);

  /**
   * Определяет увеличение прямоугольника фокуса
   *
   * @return размеры увеличения по сторонам
   */
  SideSpaces focusMargin();
}
