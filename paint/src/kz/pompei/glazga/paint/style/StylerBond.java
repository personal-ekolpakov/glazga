package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.paint.painter.path.PainterPathStyles;
import kz.pompei.glazga.paint.painter.text.PaintTextStyles;
import kz.pompei.glazga.paint.painter.text.PainterText;

public interface StylerBond {

  /**
   * Задаёт стиль пустого места, куда можно вставить выражение
   */
  void applyEmptyRectStyle(PainterPathStyles prs);

  /**
   * Применить стили к скрытому тексту пустого места - определяет размеры этого пустого места
   */
  void applyEmptyHiddenText(PaintTextStyles pts);
}
