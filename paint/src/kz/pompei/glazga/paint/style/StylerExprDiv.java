package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.paint.painter.path.PainterPathStyles;
import kz.pompei.glazga.paint.style.focus.Focusing;

public interface StylerExprDiv {
  /**
   * Возвращает внутренние отступы у числителя вертикального деления
   */
  SideSpaces paddingUp();

  /**
   * Возвращает внутренние отступы у знаменателя вертикального деления
   */
  SideSpaces paddingDown();

  /**
   * Показывает величину поднятия делительной линии вертикального деления над базовой линией исходного выражения
   */
  double baseLineShift();

  /**
   * Настроить стиль линии у вертикального деления
   */
  void applyBaseLineStyle(PainterPathStyles pps);

  /**
   * Определяет размеры курсора вокруг линии деления
   *
   * @return размеры расширения прямоугольника линии разделения
   */
  SideSpaces divLineFocusMargin();

  /**
   * Определяет стиль прямоугольника фокуса вокруг линии разделения
   *
   * @param target   куда применять стили
   * @param focusing статус фокусировки
   */
  void divLineFocusStyle(PainterPathStyles target, Focusing focusing);
}
