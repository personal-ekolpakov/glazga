package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.paint.painter.path.PainterPathStyles;
import kz.pompei.glazga.paint.painter.text.PaintTextStyles;
import kz.pompei.glazga.paint.style.focus.Focusing;

public interface StylerExprOp {

  /**
   * Определяет стиль теста символа оператора (ExprOp)
   */
  void applySignStyle(PaintTextStyles target);

  /**
   * Применяет стили к подчёркивающему маркеру фокуса всей операции
   *
   * @param target   куда применять стили
   * @param focusing степень фокусировки
   */
  void applyFocusUnderline(PainterPathStyles target, Focusing focusing);

  /**
   * Применяет стили к курсору фокуса
   *
   * @param target   куда применять стили
   * @param focusing степень фокусировки
   */
  void applyFocusCursor(PainterPathStyles target, Focusing focusing);
}
