package kz.pompei.glazga.paint.style;

import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.paint.painter.path.PainterPathStyles;

/**
 * Стилизация подвешивающего держателя блоков
 */
public interface StylerDown {

  /**
   * Определяет стиль фокусной линии
   *
   * @param target куда нужно устанавливать стиль
   */
  void applyFocusedLine(PainterPathStyles target);

  /**
   * Возвращает параметры расширения для прямоугольника фокуса, чтобы в него легче было попасть машкой
   */
  SideSpaces expandForTakeFocus();

}
