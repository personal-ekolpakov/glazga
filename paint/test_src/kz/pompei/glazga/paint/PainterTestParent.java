package kz.pompei.glazga.paint;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import kz.pompei.glazga.mat.Vec;
import lombok.SneakyThrows;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import static java.util.Objects.requireNonNull;
import static kz.pompei.glazga.utils.PaintUtils.applyAntialiasing;

public abstract class PainterTestParent {

  protected BufferedImage image;
  protected Graphics2D    g;
  protected boolean       applyAntialiasing = true;

  protected void createImage() {
    int width  = 2048;
    int height = 400;
    createImage(width, height);
  }

  protected void createImage(int width, int height) {
    image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    g     = image.createGraphics();
    g.setColor(new Color(255, 255, 255));
    g.fillRect(0, 0, width, height);
    if (applyAntialiasing) {
      applyAntialiasing(g);
    }
    paintGrid();
  }

  @SuppressWarnings("SameParameterValue")
  @SneakyThrows
  protected void createImageFromResource(String backgroundImageResource) {
    try (InputStream resourceInput = getClass().getResourceAsStream(backgroundImageResource)) {
      requireNonNull(resourceInput, "7iWn4n37y6 :: No resource " + backgroundImageResource + " from class " + getClass().getSimpleName());
      image = ImageIO.read(resourceInput);
    }
    g = image.createGraphics();
    if (applyAntialiasing) {
      applyAntialiasing(g);
    }
    paintGrid(new Color(236, 236, 236),
              new Color(225, 225, 231),
              new Color(200, 200, 200),
              10);
  }

  private String pathId;

  @BeforeClass
  public void beforeClass() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd--HH-mm-ss-SSS");
    pathId = sdf.format(new Date());
    pathId = "0001";
  }

  @BeforeMethod
  public void clearImageVars() {
    image             = null;
    g                 = null;
    applyAntialiasing = true;
  }

  @AfterMethod
  @SneakyThrows
  public void saveImageToFile(Method testMethod) {
    if (image == null) {
      return;
    }
    if (g != null) {
      g.dispose();
    }

    File pngOutFile = Paths.get("build")
                           .resolve("test_paint_results")
                           .resolve(pathId)
                           .resolve(getClass().getSimpleName())
                           .resolve(testMethod.getName() + ".png")
                           .toFile();

    pngOutFile.getParentFile().mkdirs();

    ImageIO.write(image, "png", pngOutFile);
  }

  protected void paintPoint(Vec p, Color color) {
    Graphics2D g2 = (Graphics2D) g.create();
    {
      double r = 5;
      Vec    c = p.minus(r / 2, r / 2);
      int    R = Math.round((float) r);
      g2.setColor(color);
      g2.fillOval(c.xInt(), c.yInt(), R, R);
    }
    g2.dispose();
  }

  protected void paintGrid(Color color1, Color color2, Color color3, double alpha) {
    int eyeWidth  = image.getWidth();
    int eyeHeight = image.getHeight();

    Graphics2D g2 = (Graphics2D) g.create();
    {
      g2.setComposite(AlphaComposite.SrcOver.derive((float) (alpha / 100)));

      g2.setColor(color1);
      g2.setStroke(new BasicStroke(0.5f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_BEVEL));

      for (int x = 0; x < eyeWidth; x += 10) {
        g2.drawLine(x, 0, x, eyeHeight);
      }
      for (int y = 0; y < eyeHeight; y += 10) {
        g2.drawLine(0, y, eyeWidth, y);
      }

      g2.setColor(color2);

      for (int x = 0; x < eyeWidth; x += 50) {
        g2.drawLine(x, 0, x, eyeHeight);
      }
      for (int y = 0; y < eyeHeight; y += 50) {
        g2.drawLine(0, y, eyeWidth, y);
      }

      g2.setColor(color3);

      for (int x = 0; x < eyeWidth; x += 100) {
        g2.drawLine(x, 0, x, eyeHeight);
      }
      for (int y = 0; y < eyeHeight; y += 100) {
        g2.drawLine(0, y, eyeWidth, y);
      }

      g2.setComposite(AlphaComposite.SrcOver);

      g2.setFont(g2.getFont().deriveFont(10f));
      for (int x = 0; x < eyeWidth; x += 100) {
        g2.drawString("" + x, x + 1, 10);
      }
      for (int y = 0; y < eyeHeight; y += 100) {
        g2.drawString("" + y, 1, y - 1);
      }
    }
    g2.dispose();
  }

  protected void paintGrid() {
    paintGrid(new Color(236, 236, 236),
              new Color(225, 225, 231),
              new Color(200, 200, 200),
              100);
  }
}
