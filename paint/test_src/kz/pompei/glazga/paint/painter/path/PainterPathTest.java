package kz.pompei.glazga.paint.painter.path;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.geom.Path2D;
import java.util.List;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.PainterTestParent;
import kz.pompei.glazga.paint.painter.pen_fill.LineEnds;
import kz.pompei.glazga.paint.painter.pen_fill.LineJoins;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PainterPathTest extends PainterTestParent {

  @Test
  public void probe_001() {
    createImage();

    {
      Path2D.Double dp = new Path2D.Double();
      dp.moveTo(100, 200);
      dp.lineTo(300, 250);

      var dashed = new BasicStroke(5.0f/* Width */,
                                   BasicStroke.CAP_BUTT,
                                   BasicStroke.JOIN_ROUND,
                                   1f/* Miter Limit*/,
                                   new float[]{20.0f, 10f}/* Dash Pattern */,
                                   0.0f/* Dash Phase Start */);

      g.setStroke(dashed);
      g.setColor(new Color(34, 124, 0));
      g.draw(dp);
    }
    {
      Path2D.Double dp = new Path2D.Double();
      dp.moveTo(10, 10);
      dp.lineTo(100, 100);

      dp.moveTo(200, 200);
      dp.lineTo(300, 100);

      g.setColor(new Color(1, 1, 1));
      g.setStroke(new BasicStroke());
      g.draw(dp);
    }

  }

  @Test
  public void probe_002() {
    createImage();

    {
      Path2D.Double dp = new Path2D.Double();
      dp.moveTo(10, 200);
      dp.lineTo(1300, 200);

      var dashed = new BasicStroke(50.0f/* Width */,
                                   BasicStroke.CAP_BUTT,
                                   BasicStroke.JOIN_ROUND,
                                   1f/* Miter Limit*/,
                                   new float[]{100.0f, 30f, 5f, 30f}/* Dash Pattern */,
                                   0.0f/* Dash Phase Start */);

      g.setStroke(dashed);

      g.setPaint(new GradientPaint(600, 0,
                                   new Color(0, 39, 255),
                                   700, 50,
                                   new Color(218, 245, 214)));

      g.draw(dp);
    }

  }

  @Test
  public void probe_003() {
    createImageFromResource("background_001.jpg");


    {
      Path2D.Double dp = new Path2D.Double();
      dp.moveTo(10, 200);
      dp.lineTo(1000, 200);

      dp.lineTo(500, 400);
      dp.lineTo(1200, 400);

      var dashed = new BasicStroke(50.0f/* Width */,
                                   BasicStroke.CAP_BUTT,
                                   BasicStroke.JOIN_MITER,
                                   50f/* Miter Limit*/,
                                   null,//new float[]{100.0f, 30f, 5f, 30f}/* Dash Pattern */,
                                   0.0f/* Dash Phase Start */);

      g.setStroke(dashed);

      g.setComposite(AlphaComposite.SrcOver.derive(0.5f));

      g.setPaint(new GradientPaint(0, 0,
                                   new Color(0, 39, 255),
                                   1300, 400,
                                   new Color(218, 245, 214)));

      g.setColor(new Color(1, 1, 1));

      g.fill(dp);
    }
  }

  @Test
  public void paint_LINES() {
    createImage();

    PainterPath.from(100, 100)
               .lineTo(400, 100)
               .over(it -> it.pen().width(11).ends(LineEnds.ROUND).color(new Color(42, 114, 0)))
               .paint(g);

    PainterPath.from(500, 200)
               .lineTo(200, 200)
               .lineTo(400, 300)
               .close()
               .over(it -> it.pen().width(11).ends(LineEnds.ROUND).joins(LineJoins.ROUND).color(new Color(42, 114, 0)))
               .over(it -> it.fill().color(new Color(108, 221, 255)))
               .paint(g);

    PainterPath.from(300, 50)
               .lineTo(250, 300)
               .lineTo(650, 100)
               .close()
               .over(it -> it.pen().width(15).ends(LineEnds.ROUND).joins(LineJoins.ROUND).color(new Color(0, 27, 114)).alpha(30))
               .over(it -> it.fill().color(new Color(103, 116, 168)).alpha(30))
               .paint(g);

  }

  @Test
  public void split2_001() {

    String input = "Zm-100a9zxy";

    //
    //
    List<String> list = PainterPath.split2(input).toList();
    //
    //

    System.out.println("NC3KeHoO02 :: list = [" + list + "]");


    assertThat(list.get(0)).isEqualTo("Z");
    assertThat(list.get(1)).isEqualTo("m");
    assertThat(list.get(2)).isEqualTo("-100");
    assertThat(list.get(3)).isEqualTo("a");
    assertThat(list.get(4)).isEqualTo("9");
    assertThat(list.get(5)).isEqualTo("z");
    assertThat(list.get(6)).isEqualTo("x");
    assertThat(list.get(7)).isEqualTo("y");
    assertThat(list).hasSize(8);
  }

  @Test
  public void split2_002() {

    String input = "M100";

    //
    //
    List<String> list = PainterPath.split2(input).toList();
    //
    //

    System.out.println("JRlOLr1hQ2 :: list = [" + list + "]");


    assertThat(list.get(0)).isEqualTo("M");
    assertThat(list.get(1)).isEqualTo("100");
    assertThat(list).hasSize(2);


  }

  @Test
  public void split2_003() {

    String input = "200";

    //
    //
    List<String> list = PainterPath.split2(input).toList();
    //
    //

    System.out.println("2nS8QZEv0s :: list = [" + list + "]");


    assertThat(list.get(0)).isEqualTo("200");
    assertThat(list).hasSize(1);


  }

  @Test
  public void paint_BEZIER() {
    createImage();

    PainterPath.withTrans(LinearTrans.one())
               .moveTo(Vec.of(100, 100))
               .lineTo(400, 100)
               .bezierTo(450, 100, 250 + 100.0 * 2 / 3, 50 + 150.0 * 2 / 3, 350, 200)
               .lineTo(450, 350)
               .over(it -> it.pen().width(11).ends(LineEnds.ROUND).color(new Color(42, 114, 0)))
               .paint(g);

    PainterPath.withTrans(LinearTrans.one())
               .moveTo(100, 200)
               .bezierDelta(0, -50, 0, 0, 50, 0)
               .lineDelta(50, 0)
               .bezierDelta(50, 0, 0, 0, 0, 50)
               .lineDelta(Vec.of(0, 50))
               .bezierDelta(0, 50, 0, 0, -50, 0)
               .lineDelta(-50, 0)
               .bezierDelta(Vec.of(-50, 0), Vec.zero(), Vec.of(0, -50))
               .close()
               .over(it -> it.pen().width(11).color(new Color(187, 19, 222)))
               .paint(g);
  }

  @Test
  public void paint_BEZIER__strCommands() {
    createImage();

    PainterPath.withTrans(LinearTrans.one())
               .moveTo(Vec.of(100, 100))
               .lineTo(400, 100)
               .bezierTo(450, 100, 250 + 100.0 * 2 / 3, 50 + 150.0 * 2 / 3, 350, 200)
               .lineTo(450, 350)
               .over(it -> it.pen().width(11).ends(LineEnds.ROUND).color(new Color(42, 114, 0)))
               .paint(g);

    PainterPath.of()
               .svgPath("M100 200   c0 -50, 0 0, 50 0   l50 0   c50 0, 0 0, 0 50   l0 50   c0 50, 0 0, -50 0   l-50 0   c-50 0, 0 0, 0 -50 Z")
               .svgPath("M300 200   c0 -50, 0 0, 50 0   l50 0   c50 0, 0 0, 0 50   l0 50   c0 50, 0 0, -50 0   l-50 0   c-50 0, 0 0, 0 -50 Z")
               .svgPath("M500 200   c0 -50, 0 0, 50 0   l50 0   c50 0, 0 0, 0 50   l0 50   c0 50, 0 0, -50 0   l-50 0   c-50 0, 0 0, 0 -50 Z")
               .over(it -> it.pen().width(11).color(new Color(187, 19, 222)))
               .paint(g);
  }
}
