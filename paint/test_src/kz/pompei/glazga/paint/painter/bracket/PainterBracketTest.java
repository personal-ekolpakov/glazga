package kz.pompei.glazga.paint.painter.bracket;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphMetrics;
import java.awt.font.GlyphVector;
import java.awt.font.LineMetrics;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import kz.pompei.glazga.common_model.BracketKind;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.PainterTestParent;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.painter.text.FontManager;
import kz.pompei.glazga.paint.painter.text.PaintFontFamily;
import kz.pompei.glazga.paint.painter.text.PaintTextBase;
import kz.pompei.glazga.paint.painter.text.PainterText;
import org.testng.annotations.Test;

public class PainterBracketTest extends PainterTestParent {

  @Test
  public void round() {
    createImage();

    PaintFontFamily font = PaintFontFamily.Play;
    BracketKind     kind = BracketKind.ROUND;

    double textHeight = 100;
    Vec    pos        = Vec.of(100, 250);

    PainterText textPainter = PainterText.text("(Щёж)")
                                         .fontFamily(font)
                                         .color(new Color(134, 36, 0))
                                         .fontHeight(textHeight);


    PainterBracket painter1 = PainterBracket.withKind(kind)
                                            .open(true)
                                            .fontFamily(font)
                                            .color(new Color(138, 0, 176))
                                            .height(textPainter.height())
                                            .pos(pos);

    PainterPath.of()
               .over(p -> p.pen().width(1).color(new Color(74, 180, 32)))
               .rect(Rect.over(pos, painter1.width(), textPainter.height()))
               .moveTo(pos)
               .lineTo(pos.right(painter1.width()))
               .paint(g);

    painter1.paint(g);


    {
      pos = pos.right(painter1.width());

      PainterPath.of()
                 .over(p -> p.pen().width(1).color(new Color(0, 222, 198)))
                 .rect(Rect.over(pos, textPainter.width(), textPainter.height()))
                 .moveTo(pos)
                 .lineTo(pos.right(textPainter.width()))
                 .paint(g);

      textPainter.pos(pos)
                 .paintBase(PaintTextBase.LEFT_BASE_LINE)
                 .paint(g);
      pos = pos.right(textPainter.width());
    }

    PainterBracket painter2 = PainterBracket.withKind(kind)
                                            .open(false)
                                            .fontFamily(font)
                                            .color(new Color(138, 0, 176))
                                            .height(textPainter.height())
                                            .pos(pos);

    PainterPath.of()
               .over(p -> p.pen().width(1).color(new Color(74, 180, 32)))
               .rect(Rect.over(pos, painter1.width(), textPainter.height()))
               .moveTo(pos)
               .lineTo(pos.right(painter1.width()))
               .paint(g);

    painter2.paint(g);
  }

  @Test
  public void square() {
    createImage();

    PaintFontFamily font = PaintFontFamily.Roboto;
    BracketKind     kind = BracketKind.SQUARE;

    double textHeight = 100;
    Vec    pos        = Vec.of(100, 250);

    PainterText textPainter = PainterText.text("[Щёж]")
                                         .fontFamily(font)
                                         .color(new Color(134, 36, 0))
                                         .fontHeight(textHeight);


    PainterBracket painter1 = PainterBracket.withKind(kind)
                                            .open(true)
                                            .fontFamily(font)
                                            .color(new Color(138, 0, 176))
                                            .height(textPainter.height())
                                            .pos(pos);

    PainterPath.of()
               .over(p -> p.pen().width(1).color(new Color(74, 180, 32)))
               .rect(Rect.over(pos, painter1.width(), textPainter.height()))
               .moveTo(pos)
               .lineTo(pos.right(painter1.width()))
               .paint(g);

    painter1.paint(g);


    {
      pos = pos.right(painter1.width());

      PainterPath.of()
                 .over(p -> p.pen().width(1).color(new Color(0, 222, 198)))
                 .rect(Rect.over(pos, textPainter.width(), textPainter.height()))
                 .moveTo(pos)
                 .lineTo(pos.right(textPainter.width()))
                 .paint(g);

      textPainter.pos(pos)
                 .paintBase(PaintTextBase.LEFT_BASE_LINE)
                 .paint(g);
      pos = pos.right(textPainter.width());
    }

    PainterBracket painter2 = PainterBracket.withKind(kind)
                                            .open(false)
                                            .fontFamily(font)
                                            .color(new Color(138, 0, 176))
                                            .height(textPainter.height())
                                            .pos(pos);

    PainterPath.of()
               .over(p -> p.pen().width(1).color(new Color(74, 180, 32)))
               .rect(Rect.over(pos, painter1.width(), textPainter.height()))
               .moveTo(pos)
               .lineTo(pos.right(painter1.width()))
               .paint(g);

    painter2.paint(g);
  }

  @Test
  public void braces() {
    createImage();

    PaintFontFamily font = PaintFontFamily.Play;
    BracketKind     kind = BracketKind.BRACES;

    double textHeight = 100;
    Vec    pos        = Vec.of(100, 250);

    PainterText textPainter = PainterText.text("{Щёж}")
                                         .fontFamily(font)
                                         .color(new Color(134, 36, 0))
                                         .fontHeight(textHeight);


    PainterBracket painter1 = PainterBracket.withKind(kind)
                                            .open(true)
                                            .fontFamily(font)
                                            .color(new Color(138, 0, 176))
                                            .height(textPainter.height())
                                            .pos(pos);

    PainterPath.of()
               .over(p -> p.pen().width(1).color(new Color(74, 180, 32)))
               .rect(Rect.over(pos, painter1.width(), textPainter.height()))
               .moveTo(pos)
               .lineTo(pos.right(painter1.width()))
               .paint(g);

    painter1.paint(g);


    {
      pos = pos.right(painter1.width());

      PainterPath.of()
                 .over(p -> p.pen().width(1).color(new Color(0, 222, 198)))
                 .rect(Rect.over(pos, textPainter.width(), textPainter.height()))
                 .moveTo(pos)
                 .lineTo(pos.right(textPainter.width()))
                 .paint(g);

      textPainter.pos(pos)
                 .paintBase(PaintTextBase.LEFT_BASE_LINE)
                 .paint(g);
      pos = pos.right(textPainter.width());
    }

    PainterBracket painter2 = PainterBracket.withKind(kind)
                                            .open(false)
                                            .fontFamily(font)
                                            .color(new Color(138, 0, 176))
                                            .height(textPainter.height())
                                            .pos(pos);

    PainterPath.of()
               .over(p -> p.pen().width(1).color(new Color(74, 180, 32)))
               .rect(Rect.over(pos, painter1.width(), textPainter.height()))
               .moveTo(pos)
               .lineTo(pos.right(painter1.width()))
               .paint(g);

    painter2.paint(g);
  }

  private void labelPoint(Graphics2D g, Vec pos, String text) {
    Font savedFont = g.getFont();
    g.setFont(FontManager.get(PaintFontFamily.Play).getFont(false, false).deriveFont(9f));
    int stepLeft = g.getFontMetrics().stringWidth(text);
    g.drawString(text, pos.xInt() - stepLeft, pos.yInt());
    g.setFont(savedFont);
  }

  @Test
  public void fontTesting() {
    createImage(800, 800);

    double textHeight = 100;

    g.setFont(FontManager.get(PaintFontFamily.Play).getFont(false, false).deriveFont((float) textHeight));

    g.setColor(new Color(151, 0, 192));

    String text = "(Привет!)";

    Vec base = Vec.of(150, 200);

    g.drawString(text, base.xInt(), base.yInt());

    int textWidth = g.getFontMetrics().stringWidth(text);

    {
      g.setColor(new Color(70, 192, 0));
      g.drawLine(base.xInt(), base.yInt(), base.xInt() + textWidth, base.yInt());
      labelPoint(g, base, "base");
    }

    {
      g.setColor(new Color(70, 192, 0));
      Vec pos1 = base.up(g.getFontMetrics().getAscent());
      g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
      labelPoint(g, pos1, "Ascent");
    }
    {
      g.setColor(new Color(192, 125, 0));
      int height = g.getFontMetrics().getHeight();
      Vec pos1   = base.up(height);
      g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
      labelPoint(g, pos1, "Height=" + height);
    }
    {
      g.setColor(new Color(0, 192, 157));
      int descent = g.getFontMetrics().getDescent();
      Vec pos1    = base.down(descent);
      g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
      labelPoint(g, pos1, "Descent=" + descent);
    }
    {
      g.setColor(new Color(0, 0, 0));
      Vec pos1 = base.up(textHeight);
      g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
      labelPoint(g, pos1, "textHeight");
    }
    {
      g.setColor(new Color(232, 8, 8));
      int leading = g.getFontMetrics().getLeading();
      Vec pos1    = base.up(leading);
      g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
      labelPoint(g, pos1, "Leading=" + leading);
    }
    {
      g.setColor(new Color(10, 237, 245));
      int maxAdvance = g.getFontMetrics().getMaxAdvance();
      Vec pos1       = base.up(maxAdvance);
      g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
      labelPoint(g, pos1, "MaxAdvance=" + maxAdvance);
    }
    {
      g.setColor(new Color(26, 33, 33));
      LineMetrics lm                  = g.getFontMetrics().getLineMetrics(text, g);
      float       strikethroughOffset = lm.getStrikethroughOffset();
      Vec         pos1                = base.down(strikethroughOffset);
      Stroke      savedStroke         = g.getStroke();
      g.setStroke(new BasicStroke(lm.getStrikethroughThickness(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));
      g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
      labelPoint(g, pos1, "strikethroughOffset=" + strikethroughOffset);
      g.setStroke(savedStroke);
    }

    for (final String availableFontFamilyName : GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames()) {
      System.out.println("jNJrd2D710 :: availableFontFamilyName = " + availableFontFamilyName);
    }
  }

  @Test
  public void fontTesting2() {
    applyAntialiasing = true;
    createImage(800, 800);

    double textHeight = 100;

    g.setFont(FontManager.get(PaintFontFamily.Play).getFont(false, false).deriveFont((float) textHeight));

    g.setColor(new Color(151, 0, 192));

    String text = "(Привет!)";

    Vec base = Vec.of(150, 200);

    g.drawString(text, base.xInt(), base.yInt());

    int textWidth = g.getFontMetrics().stringWidth(text);

    {
      g.setColor(new Color(70, 192, 0));
      g.drawLine(base.xInt(), base.yInt(), base.xInt() + textWidth, base.yInt());
      labelPoint(g, base, "base");
    }

    {
      g.setColor(new Color(70, 192, 0));
      Vec pos1 = base.up(g.getFontMetrics().getAscent());
      g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
      labelPoint(g, pos1, "Ascent");
    }
    {
      FontRenderContext fc             = g.getFontRenderContext();
      TextLayout        layout         = new TextLayout("(", g.getFont(), fc);
      float             visibleAdvance = layout.getVisibleAdvance();

      g.setColor(new Color(12, 53, 187));
      Vec pos1 = base.up(visibleAdvance);
      g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
      labelPoint(g, pos1, "visibleAdvance");

      layout.draw(g, base.xInt(), base.yInt());
    }

    {
//      FontRenderContext fc = g.getFontRenderContext();
      FontRenderContext fc = new FontRenderContext(null, false, false);

      GlyphVector glyphVector = g.getFont().createGlyphVector(fc, "(");

      GlyphMetrics glyphMetrics = glyphVector.getGlyphMetrics(0);

      Rectangle2D bounds2D = glyphMetrics.getBounds2D();

      double bounds_Y        = bounds2D.getY();
      double bounds_Y_Height = bounds2D.getY() + bounds2D.getHeight();

      g.setColor(new Color(140, 58, 121));
      {
        Vec pos1 = base.down(bounds_Y);
        g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
        labelPoint(g, pos1, "bounds_Y");
      }
      {
        Vec pos1 = base.down(bounds_Y_Height);
        g.drawLine(pos1.xInt(), pos1.yInt(), pos1.xInt() + textWidth, pos1.yInt());
        labelPoint(g, pos1, "bounds_Y_Height");
      }

    }
  }
}
