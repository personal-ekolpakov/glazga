package kz.pompei.glazga.paint.text;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.PainterTestParent;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.painter.text.PaintFontFamily;
import kz.pompei.glazga.paint.painter.text.PaintTextBase;
import kz.pompei.glazga.paint.painter.text.PainterText;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PainterTextTest extends PainterTestParent {

  private void paintDrawRect(PainterText tp, Graphics2D g) {
    createImage();

    g.setColor(new Color(128, 128, 128));
    {
      int y1 = tp.basePosition().yInt();
      int y2 = (int) Math.round(tp.basePosition().y - tp.height().total());

      g.drawLine(0, y1, image.getWidth(), y1);
      g.drawLine(0, y2, image.getWidth(), y2);

      int x1 = tp.basePosition().xInt();
      int x2 = (int) Math.round(tp.basePosition().x + tp.width());

      g.drawLine(x1, 0, x1, image.getHeight());
      g.drawLine(x2, 0, x2, image.getHeight());
    }
  }

  @Test
  public void paintPhrase_100() {
    createImage();

    PainterText tp = PainterText.text("Начинается новый день!")
                                .bond(false).italic(false)
                                .fontFamily(PaintFontFamily.Roboto)
                                .fontHeight(100)
                                .paintBase(PaintTextBase.LEFT_BASE_LINE)
                                .pos(100, 300);

    paintDrawRect(tp, g);
    paintPoint(tp.pos(), new Color(0, 36, 255));
    tp.paint(g);
  }

  @Test
  public void paintPhrase_20() {
    createImage();

    PainterText tp = PainterText.text("Начинается новый день!")
                                .bond(false).italic(false)
                                .fontFamily(PaintFontFamily.Roboto)
                                .fontHeight(20)
                                .paintBase(PaintTextBase.LEFT_BASE_LINE)
                                .pos(100, 300);


    paintDrawRect(tp, g);
    paintPoint(tp.pos(), new Color(0, 36, 255));
    g.setColor(Color.BLUE);
    tp.paint(g).basePosition();

  }

  @Test
  public void paintPhrase_150() {
    createImage();

    PainterText tp = PainterText.text("Начинается новый день!")
                                .bond(false).italic(false)
                                .fontFamily(PaintFontFamily.Roboto)
                                .fontHeight(150)
                                .paintBase(PaintTextBase.LEFT_BASE_LINE)
                                .pos(100, 300);

    paintDrawRect(tp, g);
    paintPoint(tp.pos(), new Color(0, 36, 255));
    g.setColor(Color.BLUE);
    tp.paint(g);

  }

  @Test
  public void paintPhrase_150__LEFT_TOP() {
    createImage();

    PainterText tp = PainterText.text("Начинается новый день!")
                                .bond(false).italic(false)
                                .fontFamily(PaintFontFamily.Roboto)
                                .fontHeight(150)
                                .paintBase(PaintTextBase.LEFT_TOP)
                                .pos(100, 50);

    paintDrawRect(tp, g);

    g.setColor(new Color(0, 36, 255));
    tp.paint(g);

    paintPoint(tp.pos(), new Color(217, 6, 60));
    paintPoint(tp.basePosition(), new Color(0, 36, 255));
  }

  @Test
  public void paintPhrase_150__CENTER() {
    createImage();

    var tp = PainterText.text("Начинается новый день!")
                        .bond(false).italic(false)
                        .fontFamily(PaintFontFamily.Roboto)
                        .fontHeight(150)
                        .paintBase(PaintTextBase.CENTER)
                        .pos((double) image.getWidth() / 2, (double) image.getHeight() / 2);

    paintDrawRect(tp, g);

    g.setColor(new Color(0, 36, 255));
    tp.paint(g);

    paintPoint(tp.pos(), new Color(217, 6, 60));
    paintPoint(tp.basePosition(), new Color(0, 36, 255));
  }

  @Test
  public void checkReturnThis() {

    BufferedImage image = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);

    PainterText original = PainterText.text("");

    Graphics2D g = image.createGraphics();

    assertThat(original.paint(g)).isSameAs(original);
    g.dispose();

    assertThat(original.color(new Color(1, 1, 1))).isSameAs(original);

    assertThat(original.bond(true)).isSameAs(original);

    assertThat(original.italic(true)).isSameAs(original);

    assertThat(original.fontFamily(PaintFontFamily.Roboto)).isSameAs(original);

    assertThat(original.fontHeight(1)).isSameAs(original);

    assertThat(original.paintBase(PaintTextBase.LEFT_TOP)).isSameAs(original);
  }

  @Test
  public void paintToRect_001() {
    createImage();

    Rect rect = Rect.of(100, 100, 150, 200);

    PainterPath.of()
               .over(p -> p.pen().width(1).color(new Color(29, 196, 196)))
               .rect(rect)
               .paint(g);

    PainterText.text("SI")
               .color(new Color(126, 71, 38))
               .bond(false).italic(false)
               .fontFamily(PaintFontFamily.Roboto)
               .paintIntoRect(g, rect)
    ;
  }

  @Test
  public void paintToRect_002() {
    createImage();

    {
      Rect rect = Rect.of(100, 100, 150, 200);

      PainterPath.of()
                 .over(p -> p.pen().width(1).color(new Color(29, 196, 196)))
                 .rect(rect)
                 .paint(g);

      PainterText.text("(")
                 .color(new Color(126, 71, 38))
                 .bond(false).italic(false)
                 .fontFamily(PaintFontFamily.Roboto)
                 .paintIntoRect(g, rect);
    }
    {
      Rect rect = Rect.of(250, 100, 300, 200);

      PainterPath.of()
                 .over(p -> p.pen().width(1).color(new Color(29, 196, 196)))
                 .rect(rect)
                 .paint(g);

      PainterText pt = PainterText.text("(")
                                  .color(new Color(126, 71, 38))
                                  .bond(false).italic(false)
                                  .fontFamily(PaintFontFamily.Play)
                                  .paintIntoRect(g, rect);

      System.out.println("Ehd6RfJ8cC :: pt.visibleBounds = " + pt.visibleBounds());
    }

    {
      Vec    pos    = Vec.of(400, 100);
      double height = 100;

      PainterPath.of()
                 .over(p -> p.pen().width(1).color(new Color(29, 196, 196)))
                 .moveTo(pos).down(height)
                 .paint(g);

      PainterText pt = PainterText.text("(")
                                  .color(new Color(126, 71, 38))
                                  .bond(false).italic(false)
                                  .fontFamily(PaintFontFamily.Roboto);

      pt.paintIntoRect(g, pt.visibleBounds().proportionalSetHeight(height).rightTopMoveTo(pos));

    }
    {
      Vec    pos    = Vec.of(450, 100);
      double height = 100;

      PainterPath.of()
                 .over(p -> p.pen().width(1).color(new Color(29, 196, 196)))
                 .moveTo(pos).down(height)
                 .paint(g);

      PainterText pt = PainterText.text("(")
                                  .color(new Color(126, 71, 38))
                                  .bond(false).italic(false)
                                  .fontFamily(PaintFontFamily.Play);

      pt.paintIntoRect(g, pt.visibleBounds().proportionalSetHeight(height).rightTopMoveTo(pos));

    }
  }
}
