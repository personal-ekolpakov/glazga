package kz.pompei.glazga.mat;

import org.assertj.core.data.Offset;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RectTest {

  @Test
  public void of() {

    //
    //
    Rect rect = Rect.of(1, 2, 3, 4);
    //
    //

    assertThat(rect.left).isEqualTo(1);
    assertThat(rect.top).isEqualTo(2);
    assertThat(rect.right).isEqualTo(3);
    assertThat(rect.bottom).isEqualTo(4);
  }

  @Test
  public void from_to() {

    //
    //
    Rect rect = Rect.from(Vec.of(1, 2)).to(Size.of(10, 20));
    //
    //

    assertThat(rect.left).isEqualTo(1);
    assertThat(rect.top).isEqualTo(2);
    assertThat(rect.right).isEqualTo(1 + 10);
    assertThat(rect.bottom).isEqualTo(2 + 20);
  }

  @Test
  public void dia() {

    //
    //
    Rect rect = Rect.dia(Vec.of(1, 2), Vec.of(3, 4));
    //
    //

    assertThat(rect.left).isEqualTo(1);
    assertThat(rect.top).isEqualTo(2);
    assertThat(rect.right).isEqualTo(3);
    assertThat(rect.bottom).isEqualTo(4);
  }

  @Test
  public void over() {

    //
    //
    Rect rect = Rect.over(Vec.of(1, 2), Size.of(10, 20));
    //
    //

    assertThat(rect.left).isEqualTo(1);
    assertThat(rect.top).isEqualTo(2);
    assertThat(rect.right).isEqualTo(1 + 10);
    assertThat(rect.bottom).isEqualTo(2 + 20);
  }

  @Test
  public void norm__horizontal() {

    Rect rect = Rect.of(3, 2, 1, 4);

    //
    //
    Rect actual = rect.norm();
    //
    //

    assertThat(actual).isEqualTo(Rect.of(1, 2, 3, 4));

  }

  @Test
  public void norm__vertical() {

    Rect rect = Rect.of(1, 4, 3, 2);

    //
    //
    Rect actual = rect.norm();
    //
    //

    assertThat(actual).isEqualTo(Rect.of(1, 2, 3, 4));

  }

  @Test
  public void swapX() {

    Rect rect = Rect.of(1, 2, 3, 4);

    //
    //
    Rect actual = rect.swapX();
    //
    //

    assertThat(actual).isEqualTo(Rect.of(3, 2, 1, 4));

  }

  @Test
  public void swapY() {

    Rect rect = Rect.of(1, 2, 3, 4);

    //
    //
    Rect actual = rect.swapY();
    //
    //

    assertThat(actual).isEqualTo(Rect.of(1, 4, 3, 2));

  }

  @Test
  public void center() {
    Rect rect = Rect.of(1, 2, 3, 4);

    //
    //
    Vec center = rect.center();
    //
    //

    assertThat(center.x).isEqualTo((1 + 3) / 2.);
    assertThat(center.y).isEqualTo((2 + 4) / 2.);
  }

  @Test
  public void setLeftTop() {

    Rect rect = Rect.of(20, 10, 220, 110);

    double width  = rect.width();
    double height = rect.height();

    //
    //
    Rect actual = rect.setLeftTop(30, 40);
    //
    //

    assertThat(actual.width()).isEqualTo(width, Offset.offset(1e-6));
    assertThat(actual.height()).isEqualTo(height, Offset.offset(1e-6));

    assertThat(actual.left).isEqualTo(30, Offset.offset(1e-6));
    assertThat(actual.top).isEqualTo(40, Offset.offset(1e-6));

  }

  @DataProvider
  private Object[][] containsVec_DataProvider() {
    return new Object[][]{
      {Vec.of(40, 30), Rect.of(20, 10, 220, 110), true},

      {Vec.of(340, 30), Rect.of(20, 10, 220, 110), false},
      {Vec.of(40, 330), Rect.of(20, 10, 220, 110), false},
      {Vec.of(340, 330), Rect.of(20, 10, 220, 110), false},
      {Vec.of(0, 0), Rect.of(20, 10, 220, 110), false},

      {Vec.of(300, 5), Rect.of(20, 10, 220, 110), false},

      {Vec.of(10, 500), Rect.of(20, 10, 220, 110), false},
    };
  }

  @Test(dataProvider = "containsVec_DataProvider")
  public void containsVec(Vec pos, Rect rect, boolean expectedContains) {

    //
    //
    boolean contains = rect.containsVec(pos);
    //
    //

    assertThat(contains).isEqualTo(expectedContains);
  }

}
