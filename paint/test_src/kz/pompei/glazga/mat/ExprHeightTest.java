package kz.pompei.glazga.mat;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ExprHeightTest {

  @Test
  public void upMul() {

    ExprHeight exprHeight = ExprHeight.of(3, 7);

    //
    //
    ExprHeight actual = exprHeight.upMul(4);
    //
    //

    assertThat(actual.up).isEqualTo(3 * 4);
    assertThat(actual.down).isEqualTo(7);

  }
}
