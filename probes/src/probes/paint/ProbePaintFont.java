package probes.paint;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import lombok.SneakyThrows;

public class ProbePaintFont {
  public static void main(String[] args) {
    ProbePaintFont probePaintFont = new ProbePaintFont();
    probePaintFont.exe();
  }

  @SneakyThrows
  private void exe() {
    BufferedImage image = new BufferedImage(800, 400, BufferedImage.TYPE_INT_ARGB);

    {
      Graphics2D g = image.createGraphics();



      g.dispose();
    }

    File saveFile = new File("build/paint_probes/hello.png");
    saveFile.getParentFile().mkdirs();
    ImageIO.write(image, "png", saveFile);
  }
}
