package kz.pompei.glazga.utils;

public class MathUtil {

  public static int round(double x) {
    return Math.round((float) x);
  }

  public static float[] doubleToFloat(double[] array) {
    if (array == null) {
      return null;
    }
    int     arrayLangth = array.length;
    float[] ret         = new float[arrayLangth];
    for (int i = 0; i < arrayLangth; i++) {
      ret[i] = (float) array[i];
    }
    return ret;
  }

}
