package kz.pompei.glazga.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.NonNull;

public class StrUtil {
  public static String extractFirstKav(String message) {

    if (message == null) {
      return null;
    }

    int idx1 = message.indexOf('"');
    if (idx1 < 0) {
      return null;
    }

    int idx2 = message.indexOf('"', idx1 + 1);
    if (idx2 < 0) {
      return null;
    }

    return message.substring(idx1 + 1, idx2);
  }

  public static String displayDelay(@NonNull Date now, Date happenedDate) {

    if (happenedDate == null) {
      return "";
    }

    long delta = now.getTime() - happenedDate.getTime();
    if (delta < 0) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      return sdf.format(happenedDate);
    }

    if (delta <= 1_500) {
      return "Около секунды назад";
    }
    if (delta <= 2_500) {
      return "Около 2 секунд назад";
    }
    if (delta <= 3_500) {
      return "Около 3 секунд назад";
    }
    if (delta <= 5_500) {
      return "Около 5 секунд назад";
    }
    if (delta <= 11_000) {
      return "Около 10 секунд назад";
    }
    if (delta <= 22_000) {
      return "Около 20 секунд назад";
    }
    if (delta <= 35_000) {
      return "Около 30 секунд назад";
    }

    if (delta <= 45_000) {
      return "Меньше минуты назад";
    }

    if (delta <= 70 * 1000) {
      return "Минуту назад назад";
    }
    if (delta <= 2 * 60 * 1000 + 5_000) {
      return "Две минуты назад";
    }
    if (delta <= 3 * 60 * 1000 + 5_000) {
      return "Три минуты назад";
    }
    if (delta <= 4 * 60 * 1000 + 5_000) {
      return "Четыре минуты назад";
    }
    if (delta <= 5 * 60 * 1000 + 5_000) {
      return "Пять минут назад";
    }
    if (delta <= 10 * 60 * 1000) {
      return "Больше пяти минут назад";
    }
    if (delta <= 30 * 60 * 1000) {
      return "Больше десяти минут назад";
    }
    if (delta <= 60 * 60 * 1000) {
      return "Не больше часа назад";
    }

    if (delta <= 24 * 60 * 60 * 1000) {

      long hours = delta / (60 * 60 * 1000);

      if (hours == 1) {
        return "Около часа назад";
      }
      if (hours == 2 || hours == 3 || hours == 4 || hours == 22 || hours == 23) {
        return hours + " часа назад";
      }
      if (hours == 5 || hours == 6 || hours == 7) {
        return hours + " часов назад";
      }
      if (hours == 21) {
        return hours + " час назад";
      }

      return hours + " ";
    }

    if (delta < 32L * 24 * 60 * 60 * 1000) {
      long days  = delta / (24L * 60 * 60 * 1000);
      long hours = (delta % (24L * 60 * 60 * 1000)) / (60L * 60 * 1000);

      StringBuilder sb = new StringBuilder();

      if (days == 1 || days == 21 || days == 31) {
        sb.append(days).append(" день");
      } else if (days == 2 || days == 3 || days == 4 || days == 22 || days == 23 || days == 24) {
        sb.append(days).append(" дня");
      } else {
        sb.append(days).append(" дней");
      }

      if (hours > 0) {

        if (hours == 1 || hours == 21) {
          sb.append(' ').append(hours).append(" час");
        } else if (hours == 2 || hours == 3 || hours == 4) {
          sb.append(' ').append(hours).append(" часа");
        } else {
          sb.append(' ').append(hours).append(" часов");
        }

      }

      sb.append(" назад");
      return sb.toString();
    }


    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    return sdf.format(happenedDate);
  }

  public static @NonNull String applyStrTemplate(@NonNull String borders,
                                                 @NonNull String strTemplate,
                                                 @NonNull Map<String, String> params) {

    Set<String> usedParams = new HashSet<>();
    String      ret        = applyStrTemplate0(borders, strTemplate, params, usedParams);

    if (usedParams.equals(params.keySet())) {
      return ret;
    }

    throw new RuntimeException("mKucW1alB4 :: Different available parameters and it's values:"
                               + " Available " + params.keySet() + ", used: " + usedParams);
  }

  private static String applyStrTemplate0(@NonNull String borders,
                                          @NonNull String strTemplate,
                                          @NonNull Map<String, String> params,
                                          @NonNull Set<String> usedParams) {

    if (borders.length() == 0 || borders.length() % 2 > 0) {
      throw new IllegalArgumentException("rgcMpkQ0jq :: Illegal borders - it MUST have odd chars");
    }

    String              open  = borders.substring(0, borders.length() / 2);
    String              close = borders.substring(borders.length() / 2);
    Map<String, String> prm   = params == null ? Map.of() : params;
    int                 i     = 0;
    StringBuilder       sb    = new StringBuilder();

    while (true) {

      int i1 = strTemplate.indexOf(open, i);
      if (i1 < 0) {
        return sb + strTemplate.substring(i);
      }

      int i2 = strTemplate.indexOf(close, i1 + open.length());
      if (i2 < 0) {
        return sb + strTemplate.substring(i, i1);
      }

      sb.append(strTemplate, i, i1);

      String key   = strTemplate.substring(i1 + open.length(), i2).trim();
      String value = prm.get(key);

      usedParams.add(key);

      if (value != null) {
        sb.append(value);
      }

      i = i2 + close.length();
    }
  }


  public static String rollUpIllegalChars(String prefix) {
    if (prefix == null) {
      return null;
    }
    StringBuilder sb = new StringBuilder(prefix.length());
    for (int i = 0, L = prefix.length(); i < L; i++) {
      sb.append(rollUpIllegalChar(prefix.charAt(i)));
    }
    return sb.toString();
  }


  @SuppressWarnings("DuplicateBranchesInSwitch")
  private static String rollUpIllegalChar(char c) {

    if ('0' <= c && c <= '9' || 'A' <= c && c <= 'Z' || 'a' <= c && c <= 'z' || c == '_' || c == '.') {
      return "" + c;
    }


    String s = switch (Character.toUpperCase(c)) {
      //@formatter:off
      case 'А' -> "a";
      case 'Б' -> "b";
      case 'В' -> "v";
      case 'Г' -> "g";
      case 'Д' -> "d";
      case 'Е' -> "e";
      case 'Ё' -> "e";
      case 'Ж' -> "zh";
      case 'З' -> "z";
      case 'И' -> "i";
      case 'Й' -> "y";
      case 'К' -> "k";
      case 'Л' -> "l";
      case 'М' -> "m";
      case 'Н' -> "n";
      case 'О' -> "o";
      case 'П' -> "p";
      case 'Р' -> "r";
      case 'С' -> "s";
      case 'Т' -> "t";
      case 'У' -> "u";
      case 'Ф' -> "f";
      case 'Х' -> "h";
      case 'Ц' -> "c";
      case 'Ч' -> "ch";
      case 'Ш' -> "sh";
      case 'Щ' -> "sch";
      case 'Ъ' -> "_";
      case 'Ы' -> "y";
      case 'Ь' -> "_";
      case 'Э' -> "e";
      case 'Ю' -> "yu";
      case 'Я' -> "ya";
      default  -> "_";
      //@formatter:on
    };

    return Character.isLowerCase(c) ? s : s.toUpperCase();
  }
}
