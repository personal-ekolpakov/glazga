package kz.pompei.glazga.utils;

import lombok.NonNull;
import lombok.ToString;

@ToString
public class Page {
  public int offset;
  public int limit;

  public static @NonNull Page withLimit(int pageSize) {
    Page page = new Page();
    page.limit = pageSize;
    return page;
  }

  public static @NonNull Page with(int offset, int pageSize) {
    Page page = new Page();
    page.offset = offset;
    page.limit  = pageSize;
    return page;
  }
}
