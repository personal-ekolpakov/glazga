package kz.pompei.glazga.utils;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class Height {
  /**
   * Полная высота строки
   */
  public final float height;

  /**
   * Смещение сверху (от начала строки) до базовой линии строки.
   * <p>
   * По этой строке нужно печатать текст
   */
  public final float base;

  /**
   * Смещение сверху (от начала строки) до средней линии строки.
   * <p>
   * Относительно средней линии нужно позиционировать иконки и другие элементы
   */
  public final float center;

  public Height multiply(float factor) {
    return Height.of(height * factor, base * factor, center * factor);
  }
}
