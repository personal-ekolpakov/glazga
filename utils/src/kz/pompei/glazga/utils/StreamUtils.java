package kz.pompei.glazga.utils;

import java.io.InputStream;
import java.io.OutputStream;
import lombok.SneakyThrows;

public class StreamUtils {

  @SneakyThrows
  public static void copyStream(InputStream inputStream, OutputStream outputStream) {

    byte[] buffer = new byte[1024];

    while (true) {

      int count = inputStream.read(buffer);
      if (count < 0) {
        return;
      }

      outputStream.write(buffer, 0, count);

    }

  }

}
