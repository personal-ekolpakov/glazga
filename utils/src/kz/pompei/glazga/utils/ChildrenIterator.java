package kz.pompei.glazga.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.ObjIntConsumer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ChildrenIterator<Node> implements Iterator<Node> {
  private final @NonNull List<Node>                 roots;
  private final @NonNull Function<Node, List<Node>> childrenExtractor;
  private final @NonNull ObjIntConsumer<Node>       levelSetter;

  int parentIndex = 0;
  final List<Node>    parents     = new ArrayList<>();
  final List<Integer> lastIndexes = new ArrayList<>();

  @Override
  public boolean hasNext() {
    return parents.size() > 0 || parentIndex < roots.size();
  }

  @Override
  public Node next() {
    if (parents.isEmpty()) {
      final Node ret = roots.get(parentIndex++);
      levelSetter.accept(ret, 0);

      final List<Node> retChildren = childrenExtractor.apply(ret);
      if (retChildren.size() > 0) {
        lastIndexes.add(0);
        parents.add(ret);

        return ret;
      }
      return ret;
    }

    final Node       lastParent         = parents.get(parents.size() - 1);
    final int        lastIndex          = lastIndexes.get(lastIndexes.size() - 1);
    final List<Node> lastParentChildren = childrenExtractor.apply(lastParent);
    final Node       ret                = lastParentChildren.get(lastIndex);

    levelSetter.accept(ret, parents.size());

    {
      final List<Node> retChildren = childrenExtractor.apply(ret);
      if (retChildren.size() > 0) {
        lastIndexes.add(0);
        parents.add(ret);
        return ret;
      }
    }

    while (parents.size() > 0) {
      final Node       lastParent1         = parents.get(parents.size() - 1);
      final List<Node> lastParentChildren1 = childrenExtractor.apply(lastParent1);
      final int        nextLastIndex       = lastIndexes.get(lastIndexes.size() - 1) + 1;
      if (nextLastIndex < lastParentChildren1.size()) {
        lastIndexes.set(lastIndexes.size() - 1, nextLastIndex);
        return ret;
      }

      parents.remove(parents.size() - 1);
      lastIndexes.remove(lastIndexes.size() - 1);
    }

    return ret;
  }
}
