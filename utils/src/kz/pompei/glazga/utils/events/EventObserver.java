package kz.pompei.glazga.utils.events;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.NonNull;

public class EventObserver<Event> {

  private final ConcurrentHashMap<Integer, EventListener<Event>> map = new ConcurrentHashMap<>();

  private final AtomicInteger uniqueGenerator = new AtomicInteger(1);

  public @NonNull Disconnector add(EventListener<Event> listener) {
    if (listener == null) {
      return () -> {};
    }
    int id = uniqueGenerator.getAndIncrement();
    map.put(id, listener);
    return () -> map.remove(id);
  }

  public void fire(Event event) {
    List<Integer> ids = map.keySet().stream().sorted().toList();
    for (final Integer id : ids) {
      EventListener<Event> listener = map.get(id);
      try {
        if (!listener.handle(event)) {
          return;
        }
      } catch (Exception e) {
        throw e instanceof RuntimeException re ? re : new RuntimeException("8urW8ghKHH :: " + e.getMessage(), e);
      }
    }
  }

}
