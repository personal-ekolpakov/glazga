package kz.pompei.glazga.utils.events;

public interface Disconnector {
  @SuppressWarnings("unused")
  void disconnect();
}
