package kz.pompei.glazga.utils.events;

import java.awt.event.MouseEvent;

public class MouseKeys {
  private MouseKeys() {}

  public static MouseKeys mouseKeys() {
    return new MouseKeys();
  }

  protected boolean ctrl = false, shift = false;

  public MouseKeys ctrl() {
    ctrl = true;
    return this;
  }

  public MouseKeys shift() {
    shift = true;
    return this;
  }

  public MouseKeys free() {
    ctrl = shift = false;
    return this;
  }

  public boolean isMiddleDown(MouseEvent e) {
    return e.isControlDown() == ctrl && e.isShiftDown() == shift && e.getButton() == MouseEvent.BUTTON2;
  }

  public boolean isLeftDown(MouseEvent e) {
    return isDown(e) && e.getButton() == MouseEvent.BUTTON1;
  }

  public boolean isRightDown(MouseEvent e) {
    return isDown(e) && e.getButton() == MouseEvent.BUTTON3;
  }

  public boolean isDown(MouseEvent e) {
    return e.isControlDown() == ctrl && e.isShiftDown() == shift;
  }
}
