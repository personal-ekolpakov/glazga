package kz.pompei.glazga.utils.events;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class MouseEvents implements MouseListener, MouseMotionListener, MouseWheelListener {
  public final EventObserver<MouseEvent> clicked = new EventObserver<>();

  @Override
  public void mouseClicked(MouseEvent e) {
    clicked.fire(e);
  }

  public final EventObserver<MouseEvent> pressed = new EventObserver<>();

  @Override
  public void mousePressed(MouseEvent e) {
    pressed.fire(e);
  }

  public final EventObserver<MouseEvent> released = new EventObserver<>();

  @Override
  public void mouseReleased(MouseEvent e) {
    released.fire(e);
  }

  public final EventObserver<MouseEvent> entered = new EventObserver<>();

  @Override
  public void mouseEntered(MouseEvent e) {
    entered.fire(e);
  }

  public final EventObserver<MouseEvent> exited = new EventObserver<>();

  @Override
  public void mouseExited(MouseEvent e) {
    exited.fire(e);
  }

  public final EventObserver<MouseEvent> dragged = new EventObserver<>();

  @Override
  public void mouseDragged(MouseEvent e) {
    dragged.fire(e);
  }

  public final EventObserver<MouseEvent> moved = new EventObserver<>();

  @Override
  public void mouseMoved(MouseEvent e) {
    moved.fire(e);
  }

  public final EventObserver<MouseWheelEvent> wheelMoved = new EventObserver<>();

  @Override
  public void mouseWheelMoved(MouseWheelEvent e) {
    wheelMoved.fire(e);
  }
}
