package kz.pompei.glazga.utils.events;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.NonNull;

public class HandlerObserver<Model> {

  private final ConcurrentHashMap<Integer, Handler<Model>> map = new ConcurrentHashMap<>();

  private final AtomicInteger uniqueGenerator = new AtomicInteger(1);

  public @NonNull Disconnector add(Handler<Model> handler) {
    if (handler == null) {
      return () -> {};
    }
    int id = uniqueGenerator.getAndIncrement();
    map.put(id, handler);
    return () -> map.remove(id);
  }

  public void fire(Model event) {
    for (final Handler<Model> handler : map.values()) {
      try {
        handler.handle(event);
      } catch (Exception e) {
        throw e instanceof RuntimeException re ? re : new RuntimeException("OiJLkUUkMn :: " + e.getMessage(), e);
      }
    }
  }

}
