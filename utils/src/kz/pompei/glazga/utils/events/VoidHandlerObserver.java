package kz.pompei.glazga.utils.events;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.NonNull;

public class VoidHandlerObserver {

  private final ConcurrentHashMap<Integer, VoidHandler> map = new ConcurrentHashMap<>();

  private final AtomicInteger uniqueGenerator = new AtomicInteger(1);

  public @NonNull Disconnector add(VoidHandler handler) {
    if (handler == null) {
      return () -> {};
    }
    int id = uniqueGenerator.getAndIncrement();
    map.put(id, handler);
    return () -> map.remove(id);
  }

  public void fire() {
    List<Integer> ids = map.keySet().stream().sorted().toList();
    for (final Integer id : ids) {
      VoidHandler listener = map.get(id);
      try {
        listener.handle();
      } catch (Exception e) {
        throw e instanceof RuntimeException re ? re : new RuntimeException("CO9OX196Kf :: " + e.getMessage(), e);
      }
    }
  }
}
