package kz.pompei.glazga.utils.events;

public interface VoidHandler {
  void handle() throws Exception;
}
