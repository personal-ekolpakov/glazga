package kz.pompei.glazga.utils.events;

public interface Handler<T> {

  void handle(T t) throws Exception;

}
