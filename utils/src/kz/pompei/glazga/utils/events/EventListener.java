package kz.pompei.glazga.utils.events;

public interface EventListener<Event> {

  /**
   * Handle event
   *
   * @param event The event to handle
   * @return following handle. If true, then next handles will be to handle. false - next handle will not be handled
   * @throws Exception any exception in handle. Converts to RuntimeException
   */
  boolean handle(Event event) throws Exception;

}
