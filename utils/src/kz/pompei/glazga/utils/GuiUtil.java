package kz.pompei.glazga.utils;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.font.LineMetrics;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import kz.pompei.glazga.utils.events.VoidHandler;
import lombok.NonNull;

public class GuiUtil {

  public static @NonNull Graphics2D applyHints(@NonNull Graphics g) {
    Graphics2D g2 = (Graphics2D) g;
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    return g2;
  }

  public static Height textHeight(String text, @NonNull Font font) {

    font.getBaselineFor('c');

    BufferedImage    img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
    final Graphics2D g   = applyHints(img.createGraphics());

    g.setFont(font);

    TextLayout tl;

    font.getStringBounds(text, g.getFontRenderContext());

    final LineMetrics lineMetrics = font.getLineMetrics(text, g.getFontRenderContext());


    g.dispose();

    return null;
  }

  public static void bindKeyAction(@NonNull JComponent component, @NonNull KeyStroke keyStroke, @NonNull VoidHandler voidHandler) {
    String id = RndUtil.strEng(13);


    InputMap  inputMap  = component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    ActionMap actionMap = component.getActionMap();
    KeyStroke space     = KeyStroke.getKeyStroke("SPACE");
    inputMap.put(space, id);
    actionMap.put(id, new AbstractAction() {
      @Override public void actionPerformed(ActionEvent e) {
        try {
          voidHandler.handle();
        } catch (Exception ex) {
          throw ex instanceof RuntimeException r ? r : new RuntimeException(ex);
        }
      }
    });

  }

  public static BufferedImage createGrayImage(@NonNull Image colorImage, float graying) {
    int width  = colorImage.getWidth(null);
    int height = colorImage.getHeight(null);

    final BufferedImage source;
    if (colorImage instanceof BufferedImage x) {
      source = x;
    } else {
      source = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
      Graphics2D g = source.createGraphics();
      g.drawImage(colorImage, 0, 0, null);
      g.dispose();
    }

    BufferedImage grayImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {

        int color = source.getRGB(x, y);

        int alpha = (color >> 24) & 0xff;
        int red   = (color >> 16) & 0xff;
        int green = (color >> 8) & 0xff;
        int blue  = color & 0xff;

        int grayLevel = (int) (0.299 * red + 0.587 * green + 0.114 * blue);

        int newAlpha = Math.round(alpha * graying);

        int rgb = (newAlpha << 24) | (grayLevel << 16) | (grayLevel << 8) | grayLevel;

        grayImage.setRGB(x, y, rgb);

      }
    }

    return grayImage;
  }

}
