package kz.pompei.glazga.utils;

import java.awt.Color;

public class ColorUtil {

  public static Color strToColor(String str) {
    if (str == null) {
      return null;
    }
    String[] split = str.trim().split("\\s+");
    if (split.length != 3) {
      return null;
    }
    try {
      return new Color(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
    } catch (Exception ignore) {
      return null;
    }
  }

}
