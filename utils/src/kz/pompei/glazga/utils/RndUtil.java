package kz.pompei.glazga.utils;

import java.security.SecureRandom;
import java.util.Random;
import lombok.NonNull;

public class RndUtil {


  @SuppressWarnings("SpellCheckingInspection")
  private static final String ENG = "abcdefghijklmnopqrstuvwxyz";
  private static final String DEG = "0123456789";

  private static final char[] ENG_DEC   = (ENG.toLowerCase() + ENG.toUpperCase() + DEG).toCharArray();
  private static final char[] ENG_DEC$_ = (ENG.toLowerCase() + ENG.toUpperCase() + DEG + "$_").toCharArray();

  public static final Random RND = new SecureRandom();

  private static @NonNull String generateRndStr(Random rnd, char[] chars, int resultLength) {
    char[] array = new char[resultLength];

    int charCount = chars.length;

    for (int i = 0; i < resultLength; i++) {
      array[i] = chars[rnd.nextInt(charCount)];
    }

    return new String(array);
  }

  public static @NonNull String strEng(int len) {
    return generateRndStr(RND, ENG_DEC, len);
  }

  public static @NonNull String strEng$_(int len) {
    return generateRndStr(RND, ENG_DEC$_, len);
  }

  public static @NonNull String newId() {
    return strEng$_(16);
  }

}
