package kz.pompei.glazga.utils;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import kz.pompei.glazga.utils.events.VoidHandler;
import lombok.NonNull;
import lombok.SneakyThrows;

public class JDialogUtil {
  public static void borders(@NonNull JComponent component, int top, int left, int bottom, int right) {
    Border paddingBorder = new EmptyBorder(top, left, bottom, right);
    component.setBorder(BorderFactory.createCompoundBorder(component.getBorder(), paddingBorder));
  }

  public static @NonNull DocumentListener handlerToDocumentListenerOver(VoidHandler handler) {
    return new DocumentListener() {
      @SneakyThrows @Override public void insertUpdate(DocumentEvent e) {
        handler.handle();
      }

      @SneakyThrows @Override public void removeUpdate(DocumentEvent e) {
        handler.handle();
      }

      @SneakyThrows @Override public void changedUpdate(DocumentEvent e) {
        handler.handle();
      }
    };
  }

}
