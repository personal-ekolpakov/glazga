package kz.pompei.glazga.utils;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import lombok.NonNull;
import lombok.SneakyThrows;

public class SizeLocationSaver {

  private final Path rootDir;

  private SizeLocationSaver(Path rootDir) {
    this.rootDir = rootDir;
  }

  public static SizeLocationSaver into(Path rootDir) {
    return new SizeLocationSaver(rootDir);
  }

  private static abstract class MyWinEvents extends WindowAdapter implements ComponentListener, WindowStateListener {}

  public void overJFrame(String fileNamePrefix, @NonNull Window window, boolean reloadingSize) {
    Path fileSiz = rootDir.resolve(fileNamePrefix + ".siz.txt");
    Path fileLoc = rootDir.resolve(fileNamePrefix + ".loc.txt");

    AtomicBoolean              threadSaveWork = new AtomicBoolean(true);
    AtomicReference<Point>     currentPos     = new AtomicReference<>();
    AtomicReference<Dimension> currentSize    = new AtomicReference<>();
    Thread threadSave = new Thread() {

      Point     lastSavePos  = null;
      Dimension lastSaveSize = null;

      @Override
      public void run() {
        boolean first = true;
        while (threadSaveWork.get()) {
          try {
            //noinspection BusyWait
            Thread.sleep(first ? 1000 : 200);
          } catch (InterruptedException e) {
            return;
          }

          first = false;

          savePos();

          if (reloadingSize) {
            saveSize();
          }

        }

        savePos();

        if (reloadingSize) {
          saveSize();
        }
      }

      @SneakyThrows
      private void savePos() {
        Point curPos = currentPos.get();
        if (curPos == null) {
          return;
        }

        if (lastSavePos != null && lastSavePos.equals(curPos)) {
          return;
        }
        lastSavePos = curPos;

        String content = curPos.x + " " + curPos.y;
        fileLoc.toFile().getParentFile().mkdirs();
        Files.writeString(fileLoc, content, StandardCharsets.UTF_8);
      }

      @SneakyThrows
      private void saveSize() {
        Dimension curSize = currentSize.get();
        if (curSize == null) {
          return;
        }

        if (lastSaveSize != null && lastSaveSize.equals(curSize)) {
          return;
        }
        lastSaveSize = curSize;

        String content = curSize.width + " " + curSize.height;
        fileLoc.toFile().getParentFile().mkdirs();
        Files.writeString(fileSiz, content, StandardCharsets.UTF_8);
      }
    };

    MyWinEvents myWinEvents = new MyWinEvents() {
      boolean ignoreSave   = false;
      boolean firstShow    = true;
      int     skipSaveMove = 3;

      @Override
      public void componentResized(ComponentEvent e) {
        if (ignoreSave) {
          return;
        }
        currentSize.set(e.getComponent().getSize());
      }

      @Override
      @SneakyThrows
      public void componentMoved(ComponentEvent e) {
        if (ignoreSave) {
          return;
        }
        if (skipSaveMove > 0) {
          skipSaveMove--;
          return;
        }

        currentPos.set(e.getComponent().getLocation());
      }

      @Override
      @SneakyThrows
      public void componentShown(ComponentEvent e) {
        ignoreSave = false;

        if (firstShow) {
          firstShow    = false;
          skipSaveMove = 3;

          if (Files.exists(fileLoc)) {
            String   content = Files.readString(fileLoc, StandardCharsets.UTF_8);
            String[] split   = content.split("\\s+");
            if (split.length == 2) {
              try {
                int x = Integer.parseInt(split[0]);
                int y = Integer.parseInt(split[1]);
                window.setLocation(x, y);
              } catch (NumberFormatException ignore) {
                //ignore
              }
            }
          }
          if (reloadingSize && Files.exists(fileSiz)) {
            String   content = Files.readString(fileSiz, StandardCharsets.UTF_8);
            String[] split   = content.split("\\s+");
            if (split.length == 2) {
              try {
                int width  = Integer.parseInt(split[0]);
                int height = Integer.parseInt(split[1]);
                window.setSize(width, height);
              } catch (NumberFormatException ignore) {
                //ignore
              }
            }
          }

          return;
        }
      }

      @Override
      public void componentHidden(ComponentEvent e) {
        ignoreSave = true;
      }

      @Override
      public void windowStateChanged(WindowEvent e) {
        ignoreSave = e.getNewState() != 0;
      }

      @Override
      public void windowClosing(WindowEvent e) {
        threadSaveWork.set(false);
        threadSave.interrupt();
        try {
          threadSave.join();
        } catch (InterruptedException ex) {
          // ignore
        }
        window.dispose();
      }
    };

    window.addComponentListener(myWinEvents);
    window.addWindowStateListener(myWinEvents);
    window.addWindowListener(myWinEvents);
    threadSave.start();

  }

}
