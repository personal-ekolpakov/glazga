package kz.pompei.glazga.utils;

import java.util.stream.Stream;

public class IsInstance<T> {
  private final Class<T> aClass;

  private IsInstance(Class<T> aClass) {
    this.aClass = aClass;
  }

  public static <T> IsInstance<T> on(Class<T> aClass) {
    return new IsInstance<>(aClass);
  }

  @SuppressWarnings("unchecked")
  public Stream<T> of(Object object) {
    return aClass.isInstance(object) ? Stream.of((T) object) : Stream.of();
  }

}
