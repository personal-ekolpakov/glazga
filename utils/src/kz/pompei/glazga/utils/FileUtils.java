package kz.pompei.glazga.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import lombok.NonNull;
import lombok.SneakyThrows;

import static java.nio.charset.StandardCharsets.UTF_8;

public class FileUtils {

  @SneakyThrows
  public static void removeFile(Path fileOrDir) {
    Files.walkFileTree(fileOrDir, new FileVisitor<>() {
      @Override
      public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        Files.delete(file);
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult visitFileFailed(Path file, IOException exc) {
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        Files.delete(dir);
        return FileVisitResult.CONTINUE;
      }
    });
  }

  public static String extractExt(@NonNull File file) {
    return extractExt(file.getName());
  }

  public static String extractExt(@NonNull String name) {
    int idx = name.lastIndexOf('.');
    return idx < 0
      ? null
      : name.substring(idx + 1);
  }

  @SneakyThrows
  public static Date fileCreatedAt(@NonNull Path file) {

    BasicFileAttributes bfa = Files.readAttributes(file, BasicFileAttributes.class);

    return new Date(bfa.creationTime().toMillis());

  }

  @SneakyThrows
  public static List<Path> scanDir(Path dir, Predicate<Path> fileFilter) {

    List<Path> ret = new ArrayList<>();

    Files.walkFileTree(dir, new FileVisitor<>() {
      @Override
      public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        if (file.toFile().getName().endsWith(".xml")) {
          ret.add(file);
        }


        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult visitFileFailed(Path file, IOException exc) {
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        return FileVisitResult.CONTINUE;
      }
    });

    return ret;
  }

  public static InputStream strToInputStream(@NonNull String str) {
    return new ByteArrayInputStream(str.getBytes(UTF_8));
  }

  @SneakyThrows
  public static @NonNull RunResult runCmd(Path workingDir, List<String> command) {
    Path out = File.createTempFile("runCmd-out", ".run").toPath();
    Path err = File.createTempFile("runCmd-err", ".run").toPath();
    try {

      Process process = new ProcessBuilder(command)
        .directory(workingDir.toFile())
        .redirectOutput(out.toFile())
        .redirectError(err.toFile())
        .start();

      {
        RunResult ret = new RunResult();
        ret.exitCode = process.waitFor();

        ret.out = Files.exists(out) ? Files.readString(out, UTF_8) : null;
        ret.err = Files.exists(out) ? Files.readString(err, UTF_8) : null;

        return ret;
      }
    } finally {
      out.toFile().delete();
      err.toFile().delete();
    }
  }
}
