package kz.pompei.glazga.utils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;
import lombok.NonNull;
import lombok.SneakyThrows;

public class ReflectUtil {
  @SneakyThrows
  public static <T> T getField(@NonNull Object object, @NonNull String fieldName) {
    Field field = object.getClass().getDeclaredField(fieldName);
    field.setAccessible(true);
    //noinspection unchecked
    return (T) field.get(object);
  }

  public static Type mapTypeOf(@NonNull Type keyType, @NonNull Type valueType) {
    return new ParameterizedType() {
      @Override public Type[] getActualTypeArguments() {
        return new Type[]{keyType, valueType};
      }

      @Override public Type getRawType() {
        return Map.class;
      }

      @Override public Type getOwnerType() {
        return null;
      }
    };
  }
}
