package kz.pompei.glazga.utils.err;

import java.sql.SQLException;
import lombok.NonNull;
import lombok.SneakyThrows;

import static kz.pompei.glazga.utils.StrUtil.extractFirstKav;

public class SqlErrorUtil {
  @SneakyThrows public static String extractSqlState(Throwable error) {

    if (error instanceof SqlStateError se) {
      return se.sqlState;
    }

    final Class<?> pSqlException;
    try {
      pSqlException = Class.forName("org.postgresql.util.PSQLException");
    } catch (ClassNotFoundException e) {
      return null;
    }

    if (pSqlException.isInstance(error)) {
      return (String) pSqlException.getMethod("getSQLState").invoke(error);
    }

    return null;
  }

  public static @NonNull RuntimeException convertError(@NonNull Throwable e) {

    if (e instanceof DbError de) {
      return de;
    }

    {
      String sqlState = extractSqlState(e);
      if (sqlState != null) {
        switch (sqlState) {

          case "42P01" -> {
            String relation = extractFirstKav(e.getMessage());
            return new SqlNoRelation(sqlState, relation, e);
          }

          case "3D000" -> {
            String dbName = extractFirstKav(e.getMessage());
            return new SqlNoDb(sqlState, dbName, e);
          }

          case "42704" -> {
            String dbName = extractFirstKav(e.getMessage());
            return new SqlNoType(sqlState, dbName, e);
          }

          default -> {
            return new SqlStateError(sqlState, e);
          }
        }


      }
    }

    if (e instanceof SQLException se) {
      return new SqlError("IaQkwXTHXj", e);
    }

    return e instanceof RuntimeException re ? re : new RuntimeException(e);
  }
}
