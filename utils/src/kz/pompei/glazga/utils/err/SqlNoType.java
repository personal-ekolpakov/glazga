package kz.pompei.glazga.utils.err;

public class SqlNoType extends SqlStateError {
  public final String type;

  public SqlNoType(String sqlState, String type, Throwable e) {
    super(sqlState, "type = `" + type + "`, sqlState = `" + sqlState + "`", e);
    this.type = type;
  }
}
