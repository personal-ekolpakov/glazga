package kz.pompei.glazga.utils.err;

public class SqlStateError extends SqlError {
  public final String sqlState;

  public SqlStateError(String sqlState, Throwable e) {
    super("sqlState = `" + sqlState + "` : " + e.getMessage(), e);
    this.sqlState = sqlState;
  }

  protected SqlStateError(String sqlState, String message, Throwable e) {
    super(message, e);
    this.sqlState = sqlState;
  }
}
