package kz.pompei.glazga.utils.err;

public class TooManyResults extends DbError {
  public TooManyResults(String message) {
    super(message);
  }
}
