package kz.pompei.glazga.utils.err;

public class SqlNoDb extends SqlStateError {
  public final String dbName;

  public SqlNoDb(String sqlState, String dbName, Throwable e) {
    super(sqlState, "dbName = `" + dbName + "`, sqlState = `" + sqlState + "`", e);
    this.dbName = dbName;
  }
}
