package kz.pompei.glazga.utils.err;

public class NoAnyResult extends DbError {
  public NoAnyResult(String message) {
    super(message);
  }
}
