package kz.pompei.glazga.utils.err;

public class SqlError extends DbError{
  public SqlError(String message) {
    super(message);
  }

  public SqlError(String message, Throwable cause) {
    super(message, cause);
  }
}
