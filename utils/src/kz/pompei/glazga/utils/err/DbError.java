package kz.pompei.glazga.utils.err;

public class DbError extends RuntimeException {
  public DbError(String message) {
    super(message);
  }

  public DbError(String message, Throwable cause) {
    super(message, cause);
  }
}
