package kz.pompei.glazga.utils.err;

public class SqlNoRelation extends SqlStateError {
  public final String relation;

  public SqlNoRelation(String sqlState, String relation, Throwable e) {
    super(sqlState, "relation = `" + relation + "`, sqlState = `" + sqlState + "`", e);
    this.relation = relation;
  }
}
