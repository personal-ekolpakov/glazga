package kz.pompei.glazga.utils.err;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class ErrFilter {
  private final Throwable    error;
  private final List<Ignore> ignoreList = new ArrayList<>();

  private interface Ignore {
    boolean test();

    String returnValue();
  }

  public boolean isPasswordFailed() {
    String sqlState = SqlErrorUtil.extractSqlState(error);
    return "28P01".equals(sqlState);
  }

  public boolean isPermissionDenied() {
    String sqlState = SqlErrorUtil.extractSqlState(error);
    return "42501".equals(sqlState);
  }

  public boolean isNoDb() {
    if (error instanceof SqlNoDb) {
      return true;
    }
    String sqlState = SqlErrorUtil.extractSqlState(error);
    return "3D000".equals(sqlState);
  }

  public boolean isUserAlreadyExists() {
    String sqlState = SqlErrorUtil.extractSqlState(error);
    return "42710".equals(sqlState);
  }

  public boolean isNoType() {
    if (error instanceof SqlNoType) {
      return true;
    }
    String sqlState = SqlErrorUtil.extractSqlState(error);
    return "42704".equals(sqlState);
  }


  private void appendIgnore(boolean ignore, String returnValue, BooleanSupplier checker) {
    if (ignore) {
      ignoreList.add(new Ignore() {
        @Override public boolean test() {
          return checker.getAsBoolean();
        }

        @Override public String returnValue() {
          return returnValue;
        }
      });
    }
  }

  public ErrFilter ignorePasswordFailed(boolean ignore, String returnValue) {
    appendIgnore(ignore, returnValue, this::isPasswordFailed);
    return this;
  }

  public ErrFilter ignoreUserAlreadyExists(boolean ignore, String returnValue) {
    appendIgnore(ignore, returnValue, this::isUserAlreadyExists);
    return this;
  }

  public String check(String placeId) {

    for (final Ignore ignore : ignoreList) {
      if (ignore.test()) {
        return ignore.returnValue();
      }
    }

    throw new RuntimeException(placeId + " :: ErrFilter", error);
  }

  public ErrFilter ignorePermissionDenied(boolean ignore, String returnValue) {
    appendIgnore(ignore, returnValue, this::isPermissionDenied);
    return this;
  }

  public ErrFilter ignoreNoDb(boolean ignore, String returnValue) {
    appendIgnore(ignore, returnValue, this::isNoDb);
    return this;
  }

  public ErrFilter ignoreNoType(boolean ignore, String returnValue) {
    appendIgnore(ignore, returnValue, this::isNoType);
    return this;
  }
}
