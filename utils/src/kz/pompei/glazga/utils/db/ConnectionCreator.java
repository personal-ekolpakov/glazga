package kz.pompei.glazga.utils.db;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionCreator {

  Connection create() throws SQLException;

}
