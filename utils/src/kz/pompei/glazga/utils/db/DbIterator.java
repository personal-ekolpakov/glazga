package kz.pompei.glazga.utils.db;

import java.util.Iterator;

public interface DbIterator<E> extends Iterator<E>, AutoCloseable {

  @Override
  void close();

}
