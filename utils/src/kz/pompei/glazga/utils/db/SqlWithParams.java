package kz.pompei.glazga.utils.db;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import static java.util.Collections.unmodifiableMap;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SqlWithParams {

  public final  String                     sql;
  public final  List<Object>               paramList;
  private final Map<String, List<Integer>> paramIndexes;

  public boolean throwInAbsentParam = false;

  public static SqlWithParams of(@NonNull String sql, Map<String, Object> params) {

    StringBuilder              res          = new StringBuilder();
    int                        position     = 0;
    List<Object>               paramList    = new ArrayList<>();
    Map<String, List<Integer>> paramIndexes = new HashMap<>();

    while (position < sql.length()) {

      int idx1 = sql.indexOf('{', position);
      if (idx1 < 0) {
        break;
      }

      int idx2 = sql.indexOf('}', idx1);
      if (idx2 < 0) {
        break;
      }

      res.append(sql, position, idx1);

      String  keyStr     = sql.substring(idx1 + 1, idx2).trim();
      boolean replace    = keyStr.startsWith(".");
      String  key        = replace ? keyStr.substring(1).trim() : keyStr;
      Object  paramValue = params == null ? null : params.get(key);

      if (replace) {
        if (paramValue != null) {
          res.append(paramValue);
        }
      } else {
        res.append('?');
        paramIndexes.computeIfAbsent(key, k -> new ArrayList<>()).add(paramList.size());
        paramList.add(paramValue);
      }

      position = idx2 + 1;
    }

    if (position < sql.length()) {
      res.append(sql, position, sql.length());
    }

    return new SqlWithParams(res.toString(), paramList, unmodifiableMap(paramIndexes));
  }

  @SuppressWarnings("UnusedReturnValue")
  public SqlWithParams param(@NonNull String paramName, Object paramValue) {
    List<Integer> indexes = paramIndexes.get(paramName);
    if (indexes == null) {
      if (throwInAbsentParam) {
        throw new RuntimeException("vuNF8h8MUr :: No Param " + paramName);
      }
      return this;
    }

    for (final Integer index : indexes) {
      paramList.set(index, paramValue);
    }

    return this;
  }

  @SuppressWarnings("UnusedReturnValue")
  public SqlWithParams params(@NonNull Map<String, Object> params) {
    params.forEach(this::param);
    return this;
  }

  @SneakyThrows
  public void copyParamsTo(@NonNull PreparedStatement ps) {
    int index = 1;
    for (final Object param : paramList) {
      ps.setObject(index++, param);
    }
  }
}
