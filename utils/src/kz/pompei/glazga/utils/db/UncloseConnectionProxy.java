package kz.pompei.glazga.utils.db;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.sql.Connection;

public class UncloseConnectionProxy {

  public static Connection over(Connection connection) {
    return (Connection)
      Proxy.newProxyInstance(Connection.class.getClassLoader(), new Class<?>[]{Connection.class}, (proxy, method, args) -> {

        String methodName = method.getName();

        if ("close".equals(methodName)) {
          return null;
        }

        if ("toString".equals(methodName) && method.getParameterTypes().length == 0) {
          return "3JnEvZ8D7I :: PROXY@" + System.identityHashCode(proxy);
        }

        try {
          return method.invoke(connection, args);
        } catch (InvocationTargetException e) {
          Throwable cause = e.getCause();
          if (cause instanceof RuntimeException re) {
            throw re;
          }
          throw new RuntimeException(cause);
        }

      });
  }

}
