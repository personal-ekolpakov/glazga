package kz.pompei.glazga.utils.db;

import java.sql.Connection;
import java.sql.SQLException;
import kz.pompei.glazga.utils.err.SqlErrorUtil;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.XSlf4j;

@XSlf4j
@RequiredArgsConstructor
public class Db implements QuerySource {
  private final ConnectionCreator connectionCreator;

  @Override
  public DbQuery query() {
    return new DbQueryImpl(connectionCreator);
  }

  @SuppressWarnings("UnusedReturnValue")
  public <Result> Result execTransaction(@NonNull UsingQuerySource<Result> usingQuerySource) {

    Connection[] originConnection = new Connection[]{null};

    QuerySource querySource = createQuerySource(originConnection, c -> c.setAutoCommit(false));

    try {
      Result result = usingQuerySource.operations(querySource);
      {
        Connection c = originConnection[0];
        if (c != null) {
          c.commit();
        }
      }
      return result;
    } catch (Throwable e) {
      Connection c = originConnection[0];
      if (c != null) {
        try {
          c.rollback();
        } catch (SQLException ex) {
          log.error("K6fgfHH2zw :: Rollback error", ex);
        }
      }
      throw SqlErrorUtil.convertError(e);
    } finally {
      Connection c = originConnection[0];
      if (c != null) {
        try {
          c.setAutoCommit(true);
          c.close();
        } catch (SQLException ex) {
          log.error("XxvDyWHUfX :: Close error", ex);
        }
      }
    }

  }


  @SuppressWarnings("UnusedReturnValue")
  public <Result> Result execAutoCommiting(@NonNull UsingQuerySource<Result> usingQuerySource) {

    Connection[] originConnection = new Connection[]{null};

    QuerySource querySource = createQuerySource(originConnection, c -> c.setAutoCommit(true));

    try {
      return usingQuerySource.operations(querySource);
    } catch (Throwable e) {
      throw SqlErrorUtil.convertError(e);
    } finally {
      Connection c = originConnection[0];
      if (c != null) {
        try {
          c.setAutoCommit(true);
          c.close();
        } catch (SQLException e) {
          log.error("wbnFtF6cvC :: Error closing auto-commit connection", e);
        }
      }
    }

  }

  private @NonNull QuerySource createQuerySource(Connection[] originConnection,
                                                 ConsumerThr<Connection, SQLException> prepareConnection) {

    ConnectionCreator trConnCreator = new ConnectionCreator() {

      Connection proxyConnection = null;

      @Override
      public Connection create() throws SQLException {
        {
          Connection x = proxyConnection;
          if (x != null) {
            return x;
          }
        }

        Connection connection = connectionCreator.create();
        prepareConnection.accept(connection);
        originConnection[0] = connection;
        return proxyConnection = UncloseConnectionProxy.over(connection);
      }
    };

    return () -> new DbQueryImpl(trConnCreator);
  }

  public void update(@NonNull InTransactionVoid inTransaction) {
    execTransaction((UsingQuerySource<Void>) qs -> {
      inTransaction.operations(qs);
      return null;
    });
  }

  public void withConnectionVoid(ConsumerThr<Connection, Exception> overConnection) {
    try (Connection connection = connectionCreator.create()) {
      overConnection.accept(connection);
    } catch (Exception e) {
      throw SqlErrorUtil.convertError(e);
    }
  }

  public <Result> Result withConnection(FunctionThr<Connection, Result, Exception> overConnection) {
    try (Connection connection = connectionCreator.create()) {
      return overConnection.apply(connection);
    } catch (Exception e) {
      throw SqlErrorUtil.convertError(e);
    }
  }
}
