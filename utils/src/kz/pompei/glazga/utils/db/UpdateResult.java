package kz.pompei.glazga.utils.db;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UpdateResult {
  public final          int         updatedCount;
  @SuppressWarnings("ClassEscapesDefinedScope")
  public final @NonNull DbQueryImpl dbQuery;

  public void throwIfNotSingle(@NonNull String placeId) {

    if (updatedCount <= 0) {
      throw new NoAnyUpdate(placeId + " :: No any update while execute sql: " + dbQuery.sql);
    }

    if (updatedCount > 1) {
      throw new TooManyUpdates(placeId + " :: Too many updates = " + updatedCount + " while execute sql: " + dbQuery.sql);
    }

  }
}
