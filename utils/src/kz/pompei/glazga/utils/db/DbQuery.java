package kz.pompei.glazga.utils.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import kz.pompei.glazga.utils.Page;
import lombok.NonNull;

public interface DbQuery {

  DbQuery sql(String sql);

  DbQuery param(String paramName, Object paramValue);

  DbQuery params(Map<String, Object> params);

  DbQuery limit(Integer limit);

  DbQuery offset(Integer offset);

  DbQuery page(Page page);

  <T> Optional<T> one(Class<T> classT);

  <T> Optional<T> any(Class<T> classT);

  <T> List<T> list(Class<T> classT);

  <T> DbIterator<T> iterate(Class<T> classT);

  <Result> DbIterator<Result> iterate(FunctionThr<ResultSet, Result, SQLException> extractor);

  <Result> List<Result> list(FunctionThr<ResultSet, Result, SQLException> extractor);

  <Result> Set<Result> extractSet(FunctionThr<ResultSet, Result, SQLException> extractor);

  @NonNull UpdateResult update();

  @NonNull UpdateResult update(Map<String, Object> params);

  @NonNull UpdateBatched updateBatched();

  <Result> Result one(FunctionThr<ResultSet, Result, SQLException> extractor);

  <Result> Optional<Result> oneOpt(FunctionThr<ResultSet, Result, SQLException> extractor);

  boolean bool();

  String str();

  Optional<String> strOpt();

  DbQuery copy();

  int integer();

  Optional<Integer> integerOpt();

  void foreach(@NonNull ConsumerThr<ResultSet, SQLException> consumer);

}
