package kz.pompei.glazga.utils.db;

public interface QuerySource {

  DbQuery query();

}
