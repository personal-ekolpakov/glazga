package kz.pompei.glazga.utils.db;

import java.util.Map;
import lombok.NonNull;

public interface UpdateBatched extends AutoCloseable {

  UpdateBatched param(@NonNull String paramName, Object paramValue);

  UpdateBatched params(@NonNull Map<String, Object> params);

  UpdateBatched addBatch();

  void executeBatch();

  UpdateBatched commit();

  @Override
  void close();

}
