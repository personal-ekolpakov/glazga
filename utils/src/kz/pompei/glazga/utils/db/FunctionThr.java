package kz.pompei.glazga.utils.db;

public interface FunctionThr<Input, Output, Thr extends Throwable> {

  Output apply(Input input) throws Thr;

}
