package kz.pompei.glazga.utils.db;

import kz.pompei.glazga.utils.err.DbError;

public class TooManyUpdates extends DbError {
  public TooManyUpdates(String message) {
    super(message);
  }
}
