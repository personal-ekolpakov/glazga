package kz.pompei.glazga.utils.db;

public interface ConsumerThr<T, Thr extends Throwable> {

  void accept(T t) throws Thr;

}
