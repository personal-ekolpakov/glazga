package kz.pompei.glazga.utils.db;

import kz.pompei.glazga.utils.err.DbError;

public class NoAnyUpdate extends DbError {
  public NoAnyUpdate(String message) {
    super(message);
  }
}
