package kz.pompei.glazga.utils.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Map;
import lombok.NonNull;
import lombok.SneakyThrows;

public class UpdateBatchedImpl implements UpdateBatched {

  private final Connection        connection;
  private final SqlWithParams     swp;
  private final PreparedStatement ps;

  @SneakyThrows
  public UpdateBatchedImpl(@NonNull Connection connection, @NonNull SqlWithParams swp) {
    this.connection = connection;
    this.swp        = swp;
    connection.setAutoCommit(false);
    ps = connection.prepareStatement(swp.sql);
  }

  @Override
  public UpdateBatched param(@NonNull String paramName, Object paramValue) {
    swp.param(paramName, paramValue);
    return this;
  }

  @Override
  public UpdateBatched params(@NonNull Map<String, Object> params) {
    swp.params(params);
    return this;
  }

  @Override
  @SneakyThrows
  public UpdateBatched addBatch() {
    swp.copyParamsTo(ps);
    ps.addBatch();
    return this;
  }

  @Override
  @SneakyThrows
  public void executeBatch() {
    ps.executeBatch();
  }

  @Override
  @SneakyThrows
  public UpdateBatched commit() {
    connection.commit();
    return this;
  }

  @Override
  @SneakyThrows
  public void close() {
    ps.close();
    connection.setAutoCommit(true);
    connection.close();
  }
}
