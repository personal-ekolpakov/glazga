package kz.pompei.glazga.utils.db;

import lombok.NonNull;

public interface UsingQuerySource<Result> {
  Result operations(@NonNull QuerySource qs) throws Throwable;
}
