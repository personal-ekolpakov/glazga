package kz.pompei.glazga.utils.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.utils.err.NoAnyResult;
import kz.pompei.glazga.utils.err.TooManyResults;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import static kz.pompei.glazga.utils.err.SqlErrorUtil.convertError;

@RequiredArgsConstructor
class DbQueryImpl implements DbQuery {
  private final ConnectionCreator connectionCreator;

  String sql;
  final Map<String, Object> params = new HashMap<>();
  Integer offset = null;
  Integer limit  = null;

  @Override public DbQuery copy() {

    DbQueryImpl ret = new DbQueryImpl(connectionCreator);
    ret.sql = sql;
    ret.params.putAll(params);
    ret.offset = offset;
    ret.limit  = limit;
    return ret;

  }

  @Override public DbQuery sql(String sql) {
    this.sql = sql;
    return this;
  }

  @Override public DbQuery param(String paramName, Object paramValue) {
    params.put(paramName, paramValue);
    return this;
  }

  @Override public DbQuery params(Map<String, Object> params) {
    this.params.putAll(params);
    return this;
  }

  @Override public DbQuery limit(Integer limit) {
    this.limit = limit;
    return this;
  }

  @Override public DbQuery offset(Integer offset) {
    this.offset = offset;
    return this;
  }

  @Override public DbQuery page(Page page) {
    return page == null
      ? offset(null).limit(null)
      : offset(page.offset).limit(page.limit);
  }

  @Override public <T> Optional<T> one(Class<T> classT) {
    try (DbIterator<T> iterator = iterate(classT)) {

      if (!iterator.hasNext()) {
        return Optional.empty();
      }

      T ret = iterator.next();

      if (iterator.hasNext()) {
        throw new TooManyResults("f4LLsmWKaw :: For sql: " + sql);
      }

      return Optional.ofNullable(ret);
    }
  }

  @Override public <T> Optional<T> any(Class<T> classT) {
    try (DbIterator<T> iterator = iterate(classT)) {
      return iterator.hasNext() ? Optional.ofNullable(iterator.next()) : Optional.empty();
    }
  }

  @Override public <T> List<T> list(Class<T> classT) {
    try (DbIterator<T> iterator = iterate(classT)) {
      List<T> ret = new ArrayList<>();
      while (iterator.hasNext()) {
        ret.add(iterator.next());
      }
      return ret;
    }
  }

  @Override public <T> DbIterator<T> iterate(Class<T> classT) {
    return new DbSqlIteratorWithClass<>(classT, connectionCreator, sql, params, offset, limit);
  }

  @Override public <Result> DbIterator<Result> iterate(FunctionThr<ResultSet, Result, SQLException> extractor) {
    return new DbSqlIteratorWithExtractor<>(extractor, connectionCreator, sql, params, offset, limit);
  }

  @Override public <Result> List<Result> list(FunctionThr<ResultSet, Result, SQLException> extractor) {
    List<Result> ret = new ArrayList<>();
    try (DbIterator<Result> iterate = iterate(extractor)) {
      while (iterate.hasNext()) {
        ret.add(iterate.next());
      }
    }
    return ret;
  }

  @Override public void foreach(@NonNull ConsumerThr<ResultSet, SQLException> consumer) {
    try (Connection connection = connectionCreator.create()) {
      var swp = SqlWithParams.of(sql, params);

      //noinspection SqlSourceToSinkFlow
      try (PreparedStatement ps = connection.prepareStatement(swp.sql)) {
        swp.copyParamsTo(ps);
        try (ResultSet rs = ps.executeQuery()) {
          while (rs.next()) {
            consumer.accept(rs);
          }
        }
      }

    } catch (Exception e) {
      throw convertError(e);
    }
  }

  @Override public <Result> Set<Result> extractSet(FunctionThr<ResultSet, Result, SQLException> extractor) {
    Set<Result> ret = new HashSet<>();
    try (DbIterator<Result> iterate = iterate(extractor)) {
      while (iterate.hasNext()) {
        ret.add(iterate.next());
      }
    }
    return ret;
  }

  @Override public @NonNull UpdateResult update() {
    try (Connection connection = connectionCreator.create()) {
      var swp = SqlWithParams.of(sql, params);

      //noinspection SqlSourceToSinkFlow
      try (PreparedStatement ps = connection.prepareStatement(swp.sql)) {
        swp.copyParamsTo(ps);
        return new UpdateResult(ps.executeUpdate(), this);
      }

    } catch (Exception e) {
      throw convertError(e);
    }
  }

  @Override public @NonNull UpdateResult update(Map<String, Object> params) {
    return copy().params(params).update();
  }

  @Override @SneakyThrows public @NonNull UpdateBatched updateBatched() {
    return new UpdateBatchedImpl(connectionCreator.create(), SqlWithParams.of(sql, params));
  }

  @Override @SneakyThrows public boolean bool() {
    return one(rs -> rs.getBoolean(1));
  }

  @Override public String str() {
    return one(rs -> rs.getString(1));
  }

  @Override public Optional<String> strOpt() {
    return oneOpt(rs -> rs.getString(1));
  }

  @Override public int integer() {
    return one(rs -> rs.getInt(1));
  }

  @Override public Optional<Integer> integerOpt() {
    return oneOpt(rs -> rs.getInt(1));
  }

  @Override public <Result> Optional<Result> oneOpt(FunctionThr<ResultSet, Result, SQLException> extractor) {
    final SqlWithParams swp = SqlWithParams.of(sql, params);

    try (Connection connection = connectionCreator.create()) {
      try (PreparedStatement ps = connection.prepareStatement(swp.sql)) {
        swp.copyParamsTo(ps);
        try (ResultSet rs = ps.executeQuery()) {

          if (!rs.next()) {
            return Optional.empty();
          }

          Result result = extractor.apply(rs);

          if (rs.next()) {
            throw new TooManyResults("EGNVbiNH3S :: exactlyOne from sql `" + sql + "`");
          }

          return Optional.of(result);
        }
      }
    } catch (Exception e) {
      throw convertError(e);
    }
  }

  @Override public <Result> Result one(FunctionThr<ResultSet, Result, SQLException> extractor) {
    final SqlWithParams swp = SqlWithParams.of(sql, params);

    try (Connection connection = connectionCreator.create()) {
      try (PreparedStatement ps = connection.prepareStatement(swp.sql)) {
        swp.copyParamsTo(ps);
        try (ResultSet rs = ps.executeQuery()) {

          if (!rs.next()) {
            throw new NoAnyResult("WvG7L5148u :: exactlyOne from sql `" + sql + "`");
          }

          Result result = extractor.apply(rs);

          if (rs.next()) {
            throw new TooManyResults("w5ZtTD9OJz :: exactlyOne from sql `" + sql + "`");
          }

          return result;
        }
      }
    } catch (Exception e) {
      throw convertError(e);
    }

  }

}
