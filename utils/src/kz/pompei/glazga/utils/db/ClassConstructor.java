package kz.pompei.glazga.utils.db;

import java.beans.ConstructorProperties;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static java.util.Collections.unmodifiableList;

@RequiredArgsConstructor
public class ClassConstructor {

  public final Constructor<?> constructor;
  public final List<String>   argFieldNames;

  public static List<ClassConstructor> select(Class<?> aClass) {

    List<ClassConstructor> ret = new ArrayList<>();

    for (final Constructor<?> constructor : aClass.getConstructors()) {

      if (constructor.getParameterTypes().length == 0) {
        ret.add(new ClassConstructor(constructor, List.of()));
        continue;
      }

      ConstructorProperties cProp = constructor.getAnnotation(ConstructorProperties.class);
      if (cProp == null) {
        continue;
      }

      ret.add(new ClassConstructor(constructor, Arrays.stream(cProp.value()).toList()));
    }

    ret.sort(Comparator.comparing(x -> -x.argFieldNames.size()));

    return unmodifiableList(ret);
  }

  public boolean allExistsForConstructor(@NonNull Set<String> availableFieldNames) {
    for (final String argFieldName : argFieldNames) {
      if (!availableFieldNames.contains(argFieldName)) {
        return false;
      }
    }
    return true;
  }

  public boolean existsArgField(String argFieldName) {
    return argFieldNames.contains(argFieldName);
  }
}
