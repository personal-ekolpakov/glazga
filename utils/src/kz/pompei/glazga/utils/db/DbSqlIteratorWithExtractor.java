package kz.pompei.glazga.utils.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import static kz.pompei.glazga.utils.err.SqlErrorUtil.convertError;

@RequiredArgsConstructor
public class DbSqlIteratorWithExtractor<T> implements DbIterator<T> {
  private final FunctionThr<ResultSet, T, SQLException> extractor;
  private final ConnectionCreator                       connectionCreator;
  private final String                                  sql;
  private final Map<String, Object>                     params;
  private final Integer                                 offset;
  private final Integer                                 limit;

  private Connection        connection = null;
  private PreparedStatement ps         = null;
  private ResultSet         rs         = null;

  @Override
  @SneakyThrows
  public void close() {
    {
      ResultSet rs = this.rs;
      if (rs != null) {
        rs.close();
        this.rs = null;
      }
    }

    {
      PreparedStatement ps = this.ps;
      if (ps != null) {
        ps.close();
        this.ps = null;
      }
    }

    {
      Connection connection = this.connection;
      if (connection != null) {
        connection.close();
        this.connection = null;
      }
    }
  }

  @Override
  @SneakyThrows
  public boolean hasNext() {
    if (connection == null) {
      connect();
    }
    return rs.next();
  }

  @SneakyThrows
  private void connect() {
    connection = connectionCreator.create();

    StringBuilder sb = new StringBuilder();
    sb.append(sql);
    {
      Integer limit = this.limit;
      if (limit != null) {
        String keyLimit = findNewKey("limit");
        params.put(keyLimit, limit);
        sb.append(" limit {" + keyLimit + "}");
      }
    }
    {
      Integer offset = this.offset;
      if (offset != null) {
        String keyOffset = findNewKey("offset");
        params.put(keyOffset, offset);
        sb.append(" offset {" + keyOffset + "}");
      }
    }

    var swp = SqlWithParams.of(sb.toString(), params);

    ps = connection.prepareStatement(swp.sql);
    int index = 1;
    for (final Object param : swp.paramList) {
      ps.setObject(index++, param);
    }

    rs = ps.executeQuery();

  }

  private String findNewKey(String preferredKey) {
    if (!params.containsKey(preferredKey)) {
      return preferredKey;
    }

    int i = 2;

    while (true) {
      String newKey = preferredKey + i++;
      if (!params.containsKey(newKey)) {
        return newKey;
      }
    }
  }

  @Override
  public T next() {
    try {
      return extractor.apply(rs);
    } catch (Exception e) {
      throw convertError(e);
    }
  }

}
