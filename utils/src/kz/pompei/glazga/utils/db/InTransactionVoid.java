package kz.pompei.glazga.utils.db;

import lombok.NonNull;

public interface InTransactionVoid {
  void operations(@NonNull QuerySource qs);
}
