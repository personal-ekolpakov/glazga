package kz.pompei.glazga.utils.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.moc.class_meta.ClassMeta;
import kz.pompei.glazga.moc.class_meta.ClassMetaManager;
import kz.pompei.glazga.moc.class_meta.FieldAccessor;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import static kz.pompei.glazga.utils.err.SqlErrorUtil.convertError;

@RequiredArgsConstructor
public class DbSqlIteratorWithClass<T> implements DbIterator<T> {
  private final Class<T>            classT;
  private final ConnectionCreator   connectionCreator;
  private final String              sql;
  private final Map<String, Object> params;
  private final Integer             offset;
  private final Integer             limit;

  private interface FieldMover {
    void moveTo(Object destination);
  }

  private Connection        connection            = null;
  private PreparedStatement ps                    = null;
  private ResultSet         rs                    = null;
  private ClassConstructor  constructor           = null;
  private List<FieldMover>  initMovers            = null;
  private List<Integer>     constructorColIndexes = null;

  private static final ClassMetaManager cmm = new ClassMetaManager();

  @Override
  @SneakyThrows
  public void close() {
    {
      ResultSet rs = this.rs;
      if (rs != null) {
        rs.close();
        this.rs = null;
      }
    }

    {
      PreparedStatement ps = this.ps;
      if (ps != null) {
        ps.close();
        this.ps = null;
      }
    }

    {
      Connection connection = this.connection;
      if (connection != null) {
        connection.close();
        this.connection = null;
      }
    }
  }

  @Override
  @SneakyThrows
  public boolean hasNext() {
    if (connection == null) {
      connect();
    }
    return rs.next();
  }

  @SneakyThrows
  private void connect() {
    connection = connectionCreator.create();

    StringBuilder sb = new StringBuilder();
    sb.append(sql);
    {
      Integer limit = this.limit;
      if (limit != null) {
        String keyLimit = findNewKey("limit");
        params.put(keyLimit, limit);
        sb.append(" limit {" + keyLimit + "}");
      }
    }
    {
      Integer offset = this.offset;
      if (offset != null) {
        String keyOffset = findNewKey("offset");
        params.put(keyOffset, offset);
        sb.append(" offset {" + keyOffset + "}");
      }
    }

    var swp = SqlWithParams.of(sb.toString(), params);

    ps = connection.prepareStatement(swp.sql);
    int index = 1;
    for (final Object param : swp.paramList) {
      ps.setObject(index++, param);
    }

    rs = ps.executeQuery();

  }

  private String findNewKey(String preferredKey) {
    if (!params.containsKey(preferredKey)) {
      return preferredKey;
    }

    int i = 2;

    while (true) {
      String newKey = preferredKey + i++;
      if (!params.containsKey(newKey)) {
        return newKey;
      }
    }
  }

  private void initiateCreatorFromRS() throws SQLException {

    List<String>         rsColNames   = new ArrayList<>();
    Map<String, Integer> rsColIndexes = new HashMap<>();

    ResultSetMetaData metaData = rs.getMetaData();
    for (int i = 1; i <= metaData.getColumnCount(); i++) {
      String columnLabel = metaData.getColumnLabel(i);
      rsColNames.add(columnLabel);
      rsColIndexes.put(columnLabel, i);
    }

    for (final ClassConstructor c : ClassConstructor.select(classT)) {
      if (c.allExistsForConstructor(rsColIndexes.keySet())) {
        constructor = c;
        break;
      }
    }

    if (constructor == null) {
      throw new RuntimeException("hm1jeRRIg8 :: Cannot find constructor in class " + classT + "."
                                 + " Available fields: " + rsColNames);
    }

    constructorColIndexes = new ArrayList<>();
    for (final String argFieldName : constructor.argFieldNames) {
      Integer index = rsColIndexes.get(argFieldName);
      if (index == null) {
        throw new RuntimeException("XVO4piZxPk :: Not found column " + argFieldName + " in sql: " + sql);
      }
      constructorColIndexes.add(index);
    }

    ClassMeta classMeta = cmm.get(classT);

    initMovers = new ArrayList<>();

    for (final String fieldName : classMeta.fieldNames().stream().sorted().toList()) {
      if (constructor.existsArgField(fieldName)) {
        continue;
      }

      Integer rsColIndex = rsColIndexes.get(fieldName);

      if (rsColIndex == null) {
        continue;
      }

      FieldAccessor accessor = classMeta.accessor(fieldName);

      initMovers.add(destination -> {
        try {
          accessor.set(destination, rs.getObject(rsColIndex));
        } catch (Exception e) {
          throw convertError(e);
        }
      });
    }

  }

  @Override
  public T next() {
    try {

      if (constructor == null) {
        initiateCreatorFromRS();
      }

      int      argCount        = constructorColIndexes.size();
      Object[] constructorArgs = new Object[argCount];
      for (int i = 0; i < argCount; i++) {
        constructorArgs[i] = rs.getObject(constructorColIndexes.get(i));
      }

      Object newInstance = constructor.constructor.newInstance(constructorArgs);

      initMovers.forEach(movers -> movers.moveTo(newInstance));

      //noinspection unchecked
      return (T) newInstance;

    } catch (Exception e) {
      throw convertError(e);
    }
  }

}
