package kz.pompei.glazga.utils;

public class RunResult {

  public int exitCode;

  public String out;
  public String err;

}
