package kz.pompei.glazga.utils;

public class NumUtil {
  public static String toLen(int value, int len) {
    final String I      = "" + value;
    final int    length = I.length();
    return length >= len ? I : "0".repeat(len - length) + I;
  }

  public static double parseDoubleDef(String source, double def) {
    try {
      return Double.parseDouble(source);
    } catch (Exception ignore) {
      return def;
    }
  }

  public static int parseIntDef(String str, int defReturn) {
    try {
      return Integer.parseInt(str);
    } catch (Exception ignore) {
      return defReturn;
    }
  }
}
