package kz.pompei.glazga.utils;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Locations {

  private static Path home() {
    return Paths.get(System.getProperty("user.home"));
  }

  public static Path userConfigDir() {
    return home().resolve(".config").resolve("glazga-v3");
  }

  public static Path buildConfig() {
    return Paths.get("build").resolve("config3");
  }

}
