package kz.pompei.glazga.utils;

import java.awt.Graphics2D;
import java.awt.RenderingHints;

public class PaintUtils {
  public static void applyAntialiasing(Graphics2D g) {
    g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                       RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                       RenderingHints.VALUE_ANTIALIAS_ON);
  }
}
