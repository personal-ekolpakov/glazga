package kz.pompei.glazga.utils;

import java.util.ArrayList;
import java.util.List;
import org.testng.annotations.Test;

public class ChildrenIteratorTest extends ChildrenIteratorTestParent {

  @Test
  public void iterate__01() {

    List<Testing> roots = new ArrayList<>();

    roots.add(Testing.of("Node 0"));
    roots.add(Testing.of("Node 1"));
    roots.add(Testing.of("Node 2"));

    roots.get(0).children.add(Testing.of("Node 00"));
    roots.get(0).children.add(Testing.of("Node 01"));
    roots.get(0).children.add(Testing.of("Node 02"));
    roots.get(1).children.add(Testing.of("Node 10"));
    roots.get(1).children.add(Testing.of("Node 11"));
    roots.get(1).children.add(Testing.of("Node 12"));
    roots.get(2).children.add(Testing.of("Node 20"));
    roots.get(2).children.add(Testing.of("Node 21"));
    roots.get(2).children.add(Testing.of("Node 22"));

    roots.get(1).children.get(1).children.add(Testing.of("Node 110"));
    roots.get(1).children.get(1).children.add(Testing.of("Node 111"));
    roots.get(1).children.get(1).children.add(Testing.of("Node 112"));

    roots.get(2).children.get(0).children.add(Testing.of("Node 200"));
    roots.get(2).children.get(0).children.add(Testing.of("Node 201"));

    roots.get(2).children.get(0).children.get(0).children.add(Testing.of("Node 2000"));
    roots.get(2).children.get(0).children.get(0).children.add(Testing.of("Node 2001"));

    roots.get(2).children.get(2).children.add(Testing.of("Node 220"));
    roots.get(2).children.get(2).children.add(Testing.of("Node 221"));
    roots.get(2).children.get(2).children.add(Testing.of("Node 222"));

    assertIt(roots);
  }

  @Test
  public void iterate__02() {

    List<Testing> roots = new ArrayList<>();

    roots.add(Testing.of("Node 0"));
    roots.add(Testing.of("Node 1"));
    roots.add(Testing.of("Node 2"));

    roots.get(0).children.add(Testing.of("Node 00"));
    roots.get(0).children.add(Testing.of("Node 01"));

    assertIt(roots);
  }

  @Test
  public void iterate__03() {

    List<Testing> roots = new ArrayList<>();

    roots.add(Testing.of("Node 1"));
    roots.get(0).children.add(Testing.of("Node 2"));
    roots.get(0).children.get(0).children.add(Testing.of("Node 3"));
    roots.get(0).children.get(0).children.get(0).children.add(Testing.of("Node 4"));

    assertIt(roots);
  }

  @Test
  public void iterate__04() {

    List<Testing> roots = new ArrayList<>();

    roots.add(Testing.of("Node 1"));
    roots.get(0).children.add(Testing.of("Node 2"));
    roots.get(0).children.get(0).children.add(Testing.of("Node 3"));
    roots.get(0).children.get(0).children.get(0).children.add(Testing.of("Node 4"));
    roots.add(Testing.of("Node LAST"));

    assertIt(roots);
  }

  @Test
  public void iterate__05() {

    List<Testing> roots = new ArrayList<>();

    roots.add(Testing.of("Node 1"));
    roots.get(0).children.add(Testing.of("Node 11"));
    roots.get(0).children.get(0).children.add(Testing.of("Node 111"));
    roots.get(0).children.get(0).children.get(0).children.add(Testing.of("Node 1111"));
    roots.get(0).children.add(Testing.of("Node 12"));
    roots.get(0).children.get(1).children.add(Testing.of("Node 121"));
    roots.add(Testing.of("Node 2"));

    assertIt(roots);
  }

}
