package kz.pompei.glazga.utils;

import java.util.Map;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StrUtilTest {

  @Test
  public void applyStrTemplate_002() {

    //
    //
    String str = StrUtil.applyStrTemplate("{}", "Привет, {name}!!", Map.of("name", "Мир"));
    //
    //

    assertThat(str).isEqualTo("Привет, Мир!!");
  }

  @Test
  public void applyStrTemplate_003() {

    String strTemplate = "tst {a1} t2 {a2} t3 {a1} t4 {a3}";
    Map<String, String> params = Map.of(
      "a1", "VA1",
      "a2", "VA2",
      "a3", "VA3"
    );

    //
    //
    String str = StrUtil.applyStrTemplate("{}", strTemplate, params);
    //
    //

    assertThat(str).isEqualTo("tst VA1 t2 VA2 t3 VA1 t4 VA3");
  }

  @Test
  public void applyStrTemplate_004() {

    //
    //
    String str = StrUtil.applyStrTemplate("{}", "{a1}", Map.of("a1", "VA1"));
    //
    //

    assertThat(str).isEqualTo("VA1");
  }

  @Test
  public void applyStrTemplate_005() {

    //
    //
    String str = StrUtil.applyStrTemplate("{}", "{a1} END", Map.of("a1", "VA1"));
    //
    //

    assertThat(str).isEqualTo("VA1 END");
  }

  @Test
  public void applyStrTemplate_006() {

    //
    //
    String str = StrUtil.applyStrTemplate("{}", "BEGIN {a1} END", Map.of());
    //
    //

    assertThat(str).isEqualTo("BEGIN  END");
  }

  @Test
  public void applyStrTemplate_007() {

    //
    //
    String str = StrUtil.applyStrTemplate("{}", "BEGIN {a1} END", Map.of());
    //
    //

    assertThat(str).isEqualTo("BEGIN  END");
  }

  @Test
  public void applyStrTemplate_008() {

    //
    //
    String str = StrUtil.applyStrTemplate("{}", "BEGIN {  a1   } END", Map.of("a1", "WOW"));
    //
    //

    assertThat(str).isEqualTo("BEGIN WOW END");
  }

  @Test
  public void applyStrTemplate_009() {

    //
    //
    String str = StrUtil.applyStrTemplate("{}", "BEGIN {  a1   ", Map.of());
    //
    //

    assertThat(str).isEqualTo("BEGIN ");
  }

  @Test
  public void applyStrTemplate_010() {

    //
    //
    String str = StrUtil.applyStrTemplate("{}", "BEGIN {a1} END, START {  a2   ", Map.of("a1", "SAD"));
    //
    //

    assertThat(str).isEqualTo("BEGIN SAD END, START ");
  }

  @Test
  public void applyStrTemplate_012() {

    //
    //
    String str = StrUtil.applyStrTemplate("{{}}", "Привет, {{name}}!!", Map.of("name", "Мир"));
    //
    //

    assertThat(str).isEqualTo("Привет, Мир!!");
  }

  @Test
  public void applyStrTemplate_013() {

    String strTemplate = "tst {{a1}} t2 {{a2}} t3 {{a1}} t4 {{a3}}";
    Map<String, String> params = Map.of(
      "a1", "VA1",
      "a2", "VA2",
      "a3", "VA3"
    );

    //
    //
    String str = StrUtil.applyStrTemplate("{{}}", strTemplate, params);
    //
    //

    assertThat(str).isEqualTo("tst VA1 t2 VA2 t3 VA1 t4 VA3");
  }

  @Test
  public void applyStrTemplate_014() {

    //
    //
    String str = StrUtil.applyStrTemplate("{{}}", "{{a1}}", Map.of("a1", "VA1"));
    //
    //

    assertThat(str).isEqualTo("VA1");
  }

  @Test
  public void applyStrTemplate_015() {

    //
    //
    String str = StrUtil.applyStrTemplate("{{}}", "{{a1}} END", Map.of("a1", "VA1"));
    //
    //

    assertThat(str).isEqualTo("VA1 END");
  }

  @Test
  public void applyStrTemplate_016() {

    //
    //
    String str = StrUtil.applyStrTemplate("{{}}", "BEGIN {{a1}} END", Map.of());
    //
    //

    assertThat(str).isEqualTo("BEGIN  END");
  }

  @Test
  public void applyStrTemplate_017() {

    //
    //
    String str = StrUtil.applyStrTemplate("{{}}", "BEGIN {{a1}} END", Map.of());
    //
    //

    assertThat(str).isEqualTo("BEGIN  END");
  }

  @Test
  public void applyStrTemplate_018() {

    //
    //
    String str = StrUtil.applyStrTemplate("{{}}", "BEGIN {{  a1   }} END", Map.of("a1", "WOW"));
    //
    //

    assertThat(str).isEqualTo("BEGIN WOW END");
  }

  @Test
  public void applyStrTemplate_019() {

    //
    //
    String str = StrUtil.applyStrTemplate("{{}}", "BEGIN {{  a1   ", Map.of());
    //
    //

    assertThat(str).isEqualTo("BEGIN ");
  }

  @Test
  public void applyStrTemplate_020() {

    //
    //
    String str = StrUtil.applyStrTemplate("{{}}", "BEGIN {{a1}} END, START {{  a2   ", Map.of("a1", "SAD"));
    //
    //

    assertThat(str).isEqualTo("BEGIN SAD END, START ");
  }

  @Test
  public void applyStrTemplate_021() {

    //
    //
    String str = StrUtil.applyStrTemplate("{{}}", "BEGIN {a1} MIDDLE {{a1}} END", Map.of("a1", "SAD"));
    //
    //

    assertThat(str).isEqualTo("BEGIN {a1} MIDDLE SAD END");
  }
}
