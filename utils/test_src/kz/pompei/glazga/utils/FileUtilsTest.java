package kz.pompei.glazga.utils;

import java.nio.file.Path;
import java.util.List;
import org.testng.annotations.Test;

public class FileUtilsTest {

  @Test
  public void runCmd() {

    Path home = Path.of(System.getProperty("user.home"));
    System.out.println(home);

    Path tmp = home.resolve("tmp");

    RunResult rr = FileUtils.runCmd(tmp, List.of("cat", "hello.txt"));

    System.out.println("lDCD5mssGV :: rr.exitCode = " + rr.exitCode);
    System.out.println("lDCD5mssGV :: rr.out = " + rr.out);
    System.out.println("lDCD5mssGV :: rr.err = " + rr.err);

  }
}
