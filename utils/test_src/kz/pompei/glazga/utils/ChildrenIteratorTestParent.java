package kz.pompei.glazga.utils;

import java.util.ArrayList;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class ChildrenIteratorTestParent {


  @RequiredArgsConstructor(staticName = "of")
  protected static class Testing {
    public final String        name;
    public final List<Testing> children = new ArrayList<>();

    int level, expectedLevel;

    @Override
    public String toString() {
      return name;
    }
  }

  protected static void printTesting(int tab, @NonNull Testing testing) {
    testing.expectedLevel = tab;
    System.out.println("7KiEi9XXcd :: " + ("  ".repeat(tab)) + testing.name + " (" + tab + ")");
    for (final Testing child : testing.children) {
      printTesting(tab + 1, child);
    }
  }

  protected static void addFull(@NonNull List<Testing> list, @NonNull List<String> target) {
    for (final Testing item : list) {
      target.add(item.name);
      addFull(item.children, target);
    }
  }

  protected static void assertIt(@NonNull List<Testing> roots) {

    List<Testing> list = new ArrayList<>();

    for (final Testing root : roots) {
      printTesting(0, root);
      appendNode(list, root);
    }
    System.out.println();

    final ChildrenIterator<Testing> iterator = new ChildrenIterator<>(roots, t -> t.children, (t, level) -> t.level = level);

    List<String> expected = new ArrayList<>();
    addFull(roots, expected);

    List<String> actual = new ArrayList<>();

    while (iterator.hasNext()) {
      final Testing next = iterator.next();
      actual.add(next.name);
    }

    final int LEN   = 20;
    int       count = Math.max(actual.size(), expected.size());
    for (int i = 0; i < count; i++) {
      String s1 = i < actual.size() ? actual.get(i) : "";
      String L1 = s1.length() > LEN ? "" : " ".repeat(LEN - s1.length());
      String s2 = i < expected.size() ? expected.get(i) : "";
      System.out.println("08eAwrOYoH :: " + s1 + L1 + s2);
    }

    assertThat(actual).isEqualTo(expected);

    for (final Testing node : list) {
      assertThat(node.level).as("LuUMj7Jv1v :: node.name = " + node.name).isEqualTo(node.expectedLevel);
    }
  }

  protected static void appendNode(List<Testing> list, Testing node) {
    list.add(node);
    for (final Testing child : node.children) {
      appendNode(list, child);
    }
  }

}
