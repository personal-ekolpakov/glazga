package kz.pompei.glazga.core.err;

public class BondOwnerIsNull extends RuntimeException {
  public BondOwnerIsNull(String message) {
    super(message);
  }
}
