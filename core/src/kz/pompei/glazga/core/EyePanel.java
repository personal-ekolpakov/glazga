package kz.pompei.glazga.core;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;
import javax.swing.JPanel;
import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.core.err.BondOwnerIsNull;
import kz.pompei.glazga.core.events.EventAcceptor;
import kz.pompei.glazga.core.events.EventContext;
import kz.pompei.glazga.core.logging.Log;
import kz.pompei.glazga.core.logging.LogImpl;
import kz.pompei.glazga.core.model.events.EventTrap;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.UBaseVessel;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.core.model.eye.UElement;
import kz.pompei.glazga.core.model.eye.block.UBlock;
import kz.pompei.glazga.core.model.eye.expr.UExpr;
import kz.pompei.glazga.core.model.eye.expr.UExprCall;
import kz.pompei.glazga.core.model.eye.expr.UExprFix;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.core.model.focus.MoveDirection;
import kz.pompei.glazga.core.mods.EyeMod;
import kz.pompei.glazga.core.mods.EyeMod_FocusDebug;
import kz.pompei.glazga.core.utils.ModelUtil;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.style.Styler;
import kz.pompei.glazga.paint.style.StylerEye;
import kz.pompei.glazga.paint.style.focus.Focusing;
import kz.pompei.glazga.server_face.controller.StoreController;
import kz.pompei.glazga.server_face.model.eye.block.Block;
import kz.pompei.glazga.server_face.model.eye.block.BlockFix;
import kz.pompei.glazga.server_face.model.eye.etc.ActSpec;
import kz.pompei.glazga.server_face.model.eye.etc.ConnectType;
import kz.pompei.glazga.server_face.model.eye.etc.EyeData;
import kz.pompei.glazga.server_face.model.eye.expr.Expr;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static kz.pompei.glazga.core.model.events.EventTrap.free;
import static kz.pompei.glazga.core.model.events.EventTrap.ofCtrl;
import static kz.pompei.glazga.core.model.events.EventTrap.ofCtrlAltShift;
import static kz.pompei.glazga.core.model.events.EventTrap.ofCtrlShift;
import static kz.pompei.glazga.core.model.events.EventTrap.ofKey;
import static kz.pompei.glazga.core.model.events.KeyCode.Down;
import static kz.pompei.glazga.core.model.events.KeyCode.Left;
import static kz.pompei.glazga.core.model.events.KeyCode.Right;
import static kz.pompei.glazga.core.model.events.KeyCode.Semicolon;
import static kz.pompei.glazga.core.model.events.KeyCode.Up;
import static kz.pompei.glazga.core.model.events.KeyCode.keyA;
import static kz.pompei.glazga.core.model.events.KeyCode.keyD;
import static kz.pompei.glazga.core.model.events.KeyCode.keyF;
import static kz.pompei.glazga.core.model.events.KeyCode.keyJ;
import static kz.pompei.glazga.core.model.events.KeyCode.keyK;
import static kz.pompei.glazga.core.model.events.KeyCode.keyL;
import static kz.pompei.glazga.core.model.events.KeyCode.keyS;
import static kz.pompei.glazga.core.model.focus.MoveDirection.TO_DOWN;
import static kz.pompei.glazga.core.model.focus.MoveDirection.TO_LEFT;
import static kz.pompei.glazga.core.model.focus.MoveDirection.TO_RIGHT;
import static kz.pompei.glazga.core.model.focus.MoveDirection.TO_UP;
import static kz.pompei.glazga.utils.PaintUtils.applyAntialiasing;
import static kz.pompei.glazga.utils.events.MouseKeys.mouseKeys;

@RequiredArgsConstructor
public class EyePanel implements ULogic {

  private final StoreController  controller;
  private final Supplier<Styler> styler;

  private final UBaseVessel vessel = new UBaseVessel();

  private boolean debug = false;

  private LinearTrans trans = LinearTrans.one();

  private final JPanel source = new JPanel() {
    @Override
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      drawEye((Graphics2D) g);
    }
  };

  {
    source.addMouseWheelListener(new MouseAdapter() {
      @Override
      public void mouseWheelMoved(MouseWheelEvent e) {

        for (final EyeMod eyeMod : modList) {
          if (eyeMod.mouseWheelMoved(e)) {
            return;
          }
        }

        if (mouseKeys().free().isDown(e)) {
          updateScale(e.getWheelRotation() < 0, Vec.fromPoint(e.getPoint()));
          return;
        }
      }
    });

    source.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {

        for (final EyeMod eyeMod : modList) {
          if (eyeMod.mouseClicked(e)) {
            return;
          }
        }

        if (mouseKeys().free().isMiddleDown(e) && e.getClickCount() == 2) {
          resetScale();
          return;
        }
      }
    });

    source.addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseDragged(MouseEvent e) {

        for (final EyeMod eyeMod : modList) {
          if (eyeMod.mouseDragged(e)) {
            return;
          }
        }

      }

      @Override
      public void mouseMoved(MouseEvent e) {

        for (final EyeMod eyeMod : modList) {
          if (eyeMod.mouseMoved(e)) {
            return;
          }
        }

      }
    });
  }

  private final List<EyeMod> modList = new ArrayList<>();
  private final EventContext eventContext = new EventContext();

  {
    eventContext.register(List.of(ofCtrlShift().key(keyD), ofKey(keyA).key(keyS).key(keyD)), this::changeDebug,
                          "gItJNK10y2 :: Включить/выключить отладку");

    eventContext.register(List.of(free().key(keyK), free().key(Up)), () -> moveFocus(FocusMove.of(TO_UP, false)),
                          "eDv2x76nYk :: Двинуть фокус вверх");
    eventContext.register(List.of(free().key(keyL), free().key(Down)), () -> moveFocus(FocusMove.of(TO_DOWN, false)),
                          "c8XBSqi4Xj :: Двинуть фокус вниз");
    eventContext.register(List.of(free().key(keyJ), free().key(Left)), () -> moveFocus(FocusMove.of(TO_LEFT, false)),
                          "1DMuX2o1dg :: Двинуть фокус влево");
    eventContext.register(List.of(free().key(Semicolon), free().key(Right)), () -> moveFocus(FocusMove.of(TO_RIGHT, false)),
                          "qt2jolEplW :: Двинуть фокус вправо");

    eventContext.register(List.of(ofKey(keyF).key(keyK), ofCtrl().key(Up)), () -> moveFocus(FocusMove.of(TO_UP, true)),
                          "1Bu071m1XW :: Двинуть фокус вверх дальше");
    eventContext.register(List.of(ofKey(keyF).key(keyL), ofCtrl().key(Down)), () -> moveFocus(FocusMove.of(TO_DOWN, true)),
                          "E51698iuWm :: Двинуть фокус вниз дальше");
    eventContext.register(List.of(ofKey(keyF).key(keyJ), ofCtrl().key(Left)), () -> moveFocus(FocusMove.of(TO_LEFT, true)),
                          "ItV9kXdI2p :: Двинуть фокус влево дальше");
    eventContext.register(List.of(ofKey(keyF).key(Semicolon), ofCtrl().key(Right)), () -> moveFocus(FocusMove.of(TO_RIGHT, true)),
                          "qhf9M8w4d4 :: Двинуть фокус вправо дальше");

    List<EventTrap> eventTrapList = List.of(ofCtrlAltShift().key(keyS));
    eventContext.register(eventTrapList, () -> modList.add(new EyeMod_FocusDebug(this, eventTrapList)),
                          "4XPqL1qlU8 :: Активация режима отладки движения фокуса");

  }

  @Override
  public void redraw() {
    source.repaint();
  }

  @Override
  public Styler styler() {
    return styler.get();
  }

  @Override
  public void removeMods(Predicate<EyeMod> testModuleToRemove) {

    int i = 0;

    while (i < modList.size()) {

      EyeMod eyeMod = modList.get(i);
      if (testModuleToRemove.test(eyeMod)) {
        System.out.println("Yc9rLRr3WG :: Удаление мода " + eyeMod);
        modList.remove(i);
        continue;
      }

      i++;
    }

    source.repaint();
  }

  @Override
  public ULogicFocus focus() {
    return focus;
  }

  @Override
  public UBaseVessel vessel() {
    return vessel;
  }

  private final ULogicFocusAbstract focus = new ULogicFocusAbstract() {
    @Override
    public void saveAndRefreshFocus() {
      controller.saveFocusRef(filePath, lastChangeGroup(), ref());
      refreshFocus();
      source.repaint();
    }
  };

  private final LogImpl log = new LogImpl(this);

  private Rect drawRect() {

    Vec pixelLeftTop     = Vec.zero();
    Vec pixelRightBottom = Vec.of(source.getWidth(), source.getHeight());

    Vec leftTop     = trans().back(pixelLeftTop);
    Vec rightBottom = trans().back(pixelRightBottom);

    return Rect.dia(leftTop, rightBottom);
  }

  private void moveFocus(FocusMove focusMove) {
    log().focus().eye_moveFocus(focusMove);

    if (focus.selectedEye) {

      Double left = focus.left;
      Double top  = focus.top;

      Rect drawRect = drawRect();

      if (left == null) {
        left = drawRect.center().x;
      }
      if (top == null) {
        top = drawRect.center().y;
      }

      Vec currentPos  = Vec.of(left, top);
      Vec newFocusPos = focusMove.direction.move(currentPos, styler.get().eye().focusStep());

      setEyeFocusTo(newFocusPos);

      return;
    }

    {
      UElement focusedElement = focus.focusedElement;
      if (focusedElement != null) {
        focusedElement.moveFocus(focusMove);
        return;
      }
    }

    {
      Bond focusedBond = focus.focusedBond;
      if (focusedBond != null) {
        focusedBond.moveFocus(focusMove);
        return;
      }
    }

    {
      UDown focusedDown = focus.focusedDown;
      if (focusedDown != null) {
        focusedDown.moveFocus(focusMove);
        return;
      }
    }

    throw new RuntimeException("KS9St1pGiP :: Здесь нужно включить фокус где-то посередине экрана");
  }

  public List<EventAcceptor> eventAcceptors() {
    return Stream.concat(modList.stream().map(EyeMod::eventContext).filter(Objects::nonNull), Stream.of(eventContext))
                 .map(EventAcceptor.class::cast)
                 .toList();
  }

  private void changeDebug() {
    debug = !debug;
    source.repaint();
  }

  public JPanel source() {
    return this.source;
  }


  private void updateScale(boolean inScale, Vec overPoint) {
    final double MU = 1.1;
    final double mu = inScale ? MU : 1 / MU;
    trans = trans.scaleOverPoint(overPoint, mu);
    controller.saveTrans(filePath, lastChangeGroup(), trans);
    source.repaint();
  }

  /**
   * Этот метод должен возвращать последнюю группу обновлений, чтобы данное обновление отменилось вместе с предыдущим обновлением
   */
  private String lastChangeGroup() {
    return null;
  }

  private void resetScale() {
    trans = LinearTrans.one();
    controller.saveTrans(filePath, lastChangeGroup(), trans);
    source.repaint();
  }

  private String filePath;

  public void setFilePath(String filePath) {

    if (Objects.equals(this.filePath, filePath)) {
      return;
    }

    this.filePath = filePath;

    EyeData eyeData = controller.loadEyeData(filePath);
    trans = eyeData.trans;
    focus.setFocus(eyeData.focusRef);

    for (final Map.Entry<String, Block> e : eyeData.eye.blocks.entrySet()) {
      String        id     = e.getKey();
      Block         data   = e.getValue();
      UBlock<Block> uBlock = ModelUtil.createUBlock(new ModelUtil.UInit<>(id, data, styler, this));
      vessel.blocks.put(id, uBlock);
    }

    for (final Map.Entry<String, Expr> e : eyeData.eye.expressions.entrySet()) {
      String      id    = e.getKey();
      Expr        data  = e.getValue();
      UExpr<Expr> uExpr = ModelUtil.createUExpr(new ModelUtil.UInit<>(id, data, styler, this));
      vessel.expressions.put(id, uExpr);
    }

    assignActSpecMap(eyeData.actSpecifications);

    initDownsAndBonds();

    printElements();

    checkBondOwner();

    vessel.forEach(UElement::calcGeometry);

    removeExprWithoutOwner();

    calculateFocusGeometry();

    for (final UElement u : vessel) {
      u.validateAfterAllInitializations();
    }

    refreshFocus();
  }

  private void initDownsAndBonds() {

    Set<String> usedBlockIds = new HashSet<>();
    Set<String> usedExprIds  = new HashSet<>();

    List<UElement> elementList = vessel.stream().filter(UElement::isFixed).collect(toList());

    TOP:
    while (elementList.size() > 0) {
      UElement element = elementList.remove(0);

      boolean unknownElement = true;

      if (element instanceof UBlock<?> block) {
        unknownElement = false;

        if (usedBlockIds.contains(block.id())) {
          System.err.println("3QiNFC55nL :: Double using of block = " + block);
          continue TOP;
        }
        usedBlockIds.add(block.id());

        List<UDown> downs = block.downs().values().stream().toList();

        DOWN_BLOCK:
        for (final UDown down : downs) {

          String downBlockId = down.downBlockId();
          if (downBlockId == null) {
            continue DOWN_BLOCK;
          }

          UBlock<?> downBlock = vessel.blocks.get(downBlockId);
          if (downBlock == null) {
            System.err.println("Es3pa7XuTM :: ERR No downBlockId=" + downBlockId + " in " + down);
            continue DOWN_BLOCK;
          }

          if (downBlock.owner() != null) {
            System.err.println("01XX2zix32 :: ERR there are several references to block " + downBlock + ", :"
                                 + "\n    REF 1: from " + down
                                 + "\n    REF 2: from " + downBlock
            );
            continue DOWN_BLOCK;
          }

          elementList.add(downBlock);

          UDown.__initDown__.setDownBlock(down, downBlock);
          UBlock.__initBlock__.setOwner(downBlock, down);
        }

      }

      if (element instanceof UExpr<?> expr) {
        unknownElement = false;

        if (usedExprIds.contains(expr.id())) {
          System.err.println("3QiNFC55nL :: Double using of expr = " + expr);
          continue TOP;
        }
        usedExprIds.add(expr.id());
      }

      if (unknownElement) {
        System.err.println("m1QF1T8RHF :: Unknown element " + element.getClass());
        continue TOP;
      }

      BOND:
      for (final Bond bond : element.bonds().values()) {

        String bondExprId = bond.ownerConnect.exprId();
        if (bondExprId == null) {
          continue BOND;
        }

        UExpr<?> bondExpr = vessel.expressions.get(bondExprId);
        if (bondExpr == null) {
          System.err.println("et0WgmXNWu :: Broken bond ref: " + bond);
          continue BOND;
        }

        Bond ownerBond = bondExpr.bondOwner();
        if (ownerBond != null) {
          System.err.println("d6XyaSFhVx :: There are several references to expr " + bondExpr
                               + "    \nREF1: from " + bond
                               + "    \nREF2: from " + ownerBond

          );
          continue BOND;
        }

        elementList.add(bondExpr);

        Bond.__initBond__.setBondExpr(bond, bondExpr);
        UExpr.__initExpr__.setBondOwner(bondExpr, bond);
      }

    }

    {
      Set<String> ownerLessBlockIds = new HashSet<>();

      CHECK_BLOCK_OWNERS:
      for (final UBlock<?> block : vessel.blocks.values()) {
        if (block.isFixed()) {
          continue CHECK_BLOCK_OWNERS;
        }
        if (block.owner() == null) {
          System.err.println("DV9jFb2wMU :: Owner-less block " + block);
          ownerLessBlockIds.add(block.id());
        }
      }

      ownerLessBlockIds.forEach(vessel.blocks::remove);
    }

    {
      Set<String> ownerLessExprIds = new HashSet<>();

      for (final UExpr<?> expr : vessel.expressions.values()) {
        if (expr.bondOwner() == null) {
          System.err.println("JlDf8uSGS8 :: Owner-less expr " + expr);
          ownerLessExprIds.add(expr.id());
        }
      }

      ownerLessExprIds.forEach(vessel.expressions::remove);
    }

  }

  private void printElements() {

    List<UBlock<?>> fixBlocks = vessel.blocks.values()
                                             .stream()
                                             .filter(b -> b.data() instanceof BlockFix)
                                             .sorted(Comparator.comparing(UBlock::id))
                                             .toList();

    Map<String, String> passedExprToBlockIds = new HashMap<>();

    fixBlocks.forEach(fixBlock -> printFixBlock(fixBlock, passedExprToBlockIds));

  }

  private void printFixBlock(UBlock<?> fixBlock, Map<String, String> passedExprToBlockIds) {

    List<UBlock<?>> listToPrint = new ArrayList<>();
    listToPrint.add(fixBlock);

    Set<String> printBlockIds = new HashSet<>();

    boolean first = true;

    while (listToPrint.size() > 0) {
      UBlock<?> block = listToPrint.remove(0);
      if (printBlockIds.contains(block.id())) {
        System.out.println("___ ___ ___ ERR TC1Yu61nRH :: "
                             + "block " + block + " wanna print twice");
        continue;
      }
      printBlockIds.add(block.id());

      printBlock(block, first, passedExprToBlockIds);
      first = false;

      block.downs()
           .values()
           .stream()
           .map(UDown::downBlock)
           .filter(Objects::nonNull)
           .forEach(listToPrint::add);
    }

  }

  private void printBlock(UBlock<?> block, boolean first, Map<String, String> passedExprToBlockIds) {

    StringBuilder sb = new StringBuilder();
    if (first) {
      sb.append("RcT7q33Pa7 :: ");
    } else {
      sb.append("              ");
    }

    sb.append(block);

    List<String> exprIdsToPrint = block.bonds()
                                       .values()
                                       .stream()
                                       .map(Bond::bondExpr)
                                       .filter(Objects::nonNull)
                                       .map(UExpr::id)
                                       .sorted()
                                       .collect(toList());

    List<String> exprIdList = new ArrayList<>();

    while (exprIdsToPrint.size() > 0) {
      String exprId = exprIdsToPrint.remove(0);
      {
        String alreadyContainsBlockId = passedExprToBlockIds.get(exprId);
        if (alreadyContainsBlockId != null) {
          System.out.println("___ ___ ___ ERR 2oRT64zKtQ :: " + exprId
                               + " already contains in blockId = " + alreadyContainsBlockId);
          continue;
        }
        passedExprToBlockIds.put(exprId, block.id());
      }
      exprIdList.add(exprId);

      UExpr<?> expr = vessel.expressions.get(exprId);
      if (expr == null) {
        System.out.println("___ ___ ___ ERR 9vvQbZx7SO :: " + exprId
                             + " No expr with id = " + exprId);
        continue;
      }

      expr.bonds()
          .values()
          .stream()
          .map(Bond::bondExpr)
          .filter(Objects::nonNull)
          .map(UExpr::id)
          .forEach(exprIdsToPrint::add);
    }

    sb.append(exprIdList.stream().collect(joining(", ", " [", "]")));

    System.out.println(sb);
  }

  @SneakyThrows
  private void checkBondOwner() {
    for (final UExpr<?> expr : vessel.expressions.values()) {

      if (expr instanceof UExprFix) {
        continue;
      }

      String bondOwnerName  = UExpr.Fields.bondOwner;
      Field  bondOwnerField = UExpr.class.getDeclaredField(bondOwnerName);
      bondOwnerField.setAccessible(true);
      Object bondOwner = bondOwnerField.get(expr);
      if (bondOwner == null) {
        throw new BondOwnerIsNull("vx9F9W42GC :: " + bondOwnerName + " == null" +
                                    " in " + expr.getClass().getSimpleName() + ", id=`" + expr.id() + "`"
                                    + "\n" + bondOwnerName + " may be null only in " + UExprFix.class.getSimpleName());
      }

    }
  }

  private void calculateFocusGeometry() {
    for (final UElement element : vessel) {
      element.initFocusGeometry();
    }
  }

  private void refreshFocus() {

    focus.focusedBond    = null;
    focus.focusedDown = null;
    focus.focusedElement = null;


    for (final UElement u : vessel) {
      u.focusing = Focusing.NONE;
    }

    if (focus.selectedEye) {
      return;
    }

    focus.focusedElement = vessel.getElement(focus.type, focus.getElementId());

    if (focus.focusedElement == null) {
      return;
    }

    UElement current = focus.focusedElement;
    current.focusing = Focusing.IS;

    SELECT_CONNECTOR:
    if (focus.connectName != null && focus.connectType != null) {

      if (focus.connectType == ConnectType.DOWN) {

        if (!(focus.focusedElement instanceof UBlock<?> block)) {
          return;
        }

        focus.focusedDown = block.downs().get(focus.connectName);
        if (focus.focusedDown != null) {
          focus.focusedElement = null;
          current.focusing     = Focusing.HAVE;
        }
        break SELECT_CONNECTOR;
      }

      if (focus.connectType == ConnectType.BOND) {

        focus.focusedBond = focus.focusedElement.bonds().get(focus.connectName);
        if (focus.focusedBond != null) {
          focus.focusedElement = null;
          current.focusing     = Focusing.HAVE;
        }
        break SELECT_CONNECTOR;
      }

      throw new RuntimeException("sGwZ8U7rJ6 :: Unknown " + ConnectType.class.getSimpleName() + " = " + focus.connectType);
    }

    if (current instanceof UExpr<?>) {

      INFINITY_LOOP:
      while (true) {

        UElement it = current;
        current = it.up();
        if (current == null) {
          break INFINITY_LOOP;
        }

        current.focusing = Focusing.HAVE;

        if (current instanceof UBlock<?>) {
          break INFINITY_LOOP;
        }

      }

    }
  }

  private void removeExprWithoutOwner() {
    vessel.expressions.entrySet().removeIf(e -> !e.getValue().hasHolder());
  }

  private void assignActSpecMap(Map<String, ActSpec> actSpecifications) {
    for (final UElement u : vessel) {
      if (u instanceof UExprCall uec) {
        UExprCall.__initExprCall__.initSpec(uec, actSpecifications);
      }
    }
  }

  private void drawEye(Graphics2D g) {
    applyAntialiasing(g);

    modList.forEach(mod -> mod.drawEyeBeforeBlocks(g));

    for (final UBlock<?> block : vessel.blocks.values()) {
      block.paint(g);
      for (final Bond bond : block.bonds().values()) {
        bond.paint(g);
      }
    }

    if (focus.focusedDown != null) {
      focus.focusedDown.paintFocused(g);
    }

    if (focus.selectedEye) {
      StylerEye stylerEye = styler.get().eye();
      double    len       = stylerEye.focusCrossLen();

      Vec pos = Vec.of(focus.left, focus.top);

      PainterPath.withTrans(trans())
                 .over(stylerEye::applyFocusCross)
                 .moveTo(pos.left(len)).right(2 * len)
                 .moveTo(pos.up(len)).down(2 * len)
                 .paint(g);
    }

    modList.forEach(mod -> mod.drawEyeAfterBlocks(g));
  }

  @Override
  public boolean debug() {
    return debug;
  }

  @Override
  public ULogicElementSizes elementSizes() {
    return elementSizes;
  }

  private final ULogicElementSizes elementSizes = new ULogicElementSizes() {

    @Override
    public void saveBlockHeight(String blockId, double height) {
      // while do nothing
    }

    @Override
    public void saveExprHeight(String exprId, ExprHeight height) {
      // while do nothing
    }

    @Override
    public void saveElementWidth(UType uType, String elementId, double width) {
      // while do nothing
    }
  };

  @Override
  public LinearTrans trans() {
    return trans;
  }

  @Override
  public void blockGivesFocusToEye(UBlock<?> thisBlock, FocusMove focusMove) {
    log().focus().eye_blockGivesFocusToEye(thisBlock, focusMove);
    setEyeFocusTo(stepFocusFrom(thisBlock.rect(), focusMove.direction, styler.get().eye().focusStep()));
  }

  @Override
  public void exprGivesFocusToEye(UExpr<?> thisExpr, FocusMove focusMove) {
    log().focus().eye_exprGivesFocusToEye(thisExpr, focusMove);
    setEyeFocusTo(stepFocusFrom(thisExpr.rect(), focusMove.direction, styler.get().eye().focusStep()));
  }

  @Override
  public void downGivesFocusToEye(UDown down, FocusMove focusMove) {
    log().focus().eye_downGivesFocusToEye(down, focusMove);
    setEyeFocusTo(stepFocusFrom(down.rect(), focusMove.direction, styler.get().eye().focusStep()));
  }

  @SuppressWarnings("SameParameterValue")
  private static Vec stepFocusFrom(Rect rect, MoveDirection direction, double eyeStep) {

    switch (direction) {
      case TO_RIGHT -> {
        return Vec.of(rect.right + eyeStep, rect.center().y);
      }
      case TO_LEFT -> {
        return Vec.of(rect.left - eyeStep, rect.center().y);
      }
      case TO_UP -> {
        return Vec.of(rect.center().x, rect.top - eyeStep);
      }
      case TO_DOWN -> {
        return Vec.of(rect.center().x, rect.bottom + eyeStep);
      }
      default -> throw new RuntimeException("gpQVmDLX1f :: Unknown " + MoveDirection.class + " = " + direction);
    }
  }

  private void setEyeFocusTo(Vec newFocusPoint) {

    for (final UBlock<?> block : vessel.blocks.values()) {
      if (block.takeFocusAt(newFocusPoint)) {
        return;
      }
    }

    focus().selectEye(newFocusPoint);
    focus().saveAndRefreshFocus();
  }

  @Override
  public Log log() {
    return log;
  }

  @Override
  public Rect rectEye() {
    Vec pixelLeftTop     = Vec.zero();
    Vec pixelRightBottom = Vec.of(source.getWidth(), source.getHeight());

    Vec leftTop     = trans().back(pixelLeftTop);
    Vec rightBottom = trans().back(pixelRightBottom);

    return Rect.dia(leftTop, rightBottom);
  }
}
