package kz.pompei.glazga.core.logging;

public interface Log {
  LogFocus focus();
}
