package kz.pompei.glazga.core.logging;

import kz.pompei.glazga.core.ULogic;

public class LogImpl implements Log {

  private final LogFocus focus;

  public LogImpl(ULogic logic) {
    this.focus = new LogFocusImpl(logic);
  }

  @Override
  public LogFocus focus() {
    return focus;
  }
}
