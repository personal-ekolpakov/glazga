package kz.pompei.glazga.core.logging;

import java.util.concurrent.atomic.AtomicBoolean;
import kz.pompei.glazga.core.ULogic;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.core.model.eye.UElement;
import kz.pompei.glazga.core.model.focus.FocusLogic;
import kz.pompei.glazga.core.model.focus.FocusLogicBond;
import kz.pompei.glazga.core.model.focus.FocusLogicDown;
import kz.pompei.glazga.core.model.focus.FocusLogicOwn;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.core.utils.NumberUtil;
import kz.pompei.glazga.mat.Vec;

public abstract class LogFocusParent implements LogFocus {
  public final    AtomicBoolean enabled = new AtomicBoolean(true);
  protected final ULogic        logic;

  protected LogFocusParent(ULogic logic) {
    this.logic = logic;
  }

  protected void print(String message) {
    System.out.println("FOC " + message);
  }

  protected String doubleToStr(Double value) {
    final int intLen = 4, fracLen = 1;
    if (value == null) {
      return " ".repeat(intLen - 1) + "▯" + " ".repeat(fracLen + 1);
    }
    return NumberUtil.doubleToStrLen(value, intLen, fracLen);
  }

  protected String vecToStr(Vec pos) {
    return "Vec{" + doubleToStr(pos.x) + " " + doubleToStr(pos.y) + "}";
  }

  protected String elementId(UElement element) {
    return element.getClass().getSimpleName() + ".id=" + element.id();
  }

  protected boolean disabled() {
    return !enabled.get();
  }

  protected String downId(UDown down) {
    return "Down{" + down.name() + "@" + elementId(down.owner) + "}";
  }

  protected String bondId(Bond bond) {
    return "Bond{" + bond.name() + "@" + elementId(bond.owner) + "}";
  }

  protected String moveId(FocusMove focusMove) {
    return focusMove.direction.name();
  }

  protected String focusLogicId(FocusLogic focusLogic) {
    String cl = focusLogic.getClass().getSimpleName();
    if (focusLogic instanceof FocusLogicBond flb) {
      return cl + "{" + bondId(flb.bond) + "}";
    }
    if (focusLogic instanceof FocusLogicDown fld) {
      return cl + "{" + downId(fld.down) + "}";
    }
    if (focusLogic instanceof FocusLogicOwn flo) {
      return cl + "{state=" + flo.place.state() + "@" + elementId(flo.owner) + "}";
    }
    throw new RuntimeException("e6ob06dn41 :: Unknown class " + focusLogic.getClass().getSimpleName());
  }
}
