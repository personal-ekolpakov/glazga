package kz.pompei.glazga.core.logging;

import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.core.model.eye.UElement;
import kz.pompei.glazga.core.model.eye.block.UBlock;
import kz.pompei.glazga.core.model.eye.expr.UExpr;
import kz.pompei.glazga.core.model.focus.FocusLogicBond;
import kz.pompei.glazga.core.model.focus.FocusLogicDown;
import kz.pompei.glazga.core.model.focus.FocusLogicOwn;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.mat.Vec;

public interface LogFocus {

  void element_takeFocusAt(UElement self, Vec pos);

  void focusLogicBond_takeFocusAt(FocusLogicBond self, Vec pos);

  void focusLogicBond_takeFocusAndRepaint(FocusLogicBond self, FocusMove focusMove);

  void focusLogicDown_takeFocusAndRepaint(FocusLogicDown self, FocusMove focusMove);

  void focusLogicDown_takeFocusAt(FocusLogicDown self, Vec pos);

  void focusLogicOwn_takeFocusAndRepaint(FocusLogicOwn self, FocusMove focusMove);

  void focusLogicOwn_takeFocusAt(FocusLogicOwn self, Vec pos);

  void element_moveFocus(UElement self, FocusMove focusMove);

  void element_childBondMovesFocus(UElement self, Bond bond, FocusMove focusMove);

  void eye_blockGivesFocusToEye(UBlock<?> givingBlock, FocusMove focusMove);

  void eye_exprGivesFocusToEye(UExpr<?> givingExpr, FocusMove focusMove);

  void eye_downGivesFocusToEye(UDown givingDown, FocusMove focusMove);

  void eye_moveFocus(FocusMove focusMove);

  void down_moveFocus(UDown self, FocusMove focusMove);

  void bond_moveFocus(Bond self, FocusMove focusMove);
}
