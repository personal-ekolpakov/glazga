package kz.pompei.glazga.core.logging;

import kz.pompei.glazga.core.ULogic;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.core.model.eye.UElement;
import kz.pompei.glazga.core.model.eye.block.UBlock;
import kz.pompei.glazga.core.model.eye.expr.UExpr;
import kz.pompei.glazga.core.model.focus.FocusLogicBond;
import kz.pompei.glazga.core.model.focus.FocusLogicDown;
import kz.pompei.glazga.core.model.focus.FocusLogicOwn;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;

public class LogFocusImpl extends LogFocusParent {

  public LogFocusImpl(ULogic logic) {
    super(logic);
  }

  @Override
  public void element_takeFocusAt(UElement self, Vec pos) {
    if (disabled()) {
      return;
    }

    print("5ZDr1bRZ1Z " + focusPosAndRect(self.rect()) + elementId(self) + ".takeFocusAt pos=" + vecToStr(pos));
  }

  protected String focusPosAndRect(Rect rect) {
    return "[" + doubleToStr(rect.left) + " " + doubleToStr(rect.top) + "/" + doubleToStr(rect.right) + doubleToStr(rect.bottom) + "]"
      + "(" + doubleToStr(logic.focus().getLeft()) + " " + doubleToStr(logic.focus().getTop()) + ") ";
  }

  @Override
  public void focusLogicBond_takeFocusAt(FocusLogicBond self, Vec pos) {
    if (disabled()) {
      return;
    }
    print("Fo39fqMoQp " + focusPosAndRect(self.rect()) + focusLogicId(self) + ".takeFocusAt pos=" + vecToStr(pos));
  }

  @Override
  public void focusLogicBond_takeFocusAndRepaint(FocusLogicBond self, FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("9H1E6aV1KC " + focusPosAndRect(self.rect()) + focusLogicId(self) + ".takeFocusAndRepaint " + moveId(focusMove));
  }

  @Override
  public void focusLogicDown_takeFocusAndRepaint(FocusLogicDown self, FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("4k2O8HgsZM " + focusPosAndRect(self.rect()) + focusLogicId(self) + ".takeFocusAndRepaint " + moveId(focusMove));
  }

  @Override
  public void focusLogicDown_takeFocusAt(FocusLogicDown self, Vec pos) {
    if (disabled()) {
      return;
    }
    print("6IGPwzTc6Z " + focusPosAndRect(self.rect()) + focusLogicId(self) + ".takeFocusAt pos=" + vecToStr(pos));
  }

  @Override
  public void focusLogicOwn_takeFocusAndRepaint(FocusLogicOwn self, FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("C3K4p55BfK " + focusPosAndRect(self.rect()) + focusLogicId(self) + ".takeFocusAndRepaint " + moveId(focusMove));
  }

  @Override
  public void focusLogicOwn_takeFocusAt(FocusLogicOwn self, Vec pos) {
    if (disabled()) {
      return;
    }
    print("hCD1HA8HBB " + focusPosAndRect(self.rect()) + focusLogicId(self) + ".takeFocusAt pos=" + vecToStr(pos));
  }

  @Override
  public void element_moveFocus(UElement self, FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("46pur9Sqt6 " + focusPosAndRect(self.rect()) + elementId(self) + ".moveFocus " + moveId(focusMove));
  }

  @Override
  public void element_childBondMovesFocus(UElement self, Bond bond, FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("FKRu99E8FD " + focusPosAndRect(self.rect()) + elementId(self)
            + ".childBondMovesFocus bond.name=" + bond.name() + " " + moveId(focusMove));
  }

  @Override
  public void eye_blockGivesFocusToEye(UBlock<?> givingBlock, FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("gFD4lamOf4 " + focusPosAndRect(logic.rectEye())
            + "EYE.blockGivesFocusToEye: givingBlock=" + elementId(givingBlock) + " " + moveId(focusMove));
  }

  @Override
  public void eye_exprGivesFocusToEye(UExpr<?> givingExpr, FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("1JannO4Zu2 " + focusPosAndRect(logic.rectEye())
            + "EYE.exprGivesFocusToEye: givingExpr=" + elementId(givingExpr) + " " + moveId(focusMove));
  }

  @Override
  public void eye_downGivesFocusToEye(UDown givingDown, FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("pWc9xnLIF3 " + focusPosAndRect(logic.rectEye())
            + " EYE.downGivesFocusToEye: givingDown=" + downId(givingDown) + " " + moveId(focusMove));
  }

  @Override
  public void eye_moveFocus(FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("hCD1HA8HBB " + focusPosAndRect(logic.rectEye()) + "EYE " + ".moveFocus " + moveId(focusMove));
  }

  @Override
  public void down_moveFocus(UDown self, FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("0D2ZkfH6YD " + focusPosAndRect(self.rect()) + " " + downId(self) + ".moveFocus " + moveId(focusMove));
  }

  @Override
  public void bond_moveFocus(Bond self, FocusMove focusMove) {
    if (disabled()) {
      return;
    }
    print("0UpZ194TAa " + focusPosAndRect(self.rect()) + " " + bondId(self) + ".moveFocus " + moveId(focusMove));
  }
}
