package kz.pompei.glazga.core;

import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.server_face.model.eye.etc.ConnectType;

public interface ULogicFocus {

  void setLeft(Double left);

  Double getLeft();

  void setTop(Double top);

  Double getTop();

  String getState();

  void setState(String state);

  void saveAndRefreshFocus();

  void selectElement(UType type, String elementId);

  void selectElementConnect(UType type, String elementId, ConnectType connectType, String connectId);

  String getElementId();

  void selectEye(Vec point);

}
