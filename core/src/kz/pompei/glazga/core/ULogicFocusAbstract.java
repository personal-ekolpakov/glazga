package kz.pompei.glazga.core;

import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.core.model.eye.UElement;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.server_face.model.eye.etc.ConnectType;
import kz.pompei.glazga.server_face.model.eye.etc.UFocusRef;
import lombok.Getter;
import lombok.Setter;

import static java.util.Objects.requireNonNull;

public abstract class ULogicFocusAbstract implements ULogicFocus {
  @Getter
  @Setter
  public Double      left;
  @Getter
  @Setter
  public Double      top;
  public UType       type;
  @Getter
  public String      elementId;
  public String      connectName;
  public ConnectType connectType;
  @Getter
  @Setter
  public String      state;
  public boolean     selectedEye;

  public UElement focusedElement = null;
  public Bond     focusedBond    = null;
  public UDown    focusedDown    = null;

  public void setFocus(UFocusRef ref) {

    left        = null;
    top         = null;
    type        = null;
    elementId   = null;
    connectName = null;
    connectType = null;
    state       = null;
    selectedEye = false;

    if (ref == null) {
      return;
    }

    left        = ref.left;
    top         = ref.top;
    type        = ref.uType;
    elementId   = ref.elementId;
    connectName = ref.connectName;
    connectType = ref.connectType;
    state       = ref.state;
    selectedEye = ref.selectedEye;
  }

  @Override
  public void selectElement(UType type, String elementId) {
    this.type        = requireNonNull(type, "WWS38F30D4");
    this.elementId = requireNonNull(elementId, "VkT2IyRhkL");
    this.connectName = null;
    this.connectType = null;
    selectedEye    = false;
  }

  @Override
  public void selectElementConnect(UType type, String elementId, ConnectType connectType, String connectId) {
    this.type        = requireNonNull(type, "WWS38F30D4");
    this.elementId   = requireNonNull(elementId, "w4HOtkHb2A");
    this.connectType = requireNonNull(connectType, "C5H36an14i");
    this.connectName = requireNonNull(connectId, "R5Pp6zg4Mh");
    selectedEye = false;
  }

  @Override
  public void selectEye(Vec pos) {
    selectedEye = true;
    left        = pos.x;
    top         = pos.y;
  }

  protected UFocusRef ref() {
    return UFocusRef.of(left, top, type, elementId, connectName, connectType, state, selectedEye);
  }
}
