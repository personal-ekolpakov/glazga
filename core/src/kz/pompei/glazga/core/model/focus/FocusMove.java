package kz.pompei.glazga.core.model.focus;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@RequiredArgsConstructor(staticName = "of")
public class FocusMove {
  public final MoveDirection direction;
  public final boolean       longer;

  public boolean isHorizontal() {
    MoveDirection direction = this.direction;
    return direction == MoveDirection.TO_RIGHT || direction == MoveDirection.TO_LEFT;
  }

  public boolean forward() {
    MoveDirection direction = this.direction;
    return direction == MoveDirection.TO_RIGHT || direction == MoveDirection.TO_DOWN;
  }
}
