package kz.pompei.glazga.core.model.focus;

import kz.pompei.glazga.core.ULogicFocus;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.expr.UExpr;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.server_face.model.eye.etc.ConnectType;
import lombok.NonNull;

public class FocusLogicBond implements FocusLogic {
  public final Bond bond;

  public FocusLogicBond(Bond bond) {
    this.bond = bond;
  }

  @Override
  public Rect rect() {
    return bond.rect();
  }

  @Override
  public void takeFocusAndRepaint(FocusMove focusMove) {
    bond.owner.logic.log().focus().focusLogicBond_takeFocusAndRepaint(this, focusMove);
    {
      UExpr<?> expr = bond.bondExpr();
      if (expr != null) {
        expr.moveFocus(focusMove);
        return;
      }
    }

    throw new RuntimeException("ejDNBdo199");
  }

  @Override
  public String toString() {
    return "FocusLogicBond{" + bond.name()
      + " of " + bond.owner.getClass().getSimpleName() + ".id=" + bond.owner.id() + " " + rect() + "}";
  }

  @Override
  public boolean takeFocusAt(@NonNull Vec pos) {
    bond.owner.logic.log().focus().focusLogicBond_takeFocusAt(this, pos);

    if (!rect().containsVec(pos)) {
      return false;
    }
    {
      UExpr<?> expr = bond.bondExpr();
      if (expr != null) {
        boolean tookFocus = expr.takeFocusAt(pos);
        if (!tookFocus) {
          throw new RuntimeException("16dj5o1djS :: Фокус должен быть получен," +
                                       " потому что позиция находиться внутри выражения");
        }
        return true;
      }
    }

    {
      ULogicFocus focus = bond.owner.logic.focus();
      focus.selectElementConnect(bond.owner.type(), bond.owner.id(), ConnectType.BOND, bond.name());
      focus.setLeft(pos.x);
      focus.setTop(pos.y);
      focus.saveAndRefreshFocus();
    }
    return true;
  }
}
