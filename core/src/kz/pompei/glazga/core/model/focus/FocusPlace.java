package kz.pompei.glazga.core.model.focus;

import kz.pompei.glazga.mat.Rect;

public interface FocusPlace {
  String state();

  Rect rect();
}
