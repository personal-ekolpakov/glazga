package kz.pompei.glazga.core.model.focus;

import kz.pompei.glazga.core.ULogicFocus;
import kz.pompei.glazga.core.model.eye.UElement;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import lombok.NonNull;

public class FocusLogicOwn implements FocusLogic {
  public final FocusPlace place;
  public final UElement   owner;

  public FocusLogicOwn(FocusPlace place, UElement owner) {
    this.place = place;
    this.owner = owner;
  }

  @Override
  public Rect rect() {
    return place.rect();
  }

  @Override
  public void takeFocusAndRepaint(FocusMove focusMove) {
    owner.logic.log().focus().focusLogicOwn_takeFocusAndRepaint(this, focusMove);

    ULogicFocus focus = owner.logic.focus();

    focus.selectElement(owner.type(), owner.id());

    Vec center = place.rect().center();
    if (focusMove.isHorizontal()) {
      focus.setLeft(center.x);
    } else {
      focus.setTop(center.y);
    }

    focus.setState(place.state());

    focus.saveAndRefreshFocus();
  }

  @Override
  public String toString() {
    return "FocusLogicOwn{"
      + owner.getClass().getSimpleName() + ".id=" + owner.id() + ", state=" + place.state() + " " + rect() + "}";
  }

  @Override
  public boolean takeFocusAt(@NonNull Vec pos) {
    owner.logic.log().focus().focusLogicOwn_takeFocusAt(this, pos);

    if (!rect().containsVec(pos)) {
      return false;
    }

    {
      ULogicFocus focus = owner.logic.focus();
      focus.selectElement(owner.type(), owner.id());
      focus.setState(place.state());
      focus.setLeft(pos.x);
      focus.setTop(pos.y);
      focus.saveAndRefreshFocus();
    }
    return true;
  }
}
