package kz.pompei.glazga.core.model.focus;

import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import lombok.NonNull;

public interface FocusLogic {

  Rect rect();

  void takeFocusAndRepaint(FocusMove focusMove);

  boolean takeFocusAt(@NonNull Vec pos);
}
