package kz.pompei.glazga.core.model.focus;

import kz.pompei.glazga.mat.Vec;
import lombok.NonNull;

public enum MoveDirection {
  TO_LEFT, TO_UP, TO_RIGHT, TO_DOWN;

  public @NonNull Vec move(@NonNull Vec pos, double step) {
    return switch (this) {
      case TO_LEFT -> pos.left(step);
      case TO_RIGHT -> pos.right(step);
      case TO_DOWN -> pos.down(step);
      case TO_UP -> pos.up(step);
    };
  }

}
