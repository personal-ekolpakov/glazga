package kz.pompei.glazga.core.model.focus;

import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.core.ULogicFocus;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.server_face.model.eye.etc.ConnectType;
import lombok.NonNull;

public class FocusLogicDown implements FocusLogic {
  public final UDown down;

  public FocusLogicDown(UDown down) {
    this.down = down;
  }

  @Override
  public Rect rect() {
    Vec pos  = down.downLeftTop();
    Vec pos2 = pos.right(down.connectLineWidth());
    return Rect.dia(pos, pos2).expand(down.owner.styler().down().expandForTakeFocus());
  }

  @Override
  public void takeFocusAndRepaint(FocusMove focusMove) {
    down.owner.logic.log().focus().focusLogicDown_takeFocusAndRepaint(this, focusMove);
    ULogicFocus focus = down.owner.logic.focus();
    focus.selectElementConnect(UType.BLOCK, down.owner.id(), ConnectType.DOWN, down.name());
    focus.saveAndRefreshFocus();
  }

  @Override
  public boolean takeFocusAt(@NonNull Vec pos) {
    down.owner.logic.log().focus().focusLogicDown_takeFocusAt(this, pos);

    if (!rect().containsVec(pos)) {
      return false;
    }

    {
      ULogicFocus focus = down.owner.logic.focus();
      focus.selectElementConnect(down.owner.type(), down.owner.id(), ConnectType.DOWN, down.name());
      focus.setLeft(pos.x);
      focus.setTop(pos.y);
      focus.saveAndRefreshFocus();
    }
    return true;
  }
}
