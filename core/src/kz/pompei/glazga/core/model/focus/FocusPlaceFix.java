package kz.pompei.glazga.core.model.focus;

import kz.pompei.glazga.mat.Rect;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class FocusPlaceFix implements FocusPlace {
  private final String state;
  private final Rect   rect;

  @Override
  public String state() {
    return state;
  }

  @Override
  public Rect rect() {
    return rect;
  }

  @Override
  public String toString() {
    return "FocusPlace{`" + state() + "`, " + rect() + "}";
  }
}
