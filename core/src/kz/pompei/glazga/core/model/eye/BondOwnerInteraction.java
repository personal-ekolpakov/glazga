package kz.pompei.glazga.core.model.eye;

import kz.pompei.glazga.mat.Vec;
import lombok.NonNull;

/**
 * Определяет взаимодействие с владельцем
 */
public interface BondOwnerInteraction {

  /**
   * Возвращает имя ограды, уникальное в рамках U-элемента-владельца
   */
  @NonNull String name();

  /**
   * Возвращает позицию левой базовой точки выражения (или окантовки)
   */
  @NonNull Vec leftBaseLine();

  /**
   * Возвращает идентификатор выражения, которую данная окантовка содержит.
   * Если вернёт null, то данная окантовка пустая, и должна отобразить соответствующий прямоугольник
   */
  String exprId();

}
