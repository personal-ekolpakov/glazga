package kz.pompei.glazga.core.model.eye;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.core.ULogic;
import kz.pompei.glazga.core.ULogicFocus;
import kz.pompei.glazga.core.model.eye.block.UBlock;
import kz.pompei.glazga.core.model.eye.expr.UExpr;
import kz.pompei.glazga.core.model.focus.FocusLogic;
import kz.pompei.glazga.core.model.focus.FocusLogicBond;
import kz.pompei.glazga.core.model.focus.FocusLogicDown;
import kz.pompei.glazga.core.model.focus.FocusLogicOwn;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.style.Styler;
import kz.pompei.glazga.paint.style.focus.Focusing;
import lombok.NonNull;

import static java.util.Objects.requireNonNull;
import static kz.pompei.glazga.core.utils.UUtil.findNextFocusLogic;

public abstract class UElement {
  public static final double SAVE_MIN_OFFSET = 0.1;

  public ULogic logic;

  public Focusing focusing = Focusing.NONE;

  public abstract String id();

  /**
   * Возвращает все ограды для внутренних выражений у данного U-элемента
   *
   * @return колода оград. Ключ колоды - имя ограды, уникальное в рамках данного U-элемента, значение колоды - ограда
   */
  public abstract @NonNull Map<String, Bond> bonds();

  public abstract @NonNull LinearTrans trans();

  public abstract @NonNull Styler styler();

  public abstract @NonNull Rect rect();

  public abstract boolean isFixed();

  public abstract void paint(Graphics2D g);

  public abstract @NonNull UType type();

  public abstract String focusState();

  public final List<FocusLogic> focusLogicLeftToRightList = new ArrayList<>();
  public final List<FocusLogic> focusLogicTopToDownList   = new ArrayList<>();

  protected abstract @NonNull Collection<FocusPlace> calculateOwnFocuses();

  public void validateAfterAllInitializations() {
    requireNonNull(bonds(), "C54TyOfPmK :: havingBonds() == null");
    requireNonNull(trans(), "j41O5GAu3K :: trans() == null");
    requireNonNull(styler(), "75v1alM4RL :: styler() == null");
  }

  public void calcGeometry() {
    // override it
  }

  public abstract UElement up();

  public void initFocusGeometry() {
    {
      if (this instanceof UBlock<?> block) {
        block.downs().values().stream().map(FocusLogicDown::new).forEach(focusLogicLeftToRightList::add);
      }
      bonds().values().stream().map(FocusLogicBond::new).forEach(focusLogicLeftToRightList::add);
      calculateOwnFocuses().stream()
                           .map(src -> new FocusLogicOwn(src, this))
                           .forEach(focusLogicLeftToRightList::add);
    }

    focusLogicTopToDownList.addAll(focusLogicLeftToRightList);

    focusLogicLeftToRightList.sort(Comparator.comparingDouble(x -> x.rect().center().x));
    focusLogicTopToDownList.sort(Comparator.comparingDouble(x -> x.rect().center().y));
  }

  @Override
  public final String toString() {
    String viewInfo = viewInfo();
    return getClass().getSimpleName() + "{id=" + id() + (viewInfo == null ? "" : ", " + viewInfo) + "}";
  }

  protected String viewInfo() {
    return null;
  }

  protected Map<String, FocusPlace> printFocusMap(Map<String, FocusPlace> mapToPrint) {
    System.out.println();
    System.out.println("5dBNtH05IP :: " + this + " FocusMap");
    mapToPrint.entrySet()
              .stream()
              .sorted(Map.Entry.comparingByKey())
              .forEach(e -> System.out.println("     " + e.getValue()));
    System.out.println();
    return mapToPrint;
  }

  public boolean equalTo(UElement another) {
    if (this == another) {
      return true;
    }
    return id().equals(another.id()) && type() == another.type();
  }

  public void moveFocus(@NonNull FocusMove focusMove) {
    moveFocus(focusMove, this::focusLogicEqualsTo);
  }

  public void moveFocus(@NonNull FocusMove focusMove, Predicate<FocusLogic> isCurrent) {
    logic.log().focus().element_moveFocus(this, focusMove);
    {
      FocusLogic focusLogic = findNextFocusLogic(focusMove,
                                                 focusLogicLeftToRightList,
                                                 focusLogicTopToDownList,
                                                 isCurrent,
                                                 logic.focus()::getLeft,
                                                 logic.focus()::getTop);

      if (focusLogic != null) {
        focusLogic.takeFocusAndRepaint(focusMove);
        return;
      }
    }

    if (this instanceof UBlock<?> thisBlock) {
      logic.blockGivesFocusToEye(thisBlock, focusMove);
      return;
    }

    if (this instanceof UExpr<?> thisExpr) {

      Bond bondOwner = thisExpr.bondOwner();
      if (bondOwner == null) {
        logic.exprGivesFocusToEye(thisExpr, focusMove);
        return;
      }

      correctFocusPlaceExitingFromThisElement(focusMove);

      bondOwner.owner.childBondMovesFocus(bondOwner, focusMove);
      return;
    }

    throw new RuntimeException("WgW8mUSTbM :: Unknown child class " + getClass().getSimpleName()
                                 + " of " + UElement.class.getSimpleName());
  }

  private void correctFocusPlaceExitingFromThisElement(FocusMove focusMove) {

    Rect rect = rect();

    ULogicFocus focus = logic.focus();

    if (focusMove.isHorizontal()) {
      Double top = focus.getTop();
      if (top == null || top < rect.top || rect.bottom < top) {
        focus.setTop(rect.center().y);
      }
    } else {
      Double left = focus.getLeft();
      if (left == null || left < rect.left || rect.right < left) {
        focus.setLeft(rect.center().x);
      }
    }
  }

  private void childBondMovesFocus(@NonNull Bond bond, @NonNull FocusMove focusMove) {
    logic.log().focus().element_childBondMovesFocus(this, bond, focusMove);
    moveFocus(focusMove, fl -> fl instanceof FocusLogicBond b && b.bond.eq(bond));
  }

  private boolean focusLogicEqualsTo(FocusLogic checkingFocusLogic) {
    return checkingFocusLogic instanceof FocusLogicOwn checking
      && this.equalTo(checking.owner)
      && Objects.equals(focusState(), checking.place.state());
  }

  /**
   * Должен пройтись по всем внутренним элементам, найти самый маленький, тот который содержит данную позицию.
   * Если таковой нашёлся, то ставит на него фокус и возвращает Да. Если не нашёл, должен вернуть Нет,
   * чтобы сверху продолжили искать дальше
   *
   * @param pos позиция, куда нужно поместить фокус
   * @return признак того, что нужный элемент найден и фокус на него установлен. Да - если найден и установлен. Нет - если нет
   */
  public boolean takeFocusAt(@NonNull Vec pos) {
    logic.log().focus().element_takeFocusAt(this, pos);

    if (!rect().containsVec(pos)) {
      return false;
    }

    for (final FocusLogic focusLogic : focusLogicLeftToRightList) {
      if (focusLogic.takeFocusAt(pos)) {
        return true;
      }
    }

    {
      ULogicFocus focus = logic.focus();
      focus.selectElement(type(), id());
      focus.setLeft(pos.x);
      focus.setTop(pos.y);
      focus.saveAndRefreshFocus();
    }
    return true;
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof final UElement other)) {
      return false;
    }

    return type() == other.type() && Objects.equals(id(), other.id());

  }

  @Override
  public int hashCode() {
    String id = id();
    return id == null ? 17 : id.hashCode();
  }
}
