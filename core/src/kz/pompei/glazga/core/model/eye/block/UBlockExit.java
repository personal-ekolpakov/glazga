package kz.pompei.glazga.core.model.eye.block;

import java.awt.Graphics2D;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.core.model.focus.FocusPlaceFix;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.mat.Size;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.painter.text.PaintTextBase;
import kz.pompei.glazga.paint.painter.text.PainterText;
import kz.pompei.glazga.paint.style.StylerBlockExit;
import kz.pompei.glazga.paint.style.focus.Focusing;
import kz.pompei.glazga.server_face.model.eye.block.BlockExit;
import kz.pompei.glazga.server_face.model.eye.block.exit.ExitKind;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

@UsedOut
public class UBlockExit extends UBlock<BlockExit> {

  @Override
  public void paint(Graphics2D g) {

    PainterPath.withTrans(trans())
               .over(it -> myStyle().applyAroundBorder(it))
               .rect(rect())
               .paint(g);

    painter_displayLabel().pos(leftTop_displayLabel()).paintBase(PaintTextBase.LEFT_TOP)
                          .paint(g);

    if (focusing == Focusing.IS) {
      PainterPath.withTrans(trans())
                 .over(it -> myStyle().applyFocusRect(it))
                 .rect(focusRect_displayLabel())
                 .paint(g);
    }

  }

  private Vec leftTop_displayLabel() {
    SideSpaces blockPadding = myStyle().padding();
    return leftTop().plus(blockPadding.left, blockPadding.top);
  }

  private PainterText painter_displayLabel() {
    return PainterText.text(displayLabel())
                      .over(it -> myStyle().applyDisplayLabel(it))
                      .trans(trans());
  }

  private Rect focusRect_displayLabel() {

    Vec textLeftTop = leftTop_displayLabel();

    PainterText painterText = painter_displayLabel();

    double fontHeight = painterText.fontHeight();
    double width      = painterText.width();

    Rect textRect = Rect.over(textLeftTop, Size.of(width, fontHeight));

    return textRect.expand(myStyle().displayLabelFocusMargin());
  }

  private StylerBlockExit myStyle() {
    return styler().blockExit();
  }

  @Override
  protected double calcWidth() {
    double textWidth = PainterText.text(displayLabel())
                                  .over(it -> myStyle().applyDisplayLabel(it))
                                  .trans(trans())
                                  .width();

    SideSpaces blockPadding = myStyle().padding();
    return blockPadding.left + textWidth + blockPadding.right;
  }

  @Override
  protected double calcHeight() {
    double textHeight = PainterText.text(displayLabel())
                                   .over(it -> myStyle().applyDisplayLabel(it))
                                   .trans(trans())
                                   .height()
                                   .total();

    SideSpaces blockPadding = myStyle().padding();
    return blockPadding.top + textHeight + blockPadding.bottom;
  }


  private String displayLabel() {
    ExitKind exitKind = data().kind;
    if (exitKind == null) {
      return "<Выберите тип выхода>";
    }
    return switch (exitKind) {
      case THROW -> "Сгенерировать исключение";
      case CIRCLE_CONTINUE -> "Продолжить цикл";
      case FROM_METHOD -> "Выйти из метода";
      case FROM_CIRCLE -> "Выйти из цикла";
    };
  }

  @Override
  public @NonNull Map<String, Bond> bonds() {
    return Map.of();
  }

  @Override
  public Map<String, UDown> downs() {
    return Map.of();
  }

  @Override
  protected @NonNull Collection<FocusPlace> calculateOwnFocuses() {
    return List.of(FocusPlaceFix.of("main", focusRect_displayLabel()));
  }

  @Override
  public String focusState() {
    return "main";
  }

  @Override
  protected String viewInfo() {
    return "label=" + displayLabel();
  }
}
