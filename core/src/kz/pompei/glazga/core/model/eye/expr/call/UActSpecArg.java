package kz.pompei.glazga.core.model.eye.expr.call;

import java.awt.Graphics2D;
import java.util.Map;
import java.util.stream.Stream;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.BondOwnerInteraction;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.core.model.focus.FocusPlaceFix;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.painter.text.PaintTextBase;
import kz.pompei.glazga.paint.painter.text.PainterText;
import kz.pompei.glazga.paint.style.StylerExprCall;
import kz.pompei.glazga.paint.style.expr_call.ExprCallArgStyleProps;
import kz.pompei.glazga.server_face.model.eye.etc.ActSpecArg;
import lombok.NonNull;

import static java.util.Objects.requireNonNull;

public class UActSpecArg implements UArgNeighbour, ExprCallArgStyleProps {
  public final String     argId;
  public final ActSpecArg data;
  public final UActSpec   owner;
  public final Bond       bond;

  private UArgNeighbour left;
  private boolean       first;
  private boolean       last;
  private int           indexInRow;

  public static class __init__ {
    public static void setLeft(UActSpecArg a, UArgNeighbour left) {
      a.left = left;
    }

    public static void setFirst(UActSpecArg a, boolean first) {
      a.first = first;
    }

    public static void setLast(UActSpecArg a, boolean last) {
      a.last = last;
    }

    public static void setIndexInRow(UActSpecArg a, int indexInRow) {
      a.indexInRow = indexInRow;
    }
  }

  public UActSpecArg(Map.Entry<String, ActSpecArg> e, UActSpec owner) {
    argId      = e.getKey();
    data       = e.getValue();
    this.owner = owner;
    bond       = new Bond(owner.owner, new BondOwnerInteraction() {
      @Override
      public @NonNull String name() {
        return "ARG-" + argId;
      }

      @Override
      public @NonNull Vec leftBaseLine() {
        return bond__LeftBaseLine();
      }

      @Override
      public String exprId() {
        return owner.owner.data().callArg(argId).map(x -> x.exprId).orElse(null);
      }

    });
  }

  public void validateAfterInit() {
    requireNonNull(argId, "1KeBNM1MFZ");
    requireNonNull(data, "GV2s1tf6i9");
    requireNonNull(owner, "Ncw17wSeDD");
    requireNonNull(bond, "uw3DPIKx1U");
    requireNonNull(left, "Vy049kCg8I");
  }

  private Vec leftBaseLine() {
    return left.neighbourLeftBaseLine().right(left.neighbourWidth());
  }

  private Rect actRect() {
    return Rect.over(leftBaseLine(), width(), height());
  }

  private Vec labelBefore__BaseLine() {
    return leftBaseLine().right(padding().left);
  }

  private String labelBefore__Text() {
    String s = data.labelBefore;
    return s == null ? "" : s;
  }

  private PainterText labelBefore__Painter() {
    return PainterText.text(labelBefore__Text())
                      .trans(trans())
                      .over(pts -> myStyle().applyLabelBeforeText(pts))
      ;
  }

  private Vec bond__LeftBaseLine() {
    return labelBefore__BaseLine().right(labelBefore__Painter().width());
  }

  private Vec labelAfter__BaseLine() {
    return bond__LeftBaseLine().right(bond.width());
  }

  private String labelAfter__Text() {
    String s = data.labelAfter;
    return s == null ? "" : s;
  }

  private PainterText labelAfter__Painter() {
    return PainterText.text(labelAfter__Text())
                      .trans(trans())
                      .over(pts -> myStyle().applyLabelAfterText(pts));
  }

  public void paint(Graphics2D g) {

    PainterPath.withTrans(trans())
               .over(pps -> myStyle().actRectApply(pps))
               .rect(actRect())
               .paint(g);

    labelBefore__Painter().pos(labelBefore__BaseLine())
                          .paintBase(PaintTextBase.LEFT_BASE_LINE)
                          .paint(g);

    bond.paint(g);

    labelAfter__Painter().pos(labelAfter__BaseLine())
                         .paintBase(PaintTextBase.LEFT_BASE_LINE)
                         .paint(g);
  }

  @Override
  public boolean first() {
    return first;
  }

  @Override
  public boolean last() {
    return last;
  }

  @Override
  public int indexInRow() {
    return indexInRow;
  }

  private StylerExprCall myStyle() {
    return owner.owner.styler().exprCall();
  }

  private SideSpaces padding() {
    return myStyle().argPadding(this);
  }

  private LinearTrans trans() {
    return owner.owner.trans();
  }

  public double width() {
    return padding().left + labelBefore__Painter().width() + bond.width() + labelAfter__Painter().width() + padding().right;
  }

  public ExprHeight height() {
    ExprHeight ret = ExprHeight.zero();

    ret = ret.combine(labelBefore__Painter().height());
    ret = ret.combine(bond.height());
    ret = ret.combine(labelAfter__Painter().height());

    ret = ret.expand(padding());

    return ret;
  }

  @Override
  public double neighbourWidth() {
    return width();
  }

  @Override
  public Vec neighbourLeftBaseLine() {
    return leftBaseLine();
  }

  public Stream<FocusPlace> focuses() {
    Rect labelBeforeRect = labelBefore__Painter().visibleBounds().move(labelBefore__BaseLine());
    Rect labelAfterRect  = labelAfter__Painter().visibleBounds().move(labelAfter__BaseLine());

    SideSpaces focusMargin = myStyle().focusMargin();

    Rect labelBeforeFocusRect = labelBeforeRect.expand(focusMargin);
    Rect labelAfterFocusRect  = labelAfterRect.expand(focusMargin);

    return Stream.of(FocusPlaceFix.of("before-" + argId, labelBeforeFocusRect),
                     FocusPlaceFix.of("after-" + argId, labelAfterFocusRect));
  }
}
