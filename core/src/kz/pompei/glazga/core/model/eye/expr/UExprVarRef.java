package kz.pompei.glazga.core.model.eye.expr;

import java.awt.Graphics2D;
import java.util.Collection;
import java.util.Map;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.server_face.model.eye.expr.ExprVarRef;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

@UsedOut
public class UExprVarRef extends UExpr<ExprVarRef> {

  @Override
  public void paint(Graphics2D g) {
    throw new RuntimeException("SQ7R343sEj");
  }

  @Override
  protected double calcWidth() {
    throw new RuntimeException("V535vso6kZ");
  }

  @Override
  protected @NonNull ExprHeight calcHeight() {
    throw new RuntimeException("JcQMwCnp24");
  }

  @Override
  public @NonNull Map<String, Bond> bonds() {
    throw new RuntimeException("4WgCs3lOuo");
  }

  @Override
  protected @NonNull Collection<FocusPlace> calculateOwnFocuses() {
    throw new RuntimeException("06.08.2023 06:34 Not impl yet UExprVarRef.calculateOwnFocuses()");
  }

  @Override
  protected String viewInfo() {
    return "defBlockId=" + data().varDefBlockId;
  }

  @Override
  public String focusState() {
    throw new RuntimeException("13.08.2023 18:22 Not impl yet UExprVarRef.focusState()");
  }
}
