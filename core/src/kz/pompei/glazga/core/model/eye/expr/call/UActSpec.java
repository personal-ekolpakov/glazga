package kz.pompei.glazga.core.model.eye.expr.call;

import java.awt.Graphics2D;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;
import kz.pompei.glazga.core.model.eye.expr.UExprCall;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.core.model.focus.FocusPlaceFix;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.text.PaintTextBase;
import kz.pompei.glazga.paint.painter.text.PainterText;
import kz.pompei.glazga.server_face.model.eye.etc.ActSpec;

import static java.util.Objects.requireNonNull;

public class UActSpec implements UArgNeighbour {
  public final UExprCall         owner;
  public final ActSpec           data;
  public final List<UActSpecArg> args;

  public UActSpec(UExprCall owner, ActSpec data) {
    this.owner = owner;
    this.data  = data;
    if (data == null || data.args == null) {
      args = List.of();
    } else {
      args = data.args.entrySet()
                      .stream()
                      .map(e -> new UActSpecArg(e, this))
                      .sorted(Comparator.comparing(x -> x.data.nativeOrder))
                      .toList();

      for (int i = 0; i < args.size(); i++) {
        UActSpecArg.__init__.setIndexInRow(args.get(i), i);
        UActSpecArg.__init__.setLeft(args.get(i), i == 0 ? this : args.get(i - 1));
      }
      if (args.size() > 0) {
        UActSpecArg.__init__.setFirst(args.get(0), true);
        UActSpecArg.__init__.setLast(args.get(args.size() - 1), true);
      }
    }
  }

  public void validateAfterInit() {
    requireNonNull(owner, "g7pS56Xr83 :: owner == null");
    requireNonNull(args, "eQp0qFXq2t :: args == null");
    args.forEach(UActSpecArg::validateAfterInit);
  }

  public String label() {
    return data != null ? (data.label != null ? "." + data.label : ".<Нет имени>")
      /* ->data==null*/ : (".<Не найден метод с ИД=" + owner.data().actId + ">");
  }

  private boolean actNotFound() {
    return data == null;
  }

  public Vec labelBaseLine() {
    return owner.leftBaseLine().right(owner.leftExprBond.width());
  }

  public PainterText labelPainter() {
    return PainterText.text(label())
                      .trans(owner.trans())
                      .over(it -> owner.styler().exprCall().actLabelApply(it, actNotFound()));
  }

  public double labelWidth() {
    return labelPainter().width();
  }

  public ExprHeight labelHeight() {
    return labelPainter().height();
  }

  public void paint(Graphics2D g) {
    labelPainter().paintBase(PaintTextBase.LEFT_BASE_LINE)
                  .pos(labelBaseLine())
                  .paint(g);

    args.forEach(a -> a.paint(g));
  }

  public double width() {
    return labelWidth() + args.stream().mapToDouble(UActSpecArg::width).sum();
  }

  public ExprHeight height() {
    return args.stream()
               .map(UActSpecArg::height)
               .reduce(labelHeight(), ExprHeight::combine);
  }

  @Override
  public double neighbourWidth() {
    return labelWidth();
  }

  @Override
  public Vec neighbourLeftBaseLine() {
    return labelBaseLine();
  }

  public Stream<FocusPlace> focuses() {
    Rect labelFocusRect = Rect.over(labelBaseLine(), labelWidth(), labelHeight()).expand(owner.myStyle().focusMargin());

    return Stream.of(FocusPlaceFix.of(FOCUS_STATE_METHOD_NAME, labelFocusRect));
  }

  public static final String FOCUS_STATE_METHOD_NAME = "methodName";
}
