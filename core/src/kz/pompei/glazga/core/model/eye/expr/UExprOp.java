package kz.pompei.glazga.core.model.eye.expr;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.BondOwnerInteraction;
import kz.pompei.glazga.core.model.eye.expr.op.ArgsInteract;
import kz.pompei.glazga.core.model.eye.expr.op.UOpArg;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.core.model.focus.FocusPlaceFix;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.style.focus.Focusing;
import kz.pompei.glazga.server_face.model.eye.expr.ExprOp;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;
import static kz.pompei.glazga.core.model.eye.expr.op.UOpArg.opToStr;

@UsedOut
public class UExprOp extends UExpr<ExprOp> implements ArgsInteract {

  private Bond         firstBond;
  private List<UOpArg> args = new ArrayList<>();

  @Override
  protected void initBonds() {

    firstBond = new Bond(this, new BondOwnerInteraction() {
      @Override
      public @NonNull String name() {
        return "FIRST";
      }

      @Override
      public @NonNull Vec leftBaseLine() {
        return firstLeftBaseLine();
      }

      @Override
      public String exprId() {
        return data().firstExprId;
      }

    });

    args = Optional.ofNullable(data().args)
                   .orElseGet(HashMap::new)
                   .entrySet()
                   .stream()
                   .map(e -> new UOpArg(e, this))
                   .sorted(Comparator.comparing(x -> x.data.order))
                   .toList();

    if (args.size() > 0) {
      UOpArg.__init__.setLeft(args.get(0), this);
      for (int i = 1; i < args.size(); i++) {
        UOpArg.__init__.setLeft(args.get(i), args.get(i - 1));
      }

      for (int i = 0; i < args.size(); i++) {
        UOpArg.__init__.setArgIndex(args.get(i), i);
      }
    }
  }

  private Vec firstLeftBaseLine() {
    return leftBaseLine();
  }

  @Override
  public Vec interactLeftBaseLine() {
    return leftBaseLine();
  }

  @Override
  public double interactWidth() {
    return useFirst() ? firstBond.width() : 0;
  }

  @Override
  public @NonNull Map<String, Bond> bonds() {
    List<Bond> ret = new ArrayList<>();
    if (data().useFirst) {
      ret.add(firstBond);
    }
    args.stream().map(x -> x.bond).forEach(ret::add);
    return Bond.map(ret);
  }

  @Override
  public void paint(Graphics2D g) {
    if (useFirst()) {
      firstBond.paint(g);
    }

    args.forEach(x -> x.paint(g));

    if (focusing == Focusing.IS) {
      PainterPath.withTrans(trans())
                 .over(it -> styler().exprOp().applyFocusUnderline(it, focusing))
                 .moveTo(Rect.over(leftBaseLine(), width(), height()).leftBottom())
                 .right(width())
                 .paint(g);

      String focusState = focusState();
      if (focusState != null) {
        FocusPlace focusPlace = focusMap().get(focusState);
        if (focusPlace != null) {
          Rect    rect  = focusPlace.rect();
          boolean right = focusState.startsWith("b");

          PainterPath.withTrans(trans())
                     .over(it -> styler().exprOp().applyFocusCursor(it, focusing))
                     .moveTo(right ? rect.rightBottom() : rect.leftBottom())
                     .up(rect.height())
                     .paint(g);

        }
      }
    }
  }

  @Override
  public String focusState() {
    String focusState = logic.focus().getState();
    if (focusMap().containsKey(focusState)) {
      return focusState;
    }
    if (useFirst()) {
      return FOCUS_STATE_FIRST;
    }
    if (args.size() > 0) {
      return "a0";
    }
    return null;
  }

  @Override
  protected double calcWidth() {
    double ret = 0;
    if (useFirst()) {
      ret += firstBond.width();
    }

    ret += args.stream().mapToDouble(UOpArg::width).sum();

    return ret;
  }

  private boolean useFirst() {
    return data().useFirst;
  }

  @Override
  protected @NonNull ExprHeight calcHeight() {

    List<ExprHeight> heights = new ArrayList<>();

    if (useFirst()) {
      heights.add(firstBond.height());
    }

    args.stream().map(UOpArg::height).forEach(heights::add);

    return heights.stream().reduce(ExprHeight.zero(), ExprHeight::combine);
  }

  @Override
  protected @NonNull Collection<FocusPlace> calculateOwnFocuses() {
    return focusMap().values();
  }

  private Map<String, FocusPlace> focusMap = null;

  private Map<String, FocusPlace> focusMap() {
    {
      var map = focusMap;
      if (map != null) {
        return map;
      }
    }

    return printFocusMap(focusMap = createFocuses().collect(toMap(FocusPlace::state, x -> x)));
  }

  private Stream<FocusPlace> createFocuses() {
    return Stream.concat(createFirstFocuses(),
                         args.stream().flatMap(uOpArg -> uOpArg.focuses(height())));

  }

  private static final String FOCUS_STATE_FIRST = "first";

  private Stream<FocusPlace> createFirstFocuses() {
    return !useFirst() ? Stream.of() : Stream.of(FocusPlaceFix.of(FOCUS_STATE_FIRST, Rect.over(firstLeftBaseLine(), 0, height())));
  }

  @Override
  protected String viewInfo() {
    return args.stream().map(x -> opToStr(x.data.op)).collect(joining(" ", "[", "]"));
  }

}
