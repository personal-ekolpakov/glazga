package kz.pompei.glazga.core.model.eye.expr;

import java.awt.Graphics2D;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.BondOwnerInteraction;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.core.model.focus.FocusPlaceFix;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.mat.Size;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.style.StylerExprDiv;
import kz.pompei.glazga.server_face.model.eye.expr.ExprDiv;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

@UsedOut
public class UExprDiv extends UExpr<ExprDiv> {

  private final Bond up = new Bond(this, new BondOwnerInteraction() {

    @Override
    public @NonNull String name() {
      return "UP";
    }

    @Override
    public @NonNull Vec leftBaseLine() {
      return upLeftBaseLine();
    }

    @Override
    public String exprId() {
      return data().upExprId;
    }

  });

  private final Bond down = new Bond(this, new BondOwnerInteraction() {

    @Override
    public @NonNull String name() {
      return "DOWN";
    }

    @Override
    public @NonNull Vec leftBaseLine() {
      return downLeftBaseLine();
    }

    @Override
    public String exprId() {
      return data().downExprId;
    }

  });

  private Vec upLeftBaseLine() {
    return center().left(up.width() / 2).up(upHeight().down);
  }

  private Vec downLeftBaseLine() {
    return center().left(down.width() / 2).down(downHeight().up);
  }

  private Vec center() {
    return startLine().right(width() / 2);
  }

  private Vec startLine() {
    return leftBaseLine().up(shift());
  }

  @Override
  public void paint(Graphics2D g) {

    PainterPath.withTrans(trans())
               .over(it -> myStyle().applyBaseLineStyle(it))
               .moveTo(startLine()).right(width())
               .paint(g);

    up.paint(g);
    down.paint(g);

    PainterPath.withTrans(trans())
               .over(it -> myStyle().divLineFocusStyle(it, focusing))
               .rect(focusRect())
               .paint(g);
  }

  private Rect focusRect() {
    return Rect.from(startLine()).rightWidth(width()).expand(myStyle().divLineFocusMargin());
  }

  private double upWidth() {
    SideSpaces upPadding   = myStyle().paddingUp();
    double     upExprWidth = up.width();
    return upPadding.left + upExprWidth + upPadding.right;
  }

  private ExprHeight upHeight() {
    SideSpaces upPadding    = myStyle().paddingUp();
    ExprHeight upExprHeight = up.height();

    double up   = upPadding.top + upExprHeight.up;
    double down = upExprHeight.down + upPadding.bottom;

    return ExprHeight.of(up, down);
  }

  private double downWidth() {
    SideSpaces downPadding   = myStyle().paddingDown();
    double     downExprWidth = down.width();
    return downPadding.left + downExprWidth + downPadding.right;
  }

  private StylerExprDiv myStyle() {
    return styler().exprDiv();
  }

  private ExprHeight downHeight() {
    SideSpaces downPadding    = myStyle().paddingDown();
    ExprHeight downExprHeight = down.height();

    double up   = downPadding.top + downExprHeight.up;
    double down = downExprHeight.down + downPadding.bottom;

    return ExprHeight.of(up, down);
  }

  @Override
  protected double calcWidth() {
    return Math.max(upWidth(), downWidth());
  }

  private double shift() {
    return myStyle().baseLineShift();
  }

  @Override
  protected @NonNull ExprHeight calcHeight() {

    double heightUP   = upHeight().total() + shift();
    double heightDOWN = downHeight().total() - shift();

    heightUP   = heightUP < 0 ? 0 : heightUP;
    heightDOWN = heightDOWN < 0 ? 0 : heightDOWN;

    return ExprHeight.of(heightUP, heightDOWN);
  }

  @Override
  public @NonNull Map<String, Bond> bonds() {
    return Bond.map(up, down);
  }

  @Override
  protected @NonNull Collection<FocusPlace> calculateOwnFocuses() {
    return List.of(FocusPlaceFix.of("divLine", Rect.from(startLine()).to(Size.of(width(), 0))));
  }

  @Override
  public String focusState() {
    return "divLine";
  }
}
