package kz.pompei.glazga.core.model.eye.expr;

import java.util.function.Supplier;
import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.UElement;
import kz.pompei.glazga.core.utils.ModelUtil;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.style.Styler;
import kz.pompei.glazga.server_face.model.eye.expr.Expr;
import kz.pompei.glazga.server_face.model.eye.expr.ExprFix;
import lombok.NonNull;
import lombok.experimental.FieldNameConstants;

import static java.util.Objects.requireNonNull;

@FieldNameConstants
public abstract class UExpr<Data extends Expr> extends UElement {
  private String           id;
  private Data             data;
  private Supplier<Styler> styler;

  private Bond bondOwner;
  private boolean refreshLeftTop = true;
  private Vec     leftBase;

  private boolean    refreshWidth  = true;
  private double     width;
  private boolean    refreshHeight = true;
  private ExprHeight height;

  public static class __initExpr__ {
    public static void setBondOwner(UExpr<?> expr, Bond bondOwner) {
      expr.bondOwner = bondOwner;
    }
  }

  private String idn() {
    return getClass().getSimpleName() + "{id = " + id + '}';
  }

  @Override
  public void validateAfterAllInitializations() {
    super.validateAfterAllInitializations();
    requireNonNull(bondOwner, "63V81uF8HH :: holder == null of " + idn());
    requireNonNull(id, "09nCXaVK37 :: id == null of " + idn());
    requireNonNull(data, "04QXIcbNTo :: data == null of " + idn());
    requireNonNull(logic, "q60RcCJf21 :: logic == null of " + idn());
  }

  public static <Data extends Expr> void init(UExpr<Data> expr, ModelUtil.UInit<Data> init) {
    requireNonNull(init.id, "4dy3EIR0y5 :: init.id == null");
    requireNonNull(init.data, "zw9x0UOlaQ :: init.data == null");
    requireNonNull(init.styler, "Xm907i9b2N :: init.styler == null");
    requireNonNull(init.uLogic, "ojjC1MH50k :: init.uLogic == null");
    expr.id     = init.id;
    expr.data   = init.data;
    expr.styler = init.styler;
    expr.logic  = init.uLogic;
    expr.initBonds();
  }

  @Override
  public final @NonNull UType type() {
    return UType.EXPR;
  }

  @Override
  public UElement up() {
    Bond h = bondOwner;
    if (h == null) {
      return null;
    }
    return h.owner;
  }

  protected void initBonds() {}

  @Override
  public @NonNull LinearTrans trans() {
    return logic.trans();
  }

  public String id() {
    return id;
  }

  public Data data() {
    return data;
  }

  @Override
  public @NonNull Styler styler() {
    return styler.get();
  }

  protected abstract double calcWidth();

  protected abstract @NonNull ExprHeight calcHeight();

  @Override
  public final boolean isFixed() {
    return data() instanceof ExprFix;
  }

  public final Vec leftBaseLine() {
    if (data instanceof ExprFix bf) {
      return requireNonNull(bf.leftTop(), "NlBnSgDf3C :: Calculated leftTop in null");
    }

    {
      Vec lb = leftBase;
      if (lb != null && !refreshLeftTop) {
        return lb;
      }
    }

    {
      Vec ret = leftBase = calcLeftBaseLine();
      refreshLeftTop = false;
      return ret;
    }
  }

  protected Vec calcLeftBaseLine() {
    if (data instanceof ExprFix bf) {
      return requireNonNull(bf.leftTop(), "rYi1vJmnLZ :: Calculated leftTop in null");
    }

    Bond owner = this.bondOwner;
    requireNonNull(owner,
                   "bCvSuo9ZlT :: Bond owner may be null only in " + UExprFix.class.getSimpleName()
                     + " not in " + getClass().getSimpleName() + " with id = `" + id() + "`");

    return owner.leftBaseLine();
  }

  public double width() {
    if (!refreshWidth) {
      return width;
    }
    {
      double oldWidth = width;
      double newWidth = width = calcWidth();
      refreshWidth = false;
      if (Math.abs(oldWidth - newWidth) <= UElement.SAVE_MIN_OFFSET) {
        String exprId = id();
        logic.elementSizes().saveElementWidth(UType.EXPR, exprId, newWidth);
      }
      return newWidth;
    }
  }

  public ExprHeight height() {
    if (height != null && !refreshHeight) {
      return height;
    }
    {
      ExprHeight oldHeight = height;
      ExprHeight newHeight = height = calcHeight();
      requireNonNull(newHeight, "PiC15S5sfn :: newHeight == null");
      refreshHeight = false;
      if (oldHeight == null
        || Math.abs(newHeight.up - oldHeight.up) > UElement.SAVE_MIN_OFFSET
        || Math.abs(newHeight.down - oldHeight.down) > UElement.SAVE_MIN_OFFSET) {
        String exprId = id();
        logic.elementSizes().saveExprHeight(exprId, newHeight);
      }
      return newHeight;
    }
  }

  @SuppressWarnings("BooleanMethodIsAlwaysInverted")
  public boolean hasHolder() {
    return bondOwner != null;
  }

  public Bond bondOwner() {
    return bondOwner;
  }

  public boolean debug() {
    return logic.debug();
  }

  @Override
  public @NonNull Rect rect() {
    return Rect.over(leftBaseLine(), width(), height());
  }
}
