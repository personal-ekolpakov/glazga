package kz.pompei.glazga.core.model.eye.block;

import java.util.Map;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.core.model.eye.UDownOwnerInteraction;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.server_face.model.eye.block.Block;

public abstract class UBlockDown<Data extends Block> extends UBlock<Data> {

  protected final UDown main = new UDown(this, new UDownOwnerInteraction() {
    @Override
    public String name() {
      return UDown.MAIN;
    }

    @Override
    public String downBlockId() {
      return data().downBlockId;
    }

    @Override
    public Vec downLeftTop() {
      return leftTop().down(height());
    }

    @Override
    public double width() {
      return rect().width();
    }
  });

  @Override
  public Map<String, UDown> downs() {
    return Map.of(main.name(), main);
  }

}
