package kz.pompei.glazga.core.model.eye;

import kz.pompei.glazga.mat.Vec;

/**
 * Связывает крепёж со своим хозяином
 */
public interface UDownOwnerInteraction {
  /**
   * Возвращает имя спуска к следующему блоку, которое должно быть уникальным в рамках блока-хозяина
   */
  String name();

  /**
   * Возвращает идентификатор блока, который должен быть подвешен снизу
   */
  String downBlockId();

  /**
   * Возвращает координаты левого верхнего угла блока, который удерживается данным крепежом
   */
  Vec downLeftTop();

  /**
   * Возвращает ширину соединительной линии
   */
  double width();
}
