package kz.pompei.glazga.core.model.eye.block;

import java.awt.Graphics2D;
import java.util.Collection;
import java.util.Map;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.server_face.model.eye.block.BlockAssign;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

@UsedOut
public class UBlockAssign extends UBlock<BlockAssign> {
  @Override
  public Map<String, UDown> downs() {
    throw new RuntimeException("Uzc3tmFZNz");
  }

  @Override
  public void paint(Graphics2D g) {
    throw new RuntimeException("WS77uYDd2u");
  }

  @Override
  protected double calcWidth() {
    throw new RuntimeException("4qWs4yTyp6");
  }

  @Override
  protected double calcHeight() {
    throw new RuntimeException("idGRM3nR6x");
  }

  @Override
  public @NonNull Map<String, Bond> bonds() {
    return Map.of();
  }

  @Override
  protected @NonNull Collection<FocusPlace> calculateOwnFocuses() {
    throw new RuntimeException("06.08.2023 06:35 Not impl yet UBlockAssign.calculateOwnFocuses()");
  }

  @Override
  public String focusState() {
    throw new RuntimeException("13.08.2023 18:07 Not impl yet UBlockAssign.focusState()");
  }
}
