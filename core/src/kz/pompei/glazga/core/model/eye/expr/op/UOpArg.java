package kz.pompei.glazga.core.model.eye.expr.op;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Map;
import java.util.stream.Stream;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.BondOwnerInteraction;
import kz.pompei.glazga.core.model.eye.expr.UExprOp;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.core.model.focus.FocusPlaceFix;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.painter.text.PainterText;
import kz.pompei.glazga.paint.style.StylerExprOp;
import kz.pompei.glazga.server_face.model.eye.expr.op.Op;
import kz.pompei.glazga.server_face.model.eye.expr.op.OpArg;
import lombok.NonNull;

import static kz.pompei.glazga.paint.painter.text.PaintTextBase.LEFT_BASE_LINE;

public class UOpArg implements ArgsInteract {

  public final String  argId;
  public final OpArg   data;
  public final UExprOp owner;
  public final Bond    bond;

  private ArgsInteract left; // This is NOT NULL after initialization.
  private int          argIndex;

  public UOpArg(Map.Entry<String, OpArg> e, UExprOp owner) {
    argId      = e.getKey();
    data       = e.getValue();
    this.owner = owner;

    bond = new Bond(owner, new BondOwnerInteraction() {

      @Override
      public @NonNull String name() {
        return "ARG-" + argId;
      }

      @Override
      public @NonNull Vec leftBaseLine() {
        return interactLeftBaseLine().right(signWidth());
      }

      @Override
      public String exprId() {
        return data.argExprId;
      }
    });
  }

  public static class __init__ {
    public static void setLeft(UOpArg arg, ArgsInteract left) {
      arg.left = left;
    }

    public static void setArgIndex(UOpArg arg, int argIndex) {
      arg.argIndex = argIndex;
    }
  }

  @Override
  public double interactWidth() {
    return width();
  }

  @Override
  public Vec interactLeftBaseLine() {
    return left.interactLeftBaseLine().right(left.interactWidth());
  }

  public int argIndex() {
    return argIndex;
  }

  private String signText() {
    return opToStr(data.op);
  }

  public static String opToStr(Op op) {
    if (op == null) {
      return "unknown";
    }
    return switch (op) {
      case PLUS -> "+";
      case MINUS -> "-";
      case MUL -> "·";
      case EQ -> "=";
      case NOT_EQ -> "≠";
      case OR -> "ИЛИ";
      case AND -> "И";
      case AND_NOT_EQ -> "И ≠";
      case OR_EQ -> "ИЛИ =";
      case NOT -> "НЕ";
      case CONCAT -> "◦";
      case XOR -> "ИЛИ-ИЛИ";
      case LESS -> "<";
      case LESS_EQ -> "⩽";
      case GREATER -> ">";
      case GREATER_EQ -> "⩾";
    };
  }

  private PainterText signPainter() {
    return PainterText.text(signText())
                      .trans(owner.trans())
                      .over(it -> myStyle().applySignStyle(it));
  }

  private double signWidth() {
    return signPainter().width();
  }

  private Vec textLeftBaseLine() {
    return interactLeftBaseLine();
  }

  public double width() {
    return signWidth() + bond.width();
  }

  public void paint(Graphics2D g) {

    if (owner.debug()) {

      PainterPath.withTrans(owner.trans())
                 .over(pps -> pps.pen().width(1).color(new Color(1, 1, 1)))
                 .rect(signRect())
                 .moveTo(textLeftBaseLine()).right(signPainter().pos(textLeftBaseLine()).paintBase(LEFT_BASE_LINE).width())
                 .scaleWidth(false)
                 .paint(g);

    }

    signPainter().pos(textLeftBaseLine()).paintBase(LEFT_BASE_LINE)
                 .paint(g);

    bond.paint(g);
  }

  private Rect signRect() {
    PainterText painterText = signPainter().pos(textLeftBaseLine()).paintBase(LEFT_BASE_LINE);
    return Rect.over(textLeftBaseLine(), painterText.width(), painterText.height());
  }

  private StylerExprOp myStyle() {
    return owner.styler().exprOp();
  }

  public ExprHeight height() {
    return signPainter().height().combine(bond.height());
  }

  public Rect rect() {
    return Rect.over(interactLeftBaseLine(), interactWidth(), height());
  }

  public Stream<FocusPlace> focuses(ExprHeight height) {

    double signWidth = signPainter().width();

    double width = signWidth / 2;

    Vec posA = textLeftBaseLine();
    Vec posB = posA.right(width);

    Rect rectA = Rect.over(posA, width, height);
    Rect rectB = Rect.over(posB, width, height);

    return Stream.of(FocusPlaceFix.of("a" + argIndex(), rectA),
                     FocusPlaceFix.of("b" + argIndex(), rectB));
  }
}
