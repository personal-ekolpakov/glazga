package kz.pompei.glazga.core.model.eye;

import java.awt.Graphics2D;
import kz.pompei.glazga.core.model.eye.block.UBlock;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.core.model.focus.MoveDirection;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Size;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;

import static java.util.Objects.requireNonNull;

/**
 * Крепёж блока внутри другого блока-хозяина
 */
public class UDown {
  public final  UBlock<?>             owner;
  private final UDownOwnerInteraction interaction;

  /**
   * Имя главного держателя, по которому определяется основная линия исполнения. Другие держатели содержат цепочки, которые сводятся к этой.
   */
  public static final String MAIN = "MAIN";

  private UBlock<?> downBlock;

  /**
   * Создаёт блок держатель другого блока
   *
   * @param owner       блок-хозяин данного держателя
   * @param interaction интерфейс для общения с блоком-хозяином, содержащий отличия данного держателя от других держателей блоков в блоке-хозяине
   */
  public UDown(UBlock<?> owner, UDownOwnerInteraction interaction) {
    requireNonNull(owner, "QXi6S9h2fr");
    requireNonNull(interaction, "KB79WVWj7y");
    this.owner       = owner;
    this.interaction = interaction;
  }

  public static class __initDown__ {
    public static void setDownBlock(UDown self, UBlock<?> down) {
      self.downBlock = down;
    }
  }

  public UBlock<?> downBlock() {
    return downBlock;
  }

  public Vec downLeftTop() {
    return interaction.downLeftTop();
  }

  public String name() {
    return interaction.name();
  }

  public String downBlockId() {
    return interaction.downBlockId();
  }

  public double connectLineWidth() {
    UBlock<?> d = downBlock();
    return d != null ? Math.max(interaction.width(), d.width()) : interaction.width();
  }

  public Rect rect() {
    return Rect.over(downLeftTop(), Size.of(connectLineWidth(), 0));
  }

  public void paintFocused(Graphics2D g) {

    PainterPath.withTrans(owner.trans())
               .over(it -> owner.styler().down().applyFocusedLine(it))
               .moveTo(interaction.downLeftTop()).right(connectLineWidth())
               .paint(g);

  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{name=`" + name() + "`, downBlockId=`" + downBlockId() + "` of " + owner + "}";
  }

  public void moveFocus(FocusMove focusMove) {
    owner.logic.log().focus().down_moveFocus(this, focusMove);

    if (focusMove.direction == MoveDirection.TO_DOWN) {

      UBlock<?> downBlock = downBlock();
      if (downBlock != null) {
        downBlock.moveFocus(focusMove);
        return;
      }

      owner.logic.downGivesFocusToEye(this, focusMove);
      return;
    }

    throw new RuntimeException("TLxP5RsULR " + focusMove);
  }
}
