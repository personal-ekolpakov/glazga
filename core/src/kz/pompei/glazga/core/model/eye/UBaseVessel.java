package kz.pompei.glazga.core.model.eye;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.core.model.eye.block.UBlock;
import kz.pompei.glazga.core.model.eye.expr.UExpr;
import kz.pompei.glazga.mat.Vec;
import lombok.NonNull;

public class UBaseVessel implements Iterable<UElement> {
  public final Map<String, UBlock<?>> blocks      = new HashMap<>();
  public final Map<String, UExpr<?>>  expressions = new HashMap<>();

  @NonNull
  @Override
  public Iterator<UElement> iterator() {
    return new Iterator<>() {
      final Iterator<UBlock<?>> blockIterator = blocks.values().iterator();
      Iterator<UExpr<?>> exprIterator = null;

      @Override
      public boolean hasNext() {
        var exprIterator = this.exprIterator;
        if (exprIterator == null) {
          if (blockIterator.hasNext()) {
            return true;
          }
          this.exprIterator = expressions.values().iterator();
        }
        return this.exprIterator.hasNext();
      }

      @Override
      public UElement next() {
        var exprIterator = this.exprIterator;
        return exprIterator == null ? blockIterator.next() : exprIterator.next();
      }
    };
  }

  public Stream<UElement> stream() {
    Spliterator<UElement> spliterator = Spliterators.spliteratorUnknownSize(iterator(), Spliterator.ORDERED);
    return StreamSupport.stream(spliterator, false);
  }

  public UElement getElement(UType uType, String id) {
    if (id == null) {
      return null;
    }
    if (uType == UType.BLOCK) {
      return blocks.get(id);
    }
    if (uType == UType.EXPR) {
      return expressions.get(id);
    }
    return null;
  }

  public UElement findHit(Vec position) {

    List<UBlock<?>> foundBlocks = new ArrayList<>();

    for (final UBlock<?> block : blocks.values()) {
      if (block.rect().containsVec(position)) {
        foundBlocks.add(block);
      }
    }

    List<UExpr<?>> foundExprList = new ArrayList<>();

    for (final UBlock<?> block : foundBlocks) {
      for (final Bond bond : block.bonds().values()) {
        UExpr<?> expr = bond.bondExpr();
        if (expr != null) {
          if (expr.rect().containsVec(position)) {
            foundExprList.add(expr);
          }
        }
      }
    }

    while (true) {
      List<UExpr<?>> newFoundExprList = new ArrayList<>();
      boolean        leaveSame        = true;

      for (final UExpr<?> expr : foundExprList) {

        List<? extends UExpr<?>> subHits = expr.bonds()
                                               .values()
                                               .stream()
                                               .map(Bond::bondExpr)
                                               .filter(Objects::nonNull)
                                               .filter(x -> x.rect().containsVec(position))
                                               .toList();

        if (subHits.isEmpty()) {
          newFoundExprList.add(expr);
        } else {
          newFoundExprList.addAll(subHits);
          leaveSame = false;
        }
      }

      if (leaveSame) {
        break;
      }

      foundExprList = newFoundExprList;
    }

    if (foundExprList.size() > 0) {
      return foundExprList.get(0);
    }

    if (foundBlocks.size() > 0) {
      return foundBlocks.get(0);
    }

    return null;
  }


}
