package kz.pompei.glazga.core.model.eye.expr;

import java.awt.Graphics2D;
import java.util.Collection;
import java.util.Map;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.server_face.model.eye.expr.ExprFix;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

@UsedOut
public class UExprFix extends UExpr<ExprFix> {

  @Override
  public void paint(Graphics2D g) {
    // nothing to paint
  }

  @Override
  protected double calcWidth() {
    return 0;
  }

  @Override
  protected @NonNull ExprHeight calcHeight() {
    return ExprHeight.of(0, 0);
  }

  @Override
  public @NonNull Map<String, Bond> bonds() {
    throw new RuntimeException("gRqY7ndgXu");
  }

  @Override
  protected @NonNull Collection<FocusPlace> calculateOwnFocuses() {
    throw new RuntimeException("06.08.2023 06:34 Not impl yet UExprFix.calculateOwnFocuses()");
  }

  @Override
  public String focusState() {
    throw new RuntimeException("13.08.2023 18:16 Not impl yet UExprFix.focusState()");
  }
}
