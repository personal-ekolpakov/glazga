package kz.pompei.glazga.core.model.eye.block;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.BondOwnerInteraction;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.core.model.focus.FocusPlaceFix;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.painter.text.PaintTextBase;
import kz.pompei.glazga.paint.painter.text.PainterText;
import kz.pompei.glazga.paint.style.StylerBlockNewVar;
import kz.pompei.glazga.server_face.model.eye.block.BlockNewVar;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

@UsedOut
public class UBlockNewVar extends UBlockDown<BlockNewVar> {

  private final Bond bondValueExpr = new Bond(this, new BondOwnerInteraction() {

    @Override
    public @NonNull String name() {
      return "VALUE";
    }

    @Override
    public String exprId() {
      return data().valueExprId;
    }

    @Override
    public @NonNull Vec leftBaseLine() {
      return exprLeftBase();
    }

  });

  @Override
  public @NonNull Map<String, Bond> bonds() {
    return Bond.map(bondValueExpr);
  }

  @Override
  public void paint(Graphics2D g) {

    PainterPath.withTrans(trans())
               .over(it -> myStyle().applyAroundRectStyle(it))
               .moveTo(leftTop())
               .right(width()).down(height())
               .right(-width()).close()
               .paint(g);

    varName_painter().pos(varName_leftBaseLine()).paintBase(PaintTextBase.LEFT_BASE_LINE)
                     .paint(g);

    assign_painter().pos(assign_leftBaseLine()).paintBase(PaintTextBase.LEFT_BASE_LINE)
                    .paint(g);


    if (debug()) {
      PainterPath.withTrans(trans())
                 .over(it -> it.pen().width(1).color(new Color(1, 1, 1)))
                 .rect(Rect.over(varName_leftBaseLine(), varName_width(), varName_painter().height()))
                 .moveTo(varName_leftBaseLine()).right(varName_width())
                 .scaleWidth(false)
                 .paint(g);
    }

    PainterPath.withTrans(trans())
               .over(it -> myStyle().applyFocusRect(it, focusing))
               .rect(varName_rect().expand(myStyle().focusRectMargin()))
               .paint(g);
  }

  private Rect focusRect() {
    return varName_rect().expand(myStyle().focusRectMargin());
  }

  //region varName txt

  private String varName_txt() {
    return data().varNameTxt();
  }


  private PainterText varName_painter() {
    return PainterText.text(varName_txt())
                      .trans(trans())
                      .over(it -> myStyle().applyVarNameStyle(it));
  }


  private Vec varName_leftBaseLine() {
    return leftTop().plus(padding().left, padding().top + baseLineHeight().up);
  }

  private double varName_width() {
    return varName_painter().width();
  }

  private ExprHeight varName_height() {
    return varName_painter().height();
  }

  private Rect varName_rect() {
    return Rect.over(varName_leftBaseLine(), varName_width(), varName_height());
  }

  //endregion


  //region assign txt

  private String assign_txt() {
    return " :=";
  }

  private PainterText assign_painter() {
    return PainterText.text(assign_txt())
                      .trans(trans())
                      .over(it -> myStyle().applyAssignTxtStyle(it));
  }

  private double assign_width() {
    return assign_painter().width();
  }

  private ExprHeight assign_height() {
    return assign_painter().height();
  }

  private Vec assign_leftBaseLine() {
    return varName_leftBaseLine().right(varName_width());
  }

  //endregion

  private SideSpaces padding() {
    return myStyle().aroundRectPadding();
  }

  private StylerBlockNewVar myStyle() {
    return styler().blockNewVar();
  }

  private double textExprDist() {
    return myStyle().text_expr_dist();
  }

  @Override
  protected double calcWidth() {
    return padding().left + varName_width() + assign_width() + textExprDist() + bondValueExpr.width() + padding().right;
  }

  private Vec exprLeftBase() {
    return varName_leftBaseLine().plus(varName_width() + assign_width() + textExprDist(), 0);
  }


  private ExprHeight baseLineHeight() {
    return varName_height().combine(assign_height()).combine(bondValueExpr.height());
  }

  @Override
  protected double calcHeight() {
    return baseLineHeight().total() + padding().top + padding().bottom;
  }

  @Override
  protected @NonNull Collection<FocusPlace> calculateOwnFocuses() {
    return List.of(FocusPlaceFix.of("main", focusRect()));
  }

  @Override
  public String focusState() {
    return "main";
  }

  @Override
  protected String viewInfo() {
    return "varName=" + varName_txt();
  }
}
