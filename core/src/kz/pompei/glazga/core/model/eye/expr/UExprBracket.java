package kz.pompei.glazga.core.model.eye.expr;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.BondOwnerInteraction;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.bracket.PainterBracket;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.server_face.model.eye.expr.ExprBracket;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

@UsedOut
public class UExprBracket extends UExpr<ExprBracket> {

  private final Bond bond = new Bond(this, new BondOwnerInteraction() {

    @Override
    public @NonNull String name() {
      return "MAIN";
    }

    @Override
    public @NonNull Vec leftBaseLine() {
      return exprLeftBaseLine();
    }

    @Override
    public String exprId() {
      return data().exprId;
    }

  });

  private Vec exprLeftBaseLine() {
    return leftBaseLine().right(leftBracketWidth());
  }

  private boolean hasBrackets() {
    return data().kind != null;
  }

  private PainterBracket painterBracketLeft() {
    return PainterBracket.withKind(data().kind)
                         .open(true)
                         .trans(trans())
                         .over(pbs -> styler().exprBracket().apply(pbs))
                         .height(bond.height());
  }

  private double leftBracketWidth() {
    if (!hasBrackets()) {
      return 0;
    }

    return painterBracketLeft().width();
  }

  private PainterBracket painterBracketRight() {
    return PainterBracket.withKind(data().kind)
                         .open(false)
                         .trans(trans())
                         .over(pbs -> styler().exprBracket().apply(pbs))
                         .height(bond.height());
  }

  private double rightBracketWidth() {
    if (!hasBrackets()) {
      return 0;
    }

    return painterBracketRight().width();
  }

  @Override
  public @NonNull Map<String, Bond> bonds() {
    return Bond.map(bond);
  }

  @Override
  public void paint(Graphics2D g) {

    if (hasBrackets()) {

      painterBracketLeft().pos(leftBaseLine())
                          .paint(g);

      painterBracketRight().pos(leftBaseLine().right(leftBracketWidth()).right(bond.width()))
                           .paint(g);

      if (debug()) {

        PainterPath.withTrans(trans())
                   .over(pps -> pps.pen().width(1).color(new Color(1, 1, 1)))
                   .rect(Rect.over(leftBaseLine(), leftBracketWidth(), bond.height()))
                   .moveTo(leftBaseLine()).right(leftBracketWidth())
                   .scaleWidth(false)
                   .paint(g);

      }

    }

    bond.paint(g);
  }

  @Override
  protected double calcWidth() {
    return leftBracketWidth() + bond.width() + rightBracketWidth();
  }

  @Override
  protected @NonNull ExprHeight calcHeight() {
    return bond.height();
  }

  @Override
  protected @NonNull Collection<FocusPlace> calculateOwnFocuses() {
    return List.of();
  }

  @Override
  public String focusState() {
    return null;
  }
}
