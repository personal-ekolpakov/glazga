package kz.pompei.glazga.core.model.eye.block;

import java.awt.Graphics2D;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.core.model.focus.FocusPlaceFix;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.SideSpaces;
import kz.pompei.glazga.mat.Size;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.painter.text.PaintTextBase;
import kz.pompei.glazga.paint.painter.text.PainterText;
import kz.pompei.glazga.paint.style.StylerBlockFixMethodStart;
import kz.pompei.glazga.paint.style.focus.Focusing;
import kz.pompei.glazga.server_face.model.eye.block.BlockFixMethodStart;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

@UsedOut
public class UBlockFixMethodStart extends UBlockDown<BlockFixMethodStart> {

  @Override
  public void paint(Graphics2D g) {

    PainterPath.withTrans(trans())
               .over(it -> myStyle().applyAroundBorder(it))
               .rect(rect())
               .paint(g);

    painter_displayName().pos(textLeftTop()).paintBase(PaintTextBase.LEFT_TOP)
                         .paint(g);

    if (focusing == Focusing.IS) {
      PainterPath.withTrans(trans())
                 .over(it -> myStyle().applyFocusRect(it))
                 .rect(displayNameFocusRect())
                 .paint(g);
    }
  }

  private Vec textLeftTop() {
    SideSpaces blockPadding = myStyle().aroundBorderPadding();
    return leftTop().plus(blockPadding.left, blockPadding.top);
  }

  private PainterText painter_displayName() {
    return PainterText.text(data().methodNameDisplay())
                      .trans(trans())
                      .over(it -> myStyle().applyMethodName(it));
  }

  private Rect displayNameFocusRect() {

    Vec textLeftTop = textLeftTop();

    PainterText painterText = painter_displayName();

    double fontHeight = painterText.fontHeight();
    double width      = painterText.width();

    Rect textRect = Rect.over(textLeftTop, Size.of(width, fontHeight));

    return textRect.expand(myStyle().displayNameFocusRectMargin());
  }

  private StylerBlockFixMethodStart myStyle() {
    return styler().blockFixMethodStart();
  }

  @Override
  protected double calcWidth() {
    double textWidth = painter_displayName().width();

    SideSpaces blockPadding = myStyle().aroundBorderPadding();
    return blockPadding.left + textWidth + blockPadding.right;
  }

  @Override
  protected double calcHeight() {
    double textHeight = painter_displayName().height().total();

    SideSpaces blockPadding = myStyle().aroundBorderPadding();
    return blockPadding.top + textHeight + blockPadding.bottom;
  }

  @Override
  public @NonNull Map<String, Bond> bonds() {
    return Map.of();
  }

  @Override
  protected @NonNull Collection<FocusPlace> calculateOwnFocuses() {
    return List.of(FocusPlaceFix.of("main", displayNameFocusRect()));
  }

  @Override
  public String focusState() {
    return "main";
  }

  @Override
  protected String viewInfo() {
    return "methodName=" + data().methodNameDisplay();
  }
}
