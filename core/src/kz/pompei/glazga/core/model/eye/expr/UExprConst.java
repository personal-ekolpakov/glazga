package kz.pompei.glazga.core.model.eye.expr;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.core.model.focus.FocusPlaceFix;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.painter.text.PaintTextBase;
import kz.pompei.glazga.paint.painter.text.PainterText;
import kz.pompei.glazga.paint.style.StylerExprConst;
import kz.pompei.glazga.server_face.model.eye.expr.ExprConst;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

@UsedOut
public class UExprConst extends UExpr<ExprConst> {
  @Override
  public void paint(Graphics2D g) {

    PainterPath.withTrans(trans())
               .over(it -> myStyle().applyFocusRect(it, focusing))
               .rect(focusRect())
               .paint(g);

    painterText().pos(leftBaseLine()).paintBase(PaintTextBase.LEFT_BASE_LINE)
                 .paint(g);

    if (debug()) {

      PainterPath.withTrans(trans())
                 .over(pps -> pps.pen().width(1).color(new Color(1, 1, 1)))
                 .rect(Rect.over(leftBaseLine(), width(), height()))
                 .moveTo(leftBaseLine()).right(width())
                 .scaleWidth(false)
                 .paint(g);

    }

  }

  private Rect focusRect() {
    PainterText value = painterText();
    return Rect.over(leftBaseLine(), value.width(), value.height())
               .expand(myStyle().focusRectMargin());
  }

  private StylerExprConst myStyle() {
    return styler().exprConst();
  }

  @Override
  protected double calcWidth() {
    return painterText().width();
  }

  private @NonNull PainterText painterText() {
    return PainterText.text(data().defDisplay())
                      .over(it -> myStyle().applyTextStyle(it, focusing))
                      .trans(trans());
  }

  @Override
  protected @NonNull ExprHeight calcHeight() {
    return painterText().height();
  }

  @Override
  public @NonNull Map<String, Bond> bonds() {
    return Map.of();
  }

  @Override
  protected @NonNull List<FocusPlace> calculateOwnFocuses() {
    return List.of(FocusPlaceFix.of("main", focusRect()));
  }

  @Override
  public String focusState() {
    return "main";
  }

  @Override
  protected String viewInfo() {
    return data().def;
  }

}
