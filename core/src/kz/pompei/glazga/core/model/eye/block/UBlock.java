package kz.pompei.glazga.core.model.eye.block;

import java.util.Map;
import java.util.function.Supplier;
import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.core.model.eye.UElement;
import kz.pompei.glazga.core.utils.ModelUtil;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Size;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.style.Styler;
import kz.pompei.glazga.server_face.model.eye.block.Block;
import kz.pompei.glazga.server_face.model.eye.block.BlockFix;
import lombok.NonNull;

import static java.util.Objects.requireNonNull;

public abstract class UBlock<Data extends Block> extends UElement {
  private String           id;
  private Data             data;
  private Supplier<Styler> styler;

  private boolean refreshLeftTop = true;
  private Vec     leftTop;
  private boolean refreshWidth   = true;
  private double  width;
  private boolean refreshHeight  = true;
  private double  height;

  private UDown owner;

  public UDown owner() {
    return owner;
  }

  @Override
  public final @NonNull UType type() {
    return UType.BLOCK;
  }

  @Override
  public UElement up() {
    UDown h = owner;
    if (h == null) {
      return null;
    }
    return h.owner;
  }

  /**
   * Возвращает список удерживаемых блоков данным блоком
   *
   * @return колода держателей блоков. Ключ колоды - имя держателя блока, значение - сам держатель блока
   */
  public abstract Map<String, UDown> downs();

  public static class __initBlock__ {
    public static void setOwner(UBlock<?> block, UDown owner) {
      block.owner = owner;
    }
  }

  public static <Data extends Block> void init(UBlock<Data> block, ModelUtil.UInit<Data> init) {
    requireNonNull(init.id, "hIjDygN4x9 :: init.id == null");
    requireNonNull(init.data, "nTg8dP3QN7 :: init.data == null");
    requireNonNull(init.styler, "mb1yW8yUBA :: init.styler == null");
    requireNonNull(init.uLogic, "dFek7E2jxF :: init.uLogic == null");
    block.id     = init.id;
    block.data   = init.data;
    block.styler = init.styler;
    block.logic  = init.uLogic;

    block.postInit();
  }

  protected void postInit() {}

  @Override
  public final boolean isFixed() {
    return data() instanceof BlockFix;
  }

  @Override
  public void validateAfterAllInitializations() {
    super.validateAfterAllInitializations();
    requireNonNull(id, "FqvNlkIlwZ :: id == null");
    requireNonNull(data, "vn3lw2ckeB :: data == null");
    requireNonNull(logic, "BIGjBk3Ei6 :: logic == null");
  }

  public String id() {
    return id;
  }

  public Data data() {
    return data;
  }

  public @NonNull Styler styler() {
    return styler.get();
  }

  protected abstract double calcWidth();

  protected abstract double calcHeight();

  protected Vec calcLeftTop() {
    if (data instanceof BlockFix bf) {
      return requireNonNull(bf.leftTop(), "Yb0IxABWf7 :: Calculated pos in null");
    }
    {
      UDown up = this.owner;
      return up != null ? up.downLeftTop() : Vec.zero();
    }
  }

  public Vec leftTop() {
    if (data instanceof BlockFix bf) {
      return bf.leftTop();
    }
    if (leftTop != null && !refreshLeftTop) {
      return leftTop;
    }
    {
      Vec ret = leftTop = calcLeftTop();
      refreshLeftTop = false;
      return ret;
    }
  }

  public double width() {
    if (!refreshWidth) {
      return width;
    }
    {
      double oldWidth = width;
      double newWidth = width = calcWidth();
      refreshWidth = false;
      if (Math.abs(oldWidth - newWidth) <= UElement.SAVE_MIN_OFFSET) {
        String blockId = id();
        logic.elementSizes().saveElementWidth(UType.BLOCK, blockId, newWidth);
      }
      return newWidth;
    }
  }

  public double height() {
    if (!refreshHeight) {
      return height;
    }
    {
      double oldHeight = height;
      double newHeight = height = calcHeight();
      refreshHeight = false;
      if (Math.abs(newHeight - oldHeight) <= UElement.SAVE_MIN_OFFSET) {
        String blockId = id();
        logic.elementSizes().saveBlockHeight(blockId, newHeight);
      }
      return newHeight;
    }
  }

  @Override
  public @NonNull Rect rect() {
    return Rect.over(leftTop(), size());
  }

  public Size size() {
    return Size.of(width(), height());
  }

  public @NonNull LinearTrans trans() {
    return logic.trans();
  }

  protected boolean debug() {
    return logic.debug();
  }

}
