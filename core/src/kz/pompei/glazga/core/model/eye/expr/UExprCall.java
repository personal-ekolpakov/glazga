package kz.pompei.glazga.core.model.eye.expr;

import java.awt.Graphics2D;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Stream;
import kz.pompei.glazga.core.model.eye.Bond;
import kz.pompei.glazga.core.model.eye.BondOwnerInteraction;
import kz.pompei.glazga.core.model.eye.expr.call.UActSpec;
import kz.pompei.glazga.core.model.eye.expr.call.UActSpecArg;
import kz.pompei.glazga.core.model.focus.FocusPlace;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.style.StylerExprCall;
import kz.pompei.glazga.paint.style.focus.Focusing;
import kz.pompei.glazga.server_face.model.eye.etc.ActSpec;
import kz.pompei.glazga.server_face.model.eye.expr.ExprCall;
import kz.pompei.glazga.utils.UsedOut;
import lombok.NonNull;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toMap;

@UsedOut
public class UExprCall extends UExpr<ExprCall> {

  private UActSpec spec;

  public final Bond leftExprBond = new Bond(this, new BondOwnerInteraction() {

    @Override
    public @NonNull String name() {
      return "LEFT";
    }

    @Override
    public @NonNull Vec leftBaseLine() {
      return UExprCall.this.leftBaseLine();
    }

    @Override
    public String exprId() {
      return data().leftExprId;
    }

  });

  public static class __initExprCall__ {
    public static void initSpec(UExprCall uec, Map<String, ActSpec> actSpecifications) {
      uec.spec = new UActSpec(uec, actSpecifications.get(uec.data().actId));
    }
  }

  @Override
  public void validateAfterAllInitializations() {
    super.validateAfterAllInitializations();
    requireNonNull(spec, "oX2LYeG1NH :: props == null");
    spec.validateAfterInit();
  }

  @Override
  public @NonNull Map<String, Bond> bonds() {
    return Stream.concat(Stream.of(leftExprBond), spec.args.stream().map(x -> x.bond)).collect(toMap(Bond::name, b -> b));
  }

  @Override
  public void paint(Graphics2D g) {
    leftExprBond.paint(g);
    spec.paint(g);

    if (focusing == Focusing.IS) {
      FocusPlace focusPlace = focusMap().get(focusState());
      if (focusPlace != null) {
        PainterPath.withTrans(trans())
                   .over(it -> myStyle().applyFocusRect(it))
                   .rect(focusPlace.rect())
                   .paint(g);
      }
    }

  }

  @Override
  public String focusState() {
    String state = logic.focus().getState();
    if (focusMap().containsKey(state)) {
      return state;
    }

    return UActSpec.FOCUS_STATE_METHOD_NAME;
  }

  public StylerExprCall myStyle() {
    return styler().exprCall();
  }

  @Override
  protected double calcWidth() {
    return leftExprBond.width() + spec.width();
  }

  @Override
  protected @NonNull ExprHeight calcHeight() {
    return leftExprBond.height().combine(spec.height());
  }

  @Override
  protected @NonNull Collection<FocusPlace> calculateOwnFocuses() {
    return focusMap().values();
  }

  private Map<String, FocusPlace> focusMap;

  private Map<String, FocusPlace> focusMap() {
    {
      var map = focusMap;
      if (map != null) {
        return map;
      }
    }

    return printFocusMap(focusMap = createFocuses().collect(toMap(FocusPlace::state, x -> x)));
  }

  private Stream<FocusPlace> createFocuses() {
    return Stream.concat(spec.focuses(),
                         spec.args.stream().flatMap(UActSpecArg::focuses));
  }

}
