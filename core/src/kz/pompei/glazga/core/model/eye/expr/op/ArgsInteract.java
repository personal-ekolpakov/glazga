package kz.pompei.glazga.core.model.eye.expr.op;

import kz.pompei.glazga.mat.Vec;

public interface ArgsInteract {
  Vec interactLeftBaseLine();

  double interactWidth();
}
