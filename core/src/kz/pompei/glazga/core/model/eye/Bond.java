package kz.pompei.glazga.core.model.eye;

import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.core.model.eye.expr.UExpr;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.mat.ExprHeight;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;
import kz.pompei.glazga.paint.painter.text.PainterText;
import kz.pompei.glazga.paint.style.StylerBond;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Этот класс рисует окантовку вокруг выражения. Но обычно этой окантовки нет.
 * <p>
 * Пустое место для выражения называется знакоместо
 */
@RequiredArgsConstructor
public class Bond {

  public final @NonNull UElement owner;
  public final @NonNull BondOwnerInteraction ownerConnect;

  public Vec leftBaseLine() {
    return ownerConnect.leftBaseLine();
  }

  public double width() {
    UExpr<?> expr = this.bondExpr;
    return expr != null ? expr.width() : emptyHiddenText().width();
  }

  public @NonNull ExprHeight height() {
    UExpr<?> expr = this.bondExpr;
    return expr != null ? expr.height() : emptyHiddenText().height();
  }

  private UExpr<?> bondExpr;

  public static class __initBond__ {
    public static void setBondExpr(Bond bond, UExpr<?> bondExpr) {
      bond.bondExpr = bondExpr;
    }
  }

  private PainterText emptyHiddenText() {
    return PainterText.text("Ы")
                      .over(pts -> myStyle().applyEmptyHiddenText(pts));
  }

  public void paint(Graphics2D g) {

    if (bondExpr != null) {
      bondExpr.paint(g);
      return;
    }

    ExprHeight height = height();

    Vec leftTop = leftBaseLine().plus(0, -height.up);

    PainterPath.withTrans(owner.trans())
               .over(it -> myStyle().applyEmptyRectStyle(it))
               .moveTo(leftTop)
               .right(width()).down(height.up)
               .right(-width()).close()
               .paint(g);
  }

  private StylerBond myStyle() {
    return owner.styler().bond();
  }

  public UExpr<?> bondExpr() {
    return bondExpr;
  }

  public static Map<String, Bond> map(Bond... bonds) {
    Map<String, Bond> ret = new HashMap<>();
    for (final Bond bond : bonds) {
      String name    = bond.name();
      Bond   oldBond = ret.get(name);
      if (oldBond != null) {
        throw new RuntimeException("0c8Yg8okSs :: Bond with name = `" + name + "` already exists");
      }
      ret.put(name, bond);
    }
    return ret;
  }

  public static Map<String, Bond> map(List<Bond> ret) {
    return map(ret.toArray(new Bond[0]));
  }

  public @NonNull String name() {
    return ownerConnect.name();
  }

  public Rect rect() {
    return Rect.over(leftBaseLine(), width(), height());
  }

  @Override
  public String toString() {
    return getClass().getSimpleName()
      + "{name=" + name()
      + " of " + owner.getClass().getSimpleName() + ".id=" + owner.id()
      + "}";
  }

  @SuppressWarnings("unused")
  public void moveFocus(@NonNull FocusMove focusMove) {
    owner.logic.log().focus().bond_moveFocus(this, focusMove);
    throw new RuntimeException("25Ih1bj7Bs");
  }

  public boolean eq(Bond another) {
    return another != null
      && owner.id().equals(another.owner.id())
      && name().equals(another.name());
  }
}
