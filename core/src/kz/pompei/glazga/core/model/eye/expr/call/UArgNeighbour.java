package kz.pompei.glazga.core.model.eye.expr.call;

import kz.pompei.glazga.mat.Vec;

public interface UArgNeighbour {
  double neighbourWidth();

  Vec neighbourLeftBaseLine();
}
