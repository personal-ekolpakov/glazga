package kz.pompei.glazga.core.model.events;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import static java.util.stream.Collectors.toList;

public final class EventTrap {

  private final List<KeyCode> keyCodes = new ArrayList<>();

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" + keyCodes.stream().map(Enum::name).collect(Collectors.joining("+")) + "}";
  }

  private EventTrap() {}

  public static EventTrap free() {
    return new EventTrap();
  }

  public static EventTrap ofCtrl() {
    return free().ctrl();
  }

  public static EventTrap ofAlt() {
    return free().alt();
  }

  public static EventTrap ofShift() {
    return free().shift();
  }

  public static EventTrap ofCtrlShift() {
    return ofCtrl().shift();
  }

  public static EventTrap ofCtrlAltShift() {
    return ofCtrl().alt().shift();
  }

  public EventTrap ctrl() {
    return key(KeyCode.CTRL);
  }

  public EventTrap shift() {
    return key(KeyCode.SHIFT);
  }

  public EventTrap alt() {
    return key(KeyCode.ALT);
  }

  public EventTrap key(KeyCode keyCode) {
    keyCodes.add(keyCode);
    return this;
  }

  public static EventTrap ofKey(KeyCode keyCode) {
    return free().key(keyCode);
  }

  public Fix fix() {
    return new Fix(List.copyOf(keyCodes));
  }

  @EqualsAndHashCode
  @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
  public static final class Fix {
    private final List<KeyCode> keyCodes;

    public boolean isTrapped(PressedKeys pressedKeys) {
      List<KeyCode> keyCodesCopy = new ArrayList<>(keyCodes);
      if (keyCodesCopy.isEmpty()) {
        return false;
      }

      KeyCode      expectKey       = keyCodesCopy.remove(keyCodesCopy.size() - 1);
      Set<KeyCode> expectModifiers = new HashSet<>(keyCodesCopy);


      List<KeyCode> pressedKeyCodes = pressedKeys.pressedKeys().map(x -> x.code).collect(toList());

      if (pressedKeyCodes.isEmpty()) {
        return false;
      }

      KeyCode      pressedKey       = pressedKeyCodes.remove(pressedKeyCodes.size() - 1);
      Set<KeyCode> pressedModifiers = new HashSet<>(pressedKeyCodes);

      return expectKey == pressedKey && expectModifiers.equals(pressedModifiers);
    }
  }
}
