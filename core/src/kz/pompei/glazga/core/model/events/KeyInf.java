package kz.pompei.glazga.core.model.events;

import java.awt.event.KeyEvent;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode
@RequiredArgsConstructor(staticName = "of")
public class KeyInf {
  public final boolean pressed;
  public final KeyCode code;
  public final char    txt;

  public static KeyInf extractFrom(KeyEvent event) {

    if (event.getID() != KeyEvent.KEY_PRESSED && event.getID() != KeyEvent.KEY_RELEASED) {
      return null;
    }

    KeyCode keyCode = KeyCode.byIntOrNull(event.getKeyCode());
    if (keyCode == null) {
      return null;
    }

    boolean pressed = event.getID() == KeyEvent.KEY_PRESSED;
    char    keyChar = event.getKeyChar();

    return KeyInf.of(pressed, keyCode, keyChar);
  }

  @Override
  public String toString() {
    return "" +
      (pressed ? "PRESSED" : "RELEASED") +
      ", code=" + code +
      ", txt=" + txt;
  }
}
