package kz.pompei.glazga.core.model.events;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum KeyCode {
  CTRL(17),
  SHIFT(16),
  ALT(18),

  Insert(155),
  Home(36),
  PageUp(33),
  PageDown(34),
  End(35),
  Delete(127),

  Escape(27),

  Left(37),
  Right(39),
  Up(38),
  Down(40),

  Semicolon(59),
  Quote(222),
  Enter(10),
  Comma(44),
  Dot(46),
  BackSlash(92),
  BracketOpen(91),
  BracketClose(93),
  Minus(45),
  Equal(61),
  Backspace(8),
  Space(32),

  keyA(65),
  keyB(66),
  keyC(67),
  keyD(68),
  keyE(69),
  keyF(70),
  keyG(71),
  keyH(72),
  keyI(73),
  keyJ(74),
  keyK(75),
  keyL(76),
  keyM(77),
  keyN(78),
  keyO(79),
  keyP(80),
  keyQ(81),
  keyR(82),
  keyS(83),
  keyT(84),
  keyU(85),
  keyV(86),
  keyW(87),
  keyX(88),
  keyY(89),
  keyZ(90),

  key0(48),
  key1(49),
  key2(50),
  key3(51),
  key4(52),
  key5(53),
  key6(54),
  key7(55),
  key8(56),
  key9(57),

  ;

  public final int code;

  private static final Map<Integer, KeyCode> map;

  static {
    Map<Integer, KeyCode> m = new HashMap<>();
    for (final KeyCode value : values()) {
      m.put(value.code, value);
    }
    map = Collections.unmodifiableMap(m);
  }

  KeyCode(int code) {
    this.code = code;
  }

  public static KeyCode byIntOrNull(int intKeyCode) {
    return map.get(intKeyCode);
  }
}
