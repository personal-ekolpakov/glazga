package kz.pompei.glazga.core.model.events;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class PressedKeys {
  private final List<KeyInf> pressedKeys = new ArrayList<>();

  public void clean() {
    pressedKeys.clear();
  }

  public void apply(KeyInf keyInf) {
    if (keyInf.pressed) {

      if (pressedKeys.size() > 0 && pressedKeys.get(pressedKeys.size() - 1).code == keyInf.code) {
        return;
      }

      pressedKeys.add(keyInf);
      return;
    }

    int index = 0;
    while (index < pressedKeys.size()) {

      if (pressedKeys.get(index).code == keyInf.code) {
        pressedKeys.remove(index);
      } else {
        index++;
      }

    }

  }

  public PressedKeys copy() {
    PressedKeys x = new PressedKeys();
    x.pressedKeys.addAll(pressedKeys);
    return x;
  }

  public Stream<KeyInf> pressedKeys() {
    return pressedKeys.stream();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + pressedKeys;
  }
}
