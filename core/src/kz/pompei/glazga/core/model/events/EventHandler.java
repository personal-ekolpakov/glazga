package kz.pompei.glazga.core.model.events;

public interface EventHandler {
  void handle() throws Exception;
}
