package kz.pompei.glazga.core;

import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.mat.ExprHeight;

public interface ULogicElementSizes {

  void saveBlockHeight(String blockId, double height);

  void saveExprHeight(String exprId, ExprHeight height);

  void saveElementWidth(UType uType, String elementId, double width);

}
