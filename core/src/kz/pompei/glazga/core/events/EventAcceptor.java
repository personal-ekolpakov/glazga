package kz.pompei.glazga.core.events;

import kz.pompei.glazga.core.model.events.PressedKeys;

public interface EventAcceptor {

  boolean acceptKey(PressedKeys pressedKeys);

}
