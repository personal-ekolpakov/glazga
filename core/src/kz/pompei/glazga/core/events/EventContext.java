package kz.pompei.glazga.core.events;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.core.model.events.EventHandler;
import kz.pompei.glazga.core.model.events.EventTrap;
import kz.pompei.glazga.core.model.events.PressedKeys;
import lombok.RequiredArgsConstructor;

public class EventContext implements EventAcceptor {
  @RequiredArgsConstructor
  private static class EventDot {
    public final String       name;
    public final EventHandler eventHandler;
  }

  private final Map<EventTrap.Fix, EventDot> registeredHandlers = new HashMap<>();

  public void register(EventTrap trap, EventHandler eventHandler, String name) {
    EventTrap.Fix fixedTrap = trap.fix();
    EventDot      oldEvent  = registeredHandlers.get(fixedTrap);
    if (oldEvent != null) {
      throw new RuntimeException("sZp626VHlN :: event trap " + trap + " already registered for handler `" + oldEvent.name + "`");
    }
    registeredHandlers.put(fixedTrap, new EventDot(name, eventHandler));
  }

  public void register(List<EventTrap> traps, EventHandler eventHandler, String name) {
    for (final EventTrap trap : traps) {
      register(trap, eventHandler, name);
    }
  }

  @Override
  public boolean acceptKey(PressedKeys pressedKeys) {

    for (final Map.Entry<EventTrap.Fix, EventDot> e : registeredHandlers.entrySet()) {
      if (e.getKey().isTrapped(pressedKeys)) {
        try {
          e.getValue().eventHandler.handle();
          return true;
        } catch (Exception ex) {
          throw new RuntimeException(ex);
        }
      }
    }

    return false;
  }

}
