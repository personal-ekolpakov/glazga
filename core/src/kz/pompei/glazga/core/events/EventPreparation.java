package kz.pompei.glazga.core.events;

import java.awt.event.KeyEvent;
import java.util.List;
import java.util.function.Supplier;
import kz.pompei.glazga.core.model.events.KeyInf;
import kz.pompei.glazga.core.model.events.PressedKeys;

public class EventPreparation {

  private final PressedKeys pressedKeys = new PressedKeys();

  public Supplier<List<EventAcceptor>> eventAcceptors = List::of;

  public void clean() {
    pressedKeys.clean();
  }

  public void sendNativeEvent(KeyEvent keyEvent) {

//    System.out.println("Nih9LhxEx1 :: " + keyEvent);

    KeyInf keyInf = KeyInf.extractFrom(keyEvent);
    if (keyInf == null) {
      return;
    }

    pressedKeys.apply(keyInf);

    if (keyInf.pressed) {
      sendPressedKeys(pressedKeys.copy());
    }
  }

  private void sendPressedKeys(PressedKeys pressedKeys) {
    Supplier<List<EventAcceptor>> getter = eventAcceptors;
    if (getter == null) {
      return;
    }
    List<EventAcceptor> lst = getter.get();
    if (lst == null) {
      return;
    }

    for (final EventAcceptor eventAcceptor : lst) {
      if (eventAcceptor.acceptKey(pressedKeys)) {
        return;
      }
    }
  }

}
