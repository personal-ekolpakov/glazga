package kz.pompei.glazga.core.utils;

import java.util.List;
import java.util.function.DoubleSupplier;
import java.util.function.Predicate;
import kz.pompei.glazga.core.model.focus.FocusLogic;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.mat.Rect;
import lombok.NonNull;

public class UUtil {
  public static FocusLogic findNextFocusLogic(@NonNull FocusMove focusMove,
                                              List<FocusLogic> focusLogicLeftToRightList,
                                              List<FocusLogic> focusLogicTopToDownList,
                                              Predicate<FocusLogic> isCurrent,
                                              @NonNull DoubleSupplier focusLeft,
                                              @NonNull DoubleSupplier focusTop) {

    boolean          horizontal     = focusMove.isHorizontal();
    List<FocusLogic> focusLogicList = horizontal ? focusLogicLeftToRightList : focusLogicTopToDownList;

    int step = focusMove.forward() ? +1 : -1;

    final int lastIndex = focusLogicList.size() - 1;

    // Здесь нужно найти текущий в списке

    int index = focusMove.forward() ? 0 : lastIndex;

    if (isCurrent != null) {
      boolean notFound = true;

      while (0 <= index && index <= lastIndex) {

        FocusLogic current = focusLogicList.get(index);
        index += step;

        if (isCurrent.test(current)) {
          // Нашли текущий - запоминаем
          notFound = false;
          break;
        }
      }

      if (notFound) {
        return null;
      }
    }

    double line = horizontal ? focusTop.getAsDouble() : focusLeft.getAsDouble();

    FocusLogic found = null;

    boolean shorter = !focusMove.longer;

    while (0 <= index && index <= lastIndex) {
      FocusLogic current = focusLogicList.get(index);
      index += step;

      Rect   rect = current.rect();
      double low  = horizontal ? rect.top : rect.left;
      double high = horizontal ? rect.bottom : rect.right;

      if (low <= line && line <= high) {
        if (shorter) {
          return current;
        }
        found = current;
      }

    }

    return found;
  }
}
