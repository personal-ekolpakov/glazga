package kz.pompei.glazga.core.utils;

public class ConvertUtil {
  public static int strToInt(String str, int defaultValue) {
    if (str == null) {
      return defaultValue;
    }
    try {
      return Integer.parseInt(str);
    } catch (NumberFormatException ignore) {
      return defaultValue;
    }
  }
}
