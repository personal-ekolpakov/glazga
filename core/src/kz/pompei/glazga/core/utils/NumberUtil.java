package kz.pompei.glazga.core.utils;

public class NumberUtil {

  public static String doubleToStrLen(double value, int intLen, int fracLen) {

    if (fracLen < 0) {
      throw new RuntimeException("K6MQ2m26Qu :: Not impl yet");
    }

    double rounded = value > 0 ? value : -value;

    for (int i = 0; i < fracLen; i++) {
      rounded *= 10;
    }

    long intValue = Math.round(rounded);

    if (intValue == 0) {
      return fracLen == 0
        ?
        " ".repeat(intLen - 1) + "0"
        :
        " ".repeat(intLen - 1) + "0 " + " ".repeat(fracLen)
        ;
    }

    if (fracLen == 0) {
      StringBuilder sb = new StringBuilder(100);
      if (value < 0) {
        sb.append("-");
      }
      sb.append(intValue);
      while (sb.length() < intLen) {
        sb.insert(0, ' ');
      }
      return sb.toString();
    }

    String intStr = "" + intValue;

    String frac    = intStr.substring(intStr.length() - fracLen);
    String intPart = intStr.substring(0, intStr.length() - fracLen);

    if (intPart.isEmpty()) {
      intPart = "0";
    }

    if (value < 0) {
      intPart = "-" + intPart;
    }

    String intPartPrefix = "";
    if (intPart.length() < intLen) {
      intPartPrefix = " ".repeat(intLen - intPart.length());
    }

    int fracRightSpaces = 0;
    while (frac.endsWith("0")) {
      fracRightSpaces++;
      frac = frac.substring(0, frac.length() - 1);
    }

    String fracOk    = frac.length() == 0 ? " ".repeat(fracRightSpaces + 1) : "." + frac + " ".repeat(fracRightSpaces);
    String intPartOk = intPartPrefix + intPart;

    return intPartOk + fracOk;
  }

}
