package kz.pompei.glazga.core.utils;

import java.lang.reflect.Constructor;
import java.util.function.Supplier;
import kz.pompei.glazga.core.ULogic;
import kz.pompei.glazga.core.model.eye.block.UBlock;
import kz.pompei.glazga.core.model.eye.expr.UExpr;
import kz.pompei.glazga.paint.style.Styler;
import kz.pompei.glazga.server_face.model.eye.block.Block;
import kz.pompei.glazga.server_face.model.eye.expr.Expr;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import static java.util.Objects.requireNonNull;

public class ModelUtil {

  @AllArgsConstructor
  public static class UInit<Data> {
    public final String           id;
    public final Data             data;
    public final Supplier<Styler> styler;
    public final ULogic           uLogic;
  }

  @SneakyThrows
  public static <Data extends Block> UBlock<Data> createUBlock(UInit<Data> init) {
    requireNonNull(init.id, "w7W6SEr1rf :: init.id == null");
    requireNonNull(init.data, "FgHV62h5o6 :: init.data == null");
    requireNonNull(init.styler, "0Ok8ilv3Y4 :: init.styler == null");

    Class<? extends Block> dataClass = init.data.getClass();

    String classSimpleName = dataClass.getSimpleName();

    Class<?> uClass = Class.forName(UBlock.class.getPackage().getName() + ".U" + classSimpleName);

    Constructor<?> constructor = uClass.getConstructor();

    // noinspection rawtypes
    UBlock ret = (UBlock) constructor.newInstance();

    //noinspection unchecked
    UBlock.init(ret, init);

    //noinspection unchecked
    return ret;
  }


  @SneakyThrows
  public static <Data extends Expr> UExpr<Data> createUExpr(UInit<Data> init) {
    requireNonNull(init.id, "Plt12X2LQE :: init.id == null");
    requireNonNull(init.data, "lHg5CgSHVf :: init.data == null");
    requireNonNull(init.styler, "v12IUw4OWH :: init.styler == null");

    Class<? extends Expr> dataClass = init.data.getClass();

    String classSimpleName = dataClass.getSimpleName();

    Class<?> uClass = Class.forName(UExpr.class.getPackage().getName() + ".U" + classSimpleName);

    Constructor<?> constructor = uClass.getConstructor();

    // noinspection rawtypes
    UExpr ret = (UExpr) constructor.newInstance();

    //noinspection unchecked
    UExpr.init(ret, init);

    //noinspection unchecked
    return ret;
  }


}
