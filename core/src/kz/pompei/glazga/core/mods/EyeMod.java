package kz.pompei.glazga.core.mods;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import kz.pompei.glazga.core.events.EventContext;

/**
 * Мод глаза. Позволяет вносить дополнительную функциональность динамически. И выносить, если больше не нужна - включать/выключать моды.
 */
public interface EyeMod {

  /**
   * @return Контекст событий. Может возвращать null, если обработка событий не нужна
   */
  EventContext eventContext();

  /**
   * Вызывается перед рисованием блоков
   *
   * @param g куда нужно чё-то рисовать
   */
  void drawEyeBeforeBlocks(Graphics2D g);

  /**
   * Вызывается после рисования блоков
   *
   * @param g куда нужно чё-то рисовать
   */
  void drawEyeAfterBlocks(Graphics2D g);

  /**
   * Обрабатывает события колёсика мышки
   *
   * @param e событие колёсика мышки
   * @return нужно ли обрабатывать это событие дальше: true - НЕ нужно, false - нужно
   */
  boolean mouseWheelMoved(MouseWheelEvent e);

  /**
   * Обрабатывает событие клика мышки
   *
   * @param e событие клика мышки
   * @return нужно ли обрабатывать это событие дальше: true - НЕ нужно, false - нужно
   */
  boolean mouseClicked(MouseEvent e);

  /**
   * Обрабатывает событие таскания мышки
   *
   * @param e события таскания мышки
   * @return нужно ли обрабатывать это событие дальше: true - НЕ нужно, false - нужно
   */
  boolean mouseDragged(MouseEvent e);

  /**
   * Обрабатывает событие движения мышки
   *
   * @param e событие движения мышки
   * @return нужно ли обрабатывать это событие дальше: true - НЕ нужно, false - нужно
   */
  boolean mouseMoved(MouseEvent e);

}
