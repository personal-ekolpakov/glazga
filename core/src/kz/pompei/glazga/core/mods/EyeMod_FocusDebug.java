package kz.pompei.glazga.core.mods;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.List;
import java.util.Objects;
import kz.pompei.glazga.core.ULogic;
import kz.pompei.glazga.core.events.EventContext;
import kz.pompei.glazga.core.model.events.EventTrap;
import kz.pompei.glazga.core.model.eye.UElement;
import kz.pompei.glazga.core.model.focus.FocusLogic;
import kz.pompei.glazga.mat.Vec;
import kz.pompei.glazga.paint.painter.path.PainterPath;

import static kz.pompei.glazga.core.model.events.EventTrap.ofKey;
import static kz.pompei.glazga.core.model.events.KeyCode.Escape;
import static kz.pompei.glazga.utils.events.MouseKeys.mouseKeys;

public class EyeMod_FocusDebug implements EyeMod {

  private final EventContext eventContext = new EventContext();
  private final ULogic       logic;

  private UElement hit = null;

  public EyeMod_FocusDebug(ULogic logic, List<EventTrap> activatedThisModBy) {
    this.logic = logic;

    eventContext.register(activatedThisModBy, this::deactivateThisMod,
                          "wpg1zprAr5 :: Деактивация мода " + getClass().getSimpleName());
    eventContext.register(List.of(ofKey(Escape)), this::deactivateThisMod,
                          "Smt8gBl6lb :: Деактивация мода " + getClass().getSimpleName());

    System.out.println("nvbPok1gIC :: Активация мода " + getClass().getSimpleName());
  }

  private void deactivateThisMod() {
    logic.removeMods(mod -> mod == this);
  }

  @Override
  public EventContext eventContext() {
    return eventContext;
  }

  @Override
  public void drawEyeBeforeBlocks(Graphics2D g) {

  }

  @Override
  public void drawEyeAfterBlocks(Graphics2D g) {
    UElement hit = this.hit;
    if (hit != null) {
      PainterPath.withTrans(logic.trans())
                 .over(x -> logic.styler().eye().applyMarkedElement(x, hit.type()))
                 .rect(hit.rect())
                 .scaleWidth(false)
                 .paint(g);


      int i = 0;
      for (final FocusLogic focusLogic : hit.focusLogicLeftToRightList) {
        int I = i++;

        PainterPath.withTrans(logic.trans())
                   .over(x -> logic.styler().eye().applyFocusRect(x, hit.type(), I))
                   .rect(focusLogic.rect())
                   .scaleWidth(false)
                   .paint(g);
      }

    }
  }

  @Override
  public boolean mouseWheelMoved(MouseWheelEvent e) {
    return false;
  }

  @Override
  public boolean mouseClicked(MouseEvent e) {

    if (mouseKeys().free().isLeftDown(e)) {

      UElement oldHit = hit;

      Vec position = logic.trans().back(e.getPoint());
      hit = logic.vessel().findHit(position);

      if (!Objects.equals(hit, oldHit)) {
        logic.redraw();
      }

      return true;
    }

    return false;
  }

  @Override
  public boolean mouseDragged(MouseEvent e) {
    return false;
  }

  @Override
  public boolean mouseMoved(MouseEvent e) {
    logic.redraw();
    return false;
  }
}
