package kz.pompei.glazga.core;

import java.util.function.Predicate;
import kz.pompei.glazga.core.logging.Log;
import kz.pompei.glazga.core.model.eye.UBaseVessel;
import kz.pompei.glazga.core.model.eye.UDown;
import kz.pompei.glazga.core.model.eye.block.UBlock;
import kz.pompei.glazga.core.model.eye.expr.UExpr;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.core.mods.EyeMod;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.paint.style.Styler;

public interface ULogic {

  LinearTrans trans();

  boolean debug();

  ULogicElementSizes elementSizes();

  ULogicFocus focus();

  UBaseVessel vessel();

  void blockGivesFocusToEye(UBlock<?> thisBlock, FocusMove focusMove);

  void exprGivesFocusToEye(UExpr<?> thisExpr, FocusMove focusMove);

  void downGivesFocusToEye(UDown down, FocusMove focusMove);

  Log log();

  Rect rectEye();

  void removeMods(Predicate<EyeMod> testModuleToRemove);

  void redraw();

  Styler styler();
}
