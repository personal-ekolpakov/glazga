package kz.pompei.glazga.core.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.DoubleSupplier;
import java.util.function.Predicate;
import kz.pompei.glazga.core.model.focus.FocusLogic;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.core.model.focus.MoveDirection;
import kz.pompei.glazga.core.model_for_tests.FocusLogicForTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("ExtractMethodRecommender")
public class UUtilTest {

  @DataProvider
  private Object[][] findNextFocusLogic__horizontal__DataProvider() {
    return new Object[][]{
      // top  currIndex    direction   longer    expectedResultIndex
      {" 2.5          0    to_right    short                       1   "},
      {" 3.5          0    to_right    short                       4   "},
      {" 4.5          0    to_right    short                       2   "},
      {" 7.5          0    to_right    short                       5   "},
      {" 8.5          0    to_right    short                       3   "},

      {" 2.5          0    to_right    long                        7   "},
      {" 3.5          0    to_right    long                        6   "},
      {" 4.5          0    to_right    long                        8   "},
      {" 7.5          0    to_right    long                        6   "},
      {" 8.5          0    to_right    long                        9   "},

      {" 0.5          1    to_right    short                       7   "},
      {" 1.5          1    to_right    short                       6   "},
      {" 2.5          1    to_right    short                       4   "},
      {" 4.5          2    to_right    short                       4   "},
      {" 5.5          2    to_right    short                       6   "},
      {" 6.5          2    to_right    short                       5   "},
      {" 8.5          3    to_right    short                       5   "},


      {" 0.5          1    to_right    long                        7   "},
      {" 1.5          1    to_right    long                        7   "},
      {" 2.5          1    to_right    long                        7   "},
      {" 4.5          2    to_right    long                        8   "},
      {" 5.5          2    to_right    long                        8   "},
      {" 6.5          2    to_right    long                        8   "},
      {" 8.5          3    to_right    long                        9   "},

      {" 2.5          4    to_right    short                       6   "},
      {" 3.5          4    to_right    short                       6   "},
      {" 4.5          4    to_right    short                       6   "},
      {" 6.5          5    to_right    short                       6   "},
      {" 7.5          5    to_right    short                       6   "},
      {" 8.5          5    to_right    short                       6   "},

      {" 2.5          4    to_right    long                        7   "},
      {" 3.5          4    to_right    long                        6   "},
      {" 4.5          4    to_right    long                        8   "},
      {" 6.5          5    to_right    long                        8   "},
      {" 7.5          5    to_right    long                        6   "},
      {" 8.5          5    to_right    long                        9   "},

      {" 2.5          6    to_right    short                       7   "},
      {" 3.5          6    to_right    short                       x   "},
      {" 5.5          6    to_right    short                       8   "},
      {" 7.5          6    to_right    short                       x   "},
      {" 8.5          6    to_right    short                       9   "},

      {" 2.5          6    to_right    long                        7   "},
      {" 3.5          6    to_right    long                        x   "},
      {" 5.5          6    to_right    long                        8   "},
      {" 7.5          6    to_right    long                        x   "},
      {" 8.5          6    to_right    long                        9   "},

      {" 1.5          7    to_right    short                       x   "},
      {" 1.5          7    to_right    long                        x   "},

      {" 0.5          7    to_left     short                       1   "},
      {" 1.5          7    to_left     short                       6   "},
      {" 4.5          8    to_left     short                       6   "},

      {" 0.5          7    to_left     long                        1   "},
      {" 1.5          7    to_left     long                        1   "},
      {" 4.5          8    to_left     long                        0   "},

      {" 9.5          9    to_left     short                       x   "},
      {" 9.5          9    to_left     long                        x   "},

      {" 1.5          6    to_left     short                       1   "},
      {" 2.5          6    to_left     short                       4   "},
      {" 5.5          6    to_left     short                       2   "},

      {" 1.5          6    to_left     long                        1   "},
      {" 2.5          6    to_left     long                        0   "},
      {" 5.5          6    to_left     long                        0   "},

      {" 1.5          1    to_left     short                       x   "},
      {" 2.5          1    to_left     short                       0   "},

      {" 1.5          1    to_left     long                        x   "},
      {" 2.5          1    to_left     long                        0   "},

      {" 2.5          0    to_left     short                       x   "},
      {" 2.5          0    to_left     long                        x   "},

    };
  }

  @Test(dataProvider = "findNextFocusLogic__horizontal__DataProvider")
  public void findNextFocusLogic__horizontal(String input) {
    String[]      split               = input.trim().split("\\s+");
    double        top                 = Double.parseDouble(split[0]);
    int           currIndex           = Integer.parseInt(split[1]);
    MoveDirection direction           = "to_right".equals(split[2]) ? MoveDirection.TO_RIGHT : MoveDirection.TO_LEFT;
    boolean       longer              = "long".equals(split[3]);
    int           expectedResultIndex = "x".equals(split[4]) ? -1 : Integer.parseInt(split[4]);

    var f0 = new FocusLogicForTests(0, 0, 2, 1, 9);
    var f1 = new FocusLogicForTests(1, 2, 0, 3, 3);
    var f2 = new FocusLogicForTests(2, 2, 4, 3, 7);
    var f3 = new FocusLogicForTests(3, 2, 8, 3, 9);
    var f4 = new FocusLogicForTests(4, 4, 2, 5, 5);
    var f5 = new FocusLogicForTests(5, 4, 6, 5, 9);
    var f6 = new FocusLogicForTests(6, 6, 1, 7, 9);
    var f7 = new FocusLogicForTests(7, 8, 0, 9, 3);
    var f8 = new FocusLogicForTests(8, 8, 4, 9, 7);
    var f9 = new FocusLogicForTests(9, 8, 8, 9, 9);

    List<FocusLogicForTests> list        = List.of(f0, f1, f2, f3, f4, f5, f6, f7, f8, f9);
    List<FocusLogic>         listToRight = new ArrayList<>(list);
    FocusMove                focusMove   = FocusMove.of(direction, longer);
    Predicate<FocusLogic>    isCurrent   = f -> f instanceof FocusLogicForTests x && x.number == currIndex;
    DoubleSupplier           left        = () -> {throw new RuntimeException("W4H1JX7wxA :: Need not read left");};

    listToRight.sort(Comparator.comparingDouble(x -> x.rect().left));

    //
    //
    FocusLogic result = UUtil.findNextFocusLogic(focusMove, listToRight, null, isCurrent, left, () -> top);
    //
    //

    if (expectedResultIndex < 0) {
      assertThat(result).isNull();
    } else {
      assertThat(result).isSameAs(list.get(expectedResultIndex));
    }
  }


  @DataProvider
  private Object[][] findNextFocusLogic__vertical__DataProvider() {
    return new Object[][]{
      // left  currIndex    direction   longer    expectedResultIndex
      {"  2.5          0    to_down     short                       2   "},
      {"  4.5          0    to_down     short                       8   "},
      {"  6.5          1    to_down     short                       3   "},

      {"  2.5          0    to_down     long                        8   "},
      {"  4.5          0    to_down     long                        8   "},
      {"  6.5          1    to_down     long                        9  "},

      {"  0.5          2    to_down     short                       8  "},
      {"  1.5          2    to_down     short                       6  "},
      {"  3.5          2    to_down     short                       4  "},
      {"  5.5          3    to_down     short                       7  "},
      {"  6.5          3    to_down     short                       5  "},
      {"  7.5          3    to_down     short                       5  "},
      {"  8.5          3    to_down     short                       x  "},

      {"  0.5          2    to_down     long                        8  "},
      {"  1.5          2    to_down     long                        8  "},
      {"  3.5          2    to_down     long                        8  "},
      {"  5.5          3    to_down     long                        7  "},
      {"  6.5          3    to_down     long                        9  "},
      {"  7.5          3    to_down     long                        9  "},
      {"  8.5          3    to_down     long                        x  "},

      {"  0.5          8    to_up       short                       2  "},
      {"  1.5          8    to_up       short                       6  "},
      {"  4.5          8    to_up       short                       x  "},
      {"  6.5          9    to_up       short                       7  "},

      {"  0.5          8    to_up       long                        2  "},
      {"  1.5          8    to_up       long                        0  "},
      {"  4.5          8    to_up       long                        x  "},
      {"  6.5          9    to_up       long                        1  "},

    };
  }

  @Test(dataProvider = "findNextFocusLogic__vertical__DataProvider")
  public void findNextFocusLogic__vertical(String input) {
    String[]      split               = input.trim().split("\\s+");
    double        left                = Double.parseDouble(split[0]);
    int           currIndex           = Integer.parseInt(split[1]);
    MoveDirection direction           = "to_down".equals(split[2]) ? MoveDirection.TO_DOWN : MoveDirection.TO_UP;
    boolean       longer              = "long".equals(split[3]);
    int           expectedResultIndex = "x".equals(split[4]) ? -1 : Integer.parseInt(split[4]);

    var f0 = new FocusLogicForTests(0, 1, 0, 3, 1);
    var f1 = new FocusLogicForTests(1, 6, 0, 8, 1);
    var f2 = new FocusLogicForTests(2, 0, 2, 4, 3);
    var f3 = new FocusLogicForTests(3, 5, 2, 9, 3);
    var f4 = new FocusLogicForTests(4, 2, 4, 4, 5);
    var f5 = new FocusLogicForTests(5, 6, 4, 8, 5);
    var f6 = new FocusLogicForTests(6, 1, 6, 4, 7);
    var f7 = new FocusLogicForTests(7, 5, 6, 7, 7);
    var f8 = new FocusLogicForTests(8, 0, 8, 5, 9);
    var f9 = new FocusLogicForTests(9, 6, 8, 8, 9);

    List<FocusLogicForTests> list       = List.of(f0, f1, f2, f3, f4, f5, f6, f7, f8, f9);
    List<FocusLogic>         listToDown = new ArrayList<>(list);
    FocusMove                focusMove  = FocusMove.of(direction, longer);
    Predicate<FocusLogic>    isCurrent  = f -> f instanceof FocusLogicForTests x && x.number == currIndex;
    DoubleSupplier           top        = () -> {throw new RuntimeException("UtVzs8A1YN :: Need not read top ");};

    listToDown.sort(Comparator.comparingDouble(x -> x.rect().top));

    //
    //
    FocusLogic result = UUtil.findNextFocusLogic(focusMove, null, listToDown, isCurrent, () -> left, top);
    //
    //

    if (expectedResultIndex < 0) {
      assertThat(result).isNull();
    } else {
      assertThat(result).isSameAs(list.get(expectedResultIndex));
    }
  }
}
