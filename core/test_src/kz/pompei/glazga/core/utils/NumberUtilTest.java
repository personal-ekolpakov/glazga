package kz.pompei.glazga.core.utils;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NumberUtilTest {

  @DataProvider
  private Object[][] doubleToStrLen_DataProvider() {
    return new Object[][]{
      //@formatter:off
      {  123.34,   "  123.34", 5, 2},
      {   13.1,    "   13.1 ", 5, 2},
      {12345.3478, "12345.35", 5, 2},
      {    1.008,  "    1.01", 5, 2},
      {   17.0,    "   17   ", 5, 2},

      {12345.3478, "12345", 5, 0},
      {  345.3478, "  345", 5, 0},
      {  345.378 , "  345.4", 5, 1},
      { -345.378 , " -345.4", 5, 1},

      {    0     , "    0"   , 5, 0},
      {    0     , "    0  " , 5, 1},
      {    0     , "    0   ", 5, 2},
      {    0.1   , "    0.1 ", 5, 2},
      {   -0.1   , "   -0.1 ", 5, 2},
      {   -0.1071, "   -0.11", 5, 2},

      {1234567.1071, "1234567.11", 5, 2},

      {1e10, "10000000000   ", 5, 2},

      {1e100, "92233720368547758.07", 5, 2},
      //@formatter:on
    };
  }

  @Test(dataProvider = "doubleToStrLen_DataProvider")
  public void doubleToStrLen(double src, String expectedStr, int intLen, int fracLen) {

    //
    //
    String actualStr = NumberUtil.doubleToStrLen(src, intLen, fracLen);
    //
    //

    assertThat(actualStr).isEqualTo(expectedStr);

  }
}
