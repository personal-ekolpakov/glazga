package kz.pompei.glazga.core.model_for_tests;

import kz.pompei.glazga.core.model.focus.FocusLogic;
import kz.pompei.glazga.core.model.focus.FocusMove;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.mat.Vec;
import lombok.NonNull;

public class FocusLogicForTests implements FocusLogic {

  private final Rect rect;
  public final  int  number;

  public FocusLogicForTests(int number, Rect rect) {
    this.number = number;
    this.rect   = rect;
  }

  public FocusLogicForTests(int number, double left, double top, double right, double bottom) {
    this(number, Rect.of(left, top, right, bottom));
  }

  @Override
  public Rect rect() {
    return rect;
  }

  @Override
  public void takeFocusAndRepaint(FocusMove focusMove) {
    throw new RuntimeException("txsKqvVlxF :: Not impl");
  }

  @Override
  public boolean takeFocusAt(@NonNull Vec pos) {
    throw new RuntimeException("19.08.2023 15:12 Not impl yet FocusLogicForTests.takeFocusAt()");
  }

  @Override
  public String toString() {
    return "FocusLogic{" + number + '}';
  }
}
