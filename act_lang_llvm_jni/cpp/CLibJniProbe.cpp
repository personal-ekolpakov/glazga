#include <jni.h>
#include "kz_pompei_glazga_llvm_jni_CLibJniProbe.h"
#include <cstring>
#include <cstdlib>


JNIEXPORT jstring JNICALL
Java_kz_pompei_glazga_llvm_jni_CLibJniProbe_concatStrings
    (JNIEnv *env, jobject obj, jstring string1, jstring string2) {

  const char *prefix = "jJQrDzqZLj :: Конкатенация строк это: ";

  const char *str1 = env->GetStringUTFChars(string1, nullptr);
  const char *str2 = env->GetStringUTFChars(string2, nullptr);

  char *result = (char *) malloc(strlen(prefix) + strlen(str1) + strlen(str2) + 1); // +1 for the null-terminator
  strcpy(result, prefix);
  strcat(result, str1);
  strcat(result, str2);

  env->ReleaseStringUTFChars( string1, str1);
  env->ReleaseStringUTFChars( string2, str2);

  jstring ret = env->NewStringUTF(result);

  free(result);

  return ret;

}
