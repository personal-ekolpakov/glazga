#!/bin/bash

cd "$(dirname "$0")" || exit 113

export JAVA_HOME=/usr/lib/jvm/java-21-openjdk-amd64

make
