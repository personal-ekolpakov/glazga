package kz.pompei.glazga.llvm.jni;

import java.nio.file.Path;
import kz.pompei.glazga.env.Modules;

public class CLibJniProbe {

  public native String concatStrings(String str1, String str2);

  public static void main(String[] args) throws Exception {

    Path cppRoot = Modules.prjRoot().resolve("act_lang_llvm_jni").resolve("cpp");

    ProcessBuilder builder = new ProcessBuilder();

    builder.directory(cppRoot.toFile())
           .redirectOutput(ProcessBuilder.Redirect.INHERIT)
           .redirectError(ProcessBuilder.Redirect.INHERIT)
           .command("make");

    builder.environment().put("JAVA_HOME", "/usr/lib/jvm/java-21-openjdk-amd64");

    int exitCode = builder.start().waitFor();
    if (exitCode != 0) {
      throw new RuntimeException("WLF4pPLzJO :: Back compile CPP. Exit code = " + exitCode);
    }

    String pathToSo = cppRoot.resolve("build").resolve("jni_probe.so")
                             .toAbsolutePath()
                             .normalize()
                             .toString();

    System.load(pathToSo);

    CLibJniProbe x = new CLibJniProbe();

    System.out.println("5fHq1glVgt :: " + x.concatStrings("hello ", "WORLD"));
  }

}
