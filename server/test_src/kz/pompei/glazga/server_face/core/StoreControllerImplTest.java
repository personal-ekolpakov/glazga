package kz.pompei.glazga.server_face.core;

import java.util.HashMap;
import java.util.List;
import kz.pompei.glazga.for_tests.StorageForTests;
import kz.pompei.glazga.server.controller.StoreControllerImpl;
import kz.pompei.glazga.server_face.model.eye.Eye;
import kz.pompei.glazga.server_face.model.eye.block.BlockAssign;
import kz.pompei.glazga.server_face.model.eye.block.BlockExit;
import kz.pompei.glazga.server_face.model.eye.block.exit.ExitKind;
import kz.pompei.glazga.server_face.model.eye.etc.EyeData;
import kz.pompei.glazga.server_face.model.update.MapUpdate;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StoreControllerImplTest {

  @Test
  public void loadEyeData__emptyStorage() {

    StorageForTests storage = new StorageForTests();

    StoreControllerImpl controller = new StoreControllerImpl(storage);

    //
    //
    EyeData eyeData = controller.loadEyeData("crH05Gz99d");
    //
    //

    Eye eye = eyeData.eye;


    assertThat(eye).isNotNull();

    assertThat(eye.blocks).isNotNull()
                          .isEmpty();

    assertThat(eye.expressions).isNotNull()
                               .isEmpty();

  }

  @Test
  public void loadEye__loadSomeValue() {

    StorageForTests storage = new StorageForTests();

    HashMap<String, String> test = new HashMap<>();
    storage.datum.putIfAbsent("test", test);

    test.put("blocks.c1.type", "BlockAssign");
    test.put("blocks.c1.downBlockId", "id1");
    test.put("blocks.c1.leftExprId", "id2");
    test.put("blocks.c1.rightExprId", "id3");

    test.put("blocks.c2.type", "BlockExit");
    test.put("blocks.c2.downBlockId", "id4");
    test.put("blocks.c2.kind", ExitKind.CIRCLE_CONTINUE.name());
    test.put("blocks.c2.throwExprId", "id5");
    test.put("blocks.c2.returnExprId", "id6");

    StoreControllerImpl controller = new StoreControllerImpl(storage);

    //
    //
    EyeData eyeData = controller.loadEyeData("test");
    //
    //

    Eye eye = eyeData.eye;


    assertThat(eye).isNotNull();

    assertThat(eye.blocks).isNotNull();

    assertThat(eye.expressions).isNotNull()
                               .isEmpty();

    BlockAssign c1 = (BlockAssign) eye.blocks.get("c1");
    assertThat(c1.downBlockId).isEqualTo("id1");
    assertThat(c1.leftExprId).isEqualTo("id2");
    assertThat(c1.rightExprId).isEqualTo("id3");

    BlockExit c2 = (BlockExit) eye.blocks.get("c2");
    assertThat(c2.downBlockId).isEqualTo("id4");
    assertThat(c2.kind).isEqualTo(ExitKind.CIRCLE_CONTINUE);
    assertThat(c2.throwExprId).isEqualTo("id5");
    assertThat(c2.returnExprId).isEqualTo("id6");
  }

  @Test
  public void save__changeValues() {

    StorageForTests storage = new StorageForTests();

    HashMap<String, String> test = new HashMap<>();
    storage.datum.putIfAbsent("test", test);

    test.put("blocks.c1.type", "BlockAssign");
    test.put("blocks.c1.downBlockId", "id1");
    test.put("blocks.c1.leftExprId", "id2");
    test.put("blocks.c1.rightExprId", "id3");

    test.put("blocks.c2.type", "BlockExit");
    test.put("blocks.c2.downBlockId", "id4");
    test.put("blocks.c2.kind", ExitKind.CIRCLE_CONTINUE.name());
    test.put("blocks.c2.throwExprId", "id5");
    test.put("blocks.c2.returnExprId", "id6");

    StoreControllerImpl controller = new StoreControllerImpl(storage);

    //
    //
    controller.save("test", "", "", List.of(
      MapUpdate.of("blocks.c1.downBlockId", "id_changed_1"),
      MapUpdate.of("blocks.c2.throwExprId", "id_changed_2")
    ), List.of());
    //
    //

    assertThat(test).containsEntry("blocks.c1.downBlockId", "id_changed_1");
    assertThat(test).containsEntry("blocks.c2.throwExprId", "id_changed_2");
  }

  @Test
  public void save__deleteValues() {

    StorageForTests storage = new StorageForTests();

    HashMap<String, String> test = new HashMap<>();
    storage.datum.put("test", test);

    test.put("blocks.c1.type", "BlockAssign");
    test.put("blocks.c1.downBlockId", "id1");
    test.put("blocks.c1.leftExprId", "id2");
    test.put("blocks.c1.rightExprId", "id3");

    test.put("blocks.c2.type", "BlockExit");
    test.put("blocks.c2.downBlockId", "id4");
    test.put("blocks.c2.kind", ExitKind.CIRCLE_CONTINUE.name());
    test.put("blocks.c2.throwExprId", "id5");
    test.put("blocks.c2.returnExprId", "id6");

    StoreControllerImpl controller = new StoreControllerImpl(storage);

    //
    //
    controller.save("test", "", "", List.of(
      MapUpdate.of("blocks.c1.downBlockId", null),
      MapUpdate.of("blocks.c2.throwExprId", null)
    ), List.of());
    //
    //

    assertThat(test).doesNotContainKey("blocks.c1.downBlockId");
    assertThat(test).doesNotContainKey("blocks.c2.throwExprId");

  }

}
