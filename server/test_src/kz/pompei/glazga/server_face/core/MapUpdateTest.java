package kz.pompei.glazga.server_face.core;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import kz.pompei.glazga.moc.MapObjectConverter;
import kz.pompei.glazga.moc.MocUtils;
import kz.pompei.glazga.server_face.model.update.MapUpdate;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MapUpdateTest {

  @Test
  public void viewProperties() {

    Class<MapUpdate> aClass = MapUpdate.class;

    for (final Method m : aClass.getDeclaredMethods()) {
      System.out.println("5XBzo2Y3je :: m = " + m.getName());
      for (final Annotation a : m.getDeclaredAnnotations()) {
        System.out.println("kLKIqsRia1 ::       ann = " + a);
      }
    }

    for (final Constructor<?> c : aClass.getDeclaredConstructors()) {
      System.out.println(
        "2jfyIJO6En :: c = " + Arrays.stream(c.getParameterTypes()).map(Class::getName).collect(Collectors.joining(", ", "[", "]")));
      for (final Annotation a : c.getDeclaredAnnotations()) {
        System.out.println("baDwNVsK6m ::       ann = " + a);
      }
    }

  }

  @Test(enabled = false)
  public void parsing() {

    MapUpdate update = MapUpdate.of("asd.dsa", "some value");

    MapObjectConverter converter = new MapObjectConverter();

    Map<String, Object> mapUpdate = converter.convertToMap(update);

    MocUtils.printMap("GOv5MmIW7F", mapUpdate);

    MapUpdate actual = converter.convertFromMap(mapUpdate, MapUpdate.class);

    System.out.println("Gv4TQ31174 :: actual = " + actual);

    assertThat(actual).isEqualTo(update);
  }
}
