package kz.pompei.glazga.server.controller;

import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toUnmodifiableMap;

public class ExtractUtil {
  @SuppressWarnings("SameParameterValue")
  public static Map<String, String> extractMapStr(Map<String, String> map, String keyPrefix) {
    return map.entrySet()
              .stream()
              .filter(e -> e.getKey() != null)
              .filter(e -> e.getKey().startsWith(keyPrefix))
              .collect(toUnmodifiableMap(e -> e.getKey().substring(keyPrefix.length()),
                                         Map.Entry::getValue));
  }

  @SuppressWarnings("SameParameterValue")
  public static String extractStr(Map<String, String> map, String key, String defaultValue) {
    return Optional.of(map).map(x -> x.get(key)).orElse(defaultValue);
  }

  public static boolean extractBool(Map<String, String> map, String key, boolean defaultValue) {

    String str = map.get(key);
    if (str == null) {
      return defaultValue;
    }

    String ss = str.trim().toLowerCase();

    return "1".equals(ss) || "true".equals(ss) || "t".equals(ss) || "y".equals(ss) || "yes".equals(ss) || "on".equals(ss);
  }

  @SuppressWarnings("SameParameterValue")
  public static <E extends Enum<E>> E extractEnum(Map<String, String> map, String key, Class<E> enumClass, E defaultValue) {
    try {
      return Enum.valueOf(enumClass, map.get(key));
    } catch (IllegalArgumentException | NullPointerException ignore) {
      return defaultValue;
    }
  }

  public static Double extractDouble(Map<String, String> map, String key, Double defaultValue) {
    String str = map.get(key);
    if (str == null) {
      return defaultValue;
    }
    try {
      return Double.parseDouble(str);
    } catch (NumberFormatException ignore) {
      return defaultValue;
    }
  }

}
