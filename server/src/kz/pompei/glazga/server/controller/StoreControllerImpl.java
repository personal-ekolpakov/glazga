package kz.pompei.glazga.server.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.mat.LinearTrans;
import kz.pompei.glazga.moc.MapObjectConverter;
import kz.pompei.glazga.moc.OneLevel;
import kz.pompei.glazga.server_face.controller.StoreController;
import kz.pompei.glazga.server_face.model.eye.Eye;
import kz.pompei.glazga.server_face.model.eye.etc.ActSpec;
import kz.pompei.glazga.server_face.model.eye.etc.ActSpecArg;
import kz.pompei.glazga.server_face.model.eye.etc.ConnectType;
import kz.pompei.glazga.server_face.model.eye.etc.EyeData;
import kz.pompei.glazga.server_face.model.eye.etc.UFocusRef;
import kz.pompei.glazga.server_face.model.update.MapUpdate;
import kz.pompei.glazga.store.Storage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import static kz.pompei.glazga.server.controller.ExtractUtil.extractBool;
import static kz.pompei.glazga.server.controller.ExtractUtil.extractDouble;
import static kz.pompei.glazga.server.controller.ExtractUtil.extractEnum;
import static kz.pompei.glazga.server.controller.ExtractUtil.extractStr;

@RequiredArgsConstructor
public class StoreControllerImpl implements StoreController {

  private final       Storage storage;
  public static final String  FOCUS = ".focus";
  public static final String  TRANS = ".trans";

  @Override
  public EyeData loadEyeData(String filePath) {

    Map<String, Object> deepMap = new HashMap<>();

    storage.loadAll(filePath)
           .forEach((k, v) -> OneLevel.set(deepMap, k, v));

    MapObjectConverter converter = new MapObjectConverter();

    Eye eye = converter.convertFromMap(deepMap, Eye.class);

    {
      EyeData eyeData = new EyeData();
      eyeData.eye = eye;
      eyeData.trans    = loadTrans(filePath);
      eyeData.focusRef = loadFocusRef(filePath);

      initActSpecifications(eyeData.actSpecifications);

      return eyeData;
    }
  }

  @Override
  public void save(@NotNull String filePath, @NotNull String changeName, String changeGroup,
                   @NotNull List<MapUpdate> fw, @NotNull List<MapUpdate> bk) {

    Map<String, String> map = new HashMap<>();
    for (final MapUpdate u : fw) {
      map.put(u.dotPath, u.simpleValue);
    }
    storage.save(filePath, map);
  }

  private @NonNull LinearTrans loadTrans(@NonNull String filePath) {
    Map<String, String> map = storage.loadAll(filePath + TRANS);

    double Kx = extractDouble(map, "Kx", 1.0);
    double Ky = extractDouble(map, "Ky", 1.0);
    double Bx = extractDouble(map, "Bx", 0.0);
    double By = extractDouble(map, "By", 0.0);

    return LinearTrans.of(Kx, Ky, Bx, By);
  }

  private @NonNull UFocusRef loadFocusRef(@NonNull String filePath) {
    Map<String, String> map = storage.loadAll(filePath + FOCUS);

    Double      left        = extractDouble(map, UFocusRef.Fields.left, null);
    Double      top         = extractDouble(map, UFocusRef.Fields.top, null);
    UType       uType       = extractEnum(map, UFocusRef.Fields.uType, UType.class, null);
    String      elementId   = extractStr(map, UFocusRef.Fields.elementId, null);
    String      connectName = extractStr(map, UFocusRef.Fields.connectName, null);
    ConnectType connectType = extractEnum(map, UFocusRef.Fields.uType, ConnectType.class, null);
    String      state       = extractStr(map, UFocusRef.Fields.state, null);
    boolean     selectedEye = extractBool(map, UFocusRef.Fields.selectedEye, false);

    return UFocusRef.of(left, top, uType, elementId, connectName, connectType, state, selectedEye);
  }

  @Override
  public void saveFocusRef(@NonNull String filePath, String changeGroup, @NonNull UFocusRef focusRef) {
    Map<String, String> map = convertFocusRefToMap(focusRef);

    System.out.println("1Nc03soTn9 :: Before save focus : focusRef = " + focusRef);

    storage.save(filePath + FOCUS, map);
  }

  @NotNull
  private static Map<String, String> convertFocusRefToMap(@NotNull UFocusRef focusRef) {
    Map<String, String> map = new HashMap<>();

    map.put(UFocusRef.Fields.left, focusRef.left != null ? "" + focusRef.left : null);
    map.put(UFocusRef.Fields.top, focusRef.top != null ? "" + focusRef.top : null);
    map.put(UFocusRef.Fields.uType, focusRef.uType == null ? null : focusRef.uType.name());
    map.put(UFocusRef.Fields.elementId, focusRef.elementId);
    map.put(UFocusRef.Fields.connectName, focusRef.connectName);
    map.put(UFocusRef.Fields.connectType, focusRef.connectType == null ? null : focusRef.connectType.name());
    map.put(UFocusRef.Fields.state, focusRef.state);
    map.put(UFocusRef.Fields.selectedEye, "" + focusRef.selectedEye);

    return map;
  }

  @Override
  public void saveTrans(@NonNull String filePath, String changeGroup, @NonNull LinearTrans trans) {

    Map<String, String> data = new HashMap<>();
    data.put("Kx", "" + trans.Kx);
    data.put("Ky", "" + trans.Ky);
    data.put("Bx", "" + trans.Bx);
    data.put("By", "" + trans.By);

    storage.save(filePath + TRANS, data);
  }

  private void initActSpecifications(Map<String, ActSpec> actSpecifications) {
    {
      ActSpec spec = new ActSpec();
      actSpecifications.put("act001", spec);

      spec.label = "Перейти ";
      {
        ActSpecArg a = new ActSpecArg();
        spec.args.put("a1", a);
        a.labelBefore = "на ";
        a.labelAfter  = "сек.";
        a.nativeOrder = 1;
      }
      {
        ActSpecArg a = new ActSpecArg();
        spec.args.put("a2", a);
        a.labelBefore = "по ";
        a.labelAfter  = "шт.";
        a.nativeOrder = 2;
      }
      {
        ActSpecArg a = new ActSpecArg();
        spec.args.put("a3", a);
        a.labelBefore = "каждые ";
        a.labelAfter  = "дн.";
        a.nativeOrder = 3;
      }
    }
  }

}
