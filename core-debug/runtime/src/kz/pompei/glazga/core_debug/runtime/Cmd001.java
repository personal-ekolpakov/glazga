package kz.pompei.glazga.core_debug.runtime;

import kz.pompei.glazga.core.EyePanel;
import kz.pompei.glazga.core_debug.cmd.Cmd;
import kz.pompei.glazga.utils.UsedOut;

@UsedOut
public class Cmd001 implements Cmd {
  EyePanel eyePanel;

  @Override
  public void setEyePanel(EyePanel eyePanel) {
    this.eyePanel = eyePanel;
  }

  @Override
  public void execute() {

//    eyePanel.focus().selectElementConnect(UType.BLOCK, "b3", ConnectType.DOWN, "MAIN");
//    eyePanel.focus().setState("asd");
//    eyePanel.focus().saveAndRefreshFocus();

//    eyePanel.focus().selectElement(UType.EXPR, "nd_op1_c2");
//    eyePanel.focus().setState("a0");
//    eyePanel.focus().saveAndRefreshFocus();

    eyePanel.focus().setLeft(null);
    eyePanel.focus().setTop(null);

    System.out.println("tWRYaRIK6X :: Executed command " + getClass().getSimpleName());
  }
}
