package kz.pompei.glazga.core_debug.cmd;

import kz.pompei.glazga.core.EyePanel;

public interface Cmd {

  void setEyePanel(EyePanel eyePanel);

  void execute() throws Exception;
}
