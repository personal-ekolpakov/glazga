package kz.pompei.glazga.core_debug.cmd;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor
public class JarClassLoader extends ClassLoader {
  private final ZipFile zipFile;

  @SneakyThrows
  public JarClassLoader(File jarFile) {
    zipFile = new ZipFile(jarFile);
  }

  @Override
  @SneakyThrows
  protected Class<?> findClass(String name) throws ClassNotFoundException {

    ZipEntry entry = zipFile.getEntry(name.replace('.', '/') + ".class");

    if (entry == null) {
      throw new ClassNotFoundException("qXE6W0q2Vt :: " + name);
    }

    try (InputStream inputStream = zipFile.getInputStream(entry)) {

      if (inputStream == null) {
        throw new ClassNotFoundException("JP2Re809xX :: " + name);
      }

      byte[] bytes = inputStream.readAllBytes();

      return defineClass(name, bytes, 0, bytes.length);

    }


  }

  @Override
  public InputStream getResourceAsStream(String name) {
    // Get the entry by its name
    ZipEntry entry = zipFile.getEntry(name);
    if (entry != null) {
      // The entry could be found
      try {
        // Gives the content of the entry as InputStream
        return zipFile.getInputStream(entry);
      } catch (IOException e) {
        // Could not get the content of the entry
        // you could log the error if needed
        return null;
      }
    }
    // The entry could not be found
    return null;
  }
}
