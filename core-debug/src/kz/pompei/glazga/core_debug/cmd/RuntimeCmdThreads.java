package kz.pompei.glazga.core_debug.cmd;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import kz.pompei.glazga.core.EyePanel;
import lombok.SneakyThrows;

public class RuntimeCmdThreads {
  private final Map<String, String> cmdRegistry   = new HashMap<>();
  private final Map<File, String>   filesClassMap = new HashMap<>();

  public EyePanel eyePanel;

  private final AtomicBoolean working = new AtomicBoolean(true);

  private final Thread thread = new Thread(() -> {
    while (working.get()) {

      boolean noExec = true;

      EXEC:
      for (final Map.Entry<File, String> e : filesClassMap.entrySet()) {
        File file = e.getKey();
        if (file.exists()) {
          continue EXEC;
        }

        {
          try {
            file.getParentFile().mkdirs();
            file.createNewFile();
          } catch (IOException ex) {
            throw new RuntimeException(ex);
          }

          try {
            executeCmdClass(e.getValue());
          } catch (Exception err) {
            System.out.println("L6QYxaO2Vt :: exec cmd " + file.getName() + " was error:\n");
            err.printStackTrace(System.out);
          }
          noExec = false;
        }
      }

      if (noExec) {
        try {
          Thread.sleep(300);
        } catch (InterruptedException e) {
          return;
        }
      }

    }
  });


  @SneakyThrows
  public void start() {
    Path commandsDir = Paths.get("build").resolve("commands");

    for (final Map.Entry<String, String> e : cmdRegistry.entrySet()) {

      File file = commandsDir.resolve(e.getKey()).toFile();
      file.getParentFile().mkdirs();
      file.createNewFile();

      filesClassMap.put(file, e.getValue());
    }

    thread.start();
  }

  public void close() {
    working.set(false);
    try {
      thread.join();
    } catch (InterruptedException e) {
      // do nothing
    }
  }

  public void put(String cmdFileName, String cmdClassName) {
    cmdRegistry.put(cmdFileName, cmdClassName);
  }

  @SneakyThrows
  private void executeCmdClass(String cmdClassName) {

    ProcessBuilder builder = new ProcessBuilder();

    builder.command("sh", "gradlew", ":core-debug:runtime:jar");

    builder.redirectError(ProcessBuilder.Redirect.INHERIT);
    builder.redirectInput(ProcessBuilder.Redirect.INHERIT);

    builder.directory(Paths.get("..").toFile());

    Process process = builder.start();

    int exitStatus = process.waitFor();
    if (exitStatus != 0) {
      throw new RuntimeException("pwwQ61ElbX :: compilation complete illegal");
    }
    System.out.println("gBHzj0vNM7 :: Compilation complete OK");

    File file = Paths.get("runtime").resolve("build").resolve("libs").resolve("runtime-0.0.1.jar").toFile();
    if (!file.exists()) {
      throw new RuntimeException("Zqzt12R1Wt :: No file " + file);
    }

    JarClassLoader jarClassLoader = new JarClassLoader(file);

    Class<?> cmdClass = jarClassLoader.findClass(cmdClassName);

    Cmd cmd = (Cmd) cmdClass.getConstructor().newInstance();

    cmd.setEyePanel(eyePanel);
    cmd.execute();
  }
}
