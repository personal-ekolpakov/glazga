package kz.pompei.glazga.core_debug.launchers.icons;

import java.io.InputStream;
import java.util.Objects;
import lombok.SneakyThrows;

public class IconAnchor {
  @SneakyThrows
  public static byte[] bytes(String name) {
    try (InputStream rs = IconAnchor.class.getResourceAsStream(name)) {
      Objects.requireNonNull(rs, "DA0g2Sd8Ue :: No resource with name = " + name);
      return rs.readAllBytes();
    }
  }
}
