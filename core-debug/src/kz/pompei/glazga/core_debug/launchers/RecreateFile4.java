package kz.pompei.glazga.core_debug.launchers;

import java.util.Map;
import kz.pompei.glazga.moc.MapObjectConverter;
import kz.pompei.glazga.moc.OneLevel;
import kz.pompei.glazga.server_face.model.eye.Eye;
import kz.pompei.glazga.server_face.model.eye.block.BlockExit;
import kz.pompei.glazga.server_face.model.eye.block.BlockFixMethodStart;
import kz.pompei.glazga.server_face.model.eye.block.BlockNewVar;
import kz.pompei.glazga.server_face.model.eye.block.exit.ExitKind;
import kz.pompei.glazga.server_face.model.eye.block.method_start.MethodArg;
import kz.pompei.glazga.server_face.model.eye.expr.ExprCall;
import kz.pompei.glazga.server_face.model.eye.expr.ExprConst;
import kz.pompei.glazga.server_face.model.eye.expr.ExprDiv;
import kz.pompei.glazga.server_face.model.eye.expr.ExprOp;
import kz.pompei.glazga.server_face.model.eye.expr.call.CallArg;
import kz.pompei.glazga.server_face.model.eye.expr.op.Op;
import kz.pompei.store_map_db.StorageMapDb;

import static kz.pompei.glazga.core_debug.launchers.DebugForm.TEST_FILE_NAME;
import static kz.pompei.glazga.core_debug.launchers.DebugForm.createStorageMapDb;

public class RecreateFile4 {
  public static void main(String[] args) {
    RecreateFile4 recreate = new RecreateFile4();

    recreate.recreate();
  }

  private void recreate() {
    StorageMapDb storage = createStorageMapDb(true);

    Eye eye = createEye();

    MapObjectConverter converter = new MapObjectConverter();

    Map<String, Object> eyeMap = converter.convertToMap(eye);

    OneLevel oneLevel = new OneLevel();

    Map<String, String> oneLevelMap = oneLevel.append(eyeMap);

    storage.save(TEST_FILE_NAME, oneLevelMap);
  }

  private Eye createEye() {
    var eye = new Eye();

    {
      var b = new BlockFixMethodStart();
      b.left = 30;
      b.top  = 10;
      eye.blocks.put("b1", b);
      b.methodName = "Hello World Call";
      b.methodId   = "OZc9YJ1KKh";
      {
        var a = new MethodArg();
        b.args.put("a1", a);
        a.orderIndex = 1;
        a.name       = "Status";
      }
      {
        var a = new MethodArg();
        b.args.put("a1", a);
        a.orderIndex = 2;
        a.name       = "Tank";
      }

      b.downBlockId = "b2";
    }
    {
      var b = new BlockNewVar();
      eye.blocks.put("b2", b);
      b.varName     = "Новая переменная";
      b.downBlockId = "b3";
      b.valueExprId = "e1";

      {
        var e = new ExprConst();
        eye.expressions.put("e1", e);
        e.def = "123.45e-18";
      }
    }
    {
      var b = new BlockNewVar();
      eye.blocks.put("b3", b);
      b.varName     = "Статус";
      b.downBlockId = "b4";
      b.valueExprId = "e2";

      {
        var e = new ExprDiv();
        eye.expressions.put("e2", e);
        e.upExprId   = "ec1";
        e.downExprId = "op1";
      }
      {
        var e = new ExprConst();
        eye.expressions.put("ec1-in", e);
        e.def = "1234.324";
      }
      {
        var e = new ExprCall();
        eye.expressions.put("ec1", e);
        e.actId = "act001";
        e.leftExprId = "ec1-in";
        e.argExprIds.put("a1", CallArg.withExprId("q1"));
        e.argExprIds.put("a2", CallArg.withExprId("q2"));
        e.argExprIds.put("a3", CallArg.withExprId("q3"));
        {
          var q = new ExprConst();
          eye.expressions.put("q1", q);
          q.def = "5646";
        }
        {
          var q = new ExprConst();
          eye.expressions.put("q2", q);
          q.def = "27";
        }
        {
          var q = new ExprConst();
          eye.expressions.put("q3", q);
          q.def = "71.4";
        }
      }
      {
        var e = new ExprOp();
        eye.expressions.put("op1", e);
        e.firstExprId = "ec2";
        e.arg("a1", Op.PLUS, "ec3");
        e.arg("a2", Op.MUL, "ec4");
        e.useFirst = true;
      }
      {
        var e = new ExprConst();
        eye.expressions.put("ec2", e);
        e.def = "111";
      }
      {
        var e = new ExprConst();
        eye.expressions.put("ec3", e);
        e.def = "222";
      }
      {
        var e = new ExprConst();
        eye.expressions.put("ec4", e);
        e.def = "333";
      }
    }
    {
      var b = new BlockNewVar();
      eye.blocks.put("b4", b);
      b.varName     = "Начинается новый день";
      b.downBlockId = "b5";
    }
    {
      var b = new BlockExit();
      eye.blocks.put("b5", b);
      b.kind = ExitKind.CIRCLE_CONTINUE;
    }

    return eye;
  }
}
