package kz.pompei.glazga.core_debug.launchers;

import java.util.Map;
import kz.pompei.glazga.moc.MapObjectConverter;
import kz.pompei.glazga.moc.OneLevel;
import kz.pompei.glazga.server_face.model.eye.Eye;
import kz.pompei.glazga.server_face.model.eye.block.BlockExit;
import kz.pompei.glazga.server_face.model.eye.block.BlockFixMethodStart;
import kz.pompei.glazga.server_face.model.eye.block.BlockNewVar;
import kz.pompei.glazga.server_face.model.eye.block.exit.ExitKind;
import kz.pompei.glazga.server_face.model.eye.block.method_start.MethodArg;
import kz.pompei.glazga.server_face.model.eye.expr.ExprConst;
import kz.pompei.glazga.server_face.model.eye.expr.ExprDiv;
import kz.pompei.store_map_db.StorageMapDb;

import static kz.pompei.glazga.core_debug.launchers.DebugForm.TEST_FILE_NAME;
import static kz.pompei.glazga.core_debug.launchers.DebugForm.createStorageMapDb;

public class RecreateFile2 {
  public static void main(String[] args) {
    RecreateFile2 recreate = new RecreateFile2();

    recreate.recreate();
  }

  private void recreate() {
    StorageMapDb storage = createStorageMapDb(true);

    Eye eye = createEye();

    MapObjectConverter converter = new MapObjectConverter();

    Map<String, Object> eyeMap = converter.convertToMap(eye);

    OneLevel oneLevel = new OneLevel();

    Map<String, String> oneLevelMap = oneLevel.append(eyeMap);

    storage.save(TEST_FILE_NAME, oneLevelMap);
  }

  private Eye createEye() {
    Eye eye = new Eye();

    {
      BlockFixMethodStart b = new BlockFixMethodStart();
      b.left = 30;
      b.top  = 10;
      eye.blocks.put("b1", b);
      b.methodName = "Hello World Call";
      b.methodId   = "OZc9YJ1KKh";
      {
        final MethodArg a = new MethodArg();
        b.args.put("a1", a);
        a.orderIndex = 1;
        a.name       = "Status";
      }
      {
        final MethodArg a = new MethodArg();
        b.args.put("a1", a);
        a.orderIndex = 2;
        a.name       = "Tank";
      }

      b.downBlockId = "b2";
    }
    {
      BlockNewVar b = new BlockNewVar();
      eye.blocks.put("b2", b);
      b.varName     = "Новая переменная";
      b.downBlockId = "b3";
      b.valueExprId = "e1";

      {
        ExprConst e = new ExprConst();
        eye.expressions.put("e1", e);
        e.def = "123.45e-18";
      }
    }
    {
      BlockNewVar b = new BlockNewVar();
      eye.blocks.put("b3", b);
      b.varName     = "Статус";
      b.downBlockId = "b4";
      b.valueExprId = "e2";

      {
        ExprDiv e = new ExprDiv();
        eye.expressions.put("e2", e);
      }
    }
    {
      BlockNewVar b = new BlockNewVar();
      eye.blocks.put("b4", b);
      b.varName     = "Начинается новый день";
      b.downBlockId = "b5";
    }
    {
      BlockExit b = new BlockExit();
      eye.blocks.put("b5", b);
      b.kind = ExitKind.CIRCLE_CONTINUE;
    }

    return eye;
  }
}
