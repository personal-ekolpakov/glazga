package kz.pompei.glazga.core_debug.launchers;

import java.util.Comparator;
import java.util.Map;
import kz.pompei.store_map_db.StorageMapDb;

import static kz.pompei.glazga.core_debug.launchers.DebugForm.TEST_FILE_NAME;
import static kz.pompei.glazga.server.controller.StoreControllerImpl.FOCUS;

public class ShowFocusRef {
  public static void main(String[] args) {
    new ShowFocusRef().run();
  }

  private void run() {
    StorageMapDb storage = DebugForm.createStorageMapDb(false);

    Map<String, String> focusMap = storage.loadAll(TEST_FILE_NAME + FOCUS);

    focusMap.entrySet()
            .stream()
            .sorted(Comparator.comparing(Map.Entry::getKey))
            .forEachOrdered(e -> System.out.println("rry82zUaa8 :: " + e.getKey() + " = " + e.getValue()));
  }
}
