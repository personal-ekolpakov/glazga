package kz.pompei.glazga.core_debug.launchers;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import kz.pompei.glazga.core.EyePanel;
import kz.pompei.glazga.core.events.EventPreparation;
import kz.pompei.glazga.core_debug.cmd.RuntimeCmdThreads;
import kz.pompei.glazga.core_debug.launchers.icons.IconAnchor;
import kz.pompei.glazga.paint.style.Styler;
import kz.pompei.glazga.paint.style.examples.Styler_STD_DAY;
import kz.pompei.glazga.server.controller.StoreControllerImpl;
import kz.pompei.glazga.utils.FileUtils;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.store_map_db.StorageMapDb;
import lombok.SneakyThrows;

public class DebugForm {

  public static void main(String[] args) {
    DebugForm debugForm = new DebugForm();

    debugForm.run();
  }

  private void run() {
    JFrame frame = new JFrame();
    frame.setSize(1024, 800);
    frame.setTitle("Core Test - ГлазГа");

    RuntimeCmdThreads runtimeCmdThreads = new RuntimeCmdThreads();
    runtimeCmdThreads.put("cmd001", "kz.pompei.glazga.core_debug.runtime.Cmd001");

    EyePanel eyePanel = createEyePane();

    runtimeCmdThreads.eyePanel = eyePanel;

    EventPreparation eventPreparation = new EventPreparation();
    eventPreparation.eventAcceptors = eyePanel::eventAcceptors;

    frame.setContentPane(eyePanel.source());

    SizeLocationSaver.into(Paths.get("build").resolve("sizes_and_locations"))
                     .overJFrame("DebugForm", frame, true);
    {
      ImageIcon imageIcon = new ImageIcon(IconAnchor.bytes("Glazga-64x64.png"));
      frame.setIconImage(imageIcon.getImage());
    }

    frame.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        eventPreparation.sendNativeEvent(e);
      }

      @Override
      @SneakyThrows
      public void keyReleased(KeyEvent e) {
        eventPreparation.sendNativeEvent(e);
      }
    });

    frame.addWindowFocusListener(new WindowFocusListener() {
      @Override
      public void windowGainedFocus(WindowEvent e) {
        eventPreparation.clean();
      }

      @Override
      public void windowLostFocus(WindowEvent e) {
        eventPreparation.clean();
      }
    });

    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        runtimeCmdThreads.close();
      }
    });

    runtimeCmdThreads.start();

    frame.setVisible(true);
  }

  public static final String TEST_FILE_NAME = "file.glazga";

  public static StorageMapDb createStorageMapDb(boolean deleteIfExists) {
    Path rootPath = Paths.get("build").resolve("storage");
    if (deleteIfExists) {
      FileUtils.removeFile(rootPath.resolve(TEST_FILE_NAME));
    }
    return new StorageMapDb(rootPath);
  }

  private static EyePanel createEyePane() {
    StorageMapDb        storage    = createStorageMapDb(false);
    StoreControllerImpl controller = new StoreControllerImpl(storage);
    Styler              styler     = new Styler_STD_DAY();

    EyePanel contentPane = new EyePanel(controller, () -> styler);
    contentPane.setFilePath(TEST_FILE_NAME);
    return contentPane;
  }
}
