package kz.pompei.glazga.core_debug.launchers;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.common_model.BracketKind;
import kz.pompei.glazga.common_model.UType;
import kz.pompei.glazga.moc.MapObjectConverter;
import kz.pompei.glazga.moc.OneLevel;
import kz.pompei.glazga.server_face.model.eye.Eye;
import kz.pompei.glazga.server_face.model.eye.block.BlockExit;
import kz.pompei.glazga.server_face.model.eye.block.BlockFixMethodStart;
import kz.pompei.glazga.server_face.model.eye.block.BlockNewVar;
import kz.pompei.glazga.server_face.model.eye.block.exit.ExitKind;
import kz.pompei.glazga.server_face.model.eye.block.method_start.MethodArg;
import kz.pompei.glazga.server_face.model.eye.expr.ExprBracket;
import kz.pompei.glazga.server_face.model.eye.expr.ExprCall;
import kz.pompei.glazga.server_face.model.eye.expr.ExprConst;
import kz.pompei.glazga.server_face.model.eye.expr.ExprDiv;
import kz.pompei.glazga.server_face.model.eye.expr.ExprOp;
import kz.pompei.glazga.server_face.model.eye.expr.call.CallArg;
import kz.pompei.glazga.server_face.model.eye.expr.op.Op;
import kz.pompei.store_map_db.StorageMapDb;

import static kz.pompei.glazga.core_debug.launchers.DebugForm.TEST_FILE_NAME;
import static kz.pompei.glazga.core_debug.launchers.DebugForm.createStorageMapDb;

public class RecreateFile5 {
  public static void main(String[] args) {
    RecreateFile5 recreate = new RecreateFile5();

    recreate.recreate();
  }

  private void recreate() {

    StorageMapDb storage = createStorageMapDb(false);

    {
      Eye eye = createEye();

      MapObjectConverter converter = new MapObjectConverter();

      Map<String, Object> eyeMap = converter.convertToMap(eye);

      OneLevel oneLevel = new OneLevel();

      Map<String, String> oneLevelMap = oneLevel.append(eyeMap);

      storage.save(TEST_FILE_NAME, oneLevelMap);
    }

    {
      Map<String, String> focus = new HashMap<>();
      focus.put("uType", UType.EXPR.name());
      focus.put("elementId", "nd_op1_c2");
      storage.save(TEST_FILE_NAME + ".focus", focus);
    }
  }

  private Eye createEye() {
    var eye = new Eye();

    {
      var b = new BlockFixMethodStart();
      b.left = 30;
      b.top  = 10;
      eye.blocks.put("b1", b);
      b.methodName = "Hello World Call";
      b.methodId   = "OZc9YJ1KKh";
      {
        var a = new MethodArg();
        b.args.put("a1", a);
        a.orderIndex = 1;
        a.name       = "Status";
      }
      {
        var a = new MethodArg();
        b.args.put("a1", a);
        a.orderIndex = 2;
        a.name       = "Tank";
      }

      b.downBlockId = "b2";
    }
    {
      var b = new BlockNewVar();
      eye.blocks.put("b2", b);
      b.varName     = "Новая переменная";
      b.downBlockId = "b3";
      b.valueExprId = "e1";

      {
        var e = new ExprConst();
        eye.expressions.put("e1", e);
        e.def = "123.45e-18";
      }
    }
    {
      var b = new BlockNewVar();
      eye.blocks.put("b3", b);
      b.varName     = "Статус";
      b.downBlockId = "b4";
      b.valueExprId = "e_div_1";

      {
        var e = new ExprDiv();
        eye.expressions.put("e_div_1", e);
        e.upExprId   = "ec1";
        e.downExprId = "br1";
      }
      {
        var e = new ExprConst();
        eye.expressions.put("ec1-in", e);
        e.def = "1234.324";
      }
      {
        var e = new ExprCall();
        eye.expressions.put("ec1", e);
        e.actId = "act001";
        e.leftExprId = "ec1-in";
        e.argExprIds.put("a1", CallArg.withExprId("q1"));
        e.argExprIds.put("a2", CallArg.withExprId("q2"));
        e.argExprIds.put("a3", CallArg.withExprId("q3"));
        {
          var q = new ExprConst();
          eye.expressions.put("q1", q);
          q.def = "5646";
        }
        {
          var q = new ExprConst();
          eye.expressions.put("q2", q);
          q.def = "27";
        }
        {
          var q = new ExprConst();
          eye.expressions.put("q3", q);
          q.def = "71.4";
        }
      }
      {
        var e = new ExprOp();
        eye.expressions.put("op1", e);
        e.firstExprId = "ec2";
        e.useFirst = true;
        e.arg("a1", Op.PLUS, "br2");
        e.arg("a2", Op.MUL, "ec4");
      }
      {
        var e = new ExprBracket();
        eye.expressions.put("br1", e);
        e.exprId = "op1";
        e.kind   = BracketKind.ROUND;
      }
      {
        var e = new ExprConst();
        eye.expressions.put("c7", e);
        e.def = "7";
      }
      {
        var e = new ExprConst();
        eye.expressions.put("ec2", e);
        e.def = "111";
      }
      {
        {
          var e = new ExprOp();
          eye.expressions.put("op2", e);
          e.firstExprId = "ec3";
          e.useFirst    = true;
          e.arg("a1", Op.PLUS, "ec3_1");
        }
        {
          var e = new ExprConst();
          eye.expressions.put("ec3", e);
          e.def = "222";
        }
        {
          var e = new ExprConst();
          eye.expressions.put("ec3_1", e);
          e.def = "777";
        }
      }
      {
        var e = new ExprBracket();
        eye.expressions.put("br2", e);
        e.exprId = "op2";
        e.kind   = BracketKind.ROUND;
      }
      {
        var e = new ExprConst();
        eye.expressions.put("ec4", e);
        e.def = "333";
      }
    }
    {
      var b = new BlockNewVar();
      eye.blocks.put("b4", b);
      b.varName     = "Начинается новый день";
      b.valueExprId = "nd_op1";
      b.downBlockId = "b5";
      {
        var e = new ExprOp();
        eye.expressions.put("nd_op1", e);
        e.firstExprId = "nd_op1_c1";
        e.useFirst    = true;
        e.arg("a1", Op.MINUS, "nd_op1_c2");
        e.arg("a2", Op.PLUS, "nd_op1_c3");
        e.arg("a3", Op.MUL, "nd_op1_c4");
        e.arg("a4", Op.PLUS, "nd_op1_c5");
        {
          var c = new ExprConst();
          eye.expressions.put("nd_op1_c1", c);
          c.def = "11";
        }
        {
          var c = new ExprConst();
          eye.expressions.put("nd_op1_c2", c);
          c.def = "22";
        }
        {
          var c = new ExprConst();
          eye.expressions.put("nd_op1_c3", c);
          c.def = "33";
        }
        {
          var c = new ExprConst();
          eye.expressions.put("nd_op1_c4", c);
          c.def = "44";
        }
        {
          var c = new ExprConst();
          eye.expressions.put("nd_op1_c5", c);
          c.def = "55";
        }
      }
    }
    {
      var b = new BlockExit();
      eye.blocks.put("b5", b);
      b.kind = ExitKind.CIRCLE_CONTINUE;
    }

    return eye;
  }
}
