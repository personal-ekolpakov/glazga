package kz.pompei.glazga.core_debug.launchers;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.nio.file.Paths;
import javax.swing.JFrame;
import kz.pompei.glazga.utils.SizeLocationSaver;

public class ShowKeyCodes {
  public static void main(String[] args) {
    new ShowKeyCodes().run();
  }

  private void run() {
    JFrame frame = new JFrame();

    frame.setTitle("Показывает коды клавиш");
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.setSize(300, 200);

    SizeLocationSaver.into(Paths.get("build").resolve("sizes_and_locations"))
                     .overJFrame(getClass().getSimpleName(), frame, true);

    frame.addKeyListener(new KeyAdapter() {
      @Override
      public void keyReleased(KeyEvent e) {
        System.out.println("tCkxP893GD :: key code = " + e.getKeyCode());
      }
    });

    frame.setVisible(true);
  }
}
