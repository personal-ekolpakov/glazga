package kz.pompei.glazga.llvm;

import java.nio.file.Files;
import java.util.Map;
import kz.pompei.glazga.llvm.cpp.CppFunc;
import lombok.SneakyThrows;

public class NativeLlvmProbe extends LlvmParentTest {
  public static void main(String[] args) {

    NativeLlvmProbe probe = new NativeLlvmProbe();
    probe.prepareLlvmSrcDir(null);

    try {
      probe.exec();
    } finally {
      probe.closeLibrary();
    }


  }

  @SneakyThrows
  private void exec() {

    Files.writeString(llvmSrc.resolve("hello.ll"), """
      %mass = type [26 x i8];
      %Buffer = type [256 x i8];
      
      @process_pattern = private constant %mass c"PROCESSES ПРИВЕТ %s\\00", align 1
      @output_buffer = global %Buffer zeroinitializer, align 1

      define i8* @processStr(i8* %input) {
      entry:

        %buffer = getelementptr inbounds %Buffer, %Buffer* @output_buffer, i64 0, i64 0

        %format = getelementptr inbounds %mass, %mass* @process_pattern, i32 0, i32 0

        call i32 (i8*, i8*, ...) @sprintf(i8* %buffer, i8* %format, i8* %input)

        ret i8* %buffer

      }

      declare i32 @sprintf(i8*, i8*, ...)
      """);
    Files.writeString(llvmSrc.resolve("Makefile"), """
      hello.so: hello.o
      \tgcc -shared  -o hello.so hello.o
      
      
      hello.o: hello.ll
      \tllc -relocation-model=pic -filetype=obj hello.ll -o hello.o
      
      """);

    runCmd(llvmSrc, "make");

    System.out.println("tuUq1eiGiM :: Make OK");

    cpp.openLibrary(llvmSrc.resolve("hello.so"), Map.of(CppFunc.constCharRef_charRef, "processStr"));

    String result = cpp.call__constCharRef_constCharRef("Test Value");

    System.out.println("9SDniWbiHL :: result = " + result);
  }
}
