package kz.pompei.glazga.llvm.gen.module_decl;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.llvm.gen.LLModule;
import kz.pompei.glazga.llvm.gen.meta.CallConvention;
import kz.pompei.glazga.llvm.gen.meta.DLLStorageClass;
import kz.pompei.glazga.llvm.gen.meta.FloatType;
import kz.pompei.glazga.llvm.gen.meta.Linkage;
import kz.pompei.glazga.llvm.gen.meta.PreemptionSpecifier;
import kz.pompei.glazga.llvm.gen.meta.UnNamed;
import kz.pompei.glazga.llvm.gen.meta.VisibilityStyle;
import kz.pompei.glazga.llvm.gen.module_decl.func.FuncBlock;
import kz.pompei.glazga.llvm.gen.module_decl.func.instructions.GetElementPtr;
import kz.pompei.glazga.llvm.gen.types.TArray;
import kz.pompei.glazga.llvm.gen.types.TFloat;
import kz.pompei.glazga.llvm.gen.types.TInt;
import kz.pompei.glazga.llvm.gen.types.TPtr;
import kz.pompei.glazga.llvm.gen.types.TStruct;
import kz.pompei.glazga.llvm.gen.types.TTypeRef;
import kz.pompei.glazga.llvm.gen.types.values.IntTypeValue;
import kz.pompei.glazga.llvm.gen.types.values.PtrValue;
import kz.pompei.glazga.llvm.gen.types.values.TypeValue;
import kz.pompei.glazga.llvm.gen.types.values.ValueConst;
import kz.pompei.glazga.llvm.gen.types.values.ValueRef;
import org.testng.annotations.Test;

import static kz.pompei.glazga.llvm.gen.meta.MemoryAccessType.Read;
import static kz.pompei.glazga.llvm.gen.meta.MemoryAccessType.ReadWrite;
import static kz.pompei.glazga.utils.NumUtil.toLen;
import static org.assertj.core.api.Assertions.assertThat;

public class DefineFuncTest {

  @Test
  public void simple_1() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.returnType(TInt.len(64))
          .funcIndex(0)
          .call(CallConvention.CfGuard_CheckCC)
          .addressSpace(5);

    method.countIndexes();

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo("define \"CfGuard_CheckCC\" i64 @1() addrSpace(5)".toLowerCase());
    assertThat(lines.get(2)).isEqualTo("{");
    assertThat(lines.get(3)).isEqualTo("}");
  }

  @Test
  public void functionAttributes() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("hello_world")
          .returnType(TInt.len(64))
          .call(CallConvention.CfGuard_CheckCC)
          .addressSpace(4)
          .attrs().builtIn(true)
          .attrs().convergent(true)
          .attrs().allocSize(4, 3)
          .sectionName("table");

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo(("define \"cfGuard_checkCC\" i64 @hello_world() addrSpace(4) allocSize(4, 3)" +
                                        " builtin convergent, section \"TABLE\"").toLowerCase());
  }

  @Test
  public void functionAttributes__memory_01() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("sinus")
          .returnType(TInt.len(64))
          .attrs().memory().inAccessibleMem(Read)
          .attrs().memory().argMem(ReadWrite)
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo(("define i64 @sinus() memory(argMem: readWrite, inAccessibleMem: read)").toLowerCase());
  }

  @Test
  public void functionAttributes__memory_02() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("sinus")
          .returnType(TInt.len(64))
          .attrs().memory().inAccessibleMem(Read)
          .attrs().memory().any(ReadWrite)
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo(("define i64 @sinus() memory(ReadWrite, inAccessibleMem: read)").toLowerCase());
  }

  @Test
  public void functionAttributes__memory_03() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("sinus")
          .returnType(TInt.len(64))
          .attrs().memory().any(ReadWrite)
          .attrs().memory().argMem(ReadWrite)
          .attrs().memory().inAccessibleMem(Read)
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1))
      .isEqualTo("define i64 @sinus() memory(ReadWrite, argMem: ReadWrite, InAccessibleMem: Read)".toLowerCase());
  }

  @Test
  public void functionAttributes__memory_04() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("sinus")
          .returnType(TInt.len(64))
          .attrs().memory().any(ReadWrite)
          .attrs().memory().argMem(ReadWrite)
          .attrs().memory().inAccessibleMem(Read)
          .attrs().memory().on(false)
          .linkage(Linkage.Available_Externally)
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1))
      .isEqualTo("define available_externally i64 @sinus()".toLowerCase());
  }

  @Test
  public void functionAttributes__gc() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("sinus")
          .returnType(TInt.len(64))
          .gc("stone")
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo(("define i64 @sinus() gc \"stone\"").toLowerCase());
  }

  @Test
  public void functionAttributes__returnTypeAttrs() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("sinus")
          .returnType(TInt.len(64))
          .returnTypeAttrs().zeroExt(true).signExt(true).up()
          .linkage(Linkage.Common)
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo(("define common i64 zeroExt signExt @sinus()").toLowerCase());
  }

  @Test
  public void functionParams_01() {

    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("sinus")
          .returnType(TInt.len(64))
          .linkage(Linkage.External)
          .newArg(p -> p.type(TInt.len(16)).name("arg1").attrs().zeroExt(true))
          .newArg(p -> p.type(TInt.len(16)).name("arg2").attrs().signExt(true))
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo(("define external i64 @sinus(i16 zeroExt %arg1, i16 signExt %arg2)").toLowerCase());

  }

  @Test
  public void functionParams_02() {

    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("sinus")
          .returnType(TInt.len(64))
          .linkage(Linkage.External)
          .newArg(p -> p.type(TInt.len(16)).name("arg1").attrs().byVal(TInt.len(8)).zeroExt(false))
          .newArg(p -> p.type(TInt.len(32)).name("arg2").attrs().byRef(TInt.len(64)).signExt(true))
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo(
      ("define external i64 @sinus(i16 byVal(i8) %arg1, i32 signExt byRef(i64) %arg2)").toLowerCase());

  }

  @Test
  public void functionParams_03() {

    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("sinus")
          .returnType(TInt.len(64))
          .linkage(Linkage.External)
          .newArg(p -> p.type(TInt.len(16)).index(0).attrs().byVal(TInt.len(8)).zeroExt(false))
          .newArg(p -> p.type(TInt.len(32)).index(0).attrs().byRef(TInt.len(64)).signExt(true))
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo(("define external i64 @sinus(i16 byVal(i8) %1, i32 signExt byRef(i64) %2)").toLowerCase());

  }

  @Test
  public void functionParams_04() {

    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.funcName("sinus")
          .returnType(TInt.len(64))
          .linkage(Linkage.External)
          .newArg(p -> p.type(TInt.len(16)).attrs().byVal(TInt.len(8)).zeroExt(false))
          .newArg(p -> p.type(TInt.len(32)).attrs().byRef(TInt.len(64)).signExt(true))
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo(("define external i64 @sinus(i16 byVal(i8), i32 signExt byRef(i64))").toLowerCase());

  }


  @Test
  public void simple_2() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.returnType(TInt.len(64))
          .funcIndex(0)
          .call(CallConvention.CfGuard_CheckCC)
          .sectionName("sinus")
          .partitionName("sun")
          .comDat(new ComDat())
          .align(4)
          .gc("main")
          .unNamed(UnNamed.Unnamed_Addr)
    ;

    method.countIndexes();


    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo(("define \"cfGuard_checkCC\" i64 @1() unnamed_addr, section \"sinus\"," +
                                        " partition \"sun\" comDat align 4 gc \"main\"").toLowerCase());
  }

  @Test
  public void simple_3() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.returnType(TInt.len(64))
          .unNamed(UnNamed.Unnamed_Addr)
          .preemptionSpecifier(PreemptionSpecifier.DSO_Local)
          .visibility(VisibilityStyle.Hidden)
          .dllStorageClass(DLLStorageClass.DLLExport)
          .call(CallConvention.CC)
          .funcName("PowerPoint")
    ;


    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo("define dso_local hidden dllexport \"cc\" i64 @PowerPoint() unnamed_addr");
  }

  @Test
  public void simple_4() {
    LLModule   module = new LLModule();
    DefineFunc method = module.define();

    method.returnType(TInt.len(64))
          .funcName("PowerPoint")
          .newArg(arg -> arg.type(TInt.len(64)))
    ;


    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(1)).isEqualTo("define i64 @PowerPoint(i64)");
  }


  @Test
  public void funcWithGetElementPtr_01() {

    LLModule module = new LLModule();


    SttTypeDef StructRT = module.newTypeDef()
                                .varName("struct.RT")
                                .type(TStruct.of(
                                  TInt.len(8),
                                  TArray.of(10, TArray.of(20, TInt.len(32))),
                                  TInt.len(8)
                                ));

    SttTypeDef StructST = module.newTypeDef()
                                .varName("struct.ST")
                                .type(TStruct.of(
                                  TInt.len(32),
                                  TFloat.of(FloatType.Double),
                                  TTypeRef.of(StructRT)
                                ));


    DefineFunc func = module.define()
                            .funcName("foo")
                            .returnType(TPtr.of())
                            .newArg(arg -> arg.name("s").type(TPtr.of()));

    FuncBlock entry = func.newBlock().name("entry");

    GetElementPtr arrayIdx = entry.newGetElementPtr()
                                  .varName("arrayIdx")
                                  .inbounds(true)
                                  .type(TTypeRef.of(StructST))
                                  .ptrValue(PtrValue.of(TPtr.of(), func.argRef("s")))
                                  .index(IntTypeValue.of(TInt.len(64), ValueConst.of("0")))
                                  .index(IntTypeValue.of(TInt.len(32), ValueConst.of("2")))
                                  .index(IntTypeValue.of(TInt.len(32), ValueConst.of("1")))
                                  .index(IntTypeValue.of(TInt.len(64), ValueConst.of("5")))
                                  .index(IntTypeValue.of(TInt.len(64), ValueConst.of("13")));

    entry.newRet().ret(TypeValue.of(TPtr.of(), ValueRef.of(arrayIdx)));

    module.countIndexes();

    List<String> lines = new ArrayList<>();
    module.print(lines::add);

    int LL = ("" + (lines.size() - 1)).length();
    int i  = 0;
    for (final String line : lines) {
      System.out.println("t1SheHnE6s :: " + toLen(i++, LL) + " " + line);
    }

    assertThat(lines.get(0)).isEqualTo("%struct.RT = type { i8, [10 x [20 x i32]], i8 }");
    assertThat(lines.get(1)).isEqualTo("%struct.ST = type { i32, double, %struct.RT }");

    assertThat(lines.get(3)).isEqualTo("define ptr @foo(ptr %s)");
    assertThat(lines.get(4)).isEqualTo("{");
    assertThat(lines.get(5)).isEqualTo("  entry:");
    assertThat(lines.get(6)).isEqualTo("    %arrayIdx = getelementptr inbounds %struct.ST, ptr %s," +
                                       " i64 0, i32 2, i32 1, i64 5, i64 13");
    assertThat(lines.get(7)).isEqualTo("    ret ptr %arrayIdx");
    assertThat(lines.get(8)).isEqualTo("}");
  }

  @Test
  public void funcWithGetElementPtr_02() {
    LLModule module = new LLModule();


    SttTypeDef StructRT = module.newTypeDef()
                                .varName("struct.RT")
                                .type(TStruct.of(
                                  TInt.len(8),
                                  TArray.of(10, TArray.of(20, TInt.len(32))),
                                  TInt.len(8)
                                ));

    SttTypeDef StructST = module.newTypeDef()
                                .varName("struct.ST")
                                .type(TStruct.of(
                                  TInt.len(32),
                                  TFloat.of(FloatType.Double),
                                  TTypeRef.of(StructRT)
                                ));


    DefineFunc func = module.define()
                            .funcName("foo")
                            .returnType(TPtr.of())
                            .newArg(arg -> arg.name("s").type(TPtr.of()));

    FuncBlock entry = func.newBlock().name("entry");

    GetElementPtr t1 = entry.newGetElementPtr()
                            .varIndex(0)
                            .type(TTypeRef.of(StructST))
                            .ptrValue(PtrValue.of(TPtr.of(), func.argRef("s")))
                            .index(IntTypeValue.of(TInt.len(32), ValueConst.of("0")))
                            .index(IntTypeValue.of(TInt.len(32), ValueConst.of("2")));

    GetElementPtr t2 = entry.newGetElementPtr()
                            .varIndex(0)
                            .type(TTypeRef.of(StructRT))
                            .ptrValue(PtrValue.of(TPtr.of(), ValueRef.of(t1)))
                            .index(IntTypeValue.of(TInt.len(32), ValueConst.of("0")))
                            .index(IntTypeValue.of(TInt.len(32), ValueConst.of("1")));


    GetElementPtr t3 = entry.newGetElementPtr()
                            .varIndex(0)
                            .type(TArray.of(10, TArray.of(20, TInt.len(32))))
                            .ptrValue(PtrValue.of(TPtr.of(), ValueRef.of(t2)))
                            .index(IntTypeValue.of(TInt.len(32), ValueConst.of("0")))
                            .index(IntTypeValue.of(TInt.len(32), ValueConst.of("5")));


    GetElementPtr t4 = entry.newGetElementPtr()
                            .varIndex(0)
                            .type(TArray.of(20, TInt.len(32)))
                            .ptrValue(PtrValue.of(TPtr.of(), ValueRef.of(t3)))
                            .index(IntTypeValue.of(TInt.len(32), ValueConst.of("0")))
                            .index(IntTypeValue.of(TInt.len(32), ValueConst.of("13")));


    entry.newRet().ret(TypeValue.of(TPtr.of(), ValueRef.of(t4)));

    module.countIndexes();

    List<String> lines = new ArrayList<>();
    module.print(lines::add);

    int LL = ("" + (lines.size() - 1)).length();
    int i  = 0;
    for (final String line : lines) {
      System.out.println("t1SheHnE6s :: " + toLen(i++, LL) + " " + line);
    }

    assertThat(lines.get(0)).isEqualTo("%struct.RT = type { i8, [10 x [20 x i32]], i8 }");
    assertThat(lines.get(1)).isEqualTo("%struct.ST = type { i32, double, %struct.RT }");

    //@formatter:off
    int b = 3;
    assertThat(lines.get(b + 0)).isEqualTo("define ptr @foo(ptr %s)");
    assertThat(lines.get(b + 1)).isEqualTo("{");
    assertThat(lines.get(b + 2)).isEqualTo("  entry:");
    assertThat(lines.get(b + 3)).isEqualTo("    %1 = getelementptr %struct.ST, ptr %s, i32 0, i32 2");
    assertThat(lines.get(b + 4)).isEqualTo("    %2 = getelementptr %struct.RT, ptr %1, i32 0, i32 1");
    assertThat(lines.get(b + 5)).isEqualTo("    %3 = getelementptr [10 x [20 x i32]], ptr %2, i32 0, i32 5");
    assertThat(lines.get(b + 6)).isEqualTo("    %4 = getelementptr [20 x i32], ptr %3, i32 0, i32 13");
    assertThat(lines.get(b + 7)).isEqualTo("    ret ptr %4");
    assertThat(lines.get(b + 8)).isEqualTo("}");
    //@formatter:on
  }

}
