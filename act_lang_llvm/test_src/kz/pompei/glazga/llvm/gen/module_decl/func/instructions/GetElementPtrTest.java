package kz.pompei.glazga.llvm.gen.module_decl.func.instructions;

import kz.pompei.glazga.llvm.gen.LLModule;
import kz.pompei.glazga.llvm.gen.module_decl.DefineFunc;
import kz.pompei.glazga.llvm.gen.module_decl.func.FuncBlock;
import kz.pompei.glazga.llvm.gen.types.TInt;
import kz.pompei.glazga.llvm.gen.types.TPtr;
import kz.pompei.glazga.llvm.gen.types.values.IntTypeValue;
import kz.pompei.glazga.llvm.gen.types.values.PtrValue;
import kz.pompei.glazga.llvm.gen.types.values.ValueConst;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GetElementPtrTest {

  @Test
  public void print() {

    LLModule module = new LLModule();

    DefineFunc func = module.define();

    FuncBlock entry = func.newBlock().name("entry");

    GetElementPtr instr = entry.newGetElementPtr()
                               .varIndex(0)
                               .inbounds(true)
                               .type(TInt.len(32))
                               .ptrValue(PtrValue.of(TPtr.of(), ValueConst.of("113")))
                               .index(IntTypeValue.of(TInt.len(64), ValueConst.of("0")))
                               .index(IntTypeValue.of(TInt.len(64), ValueConst.of("1")));

    instr.countIndexes();

    assertThat(instr.print()).isEqualTo("    %1 = getelementptr inbounds i32, ptr 113, i64 0, i64 1");

  }
}
