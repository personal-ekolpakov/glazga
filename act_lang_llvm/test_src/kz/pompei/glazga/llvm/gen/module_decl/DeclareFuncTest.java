package kz.pompei.glazga.llvm.gen.module_decl;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.llvm.gen.LLModule;
import kz.pompei.glazga.llvm.gen.meta.CallConvention;
import kz.pompei.glazga.llvm.gen.meta.DLLStorageClass;
import kz.pompei.glazga.llvm.gen.meta.Linkage;
import kz.pompei.glazga.llvm.gen.meta.UnNamed;
import kz.pompei.glazga.llvm.gen.meta.VisibilityStyle;
import kz.pompei.glazga.llvm.gen.types.TInt;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DeclareFuncTest {

  @Test
  public void simple() {
    LLModule    module = new LLModule();
    DeclareFunc method = module.declare();

    method.returnType(TInt.len(64))
          .call(CallConvention.CfGuard_CheckCC)
          .funcName("status")
          .align(4)
          .gc("round")
          .unNamed(UnNamed.Local_Unnamed_Addr)
          .returnTypeAttrs().byRef(TInt.len(8)).up()
          .visibility(VisibilityStyle.Protected)
          .dllStorageClass(DLLStorageClass.DLLImport)
          .linkage(Linkage.Common)
    ;


    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(0)).isEqualTo(("declare common protected dllimport \"cfGuard_checkCC\" i64 byRef(i8)" +
                                        " @status() local_unnamed_addr align 4 gc \"round\"").toLowerCase());
    assertThat(lines).hasSize(1);
  }

  @Test
  public void args_01() {

    LLModule    module = new LLModule();
    DeclareFunc method = module.declare();

    method.returnType(TInt.len(64))
          .funcName("status")
          .linkage(Linkage.Common)
          .newArg(a -> a.name("input").type(TInt.len(32)))
          .newArg(a -> a.name("reload").type(TInt.len(32)))
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(0)).isEqualTo(("declare common i64 @status(i32 %input, i32 %reload)").toLowerCase());
    assertThat(lines).hasSize(1);
  }

  @Test
  public void args_02() {

    LLModule    module = new LLModule();
    DeclareFunc method = module.declare();

    method.returnType(TInt.len(64))
          .funcName("status")
          .linkage(Linkage.Common)
          .newArg(a -> a.index(0).type(TInt.len(32)))
          .newArg(a -> a.index(0).type(TInt.len(32)))
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(0)).isEqualTo(("declare common i64 @status(i32 %1, i32 %2)").toLowerCase());
    assertThat(lines).hasSize(1);
  }

  @Test
  public void args_03() {

    LLModule    module = new LLModule();
    DeclareFunc method = module.declare();

    method.returnType(TInt.len(64))
          .funcName("status")
          .linkage(Linkage.Common)
          .newArg(a -> a.type(TInt.len(32)))
          .newArg(a -> a.type(TInt.len(32)))
    ;

    List<String> lines = new ArrayList<>();
    method.print(lines::add);

    assertThat(lines.get(0)).isEqualTo(("declare common i64 @status(i32, i32)").toLowerCase());
    assertThat(lines).hasSize(1);
  }
}
