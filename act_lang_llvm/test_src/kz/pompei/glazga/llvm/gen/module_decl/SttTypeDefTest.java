package kz.pompei.glazga.llvm.gen.module_decl;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.llvm.gen.LLModule;
import kz.pompei.glazga.llvm.gen.meta.FloatType;
import kz.pompei.glazga.llvm.gen.types.TArray;
import kz.pompei.glazga.llvm.gen.types.TFloat;
import kz.pompei.glazga.llvm.gen.types.TInt;
import kz.pompei.glazga.llvm.gen.types.TStruct;
import kz.pompei.glazga.llvm.gen.types.TTypeRef;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SttTypeDefTest {

  @Test
  public void print() {

    LLModule module = new LLModule();

    SttTypeDef typeStructRT = module.newTypeDef()
                                    .varName("struct.RT")
                                    .type(TStruct.of(
                                      TInt.len(8),
                                      TArray.of(10, TArray.of(20, TInt.len(32))),
                                      TInt.len(8)
                                    ));

    SttTypeDef structST = module.newTypeDef()
                                .varName("struct.ST")
                                .type(TStruct.of(
                                  TInt.len(32),
                                  TFloat.of(FloatType.Double),
                                  TTypeRef.of(typeStructRT)
                                ));


    module.countIndexes();

    List<String> lines = new ArrayList<>();

    typeStructRT.print(lines::add);
    structST.print(lines::add);

    for (final String line : lines) {
      System.out.println("yx9ZZEplQR :: " + line);
    }

    assertThat(lines.get(0)).isEqualTo("%struct.RT = type { i8, [10 x [20 x i32]], i8 }");
    assertThat(lines.get(1)).isEqualTo("%struct.ST = type { i32, double, %struct.RT }");
  }
}
