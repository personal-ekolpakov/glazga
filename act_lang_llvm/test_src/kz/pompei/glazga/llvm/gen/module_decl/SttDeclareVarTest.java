package kz.pompei.glazga.llvm.gen.module_decl;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.llvm.gen.LLModule;
import kz.pompei.glazga.llvm.gen.meta.DLLStorageClass;
import kz.pompei.glazga.llvm.gen.meta.Linkage;
import kz.pompei.glazga.llvm.gen.meta.PreemptionSpecifier;
import kz.pompei.glazga.llvm.gen.meta.UnNamed;
import kz.pompei.glazga.llvm.gen.meta.VisibilityStyle;
import kz.pompei.glazga.llvm.gen.types.TInt;
import org.testng.annotations.Test;

import static kz.pompei.glazga.llvm.gen.meta.ThreadLocalEnum.InitialExec;
import static org.assertj.core.api.Assertions.assertThat;

public class SttDeclareVarTest {

  @Test
  public void print__01() {

    LLModule module = new LLModule();

    SttDeclareVar gvd = module.newConstant()
                              .varName("G")
                              .addressSpace(5)
                              .type(TInt.len(32))
                              .initializerConstant("10")
                              .sectionName("foo")
                              .align(4);

    List<String> list = new ArrayList<>();
    gvd.print(list::add);


    assertThat(list.get(0)).isEqualTo("@G = addr" + "space(5) constant i32 10, section \"foo\", align 4");

    assertThat(list).hasSize(1);
  }

  @Test
  public void print__02() {

    LLModule module = new LLModule();

    SttDeclareVar gvd = module.newConstant()
                              .type(TInt.len(32))
                              .initializerConstant("103")
                              .threadLocal(InitialExec)
                              .varName("status")
                              .align(4);

    List<String> list = new ArrayList<>();
    gvd.print(list::add);


    assertThat(list.get(0)).isEqualTo("@status = thread_local(initialExec) constant i32 103, align 4".toLowerCase());

    assertThat(list).hasSize(1);
  }

  @Test
  public void print__03() {

    LLModule module = new LLModule();

    SttDeclareVar gvd = module.newGlobal()
                              .type(TInt.len(64))
                              .varName("status")
                              .linkage(Linkage.External);

    List<String> list = new ArrayList<>();
    gvd.print(list::add);


    assertThat(list.get(0)).isEqualTo("@status = external global i64".toLowerCase());

    assertThat(list).hasSize(1);
  }

  @Test
  public void print__04() {

    LLModule module = new LLModule();

    SttDeclareVar gvd = module.newGlobal()
                              .type(TInt.len(64))
                              .varName("status")
                              .visibility(VisibilityStyle.Protected)
                              .preemptionSpecifier(PreemptionSpecifier.DSO_Local)
                              .unNamed(UnNamed.Local_Unnamed_Addr)
                              .linkage(Linkage.External)
                              .partitionName("hello-world")
                              .comDat(new ComDat())
                              .code_model("test")
                              .no_sanitize_address(true)
                              .no_sanitize_hwAddress(true)
                              .sanitize_address_dynInit(true)
                              .sanitize_memTag(true)
                              .dllStorageClass(DLLStorageClass.DLLImport)
                              .meta("a", "address")
                              .meta("base", "sinus");

    List<String> list = new ArrayList<>();
    gvd.print(list::add);

    assertThat(list.get(0)).isEqualTo(("@status = external dso_local protected dllimport local_unnamed_addr global i64," +
                                       " partition \"hello-world\", comDat, code_model \"test\", no_sanitize_address," +
                                       " no_sanitize_hwAddress, sanitize_address_dynInit, sanitize_memTag," +
                                       " !a !address, !base !sinus").toLowerCase());
    assertThat(list).hasSize(1);
  }

  @Test
  public void print__05() {
    LLModule module = new LLModule();

    SttDeclareVar gvd = module.newConstant()
                              .varName("str")
                              .linkage(Linkage.Private)
                              .align(1)
                              .arrayWithText("Привет мир - \"полёт\" навигатора\n\0");

    List<String> list = new ArrayList<>();
    gvd.print(list::add);

    assertThat(list.get(0)).isEqualTo("@str = private constant [57 x i8] \"Привет мир - \\\"полёт\\\" навигатора\\0A\\00\", align 1");

    assertThat(list).hasSize(1);
  }

  @Test
  public void print__06() {
    LLModule module = new LLModule();

    SttDeclareVar gvd = module.newConstant()
                              .varName("str")
                              .linkage(Linkage.Private)
                              .align(1)
                              .arrayWithText("HelloWorld\n\0");

    List<String> list = new ArrayList<>();
    gvd.print(list::add);

    assertThat(list.get(0)).isEqualTo("@str = private constant [12 x i8] \"HelloWorld\\0A\\00\", align 1");

    assertThat(list).hasSize(1);
  }

}
