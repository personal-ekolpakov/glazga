package kz.pompei.glazga.llvm.gen.types;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TFuncTest {

  @Test
  public void print_01() {

    TFunc type = TFunc.of(TInt.len(32)).add(TInt.len(32));

    assertThat(type.print()).isEqualTo("i32 (i32)");

  }

  @Test
  public void print_02() {

    TFunc type = TFunc.of(TInt.len(32)).add(TInt.len(32)).add(TInt.len(64));

    assertThat(type.print()).isEqualTo("i32 (i32, i64)");

  }

  @Test
  public void print_03() {

    TFunc type = TFunc.of(TInt.len(32)).etc(true).add(TInt.len(32)).add(TInt.len(64));

    assertThat(type.print()).isEqualTo("i32 (i32, i64, ...)");

  }
}
