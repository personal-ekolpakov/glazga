package kz.pompei.glazga.llvm.gen.types;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TStructTest {

  @Test
  public void print_01() {

    TStruct struct = TStruct.of(TInt.len(32), TInt.len(32), TInt.len(64));

    assertThat(struct.print()).isEqualTo("{ i32, i32, i64 }");

  }

  @Test
  public void print_02() {

    TStruct struct = TStruct.of(TInt.len(32), TInt.len(32)).field(TInt.len(64)).packed(true);

    assertThat(struct.print()).isEqualTo("<{ i32, i32, i64 }>");

  }
}
