package kz.pompei.glazga.llvm.gen.types;

import kz.pompei.glazga.llvm.gen.meta.FloatType;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TVectorTest {

  @Test
  public void print_01() {

    TVector vector = TVector.of(8, TFloat.of(FloatType.Float));

    assertThat(vector.print()).isEqualTo("<8 x float>");

  }

  @Test
  public void print_02() {

    TVector vector = TVector.of(8, TFloat.of(FloatType.Float)).scale(true);

    assertThat(vector.print()).isEqualTo("<vScale x 8 x float>".toLowerCase());

  }
}
