package kz.pompei.glazga.llvm.gen.types;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TVoidTest {

  @Test
  public void print() {
    assertThat(TVoid.of().print()).isEqualTo("void");
  }

}
