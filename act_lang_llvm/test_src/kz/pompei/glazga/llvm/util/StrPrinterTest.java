package kz.pompei.glazga.llvm.util;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StrPrinterTest {

  @Test
  public void printStr() {

    String input = "Begin \"Of Status\"\nThe END.\0";

    //
    //
    String actual = StrPrinter.printStr(input);
    //
    //

    assertThat(actual).isEqualTo("\"Begin \\\"Of Status\\\"\\0AThe END.\\00\"");

  }


  @Test
  public void toHex_01() {
    assertThat(StrPrinter.toHex(12, 2)).isEqualTo("0C");
  }
}
