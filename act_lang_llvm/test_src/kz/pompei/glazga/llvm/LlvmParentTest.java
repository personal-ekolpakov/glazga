package kz.pompei.glazga.llvm;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import kz.pompei.glazga.llvm.cpp.CppModule;
import kz.pompei.glazga.utils.StreamUtils;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public abstract class LlvmParentTest {

  protected Path llvmSrc;

  protected final CppModule cpp = new CppModule();

  @BeforeMethod
  public void prepareLlvmSrcDir(Method testMethod) {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");

    llvmSrc = Paths.get("build")
                   .resolve("aaa-tst-" + sdf.format(new Date()))
                   .resolve(getClass().getSimpleName() + (testMethod == null ? "" : "_" + testMethod.getName()))
                   .resolve("llvm-src");

    llvmSrc.toFile().mkdirs();
  }

  @SuppressWarnings("SameParameterValue")
  @SneakyThrows
  protected void runCmd(@NonNull Path workingDir, String command) {
    Process process = new ProcessBuilder(command)
      .directory(workingDir.toFile())
      .redirectErrorStream(true)
      .start();

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    try (InputStream inputStream = process.getInputStream()) {
      StreamUtils.copyStream(inputStream, outputStream);
    }


    int exitCode = process.waitFor();

    if (outputStream.size() > 0) {
      System.out.println(outputStream.toString(StandardCharsets.UTF_8));
    }


    if (exitCode == 0) {
      return;
    }

    throw new RuntimeException("OOmUUb8mig :: Error in cmd: `" + command + "` with exit code: " + exitCode);
  }

  @AfterMethod
  public void closeLibrary() {
    System.out.println("7SFhh1Ya1w :: before close CppLibrary");
    cpp.closeLibrary();
  }
}
