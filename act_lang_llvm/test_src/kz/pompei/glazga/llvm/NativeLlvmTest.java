package kz.pompei.glazga.llvm;

import java.nio.file.Files;
import java.util.Map;
import kz.pompei.glazga.llvm.cpp.CppFunc;
import kz.pompei.glazga.llvm.cpp.InnerError;
import org.testng.annotations.Test;

public class NativeLlvmTest extends LlvmParentTest {

  @Test
  public void stringConversion() throws Exception {

    Files.writeString(llvmSrc.resolve("hello.ll"), """
      @process_pattern = private constant [13 x i8] c"Processes %s\\00", align 1
      @output_buffer = global [256 x i8] zeroinitializer, align 1

      define i8* @processStr(i8* %input) {
      entry:

        %buffer = getelementptr inbounds [256 x i8], [256 x i8]* @output_buffer, i64 0, i64 0

        %format = getelementptr inbounds [13 x i8], [13 x i8]* @process_pattern, i32 0, i32 0

        call i32 (i8*, i8*, ...) @sprintf(i8* %buffer, i8* %format, i8* %input)

        ret i8* %buffer

      }

      declare i32 @sprintf(i8*, i8*, ...)
      """);
    Files.writeString(llvmSrc.resolve("Makefile"), """
      hello.so: hello.o
      \tgcc -shared  -o hello.so hello.o
            
            
      hello.o: hello.ll
      \tllc -relocation-model=pic -filetype=obj hello.ll -o hello.o
            
      """);

    runCmd(llvmSrc, "make");

    cpp.openLibrary(llvmSrc.resolve("hello.so"), Map.of(CppFunc.constCharRef_charRef, "processStr"));

    String result = cpp.call__constCharRef_constCharRef("Test Value");

    System.out.println("9SDniWbiHL :: result = " + result);
  }

  @Test(expectedExceptions = InnerError.class, expectedExceptionsMessageRegExp = ".*WOW STATUS.*")
  public void callInnerError() {
    cpp.callInnerError("WOW STATUS");
  }

  @Test
  public void nakedRefs() throws Exception {

    Files.writeString(llvmSrc.resolve("hello.ll"), """
      @process_pattern = private constant [13 x i8] c"Processes %s\\00", align 1
      @output_buffer = global [256 x i8] zeroinitializer, align 1

      define i8* @processStr(i8* %input) {
      entry:

        %buffer = getelementptr inbounds [1 x i8], ptr @output_buffer, i64 0, i64 0

        %format = getelementptr inbounds [1 x i8], ptr @process_pattern, i32 0, i32 0

        call i32 (i8*, i8*, ...) @sprintf(i8* %buffer, i8* %format, i8* %input)

        ret i8* %buffer

      }

      declare i32 @sprintf(i8*, i8*, ...)
      """);
    Files.writeString(llvmSrc.resolve("Makefile"), """
      hello.so: hello.o
      \tgcc -shared  -o hello.so hello.o
            
            
      hello.o: hello.ll
      \tllc -relocation-model=pic -opaque-pointers -filetype=obj hello.ll -o hello.o
            
      """);

    runCmd(llvmSrc, "make");

    cpp.openLibrary(llvmSrc.resolve("hello.so"), Map.of(CppFunc.constCharRef_charRef, "processStr"));

    String result = cpp.call__constCharRef_constCharRef("Test Value");

    System.out.println("9SDniWbiHL :: result = " + result);
  }
}
