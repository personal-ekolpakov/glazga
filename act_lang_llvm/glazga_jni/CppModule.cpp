#include <jni.h>
#include "kz_pompei_glazga_llvm_cpp_CppModule.h"
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <dlfcn.h>
#include <memory>

using namespace std;

struct FreeDeleter {
  void operator()(char *ptr) const {
    free((void *) ptr);
  }
};

struct ConstFreeDeleter {
  void operator()(const char *ptr) const {
    free((void *) ptr);
  }
};

typedef std::unique_ptr<char, FreeDeleter> UniqueCharPtr;
typedef std::unique_ptr<const char, ConstFreeDeleter> UniqueConstCharPtr;

static void *library_handle = nullptr;

extern "C" typedef void (*func_void_void_type)();
extern "C" typedef const char *(*func_constCharPtr_constCharPtr_type)(const char *);

static func_void_void_type func_void_void;
static func_constCharPtr_constCharPtr_type func_constCharPtr_constCharPtr;

static void clearFuncPtrs() {
  func_void_void = nullptr;
  func_constCharPtr_constCharPtr = nullptr;
}

JNIEXPORT jstring JNICALL Java_kz_pompei_glazga_llvm_cpp_CppModule_concatStrings
    (JNIEnv *env, jobject javaThis, jstring string1, jstring string2) {

  const char *str1 = env->GetStringUTFChars(string1, nullptr);
  const char *str2 = env->GetStringUTFChars(string2, nullptr);

  char *result = (char *) malloc(strlen(str1) + strlen(str2) + 1); // +1 for the null-terminator
  strcpy(result, str1);
  strcat(result, str2);

  env->ReleaseStringUTFChars(string1, str1);
  env->ReleaseStringUTFChars(string2, str2);

  jstring ret = env->NewStringUTF(result);

  free(result);

  return ret;

}

static void callInnerError(JNIEnv *env, jobject thisObject, const char *errorPrefix, const char *error) {

  jclass thisClass = env->GetObjectClass(thisObject);

  UniqueCharPtr concat{(char *) malloc(strlen(errorPrefix) + strlen(error) + 1)}; // +1 for the null-terminator

  strcpy(concat.get(), errorPrefix);
  strcat(concat.get(), error);

  jstring errorText = env->NewStringUTF(concat.get());

  jmethodID innerErrorID = env->GetMethodID(thisClass, "innerError", "(Ljava/lang/String;)V");

  env->CallVoidMethod(thisObject, innerErrorID, errorText);
}

/*
 * Class:     kz_pompei_glazga_llvm_cpp_CppModule
 * Method:    openLibrary0
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_kz_pompei_glazga_llvm_cpp_CppModule_openLibrary0
    (JNIEnv *env, jobject thisObject, jstring absolutePathToSoFile) {

  UniqueConstCharPtr filePath{env->GetStringUTFChars(absolutePathToSoFile, nullptr)};

  void *handle = dlopen(filePath.get(), RTLD_LAZY);
  if (!handle) {
    callInnerError(env, thisObject, "nCsS4PYQtS :: dlopen", dlerror());
    return;
  }

  library_handle = handle;

  cout << "4rRov7obAc :: library opened " << filePath.get() << endl;

  clearFuncPtrs();
}

/*
 * Class:     kz_pompei_glazga_llvm_cpp_CppModule
 * Method:    closeLibrary
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_kz_pompei_glazga_llvm_cpp_CppModule_closeLibrary
    (JNIEnv *, jobject) {

  if (!library_handle) return;

  dlclose(library_handle);
  clearFuncPtrs();
  library_handle = nullptr;

}

/*
 * Class:     kz_pompei_glazga_llvm_cpp_CppModule
 * Method:    assignFunc_void_void
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_kz_pompei_glazga_llvm_cpp_CppModule_assignFunc_1void_1void
    (JNIEnv *env, jobject thisObject, jstring funcName) {

  UniqueConstCharPtr funcNamePtr{env->GetStringUTFChars(funcName, nullptr)};

  dlerror();// очистить лот ошибки
  auto funcPtr = (func_void_void_type) dlsym(library_handle, funcNamePtr.get());
  const char *dl_sym_error = dlerror();
  if (dl_sym_error) {
    callInnerError(env, thisObject, "Io8LggAI6k:: error call dlSym for `void_void`: ", dl_sym_error);
    return;
  }

  func_void_void = funcPtr;

  cout << "C++ WeBX1MH0zr :: assigned func_void_void to `" << funcNamePtr.get() << "`" << endl;
}

/*
 * Class:     kz_pompei_glazga_llvm_cpp_CppModule
 * Method:    assignFunc_constCharRef_constCharRef
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_kz_pompei_glazga_llvm_cpp_CppModule_assignFunc_1constCharRef_1charRef
    (JNIEnv *env, jobject thisObject, jstring funcName) {

  UniqueConstCharPtr funcNamePtr{env->GetStringUTFChars(funcName, nullptr)};

  dlerror();// очистить лот ошибки
  auto funcPtr = (func_constCharPtr_constCharPtr_type) dlsym(library_handle, funcNamePtr.get());
  const char *dl_sym_error = dlerror();
  if (dl_sym_error) {
    callInnerError(env, thisObject, "1yUw94lMZ8 :: error call dlSym for `constCharRef_charRef`: ", dl_sym_error);
    return;
  }

  func_constCharPtr_constCharPtr = funcPtr;

  cout << "C++ Tu8Kfk1kSv :: assigned func_constCharPtr_constCharPtr to `" << funcNamePtr.get() << "`" << endl;
}

/*
 * Class:     kz_pompei_glazga_llvm_cpp_CppModule
 * Method:    call__void_void
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_kz_pompei_glazga_llvm_cpp_CppModule_call_1_1void_1void
    (JNIEnv *env, jobject thisObject) {

  if (func_void_void) {
    func_void_void();
  } else {
    callInnerError(env, thisObject, "KpE3E4QvbF :: `func_void_void` is not defined", "");
  }

}

/*
 * Class:     kz_pompei_glazga_llvm_cpp_CppModule
 * Method:    call__constCharRef_constCharRef
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_kz_pompei_glazga_llvm_cpp_CppModule_call_1_1constCharRef_1constCharRef
    (JNIEnv *env, jobject thisObject, jstring arg) {

  cout << "C++ Nc5YUDaGEn :: calling constCharRef_constCharRef" << endl;

  if (!func_constCharPtr_constCharPtr) {
    callInnerError(env, thisObject, "qBhGedTpGT :: `func_constCharPtr_constCharPtr` is not defined", "");
    return nullptr;
  }

  cout << "C++ Nc5YUDaGEn :: function constCharRef_constCharRef exists" << endl;

  UniqueConstCharPtr argPtr{env->GetStringUTFChars(arg, nullptr)};

  cout << "C++ QwNaSJwvW5 :: argPtr = " << argPtr.get() << endl;

  const char *result = func_constCharPtr_constCharPtr(argPtr.get());

  cout << "C++ Wld95aLL1X :: called func_constCharPtr_constCharPtr : result = `" << result << "`" << endl;

  if (result == nullptr) return nullptr;

  return env->NewStringUTF(result);
}

/*
 * Class:     kz_pompei_glazga_llvm_cpp_CppModule
 * Method:    callInnerError
 * Signature: (Ljava/lang/String;)V
 */
void Java_kz_pompei_glazga_llvm_cpp_CppModule_callInnerError(JNIEnv *env, jobject thisObject, jstring errorText) {

  UniqueConstCharPtr errorTextPtr{env->GetStringUTFChars(errorText, nullptr)};
  
  callInnerError(env, thisObject, "vDBASg8dLE :: Java errorText = ", errorTextPtr.get());

}
