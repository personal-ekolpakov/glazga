cmake_minimum_required(VERSION 3.27)
project(glazga_jni)

set(CMAKE_CXX_STANDARD 17)

set(ENV{JAVA_HOME} "/usr/lib/jvm/java-21-openjdk-amd64")

find_package(JNI REQUIRED)

add_library(glazga SHARED CppModule.cpp)

add_executable(glazga_jni main.cpp)

target_include_directories(glazga PRIVATE ${JNI_INCLUDE_DIRS})

target_link_libraries(glazga_jni glazga )
