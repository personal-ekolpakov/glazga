#include <stdio.h>
#include <stdint.h>

typedef struct {
  uint32_t func_type;
  char *func_name;
  char *func_description;
} T_rec;

typedef struct {
  uint32_t size;
  T_rec **rec_arr_ref;
} T_rec_arr;

T_rec_arr get_test_list();

int main(int argc, char *argv[]) {
  T_rec_arr result = get_test_list();
  printf("ZbJz1a6ISG :: size = %d\n", result.size);

  for(uint32_t i = 0; i < result.size; i++) {
    uint32_t func_type = (**(result.rec_arr_ref + i)).func_type;
    char    *func_name = (**(result.rec_arr_ref + i)).func_name;
    char    *func_desc = (**(result.rec_arr_ref + i)).func_description;
    printf("gmDE1cy7Y7 :: i = %d, func_type = %d, func_name = `%s`, desc = `%s`\n", i, func_type, func_name, func_desc );
  }
  return 0;
}
