#!/usr/bin/env bash

mkdir -p build

cc -c -S -masm=intel -fverbose-asm -o build/print_tst_list.asm print_tst_list.c

llc -relocation-model=pic -opaque-pointers -march=x86-64 -x86-asm-syntax=intel -o build/test_lst.asm test_lst.ll
