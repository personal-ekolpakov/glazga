%T_testFuncName1 = type [20 x i8];
@r_testFuncName1 = private constant %T_testFuncName1 c"function_number_one\00", align 1

%T_testFuncDescription1 = type [50 x i8];
@r_testFuncDescription1 = private constant %T_testFuncDescription1 c"Описание функции номер раз\00", align 1

%T_testFuncName2 = type [8 x i8];
@r_testFuncName2 = private constant %T_testFuncName2 c"do_hard\00", align 1

%T_testFuncDescription2 = type [40 x i8];
@r_testFuncDescription2 = private constant %T_testFuncDescription2 c"Сделай что-то тяжёлое\00", align 1


%T_testFuncName3 = type [18 x i8];
@r_testFuncName3 = private constant %T_testFuncName3 c"m.Probe_SInt4.ggg\00", align 1

%T_testFuncDescription3 = type [49 x i8];
@r_testFuncDescription3 = private constant %T_testFuncDescription3 c"Сделай что-то очень лёгкое\00", align 1

%T_rec = type {i32, ptr, ptr};
@tst1 = private constant %T_rec {i32 23, %T_testFuncName1* @r_testFuncName1, %T_testFuncDescription1* @r_testFuncDescription1}
@tst2 = private constant %T_rec {i32 71, %T_testFuncName2* @r_testFuncName2, %T_testFuncDescription2* @r_testFuncDescription2}
@tst3 = private constant %T_rec {i32 11, %T_testFuncName3* @r_testFuncName3, %T_testFuncDescription3* @r_testFuncDescription3}

%T_rec_arr = type [3 x ptr];
@tst_arr = private constant %T_rec_arr [ ptr @tst1, ptr @tst2, ptr @tst3 ];

define {i32, ptr} @get_test_list() {
    ;%ret = getelementptr inbounds %T_testFuncName1, %T_testFuncName1* @r_testFuncName1, i32 0
    ret {i32, ptr} {i32 3, ptr @tst_arr}
}
