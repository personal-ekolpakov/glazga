#!/usr/bin/env bash

set -e

cd "$(dirname "$0")" || exit 113

JAVA_HOME="/usr/lib/jvm/java-21-openjdk-amd64"

JAVA="$JAVA_HOME/bin/java"

PRJ="$(readlink -f ../../)"

CP=
CP="$CP:$PRJ/env/build/libs/env-0.0.1.jar"
CP="$CP:$PRJ/utils/build/libs/utils-0.0.1.jar"
CP="$CP:$PRJ/mat/build/libs/mat-0.0.1.jar"
CP="$CP:$PRJ/ann/build/libs/ann-0.0.1.jar"

LM="/home/pompei/.gradle/caches/modules-2/files-2.1/"

CP="$CP:$LM/org.jetbrains/annotations/24.0.0/69b8b443c437fdeefa8d20c18d257b94836a92b9/annotations-24.0.0.jar"
CP="$CP:$LM/org.testng/testng/7.4.0/b6becc0a058e272473ac3ba983966ff507ca0300/testng-7.4.0.jar"
CP="$CP:$LM/org.webjars/jquery/3.6.1/d08df6250157cd2db3d9b01b11b76e9b7225083a/jquery-3.6.1.jar"
CP="$CP:$LM/org.assertj/assertj-core/3.24.2/ebbf338e33f893139459ce5df023115971c2786f/assertj-core-3.24.2.jar"
CP="$CP:$LM/com.beust/jcommander/1.78/a3927de9bd6f351429bcf763712c9890629d8f51/jcommander-1.78.jar"
CP="$CP:$LM/net.bytebuddy/byte-buddy/1.12.21/1a257bbdb9288f2558d3767cc2cc22b499d5091c/byte-buddy-1.12.21.jar"

CLASSES="$PRJ/act_lang_llvm/build/classes"

CP="$CP:$CLASSES/java/main"
CP="$CP:$CLASSES/java/test"

echo $JAVA -classpath "$CP" kz.pompei.glazga.llvm.NativeLlvmProbe
