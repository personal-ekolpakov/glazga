package kz.pompei.glazga.llvm.gen.meta;

import lombok.NonNull;

public enum AllocKind {

  Alloc,

  ReAlloc,

  AllocPtr,

  Free,

  UnInitialized,

  Zeroed,

  Aligned,

  ;

  public @NonNull String print() {
    return name().toLowerCase();
  }

}
