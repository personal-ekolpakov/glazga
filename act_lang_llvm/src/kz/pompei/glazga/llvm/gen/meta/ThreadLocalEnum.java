package kz.pompei.glazga.llvm.gen.meta;

public enum ThreadLocalEnum {
  LocalDynamic,

  InitialExec,

  LocalExec,

  ;

  @SuppressWarnings("unused")
  public String print() {
    return "thread_local(" + name().toLowerCase() + ")";
  }
}
