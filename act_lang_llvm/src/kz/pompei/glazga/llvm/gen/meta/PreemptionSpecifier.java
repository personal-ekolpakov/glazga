package kz.pompei.glazga.llvm.gen.meta;

import lombok.NonNull;

public enum PreemptionSpecifier {

  DSO_PreEmpTable,

  DSO_Local,

  ;

  public @NonNull String print() {
    return name().toLowerCase();
  }
}
