package kz.pompei.glazga.llvm.gen.meta;

import lombok.NonNull;

public enum UnNamed {
  Unnamed_Addr,
  Local_Unnamed_Addr,

  ;

  public @NonNull String print() {
    return name().toLowerCase();
  }
}
