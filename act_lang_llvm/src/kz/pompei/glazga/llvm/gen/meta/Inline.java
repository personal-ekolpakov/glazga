package kz.pompei.glazga.llvm.gen.meta;

import lombok.NonNull;

public enum Inline {
  NoInline,

  AlwaysInline,

  ;

  public @NonNull String print() {
    return name().toLowerCase();
  }
}
