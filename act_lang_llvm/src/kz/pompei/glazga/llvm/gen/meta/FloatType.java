package kz.pompei.glazga.llvm.gen.meta;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum FloatType {

  Half(16),

  BFloat(16),

  Float(32),

  Double(64),

  Fp128(128),

  X86_fp80(80),

  PPC_fp128(128),


  ;

  public final int sizeBits;

  public @NonNull String print() {
    return name().toLowerCase();
  }
}
