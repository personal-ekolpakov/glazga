package kz.pompei.glazga.llvm.gen.meta;

import lombok.NonNull;

public enum MemoryAccessType {
  None, Read, Write, ReadWrite;

  public @NonNull String print() {
    return name().toLowerCase();
  }
}
