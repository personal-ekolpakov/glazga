package kz.pompei.glazga.llvm.gen.meta;

import lombok.NonNull;

public enum GlobalVarKind {
  Global,
  Constant,

  ;

  public @NonNull String print() {
    return name().toLowerCase();
  }
}
