package kz.pompei.glazga.llvm.gen.meta;

import lombok.NonNull;

public enum DLLStorageClass {
  DLLImport,

  DLLExport,
  ;

  public @NonNull String print() {
    return name().toLowerCase();
  }
}
