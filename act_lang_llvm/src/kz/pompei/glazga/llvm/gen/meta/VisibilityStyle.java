package kz.pompei.glazga.llvm.gen.meta;

import lombok.NonNull;

public enum VisibilityStyle {
  Default,

  Hidden,

  Protected,

  ;

  public @NonNull String print() {
    return name().toLowerCase();
  }
}
