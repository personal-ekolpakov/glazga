package kz.pompei.glazga.llvm.gen.meta;

/**
 * Функции, вызовы и invoke LLVM могут иметь опционально указанную соглашение о вызове.
 * Соглашение о вызове любой пары динамически вызывающего/вызываемого должно совпадать,
 * иначе поведение программы не определено. Следующие соглашения о вызове поддерживаются LLVM,
 * и в будущем могут быть добавлены дополнительные.
 */
public enum CallConvention {

  /**
   * The C calling convention
   */
  CCC,

  /**
   * The fast calling convention
   */
  FastCC,

  ColdCC,
  GhCCC,
  CC_11,
  AnyRegCC,
  Preserve_MostCC,
  Preserve_AllCC,
  Preserve_NoneCC,
  CXX_fast_TlsCC,
  TailCC,
  SwiftCC,
  SwiftTailCC,
  CfGuard_CheckCC,
  CC,

  ;

  @SuppressWarnings("unused")
  public String print() {
    return '"' + name().toLowerCase() + '"';
  }
}
