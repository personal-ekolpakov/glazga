package kz.pompei.glazga.llvm.gen.module_decl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import kz.pompei.glazga.llvm.gen.LLModule;
import kz.pompei.glazga.llvm.gen.ann.CanVarRef;
import kz.pompei.glazga.llvm.gen.ann.LineAccepter;
import kz.pompei.glazga.llvm.gen.ann.MyFunc;
import kz.pompei.glazga.llvm.gen.meta.CallConvention;
import kz.pompei.glazga.llvm.gen.meta.DLLStorageClass;
import kz.pompei.glazga.llvm.gen.meta.Linkage;
import kz.pompei.glazga.llvm.gen.meta.PreemptionSpecifier;
import kz.pompei.glazga.llvm.gen.meta.UnNamed;
import kz.pompei.glazga.llvm.gen.meta.VisibilityStyle;
import kz.pompei.glazga.llvm.gen.module_decl.func.FuncBlock;
import kz.pompei.glazga.llvm.gen.module_decl.func.LLArg;
import kz.pompei.glazga.llvm.gen.module_decl.func.LLArgAttrs;
import kz.pompei.glazga.llvm.gen.module_decl.func.LLFuncAttrs;
import kz.pompei.glazga.llvm.gen.module_decl.func.LLFuncPrefix;
import kz.pompei.glazga.llvm.gen.module_decl.func.LLFuncPrologue;
import kz.pompei.glazga.llvm.gen.types.TType;
import kz.pompei.glazga.llvm.gen.types.values.ValueFromArg;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DefineFunc extends ModuleStatement implements MyFunc, CanVarRef {
  public final @NonNull LLModule myModule;

  private final List<FuncBlock> blocks = new ArrayList<>();

  private int nextLocalIndex = 1;

  public Linkage linkage;

  public PreemptionSpecifier preemptionSpecifier;

  public VisibilityStyle visibility;

  public DLLStorageClass dllStorageClass;

  public CallConvention call;

  public final LLArgAttrs<DefineFunc> returnTypeAttrs = new LLArgAttrs<>(this);

  public TType returnType;

  public String funcName;

  public Integer funcIndex;

  public final List<LLArg> argList = new ArrayList<>();

  public UnNamed unNamed;

  public Integer addressSpace;

  private final LLFuncAttrs attrs = new LLFuncAttrs(this);

  public String sectionName;

  public String partitionName;

  public ComDat comDat;

  public Integer align;

  public String gc;

  public final LLFuncPrefix prefix = new LLFuncPrefix();

  public final LLFuncPrologue prologue = new LLFuncPrologue();

  public final LinkedHashMap<String, String> meta = new LinkedHashMap<>();

  public @NonNull String printHead() {
    StringBuilder sb = new StringBuilder();

    sb.append("define");

    if (linkage != null) {
      sb.append(' ').append(linkage.print());
    }

    if (preemptionSpecifier != null) {
      sb.append(' ').append(preemptionSpecifier.print());
    }

    if (visibility != null) {
      sb.append(' ').append(visibility.print());
    }

    if (dllStorageClass != null) {
      sb.append(' ').append(dllStorageClass.print());
    }

    if (call != null) {
      sb.append(' ').append(call.print());
    }

    {
      if (returnType == null) {
        throw new RuntimeException("yXVr6HKcV9 :: returnType == null");
      }
      sb.append(' ').append(returnType.print());
    }

    returnTypeAttrs.printFirstSpace(sb);

    sb.append(" " + varRefName());

    sb.append('(');
    {
      boolean first = true;
      for (final LLArg param : argList) {
        if (first) {
          first = false;
        } else {
          sb.append(", ");
        }

        sb.append(param.print());
      }
    }
    sb.append(')');

    if (unNamed != null) {
      sb.append(' ').append(unNamed.print());
    }

    if (addressSpace != null) {
      sb.append((" addrSpace(" + addressSpace + ")").toLowerCase());
    }

    attrs.printFirstSpace(sb);

    if (sectionName != null) {
      sb.append((", section '" + sectionName + "'").replace('\'', '"'));
    }

    if (partitionName != null) {
      sb.append((", partition '" + partitionName + "'").replace('\'', '"'));
    }

    if (comDat != null) {
      sb.append(' ').append(comDat.print());
    }

    if (align != null) {
      sb.append(" align " + align);
    }

    if (gc != null) {
      sb.append((" gc '" + gc + "'").replace('\'', '"'));
    }

    prefix.printFirstSpace(sb);

    prologue.printFirstSpace(sb);

    for (final Map.Entry<String, String> e : meta.entrySet()) {
      sb.append(' ').append(e.getKey()).append(' ').append(e.getValue());
    }

    return sb.toString();
  }

  public LLFuncAttrs attrs() {
    return attrs;
  }

  @Override
  public @NonNull String varRefName() {
    if (funcName != null) {
      return "@" + funcName;
    }
    if (funcIndex != null) {
      return "@" + funcIndex;
    }
    throw new RuntimeException("YZjJtQwgHG :: Not defined func name or index");
  }

  private boolean countIndexes = false;

  @Override
  public void print(@NonNull LineAccepter lineAccepter) {
    if (!countIndexes) {
      countIndexes();
      countIndexes = true;
    }
    lineAccepter.accept("");
    lineAccepter.accept(printHead());
    lineAccepter.accept("{");
    for (final FuncBlock block : blocks) {
      block.print(lineAccepter);
    }
    lineAccepter.accept("}");
  }

  @Override
  public void countIndexes() {
    if (Integer.valueOf(0).equals(funcIndex)) {
      funcIndex = myModule.nextIndex();
    }
    argList.forEach(p -> p.countIndex(this::nextLocalIndex));
    blocks.forEach(FuncBlock::countIndexes);
  }

  public int nextLocalIndex() {
    return nextLocalIndex++;
  }

  public DefineFunc linkage(Linkage linkage) {
    this.linkage = linkage;
    return this;
  }

  public DefineFunc preemptionSpecifier(PreemptionSpecifier preemptionSpecifier) {
    this.preemptionSpecifier = preemptionSpecifier;
    return this;
  }

  public DefineFunc visibility(VisibilityStyle visibility) {
    this.visibility = visibility;
    return this;
  }

  public DefineFunc dllStorageClass(DLLStorageClass dllStorageClass) {
    this.dllStorageClass = dllStorageClass;
    return this;
  }

  public DefineFunc call(CallConvention call) {
    this.call = call;
    return this;
  }

  public DefineFunc returnType(TType returnType) {
    this.returnType = returnType;
    return this;
  }

  public DefineFunc funcName(String funcName) {
    this.funcName = funcName;
    return this;
  }

  public DefineFunc funcIndex(Integer funcIndex) {
    this.funcIndex = funcIndex;
    return this;
  }

  public DefineFunc addressSpace(Integer addressSpace) {
    this.addressSpace = addressSpace;
    return this;
  }

  public DefineFunc sectionName(String sectionName) {
    this.sectionName = sectionName;
    return this;
  }

  public DefineFunc partitionName(String partitionName) {
    this.partitionName = partitionName;
    return this;
  }

  public DefineFunc comDat(ComDat comDat) {
    this.comDat = comDat;
    return this;
  }

  public DefineFunc align(Integer align) {
    this.align = align;
    return this;
  }

  public DefineFunc gc(String gc) {
    this.gc = gc;
    return this;
  }

  public DefineFunc unNamed(UnNamed unNamed) {
    this.unNamed = unNamed;
    return this;
  }

  public LLArgAttrs<DefineFunc> returnTypeAttrs() {
    return returnTypeAttrs;
  }

  public DefineFunc newArg(Consumer<LLArg> consumer) {
    LLArg param = new LLArg();
    argList.add(param);
    consumer.accept(param);
    return this;
  }

  public FuncBlock newBlock() {
    FuncBlock ret = new FuncBlock(this);
    blocks.add(ret);
    return ret;
  }

  public ValueFromArg argRef(@NonNull String argName) {
    for (final LLArg arg : argList) {
      if (argName.equals(arg.name)) {
        return ValueFromArg.of(arg);
      }
    }
    throw new RuntimeException("l9eIx1lJSp :: No arg with name = `" + argName + "` in function `" + varRefName() + "`");
  }
}
