package kz.pompei.glazga.llvm.gen.module_decl.func;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.llvm.gen.ann.LineAccepter;
import kz.pompei.glazga.llvm.gen.module_decl.DefineFunc;
import kz.pompei.glazga.llvm.gen.module_decl.func.instructions.GetElementPtr;
import kz.pompei.glazga.llvm.gen.module_decl.func.instructions.Instruction;
import kz.pompei.glazga.llvm.gen.module_decl.func.instructions.Ret;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FuncBlock {
  public final @NonNull DefineFunc myFunc;

  public String  name;
  public Integer index;

  public final List<Instruction> list = new ArrayList<>();

  public FuncBlock name(String name) {
    this.name = name;
    return this;
  }

  public void countIndexes() {
    if (Integer.valueOf(0).equals(index)) {
      index = myFunc.nextLocalIndex();
    }
    list.forEach(Instruction::countIndexes);
  }

  public @NonNull String varRefName() {
    if (name != null) {
      return name;
    }
    if (index != null) {
      return "" + index;
    }
    throw new RuntimeException("H8M64LVFOQ :: No name or index for FuncBlock");
  }

  public @NonNull GetElementPtr newGetElementPtr() {
    GetElementPtr ret = new GetElementPtr(this);
    list.add(ret);
    return ret;
  }

  public @NonNull Ret newRet() {
    var ret = new Ret(this);
    list.add(ret);
    return ret;
  }

  public void print(@NonNull LineAccepter lineAccepter) {
    lineAccepter.accept("  " + varRefName() + ":");
    for (final Instruction instruction : list) {
      lineAccepter.accept(instruction.print());
    }
  }
}
