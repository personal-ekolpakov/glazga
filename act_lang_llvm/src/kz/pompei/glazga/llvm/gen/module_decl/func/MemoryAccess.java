package kz.pompei.glazga.llvm.gen.module_decl.func;

import kz.pompei.glazga.llvm.gen.meta.MemoryAccessType;
import kz.pompei.glazga.llvm.gen.module_decl.DefineFunc;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static kz.pompei.glazga.llvm.gen.meta.MemoryAccessType.None;

@RequiredArgsConstructor
public class MemoryAccess {
  private final @NonNull LLFuncAttrs owner;

  private boolean on = false;

  public @NonNull MemoryAccessType any             = None;
  public @NonNull MemoryAccessType argMem          = None;
  public @NonNull MemoryAccessType inAccessibleMem = None;

  public void printFirstSpace(@NonNull StringBuilder sb) {
    if (!on) {
      return;
    }

    sb.append(" memory(").append(content()).append(')');
  }

  private @NonNull String content() {
    if (any == None && argMem == None && inAccessibleMem == None) {
      return "none";
    }

    StringBuilder sb = new StringBuilder();

    if (any != None) {
      sb.append(any.print());
    }

    if (argMem != None) {
      comma(sb);
      sb.append(("argMem: " + argMem.print()).toLowerCase());
    }
    if (inAccessibleMem != None) {
      comma(sb);
      sb.append(("inAccessibleMem: " + inAccessibleMem.print()).toLowerCase());
    }

    return sb.toString();
  }

  private void comma(StringBuilder sb) {
    if (sb.length() > 0) {
      sb.append(", ");
    }
  }


  public DefineFunc any(MemoryAccessType mat) {
    on  = true;
    any = mat;
    return owner.myFunc;
  }

  public DefineFunc argMem(MemoryAccessType mat) {
    on     = true;
    argMem = mat;
    return owner.myFunc;
  }

  public DefineFunc inAccessibleMem(MemoryAccessType mat) {
    on              = true;
    inAccessibleMem = mat;
    return owner.myFunc;
  }

  public DefineFunc on(boolean on) {
    this.on = on;
    return owner.myFunc;
  }

}
