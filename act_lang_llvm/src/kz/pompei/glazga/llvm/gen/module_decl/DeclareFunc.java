package kz.pompei.glazga.llvm.gen.module_decl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import kz.pompei.glazga.llvm.gen.LLModule;
import kz.pompei.glazga.llvm.gen.ann.CanVarRef;
import kz.pompei.glazga.llvm.gen.ann.LineAccepter;
import kz.pompei.glazga.llvm.gen.ann.MyFunc;
import kz.pompei.glazga.llvm.gen.meta.CallConvention;
import kz.pompei.glazga.llvm.gen.meta.DLLStorageClass;
import kz.pompei.glazga.llvm.gen.meta.Linkage;
import kz.pompei.glazga.llvm.gen.meta.UnNamed;
import kz.pompei.glazga.llvm.gen.meta.VisibilityStyle;
import kz.pompei.glazga.llvm.gen.module_decl.func.LLArg;
import kz.pompei.glazga.llvm.gen.module_decl.func.LLArgAttrs;
import kz.pompei.glazga.llvm.gen.module_decl.func.LLFuncPrefix;
import kz.pompei.glazga.llvm.gen.module_decl.func.LLFuncPrologue;
import kz.pompei.glazga.llvm.gen.types.TType;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DeclareFunc extends ModuleStatement implements MyFunc, CanVarRef {
  public final @NonNull LLModule myModule;

  private int nextLocalIndex = 1;

  public Linkage linkage;

  public VisibilityStyle visibility;

  public DLLStorageClass dllStorageClass;

  public CallConvention call;

  public final LLArgAttrs<DeclareFunc> returnTypeAttrs = new LLArgAttrs<>(this);

  public TType returnType;

  public String funcName;

  public Integer funcIndex;

  public final List<LLArg> argList = new ArrayList<>();

  public UnNamed unNamed;

  public Integer align;

  public String gc;

  public final LLFuncPrefix prefix = new LLFuncPrefix();

  public final LLFuncPrologue prologue = new LLFuncPrologue();

  public @NonNull String printHead() {
    StringBuilder sb = new StringBuilder();

    sb.append("declare");

    if (linkage != null) {
      sb.append(' ').append(linkage.print());
    }

    if (visibility != null) {
      sb.append(' ').append(visibility.print());
    }

    if (dllStorageClass != null) {
      sb.append(' ').append(dllStorageClass.print());
    }

    if (call != null) {
      sb.append(' ').append(call.print());
    }

    {
      if (returnType == null) {
        throw new RuntimeException("yXVr6HKcV9 :: returnType == null");
      }
      sb.append(' ').append(returnType.print());
    }

    returnTypeAttrs.printFirstSpace(sb);

    sb.append(" " + varRefName());

    sb.append('(');
    {
      boolean first = true;
      for (final LLArg param : argList) {
        if (first) {
          first = false;
        } else {
          sb.append(", ");
        }

        sb.append(param.print());
      }
    }
    sb.append(')');

    if (unNamed != null) {
      sb.append(' ').append(unNamed.print());
    }

    if (align != null) {
      sb.append(" align " + align);
    }

    if (gc != null) {
      sb.append((" gc '" + gc + "'").replace('\'', '"'));
    }

    prefix.printFirstSpace(sb);

    prologue.printFirstSpace(sb);

    return sb.toString();
  }

  @Override
  public @NonNull String varRefName() {
    if (funcName != null) {
      return "@" + funcName;
    }
    if (funcIndex != null) {
      return "@" + funcIndex;
    }
    throw new RuntimeException("eHnalGvqL4 :: VarRefName does not defined in " + getClass().getSimpleName());
  }

  private boolean countIndexes = false;

  @Override
  public void print(@NonNull LineAccepter lineAccepter) {
    countIndexes();
    lineAccepter.accept(printHead());
  }

  @Override
  public void countIndexes() {
    if (countIndexes) {
      return;
    }
    countIndexes = true;

    if (Integer.valueOf(0).equals(funcIndex)) {
      funcIndex = myModule.nextIndex();
    }

    argList.forEach(p -> p.countIndex(() -> nextLocalIndex++));
  }

  public DeclareFunc linkage(Linkage linkage) {
    this.linkage = linkage;
    return this;
  }

  public DeclareFunc visibility(VisibilityStyle visibility) {
    this.visibility = visibility;
    return this;
  }

  public DeclareFunc dllStorageClass(DLLStorageClass dllStorageClass) {
    this.dllStorageClass = dllStorageClass;
    return this;
  }

  public DeclareFunc call(CallConvention call) {
    this.call = call;
    return this;
  }

  public DeclareFunc returnType(TType returnType) {
    this.returnType = returnType;
    return this;
  }

  public DeclareFunc funcName(String funcName) {
    this.funcName = funcName;
    return this;
  }

  public DeclareFunc funcIndex(Integer funcIndex) {
    this.funcIndex = funcIndex;
    return this;
  }

  public DeclareFunc align(Integer align) {
    this.align = align;
    return this;
  }

  public DeclareFunc gc(String gc) {
    this.gc = gc;
    return this;
  }

  public DeclareFunc unNamed(UnNamed unNamed) {
    this.unNamed = unNamed;
    return this;
  }

  public LLArgAttrs<DeclareFunc> returnTypeAttrs() {
    return returnTypeAttrs;
  }

  public DeclareFunc newArg(Consumer<LLArg> consumer) {
    LLArg param = new LLArg();
    argList.add(param);
    consumer.accept(param);
    return this;
  }
}
