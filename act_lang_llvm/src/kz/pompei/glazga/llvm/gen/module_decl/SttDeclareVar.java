package kz.pompei.glazga.llvm.gen.module_decl;

import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import kz.pompei.glazga.llvm.gen.LLModule;
import kz.pompei.glazga.llvm.gen.ann.CanVarRef;
import kz.pompei.glazga.llvm.gen.ann.LineAccepter;
import kz.pompei.glazga.llvm.gen.meta.DLLStorageClass;
import kz.pompei.glazga.llvm.gen.meta.GlobalVarKind;
import kz.pompei.glazga.llvm.gen.meta.Linkage;
import kz.pompei.glazga.llvm.gen.meta.PreemptionSpecifier;
import kz.pompei.glazga.llvm.gen.meta.ThreadLocalEnum;
import kz.pompei.glazga.llvm.gen.meta.UnNamed;
import kz.pompei.glazga.llvm.gen.meta.VisibilityStyle;
import kz.pompei.glazga.llvm.gen.types.TArray;
import kz.pompei.glazga.llvm.gen.types.TInt;
import kz.pompei.glazga.llvm.gen.types.TType;
import kz.pompei.glazga.llvm.util.StrPrinter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@SuppressWarnings("UnusedReturnValue")
@RequiredArgsConstructor
public class SttDeclareVar extends ModuleStatement implements CanVarRef {
  protected final LLModule owner;

  public String varName;

  public Integer varIndex;

  public Linkage linkage;

  public PreemptionSpecifier preemptionSpecifier;

  public VisibilityStyle visibility;

  public DLLStorageClass dllStorageClass;

  public ThreadLocalEnum threadLocal;

  public UnNamed unNamed;

  public Integer addressSpace;

  public GlobalVarKind kind;

  public TType type;

  public String initializerConstant;

  public String sectionName;

  public String partitionName;

  public ComDat comDat;

  public Integer align;

  public String code_model;

  public boolean no_sanitize_address = false;// false - do not print

  public boolean no_sanitize_hwAddress = false;// false - do not print

  public boolean sanitize_address_dynInit = false;// false - do not print

  public boolean sanitize_memTag = false;// false - do not print

  public final LinkedHashMap<String, String> metaMap = new LinkedHashMap<>();//store ordering

  public @NonNull String varRefName() {
    if (varName != null) {
      return "@" + varName;
    }

    if (varIndex != null) {
      return "@" + varIndex;
    }

    throw new RuntimeException("ebKmSTJg1S :: Ref name or index did not defined in " + getClass().getSimpleName());
  }

  @Override
  public void countIndexes() {
    if (Integer.valueOf(0).equals(varIndex)) {
      varIndex = owner.nextIndex();
    }
  }

  @SuppressWarnings("DuplicatedCode")
  @Override
  public void print(@NonNull LineAccepter la) {

    StringBuilder sb = new StringBuilder();

    sb.append(varRefName()).append(" =");

    if (linkage != null) {
      sb.append(' ').append(linkage.print());
    }

    if (preemptionSpecifier != null) {
      sb.append(' ').append(preemptionSpecifier.print());
    }

    if (visibility != null) {
      sb.append(' ').append(visibility.print());
    }

    if (dllStorageClass != null) {
      sb.append(' ').append(dllStorageClass.print());
    }

    if (threadLocal != null) {
      sb.append(' ').append(threadLocal.print());
    }

    if (unNamed != null) {
      sb.append(' ').append(unNamed.print());
    }

    if (addressSpace != null) {
      sb.append((" addrSpace(" + addressSpace + ")").toLowerCase());
    }

    {
      if (kind == null) {
        throw new RuntimeException("u1idQtTxe2 :: No kind");
      }
      sb.append(' ').append(kind.print());
    }

    {
      if (type == null) {
        throw new RuntimeException("yx12oQe54l :: Type == null");
      }
      sb.append(' ').append(type.print());
    }

    if (initializerConstant != null) {
      sb.append(' ').append(initializerConstant);
    }

    if (sectionName != null) {
      sb.append((", section '" + sectionName + "'").replace('\'', '"'));
    }

    if (partitionName != null) {
      sb.append((", partition '" + partitionName + "'").replace('\'', '"'));
    }

    if (comDat != null) {
      sb.append(", ").append(comDat.print());
    }

    if (align != null) {
      sb.append(", align ").append(align);
    }

    if (code_model != null) {
      sb.append((", code_model '" + code_model + "'").replace('\'', '"'));
    }

    if (no_sanitize_address) {
      sb.append(", no_sanitize_address".toLowerCase());
    }

    if (no_sanitize_hwAddress) {
      sb.append(", no_sanitize_hwAddress".toLowerCase());
    }

    if (sanitize_address_dynInit) {
      sb.append(", sanitize_address_dynInit".toLowerCase());
    }

    if (sanitize_memTag) {
      sb.append(", sanitize_memTag".toLowerCase());
    }

    for (final Map.Entry<String, String> e : metaMap.entrySet()) {
      sb.append(", !" + e.getKey() + " !" + e.getValue());
    }

    la.accept(sb.toString());
  }

  public SttDeclareVar varName(String varName) {
    this.varName = varName;
    return this;
  }


  public SttDeclareVar linkage(Linkage linkage) {
    this.linkage = linkage;
    return this;
  }

  public SttDeclareVar preemptionSpecifier(PreemptionSpecifier preemptionSpecifier) {
    this.preemptionSpecifier = preemptionSpecifier;
    return this;
  }

  public SttDeclareVar visibility(VisibilityStyle visibility) {
    this.visibility = visibility;
    return this;
  }

  public SttDeclareVar dllStorageClass(DLLStorageClass dllStorageClass) {
    this.dllStorageClass = dllStorageClass;
    return this;
  }

  public SttDeclareVar threadLocal(ThreadLocalEnum threadLocal) {
    this.threadLocal = threadLocal;
    return this;
  }

  public SttDeclareVar unNamed(UnNamed unNamed) {
    this.unNamed = unNamed;
    return this;
  }

  public SttDeclareVar addressSpace(Integer addressSpace) {
    this.addressSpace = addressSpace;
    return this;
  }

  public SttDeclareVar initializerConstant(String initializerConstant) {
    this.initializerConstant = initializerConstant;
    return this;
  }

  public SttDeclareVar sectionName(String sectionName) {
    this.sectionName = sectionName;
    return this;
  }

  public SttDeclareVar partitionName(String partitionName) {
    this.partitionName = partitionName;
    return this;
  }

  public SttDeclareVar comDat(ComDat comDat) {
    this.comDat = comDat;
    return this;
  }

  public SttDeclareVar align(Integer align) {
    this.align = align;
    return this;
  }

  public SttDeclareVar code_model(String code_model) {
    this.code_model = code_model;
    return this;
  }

  public SttDeclareVar no_sanitize_address(boolean no_sanitize_address) {
    this.no_sanitize_address = no_sanitize_address;
    return this;
  }

  public SttDeclareVar no_sanitize_hwAddress(boolean no_sanitize_hwAddress) {
    this.no_sanitize_hwAddress = no_sanitize_hwAddress;
    return this;
  }

  public SttDeclareVar sanitize_address_dynInit(boolean sanitize_address_dynInit) {
    this.sanitize_address_dynInit = sanitize_address_dynInit;
    return this;
  }

  public SttDeclareVar sanitize_memTag(boolean sanitize_memTag) {
    this.sanitize_memTag = sanitize_memTag;
    return this;
  }

  public SttDeclareVar kind(GlobalVarKind kind) {
    this.kind = kind;
    return this;
  }

  public SttDeclareVar type(TType type) {
    this.type = type;
    return this;
  }

  public SttDeclareVar meta(String key, String value) {
    if (key != null) {
      if (value == null) {
        metaMap.remove(key);
      } else {
        metaMap.put(key, value);
      }
    }
    return this;
  }


  public SttDeclareVar arrayWithText(String textString) {

    int length = textString.getBytes(StandardCharsets.UTF_8).length;

    type(TArray.of(length, TInt.len(8)));

    initializerConstant(StrPrinter.printStr(textString));

    return this;
  }
}
