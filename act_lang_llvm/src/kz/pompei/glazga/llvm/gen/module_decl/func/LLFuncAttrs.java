package kz.pompei.glazga.llvm.gen.module_decl.func;

import java.util.LinkedHashMap;
import java.util.Map;
import kz.pompei.glazga.llvm.gen.meta.AllocKind;
import kz.pompei.glazga.llvm.gen.meta.Inline;
import kz.pompei.glazga.llvm.gen.module_decl.DefineFunc;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static java.util.stream.Collectors.joining;

/**
 * See <a href="https://llvm.org/docs/LangRef.html#fnattrs">LLVM Function Attributes</a>
 */
@RequiredArgsConstructor
public class LLFuncAttrs {
  public final @NonNull DefineFunc myFunc;

  public Inline inline;

  public boolean optSize = false;

  public Integer alignStack;

  public String allocFamily;

  public final LinkedHashMap<AllocKind, Boolean> allocKind = new LinkedHashMap<>();

  public Integer allocSize, allocSizeCount;

  public boolean builtIn = false;

  public boolean cold       = false;
  public boolean convergent = false;

  public final MemoryAccess memory = new MemoryAccess(this);

  public void printFirstSpace(StringBuilder sb) {

    if (inline != null) {
      sb.append(' ').append(inline.print());
    }

    if (optSize) {
      sb.append(" optSize".toLowerCase());
    }

    if (alignStack != null) {
      sb.append((" alignStack(" + alignStack + ")").toLowerCase());
    }

    if (allocFamily != null) {
      sb.append((" 'alloc-family'='" + allocFamily + "'").replace('\'', '"'));
    }

    String allocKindStr = allocKind.entrySet()
                                   .stream()
                                   .filter(Map.Entry::getValue)
                                   .map(Map.Entry::getKey)
                                   .map(AllocKind::print)
                                   .collect(joining(", ", "\"", "\""));

    if (allocKind.size() > 0) {
      sb.append(" allocKind(".toLowerCase() + allocKindStr + ")");
    }

    if (allocSize != null) {
      sb.append(" allocSize(".toLowerCase() + allocSize + (allocSizeCount == null ? "" : ", " + allocSizeCount) + ")");
    }

    if (builtIn) {
      sb.append(" builtIn".toLowerCase());
    }

    if (cold) {
      sb.append(" cold".toLowerCase());
    }

    if (convergent) {
      sb.append(" convergent".toLowerCase());
    }

    memory.printFirstSpace(sb);
  }

  public MemoryAccess memory() {
    return memory;
  }

  public DefineFunc cold(boolean cold) {
    this.cold = cold;
    return myFunc;
  }

  public DefineFunc convergent(boolean convergent) {
    this.convergent = convergent;
    return myFunc;
  }

  public DefineFunc builtIn(boolean builtIn) {
    this.builtIn = builtIn;
    return myFunc;
  }

  public DefineFunc allocSize(Integer allocSize, Integer allocSizeCount) {
    this.allocSize      = allocSize;
    this.allocSizeCount = allocSizeCount;
    return myFunc;
  }

  public DefineFunc allocKind(AllocKind allocKind, boolean exists) {
    this.allocKind.put(allocKind, exists);
    return myFunc;
  }

  public DefineFunc allocFamily(String allocFamily) {
    this.allocFamily = allocFamily;
    return myFunc;
  }

  public DefineFunc inline(Inline inline) {
    this.inline = inline;
    return myFunc;
  }

  public DefineFunc optSize(boolean optSize) {
    this.optSize = optSize;
    return myFunc;
  }

  public DefineFunc alignStack(Integer alignStack) {
    this.alignStack = alignStack;
    return myFunc;
  }
}
