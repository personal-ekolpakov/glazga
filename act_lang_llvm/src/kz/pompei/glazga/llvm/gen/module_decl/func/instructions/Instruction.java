package kz.pompei.glazga.llvm.gen.module_decl.func.instructions;

import kz.pompei.glazga.llvm.gen.ann.CanVarRef;
import kz.pompei.glazga.llvm.gen.module_decl.func.FuncBlock;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class Instruction implements CanVarRef {
  public final @NonNull FuncBlock myBlock;

  public abstract @NonNull String print();

  public String varName;

  public Integer varIndex;

  @Override
  public void countIndexes() {
    if (Integer.valueOf(0).equals(varIndex)) {
      varIndex = myBlock.myFunc.nextLocalIndex();
    }
  }

  @Override
  public @NonNull String varRefName() {
    if (varName != null) {
      return "%" + varName;
    }
    if (varIndex != null) {
      return "%" + varIndex;
    }
    throw new RuntimeException("FvEg31LX4z :: Not defined value name in " + getClass().getSimpleName());
  }
}
