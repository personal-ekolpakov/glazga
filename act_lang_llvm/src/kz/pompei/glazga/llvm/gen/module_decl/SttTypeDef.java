package kz.pompei.glazga.llvm.gen.module_decl;

import kz.pompei.glazga.llvm.gen.LLModule;
import kz.pompei.glazga.llvm.gen.ann.CanVarRef;
import kz.pompei.glazga.llvm.gen.ann.LineAccepter;
import kz.pompei.glazga.llvm.gen.types.TType;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SttTypeDef extends ModuleStatement implements CanVarRef {
  public final @NonNull LLModule myModule;

  public String  varName;
  public Integer varIndex;

  public TType type;

  @Override
  public void print(@NonNull LineAccepter la) {
    la.accept(varRefName() + " = type " + type.print());
  }

  @Override
  public @NonNull String varRefName() {
    if (varName != null) {
      return "%" + varName;
    }
    if (varIndex != null) {
      return "%" + varIndex;
    }
    throw new RuntimeException("vgQuGRx1c4 :: No var name or index in " + getClass().getSimpleName());
  }

  public SttTypeDef type(TType type) {
    this.type = type;
    return this;
  }

  public SttTypeDef varName(String varName) {
    this.varName = varName;
    return this;
  }

  public SttTypeDef varIndex(Integer varIndex) {
    this.varIndex = varIndex;
    return this;
  }

  @Override
  public void countIndexes() {
    if (Integer.valueOf(0).equals(varIndex)) {
      varIndex = myModule.nextIndex();
    }
  }
}
