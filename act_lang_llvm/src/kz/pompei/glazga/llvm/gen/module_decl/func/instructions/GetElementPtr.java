package kz.pompei.glazga.llvm.gen.module_decl.func.instructions;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.llvm.gen.module_decl.func.FuncBlock;
import kz.pompei.glazga.llvm.gen.types.TType;
import kz.pompei.glazga.llvm.gen.types.values.IntTypeValue;
import kz.pompei.glazga.llvm.gen.types.values.PtrValue;
import lombok.NonNull;

public class GetElementPtr extends Instruction {
  public GetElementPtr(@NonNull FuncBlock myBlock) {
    super(myBlock);
  }

  public boolean inbounds = false;

  public TType type;

  public PtrValue ptrValue;

  public final List<IntTypeValue> indexes = new ArrayList<>();

  public GetElementPtr ptrValue(PtrValue ptrValue) {
    this.ptrValue = ptrValue;
    return this;
  }

  public GetElementPtr type(TType type) {
    this.type = type;
    return this;
  }

  public GetElementPtr inbounds(boolean inbounds) {
    this.inbounds = inbounds;
    return this;
  }

  public GetElementPtr index(IntTypeValue index) {
    indexes.add(index);
    return this;
  }

  public GetElementPtr varIndex(Integer varIndex) {
    this.varIndex = varIndex;
    return this;
  }

  public GetElementPtr varName(String varName) {
    this.varName = varName;
    return this;
  }

  @Override
  public @NonNull String print() {

    StringBuilder sb = new StringBuilder();

    sb.append("    ").append(varRefName()).append(" = getElementPtr".toLowerCase());

    if (inbounds) {
      sb.append(" inbounds");
    }

    sb.append(" " + type.print());

    sb.append(", " + ptrValue.print());

    for (final IntTypeValue index : indexes) {
      sb.append(", " + index.print());
    }

    return sb.toString();
  }
}
