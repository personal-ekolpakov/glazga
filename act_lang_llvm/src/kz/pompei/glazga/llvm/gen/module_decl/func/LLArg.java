package kz.pompei.glazga.llvm.gen.module_decl.func;

import java.util.function.IntSupplier;
import kz.pompei.glazga.llvm.gen.types.TType;
import lombok.NonNull;

public class LLArg {
  public TType  type;
  public String name;
  public Integer index;

  public final LLArgAttrs<LLArg> attrs = new LLArgAttrs<>(this);

  public @NonNull String print() {

    StringBuilder sb = new StringBuilder();

    if (type == null) {
      throw new RuntimeException("OdObJFnX1Z :: type == null");
    }

    sb.append(type.print());

    attrs.printFirstSpace(sb);

    if (name != null) {
      sb.append(" %").append(name);
    } else if (index != null) {
      sb.append(" %").append(index);
    }

    return sb.toString();
  }

  public LLArg type(TType type) {
    this.type = type;
    return this;
  }

  public LLArg name(String name) {
    this.name = name;
    return this;
  }

  public LLArgAttrs<LLArg> attrs() {
    return attrs;
  }

  public void countIndex(IntSupplier nextIndex) {
    if (name != null) {
      return;
    }
    if (Integer.valueOf(0).equals(index)) {
      index = nextIndex.getAsInt();
    }
  }

  public LLArg index(Integer i) {
    this.index = i;
    return this;
  }

  public String varRefName() {
    if (name != null) {
      return "%" + name;
    }
    if (index != null) {
      return "%" + index;
    }
    throw new RuntimeException("Hf3Vj0bOYK :: VarName does not defined");
  }
}
