package kz.pompei.glazga.llvm.gen.module_decl.func;

import kz.pompei.glazga.llvm.gen.types.TType;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LLArgAttrs<Owner> {
  private final @NonNull Owner up;

  public boolean zeroExt = false;
  public boolean signExt = false;

  public TType byVal;
  public TType byRef;

  public void printFirstSpace(StringBuilder sb) {

    if (zeroExt) {
      sb.append(" zeroExt".toLowerCase());
    }

    if (signExt) {
      sb.append(" signExt".toLowerCase());
    }

    if (byVal != null) {
      sb.append((" byVal(" + byVal.print() + ")").toLowerCase());
    }

    if (byRef != null) {
      sb.append((" byRef(" + byRef.print() + ")").toLowerCase());
    }

  }

  public LLArgAttrs<Owner> zeroExt(boolean zeroExt) {
    this.zeroExt = zeroExt;
    return this;
  }

  public LLArgAttrs<Owner> signExt(boolean signExt) {
    this.signExt = signExt;
    return this;
  }

  public Owner up() {
    return up;
  }

  public LLArgAttrs<Owner> byVal(TType byVal) {
    this.byVal = byVal;
    return this;
  }

  public LLArgAttrs<Owner> byRef(TType byRef) {
    this.byRef = byRef;
    return this;
  }
}
