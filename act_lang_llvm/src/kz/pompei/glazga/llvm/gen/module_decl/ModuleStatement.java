package kz.pompei.glazga.llvm.gen.module_decl;

import kz.pompei.glazga.llvm.gen.ann.LineAccepter;
import lombok.NonNull;

public abstract class ModuleStatement {

  public abstract void print(@NonNull LineAccepter la);

}
