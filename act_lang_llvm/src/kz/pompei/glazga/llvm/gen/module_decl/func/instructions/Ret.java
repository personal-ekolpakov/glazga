package kz.pompei.glazga.llvm.gen.module_decl.func.instructions;

import kz.pompei.glazga.llvm.gen.module_decl.func.FuncBlock;
import kz.pompei.glazga.llvm.gen.types.values.TypeValue;
import lombok.NonNull;

public class Ret extends Instruction {

  public TypeValue ret;

  public Ret(@NonNull FuncBlock myBlock) {
    super(myBlock);
  }

  @SuppressWarnings("UnusedReturnValue")
  public Ret ret(TypeValue ret) {
    this.ret = ret;
    return this;
  }

  @Override
  public @NonNull String print() {
    if (ret == null) {
      return "    ret";
    } else {
      return "    ret " + ret.print();
    }
  }
}
