package kz.pompei.glazga.llvm.gen.types;

import lombok.NonNull;

public abstract class TType {

  public abstract @NonNull String print();

}
