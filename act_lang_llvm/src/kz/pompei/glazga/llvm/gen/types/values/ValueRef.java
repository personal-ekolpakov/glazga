package kz.pompei.glazga.llvm.gen.types.values;

import kz.pompei.glazga.llvm.gen.ann.CanVarRef;
import kz.pompei.glazga.llvm.gen.module_decl.func.instructions.Instruction;

public class ValueRef extends Value {
  public CanVarRef from;

  public static ValueRef of(Instruction from) {
    return new ValueRef().from(from);
  }

  public ValueRef from(Instruction from) {
    this.from = from;
    return this;
  }

  @Override
  public String print() {
    return from.varRefName();
  }
}
