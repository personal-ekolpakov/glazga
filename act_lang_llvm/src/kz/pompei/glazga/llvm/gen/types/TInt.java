package kz.pompei.glazga.llvm.gen.types;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "len")
public class TInt extends TType {
  public final int sizeBits;


  @Override
  public @NonNull String print() {
    return "i" + sizeBits;
  }

}
