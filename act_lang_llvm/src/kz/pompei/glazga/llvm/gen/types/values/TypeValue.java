package kz.pompei.glazga.llvm.gen.types.values;

import kz.pompei.glazga.llvm.gen.types.TType;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class TypeValue {
  public final @NonNull TType type;
  public final @NonNull Value value;

  public String print() {
    return type.print() + " " + value.print();
  }
}
