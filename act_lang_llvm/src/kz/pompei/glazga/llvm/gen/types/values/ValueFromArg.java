package kz.pompei.glazga.llvm.gen.types.values;

import kz.pompei.glazga.llvm.gen.module_decl.func.LLArg;

public class ValueFromArg extends Value {
  public LLArg from;

  @Override
  public String print() {
    return from.varRefName();
  }

  public ValueFromArg from(LLArg from) {
    this.from = from;
    return this;
  }

  public static ValueFromArg of(LLArg arg) {
    return new ValueFromArg().from(arg);
  }
}
