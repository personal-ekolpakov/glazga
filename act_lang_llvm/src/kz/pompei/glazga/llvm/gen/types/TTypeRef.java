package kz.pompei.glazga.llvm.gen.types;

import kz.pompei.glazga.llvm.gen.module_decl.SttTypeDef;
import lombok.NonNull;

public class TTypeRef extends TType {

  public SttTypeDef def;

  public static TTypeRef of(SttTypeDef def) {
    TTypeRef ret = new TTypeRef();
    ret.def = def;
    return ret;
  }

  public TTypeRef def(SttTypeDef def) {
    this.def = def;
    return this;
  }

  @Override
  public @NonNull String print() {
    return def.varRefName();
  }
}
