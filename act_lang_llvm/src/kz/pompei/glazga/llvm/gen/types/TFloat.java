package kz.pompei.glazga.llvm.gen.types;

import kz.pompei.glazga.llvm.gen.meta.FloatType;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class TFloat extends TType {

  private final FloatType floatType;

  @Override
  public @NonNull String print() {
    return floatType.print();
  }
}
