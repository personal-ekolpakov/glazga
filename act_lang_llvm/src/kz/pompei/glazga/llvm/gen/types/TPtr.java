package kz.pompei.glazga.llvm.gen.types;

import lombok.NonNull;


public class TPtr extends TType {

  public TType under;

  private TPtr() {}

  public static TPtr of() {
    return new TPtr();
  }

  public static TPtr of(TType under) {
    return new TPtr().under(under);
  }

  @Override
  public @NonNull String print() {
    return "ptr";
  }


  public TPtr under(TType under) {
    this.under = under;
    return this;
  }
}
