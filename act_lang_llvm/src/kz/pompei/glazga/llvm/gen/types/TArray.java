package kz.pompei.glazga.llvm.gen.types;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class TArray extends TType {
  public final          int   size;
  public final @NonNull TType type;

  @Override
  public @NonNull String print() {
    return "[" + size + " x " + type.print() + "]";
  }
}
