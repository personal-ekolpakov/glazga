package kz.pompei.glazga.llvm.gen.types.values;

public abstract class Value {

  public abstract String print();

}
