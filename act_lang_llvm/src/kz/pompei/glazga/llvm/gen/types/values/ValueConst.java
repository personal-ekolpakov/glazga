package kz.pompei.glazga.llvm.gen.types.values;


import static java.util.Objects.requireNonNull;

public class ValueConst extends Value {
  public String value;

  private ValueConst() {}

  public static ValueConst of(String value) {
    return new ValueConst().value(value);
  }

  public ValueConst value(String value) {
    this.value = value;
    return this;
  }

  @Override
  public String print() {
    requireNonNull(value, "TmvUFCpoRj :: value == null");
    return value;
  }
}
