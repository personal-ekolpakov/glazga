package kz.pompei.glazga.llvm.gen.types.values;

import kz.pompei.glazga.llvm.gen.types.TPtr;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class PtrValue extends Value {
  public final @NonNull TPtr  type;
  public final          Value value;


  @Override
  public String print() {
    return type.print() + " " + value.print();
  }
}
