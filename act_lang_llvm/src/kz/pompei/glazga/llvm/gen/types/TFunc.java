package kz.pompei.glazga.llvm.gen.types;

import java.util.ArrayList;
import java.util.List;
import lombok.NonNull;

public class TFunc extends TType {

  public       TType       ret;
  public final List<TType> args = new ArrayList<>();
  public       boolean     etc  = false;

  public TFunc add(TType argType) {
    args.add(argType);
    return this;
  }

  public TFunc ret(TType ret) {
    this.ret = ret;
    return this;
  }

  public TFunc etc(boolean etc) {
    this.etc = etc;
    return this;
  }

  @Override
  public @NonNull String print() {
    StringBuilder sb = new StringBuilder();
    sb.append(ret.print());
    sb.append(" (");
    boolean first = true;
    for (final TType arg : args) {
      if (first) {
        first = false;
      } else {
        sb.append(", ");
      }
      sb.append(arg.print());
    }
    if (etc) {
      sb.append(", ...");
    }
    sb.append(")");
    return sb.toString();
  }

  public static TFunc of(TType ret) {
    return new TFunc().ret(ret);
  }
}
