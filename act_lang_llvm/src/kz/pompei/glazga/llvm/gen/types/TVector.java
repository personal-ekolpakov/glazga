package kz.pompei.glazga.llvm.gen.types;

import lombok.NonNull;

public class TVector extends TType {

  public int     size;
  public TType   type;
  public boolean scale = false;

  public static TVector of(int size, TType type) {
    return new TVector().size(size).type(type);
  }

  public TVector size(int size) {
    this.size = size;
    return this;
  }

  public TVector type(TType type) {
    this.type = type;
    return this;
  }

  public TVector scale(boolean scale) {
    this.scale = scale;
    return this;
  }

  @Override
  public @NonNull String print() {
    return "<" + (scale ? "vScale x ".toLowerCase() : "") + size + " x " + type.print() + ">";
  }
}
