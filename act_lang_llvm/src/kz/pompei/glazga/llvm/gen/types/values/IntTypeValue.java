package kz.pompei.glazga.llvm.gen.types.values;

import kz.pompei.glazga.llvm.gen.types.TInt;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class IntTypeValue {
  public final @NonNull TInt  type;
  public final @NonNull Value value;

  public String print() {
    return type.print() + " " + value.print();
  }
}
