package kz.pompei.glazga.llvm.gen.types;

import lombok.NonNull;

public class TVoid extends TType {

  @Override
  public @NonNull String print() {
    return "void";
  }

  public static TVoid of() {
    return new TVoid();
  }
}
