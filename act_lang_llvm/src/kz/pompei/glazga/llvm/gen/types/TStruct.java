package kz.pompei.glazga.llvm.gen.types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.NonNull;

import static java.util.stream.Collectors.joining;

public class TStruct extends TType {

  public final List<TType> fields = new ArrayList<>();
  public       boolean     packed = false;

  public static TStruct of(TType... fields) {
    TStruct ret = new TStruct();
    Collections.addAll(ret.fields, fields);
    return ret;
  }

  public TStruct field(@NonNull TType type) {
    fields.add(type);
    return this;
  }

  @Override
  public @NonNull String print() {

    String open  = packed ? "<{ " : "{ ";
    String close = packed ? " }>" : " }";

    return fields.stream().map(TType::print)
                 .collect(joining(", ", open, close));
  }

  public TStruct packed(boolean packed) {
    this.packed = packed;
    return this;
  }
}
