package kz.pompei.glazga.llvm.gen.ann;

import lombok.NonNull;

public interface LineAccepter {
  public void accept(@NonNull String line);
}
