package kz.pompei.glazga.llvm.gen.ann;

import lombok.NonNull;

public interface CanVarRef {

  @NonNull String varRefName();

  void countIndexes();

}
