package kz.pompei.glazga.llvm.gen;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.llvm.gen.ann.CanVarRef;
import kz.pompei.glazga.llvm.gen.ann.LineAccepter;
import kz.pompei.glazga.llvm.gen.meta.GlobalVarKind;
import kz.pompei.glazga.llvm.gen.module_decl.DeclareFunc;
import kz.pompei.glazga.llvm.gen.module_decl.DefineFunc;
import kz.pompei.glazga.llvm.gen.module_decl.ModuleStatement;
import kz.pompei.glazga.llvm.gen.module_decl.SttDeclareVar;
import kz.pompei.glazga.llvm.gen.module_decl.SttTypeDef;
import kz.pompei.glazga.utils.IsInstance;
import lombok.NonNull;

public class LLModule {

  public final List<ModuleStatement> sttList = new ArrayList<>();

  private int nextIndex = 1;

  public int nextIndex() {
    return nextIndex++;
  }

  public @NonNull SttDeclareVar newGlobalVarDecl() {
    SttDeclareVar ret = new SttDeclareVar(this);
    sttList.add(ret);
    return ret;
  }

  public @NonNull SttDeclareVar newConstant() {
    return newGlobalVarDecl().kind(GlobalVarKind.Constant);
  }

  public @NonNull SttDeclareVar newGlobal() {
    return newGlobalVarDecl().kind(GlobalVarKind.Global);
  }

  public @NonNull DefineFunc define() {
    DefineFunc ret = new DefineFunc(this);
    sttList.add(ret);
    return ret;
  }

  public @NonNull DeclareFunc declare() {
    DeclareFunc ret = new DeclareFunc(this);
    sttList.add(ret);
    return ret;
  }

  public @NonNull SttTypeDef newTypeDef() {
    SttTypeDef ret = new SttTypeDef(this);
    sttList.add(ret);
    return ret;
  }

  public void countIndexes() {
    sttList.stream().flatMap(IsInstance.on(CanVarRef.class)::of).forEach(CanVarRef::countIndexes);
  }

  public void print(@NonNull LineAccepter accepter) {
    sttList.forEach(statement -> statement.print(accepter));
  }
}
