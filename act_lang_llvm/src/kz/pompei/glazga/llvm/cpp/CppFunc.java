package kz.pompei.glazga.llvm.cpp;

public enum CppFunc {
  /**
   * typedef void (*void_void_type)();
   */
  void_void,

  /**
   * typedef const char* (*constCharRef_constCharRef_type)(const char*);
   */
  constCharRef_charRef,

}
