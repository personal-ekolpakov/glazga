package kz.pompei.glazga.llvm.cpp;

public class InnerError extends RuntimeException {
  public InnerError(String message) {
    super(message);
  }
}
