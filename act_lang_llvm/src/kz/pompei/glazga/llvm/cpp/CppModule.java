package kz.pompei.glazga.llvm.cpp;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import kz.pompei.glazga.env.Modules;

public class CppModule {

  public native String concatStrings(String string1, String string2);

  private native void openLibrary0(String absolutePathToSoFile);

  public native void closeLibrary();

  private native void assignFunc_void_void(String funcName);

  private native void assignFunc_constCharRef_charRef(String funcName);

  public native void call__void_void();

  public native String call__constCharRef_constCharRef(String input);

  public native void callInnerError(String errorText);

  @SuppressWarnings("unused")
  private void innerError(String errorText) {
    throw new InnerError(errorText);
  }

  public void openLibrary(Path soFilePath, Map<CppFunc, String> linkageMap) {

    if (!Files.exists(soFilePath)) {
      throw new RuntimeException("8u6j0AUb5j :: No file " + soFilePath);
    }

    System.out.println("lFLUqiKD1Z :: Open Library " + soFilePath);

    openLibrary0(soFilePath.toAbsolutePath().normalize().toString());

    for (final Map.Entry<CppFunc, String> e : linkageMap.entrySet()) {
      CppFunc cppFunc  = e.getKey();
      String  funcName = e.getValue();

      switch (cppFunc) {
        case void_void -> assignFunc_void_void(funcName);
        case constCharRef_charRef -> assignFunc_constCharRef_charRef(funcName);
        default -> throw new RuntimeException("wrQRhBWZgZ :: Unknown cppFunc = " + cppFunc);
      }

    }

  }

  static {
    //noinspection SpellCheckingInspection
    String libPath = Modules.prjRoot().resolve("act_lang_llvm")
                            .resolve("build").resolve("native").resolve("libglazga.so")
                            .toAbsolutePath().normalize().toString();

    System.load(libPath);
  }

}
