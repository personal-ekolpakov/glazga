package kz.pompei.glazga.llvm.util;

public class StrPrinter {

  public static String toHex(int value, int length) {

    final String result       = Integer.toUnsignedString(value, 16);
    final int    resultLength = result.length();

    return resultLength >= length
      ? result
      : "0".repeat(length - resultLength) + result.toUpperCase();

  }

  public static String printStr(String str) {
    if (str == null) {
      return "null";
    }

    StringBuilder sb = new StringBuilder();

    int len = str.length();
    for (int i = 0; i < len; i++) {
      char c = str.charAt(i);
      if (0 <= c && c < 32) {
        sb.append('\\').append(toHex(c, 2));
        continue;
      }

      if ('"' == c) {
        sb.append('\\').append('"');
        continue;
      }

      sb.append(c);
    }

    return '"' + sb.toString() + '"';

  }
}
