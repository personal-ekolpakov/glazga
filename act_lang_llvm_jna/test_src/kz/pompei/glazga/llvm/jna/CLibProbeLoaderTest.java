package kz.pompei.glazga.llvm.jna;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CLibProbeLoaderTest {

  @Test
  public void name() {
    CLibProbeLoader.get().helloWorld();

    CLibProbe probe = CLibProbeLoader.get();

    String result = probe.processStr("Booms in status");

    System.out.println("9Y1TZOilYY :: Result = " + result);

    assertThat(1);
  }

}
