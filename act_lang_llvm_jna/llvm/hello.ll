@str = private constant [55 x i8] c"Привет мир - полёт навигатора\0A\00", align 1

@process_pattern = private constant [14 x i8] c"Processes %s\0A\00", align 1
@output_buffer = global [256 x i8] zeroinitializer, align 1

define void @helloWorld() {
entry:
  ; Строка для вывода


  ; Вызов printf для вывода строки
  %str_ptr = getelementptr [55 x i8], [55 x i8]* @str, i32 0, i32 0
  call i32 (i8*, ...) @printf(i8* %str_ptr)
  ret void
}

define i8* @processStr(i8* %input) {
entry:

  %buffer = getelementptr inbounds [256 x i8], [256 x i8]* @output_buffer, i64 0, i64 0

  %format = getelementptr inbounds [14 x i8], [14 x i8]* @process_pattern, i32 0, i32 0

  call i32 (i8*, i8*, ...) @sprintf(i8* %buffer, i8* %format, i8* %input)

  ret i8* %buffer

}

declare i32 @printf(i8*, ...)
declare i32 @sprintf(i8*, i8*, ...)
