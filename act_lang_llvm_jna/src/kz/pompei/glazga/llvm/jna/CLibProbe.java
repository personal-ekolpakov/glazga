package kz.pompei.glazga.llvm.jna;

import com.sun.jna.Library;

public interface CLibProbe extends Library {

  void helloWorld();

  String processStr(String str);

}
