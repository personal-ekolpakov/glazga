package kz.pompei.glazga.llvm.jna;

import com.sun.jna.Native;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicReference;
import kz.pompei.glazga.env.Modules;

public class CLibProbeLoader {
  private static final AtomicReference<CLibProbe> LIB = new AtomicReference<>(null);

  public static CLibProbe get() {

    {
      CLibProbe lib = LIB.get();
      if (lib != null) {
        return lib;
      }
    }

    synchronized (LIB) {
      {
        CLibProbe lib = LIB.get();
        if (lib != null) {
          return lib;
        }
      }

      {
        Path root = Modules.prjRoot().resolve("act_lang_llvm_jna").resolve("llvm");

        ProcessBuilder builder = new ProcessBuilder();

        builder.redirectError(ProcessBuilder.Redirect.INHERIT)
               .redirectOutput(ProcessBuilder.Redirect.INHERIT)
               .directory(root.toFile())
               .command("/usr/bin/make");


        try {
          int exitCode = builder.start().waitFor();
          if (exitCode != 0) {
            throw new RuntimeException("psyWTE4Ng5 :: Ошибка сборки. Код выхода " + exitCode);
          }
        } catch (InterruptedException | IOException e) {
          throw new RuntimeException("nTAIjiMK1K :: Error in make", e);
        }

        String absLibSo = root.resolve("build").resolve("libhello.so").toFile().getAbsolutePath();

        CLibProbe lib = Native.load(absLibSo, CLibProbe.class);
        LIB.set(lib);
        return lib;
      }
    }
  }

}
