package kz.pompei.glazga.mat;

import java.awt.Point;
import java.awt.geom.AffineTransform;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

/**
 * Преобразует координаты по формулам:
 * <pre>
 *   x' = Kx * x + Bx
 *   y' = Ky * y + By
 * </pre>
 * <p>
 * Если K больше 1, то масштаб приближается (элементы становятся больше).
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class LinearTrans {
  public final double Kx, Ky;
  public final double Bx, By;

  public static LinearTrans one() {
    return new LinearTrans(1, 1, 0, 0);
  }

  public static LinearTrans of(double Kx, double Ky, double Bx, double By) {
    return new LinearTrans(Kx, Ky, Bx, By);
  }

  /**
   * Creates <code>LinearTrans<code> witch converts <code>(in_x, in_y)</code> to <code>(out_x, out_y)</code> by formula:
   * <pre>
   *   (in_x1 ... in_x2) -> (out_x1 ... out_x2)
   *
   *   (in_y1 ... in_y2) -> (out_y1 ... out_y2)
   * </pre>
   * Where <code>in_x</code> from diapason <code>(in_x1 ... in_x2)</code>
   * to be converted to <code>out_x</code> in diapason <code>(out_x1 ... out_x2)</code>
   * in the same proportion:
   * <pre>
   *   in_x  - in_x1     out_x  - out_x1
   *  --------------- = -----------------
   *   in_x2 - in_x1     out_x2 - out_x1
   * </pre>
   * <p>
   * Same with coordinate <code>y</code>.
   */
  public static LinearTrans diapasonTrans(
    double in_x1, double in_x2, double out_x1, double out_x2,
    double in_y1, double in_y2, double out_y1, double out_y2
  ) {
    double Kx = (out_x2 - out_x1) / (in_x2 - in_x1);
    double Bx = Math.abs(Kx) > 1 ? out_x1 - Kx * in_x1 : out_x2 - Kx * in_x2;
    double Ky = (out_y2 - out_y1) / (in_y2 - in_y1);
    double By = Math.abs(Ky) > 1 ? out_y1 - Ky * in_y1 : out_y2 - Ky * in_y2;

    return LinearTrans.of(Kx, Ky, Bx, By);
  }

  public LinearTrans combine(LinearTrans T) {
    double Kx = this.Kx * T.Kx;
    double Bx = this.Bx + this.Kx * T.Bx;
    double Ky = this.Ky * T.Ky;
    double By = this.By + this.Ky * T.By;
    return LinearTrans.of(Kx, Ky, Bx, By);
  }

  public static LinearTrans diapasonTrans(Rect in, Rect out) {
    return LinearTrans.diapasonTrans(
      in.left, in.right, out.left, out.right,
      in.top, in.bottom, out.top, out.bottom
    );
  }

  public double convX(double x) {
    return Kx * x + Bx;
  }

  public double convY(double y) {
    return Ky * y + By;
  }

  public Vec conv(Vec position) {
    return Vec.of(convX(position.x), convY(position.y));
  }

  public double backX(double x) {
    return (x - Bx) / Kx;
  }

  public double backY(double y) {
    return (y - By) / Ky;
  }

  public Vec back(Vec positionBack) {
    return Vec.of(backX(positionBack.x), backY(positionBack.y));
  }

  public Vec back(Point point) {
    return back(Vec.fromPoint(point));
  }

  public double scaleX(double distX) {
    return Kx * distX;
  }

  public double scaleY(double distY) {
    return Ky * distY;
  }

  public LinearTrans scaleOverPoint(Vec point, double scaleX, double scaleY) {
    double newBx = Bx * scaleX - point.x * (scaleX - 1);
    double newBy = By * scaleY - point.y * (scaleY - 1);
    return new LinearTrans(Kx * scaleX, Ky * scaleY, newBx, newBy);
  }

  public LinearTrans scaleOverPoint(Vec point, double scale) {
    return scaleOverPoint(point, scale, scale);
  }

  @Override
  public String toString() {
    return "LinearTrans{K=(" + Kx + ", " + Ky + "), B=(" + Bx + ", " + By + ")}";
  }

  public LinearTrans Kx(double Kx) {
    return new LinearTrans(Kx, Ky, Bx, By);
  }

  public LinearTrans Ky(double Ky) {
    return new LinearTrans(Kx, Ky, Bx, By);
  }

  public LinearTrans Bx(double Bx) {
    return new LinearTrans(Kx, Ky, Bx, By);
  }

  public LinearTrans By(double By) {
    return new LinearTrans(Kx, Ky, Bx, By);
  }

  public double convAVG(double width) {
    return width * (Kx + Ky) / 2;
  }

  public AffineTransform toAffineTransform() {
    return new AffineTransform(Kx, 0, 0, Ky, Bx, By);
  }

}
