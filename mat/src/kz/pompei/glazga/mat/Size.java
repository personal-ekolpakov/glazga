package kz.pompei.glazga.mat;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import lombok.experimental.FieldNameConstants;

@SuppressWarnings("ClassCanBeRecord")
@FieldNameConstants
public class Size implements Serializable {
  public final double width;
  public final double height;

  @ConstructorProperties({"x", "y"})
  public Size(double width, double height) {
    this.width  = width;
    this.height = height;
  }

  public static Size of(double width, double height) {
    return new Size(width, height);
  }

  @Override
  public String toString() {
    return "Size{" + width + ", " + height + "}";
  }

  public int widthInt() {
    return (int) Math.round(width);
  }

  public int heightInt() {
    return (int) Math.round(height);
  }

  public static Size over(double width, ExprHeight height) {
    return Size.of(width, height.total());
  }
}
