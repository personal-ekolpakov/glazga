package kz.pompei.glazga.mat;

import lombok.AllArgsConstructor;

@AllArgsConstructor(staticName = "of")
public class SideSpaces {
  public final double left;
  public final double top;
  public final double right;
  public final double bottom;
}
