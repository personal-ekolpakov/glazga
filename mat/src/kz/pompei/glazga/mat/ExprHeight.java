package kz.pompei.glazga.mat;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@RequiredArgsConstructor(staticName = "of")
public class ExprHeight {
  public final double up;
  public final double down;

  public double total() {
    return up + down;
  }

  public ExprHeight combine(@NonNull ExprHeight right) {
    return ExprHeight.of(Math.max(up, right.up), Math.max(down, right.down));
  }

  private static final ExprHeight ZERO = ExprHeight.of(0, 0);

  public static ExprHeight zero() {
    return ZERO;
  }

  public ExprHeight expand(SideSpaces padding) {
    return ExprHeight.of(up + padding.top, down + padding.bottom);
  }

  public ExprHeight upMul(double mul) {
    return ExprHeight.of(up * mul, down);
  }
}
