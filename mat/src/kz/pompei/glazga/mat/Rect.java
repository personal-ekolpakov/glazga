package kz.pompei.glazga.mat;

import java.awt.geom.Rectangle2D;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode
@RequiredArgsConstructor(staticName = "of")
public class Rect {
  public final double left;
  public final double top;
  public final double right;
  public final double bottom;

  @Override
  public String toString() {
    return "Rect{" +
      "left=" + left +
      ", top=" + top +
      ", right=" + right +
      ", bottom=" + bottom +
      '}';
  }

  public static Rect from(Vec point) {
    return Rect.of(point.x, point.y, point.x, point.y);
  }

  public static Rect from(Rectangle2D r) {
    double left   = r.getX();
    double top    = r.getY();
    double right  = left + r.getWidth();
    double bottom = top + r.getHeight();
    return Rect.of(left, top, right, bottom);
  }

  public static Rect dia(Vec p1, Vec p2) {
    return Rect.of(p1.x, p1.y, p2.x, p2.y);
  }

  public Rect to(Size size) {
    return Rect.of(left, top, left + size.width, top + size.height);
  }

  public Vec leftTop() {
    return Vec.of(left, top);
  }

  public Vec rightTop() {
    return Vec.of(right, top);
  }

  public Vec rightBottom() {
    return Vec.of(right, bottom);
  }

  public Vec leftBottom() {
    return Vec.of(left, bottom);
  }

  public static Rect over(Vec leftBaseLine, double width, ExprHeight height) {
    return Rect.dia(leftBaseLine.up(height.up), leftBaseLine.down(height.down).right(width));
  }

  public static Rect over(Vec leftTop, Size size) {
    return Rect.dia(leftTop, leftTop.right(size.width).down(size.height));
  }

  public Rect norm() {
    return Rect.of(Math.min(left, right), Math.min(top, bottom), Math.max(left, right), Math.max(top, bottom));
  }

  public Rect swapX() {
    return Rect.of(right, top, left, bottom);
  }

  public Rect swapY() {
    return Rect.of(left, bottom, right, top);
  }

  public Rect union(Rect a) {
    if (a == null) {
      return this;
    }

    double left   = Math.min(this.left, a.left);
    double right  = Math.max(this.right, a.right);
    double top    = Math.min(this.top, a.top);
    double bottom = Math.max(this.bottom, a.bottom);

    return Rect.of(left, top, right, bottom);
  }

  public double width() {
    return right - left;
  }

  public double height() {
    return bottom - top;
  }

  public Rect rightTopMoveTo(Vec pos) {
    double width  = width();
    double height = height();

    double left   = pos.x - width;
    double top    = pos.y;
    double right  = pos.x;
    double bottom = pos.y + height;

    return Rect.of(left, top, right, bottom);
  }

  /**
   * Изменяет высоту прямоугольника так, чтобы отношение высоты к ширине не изменилось.
   * <p>
   * Верхняя левая вершина обоих прямоугольников будет совпадать геометрически.
   *
   * @param newHeight высота возвращаемого прямоугольника
   * @return прямоугольник с изменённой высотой (ну и шириной тоже).
   */
  public Rect proportionalSetHeight(double newHeight) {

    double oldHeight = height();
    double oldWidth  = width();

    if (oldHeight == 0) {
      return Rect.of(left, top, right, top + newHeight);
    }

    double newWidth = oldWidth / oldHeight * newHeight;

    return Rect.of(left, top, left + newWidth, top + newHeight);
  }

  public Rect to(double newWidth, double newHeight) {
    return Rect.of(left, top, left + newWidth, top + newHeight);
  }

  public Rect expand(SideSpaces ss) {
    return Rect.of(left - ss.left, top - ss.top, right + ss.right, bottom + ss.bottom);
  }

  public Vec center() {
    return Vec.of((left + right) / 2, (top + bottom) / 2);
  }

  public Rect move(Vec delta) {
    return Rect.of(left + delta.x, top + delta.y, right + delta.x, bottom + delta.y);
  }

  public Rect rightWidth(double width) {
    return Rect.of(left, top, right + width, bottom);
  }

  public Rect setLeftTop(double left, double top) {
    double width  = width();
    double height = height();

    return Rect.of(left, top, left + width, top + height);
  }

  public Size size() {
    return Size.of(width(), height());
  }

  public boolean containsVec(@NonNull Vec pos) {
    return hitsLine(pos.x, left, right) && hitsLine(pos.y, top, bottom);
  }

  private static boolean hitsLine(double t, double t1, double t2) {
    return t1 < t2 ? (t1 <= t && t <= t2) : (t2 <= t && t <= t1);
  }

}
