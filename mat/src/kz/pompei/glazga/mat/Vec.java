package kz.pompei.glazga.mat;

import java.awt.Point;
import java.beans.ConstructorProperties;
import java.io.Serializable;
import kz.pompei.glazga.ann.ConvertToStr;
import kz.pompei.glazga.ann.FromString;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldNameConstants;

@EqualsAndHashCode
@FieldNameConstants
@SuppressWarnings("ClassCanBeRecord")
public class Vec implements Serializable {
  public final double x;
  public final double y;

  @ConstructorProperties({"x", "y"})
  public Vec(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public Vec copy() {
    return Vec.of(x, y);
  }

  public static Vec of(double left, double top) {
    return new Vec(left, top);
  }

  public static Vec fromPoint(Point point) {
    return Vec.of(point.x, point.y);
  }

  public Vec plus(Vec a) {
    if (a == null) {
      return this;
    }
    return Vec.of(this.x + a.x, this.y + a.y);
  }

  public Vec plus(double x, double y) {
    return Vec.of(this.x + x, this.y + y);
  }

  public Vec up(double value) {
    return plus(0, -value);
  }

  public Vec down(double value) {
    return plus(0, +value);
  }

  public Vec right(double value) {
    return plus(+value, 0);
  }

  public Vec left(double value) {
    return plus(-value, 0);
  }

  public Vec minus(double x, double y) {
    return Vec.of(this.x - x, this.y - y);
  }

  public Vec minus(Vec a) {
    if (a == null) {
      return this;
    }
    return Vec.of(this.x - a.x, this.y - a.y);
  }

  @Override
  public String toString() {
    return "VEC(" + x + ", " + y + ")";
  }

  public int xInt() {
    return (int) Math.round(x);
  }

  public int yInt() {
    return (int) Math.round(y);
  }

  public Point toPoint() {
    return new Point(xInt(), yInt());
  }

  public static Vec zero() {
    return Vec.of(0, 0);
  }

  @FromString
  public static Vec strToVec(String str) {
    if (str == null) {
      return null;
    }

    String[] split = str.split(";");
    if (split.length < 2) {
      return null;
    }

    try {
      return Vec.of(Double.parseDouble(split[0]), Double.parseDouble(split[1]));
    } catch (NumberFormatException ignore) {
      return null;
    }
  }

  @ConvertToStr
  public String vecToStr() {
    return "" + x + ";" + y;
  }
}
