package kz.pompei.glazga.mat;

import kz.greetgo.util.RND;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class VecTest {


  @Test
  public void minus() {

    assertThat(Vec.of(10, 20).minus(Vec.of(5, 7))).isEqualTo(Vec.of(5, 13));

  }

  @Test
  public void vecToStr__strToVec() {

    Vec origin = Vec.of(-RND.plusDouble(10d, 5), RND.plusDouble(100_000_000d, 5));

    //
    //
    String str = origin.vecToStr();
    //
    //

    System.out.println("CFSmRjbXTp :: str = " + str);

    //
    //
    Vec actual = Vec.strToVec(str);
    //
    //

    assertThat(actual.x).isEqualTo(origin.x);
    assertThat(actual.y).isEqualTo(origin.y);
    assertThat(actual).isEqualTo(origin);
  }

  @SuppressWarnings("ConstantValue")
  @Test
  public void strToVec__NULL() {

    //
    //
    Vec actual = Vec.strToVec(null);
    //
    //

    assertThat(actual).isNull();
  }

  @Test
  public void strToVec__IllegalStr() {

    //
    //
    Vec actual = Vec.strToVec("test");
    //
    //

    assertThat(actual).isNull();
  }

  @Test
  public void strToVec__IllegalDouble() {

    //
    //
    Vec actual = Vec.strToVec("test;test2");
    //
    //

    assertThat(actual).isNull();
  }
}
