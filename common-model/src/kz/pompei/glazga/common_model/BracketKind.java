package kz.pompei.glazga.common_model;

public enum BracketKind {
  ROUND,
  SQUARE,
  BRACES,
}
