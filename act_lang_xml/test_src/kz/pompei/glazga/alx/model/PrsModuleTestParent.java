package kz.pompei.glazga.alx.model;

import kz.pompei.glazga.alx.model.mod.detail.operation.Body;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprVarRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttVar;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class PrsModuleTestParent {

  protected void assertVarRef(ExprHolder expr, String varName) {
    assertThat(expr).describedAs("NxQ6Pev1kt :: expr is null").isNotNull();
    assertThat(expr.taken).isInstanceOf(ExprVarRef.class);

    var ref = (ExprVarRef) expr.taken;
    assertThat(ref.name).isEqualTo(varName);
  }

  protected void assertFirstVar(Body body, int i, String varName) {
    assertThat(body.statements.size()).isGreaterThan(i);
    assertThat(body.statements.get(i)).isInstanceOf(SttVar.class);
    var x = (SttVar) body.statements.get(i);
    assertThat(x.name).isEqualTo(varName);
  }

}
