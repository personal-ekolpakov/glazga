package kz.pompei.glazga.alx.model;

import kz.pompei.glazga.alx.model.mod.DetailType;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.OperationArg;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprArgRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttAssign;
import kz.pompei.glazga.alx.model.mod.detail.type_ref.TypeRef_ME;
import kz.pompei.glazga.alx.model.mod.natives.AlxNative;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static kz.pompei.glazga.utils.FileUtils.strToInputStream;
import static org.assertj.core.api.Assertions.assertThat;

public class AlxModuleValidateTest {

  @DataProvider
  private Object[][] trueFalseDataProvider() {
    return new Object[][]{{true}, {false}};
  }

  @Test(dataProvider = "trueFalseDataProvider")
  public void calculateOffsetInMemory__localSizeGet(boolean firstTotalSize) {

    // language=XML
    String detailXml = """
        <type name="Целое 4 байта" id="xx">
        <memory>
          <space size="5 bytes"/>
          <space size="9 bytes"/>
          <space size="4 bytes"/>
        </memory>
      </type>
        """;

    AlxModule module = new AlxModule();

    module.parse(strToInputStream(detailXml), "detail_int.xml");

    DetailType detail = (DetailType) module.details.get("xx");

    if (firstTotalSize) {
      assertThat(detail.memory.sizeBytes()).isEqualTo(24);
    }

    assertThat(detail.memory.spaces.get(0).offsetBytes()).isEqualTo(0);
    assertThat(detail.memory.spaces.get(1).offsetBytes()).isEqualTo(8);
    assertThat(detail.memory.spaces.get(2).offsetBytes()).isEqualTo(20);

    assertThat(detail.memory.sizeBytes()).isEqualTo(24);

  }

  @Test(dataProvider = "trueFalseDataProvider")
  public void calculateOffsetInMemory__ReferenceToType(boolean firstTotalSize) {

    // language=XML
    String anotherType1 = """
      <type name="Другой тип 1" id="anotherType1_id">
        <memory>
          <space size="1 bytes"/>
          <space size="1 bytes"/>
          <space size="1 bytes"/>
        </memory>
      </type>
        """;

    // language=XML
    String anotherType2 = """
      <type name="Другой тип" id="anotherType2_id">
        <import id="ANOTHER1" name="Другой тип 1" where="MY_MODULE" ref-id="anotherType1_id"/>
        <memory>
          <space size="1 bytes"/>
          <space size-by-type-ref="#ANOTHER1"/>
          <space size="1 bytes"/>
        </memory>
      </type>
        """;

    // language=XML
    String typeXml = """
      <type name="Целое 4 байта" id="xx">
        <import id="ANOTHER1" name="Другой тип 1" where="MY_MODULE" ref-id="anotherType1_id"/>
        <import id="ANOTHER2" name="Другой тип 2" where="MY_MODULE" ref-id="anotherType2_id"/>
        <memory>
          <space size-by-type-ref="#ANOTHER1"/>
          <space size="9 bytes"/>
          <space size-by-type-ref="#ANOTHER2"/>
        </memory>
      </type>
        """;

    AlxModule module = new AlxModule();

    module.parse(strToInputStream(anotherType1), "another_type1.xml");
    module.parse(strToInputStream(anotherType2), "another_type2.xml");
    module.parse(strToInputStream(typeXml), "type.xml");

    DetailType detail = (DetailType) module.details.get("xx");

    if (firstTotalSize) {
      assertThat(detail.memory.sizeBytes()).isEqualTo(44);
    }

    assertThat(detail.memory.spaces.get(0).offsetBytes()).isEqualTo(0);
    assertThat(detail.memory.spaces.get(1).offsetBytes()).isEqualTo(12);
    assertThat(detail.memory.spaces.get(2).offsetBytes()).isEqualTo(24);

    assertThat(detail.memory.sizeBytes()).isEqualTo(44);
  }

  @Test
  public void operations() {
    // language=XML
    String natives = """
      <natives>
            
      <NATIVE id="PLUS INT 4 bytes">
        <input1 dir="IN" size="4 bytes"/>
        <input2 dir="IN" size="4 bytes"/>
        <result dir="OUT" size="4 bytes"/>
      </NATIVE>
            
      </natives>
      """;

    // language=XML
    String detail_int = """
        <type name="Целое 4 байта" id="F285tsP2so">
          
        <memory>
          <space size="4 bytes"/>
        </memory>
          
        <operation id="M22iRMgYO7" name="PLUS">
          <arg name="left" type-ref="ME" dir="IN"/>
          <arg name="right" type-ref="ME" dir="IN"/>
          <arg name="result" type-ref="ME" dir="OUT"/>
          
          <body>
            <assign>
              <target>
                <arg-ref name="result"/>
              </target>
              <source>
                <NATIVE-REF id="PLUS INT 4 bytes" out="result">
                  <arg id="input1">
                    <arg-ref name="left"/>
                  </arg>
                  <arg id="input2">
                    <arg-ref name="right"/>
                  </arg>
                </NATIVE-REF>
              </source>
            </assign>
          </body>
        </operation>
          
      </type>
        """;

    AlxModule module = new AlxModule();

    module.parse(strToInputStream(natives), "natives.xml");
    module.parse(strToInputStream(detail_int), "detail_int.xml");

    AlxNative nativePlus = module.natives.map.get("PLUS INT 4 bytes");
    assertThat(nativePlus).isNotNull();


    DetailType typeInt = (DetailType) module.details.get("F285tsP2so");

    Operation opPlus = typeInt.operations.get("M22iRMgYO7");

    OperationArg arg_left   = opPlus.args.get("left");
    OperationArg arg_right  = opPlus.args.get("right");
    OperationArg arg_result = opPlus.args.get("result");

    assertThat(arg_left.typeRef().target()).isSameAs(typeInt);
    assertThat(arg_right.typeRef().target()).isSameAs(typeInt);
    assertThat(arg_result.typeRef().target()).isSameAs(typeInt);

    assertThat(arg_left.typeRef()).isInstanceOf(TypeRef_ME.class);
    assertThat(arg_right.typeRef()).isInstanceOf(TypeRef_ME.class);
    assertThat(arg_result.typeRef()).isInstanceOf(TypeRef_ME.class);

    SttAssign assign = (SttAssign) opPlus.body.statements.get(0);

    ExprArgRef refTo_argResult = (ExprArgRef) assign.target.taken;

    assertThat(refTo_argResult.target()).isSameAs(arg_result);

  }
}
