package kz.pompei.glazga.alx.model;

import java.util.Map;
import kz.pompei.glazga.alx.model.ann.ArgDir;
import kz.pompei.glazga.alx.model.mod.Detail;
import kz.pompei.glazga.alx.model.mod.DetailType;
import kz.pompei.glazga.alx.model.mod.detail.AlxMemory;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.OperationArg;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprArgRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprCall;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprConstant;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprNativeRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprSpaceRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttAssign;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttBlock;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttIf;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttReturn;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttVar;
import kz.pompei.glazga.alx.model.mod.natives.AlxNative;
import kz.pompei.glazga.alx.model.mod.natives.AlxNativeArg;
import org.testng.annotations.Test;

import static kz.pompei.glazga.utils.FileUtils.strToInputStream;
import static org.assertj.core.api.Assertions.assertThat;

public class AlxModuleTest extends PrsModuleTestParent {

  @Test
  public void parse___NATIVE() {

    // language=XML
    String xml = """
      <natives>
            
      <NATIVE id="MINUS FLOAT 4 bytes">
        <input1 dir="IN" size="41 bytes"/>
        <input2 dir="IN" size="42 bytes"/>
        <result dir="OUT" size="43 bytes"/>
      </NATIVE>
            
      </natives>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.natives).isNotNull();

    Map<String, AlxNative> natives = module.natives.map;
    assertThat(natives).hasSize(1);

    AlxNative nat = natives.get("MINUS FLOAT 4 bytes");

    assertThat(nat.id).isEqualTo("MINUS FLOAT 4 bytes");
    assertThat(nat.args).hasSize(3);

    AlxNativeArg arg1 = nat.args.get("input1");
    AlxNativeArg arg2 = nat.args.get("input2");
    AlxNativeArg arg3 = nat.args.get("result");

    assertThat(arg1.id).isEqualTo("input1");
    assertThat(arg2.id).isEqualTo("input2");
    assertThat(arg3.id).isEqualTo("result");

    assertThat(arg1.order).isEqualTo(1);
    assertThat(arg2.order).isEqualTo(2);
    assertThat(arg3.order).isEqualTo(3);

    assertThat(arg1.sizeBytes).isEqualTo(41L);
    assertThat(arg2.sizeBytes).isEqualTo(42L);
    assertThat(arg3.sizeBytes).isEqualTo(43L);

    assertThat(arg1.dir).isEqualTo(ArgDir.IN);
    assertThat(arg2.dir).isEqualTo(ArgDir.IN);
    assertThat(arg3.dir).isEqualTo(ArgDir.OUT);


    assertThat(nat.pos.line).isEqualTo(3);
    assertThat(arg1.pos.line).isEqualTo(4);
    assertThat(arg2.pos.line).isEqualTo(5);
    assertThat(arg3.pos.line).isEqualTo(6);
  }

  @Test
  public void parse___operation__ExprVarRef() {

    // language=XML
    String xml = """
      <type name="Тестовое название типа" id="RUvd7ehZtL">
      <operation id="5aSkl39sKz" name="TEST_OP">
          <arg name="left" type-ref="ME" dir="IN"/>
          <arg name="right" type-ref="#FLOAT" dir="IN"/>
          <arg name="result" type-ref="ME" dir="OUT"/>
            
          <body>
            <var name="test-var" type-ref="#TEST_TYPE_LOCAL_ID">
              <var-ref name="test-var-ref-name"/>
            </var>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    Detail detail = module.details.get("RUvd7ehZtL");

    assertThat(detail).isInstanceOf(DetailType.class);
    assertThat(detail.id()).isEqualTo("RUvd7ehZtL");

    var alxType = (DetailType) detail;

    assertThat(alxType.name).isEqualTo("Тестовое название типа");

    Operation op = alxType.operations.get("5aSkl39sKz");
    assertThat(op).isNotNull();

    assertThat(op.id).isEqualTo("5aSkl39sKz");
    assertThat(op.name).isEqualTo("TEST_OP");

    assertThat(op.args).hasSize(3);

    OperationArg arg1 = op.args.get("left");
    OperationArg arg2 = op.args.get("right");
    OperationArg arg3 = op.args.get("result");

    assertThat(arg1.name).isEqualTo("left");
    assertThat(arg2.name).isEqualTo("right");
    assertThat(arg3.name).isEqualTo("result");

    assertThat(arg1.__typeRef).isEqualTo("ME");
    assertThat(arg2.__typeRef).isEqualTo("#FLOAT");
    assertThat(arg3.__typeRef).isEqualTo("ME");

    assertThat(arg1.dir).isEqualTo(ArgDir.IN);
    assertThat(arg2.dir).isEqualTo(ArgDir.IN);
    assertThat(arg3.dir).isEqualTo(ArgDir.OUT);

    assertThat(op.body).isNotNull();
    assertThat(op.body.statements).hasSize(1);

    var var = (SttVar) op.body.statements.get(0);

    assertThat(var.name).isEqualTo("test-var");
    assertThat(var.typeRef).isEqualTo("#TEST_TYPE_LOCAL_ID");

    assertVarRef(var.expr, "test-var-ref-name");

    assertThat(op.pos.line).isEqualTo(2);
    assertThat(arg1.pos.line).isEqualTo(3);
    assertThat(arg2.pos.line).isEqualTo(4);
    assertThat(arg3.pos.line).isEqualTo(5);

    assertThat(var.pos.line).isEqualTo(8);
    assertThat(var.expr.taken.pos.line).isEqualTo(9);
  }

  @Test
  public void parse___operation__ExprArgRef() {

    // language=XML
    String xml = """
      <type name="Тестовое название типа" id="RUvd7ehZtL">
      <operation id="5aSkl39sKz" name="TEST_OP">
          <body>
            <var name="test-var" type-ref="#TEST_TYPE_LOCAL_ID">
              <arg-ref name="test-arg-ref-name"/>
            </var>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    Detail     detail  = module.details.get("RUvd7ehZtL");
    DetailType alxType = (DetailType) detail;
    Operation  op      = alxType.operations.get("5aSkl39sKz");
    SttVar     var     = (SttVar) op.body.statements.get(0);

    assertThat(var.expr.taken).isInstanceOf(ExprArgRef.class);

    ExprArgRef argRef = (ExprArgRef) var.expr.taken;

    assertThat(argRef.name).isEqualTo("test-arg-ref-name");
    assertThat(argRef.pos.line).isEqualTo(5);
  }

  @Test
  public void parse___operation__ExprCall() {

    // language=XML
    String xml = """
      <type name="xx" id="xx">
      <operation id="xx" name="xx">
          <body>
            <var name="xx" type-ref="xx">
              <call type-ref="#FROM_TYPE_ID" name="NAME-OF-OPERATION" out="out-argument-id">
                <arg name="left" id="left-id">
                  <var-ref name="var-ref-001"/>
                </arg>
                <arg name="right" id="right-id">
                  <var-ref name="var-ref-002"/>
                </arg>
              </call>
            </var>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    Detail     detail  = module.details.get("xx");
    DetailType alxType = (DetailType) detail;
    Operation  op      = alxType.operations.get("xx");
    SttVar     var     = (SttVar) op.body.statements.get(0);
    ExprCall call = (ExprCall) var.expr.taken;

    assertThat(call.typeRef).isEqualTo("#FROM_TYPE_ID");
    assertThat(call.name).isEqualTo("NAME-OF-OPERATION");
    assertThat(call.out).isEqualTo("out-argument-id");

    assertThat(call.args).hasSize(2);

    assertThat(call.args.get(0).id).isEqualTo("left-id");
    assertThat(call.args.get(1).id).isEqualTo("right-id");

    assertThat(call.args.get(0).name).isEqualTo("left");
    assertThat(call.args.get(1).name).isEqualTo("right");

    assertVarRef(call.args.get(0).expr, "var-ref-001");
    assertVarRef(call.args.get(1).expr, "var-ref-002");

    assertThat(call.pos.line).isEqualTo(5);
    assertThat(call.args.get(0).pos.line).isEqualTo(6);
    assertThat(call.args.get(1).pos.line).isEqualTo(9);
  }

  @Test
  public void parse___operation__ExprNativeRef() {

    // language=XML
    String xml = """
      <type name="xx" id="xx">
      <operation id="xx" name="xx">
          <body>
            <var name="xx" type-ref="xx">
              <NATIVE-REF id="T59dLMqjAd" out="result3213">
                <arg id="arg1">
                  <var-ref name="var-ref-001"/>
                </arg>
                <arg id="arg2">
                  <var-ref name="var-ref-002"/>
                </arg>
              </NATIVE-REF>
            </var>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    Detail     detail  = module.details.get("xx");
    DetailType alxType = (DetailType) detail;
    Operation  op      = alxType.operations.get("xx");
    SttVar     var     = (SttVar) op.body.statements.get(0);

    assertThat(var.expr.taken).isInstanceOf(ExprNativeRef.class);

    ExprNativeRef ref = (ExprNativeRef) var.expr.taken;

    assertThat(ref.id).isEqualTo("T59dLMqjAd");
    assertThat(ref.out).isEqualTo("result3213");

    assertThat(ref.args).hasSize(2);

    assertThat(ref.args.get(0).id).isEqualTo("arg1");
    assertThat(ref.args.get(1).id).isEqualTo("arg2");

    assertVarRef(ref.args.get(0).expr, "var-ref-001");
    assertVarRef(ref.args.get(1).expr, "var-ref-002");

    assertThat(ref.pos.line).isEqualTo(5);
    assertThat(ref.args.get(0).pos.line).isEqualTo(6);
    assertThat(ref.args.get(1).pos.line).isEqualTo(9);
  }

  @Test
  public void parse___operation__ExprConstant() {

    // language=XML
    String xml = """
      <type name="Тестовое название типа" id="RUvd7ehZtL">
      <operation id="5aSkl39sKz" name="TEST_OP">
          <body>
            <var name="test-var" type-ref="#TEST_TYPE_LOCAL_ID">
              <constant type-ref="#INT" bytes="i4 1"/>
            </var>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    Detail     detail  = module.details.get("RUvd7ehZtL");
    DetailType alxType = (DetailType) detail;
    Operation  op      = alxType.operations.get("5aSkl39sKz");
    SttVar     var     = (SttVar) op.body.statements.get(0);

    assertThat(var.expr.taken).isInstanceOf(ExprConstant.class);

    ExprConstant cnt = (ExprConstant) var.expr.taken;

    assertThat(cnt.typeRef).isEqualTo("#INT");
    assertThat(cnt.bytes).isEqualTo(new byte[]{1, 0, 0, 0});

    assertThat(cnt.pos.line).isEqualTo(5);
  }

  @Test
  public void parse___operation__ExprSpaceRef() {

    // language=XML
    String xml = """
      <type name="Тестовое название типа" id="RUvd7ehZtL">
      <operation id="5aSkl39sKz" name="TEST_OP">
          <arg name="left" type-ref="ME" dir="IN"/>
          <arg name="right" type-ref="#FLOAT" dir="IN"/>
          <arg name="result" type-ref="ME" dir="OUT"/>
            
          <body>
            <var name="test-var" type-ref="#TEST_TYPE_LOCAL_ID">
              <space-ref offset="test-field-name" in-type-ref="PARENT-TEST_TYPE">
                <var-ref name="test-var-ref-name"/>
              </space-ref>
            </var>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    Detail detail = module.details.get("RUvd7ehZtL");

    assertThat(detail).isInstanceOf(DetailType.class);
    assertThat(detail.id()).isEqualTo("RUvd7ehZtL");

    var alxType = (DetailType) detail;

    assertThat(alxType.name).isEqualTo("Тестовое название типа");

    Operation op = alxType.operations.get("5aSkl39sKz");
    assertThat(op).isNotNull();

    assertThat(op.id).isEqualTo("5aSkl39sKz");
    assertThat(op.name).isEqualTo("TEST_OP");

    assertThat(op.args).hasSize(3);


    OperationArg arg1 = op.args.get("left");
    OperationArg arg2 = op.args.get("right");
    OperationArg arg3 = op.args.get("result");

    assertThat(arg1.name).isEqualTo("left");
    assertThat(arg2.name).isEqualTo("right");
    assertThat(arg3.name).isEqualTo("result");

    assertThat(arg1.__typeRef).isEqualTo("ME");
    assertThat(arg2.__typeRef).isEqualTo("#FLOAT");
    assertThat(arg3.__typeRef).isEqualTo("ME");

    assertThat(arg1.dir).isEqualTo(ArgDir.IN);
    assertThat(arg2.dir).isEqualTo(ArgDir.IN);
    assertThat(arg3.dir).isEqualTo(ArgDir.OUT);

    assertThat(op.body).isNotNull();
    assertThat(op.body.statements).hasSize(1);

    var var = (SttVar) op.body.statements.get(0);

    assertThat(var.name).isEqualTo("test-var");
    assertThat(var.typeRef).isEqualTo("#TEST_TYPE_LOCAL_ID");

    var space = (ExprSpaceRef) var.expr.taken;

    assertThat(space.inTypeRef).isEqualTo("PARENT-TEST_TYPE");
    assertThat(space.offset).isEqualTo("test-field-name");

    assertVarRef(space.expr, "test-var-ref-name");

    assertThat(space.pos.line).isEqualTo(9);
  }

  @Test
  public void parse___operation__SttReturn() {

    // language=XML
    String xml = """
      <type name="tst" id="R5zOR1L5Gr">
      <operation id="N0BrdTkD8Q" name="txt">
          <body>
            <return/>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    Detail    detail  = module.details.get("R5zOR1L5Gr");
    var       alxType = (DetailType) detail;
    Operation op      = alxType.operations.get("N0BrdTkD8Q");
    assertThat(op).isNotNull();

    assertThat(op.body.statements).hasSize(1);

    assertThat(op.body.statements.get(0)).isInstanceOf(SttReturn.class);

    assertThat(op.body.statements.get(0).pos.line).isEqualTo(4);
  }

  @Test
  public void parse___operation__SttAssign() {

    // language=XML
    String xml = """
      <type name="tst" id="R5zOR1L5Gr">
      <operation id="N0BrdTkD8Q" name="txt">
          <body>
            <assign>
              <target>
                <var-ref name='ref-target'/>
              </target>
              <source>
                <var-ref name='ref-source'/>
              </source>
            </assign>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    Detail    detail  = module.details.get("R5zOR1L5Gr");
    var       alxType = (DetailType) detail;
    Operation op      = alxType.operations.get("N0BrdTkD8Q");
    assertThat(op).isNotNull();

    assertThat(op.body.statements).hasSize(1);

    var stt = (SttAssign) op.body.statements.get(0);

    assertVarRef(stt.target, "ref-target");
    assertVarRef(stt.source, "ref-source");

    assertThat(stt.pos.line).isEqualTo(4);
  }

  @Test
  public void parse___operation__SttVar() {

    // language=XML
    String xml = """
      <type name="tst" id="R5zOR1L5Gr">
      <operation id="N0BrdTkD8Q" name="txt">
          <body>
            <var name="test-new-var-name" type-ref="#Test-Type-REF">
              <var-ref name='test-var-ref'/>
            </var>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    Detail    detail  = module.details.get("R5zOR1L5Gr");
    var       alxType = (DetailType) detail;
    Operation op      = alxType.operations.get("N0BrdTkD8Q");
    assertThat(op).isNotNull();

    assertThat(op.body.statements).hasSize(1);

    var stt = (SttVar) op.body.statements.get(0);

    assertThat(stt.typeRef).isEqualTo("#Test-Type-REF");
    assertThat(stt.name).isEqualTo("test-new-var-name");

    assertVarRef(stt.expr, "test-var-ref");

    assertThat(stt.pos.line).isEqualTo(4);
  }

  @Test
  public void parse___operation__SttIf() {

    // language=XML
    String xml = """
      <type name="tst" id="R5zOR1L5Gr">
      <operation id="N0BrdTkD8Q" name="txt">
          <body>
            <if>
              <condition>
                <var-ref name="if-condition"/>
              </condition>
              <then>
                <var name="var-then-0" type-ref="x"/>
                <var name="var-then-1" type-ref="x"/>
              </then>
              <else>
                <var name="var-else-0" type-ref="x"/>
                <var name="var-else-1" type-ref="x"/>
              </else>
            </if>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    Detail    detail  = module.details.get("R5zOR1L5Gr");
    var       alxType = (DetailType) detail;
    Operation op      = alxType.operations.get("N0BrdTkD8Q");
    assertThat(op).isNotNull();

    assertThat(op.body.statements).hasSize(1);

    var stt = (SttIf) op.body.statements.get(0);

    assertVarRef(stt.condition, "if-condition");
    assertFirstVar(stt.then, 0, "var-then-0");
    assertFirstVar(stt.then, 1, "var-then-1");
    assertFirstVar(stt.els, 0, "var-else-0");
    assertFirstVar(stt.els, 1, "var-else-1");

    assertThat(stt.pos.line).isEqualTo(4);
  }

  @Test
  public void parse___operation__AlxMemory() {

    // language=XML
    String xml = """
      <type name="xx" id="xx">
        <memory>
          <space name="s-test-1" size="4 bytes" size-by-type-ref="#TYPE-001"/><!-- offset 0 -->
          <space name="s-test-2" size="9 bytes" size-by-type-ref="#TYPE-002"/><!-- offset 4 -->
          <space name="s-test-3" size="8 bytes" size-by-type-ref="#TYPE-003"/><!-- offset 16 -->
        </memory>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    DetailType detail = (DetailType) module.details.get("xx");

    AlxMemory memory = detail.memory;

    assertThat(memory.spaces).hasSize(3);

    assertThat(memory.spaces.get(0).__sizeBytes).isEqualTo(4L);
    assertThat(memory.spaces.get(1).__sizeBytes).isEqualTo(9L);
    assertThat(memory.spaces.get(2).__sizeBytes).isEqualTo(8L);

    assertThat(memory.spaces.get(0).__offsetBytes).isEqualTo(0);
    assertThat(memory.spaces.get(1).__offsetBytes).isEqualTo(0);
    assertThat(memory.spaces.get(2).__offsetBytes).isEqualTo(0);

    assertThat(memory.spaces.get(0).name).isEqualTo("s-test-1");
    assertThat(memory.spaces.get(1).name).isEqualTo("s-test-2");
    assertThat(memory.spaces.get(2).name).isEqualTo("s-test-3");

    assertThat(memory.spaces.get(0).__sizeByTypeRef).isEqualTo("#TYPE-001");
    assertThat(memory.spaces.get(1).__sizeByTypeRef).isEqualTo("#TYPE-002");
    assertThat(memory.spaces.get(2).__sizeByTypeRef).isEqualTo("#TYPE-003");

    assertThat(memory.spaces.get(0).pos.line).isEqualTo(3);
    assertThat(memory.spaces.get(1).pos.line).isEqualTo(4);
    assertThat(memory.spaces.get(2).pos.line).isEqualTo(5);

  }

  @Test
  public void parse___operation__SttBlock() {

    // language=XML
    String xml = """
      <type name="xx" id="xx">
        <operation id="xx" name="xx">
          <body>
            <block>
              <init>
                <var name="var init"/>
              </init>
              <before-while>
                <var-ref name="var-ref before-while"/>
              </before-while>
              <before-until>
                <var-ref name="var-ref before-until"/>
              </before-until>
              <next>
                <var name="var next"/>
              </next>
              <body>
                <var name="var body"/>
              </body>
              <after-while>
                <var-ref name="var-ref after-while"/>
              </after-while>
              <after-until>
                <var-ref name="var-ref after-until"/>
              </after-until>
            </block>
          </body>
        </operation>
      </type>
      """;

    var module = new AlxModule();

    //
    //
    module.parse(strToInputStream(xml), "test");
    //
    //

    assertThat(module.details).isNotNull();

    DetailType detail = (DetailType) module.details.get("xx");
    Operation  op     = detail.operations.get("xx");
    SttBlock   block  = (SttBlock) op.body.statements.get(0);

    assertFirstVar(block.init, 0, "var init");
    assertFirstVar(block.next, 0, "var next");
    assertFirstVar(block.body, 0, "var body");

    assertVarRef(block.beforeWhile, "var-ref before-while");
    assertVarRef(block.beforeUntil, "var-ref before-until");

    assertVarRef(block.afterWhile, "var-ref after-while");
    assertVarRef(block.afterUntil, "var-ref after-until");

  }

}
