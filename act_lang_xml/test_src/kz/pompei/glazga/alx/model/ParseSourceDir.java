package kz.pompei.glazga.alx.model;

import java.nio.file.Path;
import java.nio.file.Paths;
import kz.pompei.glazga.utils.FileUtils;

public class ParseSourceDir {

  public static void main(String[] args) {

    var module = new AlxModule();
    Path source = Paths.get(".").resolve("source");

    FileUtils.scanDir(source, file -> file.toFile().getName().endsWith(".xml"))
             .forEach(module::parseFile);

    System.out.println("brk0Ew1jff :: module = " + module);

  }

}
