package kz.pompei.glazga.alx.utils;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SizeParserTest {

  @DataProvider
  private Object[][] parseSizeDataProvider() {
    return new Object[][]{

      {" _123 bytes  ", 123L},
      {"  321 byte   ", 321L},
      {" _321 kByte  ", 321L * 1000L},
      {"3 217 kBytes ", 3217L * 1000L},
      {"3 217 mBytes ", 3217L * 1000L * 1000L},
      {" _717 mByte  ", 717L * 1000L * 1000L},
      {"1_717 gByte  ", 1717L * 1000L * 1000L * 1000L},
      {"1_817 gBytes ", 1817L * 1000L * 1000L * 1000L},
      {" _341 kiByte ", 341L * 1024L},
      {"3 417 kiBytes", 3417L * 1024L},
      {"3_417 miBytes", 3417L * 1024L * 1024L},
      {" _747 miByte ", 747L * 1024L * 1024L},
      {"1 417 giByte ", 1417L * 1024L * 1024L * 1024L},
      {"1_417 giBytes", 1417L * 1024L * 1024L * 1024L},

      {" _321 k  ", 321L * 1000L},
      {"3_217 m  ", 3217L * 1000L * 1000L},
      {"1 717 g  ", 1717L * 1000L * 1000L * 1000L},
      {"  341 ki ", 341L * 1024L},
      {"3_417 mi ", 3417L * 1024L * 1024L},
      {"1_417 gi ", 1417L * 1024L * 1024L * 1024L},
    };
  }

  @Test(dataProvider = "parseSizeDataProvider")
  public void parseSize(String str, long expected) {

    //
    //
    long actual = SizeParser.parseSize(str);
    //
    //

    assertThat(actual).isEqualTo(expected);
  }
}
