package kz.pompei.glazga.alx.utils;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConstParserTest {


  private byte[] bytes(String byteData) {

    String[] split = byteData.trim().toLowerCase().split("\\s+");

    byte[] bytes = new byte[split.length];

    for (int i = 0; i < split.length; i++) {
      bytes[i] = ConstParser.bbToByte(split[i]);
    }

    return bytes;
  }

  @DataProvider
  private Object[][] bbToBytesDataProvider() {
    return new Object[][]{
      {"00", 0x00},
      {"01", 0x01},
      {"09", 0x09},
      {"0a", 0x0a},
      {"0e", 0x0e},
      {"0f", 0x0f},
      {"0a", 0x0a},
      {"13", 0x13},
      {"3f", 0x3f},
      {"2e", 0x2e},
      {"ff", 0xff},
      {"ee", 0xee},
      {"e0", 0xe0},
    };
  }

  @Test(dataProvider = "bbToBytesDataProvider")
  public void bbToBytes(String bb, int expected) {
    System.out.println(expected);
    assertThat(ConstParser.bbToByte(bb)).isEqualTo((byte) expected);
  }

  @DataProvider
  private Object[][] parseDataProvider() {
    return new Object[][]{
      {"f8 4.1", "66 66 66 66 66 66 10 40"},
      {"f8 4.1 -5.13", "66 66 66 66 66 66 10 40    85 eb 51 b8 1e 85 14 c0"},
      {"f4 7.1", "33 33 e3 40"},
      {"f4 7.1 -8.2", "33 33 e3 40   33 33 03 c1"},

      {"i4      15301", "c5 3b 00 00"},
      {"i4  543254326", "36 67 61 20"},
      {"i4 -543254326", "ca 98 9e df"},
      {"i4 1 2 3", "01 00 00 00    02 00 00 00    03 00 00 00"},

      {"i8 543654367654254326", "f6 16 9f 58 c4 72 8b 07"},
      {"i8 7 8 9", "07 00 00 00 00 00 00 00     08 00 00 00 00 00 00 00     09 00 00 00 00 00 00 00"},

      {"i1 13", "0d"},
      {"i1 13 -13", "0d f3"},

      {"u1 13", "0d"},
      {"u1 13 201", "0d c9"},

      {"u4      15301", "c5 3b 00 00"},
      {"u4  543254326", "36 67 61 20"},
      {"u4 3751712970", "ca 98 9e df"},
      {"u4 1 2 3", "01 00 00 00    02 00 00 00    03 00 00 00"},

      {"u8   543654367654254326", "f6 16 9f 58 c4 72 8b 07"},
      {"u8 17903089706055297290", "0a e9 60 a7 3b 8d 74 f8"},
      {"u8 7 8 9", "07 00 00 00 00 00 00 00     08 00 00 00 00 00 00 00     09 00 00 00 00 00 00 00"},

      {"u1 x 1a 2B 3C 4E 5f     d 100 200 50", "1a 2b 3c 4e 5f      64 c8 32"},
      {"u4 x aaa   d 999", "aa 0a 00 00      e7 03 00 00"},
      {"u8 x ccc0ccc0   d 999888", "c0 cc c0 cc 00 00 00 00      d0 41 0f 00 00 00 00 00"},

      {"i1 x 1a 2B 3C 4E 5f     d 100 200 50", "1a 2b 3c 4e 5f      64 c8 32"},
      {"i4 x aaa   d 999", "aa 0a 00 00      e7 03 00 00"},
      {"i8 x ccc0ccc0   d 999888", "c0 cc c0 cc 00 00 00 00      d0 41 0f 00 00 00 00 00"},
    };
  }

  @Test(dataProvider = "parseDataProvider")
  public void parse(String strData, String byteData) {

    //
    //
    byte[] parsed = ConstParser.parse(strData);
    //
    //

    assertThat(parsed).isEqualTo(bytes(byteData));
  }

}
