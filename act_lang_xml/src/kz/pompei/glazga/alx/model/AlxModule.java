package kz.pompei.glazga.alx.model;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import kz.pompei.glazga.alx.model.mod.Detail;
import kz.pompei.glazga.alx.model.mod.NativeHome;
import kz.pompei.glazga.alx.model.mod.SrcFile;
import kz.pompei.glazga.alx.parse.ParseFileHandler;
import lombok.NonNull;
import lombok.SneakyThrows;

public class AlxModule {

  public NativeHome natives;

  public final Map<String, Detail> details = new HashMap<>();

  @SneakyThrows
  public void parse(@NonNull InputStream inputStream, String txtSource) {
    ParseFileHandler afh = new ParseFileHandler(this, txtSource);
    SAXParserFactory factory = SAXParserFactory.newInstance();
    SAXParser        saxParser = factory.newSAXParser();

    saxParser.parse(inputStream, afh);

    SrcFile target = afh.target();
    if (target == null) {
      throw new RuntimeException("RPzSR52Jwj :: Нет результата чтения " + txtSource);
    }

    if (target instanceof NativeHome x) {
      natives = x;
      return;
    }

    if (target instanceof Detail x) {
      x.txtSource = txtSource;

      if (x.id() == null) {
        throw new RuntimeException("qoZy1nB8DF :: No id in detail from file " + txtSource);
      }

      Detail existDetail = details.get(x.id());

      if (existDetail != null) {
        throw new RuntimeException("e6F5NoS6dE :: Same id = `" + x.id() + "`"
                                   + " in different detail files: " + txtSource
                                   + " and " + existDetail.txtSource);
      }

      details.put(x.id(), x);
      return;
    }

    throw new RuntimeException("kg0lanBYmi :: Unknown Alx class " + target.getClass());
  }

  @SneakyThrows
  public void parseFile(@NonNull Path file) {
    try (FileInputStream fileInputStream = new FileInputStream(file.toFile())) {
      parse(fileInputStream, file.toString());
    }
  }

}
