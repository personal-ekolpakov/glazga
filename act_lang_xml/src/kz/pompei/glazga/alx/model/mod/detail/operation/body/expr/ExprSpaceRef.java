package kz.pompei.glazga.alx.model.mod.detail.operation.body.expr;

import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;

public class ExprSpaceRef extends Expr {

  public String offset;
  public String inTypeRef;

  public final ExprHolder expr = new ExprHolder() {};

  public ExprSpaceRef(@NonNull Operation op, @NonNull ExprHolder parent) {
    super(op, parent);
  }
}
