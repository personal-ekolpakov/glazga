package kz.pompei.glazga.alx.model.mod.detail.type_ref;

import kz.pompei.glazga.alx.model.mod.DetailType;
import lombok.NonNull;

public abstract class TypeRef {
  public abstract @NonNull DetailType target();
}
