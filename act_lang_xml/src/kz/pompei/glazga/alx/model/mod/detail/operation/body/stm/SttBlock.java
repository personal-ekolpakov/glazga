package kz.pompei.glazga.alx.model.mod.detail.operation.body.stm;

import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.Body;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SttBlock extends Statement {
  public final @NonNull Operation op;

  public final ExprHolder beforeWhile = new ExprHolder() {};
  public final ExprHolder beforeUntil = new ExprHolder() {};

  public Body init;
  public Body next;
  public Body body;

  public final ExprHolder afterWhile = new ExprHolder() {};
  public final ExprHolder afterUntil = new ExprHolder() {};

  public SrcPos beforeWhilePos;
  public SrcPos beforeUntilPos;

  public SrcPos initPos;
  public SrcPos nextPos;
  public SrcPos bodyPos;

  public SrcPos afterWhilePos;
  public SrcPos afterUntilPos;
}
