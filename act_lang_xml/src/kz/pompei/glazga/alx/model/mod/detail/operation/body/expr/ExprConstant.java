package kz.pompei.glazga.alx.model.mod.detail.operation.body.expr;

import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;

public class ExprConstant extends Expr {
  public String typeRef;
  public byte[] bytes;

  public ExprConstant(@NonNull Operation op, @NonNull ExprHolder parent) {
    super(op, parent);
  }
}
