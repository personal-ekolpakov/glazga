package kz.pompei.glazga.alx.model.mod.detail.operation.body.stm;

import kz.pompei.glazga.alx.model.mod.detail.Operation;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SttReturn extends Statement {
  public final @NonNull Operation op;
}
