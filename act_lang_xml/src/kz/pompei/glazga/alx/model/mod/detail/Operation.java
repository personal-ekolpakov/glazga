package kz.pompei.glazga.alx.model.mod.detail;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.alx.model.mod.DetailType;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.Body;
import kz.pompei.glazga.alx.model.mod.detail.operation.OperationArg;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Operation {
  public final @NonNull DetailType owner;

  public String id;
  public String name;

  public SrcPos pos;

  public Map<String, OperationArg> args = new HashMap<>();

  public Body body;

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" + name + (id == null ? "" : ", id=" + id) + "}";
  }
}
