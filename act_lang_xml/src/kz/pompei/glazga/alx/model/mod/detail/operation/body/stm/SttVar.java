package kz.pompei.glazga.alx.model.mod.detail.operation.body.stm;

import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SttVar extends Statement {
  public final @NonNull Operation op;

  public String name;
  public String typeRef;

  public final ExprHolder expr = new ExprHolder() {};
}
