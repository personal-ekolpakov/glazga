package kz.pompei.glazga.alx.model.mod.detail;

import kz.pompei.glazga.alx.model.ann.ImportWhere;
import kz.pompei.glazga.alx.model.mod.Detail;
import kz.pompei.glazga.alx.model.mod.DetailType;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AlxImport {
  public final @NonNull DetailType owner;

  public String id;
  public String name;
  public String detailId;
  public ImportWhere where;
  public String      anotherModuleId;

  public SrcPos pos;

  @Override
  public String toString() {
    return "AlxImport{" +
           "id='" + id + '\'' +
           ", name='" + name + '\'' +
           '}';
  }

  public DetailType target() {

    var where = this.where;

    if (where == null) {
      throw new RuntimeException("dc1qASnFJ9 :: No `where` in " + pos);
    }

    if (where == ImportWhere.MY_MODULE) {

      var detailId = this.detailId;

      if (detailId == null) {
        throw new RuntimeException("lHYtbMGWX4 :: Not defined `id` in tag `import` in " + pos);
      }

      Detail target = owner.module.details.get(detailId);

      if (target == null) {
        throw new RuntimeException("qBDoJX8yDB :: No detail with id = `" + detailId + "` in " + pos);
      }

      if (!(target instanceof DetailType ret)) {
        throw new RuntimeException("kl2v26A1ry :: Detail with id = `" + detailId + "` is `" + target.getClass().getSimpleName() + "`"
                                   + " but MUST be `" + DetailType.class.getSimpleName() + "` in " + pos);
      }

      return ret;
    }

    throw new RuntimeException("femH0QCpx9 :: Not yet impl for `where` = `" + where + "` in " + pos);
  }
}
