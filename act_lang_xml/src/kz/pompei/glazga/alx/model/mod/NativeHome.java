package kz.pompei.glazga.alx.model.mod;

import java.util.Map;
import kz.pompei.glazga.alx.model.mod.natives.AlxNative;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NativeHome extends SrcFile {
  public final Map<String, AlxNative> map;
}
