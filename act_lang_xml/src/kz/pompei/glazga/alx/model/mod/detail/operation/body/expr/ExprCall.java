package kz.pompei.glazga.alx.model.mod.detail.operation.body.expr;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.call.CallArg;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;

public class ExprCall extends Expr {

  public String typeRef;
  public String name;
  public String out;

  public List<CallArg> args = new ArrayList<>();

  public ExprCall(@NonNull Operation op, @NonNull ExprHolder parent) {
    super(op, parent);
  }
}
