package kz.pompei.glazga.alx.model.mod.detail.operation.body.expr;

import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class Expr {
  public final @NonNull Operation  op;
  public final @NonNull ExprHolder parent;

  public SrcPos pos;
}
