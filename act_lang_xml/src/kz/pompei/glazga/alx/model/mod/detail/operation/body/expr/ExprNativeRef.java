package kz.pompei.glazga.alx.model.mod.detail.operation.body.expr;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.native_ref.NativeRefArg;
import lombok.NonNull;

public class ExprNativeRef extends Expr {

  public String id;
  public String out;

  public List<NativeRefArg> args = new ArrayList<>();

  public ExprNativeRef(@NonNull Operation op, @NonNull ExprHolder parent) {
    super(op, parent);
  }
}
