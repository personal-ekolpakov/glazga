package kz.pompei.glazga.alx.model.mod.detail.operation.body.stm;

import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.Body;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SttIf extends Statement {
  public final @NonNull Operation op;

  public final ExprHolder condition = new ExprHolder() {};

  public Body then;
  public Body els;

  public SrcPos posCondition;
  public SrcPos posThen;
  public SrcPos posElse;

}
