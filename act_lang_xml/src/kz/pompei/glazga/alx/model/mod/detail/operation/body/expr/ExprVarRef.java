package kz.pompei.glazga.alx.model.mod.detail.operation.body.expr;

import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;

public class ExprVarRef extends Expr {

  public String name;

  public ExprVarRef(@NonNull Operation op, @NonNull ExprHolder parent) {
    super(op, parent);
  }

}
