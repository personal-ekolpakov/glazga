package kz.pompei.glazga.alx.model.mod.detail;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.alx.model.mod.DetailType;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.memory.AlxMemorySpace;
import kz.pompei.glazga.v3.calc.Once;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AlxMemory {
  public final @NonNull DetailType           owner;
  public final @NonNull List<AlxMemorySpace> spaces = new ArrayList<>();

  public SrcPos pos;

  private final Once<Long> sizeBytes = new Once<>(this::calculateSizeBytes);

  public long sizeBytes() {
    return sizeBytes.get();
  }

  private long calculateSizeBytes() {
    long offset = 0;

    int align = 4;

    for (final AlxMemorySpace space : spaces) {
      space.__offsetBytes = offset;
      offset += space.sizeBytes();

      long mod = offset % align;

      if (mod > 0) {
        offset += align - mod;
      }

    }

    return offset;
  }
}
