package kz.pompei.glazga.alx.model.mod.detail.memory;

import java.util.function.Supplier;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.AlxMemory;
import kz.pompei.glazga.alx.model.mod.detail.type_ref.TypeRef;
import kz.pompei.glazga.v3.calc.Once;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AlxMemorySpace {
  public final @NonNull AlxMemory owner;

  public String name;
  public String __sizeByTypeRef = null;
  public long   __offsetBytes;
  public Long   __sizeBytes;
  public SrcPos pos;

  private final Once<Long> offsetBytes = new Once<>(new Supplier<>() {
    @Override
    public Long get() {
      owner.sizeBytes()/* it fills field `__offsetBytes` */;
      return __offsetBytes;
    }
  });

  private final Once<TypeRef> typeRef = new Once<>(this::calculateTypeRef);

  private TypeRef calculateTypeRef() {

    var ref = __sizeByTypeRef;
    if (ref == null) {
      throw new RuntimeException("nMmVyz2txg :: Not defined `size-by-type-ref` or `size` in " + pos);
    }

    return owner.owner.extractTypeRef(ref, "size-by-type-ref", pos);
  }

  public long offsetBytes() {
    return offsetBytes.get();
  }

  public long sizeBytes() {
    {
      Long x = __sizeBytes;
      if (x != null) {
        return x;
      }
    }

    return typeRef.get().target().memory.sizeBytes();
  }
}
