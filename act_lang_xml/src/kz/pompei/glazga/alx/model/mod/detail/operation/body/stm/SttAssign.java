package kz.pompei.glazga.alx.model.mod.detail.operation.body.stm;

import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SttAssign extends Statement {
  public final @NonNull Operation op;

  public final ExprHolder target = new ExprHolder() {};
  public final ExprHolder source = new ExprHolder() {};

  public SrcPos posTarget;
  public SrcPos posSource;
}
