package kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.call;

import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;

public class CallArg {
  public String id;
  public String name;
  public SrcPos pos;

  public final ExprHolder expr = new ExprHolder() {};
}
