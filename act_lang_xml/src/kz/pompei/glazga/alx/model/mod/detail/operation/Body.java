package kz.pompei.glazga.alx.model.mod.detail.operation;

import java.util.List;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.Statement;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Body {
  public final List<Statement> statements;
}
