package kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.native_ref;

import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprNativeRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NativeRefArg {
  public final @NonNull ExprNativeRef owner;

  public String id;
  public SrcPos pos;

  public final ExprHolder expr = new ExprHolder() {};
}
