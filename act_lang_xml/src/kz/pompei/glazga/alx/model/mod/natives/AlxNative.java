package kz.pompei.glazga.alx.model.mod.natives;

import java.util.Map;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AlxNative {
  public final String                    id;
  public final Map<String, AlxNativeArg> args;
  public       SrcPos                    pos;

  @Override
  public String toString() {
    return getClass().getSimpleName() + ": " + id;
  }

}
