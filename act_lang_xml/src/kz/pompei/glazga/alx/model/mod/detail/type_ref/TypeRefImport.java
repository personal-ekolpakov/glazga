package kz.pompei.glazga.alx.model.mod.detail.type_ref;

import kz.pompei.glazga.alx.model.mod.DetailType;
import kz.pompei.glazga.alx.model.mod.detail.AlxImport;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TypeRefImport extends TypeRef {
  public final @NonNull AlxImport alxImport;

  @Override
  public @NonNull DetailType target() {
    return alxImport.target();
  }
}
