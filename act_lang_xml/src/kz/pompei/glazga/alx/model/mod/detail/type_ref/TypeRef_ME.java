package kz.pompei.glazga.alx.model.mod.detail.type_ref;

import kz.pompei.glazga.alx.model.mod.DetailType;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TypeRef_ME extends TypeRef {
  public final @NonNull DetailType target;

  @Override
  public @NonNull DetailType target() {
    return target;
  }
}
