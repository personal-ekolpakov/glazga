package kz.pompei.glazga.alx.model.mod.detail.operation.body.expr;

import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.OperationArg;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import lombok.NonNull;

public class ExprArgRef extends Expr {

  public String name;

  public ExprArgRef(@NonNull Operation op, @NonNull ExprHolder parent) {
    super(op, parent);
  }

  public OperationArg target() {

    OperationArg arg = op.args.get(name);

    if (arg == null) {
      throw new RuntimeException("2jGqVYh4li :: No argument with name = `" + name + "` in tag `arg-ref` in " + pos);
    }

    return arg;
  }
}
