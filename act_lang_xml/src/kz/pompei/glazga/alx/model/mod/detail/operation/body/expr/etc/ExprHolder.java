package kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc;

import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.Expr;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class ExprHolder {
  public Expr taken;
}
