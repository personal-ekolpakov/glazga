package kz.pompei.glazga.alx.model.mod.detail.operation;

import kz.pompei.glazga.alx.model.ann.ArgDir;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.type_ref.TypeRef;
import kz.pompei.glazga.v3.calc.Once;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class OperationArg {
  public final @NonNull Operation owner;

  public String name;
  public String __typeRef;
  public ArgDir dir;
  public int    order;
  public SrcPos pos;

  private final Once<TypeRef> typeRef = new Once<>(this::calculateTypeRef);

  public TypeRef typeRef() {
    return typeRef.get();
  }

  private TypeRef calculateTypeRef() {

    var ref = __typeRef;
    if (ref == null) {
      throw new RuntimeException("mIXAl7dFqI :: Not defined `type-ref` in " + pos);
    }

    return owner.owner.extractTypeRef(ref, "type-ref", pos);
  }
}
