package kz.pompei.glazga.alx.model.mod;

import kz.pompei.glazga.alx.parse.ParsingXml;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@EqualsAndHashCode
public class SrcPos {
  public final String txtSource;
  public final int    line;
  public final int    column;

  public static SrcPos from(@NonNull ParsingXml parsingXml) {
    return new SrcPos(parsingXml.txtSource(),
                      parsingXml.line(),
                      parsingXml.column());
  }

  private SrcPos(String txtSource, int line, int column) {
    this.txtSource = txtSource;
    this.line      = line;
    this.column    = column;
  }

  @Override
  public String toString() {
    return txtSource + ",line=" + line + ",col=" + column;
  }

  @SuppressWarnings("unused")
  public static SrcPos of(String txtSource, int line, int column) {
    return new SrcPos(txtSource, line, column);
  }
}
