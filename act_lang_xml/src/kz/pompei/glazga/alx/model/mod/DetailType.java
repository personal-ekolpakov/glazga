package kz.pompei.glazga.alx.model.mod;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.alx.model.AlxModule;
import kz.pompei.glazga.alx.model.mod.detail.AlxImport;
import kz.pompei.glazga.alx.model.mod.detail.AlxMemory;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.type_ref.TypeRef;
import kz.pompei.glazga.alx.model.mod.detail.type_ref.TypeRefImport;
import kz.pompei.glazga.alx.model.mod.detail.type_ref.TypeRef_ME;
import lombok.NonNull;

public class DetailType extends Detail {
  public final @NonNull  AlxModule module;
  private final @NonNull String    id;
  public final @NonNull  String    name;

  public final Map<String, AlxImport> imports = new HashMap<>();

  public AlxMemory memory;

  public final Map<String, Operation> operations = new HashMap<>();

  public DetailType(@NonNull AlxModule module, @NonNull String id, @NonNull String name) {
    this.module = module;
    this.id     = id;
    this.name   = name;
  }

  @Override
  public @NonNull String id() {
    return id;
  }

  public String name() {
    return name;
  }

  @Override
  public String toString() {
    return System.identityHashCode(this) + " " + id + ": " + name;
  }


  public TypeRef extractTypeRef(
    @NonNull String __typeRef,
    @NonNull String typeRefTagName,
    @NonNull SrcPos pos
  ) {

    if ("ME".equals(__typeRef)) {
      return new TypeRef_ME(this);
    }

    if (__typeRef.startsWith("#")) {

      String importId = __typeRef.substring(1);

      AlxImport alxImport = imports.get(importId);
      if (alxImport == null) {
        throw new RuntimeException("Hlg3jDz6Cj :: No import with id = `" + importId + "` in " + pos);
      }

      return new TypeRefImport(alxImport);
    }

    throw new RuntimeException("U5GsWR5r3B :: Cannot parse `" + typeRefTagName + "` = `" + __typeRef + "` in " + pos);

  }

}
