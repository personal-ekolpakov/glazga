package kz.pompei.glazga.alx.model.mod.natives;

import kz.pompei.glazga.alx.model.ann.ArgDir;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AlxNativeArg {
  public final String id;
  public final int order;
  public final long sizeBytes;
  public final ArgDir dir;
  public final SrcPos pos;

  @Override
  public String toString() {
    return getClass().getSimpleName()
           + '{'
           + "id=`" + id + "`"
           + ", size=" + sizeBytes + " Bytes" +
           '}';
  }
}
