package kz.pompei.glazga.alx.model.mod;

import lombok.NonNull;

public abstract class Detail extends SrcFile {
  public String txtSource;

  public abstract @NonNull String id();

}
