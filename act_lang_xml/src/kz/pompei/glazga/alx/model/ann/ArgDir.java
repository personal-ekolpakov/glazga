package kz.pompei.glazga.alx.model.ann;

public enum ArgDir {
  IN, OUT;

  public static ArgDir valueOfOrNull(String str) {
    try {
      return ArgDir.valueOf(str);
    } catch (Exception ignore) {
      return null;
    }
  }
}
