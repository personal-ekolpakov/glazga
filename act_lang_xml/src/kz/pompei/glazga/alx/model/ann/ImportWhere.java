package kz.pompei.glazga.alx.model.ann;

public enum ImportWhere {
  MY_MODULE,
  ANOTHER_MODULE;

  public static ImportWhere valueOfOrNull(String str) {
    try {
      return valueOf(str);
    } catch (Exception e) {
      return null;
    }
  }
}
