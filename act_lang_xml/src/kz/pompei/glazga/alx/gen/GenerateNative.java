package kz.pompei.glazga.alx.gen;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class GenerateNative {
  private static void printToFile(Document document) throws TransformerException {
    Path nativesXml = Paths.get(".").resolve("source").resolve("natives.xml");


    // Преобразование DOM в XML
    TransformerFactory transformerFactory = TransformerFactory.newInstance();
    Transformer        transformer        = transformerFactory.newTransformer();
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    DOMSource source = new DOMSource(document);

    StreamResult file = new StreamResult(nativesXml.toFile());
    transformer.transform(source, file);
  }

  public static void main(String[] args) throws Exception {

    Document document = DocumentBuilderFactory.newInstance()
                                              .newDocumentBuilder()
                                              .newDocument();

    Element rootElement = document.createElement("natives");
    document.appendChild(rootElement);

    print_2_1(document, rootElement);

    printFloatFunctions(document, rootElement);

    printCompareFunctions(document, rootElement);

    printConvertFunctions(document, rootElement);

    printToFile(document);

  }

  private static void printFloatFunctions(Document document, Element rootElement) {

    String fns = "SIN COS TG SQRT ABS LN LG10 LG2 EXP ARCTG ARCSIN ARCCOS";

    for (final String fn : fns.trim().split("\\s+")) {
      for (int size : new int[]{4, 8}) {

        Element nat = document.createElement("NATIVE");
        rootElement.appendChild(nat);
        rootElement.appendChild(document.createTextNode("\n"));

        String id = fn + " FLOAT " + size + " bytes";

        nat.setAttribute("id", id);

        Element in1 = document.createElement("input");
        in1.setAttribute("dir", "IN");
        in1.setAttribute("size", size + " bytes");
        Element res = document.createElement("result");
        res.setAttribute("dir", "OUT");
        res.setAttribute("size", size + " bytes");

        nat.appendChild(in1);
        nat.appendChild(res);

      }
    }


  }

  private static void print_2_1(Document document, Element rootElement) {
    List<String> typeLines = new ArrayList<>();

    typeLines.add("FLOAT : 4 8      : PLUS MINUS MUL DIV");
    typeLines.add("INT   : 1 2 4 8  : PLUS MINUS MUL DIV MOD");

    for (final String typeLine : typeLines) {

      String[] split = typeLine.split(":");

      String type = split[0].trim();

      int[] sizes = Arrays.stream(split[1].trim().split("\\s+")).mapToInt(Integer::parseInt).toArray();

      List<String> operations = Arrays.stream(split[2].trim().split("\\s+")).toList();

      for (final String operation : operations) {
        for (final int size : sizes) {

          Element nat = document.createElement("NATIVE");
          rootElement.appendChild(nat);
          rootElement.appendChild(document.createTextNode("\n"));

          String id = operation + " " + type + " " + size + " bytes";

          nat.setAttribute("id", id);

          Element in1 = document.createElement("input1");
          in1.setAttribute("size", size + " bytes");
          in1.setAttribute("dir", "IN");
          Element in2 = document.createElement("input2");
          in2.setAttribute("size", size + " bytes");
          in2.setAttribute("dir", "IN");
          Element res = document.createElement("result");
          res.setAttribute("size", size + " bytes");
          res.setAttribute("dir", "OUT");

          nat.appendChild(in1);
          nat.appendChild(in2);
          nat.appendChild(res);

        }
      }
    }
  }

  private static void printCompareFunctions(Document document, Element rootElement) {

    String str = "FLOAT INT : 8 4 : LESS LESS-EQUAL EQUAL NOT-EQUAL GREATER-EQUAL GREATER";

    String[] parts = str.trim().split(":");

    String[] types      = parts[0].trim().split("\\s+");
    String[] sizes      = parts[1].trim().split("\\s+");
    String[] operations = parts[2].trim().split("\\s+");

    for (final String type : types) {
      for (final String size : sizes) {
        for (final String operation : operations) {

          Element nat = document.createElement("NATIVE");
          rootElement.appendChild(nat);
          rootElement.appendChild(document.createTextNode("\n"));

          String id = "" + operation + " : " + type + " " + size + " bytes";

          nat.setAttribute("id", id);

          Element in1 = document.createElement("left");
          in1.setAttribute("dir", "IN");
          in1.setAttribute("size", size + " bytes");
          Element in2 = document.createElement("right");
          in2.setAttribute("dir", "IN");
          in2.setAttribute("size", size + " bytes");
          Element res = document.createElement("result");
          res.setAttribute("size", "4 bytes");
          res.setAttribute("dir", "OUT");

          nat.appendChild(in1);
          nat.appendChild(in2);
          nat.appendChild(res);

        }
      }
    }

  }


  private static void printConvertFunctions(Document document, Element rootElement) {

    List<String> lines = new ArrayList<>();

    lines.add("FLOAT : INT : 8 4");
    lines.add("INT : FLOAT : 8 4");

    for (final String line : lines) {
      String[] parts = line.split(":");

      String type1 = parts[0].trim();
      String type2 = parts[1].trim();

      String[] sizes = parts[2].trim().split("\\s+");

      for (final String size : sizes) {


        Element nat = document.createElement("NATIVE");
        rootElement.appendChild(nat);
        rootElement.appendChild(document.createTextNode("\n"));

        String id = "CONVERT " + type1 + " TO " + type2 + " : " + size + " bytes";

        nat.setAttribute("id", id);

        Element in1 = document.createElement("input");
        in1.setAttribute("dir", "IN");
        in1.setAttribute("size", size + " bytes");
        Element res = document.createElement("result");
        res.setAttribute("size", size + " bytes");
        res.setAttribute("dir", "OUT");

        nat.appendChild(in1);
        nat.appendChild(res);

      }

    }

  }

}
