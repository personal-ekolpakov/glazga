package kz.pompei.glazga.alx.parse;

import kz.pompei.glazga.alx.model.AlxModule;
import kz.pompei.glazga.alx.model.mod.SrcFile;
import kz.pompei.glazga.alx.parse.delegators.DetailTypeDelegator;
import kz.pompei.glazga.alx.parse.delegators.NativeHomeDelegator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

@RequiredArgsConstructor
public class ParseFileHandler extends DefaultHandler {
  private final @NonNull AlxModule module;
  private final @NonNull String    txtSource;

  private Locator locator;

  private final ParsingXml parsingXml = new ParsingXml() {
    @Override
    public @NonNull String txtSource() {
      return txtSource;
    }

    @Override
    public int line() {
      return locator.getLineNumber();
    }

    @Override
    public int column() {
      return locator.getColumnNumber();
    }
  };

  private PrsHandlerDelegate delegator = null;

  public SrcFile target() {
    var x = delegator;
    return x == null ? null : x.target();
  }

  @Override
  public void setDocumentLocator(Locator locator) {
    this.locator = locator;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

    {
      var d = delegator;
      if (d != null) {
        try {
          d.startElement(qName, attributes);
          return;
        } catch (SAXException | RuntimeException e) {
          throw e;
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
    }

    if (NativeHomeDelegator.root.equals(qName)) {
      delegator = new NativeHomeDelegator(parsingXml);
      return;
    }

    if (DetailTypeDelegator.root.equals(qName)) {
      delegator = new DetailTypeDelegator(module, attributes, parsingXml);
      return;
    }

    throw new RuntimeException("edgLj2UDkr :: Unknown root tag name `" + qName + "`");
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    var d = delegator;
    if (d != null) {
      try {
        d.characters(ch, start, length);
        return;
      } catch (SAXException | RuntimeException e) {
        throw e;
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    {
      var d = delegator;
      if (d != null) {
        try {
          d.endElement(qName);
          return;
        } catch (SAXException | RuntimeException e) {
          throw e;
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
    }
  }
}
