package kz.pompei.glazga.alx.parse;

import lombok.NonNull;

public abstract class ParsingXml {

  public abstract @NonNull String txtSource();

  public abstract int line();

  public abstract int column();

  @Override
  public String toString() {
    return txtSource() + ",line=" + line() + ",col=" + column();
  }
}
