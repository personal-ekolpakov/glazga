package kz.pompei.glazga.alx.parse;

import kz.pompei.glazga.alx.model.mod.SrcFile;
import org.xml.sax.Attributes;

public interface PrsHandlerDelegate {

  SrcFile target();

  void startElement(String tagName, Attributes attributes) throws Exception;

  default void characters(char[] ch, int start, int length) throws Exception {}

  void endElement(String tagName) throws Exception;

}
