package kz.pompei.glazga.alx.parse.delegators.type.operation.body.statement;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.Expr;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.Statement;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttAssign;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.TypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.ExprDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class SttAssignDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml          parsingXml;
  private final @NonNull SttAssign ret;
  private final @NonNull Consumer<Statement> onFinish;

  private TypeHandlerDelegate delegate;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) throws Exception {
    ret.pos = SrcPos.from(parsingXml);
  }

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.startElement(tagName, attributes);
        return;
      }
    }

    if ("target".equals(tagName)) {
      ret.posTarget = SrcPos.from(parsingXml);
      delegate = new ExprDelegate(parsingXml, ret.op, ret.target, this::acceptTarget);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("source".equals(tagName)) {
      ret.posSource = SrcPos.from(parsingXml);
      delegate = new ExprDelegate(parsingXml, ret.op, ret.source, this::acceptSource);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    throw new RuntimeException("xGA1VNYMfC :: Unknown tag `" + tagName + "` in SttAssign in " + parsingXml);
  }

  private void acceptTarget(Expr expr) {
    delegate = null;
    if (ret.target.taken != null) {
      throw new RuntimeException("xNurWb4ID9 :: Duplicate tag `target` in ExprAssign in " + parsingXml);
    }
    ret.target.taken = expr;
  }

  private void acceptSource(Expr expr) {
    delegate = null;
    if (ret.source.taken != null) {
      throw new RuntimeException("5mV7kxGuNw :: Duplicate tag `source` in ExprAssign in " + parsingXml);
    }
    ret.source.taken = expr;
  }

  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    var d = delegate;
    if (d != null) {
      d.characters(ch, start, length);
    }
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.endElement(tagName);
        return;
      }
    }
  }

  @Override
  public void finishParentTag() throws Exception {
    onFinish.accept(ret);
  }
}
