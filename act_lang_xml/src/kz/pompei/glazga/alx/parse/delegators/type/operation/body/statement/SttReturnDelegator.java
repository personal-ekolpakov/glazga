package kz.pompei.glazga.alx.parse.delegators.type.operation.body.statement;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.Statement;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttReturn;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class SttReturnDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml          parsingXml;
  private final @NonNull SttReturn ret;
  private final @NonNull Consumer<Statement> onFinish;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) throws Exception {
    ret.pos = SrcPos.from(parsingXml);
  }

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {

  }

  @Override
  protected void endTag(String tagName) throws Exception {

  }

  @Override
  public void finishParentTag() throws Exception {
    onFinish.accept(ret);
  }
}
