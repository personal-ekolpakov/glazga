package kz.pompei.glazga.alx.parse.delegators;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.alx.model.ann.ArgDir;
import kz.pompei.glazga.alx.model.mod.NativeHome;
import kz.pompei.glazga.alx.model.mod.SrcFile;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.natives.AlxNative;
import kz.pompei.glazga.alx.model.mod.natives.AlxNativeArg;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.PrsHandlerDelegate;
import kz.pompei.glazga.alx.utils.SizeParser;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

import static java.util.Collections.unmodifiableMap;

@RequiredArgsConstructor
public class NativeHomeDelegator implements PrsHandlerDelegate {
  private final @NonNull ParsingXml parsingXml;

  public static final String root = "natives";

  private NativeHome target;

  public Map<String, AlxNative>    natives    = new HashMap<>();
  public Map<String, AlxNativeArg> nativeArgs = new HashMap<>();

  @Override
  public SrcFile target() {
    return target;
  }

  private String inTag2   = null;
  private String nativeId = null;
  private SrcPos inTag2_pos;

  private String inTag3      = null;
  private Long tag3_size = null;
  private String tag3_dirStr = null;
  private SrcPos inTag3_pos;

  @Override
  public void startElement(String tagName, Attributes attributes) {

    if (inTag2 == null) {
      inTag2   = tagName;
      nativeId = attributes.getValue("id");
      if (nativeId == null) {
        throw new RuntimeException("D0lEShlUwF :: No attribute `id` in tag /" + root + "/" + inTag2);
      }

      inTag2_pos = SrcPos.from(parsingXml);

      if ("NATIVE".equals(inTag2)) {
        return;
      }

      throw new RuntimeException("fRJtaXHRTt :: Unknown tag /" + root + "/" + inTag2);
    }

    if (inTag3 == null) {
      inTag3      = tagName;
      inTag3_pos = SrcPos.from(parsingXml);
      tag3_size  = SizeParser.parseSize(attributes.getValue("size"));
      tag3_dirStr = attributes.getValue("dir");
      return;
    }

    if (inTag3 != null) {
      throw new RuntimeException("Ps8HqDZ1EP :: Unknown tag "
                                 + "/" + root + "/" + inTag2 + "/" + inTag3 + "/" + tagName
                                 + " in file " + parsingXml);
    }
  }

  private int argOrder = 1;

  @Override
  public void endElement(String tagName) {

    if (inTag2 != null && inTag3 != null) {

      if (tag3_size == null) {
        throw new RuntimeException("Nw1pjL3Oqy :: No attribute `size` in native `" + nativeId + "`");
      }
      if (tag3_dirStr == null) {
        throw new RuntimeException("GbtDQZl7VX :: No attribute `dir` in native `" + nativeId + "`");
      }

      ArgDir dir = ArgDir.valueOfOrNull(tag3_dirStr);

      if (dir == null) {
        throw new RuntimeException("1I7U4D7qBu :: Illegal value of attribute `dir` = `" + tag3_dirStr + "`"
                                   + " in native `" + nativeId + "`");
      }

      nativeArgs.put(inTag3, new AlxNativeArg(inTag3, argOrder++, tag3_size, dir, inTag3_pos));
      inTag3 = null;
      return;
    }

    if (inTag2 != null && inTag3 == null) {
      AlxNative nat = new AlxNative(nativeId, nativeArgs);
      nat.pos = inTag2_pos;
      natives.put(nativeId, nat);
      nativeArgs = new HashMap<>();
      nativeId   = null;
      inTag2     = null;
      return;
    }

    if (inTag2 == null && inTag3 == null) {
      target = new NativeHome(unmodifiableMap(natives));
      inTag2 = null;
      return;
    }

  }
}
