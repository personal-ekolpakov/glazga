package kz.pompei.glazga.alx.parse.delegators;

import kz.pompei.glazga.alx.model.AlxModule;
import kz.pompei.glazga.alx.model.mod.DetailType;
import kz.pompei.glazga.alx.model.mod.SrcFile;
import kz.pompei.glazga.alx.model.mod.detail.AlxImport;
import kz.pompei.glazga.alx.model.mod.detail.AlxMemory;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.PrsHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.ImportDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.OperationDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.memory.MemoryDelegator;
import lombok.NonNull;
import org.xml.sax.Attributes;

public class DetailTypeDelegator implements PrsHandlerDelegate {
  private final @NonNull DetailType ret;
  private final @NonNull ParsingXml parsingXml;

  public static final String root = "type";

  private TypeHandlerDelegate delegate = null;

  public DetailTypeDelegator(@NonNull AlxModule module, Attributes attributes, @NonNull ParsingXml parsingXml) {
    ret = new DetailType(module, attributes.getValue("id"), attributes.getValue("name"));
    this.parsingXml = parsingXml;
  }

  @Override
  public SrcFile target() {
    return ret;
  }

  @Override
  public void startElement(String tagName, Attributes attributes) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.startElement(tagName, attributes);
        return;
      }
    }

    if ("memory".equals(tagName)) {
      delegate = new MemoryDelegator(parsingXml, new AlxMemory(ret), this::acceptMemory);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("operation".equals(tagName)) {
      delegate = new OperationDelegator(parsingXml, new Operation(ret), this::acceptOperation);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("import".equals(tagName)) {
      delegate = new ImportDelegator(parsingXml, new AlxImport(ret), this::acceptImport);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    throw new RuntimeException("auKv7dtXNj :: Unknown tag `" + tagName + "` for type detail in " + parsingXml);
  }

  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    var d = delegate;
    if (d != null) {
      d.characters(ch, start, length);
      return;
    }
  }

  @Override
  public void endElement(String tagName) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.endElement(tagName);
        return;
      }
    }
  }

  private void acceptMemory(AlxMemory alxMemory) {
    delegate = null;

    if (ret.memory != null) {
      throw new RuntimeException("sIF8C5rkED :: Duplicate memory block in " + parsingXml);
    }

    ret.memory = alxMemory;
  }

  private void acceptImport(AlxImport alxImport) {
    delegate = null;

    AlxImport exists = ret.imports.get(alxImport.id);
    if (exists != null) {
      throw new RuntimeException("Au1g1C2b2i :: Duplicate import id = `" + alxImport.id + "` in " + parsingXml);
    }

    ret.imports.put(alxImport.id, alxImport);
  }

  private void acceptOperation(Operation operation) {
    delegate = null;
    ret.operations.put(operation.id, operation);
  }
}
