package kz.pompei.glazga.alx.parse.delegators.type.operation.body.expr;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.Expr;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprCall;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.call.CallArg;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.TypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.ExprDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class ExprCallDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml     parsingXml;
  private final @NonNull ExprCall ret;
  private final @NonNull Consumer<Expr> onFinish;

  private TypeHandlerDelegate delegate = null;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) throws Exception {
    ret.typeRef = attributes.getValue("type-ref");
    ret.name    = attributes.getValue("name");
    ret.out     = attributes.getValue("out");
    ret.pos = SrcPos.from(parsingXml);
  }

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.startElement(tagName, attributes);
        return;
      }
    }

    if ("arg".equals(tagName)) {
      CallArg arg = new CallArg();
      arg.id   = attributes.getValue("id");
      arg.name = attributes.getValue("name");
      arg.pos  = SrcPos.from(parsingXml);
      delegate = new ExprDelegate(parsingXml, ret.op, arg.expr, expr -> acceptArgExpr(arg, expr));
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    throw new RuntimeException("CljVRa1crW :: Unknown tag ExprCall/`" + tagName + "` in " + parsingXml);
  }

  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    var d = delegate;
    if (d != null) {
      d.characters(ch, start, length);
      return;
    }
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.endElement(tagName);
        return;
      }
    }
  }

  @Override
  public void finishParentTag() throws Exception {
    onFinish.accept(ret);
  }

  private void acceptArgExpr(CallArg arg, Expr expr) {
    delegate = null;

    arg.expr.taken = expr;

    ret.args.add(arg);

  }
}
