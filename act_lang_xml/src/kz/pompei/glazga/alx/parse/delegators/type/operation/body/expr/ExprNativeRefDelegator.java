package kz.pompei.glazga.alx.parse.delegators.type.operation.body.expr;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.Expr;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprNativeRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.native_ref.NativeRefArg;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.TypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.ExprDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class ExprNativeRefDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml     parsingXml;
  private final @NonNull ExprNativeRef ret;
  private final @NonNull Consumer<Expr> onFinish;

  private NativeRefArg arg;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) throws Exception {
    ret.id = attributes.getValue("id");
    ret.out = attributes.getValue("out");
    ret.pos = SrcPos.from(parsingXml);
  }

  private TypeHandlerDelegate delegate;

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.startElement(tagName, attributes);
        return;
      }
    }

    if ("arg".equals(tagName)) {
      arg      = new NativeRefArg(ret);
      arg.pos  = SrcPos.from(parsingXml);
      arg.id   = attributes.getValue("id");
      delegate = new ExprDelegate(parsingXml, ret.op, arg.expr, this::acceptArgExpr);
      return;
    }

    throw new RuntimeException("pB3CqErHMx :: Unknown tag `" + tagName + "` in `NATIVE-REF` in " + parsingXml);
  }

  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    var d = delegate;
    if (d != null) {
      d.characters(ch, start, length);
    }
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.endElement(tagName);
        return;
      }
    }
  }

  private void acceptArgExpr(Expr expr) {
    delegate       = null;
    arg.expr.taken = expr;
    ret.args.add(arg);
    arg = null;
  }

  @Override
  public void finishParentTag() throws Exception {
    onFinish.accept(ret);
  }
}
