package kz.pompei.glazga.alx.parse.delegators.type.operation;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.Expr;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprArgRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprCall;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprConstant;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprNativeRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprSpaceRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprVarRef;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.etc.ExprHolder;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.TypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.expr.ExprArgRefDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.expr.ExprCallDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.expr.ExprConstantDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.expr.ExprNativeRefDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.expr.ExprSpaceRefDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.expr.ExprVarRefDelegator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class ExprDelegate extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml     parsingXml;
  private final @NonNull Operation  op;
  private final @NonNull ExprHolder parent;
  private final @NonNull Consumer<Expr> onFinish;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) throws Exception {

  }

  private TypeHandlerDelegate delegate = null;
  private Expr expr = null;

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.startElement(tagName, attributes);
        return;
      }
    }

    if ("call".equals(tagName)) {
      delegate = new ExprCallDelegator(parsingXml, new ExprCall(op, parent), this::acceptExpr);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("space-ref".equals(tagName)) {
      delegate = new ExprSpaceRefDelegator(parsingXml, new ExprSpaceRef(op, parent), this::acceptExpr);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("constant".equals(tagName)) {
      delegate = new ExprConstantDelegator(parsingXml, new ExprConstant(op, parent), this::acceptExpr);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("var-ref".equals(tagName)) {
      delegate = new ExprVarRefDelegator(parsingXml, new ExprVarRef(op, parent), this::acceptExpr);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("arg-ref".equals(tagName)) {
      delegate = new ExprArgRefDelegator(parsingXml, new ExprArgRef(op, parent), this::acceptExpr);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("NATIVE-REF".equals(tagName)) {
      delegate = new ExprNativeRefDelegator(parsingXml, new ExprNativeRef(op, parent), this::acceptExpr);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    throw new RuntimeException("b4ecxU3TpT :: Unknown expr tag `" + tagName + "` in " + parsingXml);
  }

  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    var d = delegate;
    if (d != null) {
      d.characters(ch, start, length);
      return;
    }
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.endElement(tagName);
        return;
      }
    }
  }

  private void acceptExpr(Expr expr) {
    if (this.expr != null) {
      throw new RuntimeException("eJT1MS5RDV :: Duplicate expression happened in " + parsingXml);
    }
    this.expr = expr;
  }

  @Override
  public void finishParentTag() throws Exception {
    onFinish.accept(expr);
  }
}
