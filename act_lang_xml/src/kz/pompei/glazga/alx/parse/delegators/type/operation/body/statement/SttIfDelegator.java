package kz.pompei.glazga.alx.parse.delegators.type.operation.body.statement;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.Body;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.Expr;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.Statement;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttIf;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.TypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.BodyDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.ExprDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class SttIfDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml          parsingXml;
  private final @NonNull SttIf ret;
  private final @NonNull Consumer<Statement> onFinish;

  private TypeHandlerDelegate delegate;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) throws Exception {
    ret.pos = SrcPos.from(parsingXml);
  }

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.startElement(tagName, attributes);
        return;
      }
    }

    if ("condition".equals(tagName)) {
      ret.posCondition = SrcPos.from(parsingXml);
      delegate = new ExprDelegate(parsingXml, ret.op, ret.condition, this::acceptCondition);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("then".equals(tagName)) {
      ret.posThen = SrcPos.from(parsingXml);
      delegate = new BodyDelegate(parsingXml, ret.op, this::acceptThen);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("else".equals(tagName)) {
      ret.posElse = SrcPos.from(parsingXml);
      delegate = new BodyDelegate(parsingXml, ret.op, this::acceptElse);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    throw new RuntimeException("gj1pjPfLlN :: Unknown tag `" + tagName + "` in tag `if` in " + parsingXml);
  }


  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    var d = delegate;
    if (d != null) {
      d.characters(ch, start, length);
    }
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.endElement(tagName);
        return;
      }
    }
  }

  private void acceptCondition(Expr expr) {
    delegate = null;

    if (ret.condition.taken != null) {
      throw new RuntimeException("jxL0YfT8ni :: Tag `condition` already exists in tag `if` in " + parsingXml);
    }

    ret.condition.taken = expr;
  }

  private void acceptThen(Body body) {
    delegate = null;
    if (ret.then != null) {
      throw new RuntimeException("uYxUwcA95K :: Tag `then` already exists in tag `if` in " + parsingXml);
    }
    ret.then = body;
  }

  private void acceptElse(Body body) {
    delegate = null;
    if (ret.els != null) {
      throw new RuntimeException("AH1gq2I3tv :: Tag `else` already exists in tag `if` in " + parsingXml);
    }
    ret.els = body;
  }

  @Override
  public void finishParentTag() throws Exception {
    onFinish.accept(ret);
  }
}
