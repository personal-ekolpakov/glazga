package kz.pompei.glazga.alx.parse.delegators.type;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.ann.ImportWhere;
import kz.pompei.glazga.alx.model.mod.detail.AlxImport;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

import static kz.pompei.glazga.alx.model.ann.ImportWhere.ANOTHER_MODULE;

@RequiredArgsConstructor
public class ImportDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml          parsingXml;
  private final @NonNull AlxImport ret;
  private final @NonNull Consumer<AlxImport> onFinish;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) {
    String id              = attributes.getValue("id");
    String name            = attributes.getValue("name");
    String whereStr        = attributes.getValue("where");
    String detailId        = attributes.getValue("ref-id");
    String anotherModuleId = null;

    if (id == null) {
      throw new RuntimeException("5SpmYE1WYP :: Tag `import` has NO attribute `id` in " + parsingXml);
    }
    if (name == null) {
      throw new RuntimeException("T8tRpkBmCE :: Tag `import` has NO attribute `name` in " + parsingXml);
    }
    if (whereStr == null) {
      throw new RuntimeException("T8tRpkBmCE :: Tag `import` has NO attribute `where` in " + parsingXml);
    }
    if (detailId == null) {
      throw new RuntimeException("T8tRpkBmCE :: Tag `import` has NO attribute `ref-id` in " + parsingXml);
    }

    ImportWhere where = ImportWhere.valueOfOrNull(whereStr);
    if (where == null) {
      throw new RuntimeException("qrHkIeNmPo :: Tag `import` has ILLEGAL attribute `where`"
                                 + " value = `" + whereStr + "` in " + parsingXml);
    }

    if (where == ANOTHER_MODULE) {
      anotherModuleId = attributes.getValue("module-ref-id");
      if (anotherModuleId == null) {
        throw new RuntimeException("KU6TYBd4bJ :: Tag `import` has NO attribute `module-ref-id` in " + parsingXml);
      }
    }

    {
      ret.id              = id;
      ret.name            = name;
      ret.detailId        = detailId;
      ret.where           = where;
      ret.anotherModuleId = anotherModuleId;
    }

  }

  @Override
  protected void startTag(String tagName, Attributes attributes) {}

  @Override
  protected void endTag(String tagName) {}

  @Override
  public void finishParentTag() {
    onFinish.accept(ret);
  }
}
