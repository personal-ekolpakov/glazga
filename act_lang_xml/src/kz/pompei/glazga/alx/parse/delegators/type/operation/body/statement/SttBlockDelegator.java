package kz.pompei.glazga.alx.parse.delegators.type.operation.body.statement;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.Body;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.Expr;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.Statement;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttBlock;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.TypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.BodyDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.ExprDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class SttBlockDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml          parsingXml;
  private final @NonNull SttBlock            ret;
  private final @NonNull Consumer<Statement> onFinish;

  private TypeHandlerDelegate delegate;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) throws Exception {
    ret.pos = SrcPos.from(parsingXml);
  }

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.startElement(tagName, attributes);
        return;
      }
    }

    if ("before-while".equals(tagName)) {
      ret.beforeWhilePos = SrcPos.from(parsingXml);
      delegate           = new ExprDelegate(parsingXml, ret.op, ret.beforeWhile, this::acceptBeforeWhile);
      delegate.beginParentTag(tagName, attributes);
      return;
    }
    if ("before-until".equals(tagName)) {
      ret.beforeUntilPos = SrcPos.from(parsingXml);
      delegate           = new ExprDelegate(parsingXml, ret.op, ret.beforeUntil, this::acceptBeforeUntil);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("after-while".equals(tagName)) {
      ret.afterWhilePos = SrcPos.from(parsingXml);
      delegate          = new ExprDelegate(parsingXml, ret.op, ret.afterWhile, this::acceptAfterWhile);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("after-until".equals(tagName)) {
      ret.afterUntilPos = SrcPos.from(parsingXml);
      delegate          = new ExprDelegate(parsingXml, ret.op, ret.afterUntil, this::acceptAfterUntil);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("init".equals(tagName)) {
      ret.initPos = SrcPos.from(parsingXml);
      delegate    = new BodyDelegate(parsingXml, ret.op, this::acceptInit);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("next".equals(tagName)) {
      ret.nextPos = SrcPos.from(parsingXml);
      delegate    = new BodyDelegate(parsingXml, ret.op, this::acceptNext);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("body".equals(tagName)) {
      ret.bodyPos = SrcPos.from(parsingXml);
      delegate    = new BodyDelegate(parsingXml, ret.op, this::acceptBody);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    throw new RuntimeException("S4jMAxOhOO :: Unknown tag `" + tagName + "` in tag `if` in " + parsingXml);
  }


  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    var d = delegate;
    if (d != null) {
      d.characters(ch, start, length);
    }
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.endElement(tagName);
        return;
      }
    }
  }

  private void acceptInit(Body body) {
    delegate = null;
    if (ret.init != null) {
      throw new RuntimeException("ve3We3niYF :: Tab `init` already exists in tag `block` in " + parsingXml);
    }
    ret.init = body;
  }

  private void acceptNext(Body body) {
    delegate = null;
    if (ret.next != null) {
      throw new RuntimeException("OpLCvxWVL2 :: Tab `next` already exists in tag `block` in " + parsingXml);
    }
    ret.next = body;
  }

  private void acceptBody(Body body) {
    delegate = null;
    if (ret.body != null) {
      throw new RuntimeException("RSa7AD9Otq :: Tab `body` already exists in tag `block` in " + parsingXml);
    }
    ret.body = body;
  }

  private void acceptBeforeWhile(Expr expr) {
    delegate = null;

    if (ret.beforeWhile.taken != null) {
      throw new RuntimeException("alKz25De1a :: Tag `before-while` already exists in tag `block` in " + parsingXml);
    }

    ret.beforeWhile.taken = expr;
  }

  private void acceptBeforeUntil(Expr expr) {
    delegate = null;

    if (ret.beforeUntil.taken != null) {
      throw new RuntimeException("Oph1jl0gFA :: Tag `before-until` already exists in tag `block` in " + parsingXml);
    }

    ret.beforeUntil.taken = expr;
  }

  private void acceptAfterWhile(Expr expr) {
    delegate = null;

    if (ret.afterWhile.taken != null) {
      throw new RuntimeException("6Q2rbJKR1a :: Tag `after-while` already exists in tag `block` in " + parsingXml);
    }

    ret.afterWhile.taken = expr;
  }

  private void acceptAfterUntil(Expr expr) {
    delegate = null;

    if (ret.afterUntil.taken != null) {
      throw new RuntimeException("kmFsf1uty2 :: Tag `after-until` already exists in tag `block` in " + parsingXml);
    }

    ret.afterUntil.taken = expr;
  }

  @Override
  public void finishParentTag() throws Exception {
    onFinish.accept(ret);
  }
}
