package kz.pompei.glazga.alx.parse.delegators.type.operation.body.expr;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.Expr;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.ExprSpaceRef;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.ExprDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class ExprSpaceRefDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml     parsingXml;
  private final @NonNull ExprSpaceRef ret;
  private final @NonNull Consumer<Expr> onFinish;

  private ExprDelegate delegate;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) throws Exception {
    ret.offset    = attributes.getValue("offset");
    ret.inTypeRef = attributes.getValue("in-type-ref");
    ret.pos  = SrcPos.from(parsingXml);
    delegate = new ExprDelegate(parsingXml, ret.op, ret.expr, this::acceptExpr);
  }

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    delegate.startElement(tagName, attributes);
  }

  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    delegate.characters(ch, start, length);
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    delegate.endElement(tagName);
  }

  @Override
  public void finishParentTag() throws Exception {
    delegate.finishParentTag();
  }

  private void acceptExpr(Expr expr) {
    ret.expr.taken = expr;
    onFinish.accept(ret);
  }
}
