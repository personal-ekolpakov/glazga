package kz.pompei.glazga.alx.parse.delegators.type.memory;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.memory.AlxMemorySpace;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.utils.SizeParser;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class MemorySpaceDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml               parsingXml;
  private final @NonNull AlxMemorySpace ret;
  private final @NonNull Consumer<AlxMemorySpace> onFinish;


  @Override
  public void beginParentTag(String tagName, Attributes attributes) throws Exception {
    ret.name            = attributes.getValue("name");
    ret.__sizeByTypeRef = attributes.getValue("size-by-type-ref");
    String sizeStr = attributes.getValue("size");
    ret.__sizeBytes = sizeStr == null ? null : SizeParser.parseSize(sizeStr);
    ret.pos         = SrcPos.from(parsingXml);
  }

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {

  }

  @Override
  protected void endTag(String tagName) throws Exception {

  }

  @Override
  public void finishParentTag() throws Exception {
    onFinish.accept(ret);
  }
}
