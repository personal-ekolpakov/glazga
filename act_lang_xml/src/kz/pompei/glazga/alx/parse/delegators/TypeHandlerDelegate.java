package kz.pompei.glazga.alx.parse.delegators;

import org.xml.sax.Attributes;

public interface TypeHandlerDelegate {

  void beginParentTag(String tagName, Attributes attributes) throws Exception;

  void startElement(String tagName, Attributes attributes) throws Exception;

  default void characters(char[] ch, int start, int length) throws Exception {}

  void endElement(String tagName) throws Exception;

}
