package kz.pompei.glazga.alx.parse.delegators.type.operation.body.statement;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.expr.Expr;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.Statement;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttVar;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.ExprDelegate;
import lombok.NonNull;
import org.xml.sax.Attributes;

public class SttVarDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml          parsingXml;
  private final @NonNull SttVar ret;
  private final @NonNull Consumer<Statement> onFinish;
  private final @NonNull ExprDelegate        exprDelegate;

  public SttVarDelegator(@NonNull ParsingXml parsingXml, @NonNull SttVar ret, @NonNull Consumer<Statement> onFinish) {
    this.parsingXml = parsingXml;
    this.ret     = ret;
    this.onFinish   = onFinish;
    exprDelegate = new ExprDelegate(parsingXml, ret.op, ret.expr, this::acceptExpr);
  }

  @Override
  public void beginParentTag(String tagName, Attributes attributes) throws Exception {
    ret.name    = attributes.getValue("name");
    ret.typeRef = attributes.getValue("type-ref");
    ret.pos = SrcPos.from(parsingXml);
    exprDelegate.beginParentTag(tagName, attributes);
  }

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    exprDelegate.startElement(tagName, attributes);
  }

  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    exprDelegate.characters(ch, start, length);
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    exprDelegate.endElement(tagName);
  }

  @Override
  public void finishParentTag() throws Exception {
    exprDelegate.finishParentTag();
  }

  private void acceptExpr(Expr expr) {

    if (ret.name == null) {
      throw new RuntimeException("hp1KFaNhTp :: No attribute `name` in tag `var` of statements in " + parsingXml);
    }

    ret.expr.taken = expr;
    onFinish.accept(ret);
  }
}
