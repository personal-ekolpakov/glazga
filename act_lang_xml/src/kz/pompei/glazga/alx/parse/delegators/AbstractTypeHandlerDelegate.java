package kz.pompei.glazga.alx.parse.delegators;

import org.xml.sax.Attributes;

public abstract class AbstractTypeHandlerDelegate implements TypeHandlerDelegate {

  protected abstract void startTag(String tagName, Attributes attributes) throws Exception;

  protected abstract void endTag(String tagName) throws Exception;

  public abstract void finishParentTag() throws Exception;

  private int deep = 0;

  @Override
  public final void startElement(String tagName, Attributes attributes) throws Exception {
    deep++;
    startTag(tagName, attributes);
  }

  @Override
  public final void endElement(String tagName) throws Exception {
    if (deep == 0) {
      finishParentTag();
      return;
    }
    endTag(tagName);
    deep--;
  }
}
