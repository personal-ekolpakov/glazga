package kz.pompei.glazga.alx.parse.delegators.type;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.Body;
import kz.pompei.glazga.alx.model.mod.detail.operation.OperationArg;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.TypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.BodyDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.OperationArgDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class OperationDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml          parsingXml;
  private final @NonNull Operation ret;
  private final @NonNull Consumer<Operation> onFinish;

  private TypeHandlerDelegate delegate = null;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) {
    ret.id   = attributes.getValue("id");
    ret.name = attributes.getValue("name");
    ret.pos = SrcPos.from(parsingXml);
  }

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.startElement(tagName, attributes);
        return;
      }
    }

    if ("arg".equals(tagName)) {
      delegate = new OperationArgDelegate(parsingXml, new OperationArg(ret), this::acceptOperationInOut);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("body".equals(tagName)) {
      delegate = new BodyDelegate(parsingXml, ret, this::acceptBody);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    throw new RuntimeException("XmYReqUu1i :: Unknown tag `" + tagName + "` under tag `operation` in " + parsingXml);
  }

  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    var d = delegate;
    if (d != null) {
      d.characters(ch, start, length);
      return;
    }
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.endElement(tagName);
        return;
      }
    }
  }

  private int argOrder = 1;

  private void acceptOperationInOut(OperationArg operationArg) {
    delegate           = null;
    operationArg.order = argOrder++;
    ret.args.put(operationArg.name, operationArg);
  }

  private void acceptBody(Body body) {
    delegate = null;

    if (ret.body != null) {
      throw new RuntimeException("fAjDTaUC8G :: Duplicate body in operation with id = `" + ret.id + "` in " + parsingXml);
    }

    ret.body = body;
  }

  @Override
  public void finishParentTag() {
    onFinish.accept(ret);
  }

}
