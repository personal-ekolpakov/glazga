package kz.pompei.glazga.alx.parse.delegators.type.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.detail.Operation;
import kz.pompei.glazga.alx.model.mod.detail.operation.Body;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.Statement;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttAssign;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttBlock;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttIf;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttReturn;
import kz.pompei.glazga.alx.model.mod.detail.operation.body.stm.SttVar;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.TypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.statement.SttAssignDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.statement.SttBlockDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.statement.SttIfDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.statement.SttReturnDelegator;
import kz.pompei.glazga.alx.parse.delegators.type.operation.body.statement.SttVarDelegator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class BodyDelegate extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml     parsingXml;
  private final @NonNull Operation op;
  private final @NonNull Consumer<Body> onFinish;

  private TypeHandlerDelegate delegate = null;

  private final List<Statement> statements = new ArrayList<>();

  @Override
  public void beginParentTag(String tagName, Attributes attributes) {

  }

  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.startElement(tagName, attributes);
        return;
      }
    }

    if ("assign".equals(tagName)) {
      delegate = new SttAssignDelegator(parsingXml, new SttAssign(op), this::acceptStatement);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("var".equals(tagName)) {
      delegate = new SttVarDelegator(parsingXml, new SttVar(op), this::acceptStatement);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("return".equals(tagName)) {
      delegate = new SttReturnDelegator(parsingXml, new SttReturn(op), this::acceptStatement);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("if".equals(tagName)) {
      delegate = new SttIfDelegator(parsingXml, new SttIf(op), this::acceptStatement);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    if ("block".equals(tagName)) {
      delegate = new SttBlockDelegator(parsingXml, new SttBlock(op), this::acceptStatement);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    throw new RuntimeException("BB1R0MJPrE :: Unknown statement tag `" + tagName + "` in " + parsingXml);
  }

  private void acceptStatement(Statement statement) {
    delegate = null;
    statements.add(statement);
  }

  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    var d = delegate;
    if (d != null) {
      d.characters(ch, start, length);
      return;
    }
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.endElement(tagName);
        return;
      }
    }
  }

  @Override
  public void finishParentTag() {
    onFinish.accept(new Body(statements));
  }
}
