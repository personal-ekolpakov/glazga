package kz.pompei.glazga.alx.parse.delegators.type.memory;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.mod.detail.AlxMemory;
import kz.pompei.glazga.alx.model.mod.detail.memory.AlxMemorySpace;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import kz.pompei.glazga.alx.parse.delegators.TypeHandlerDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class MemoryDelegator extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml          parsingXml;
  private final @NonNull AlxMemory ret;
  private final @NonNull Consumer<AlxMemory> onFinish;

  private TypeHandlerDelegate delegate;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) {

  }


  @Override
  protected void startTag(String tagName, Attributes attributes) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.startElement(tagName, attributes);
      }
    }

    if ("space".equals(tagName)) {
      delegate = new MemorySpaceDelegator(parsingXml, new AlxMemorySpace(ret), this::acceptSpace);
      delegate.beginParentTag(tagName, attributes);
      return;
    }

    throw new RuntimeException("R6deGMcNDV :: Unknown tagName = `" + tagName + "`");
  }

  @Override
  public void characters(char[] ch, int start, int length) throws Exception {
    var d = delegate;
    if (d != null) {
      d.characters(ch, start, length);
    }
  }

  @Override
  protected void endTag(String tagName) throws Exception {
    {
      var d = delegate;
      if (d != null) {
        d.endElement(tagName);
      }
    }
  }

  private void acceptSpace(AlxMemorySpace alxMemorySpace) {
    delegate = null;
    ret.spaces.add(alxMemorySpace);
  }

  @Override
  public void finishParentTag() {
    onFinish.accept(ret);
  }
}
