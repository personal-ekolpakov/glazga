package kz.pompei.glazga.alx.parse.delegators.type.operation;

import java.util.function.Consumer;
import kz.pompei.glazga.alx.model.ann.ArgDir;
import kz.pompei.glazga.alx.model.mod.SrcPos;
import kz.pompei.glazga.alx.model.mod.detail.operation.OperationArg;
import kz.pompei.glazga.alx.parse.ParsingXml;
import kz.pompei.glazga.alx.parse.delegators.AbstractTypeHandlerDelegate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.xml.sax.Attributes;

@RequiredArgsConstructor
public class OperationArgDelegate extends AbstractTypeHandlerDelegate {
  private final @NonNull ParsingXml             parsingXml;
  private final @NonNull OperationArg ret;
  private final @NonNull Consumer<OperationArg> onFinish;

  private String name;
  private String typeRef;
  private String dirStr;
  private SrcPos pos;

  @Override
  public void beginParentTag(String tagName, Attributes attributes) {
    name    = attributes.getValue("name");
    typeRef = attributes.getValue("type-ref");
    dirStr  = attributes.getValue("dir");
    pos = SrcPos.from(parsingXml);
  }

  @Override
  protected void startTag(String tagName, Attributes attributes) {
    throw new RuntimeException("Kr6G8vfMuV :: Unknown tag `" + tagName + "` in `operation/arg` in " + parsingXml);
  }

  @Override
  protected void endTag(String tagName) {}

  @Override
  public void finishParentTag() {

    if (name == null) {
      throw new RuntimeException("r8N8bStvJ4 :: No attribute `name` in tag `operation/arg` in " + parsingXml);
    }
    if (typeRef == null) {
      throw new RuntimeException("cCFk7Df6mD :: No attribute `type-ref` in tag `operation/arg` in " + parsingXml);
    }
    if (dirStr == null) {
      throw new RuntimeException("wtnMhjS1BB :: No attribute `dir` in tag `operation/arg` in " + parsingXml);
    }

    ArgDir dir = ArgDir.valueOfOrNull(dirStr);

    if (dir == null) {
      throw new RuntimeException("CmAP7oNp6i :: Illegal attribute value `dir` = `" + dirStr + "`"
                                 + " in tag `operation/arg` in " + parsingXml);
    }

    {
      ret.name      = name;
      ret.__typeRef = typeRef;
      ret.dir       = dir;
      ret.pos       = pos;
    }

    onFinish.accept(ret);
  }
}
