package kz.pompei.glazga.alx.utils.const_parser;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

public class ParsePart_i4 extends ParsePartParent {

  @Override
  public void append(List<Byte> bytes, String part) {
    int value = parse(part);

    byte[] bb = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE)
                          .order(ByteOrder.LITTLE_ENDIAN)
                          .putInt(value)
                          .array();

    for (final byte b : bb) {
      bytes.add(b);
    }
  }

  private int parse(String part) {
    return switch (inputMode) {
      case DEC -> Integer.parseInt(part);
      case HEX -> Integer.parseInt(part, 16);
    };
  }
}
