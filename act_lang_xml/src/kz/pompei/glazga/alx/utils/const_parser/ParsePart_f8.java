package kz.pompei.glazga.alx.utils.const_parser;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

public class ParsePart_f8 extends ParsePartParent {

  @Override
  public void append(List<Byte> bytes, String part) {

    double value = Double.parseDouble(part);

    byte[] bb = ByteBuffer.allocate(Double.SIZE / Byte.SIZE)
                          .order(ByteOrder.LITTLE_ENDIAN)
                          .putDouble(value)
                          .array();

    for (final byte b : bb) {
      bytes.add(b);
    }
  }

}
