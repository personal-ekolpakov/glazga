package kz.pompei.glazga.alx.utils.const_parser;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

public class ParsePart_i8 extends ParsePartParent {

  @Override
  public void append(List<Byte> bytes, String part) {
    long value = parse(part);

    byte[] bb = ByteBuffer.allocate(Long.SIZE / Byte.SIZE)
                          .order(ByteOrder.LITTLE_ENDIAN)
                          .putLong(value)
                          .array();

    for (final byte b : bb) {
      bytes.add(b);
    }
  }

  private long parse(String part) {
    return switch (inputMode) {
      case DEC -> Long.parseLong(part);
      case HEX -> Long.parseLong(part, 16);
    };
  }

}
