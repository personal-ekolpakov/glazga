package kz.pompei.glazga.alx.utils.const_parser;

import java.util.List;
import lombok.NonNull;

public interface ParsePart {
  void append(List<Byte> bytes, String part);

  void setInputMode(@NonNull InputMode inputMode);

}
