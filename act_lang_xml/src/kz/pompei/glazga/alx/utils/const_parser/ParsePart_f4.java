package kz.pompei.glazga.alx.utils.const_parser;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

public class ParsePart_f4 extends ParsePartParent {

  @Override
  public void append(List<Byte> bytes, String part) {
    float value = Float.parseFloat(part);

    byte[] bb = ByteBuffer.allocate(Float.SIZE / Byte.SIZE)
                          .order(ByteOrder.LITTLE_ENDIAN)
                          .putFloat(value)
                          .array();

    for (final byte b : bb) {
      bytes.add(b);
    }
  }

}
