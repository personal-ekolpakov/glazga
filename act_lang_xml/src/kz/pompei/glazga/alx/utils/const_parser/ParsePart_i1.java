package kz.pompei.glazga.alx.utils.const_parser;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

public class ParsePart_i1 extends ParsePartParent {

  @Override
  public void append(List<Byte> bytes, String part) {
    int value = parsePart(part);

    byte[] bb = ByteBuffer.allocate(1)
                          .order(ByteOrder.LITTLE_ENDIAN)
                          .put((byte) value)
                          .array();

    for (final byte b : bb) {
      bytes.add(b);
    }
  }

  private int parsePart(String part) {
    return switch (inputMode) {
      case DEC -> Integer.parseInt(part);
      case HEX -> Integer.parseInt(part, 16);
    };
  }

}
