package kz.pompei.glazga.alx.utils.const_parser;

import lombok.NonNull;

public abstract class ParsePartParent implements ParsePart {

  protected InputMode inputMode = InputMode.DEC;

  @Override
  public final void setInputMode(@NonNull InputMode inputMode) {
    this.inputMode = inputMode;
  }
}
