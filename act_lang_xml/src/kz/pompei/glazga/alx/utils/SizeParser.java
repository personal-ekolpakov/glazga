package kz.pompei.glazga.alx.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SizeParser {

  private static final Map<String, Long> UNITS = CALC_UNITS();

  @SuppressWarnings("SpellCheckingInspection")
  private static Map<String, Long> CALC_UNITS() {
    Map<String, Long> units = new HashMap<>();

    units.put("BYTE", 1L);
    units.put("BYTES", 1L);

    units.put("KBYTE", 1000L);
    units.put("KBYTES", 1000L);
    units.put("K", 1000L);

    units.put("MBYTE", 1000L * 1000L);
    units.put("MBYTES", 1000L * 1000L);
    units.put("M", 1000L * 1000L);

    units.put("GBYTE", 1000L * 1000L * 1000L);
    units.put("GBYTES", 1000L * 1000L * 1000L);
    units.put("G", 1000L * 1000L * 1000L);

    units.put("KIBYTE", 1024L);
    units.put("KIBYTES", 1024L);
    units.put("KI", 1024L);

    units.put("MIBYTE", 1024L * 1024L);
    units.put("MIBYTES", 1024L * 1024L);
    units.put("MI", 1024L * 1024L);

    units.put("GIBYTE", 1024L * 1024L * 1024L);
    units.put("GIBYTES", 1024L * 1024L * 1024L);
    units.put("GI", 1024L * 1024L * 1024L);

    return Map.copyOf(units);
  }

  private static final Pattern BYTES = Pattern.compile("(\\d+)(\\w+)");

  public static long parseSize(String str) {

    String convertedStr = str.replaceAll("\\s+", "").replaceAll("_", "");

    Matcher matcher = BYTES.matcher(convertedStr);

    if (!matcher.matches()) {
      throw new IllegalArgumentException("s5APrr0jA8 :: Illegal input string");
    }

    final String valueStr = matcher.group(1);
    final String unitStr  = matcher.group(2);
    final long   baseSize = Long.parseUnsignedLong(valueStr);
    final Long   unitSize = UNITS.get(unitStr.toUpperCase());

    if (unitSize == null) {
      throw new RuntimeException("7bg1jo87VO :: Unknown size unit `" + unitStr + "` in input string `" + str + "`");
    }

    return baseSize * unitSize;
  }

}
