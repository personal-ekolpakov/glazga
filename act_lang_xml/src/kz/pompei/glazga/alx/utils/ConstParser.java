package kz.pompei.glazga.alx.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.alx.utils.const_parser.InputMode;
import kz.pompei.glazga.alx.utils.const_parser.ParsePart;
import kz.pompei.glazga.alx.utils.const_parser.ParsePart_f4;
import kz.pompei.glazga.alx.utils.const_parser.ParsePart_f8;
import kz.pompei.glazga.alx.utils.const_parser.ParsePart_i1;
import kz.pompei.glazga.alx.utils.const_parser.ParsePart_i4;
import kz.pompei.glazga.alx.utils.const_parser.ParsePart_i8;
import kz.pompei.glazga.alx.utils.const_parser.ParsePart_u1;
import kz.pompei.glazga.alx.utils.const_parser.ParsePart_u4;
import kz.pompei.glazga.alx.utils.const_parser.ParsePart_u8;

public class ConstParser {

  private static int charInt(char c) {
    if ('0' <= c && c <= '9') {
      return c - '0';
    }
    return 10 + (c - 'a');
  }

  public static byte bbToByte(String bb) {
    return (byte) (charInt(bb.charAt(0)) * 16 + charInt(bb.charAt(1)));
  }


  private static final Map<String, ParsePart> PARSE_PARTS = Map.of(
    "f8", new ParsePart_f8(),
    "f4", new ParsePart_f4(),
    "i1", new ParsePart_i1(),
    "u1", new ParsePart_u1(),
    "i4", new ParsePart_i4(),
    "i8", new ParsePart_i8(),
    "u8", new ParsePart_u8(),
    "u4", new ParsePart_u4()
  );

  public static byte[] parse(String strData) {

    List<Byte> bytes = new ArrayList<>();

    ParsePart parsePart = null;

    for (final String part : strData.trim().split("\\s+")) {
      ParsePart pp = PARSE_PARTS.get(part);
      if (pp != null) {
        parsePart = pp;
        continue;
      }

      if (parsePart == null) {
        throw new RuntimeException("x6i6PFKIn0 :: Type is not defined");
      }

      if ("x".equals(part)) {
        parsePart.setInputMode(InputMode.HEX);
        continue;
      }

      if ("d".equals(part)) {
        parsePart.setInputMode(InputMode.DEC);
        continue;
      }

      parsePart.append(bytes, part);
    }

    byte[] ret = new byte[bytes.size()];
    for (int i = 0; i < bytes.size(); i++) {
      ret[i] = bytes.get(i);
    }
    return ret;
  }

}
