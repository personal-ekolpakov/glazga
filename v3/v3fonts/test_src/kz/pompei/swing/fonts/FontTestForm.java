package kz.pompei.swing.fonts;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.swing.fonts.defau.FontSourceDefault;

public class FontTestForm {
  public static void main(String[] args) {

    SwingUtilities.invokeLater(() -> {

      FontSource fontSource = new FontSourceDefault();

      JFrame frame = new JFrame("Пример дерева");

      Path build = Paths.get("build");

      SizeLocationSaver.into(build.resolve("sizes_and_locations"))
                       .overJFrame("FontTestForm", frame, true);

      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setSize(400, 300);

      frame.setContentPane(new JPanel() {

        @Override
        public void paint(Graphics g1) {
          super.paint(g1);
          Graphics2D g = (Graphics2D) g1;
          g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

          //final String family = "Roboto";
          final String family  = "Playfair Display";
          int          weight1 = 400;
          int          weight2 = 500;
          int          weight3 = 700;
          int          weight4 = 900;

          {
            g.setColor(new Color(0, 74, 218));
            g.setFont(fontSource.get(FontDef.of(family, weight1, 70, false)));
            g.drawString("Привет мир", 100, 100);

            g.setColor(new Color(70, 194, 0));
            g.setFont(fontSource.get(FontDef.of(family, weight2, 70, false)));
            g.drawString("Привет мир", 100, 200);

            g.setColor(new Color(164, 0, 218));
            g.setFont(fontSource.get(FontDef.of(family, weight3, 70, false)));
            g.drawString("Привет мир", 100, 300);

            g.setColor(new Color(153, 6, 201));
            g.setFont(fontSource.get(FontDef.of(family, weight4, 70, false)));
            g.drawString("Привет мир", 100, 400);
          }

          {
            g.setColor(new Color(0, 74, 218));
            g.setFont(fontSource.get(FontDef.of(family, weight1, 70, true)));
            g.drawString("Привет мир", 700, 100);

            g.setColor(new Color(70, 194, 0));
            g.setFont(fontSource.get(FontDef.of(family, weight2, 70, true)));
            g.drawString("Привет мир", 700, 200);

            g.setColor(new Color(164, 0, 218));
            g.setFont(fontSource.get(FontDef.of(family, weight3, 70, true)));
            g.drawString("Привет мир", 700, 300);

            g.setColor(new Color(153, 6, 201));
            g.setFont(fontSource.get(FontDef.of(family, weight4, 70, true)));
            g.drawString("Привет мир", 700, 400);
          }
        }
      });

      frame.setVisible(true);
    });


  }
}
