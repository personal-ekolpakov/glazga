package kz.pompei.swing.fonts;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.swing.fonts.defau.FontSourceDefault;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public class FontTestForm2 {

  @RequiredArgsConstructor(staticName = "of")
  private static class StyledText {
    public final Color  color;
    public final Font   font;
    public final String text;
  }

  public static void main(String[] args) {

    SwingUtilities.invokeLater(() -> {

      FontSource fontSource = new FontSourceDefault();

      JFrame frame = new JFrame("Пример метрик шрифтов");

      Path build = Paths.get("build");

      SizeLocationSaver.into(build.resolve("sizes_and_locations"))
                       .overJFrame("FontTestForm2", frame, true);

      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setSize(400, 300);

      frame.setContentPane(new JPanel() {

        @Override
        public void paint(Graphics g1) {
          super.paint(g1);
          Graphics2D g = (Graphics2D) g1;
          g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

          //final String family = "Roboto";
          final String family1 = "Playfair Display";
          final String family2 = "Roboto";

          final Font font1 = fontSource.get(FontDef.of(family1, 400, 20, false));
          final Font font2 = fontSource.get(FontDef.of(family2, 400, 40, false));
          final Font font3 = fontSource.get(FontDef.of(family1, 800, 12, false));

          Color color1 = new Color(12, 118, 239);
          Color color2 = new Color(3, 74, 154);
          Color color3 = new Color(175, 12, 239);

          StyledText txt1 = StyledText.of(color1, font1, "Добавление слушателя, ");
          StyledText txt2 = StyledText.of(color2, font2, "удаление восклицания, ");
          StyledText txt3 = StyledText.of(color3, font3, "Asd");

          StyledText txt4 = StyledText.of(color1, font1, "Переход ");
          StyledText txt5 = StyledText.of(color2, font2, "на другой ");
          StyledText txt6 = StyledText.of(color3, font3, "уровень");

          Point point = new Point(50, 100);

          point = printTexts(g, point, txt1, txt2, txt3, 15);
          point = printTexts(g, point, txt1, txt2, txt3, 15);
          point = printTexts(g, point, txt4, txt5, txt6, 14);

          System.out.println("6Cb3mK1ya7 :: " + point);

        }
      });

      frame.setVisible(true);
    });


  }

  private static @NonNull Point printTexts(@NonNull Graphics2D g,
                                           @NonNull Point p,
                                           @NonNull StyledText txt1,
                                           @NonNull StyledText txt2,
                                           @NonNull StyledText txt3,
                                           int lineDistance) {
    g.setColor(txt1.color);
    g.setFont(txt1.font);
    g.drawString(txt1.text, p.x, p.y);
    final FontMetrics fm1 = g.getFontMetrics();
    final Rectangle2D sb1 = fm1.getStringBounds(txt1.text, g);
    sb1.setRect(sb1.getX() + p.x, sb1.getY() + p.y, sb1.getWidth(), sb1.getHeight());

    g.draw(sb1);

    int x2 = p.x + (int) Math.round(sb1.getWidth());

    g.setColor(txt2.color);
    g.setFont(txt2.font);
    g.drawString(txt2.text, x2, p.y);

    final FontMetrics fm2 = g.getFontMetrics();
    final Rectangle2D sb2 = fm2.getStringBounds(txt2.text, g);
    sb2.setRect(sb2.getX() + x2, sb2.getY() + p.y, sb2.getWidth(), sb2.getHeight());

    g.draw(sb2);

    final LineMetrics lm2                    = fm2.getLineMetrics(txt2.text, g);
    final float       strikethroughOffset    = lm2.getStrikethroughOffset();
    final float       strikethroughThickness = lm2.getStrikethroughThickness();

    g.setColor(new Color(70, 218, 47));
    final Stroke savedStroke = g.getStroke();
    g.setStroke(new BasicStroke(strikethroughThickness));
    g.drawLine(x2, Math.round(p.y + strikethroughOffset),
               Math.round((float) (x2 + sb2.getWidth())), Math.round(p.y + strikethroughOffset));
    g.setStroke(savedStroke);

    int x3 = x2 + (int) Math.round(sb2.getWidth());

    g.setColor(txt3.color);
    g.setFont(txt3.font);
    g.drawString(txt3.text, x3, p.y);
    final FontMetrics fm3 = g.getFontMetrics();
    final Rectangle2D sb3 = fm3.getStringBounds(txt3.text, g);
    sb3.setRect(sb3.getX() + x3, sb3.getY() + p.y, sb3.getWidth(), sb3.getHeight());

    g.draw(sb3);

    int width = (int) Math.round(sb1.getWidth() + sb2.getWidth() + sb3.getWidth());

    g.setColor(new Color(239, 7, 86));
    g.drawLine(p.x, p.y, p.x + width, p.y);

    {
      final Rectangle2D b1 = fm1.getStringBounds(txt1.text, g);
      final Rectangle2D b2 = fm2.getStringBounds(txt2.text, g);
      final Rectangle2D b3 = fm3.getStringBounds(txt3.text, g);
      return new Point(p.x, p.y + (int) Math.round(extractHeight(b1, b2, b3)) + lineDistance);
    }
  }

  private static double extractHeight(Rectangle2D... rr) {
    double y1 = 0, y2 = 0;
    for (final Rectangle2D r : rr) {
      double Y1 = -r.getY();
      double Y2 = r.getHeight() + r.getY();
      //@formatter:off
      if (y1 < Y1) y1 = Y1;
      if (y2 < Y2) y2 = Y2;
      //@formatter:on
    }
    return y1 + y2;
  }
}
