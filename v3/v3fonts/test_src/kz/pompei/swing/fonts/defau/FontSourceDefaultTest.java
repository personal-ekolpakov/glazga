package kz.pompei.swing.fonts.defau;

import java.util.TreeMap;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FontSourceDefaultTest {

  private static final TreeMap<Integer, String> roboto = new TreeMap<>();

  static {
    roboto.put(100, "test");
    roboto.put(300, "test");
    roboto.put(400, "test");
    roboto.put(500, "test");
    roboto.put(700, "test");
    roboto.put(900, "test");
  }

  @Test
  public void nearestKey__10__100() {

    final int actual = FontSourceDefault.nearestKey(roboto, 10);
    assertThat(actual).isEqualTo(100);
  }

  @Test
  public void nearestKey__250__300() {

    final int actual = FontSourceDefault.nearestKey(roboto, 250);
    assertThat(actual).isEqualTo(300);
  }

  @Test
  public void nearestKey__110__100() {

    final int actual = FontSourceDefault.nearestKey(roboto, 110);
    assertThat(actual).isEqualTo(100);
  }

  @Test
  public void nearestKey__1000__900() {

    final int actual = FontSourceDefault.nearestKey(roboto, 1000);
    assertThat(actual).isEqualTo(900);
  }
}