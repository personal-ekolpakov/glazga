package kz.pompei.swing.fonts.defau;

import java.util.function.Supplier;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FontBytesGetters {
  public final Supplier<byte[]> normal;
  public final Supplier<byte[]> italic;
}
