package kz.pompei.swing.fonts.defau.files.PlayfairDisplay;

public interface FontPlayfairDisplay {
  //@formatter:off
  byte[] Black           ();
  byte[] BlackItalic     ();
  byte[] Bold            ();
  byte[] BoldItalic      ();
  byte[] ExtraBold       ();
  byte[] ExtraBoldItalic ();
  byte[] Medium          ();
  byte[] MediumItalic    ();
  byte[] Regular         ();
  byte[] RegularItalic   ();
  byte[] SemiBold        ();
  byte[] SemiBoldItalic  ();
  //@formatter:on
}
