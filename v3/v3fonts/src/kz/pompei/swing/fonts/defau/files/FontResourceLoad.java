package kz.pompei.swing.fonts.defau.files;

import java.io.InputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Objects;
import lombok.NonNull;

public class FontResourceLoad {
  public static @NonNull <FontInterface> FontInterface load(Class<FontInterface> fontInterface) {
    final InvocationHandler invocationHandler = (proxy, method, args) -> {
      if (method.getParameterCount() > 0) {
        throw new RuntimeException("fxuTk7fVzn :: Illegal method " + method);
      }
      if (method.getParameterCount() == 0) {
        //@formatter:off
        if ("toString" .equals(method.getName())) return FontResourceLoad.class.getSimpleName() + "##PROXY@" + System.identityHashCode(proxy);
        if ("getClass" .equals(method.getName())) return proxy.getClass();
        if ("hashCode" .equals(method.getName())) return proxy.hashCode();
        if ("clone"    .equals(method.getName())) throw new RuntimeException("ZV9G4vmUnN :: No clone");
        if ("notify"   .equals(method.getName())) throw new RuntimeException("ZV9G4vmUnN :: Cannot notify");
        if ("notifyAll".equals(method.getName())) throw new RuntimeException("ZV9G4vmUnN :: Cannot notifyAll");
        if ("wait"     .equals(method.getName())) throw new RuntimeException("ZV9G4vmUnN :: Cannot wait");
        //@formatter:on
      }

      final String resourceName = method.getName() + ".ttf";
      try (InputStream resource = fontInterface.getResourceAsStream(resourceName)) {
        Objects.requireNonNull(resource, "PqcOYIbP3h :: No resource " + method.getName() + " for " + fontInterface);
        //noinspection SuspiciousInvocationHandlerImplementation
        return resource.readAllBytes();
      }
    };
    //noinspection unchecked
    return (FontInterface) Proxy.newProxyInstance(FontResource.class.getClassLoader(),
                                                  new Class[]{fontInterface},
                                                  invocationHandler);
  }
}
