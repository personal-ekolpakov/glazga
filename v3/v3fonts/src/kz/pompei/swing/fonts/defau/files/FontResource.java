package kz.pompei.swing.fonts.defau.files;

public interface FontResource {
  //@formatter:off
  byte[] Roboto__Black        ();
  byte[] Roboto__BlackItalic  ();
  byte[] Roboto__Bold         ();
  byte[] Roboto__BoldItalic   ();
  byte[] Roboto__Italic       ();
  byte[] Roboto__Light        ();
  byte[] Roboto__LightItalic  ();
  byte[] Roboto__Medium       ();
  byte[] Roboto__MediumItalic ();
  byte[] Roboto__Regular      ();
  byte[] Roboto__Thin         ();
  byte[] Roboto__ThinItalic   ();
  //@formatter:on
}
