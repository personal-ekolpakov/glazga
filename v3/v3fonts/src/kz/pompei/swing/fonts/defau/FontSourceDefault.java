package kz.pompei.swing.fonts.defau;

import java.awt.Font;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import kz.pompei.swing.fonts.FontDef;
import kz.pompei.swing.fonts.FontSource;
import kz.pompei.swing.fonts.defau.files.FontResourceLoad;
import kz.pompei.swing.fonts.defau.files.PlayfairDisplay.FontPlayfairDisplay;
import kz.pompei.swing.fonts.defau.files.Roboto.FontRoboto;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;

import static java.util.Objects.requireNonNull;

public class FontSourceDefault implements FontSource {

  private static final Map<String, TreeMap<Integer, FontBytesGetters>> families = new HashMap<>();

  static {
    {
      final var load = FontResourceLoad.load(FontRoboto.class);
      final var font = new TreeMap<Integer, FontBytesGetters>();
      font.put(100, new FontBytesGetters(load::Thin, load::ThinItalic));
      font.put(300, new FontBytesGetters(load::Light, load::LightItalic));
      font.put(400, new FontBytesGetters(load::Regular, load::Italic));
      font.put(500, new FontBytesGetters(load::Medium, load::MediumItalic));
      font.put(700, new FontBytesGetters(load::Bold, load::BoldItalic));
      font.put(900, new FontBytesGetters(load::Black, load::BlackItalic));
      families.put("Roboto", font);
    }
    {
      final var load = FontResourceLoad.load(FontPlayfairDisplay.class);
      final var font = new TreeMap<Integer, FontBytesGetters>();
      font.put(400, new FontBytesGetters(load::Regular, load::RegularItalic));
      font.put(500, new FontBytesGetters(load::Medium, load::MediumItalic));
      font.put(600, new FontBytesGetters(load::SemiBold, load::SemiBoldItalic));
      font.put(700, new FontBytesGetters(load::Bold, load::BoldItalic));
      font.put(800, new FontBytesGetters(load::ExtraBold, load::ExtraBoldItalic));
      font.put(900, new FontBytesGetters(load::Black, load::BlackItalic));
      families.put("Playfair Display", font);

    }
  }

  static int nearestKey(@NonNull TreeMap<Integer, ?> roboto, int weight) {
    final Integer ceilingKey = roboto.ceilingKey(weight);
    final Integer floorKey   = roboto.floorKey(weight);
    if (ceilingKey == null && floorKey == null) {
      throw new RuntimeException("4v0hg3en1c :: No Roboto");
    }

    return floorKey == null
      ? ceilingKey
      : ceilingKey == null || ceilingKey - weight > weight - floorKey
      ? floorKey
      : ceilingKey;
  }

  @ToString
  @EqualsAndHashCode
  @RequiredArgsConstructor
  private static class FontId {
    public final String  family;
    public final int     weight;
    public final boolean italic;
  }

  private static final ConcurrentHashMap<FontId, Font> cache = new ConcurrentHashMap<>();

  @SneakyThrows
  private static @NonNull Font select(@NonNull TreeMap<Integer, FontBytesGetters> fontMap, @NonNull FontDef fontDef) {
    FontId id = new FontId(fontDef.family, fontDef.weight, fontDef.italic);

    Font font = cache.get(id);

    if (font == null) {
      synchronized (cache) {
        font = cache.get(id);
        if (font == null) {
          final int key = nearestKey(fontMap, fontDef.weight);
          byte[]    bb  = fontDef.italic ? fontMap.get(key).italic.get() : fontMap.get(key).normal.get();
          font = Font.createFont(Font.TRUETYPE_FONT, new ByteArrayInputStream(bb));
          cache.put(id, font);
        }
      }
    }

    requireNonNull(font, "GwDMDjyCtZ :: No font " + id);

    return font.deriveFont(fontDef.size);
  }

  private @NonNull TreeMap<Integer, FontBytesGetters> getFontMap(@NonNull String family) {
    return requireNonNull(families.get(family), "PO1BAHvI8m :: No font family = `" + family + "`");
  }

  @Override
  public @NonNull Font get(@NonNull FontDef fontDef) {
    return select(getFontMap(fontDef.family), fontDef);
  }

  @Override
  public List<String> families() {
    return families.keySet().stream().sorted().toList();
  }
}
