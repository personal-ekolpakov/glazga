package kz.pompei.swing.fonts.defau.files.Roboto;

public interface FontRoboto {
  //@formatter:off
  byte[] Black();
  byte[] BlackItalic();
  byte[] Bold();
  byte[] BoldItalic();
  byte[] Italic();
  byte[] Light();
  byte[] LightItalic();
  byte[] Medium();
  byte[] MediumItalic();
  byte[] Regular();
  byte[] Thin();
  byte[] ThinItalic();
  //@formatter:on
}
