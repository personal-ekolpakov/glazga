package kz.pompei.swing.fonts;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode
@RequiredArgsConstructor(staticName = "of")
public class FontDef {
  public final String  family;
  public final int     weight;
  public final float   size;
  public final boolean italic;
}
