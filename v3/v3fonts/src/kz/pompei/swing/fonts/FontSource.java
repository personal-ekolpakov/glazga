package kz.pompei.swing.fonts;

import java.awt.Font;
import java.util.List;
import lombok.NonNull;

public interface FontSource {

  @NonNull Font get(@NonNull FontDef fontDef);

  List<String> families();

}
