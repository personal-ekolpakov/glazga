package kz.pompei.glazga.convert.obj_access;

import java.lang.reflect.Type;
import java.util.Set;
import lombok.NonNull;

public interface ObjAccess {

  /**
   * Создаёт инстанцию данного класса
   *
   * @return инстанция данного класса, или null, если данный класс абстрактный
   */
  Object createInstance();

  @NonNull Set<String> fieldNames();

  void setField(@NonNull String fieldName, @NonNull Object object, Object value);

  Object getField(@NonNull String fieldName, @NonNull Object object);

  Type getterValueType(@NonNull String fieldName);

  Type setterValueType(@NonNull String fieldName);

  /**
   * Определяет поле, которое определяет дочерний класс
   * <p>
   * Если значения поля, которое указывается значением этого метода, получиться null, то нужно использовать данный класс для получения
   * значения. Если данный класс абстрактный, то нужно вернуть null.
   *
   * @return имя поля, определяющее дочерний класс. Если вернёт null, то данный класс не определяется дочерними через дискриминатор.
   */
  String childDetectFieldName();

  /**
   * Определяет дочерний класс по значению дискриминатора
   *
   * @param classDetector значение поля дискриминатора
   * @return класс, который соответствует значению поля, или null, если такого класса не найдено - нужно вернуть null
   */
  Class<?> getChildClass(@NonNull String classDetector);

  /**
   * Предоставляет значения поля определения данного класса при дематериализации
   *
   * @return значение поля и имя поля. Если null, то для данного класса нет дискриминации.
   */
  Discriminate discriminate();
}
