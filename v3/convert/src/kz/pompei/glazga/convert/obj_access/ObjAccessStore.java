package kz.pompei.glazga.convert.obj_access;

import java.util.concurrent.ConcurrentHashMap;
import lombok.NonNull;

public class ObjAccessStore {
  private final ConcurrentHashMap<Class<?>, ObjAccess> store = new ConcurrentHashMap<>();

  public @NonNull ObjAccess get(@NonNull Class<?> objectClass) {

    if (objectClass.isEnum()) {
      throw new RuntimeException("wg8dUCU2H8 :: Enums cannot be dismembered");
    }

    {
      ObjAccess ret = store.get(objectClass);
      if (ret != null) {
        return ret;
      }
    }

    synchronized (store) {
      {
        ObjAccess ret = store.get(objectClass);
        if (ret != null) {
          return ret;
        }
      }
      {
        ObjAccess ret = ObjAccessCreator.create(objectClass);
        store.put(objectClass, ret);
        return ret;
      }
    }
  }

}
