package kz.pompei.glazga.convert.obj_access;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Discriminate {
  public final @NonNull String fieldName;
  public final @NonNull String fieldValue;
}
