package kz.pompei.glazga.convert.obj_access;

import java.lang.reflect.Type;
import lombok.NonNull;

public interface FieldGetter {

  Object get(@NonNull Object object);

  @NonNull Type getType();

}
