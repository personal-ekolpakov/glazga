package kz.pompei.glazga.convert.obj_access;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.convert.DiscriminatorPostfix;
import lombok.NonNull;
import lombok.SneakyThrows;

public class ObjAccessCreator {

  public static @NonNull ObjAccess create(@NonNull Class<?> objectClass) {

    Map<String, FieldGetter> getters = new HashMap<>();
    Map<String, FieldSetter> setters = new HashMap<>();

    fillAcceptors(objectClass, getters, setters);

    HashSet<String> tmp = new HashSet<>(getters.keySet());
    tmp.addAll(setters.keySet());

    final Set<String> keys = Set.copyOf(tmp);

    return new ObjAccess() {
      @Override public Object createInstance() {
        try {
          return objectClass.getConstructor().newInstance();
        } catch (InstantiationException | NoSuchMethodException e) {
          return null;
        } catch (IllegalAccessException | InvocationTargetException e) {
          throw new RuntimeException(e);
        }
      }

      @Override public @NonNull Set<String> fieldNames() {
        return keys;
      }

      @Override public Type getterValueType(@NonNull String fieldName) {
        FieldGetter fieldGetter = getters.get(fieldName);
        return fieldGetter == null ? null : fieldGetter.getType();
      }

      @Override public Object getField(@NonNull String fieldName, @NonNull Object object) {
        FieldGetter fieldGetter = getters.get(fieldName);
        return fieldGetter == null ? null : fieldGetter.get(object);
      }

      @Override public Type setterValueType(@NonNull String fieldName) {
        FieldSetter fieldSetter = setters.get(fieldName);
        return fieldSetter == null ? null : fieldSetter.getType();
      }

      @Override public void setField(@NonNull String fieldName, @NonNull Object object, Object value) {
        FieldSetter fieldSetter = setters.get(fieldName);
        if (fieldSetter != null) {
          fieldSetter.set(object, value);
        }
      }

      @Override public String childDetectFieldName() {

        {
          DiscriminatorPostfix dp = objectClass.getAnnotation(DiscriminatorPostfix.class);
          if (dp != null) {
            return dp.value();
          }
        }

        return null;
      }

      @Override public Class<?> getChildClass(@NonNull String classDetector) {
        {
          DiscriminatorPostfix dp = objectClass.getAnnotation(DiscriminatorPostfix.class);
          if (dp != null) {
            try {
              return Class.forName(objectClass.getName() + classDetector);
            } catch (ClassNotFoundException e) {
              return null;
            }
          }
        }

        return null;
      }

      final Discriminate discriminate = extractDiscriminate(objectClass);

      @Override public Discriminate discriminate() {
        return discriminate;
      }
    };
  }

  private static Discriminate extractDiscriminate(@NonNull Class<?> objectClass) {
    Class<?> current = objectClass.getSuperclass();

    while (current != null) {

      {
        DiscriminatorPostfix dp = current.getAnnotation(DiscriminatorPostfix.class);
        if (dp != null) {

          String superClassName = current.getName();
          String thisClassName  = objectClass.getName();

          if (!thisClassName.startsWith(superClassName)) {
            throw new RuntimeException("ig3D70c0G7 :: Annotation @" + DiscriminatorPostfix.class.getSimpleName()
                                       + " needs that child class name MUST starts with super class name");
          }

          String fieldName  = dp.value();
          String fieldValue = thisClassName.substring(superClassName.length());

          return new Discriminate(fieldName, fieldValue);
        }
      }

      current = current.getSuperclass();
    }

    return null;
  }

  private static void fillAcceptors(@NonNull Class<?> objectClass,
                                    @NonNull Map<String, FieldGetter> getters,
                                    @NonNull Map<String, FieldSetter> setters) {

    for (final Field field : objectClass.getFields()) {

      getters.put(field.getName(), new FieldGetter() {
        @Override @SneakyThrows public Object get(@NonNull Object object) {
          return field.get(object);
        }

        @Override public @NonNull Type getType() {
          return field.getGenericType();
        }
      });

      setters.put(field.getName(), new FieldSetter() {
        @Override @SneakyThrows public void set(@NonNull Object object, Object fieldValue) {
          field.set(object, fieldValue);
        }

        @Override public @NonNull Type getType() {
          return field.getGenericType();
        }
      });

    }

    for (final Method method : objectClass.getMethods()) {

      if (Modifier.isStatic(method.getModifiers())) {
        continue;
      }

      @NonNull final String   methodName = method.getName();
      @NonNull final Class<?> returnType = method.getReturnType();

      if ("getClass".equals(methodName)) {
        continue;
      }

      if (method.getParameterTypes().length == 0 && returnType != null && returnType != Void.class) {

        String fieldName = null;

        if (methodName.length() >= 4 && methodName.startsWith("get")) {
          String S = methodName.substring(3, 4);
          if (S.equals(S.toUpperCase())) {
            fieldName = S.toLowerCase() + methodName.substring(4);
          }
        } else if (methodName.length() >= 3 && methodName.startsWith("is")) {
          String S = methodName.substring(2, 3);
          if (S.equals(S.toUpperCase())) {
            fieldName = S.toLowerCase() + methodName.substring(3);
          }
        }

        if (fieldName != null) {
          getters.put(fieldName, new FieldGetter() {
            @Override @SneakyThrows public Object get(@NonNull Object object) {
              return method.invoke(object);
            }

            @Override public @NonNull Type getType() {
              return method.getGenericReturnType();
            }
          });
          continue;
        }
      }

      if (method.getParameterTypes().length == 1 && returnType == Void.class
          && methodName.length() >= 4 && methodName.startsWith("set")) {


        String S = methodName.substring(3, 4);
        if (S.equals(S.toUpperCase())) {
          String fieldName = S.toLowerCase() + methodName.substring(4);
          setters.put(fieldName, new FieldSetter() {
            @Override @SneakyThrows public void set(@NonNull Object object, Object fieldValue) {
              method.invoke(object, fieldValue);
            }

            @Override public @NonNull Type getType() {
              return method.getGenericParameterTypes()[0];
            }
          });
          continue;
        }

      }

    }
  }

}
