package kz.pompei.glazga.convert.obj_access;

import java.lang.reflect.Type;
import lombok.NonNull;

public interface FieldSetter {

  void set(@NonNull Object object, Object fieldValue);

  @NonNull Type getType();

}
