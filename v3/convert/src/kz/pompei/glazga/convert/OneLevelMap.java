package kz.pompei.glazga.convert;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.convert.model.MapValue;
import kz.pompei.glazga.convert.model.OverMap;
import kz.pompei.glazga.convert.model.OverStr;
import lombok.NonNull;

public class OneLevelMap {

  public static Map<String, MapValue> makeMaterial(@NonNull Map<String, String> mapOneLevel) {
    Map<String, MapValue> ret = new HashMap<>();

    for (final String key : mapOneLevel.keySet().stream().sorted().toList()) {
      String value = mapOneLevel.get(key);
      if (value != null) {
        setDeepValue(ret, key, value);
      }
    }

    return ret;
  }

  private static void setDeepValue(@NonNull Map<String, MapValue> target, @NonNull String key, @NonNull String value) {

    @NonNull Map<String, MapValue> current = target;

    final String[] path = key.split("\\.");
    final int      last = path.length - 1;

    for (int i = 0; i < last; i++) {
      final String   path_i   = path[i];
      final MapValue mapValue = current.get(path_i);

      if (mapValue instanceof OverMap om) {
        current = om.luggage;
      } else {
        OverMap newMap = OverMap.of(new HashMap<>());
        current.put(path_i, newMap);
        current = newMap.luggage;
      }
    }

    current.put(path[last], OverStr.of(value));
  }

  public static Map<String, String> makeOneLevel(Map<String, MapValue> material) {
    Map<String, String> ret = new HashMap<>();
    append(ret, material);
    return ret;
  }

  public static void append(@NonNull Map<String, String> oneLevelMap, @NonNull Map<String, MapValue> material) {
    append0(oneLevelMap, "", material);
  }

  private static void append0(@NonNull Map<String, String> target, @NonNull String prefix, @NonNull Map<String, MapValue> material) {

    for (final Map.Entry<String, MapValue> e : material.entrySet()) {

      final String   key      = e.getKey();
      final MapValue mapValue = e.getValue();

      if (mapValue == null) {
        continue;
      }

      if (mapValue instanceof final OverStr os) {
        target.put(prefix + key, os.luggage);
        continue;
      }

      if (mapValue instanceof final OverMap om) {
        append0(target, prefix + key + '.', om.luggage);
        continue;
      }

      throw new RuntimeException("nklB4CFVkz :: Unknown class of " + mapValue.getClass());
    }

  }

}
