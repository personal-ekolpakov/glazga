package kz.pompei.glazga.convert.str_objec_conv;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.concurrent.ConcurrentHashMap;
import kz.pompei.glazga.convert.StringRepresented;
import lombok.NonNull;
import lombok.SneakyThrows;

public class StrObjConverters {
  private final ConcurrentHashMap<Class<?>, StrObjConverter> storage = new ConcurrentHashMap<>();

  public StrObjConverter get(@NonNull Class<?> objectClass) {

    if (objectClass.getAnnotation(StringRepresented.class) == null) {
      return null;
    }

    {
      StrObjConverter strObjConverter = storage.get(objectClass);
      if (strObjConverter != null) {
        return strObjConverter;
      }
    }

    synchronized (storage) {
      {
        StrObjConverter strObjConverter = storage.get(objectClass);
        if (strObjConverter != null) {
          return strObjConverter;
        }
      }

      {
        StrObjConverter strObjConverter = createStrObjConverter(objectClass);
        storage.put(objectClass, strObjConverter);
        return strObjConverter;
      }
    }
  }

  private static @NonNull StrObjConverter createStrObjConverter(@NonNull Class<?> objectClass) {

    Method methodStaticCreate = null;
    Method methodToString     = null;

    Class<?> current = objectClass;

    while (current != null && (methodStaticCreate == null || methodToString == null)) {

      for (final Method method : current.getMethods()) {

        if (method.getAnnotation(StringRepresented.class) == null) {
          continue;
        }

        if (Modifier.isStatic(method.getModifiers())) {

          if (method.getParameterTypes().length != 1) {
            throw new RuntimeException("10aI3JXT6k :: @" + StringRepresented.class.getSimpleName()
                                       + " static method MUST contains only one argument with type String");
          }

          if (method.getParameterTypes()[0] != String.class) {
            throw new RuntimeException("195TtqXCUz :: @" + StringRepresented.class.getSimpleName()
                                       + " static method argument type MUST be String");
          }

          if (!current.isAssignableFrom(method.getReturnType())) {
            throw new RuntimeException("tcnXpsRn3J :: Static method, marked by @" + StringRepresented.class.getSimpleName()
                                       + " MUST return type isAssignableFrom(" + current + "), but is not."
                                       + " Looking method is " + method);
          }

          if (methodStaticCreate != null) {
            throw new RuntimeException("wYGHDmCfQe :: There are two create static method found: "
                                       + method + " and " + methodStaticCreate);
          }

          methodStaticCreate = method;
          continue;
        }

        {
          if (method.getReturnType() != String.class) {
            throw new RuntimeException("oIKJyBOiTD :: @" + StringRepresented.class.getSimpleName() +
                                       " to string convert method MUST returns String, but it is not: " + method);
          }

          if (method.getParameterTypes().length > 0) {
            throw new RuntimeException("ThQr2GveTe :: @" + StringRepresented.class.getSimpleName() +
                                       " to string convert method arguments MUST be absent, but it is not: " + method);
          }

          if (methodToString != null) {
            throw new RuntimeException("iJt3J78bMW :: Found two @" + StringRepresented.class.getSimpleName() +
                                       " methods to convert into string: " + method + " AND " + methodToString);
          }

          methodToString = method;
        }
      }

      current = current.getSuperclass();
    }

    if (methodStaticCreate == null) {
      throw new RuntimeException("wmcGUvc1fq :: Cannot find @" + StringRepresented.class.getSimpleName() +
                                 " static method to convert string into class " + objectClass + " in this class");
    }
    if (methodToString == null) {
      throw new RuntimeException("SvHe1ob8fg :: Cannot find @" + StringRepresented.class.getSimpleName() +
                                 " NONE static method to convert object " + objectClass + " into string in this class");
    }

    @NonNull Method methodStaticCreate_final = methodStaticCreate;
    @NonNull Method methodToString_final     = methodToString;

    return new StrObjConverter() {
      @Override @SneakyThrows public String toStr(Object value) {
        return (String) methodToString_final.invoke(value);
      }

      @Override @SneakyThrows public Object fromStr(String value) {
        return methodStaticCreate_final.invoke(null, value);
      }
    };
  }
}
