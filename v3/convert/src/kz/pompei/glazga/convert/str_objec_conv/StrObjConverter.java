package kz.pompei.glazga.convert.str_objec_conv;

public interface StrObjConverter {

  String toStr(Object value);

  Object fromStr(String value);

}
