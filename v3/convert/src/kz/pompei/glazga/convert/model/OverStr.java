package kz.pompei.glazga.convert.model;

import java.util.Objects;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class OverStr extends MapValue {
  public final @NonNull String luggage;

  @Override public String toString() {
    return '«' + luggage + '»';
  }

  public static OverStr of(String value) {
    return value == null ? null : new OverStr(value);
  }

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final OverStr overStr = (OverStr) o;
    return Objects.equals(luggage, overStr.luggage);
  }

  @Override public int hashCode() {
    return Objects.hashCode(luggage);
  }
}
