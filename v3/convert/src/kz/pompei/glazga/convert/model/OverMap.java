package kz.pompei.glazga.convert.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static java.util.stream.Collectors.joining;

@RequiredArgsConstructor
public class OverMap extends MapValue {
  public final @NonNull Map<String, MapValue> luggage;

  public static OverMap of(Map<String, MapValue> value) {
    if (value == null) {
      return new OverMap(new HashMap<>());
    }
    if (value instanceof HashMap<String, MapValue>) {
      return new OverMap(value);
    }
    return new OverMap(new HashMap<>(value));
  }

  @Override public String toString() {
    List<String> list = new ArrayList<>();
    for (final String key : luggage.keySet().stream().sorted().toList()) {
      list.add(key + '=' + luggage.get(key));
    }
    return list.stream().collect(joining(",", "MAP{", "}"));
  }

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final OverMap overMap = (OverMap) o;
    return Objects.equals(luggage, overMap.luggage);
  }

  @Override public int hashCode() {
    return Objects.hashCode(luggage);
  }
}
