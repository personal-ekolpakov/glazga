package kz.pompei.glazga.convert;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.convert.model.MapValue;
import kz.pompei.glazga.convert.model.OverMap;
import kz.pompei.glazga.convert.model.OverStr;
import lombok.NonNull;

public class PrintUtil {
  public static void printMap(String placeId, MapValue mapValue) {
    printMap(System.out, placeId, mapValue);
  }

  public static void printMap(PrintStream out, String placeId, MapValue mapValue) {
    List<String> list = mapValueToLines(mapValue);
    if (list.isEmpty()) {
      return;
    }
    out.println(placeId + " :: mapValue = " + list.get(0));
    for (int i = 1; i < list.size(); i++) {
      out.println(list.get(i));
    }
  }

  public static List<String> mapValueToLines(MapValue mapValue) {
    final int TAB = 2;
    if (mapValue == null) {
      return List.of("NULL");
    }
    if (mapValue instanceof OverStr os) {
      return List.of(os.toString());
    }
    if (mapValue instanceof OverMap om) {
      List<String> ret = new ArrayList<>();
      ret.add("{");
      Map<String, MapValue> luggage = om.luggage;
      List<String>          keys    = luggage.keySet().stream().sorted().toList();
      for (String key : keys) {
        List<String> subList = mapValueToLines(luggage.get(key));
        if (subList.isEmpty()) {
          continue;
        }
        ret.add(" ".repeat(TAB) + key + " = " + subList.get(0));

        for (int i = 1; i < subList.size(); i++) {
          ret.add((" ".repeat(TAB)) + subList.get(i));
        }
      }
      ret.add("}");
      return ret;
    }
    throw new RuntimeException("vnkFWmMQqq :: Unknown mapValue class " + mapValue.getClass());
  }

  public static void printOneLevel(@NonNull String placeId, @NonNull Map<String, String> oneLevelMap) {
    for (final String key : oneLevelMap.keySet().stream().sorted().toList()) {
      System.out.println(placeId + " :: " + key + " = " + oneLevelMap.get(key));
    }
  }
}
