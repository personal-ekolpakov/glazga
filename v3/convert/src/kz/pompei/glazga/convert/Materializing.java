package kz.pompei.glazga.convert;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.convert.model.MapValue;
import kz.pompei.glazga.convert.model.OverMap;
import kz.pompei.glazga.convert.model.OverStr;
import kz.pompei.glazga.convert.obj_access.Discriminate;
import kz.pompei.glazga.convert.obj_access.ObjAccess;
import kz.pompei.glazga.convert.obj_access.ObjAccessStore;
import kz.pompei.glazga.convert.str_objec_conv.StrObjConverter;
import kz.pompei.glazga.convert.str_objec_conv.StrObjConverters;
import lombok.NonNull;

import static java.util.Objects.requireNonNull;

public class Materializing {

  private final StrObjConverters strObjConverters = new StrObjConverters();
  private final ObjAccessStore   objAccessStore   = new ObjAccessStore();

  public MapValue materialize(Object object) {

    if (object == null) {
      return null;
    }

    if (false
        || object instanceof Character
        || object instanceof Byte
        || object instanceof Short
        || object instanceof Integer
        || object instanceof Long
        || object instanceof Float
        || object instanceof Double
        || object instanceof Boolean
        || object instanceof BigDecimal
        || object instanceof BigInteger
    ) {
      return OverStr.of(object.toString());
    }

    if (object instanceof Date d) {
      SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
      return OverStr.of(sdf.format(d));
    }

    if (object instanceof String s) {
      return OverStr.of(s);
    }

    if (object instanceof byte[] bb) {
      return OverStr.of(Base64.getEncoder().encodeToString(bb));
    }

    @NonNull Class<?> objectClass = object.getClass();

    {
      StrObjConverter strObjConverter = strObjConverters.get(objectClass);
      if (strObjConverter != null) {
        return OverStr.of(strObjConverter.toStr(object));
      }
    }

    if (object instanceof Collection) {
      throw new RuntimeException("LN6iFXSycC :: Collection cannot be converted to Map");
    }

    if (objectClass.isEnum()) {
      return OverStr.of(((Enum<?>) object).name());
    }

    if (object instanceof Map<?, ?> map) {

      Map<String, MapValue> ret = new HashMap<>();

      for (final Map.Entry<?, ?> e : map.entrySet()) {
        Object value = e.getValue();
        if (value == null) {
          continue;
        }

        MapValue mapValueKey = materialize(e.getKey());

        if (!(mapValueKey instanceof OverStr os)) {
          throw new RuntimeException("7qMMxyIm1z :: Key of map MUST be string represented");
        }

        MapValue mapValueValue = materialize(e.getValue());

        if (mapValueValue != null) {
          ret.put(os.luggage, mapValueValue);
        }
      }

      return OverMap.of(ret);
    }

    {
      Map<String, MapValue> ret          = new HashMap<>();
      ObjAccess             objAccess    = objAccessStore.get(objectClass);
      Discriminate          discriminate = objAccess.discriminate();

      if (discriminate != null) {
        ret.put(discriminate.fieldName, OverStr.of(discriminate.fieldValue));
      }

      for (final String fieldName : objAccess.fieldNames()) {
        MapValue mapValue = materialize(objAccess.getField(fieldName, object));
        if (mapValue != null) {
          ret.put(fieldName, mapValue);
        }
      }
      return OverMap.of(ret);
    }
  }

  private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";

  public <T> T dematerialize(MapValue mapValue, @NonNull Type resultType) {
    //noinspection unchecked
    return (T) dematerialize0(mapValue, resultType);
  }

  private Object dematerialize0(MapValue mapValue, @NonNull Type resultType) {
    if (mapValue == null) {
      return null;
    }

    if (mapValue instanceof OverStr os) {
      @NonNull String str = os.luggage;

      if (resultType == String.class) {
        return str;
      }

      if (resultType == Date.class) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
          return sdf.parse(str);
        } catch (ParseException | NullPointerException e) {
          return null;
        }
      }

      if (resultType == BigDecimal.class) {
        try {
          return new BigDecimal(str);
        } catch (NumberFormatException e) {
          return null;
        }
      }

      if (resultType == BigInteger.class) {
        try {
          return new BigInteger(str);
        } catch (NumberFormatException e) {
          return null;
        }
      }

      if (resultType == Integer.class || resultType == int.class) {
        try {
          return Integer.parseInt(str);
        } catch (NumberFormatException e) {
          return null;
        }
      }

      if (resultType == Long.class || resultType == long.class) {
        try {
          return Long.parseLong(str);
        } catch (NumberFormatException e) {
          return null;
        }
      }

      if (resultType == Short.class || resultType == short.class) {
        try {
          return Short.parseShort(str);
        } catch (NumberFormatException e) {
          return null;
        }
      }

      if (resultType == Byte.class || resultType == byte.class) {
        try {
          return Byte.parseByte(str);
        } catch (NumberFormatException e) {
          return null;
        }
      }
      if (resultType == Double.class || resultType == double.class) {
        try {
          return Double.parseDouble(str);
        } catch (NumberFormatException e) {
          return null;
        }
      }

      if (resultType == Float.class || resultType == float.class) {
        try {
          return Float.parseFloat(str);
        } catch (NumberFormatException e) {
          return null;
        }
      }
      if (resultType == Boolean.class || resultType == boolean.class) {
        String trim = str.trim();
        return "yes".equals(trim) || "y".equals(trim) || "1".equals(trim)
               || "true".equals(trim) || "on".equals(trim) || "да".equals(trim);
      }

      if (resultType == Character.class || resultType == char.class) {
        if (str.isEmpty()) {
          return null;
        }
        return str.charAt(0);
      }

      if (resultType == byte[].class) {
        try {
          return Base64.getDecoder().decode(str);
        } catch (IllegalArgumentException e) {
          return null;
        }
      }

      //noinspection rawtypes
      if (resultType instanceof Class cls) {

        if (cls.isEnum()) {
          try {
            //noinspection unchecked
            return Enum.valueOf(cls, str);
          } catch (IllegalArgumentException e) {
            return null;
          }
        }

        //noinspection unchecked
        if (cls.getAnnotation(StringRepresented.class) != null) {
          return requireNonNull(strObjConverters.get(cls)).fromStr(str);
        }
      }

      return null;
    }

    if (mapValue instanceof OverMap om) {
      Map<String, MapValue> map = om.luggage;

      if (resultType instanceof Class<?> cl) {
        @NonNull ObjAccess objAccess = objAccessStore.get(cl);

        String childDetectFieldName = objAccess.childDetectFieldName();

        if (childDetectFieldName != null) {

          String classDetector = getAsString(map, childDetectFieldName);

          if (classDetector != null) {
            Class<?> childClass = objAccess.getChildClass(classDetector);
            if (childClass == null) {
              throw new RuntimeException("6ABITtV1XC :: Cannot detect child class for"
                                         + " fieldName = `" + childDetectFieldName + "`"
                                         + " and fieldValue = `" + classDetector + "` of class " + cl);
            }
            objAccess = objAccessStore.get(childClass);
          }

        }

        @NonNull final Object ret = objAccess.createInstance();

        if (ret == null) {
          return null;
        }

        for (final String fieldName : objAccess.fieldNames()) {
          MapValue fieldMapValue = map.get(fieldName);
          if (fieldMapValue != null) {
            Type fieldType = objAccess.setterValueType(fieldName);
            if (fieldType != null) {
              Object fieldValue = dematerialize0(fieldMapValue, fieldType);
              if (fieldValue != null) {
                objAccess.setField(fieldName, ret, fieldValue);
              }
            }
          }
        }

        return ret;
      }

      if (resultType instanceof ParameterizedType pt) {

        if (pt.getRawType() == Map.class) {

          Type[] typeArguments = pt.getActualTypeArguments();
          Type   typeKey       = typeArguments[0];
          Type   typeValue     = typeArguments[1];

          Map<Object, Object> ret = new HashMap<>();

          for (final Map.Entry<String, MapValue> e : map.entrySet()) {

            Object key   = dematerialize(OverStr.of(e.getKey()), typeKey);
            Object value = dematerialize(e.getValue(), typeValue);

            if (key != null && value != null) {
              ret.put(key, value);
            }

          }

          return ret;
        }

      }

      return null;
    }

    throw new IllegalArgumentException("RkFJ9JI8kf :: Unknown map value type = " + mapValue.getClass());
  }

  private static String getAsString(@NonNull Map<String, MapValue> map, String key) {
    MapValue mapValue = map.get(key);
    return mapValue instanceof OverStr os ? os.luggage : null;
  }

}
