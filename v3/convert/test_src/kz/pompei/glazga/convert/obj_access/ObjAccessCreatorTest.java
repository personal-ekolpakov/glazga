package kz.pompei.glazga.convert.obj_access;

import kz.pompei.glazga.convert.DiscriminatorPostfix;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ObjAccessCreatorTest {

  @DiscriminatorPostfix("define")
  public static class SuperClass {}

  public static class SuperClassHello extends SuperClass {}

  @Test
  public void create__withDiscriminator() {

    ObjAccess objAccess = ObjAccessCreator.create(SuperClass.class);

    String childDetectFieldName = objAccess.childDetectFieldName();

    assertThat(childDetectFieldName).isEqualTo("define");

    Class<?> helloClass = objAccess.getChildClass("Hello");

    assertThat(helloClass).isEqualTo(SuperClassHello.class);

  }

  @Test
  public void discriminate__forChild() {

    ObjAccess objAccess = ObjAccessCreator.create(SuperClassHello.class);

    //
    //
    Discriminate discriminate = objAccess.discriminate();
    //
    //

    assertThat(discriminate).isNotNull();
    assertThat(discriminate.fieldName).isEqualTo("define");
    assertThat(discriminate.fieldValue).isEqualTo("Hello");

  }

  @Test
  public void createInstance__OK() {

    ObjAccess objAccess = ObjAccessCreator.create(SuperClass.class);

    //
    //
    Object instance = objAccess.createInstance();
    //
    //

    assertThat(instance).isNotNull();

  }

  public static abstract class SuperClassAbstract {}

  @Test
  public void createInstance__forAbstractClass() {

    ObjAccess objAccess = ObjAccessCreator.create(SuperClassAbstract.class);

    //
    //
    Object instance = objAccess.createInstance();
    //
    //

    assertThat(instance).isNull();

  }

  public static abstract class SuperClassAbstract2 {
    @SuppressWarnings("unused")
    protected SuperClassAbstract2(int x, double y) {}
  }

  @Test
  public void createInstance__forAbstractClass_noDefaultConstructor() {

    ObjAccess objAccess = ObjAccessCreator.create(SuperClassAbstract2.class);

    //
    //
    Object instance = objAccess.createInstance();
    //
    //

    assertThat(instance).isNull();

  }

  public static class SuperClassAbstract3 {
    private SuperClassAbstract3() {}
  }

  @Test
  public void createInstance__forAbstractClass_privateDefaultConstructor() {

    ObjAccess objAccess = ObjAccessCreator.create(SuperClassAbstract3.class);

    //
    //
    Object instance = objAccess.createInstance();
    //
    //

    assertThat(instance).isNull();

  }
}
