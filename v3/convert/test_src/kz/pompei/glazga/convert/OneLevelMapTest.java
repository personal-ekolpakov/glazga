package kz.pompei.glazga.convert;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.convert.model.MapValue;
import kz.pompei.glazga.convert.model.OverMap;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OneLevelMapTest {

  @Test
  public void TWICE__general() {

    Map<String, String> source = new HashMap<>();

    source.put("root.one.field1", "test 1");
    source.put("root.one.field2", "test 2");
    source.put("root.one.field3", "test 3");
    source.put("root.two.field4", "test 4");
    source.put("root.two.field5", "test 5");
    source.put("status", "test 6");
    source.put("sinus.id1", "test 7");
    source.put("sinus.id2", "test 8");
    source.put("sinus.id3.group.x1", "test 9");
    source.put("sinus.id3.group.x2", "test 10");
    source.put("sinus.id3.group.x3", "test 11");
    source.put("sinus.id3.group.x4", "test 12");

    PrintUtil.printOneLevel("NTUIr1lOW7", source);

    //
    //
    Map<String, MapValue> material = OneLevelMap.makeMaterial(source);
    //
    //

    PrintUtil.printMap("kHvTUeoEPd", OverMap.of(material));

    //
    //
    Map<String, String> actual = OneLevelMap.makeOneLevel(material);
    //
    //

    PrintUtil.printOneLevel("o7M1s3A35H", actual);

    assertThat(actual).isEqualTo(source);
  }
}
