package kz.pompei.glazga.convert;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import kz.greetgo.util.RND;
import kz.pompei.glazga.convert.model.MapValue;
import kz.pompei.glazga.convert.model.OverMap;
import kz.pompei.glazga.convert.model.OverStr;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static kz.pompei.glazga.utils.ReflectUtil.mapTypeOf;
import static org.assertj.core.api.Assertions.assertThat;

public class MaterializingTest {

  @DataProvider
  public Object[][] convertSimpleTypesDataProvider() {
    return new Object[][]{
      {'W'},
      {'⤜'},
      {'亴'},
      {(byte) 34},
      {(short) 341},
      {543256},
      {54325435L},
      {4324.56f},
      {43234.56d},
      {true},
      {false},
      {"xc01SB1TKK"},
      {new Date()},
      {new BigDecimal("5964832659487326594372564.9485736924875632187949")},
      {new BigInteger("054987325987432589074392057439205784509487325943")},
    };
  }

  @Test(dataProvider = "convertSimpleTypesDataProvider")
  public void TWICE_CONVERT__SimpleTypes(Object simpleValue) {
    Materializing mat = new Materializing();

    //
    //
    MapValue mapValue = mat.materialize(simpleValue);
    //
    //

    PrintUtil.printMap("0IYg4EzVrl", mapValue);


    //
    //
    Object actual = mat.dematerialize(mapValue, simpleValue.getClass());
    //
    //

    assertThat(actual).isEqualTo(simpleValue);

  }

  @Test
  public void TWICE_CONVERT__ByteArray() {
    Materializing mat = new Materializing();

    byte[] source = RND.byteArray(20);

    //
    //
    MapValue mapValue = mat.materialize(source);
    //
    //

    PrintUtil.printMap("0IYg4EzVrl", mapValue);

    //
    //
    Object actual = mat.dematerialize(mapValue, source.getClass());
    //
    //

    assertThat(actual).isEqualTo(source);

  }

  @Test
  public void convertFromMap__byteArray_fromLeftStr() {
    Materializing mat = new Materializing();

    //
    //
    Object actual = mat.dematerialize(OverStr.of("ЖЖВоДТотОтоТд1лО"), byte[].class);
    //
    //

    assertThat(actual).isNull();

  }

  public static class SimpleFields {

    public char       field01;
    public Character  field02;
    public byte       field03;
    public Byte       field04;
    public short      field05;
    public Short      field06;
    public int        field07;
    public Integer    field08;
    public long       field09;
    public Long       field10;
    public float      field11;
    public Float      field12;
    public double     field13;
    public Double     field14;
    public boolean    field15;
    public Boolean    field16;
    public byte[]     field17;
    public String     field18;
    public Date       field19;
    public BigDecimal field20;
    public BigInteger field21;
  }

  @Test
  public void TWICE_CONVERT__SimpleFields__NULLS() {
    Materializing mat = new Materializing();

    SimpleFields source = new SimpleFields();

    //
    //
    MapValue mapValue = mat.materialize(source);
    //
    //

    PrintUtil.printMap("vriLYPn1yC", mapValue);

    //
    //
    SimpleFields actual = mat.dematerialize(mapValue, SimpleFields.class);
    //
    //

    assertThat(actual).isNotNull();
    assertThat(actual.field01).isEqualTo((char) 0);
    assertThat(actual.field02).isNull();
    assertThat(actual.field03).isEqualTo((byte) 0);
    assertThat(actual.field04).isNull();
    assertThat(actual.field05).isEqualTo((short) 0);
    assertThat(actual.field06).isNull();
    assertThat(actual.field07).isEqualTo(0);
    assertThat(actual.field08).isNull();
    assertThat(actual.field09).isEqualTo(0);
    assertThat(actual.field10).isNull();
    assertThat(actual.field11).isEqualTo(0);
    assertThat(actual.field12).isNull();
    assertThat(actual.field13).isEqualTo(0);
    assertThat(actual.field14).isNull();
    assertThat(actual.field15).isEqualTo(false);
    assertThat(actual.field16).isNull();
    assertThat(actual.field17).isNull();
    assertThat(actual.field18).isNull();
    assertThat(actual.field19).isNull();
    assertThat(actual.field20).isNull();
    assertThat(actual.field21).isNull();

  }

  @Test
  public void convertFromMap__simpleFields_fromEmptyMap() {
    Materializing mat = new Materializing();
    //
    //
    SimpleFields actual = mat.dematerialize(OverMap.of(Map.of()), SimpleFields.class);
    //
    //

    assertThat(actual).isNotNull();
    assertThat(actual.field01).isEqualTo((char) 0);
    assertThat(actual.field02).isNull();
    assertThat(actual.field03).isEqualTo((byte) 0);
    assertThat(actual.field04).isNull();
    assertThat(actual.field05).isEqualTo((short) 0);
    assertThat(actual.field06).isNull();
    assertThat(actual.field07).isEqualTo(0);
    assertThat(actual.field08).isNull();
    assertThat(actual.field09).isEqualTo(0);
    assertThat(actual.field10).isNull();
    assertThat(actual.field11).isEqualTo(0);
    assertThat(actual.field12).isNull();
    assertThat(actual.field13).isEqualTo(0);
    assertThat(actual.field14).isNull();
    assertThat(actual.field15).isEqualTo(false);
    assertThat(actual.field16).isNull();
    assertThat(actual.field17).isNull();
    assertThat(actual.field18).isNull();
    assertThat(actual.field19).isNull();
    assertThat(actual.field20).isNull();
    assertThat(actual.field21).isNull();

  }

  @Test
  public void TWICE_CONVERT__SimpleFields() {
    Materializing mat = new Materializing();

    SimpleFields source = new SimpleFields();

    source.field01 = 'W';
    source.field02 = 'Q';
    source.field03 = 23;
    source.field04 = 32;
    source.field05 = 4321;
    source.field06 = 5432;
    source.field07 = 543256;
    source.field08 = 5432634;
    source.field09 = 65436754;
    source.field10 = 76547654L;
    source.field11 = 4324.54f;
    source.field12 = 4324.55f;
    source.field13 = 45325.456;
    source.field14 = 3423.45;
    source.field15 = true;
    source.field16 = true;
    source.field17 = new byte[]{1, 2, 3, 4, 5, 6};
    source.field18 = "rHqXv7jCtX→➤➣⇶⟾➪⍄ᐉᐅ↢◀⍍⍓➷";
    source.field19 = new Date();
    source.field20 = new BigDecimal("1054325439764783654983.7659834265");
    source.field21 = new BigInteger("954837659874657893295647893256");

    //
    //
    MapValue map = mat.materialize(source);
    //
    //

    PrintUtil.printMap("1b94mz1vr0", map);

    //
    //
    SimpleFields actual = mat.dematerialize(map, SimpleFields.class);
    //
    //

    assertThat(actual).isNotNull();
    assertThat(actual.field01).isEqualTo(source.field01);
    assertThat(actual.field02).isEqualTo(source.field02);
    assertThat(actual.field03).isEqualTo(source.field03);
    assertThat(actual.field04).isEqualTo(source.field04);
    assertThat(actual.field05).isEqualTo(source.field05);
    assertThat(actual.field06).isEqualTo(source.field06);
    assertThat(actual.field07).isEqualTo(source.field07);
    assertThat(actual.field08).isEqualTo(source.field08);
    assertThat(actual.field09).isEqualTo(source.field09);
    assertThat(actual.field10).isEqualTo(source.field10);
    assertThat(actual.field11).isEqualTo(source.field11);
    assertThat(actual.field12).isEqualTo(source.field12);
    assertThat(actual.field13).isEqualTo(source.field13);
    assertThat(actual.field14).isEqualTo(source.field14);
    assertThat(actual.field15).isEqualTo(source.field15);
    assertThat(actual.field16).isEqualTo(source.field16);
    assertThat(actual.field17).isEqualTo(source.field17);
    assertThat(actual.field18).isEqualTo(source.field18);
    assertThat(actual.field19).isEqualTo(source.field19);
    assertThat(actual.field20).isEqualTo(source.field20);
    assertThat(actual.field21).isEqualTo(source.field21);

  }

  @RequiredArgsConstructor
  public static abstract class ParentStrClass {
    public final @NonNull String id;

    @StringRepresented public @NonNull String getId() {
      return id;
    }
  }

  @StringRepresented
  public static class StrClass extends ParentStrClass {
    public StrClass(@NonNull String id) {
      super(id);
    }

    @StringRepresented public static StrClass create(String id) {
      return id == null ? null : new StrClass(id);
    }
  }

  @Test
  public void TWICE_CONVERT__StringRepresented() {
    Materializing mat = new Materializing();

    StrClass source = new StrClass(RND.str(10));

    //
    //
    MapValue mapValue = mat.materialize(source);
    //
    //

    PrintUtil.printMap("3jdBzZXUxb", mapValue);

    assertThat(mapValue).isInstanceOf(OverStr.class);

    //
    //
    StrClass actual = mat.dematerialize(mapValue, StrClass.class);
    //
    //

    assertThat(actual.getId()).isEqualTo(source.id);
  }

  public static class MappingClass {
    public StrClass field1;
    public StrClass field2;
  }

  @Test
  public void TWICE_CONVERT__StrRepresented_asField() {

    Materializing mat = new Materializing();

    MappingClass source = new MappingClass();
    source.field1 = StrClass.create("tirDv1gMA6");
    source.field2 = StrClass.create("NRoFp7djEw");

    //
    //
    MapValue mapValue = mat.materialize(source);
    //
    //

    assertThat(mapValue).isInstanceOf(OverMap.class);

    Map<String, MapValue> map = ((OverMap) mapValue).luggage;

    assertThat(map).isNotNull();
    assertThat(map).containsKeys("field1", "field2");
    assertThat(map.get("field1")).isInstanceOf(OverStr.class);
    assertThat(map.get("field2")).isInstanceOf(OverStr.class);
    assertThat(map.get("field1")).isEqualTo(OverStr.of("tirDv1gMA6"));
    assertThat(map.get("field2")).isEqualTo(OverStr.of("NRoFp7djEw"));

    //
    //
    MappingClass actual = mat.dematerialize(mapValue, MappingClass.class);
    //
    //

    assertThat(actual).isNotNull();
    assertThat(actual.field1).isNotNull();
    assertThat(actual.field1.id).isEqualTo("tirDv1gMA6");
    assertThat(actual.field2).isNotNull();
    assertThat(actual.field2.id).isEqualTo("NRoFp7djEw");
  }

  public enum TestEnum {
    ELEMENT
  }

  @Test
  public void TWICE_CONVERT__enum() {
    Materializing mat = new Materializing();

    //
    //
    MapValue mapValue = mat.materialize(TestEnum.ELEMENT);
    //
    //

    PrintUtil.printMap("w3FzqVtyMO", mapValue);

    //
    //
    TestEnum actual = mat.dematerialize(mapValue, TestEnum.class);
    //
    //

    assertThat(actual).isEqualTo(TestEnum.ELEMENT);
  }

  @Test
  public void TWICE_CONVERT__map() {
    Materializing mat = new Materializing();

    Map<String, Integer> map = new HashMap<>();
    map.put("field1", 1);
    map.put("field2", 3);
    map.put("field3", 4);
    map.put("field4", 5);

    //
    //
    MapValue mapValue = mat.materialize(map);
    //
    //

    PrintUtil.printMap("CTE4SqA3Hq", mapValue);

    //
    //
    Map<String, Integer> actual = mat.dematerialize(mapValue, mapTypeOf(String.class, Integer.class));
    //
    //

    assertThat(actual).isEqualTo(map);
  }

  @EqualsAndHashCode
  public static class Person {
    public String surname;
    public String name;
    public String patronymic;
  }

  @EqualsAndHashCode
  @StringRepresented
  @RequiredArgsConstructor
  public static class PersonId {
    public final String dbId;
    public final long   index;

    @StringRepresented
    public static PersonId parse(String strId) {
      if (strId == null) {
        return null;
      }
      String[] split = strId.split("#");
      return new PersonId(split[0], Long.parseLong(split[1]));
    }

    @StringRepresented
    public String strVariant() {
      return dbId + '#' + index;
    }

  }

  public enum PhoneType {
    MOBILE, HOME, WORK, SPECIAL
  }

  @EqualsAndHashCode
  public static class Phone {
    public String    number;
    public PhoneType type;
  }

  @EqualsAndHashCode
  @StringRepresented
  @RequiredArgsConstructor
  public static class PhoneId {
    public final long id;
    public final long index;

    @StringRepresented
    public static PhoneId parse(String strId) {
      if (strId == null) {
        return null;
      }
      String[] split = strId.split("#");
      return new PhoneId(Long.parseLong(split[0]), Long.parseLong(split[1]));
    }

    @StringRepresented
    public String string() {
      return id + "#" + index;
    }
  }

  @EqualsAndHashCode
  public static class GroupStatus {
    public String state;
    public int    status;
  }

  @EqualsAndHashCode
  public static class PersonGroup {
    public String                name;
    public Map<PersonId, Person> persons;
    public Map<PhoneId, Phone>   phones;
    public GroupStatus           status;
  }

  @Test
  public void TWICE_CONVERT__fullIntegration() {

    PersonId pi1 = new PersonId(RND.str(10), RND.plusLong(10_000_000_000_000L));
    Person   p1  = new Person();
    p1.surname    = RND.str(30);
    p1.name       = RND.str(10);
    p1.patronymic = RND.str(20);

    PersonId pi2 = new PersonId(RND.str(10), RND.plusLong(10_000_000_000_000L));
    Person   p2  = new Person();
    p2.surname    = RND.str(30);
    p2.name       = RND.str(10);
    p2.patronymic = RND.str(20);

    PersonId pi3 = new PersonId(RND.str(10), RND.plusLong(10_000_000_000_000L));
    Person   p3  = new Person();
    p3.surname    = RND.str(30);
    p3.name       = RND.str(10);
    p3.patronymic = RND.str(20);

    PhoneId oi1 = new PhoneId(RND.plusLong(10_000_000_000_000L), RND.plusLong(10_000_000_000_000L));
    Phone   o1  = new Phone();
    o1.number = RND.str(10);
    o1.type   = RND.someEnum(PhoneType.values());

    PhoneId oi2 = new PhoneId(RND.plusLong(10_000_000_000_000L), RND.plusLong(10_000_000_000_000L));
    Phone   o2  = new Phone();
    o2.number = RND.str(10);
    o2.type   = RND.someEnum(PhoneType.values());

    PhoneId oi3 = new PhoneId(RND.plusLong(10_000_000_000_000L), RND.plusLong(10_000_000_000_000L));
    Phone   o3  = new Phone();
    o3.number = RND.str(10);
    o3.type   = RND.someEnum(PhoneType.values());

    GroupStatus status = new GroupStatus();
    status.state  = RND.str(25);
    status.status = RND.plusInt(10_000_000);

    PersonGroup pg = new PersonGroup();
    pg.name   = RND.str(20);
    pg.status = status;

    pg.persons = new HashMap<>();
    pg.persons.put(pi1, p1);
    pg.persons.put(pi2, p2);
    pg.persons.put(pi3, p3);

    pg.phones = new HashMap<>();
    pg.phones.put(oi1, o1);
    pg.phones.put(oi2, o2);
    pg.phones.put(oi3, o3);

    Materializing mat = new Materializing();

    //
    //
    MapValue mapValue = mat.materialize(pg);
    //
    //

    PrintUtil.printMap("mg9R1ZBWmQ", mapValue);

    //
    //
    PersonGroup actual = mat.dematerialize(mapValue, PersonGroup.class);
    //
    //

    assertThat(actual).isEqualTo(pg);
  }

  @EqualsAndHashCode
  @DiscriminatorPostfix("look")
  public static abstract class BaseClass {
    public String baseField;
  }

  @EqualsAndHashCode(callSuper = true)
  public static class BaseClassSinus extends BaseClass {
    public String childField;
  }

  @Test
  public void TWICE_CONVERT__PostfixDiscriminator() {
    BaseClassSinus sinus = new BaseClassSinus();
    sinus.baseField  = RND.str(10);
    sinus.childField = RND.str(10);

    Materializing mat = new Materializing();

    //
    //
    MapValue mapValue = mat.materialize(sinus);
    //
    //

    PrintUtil.printMap("SFnLdoFkv5", mapValue);

    assertThat(((OverMap) mapValue).luggage).containsEntry("look", OverStr.of("Sinus"));

    //
    //
    BaseClassSinus actual = mat.dematerialize(mapValue, BaseClass.class);
    //
    //

    assertThat(actual).isEqualTo(sinus);
  }
}
