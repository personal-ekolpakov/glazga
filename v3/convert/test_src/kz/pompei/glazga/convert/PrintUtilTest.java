package kz.pompei.glazga.convert;

import java.util.List;
import java.util.Map;
import kz.pompei.glazga.convert.model.OverMap;
import kz.pompei.glazga.convert.model.OverStr;
import org.testng.annotations.Test;

public class PrintUtilTest {

  @Test
  public void mapValueToLines() {

    OverMap overMap = OverMap.of(Map.of(

      "test1", OverMap.of(Map.of()),
      "test2", OverStr.of("value 2"),
      "test3", OverMap.of(Map.of(
        "test4", OverMap.of(Map.of(
          "test5", OverMap.of(Map.of()),
          "test6", OverStr.of("value 6"),
          "test7", OverStr.of("value 7")
        ))
      ))

    ));

    List<String> list = PrintUtil.mapValueToLines(overMap);

    for (final String line : list) {
      System.out.println("V4UoH4UKLy :: " + line);
    }

  }
}
