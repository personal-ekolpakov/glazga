package kz.pompei.v3.storage;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StorageUtil {
  private static final Map<Character, String> CharToCode = new HashMap<>();
  private static final Map<String, Character> CodeToChar = new HashMap<>();

  private static void append(char c, String code) {
    CharToCode.put(c, code);
    CodeToChar.put(code, c);
  }

  static {
    append('%', "01");
    append('/', "02");
    append('.', "03");
  }

  public static String encodeId(String id) {
    if (id == null) {
      return null;
    }

    StringBuilder sb = new StringBuilder();

    for (int i = 0; i < id.length(); i++) {
      final char   c    = id.charAt(i);
      final String code = CharToCode.get(c);
      if (code == null) {
        sb.append(c);
      } else {
        sb.append('%').append(code);
      }
    }

    return sb.toString();
  }

  private static final Pattern CODE = Pattern.compile("%(\\d{2})");

  public static String decodeId(String id) {
    if (id == null) {
      return null;
    }

    StringBuilder sb = new StringBuilder();

    final Matcher matcher = CODE.matcher(id);
    while (matcher.find()) {
      final String    code = matcher.group(1);
      final Character c    = CodeToChar.get(code);
      if (c == null) {
        throw new RuntimeException("Cv3Mj1vfAH :: Unknown char code = `" + code + "`");
      }

      matcher.appendReplacement(sb, "" + c);
    }

    matcher.appendTail(sb);

    return sb.toString();
  }

}
