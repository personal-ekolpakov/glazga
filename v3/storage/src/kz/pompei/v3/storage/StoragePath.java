package kz.pompei.v3.storage;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import static java.util.stream.Collectors.toSet;
import static java.util.stream.Collectors.toUnmodifiableMap;

@RequiredArgsConstructor
public class StoragePath implements Storage {
  private final @NonNull Path rootDir;

  public static @NonNull StoragePath cd(@NonNull StoragePath origin, String path) {
    return path == null ? origin : new StoragePath(origin.rootDir.resolve(path));
  }

  @Override public Storage cd(String path) {
    return path == null ? this : new StoragePath(rootDir.resolve(path));
  }

  public static final String TXT = ".txt";

  @Override
  @SneakyThrows
  public void set(@NonNull String key, String value) {
    Path filePath = rootDir.resolve(key + ".txt");

    if (value == null) {
      if (Files.exists(filePath)) {
        Files.delete(filePath);
      }
      return;
    }

    filePath.toFile().getParentFile().mkdirs();

    Files.writeString(filePath, value, StandardCharsets.UTF_8);
  }

  @Override
  @SneakyThrows
  public @NonNull Optional<String> get(@NonNull String key) {
    Path filePath = rootDir.resolve(key + TXT);

    if (Files.exists(filePath)) {
      return Optional.of(Files.readString(filePath, StandardCharsets.UTF_8));
    }

    return Optional.empty();
  }

  @Override
  public @NonNull String fullPathOf(@NonNull String key) {
    return key;
  }

  @Override
  @SneakyThrows
  public @NonNull Set<String> findKeys(String keyPrefix) {
    if (!Files.exists(rootDir)) {
      return Set.of();
    }
    try (Stream<Path> stream = Files.list(rootDir)) {
      return stream.map(Path::toFile)
                   .filter(File::isFile)
                   .map(File::getName)
                   .filter(n -> n.endsWith(TXT))
                   .map(s -> s.substring(0, s.length() - TXT.length()))
                   .filter(n -> n.startsWith(keyPrefix))
                   .collect(toSet());
    }
  }

  @Override
  public @NonNull Map<String, Optional<String>> gets(@NonNull Set<String> keys) {
    return keys.stream().collect(toUnmodifiableMap(k -> k, this::get));
  }

  @Override public void flush() {
    // do nothing
  }

  @Override public @NonNull Map<String, String> loadOneLevelMap(Set<String> prefixes) {
    throw new RuntimeException("2024-05-09 15:35 Not impl yet StoragePath.getOneLevelMap()");
  }
}
