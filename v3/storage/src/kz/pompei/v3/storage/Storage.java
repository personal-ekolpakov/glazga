package kz.pompei.v3.storage;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import lombok.NonNull;

public interface Storage {

  void set(@NonNull String key, String value);

  @NonNull Set<String> findKeys(String keyPrefix);

  @NonNull Optional<String> get(@NonNull String key);

  @NonNull Map<String, Optional<String>> gets(@NonNull Set<String> keys);

  @NonNull String fullPathOf(@NonNull String key);

  default void remove(@NonNull String key) {
    set(key, null);
  }

  Storage cd(String path);

  void flush();

  @NonNull Map<String, String> loadOneLevelMap(Set<String> prefixes);

}
