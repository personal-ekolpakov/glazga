package kz.pompei.v3.storage.update;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import kz.pompei.v3.storage.Storage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class StorageUpdate {
  private final @NonNull Storage storage;

  public static StorageUpdate over(@NonNull Storage storage) {
    return new StorageUpdate(storage);
  }

  private final Set<String> prefixes = new HashSet<>();

  public StorageUpdate cleaning(String prefix) {
    prefixes.add(prefix);
    return this;
  }

  public StorageUpdate setMap(Map<String, String> map) {
    if (map == null) {
      return this;
    }
    map.forEach(this::set);
    return this;
  }

  private final Map<String, Optional<String>> newValues = new HashMap<>();

  public StorageUpdate set(@NonNull String key, String value) {
    newValues.put(key, Optional.ofNullable(value));
    return this;
  }


  public StorageUpdate put(Map<String, Optional<String>> values) {
    newValues.putAll(values);
    return this;
  }

  public void update() {
    for (final String prefix : prefixes) {
      for (final String key : storage.findKeys(prefix)) {
        if (!newValues.containsKey(key)) {
          newValues.put(key, Optional.empty());
        }
      }
    }

    // TODO pompei данные текущие значения нужно сохранить в буфере отмены
    //noinspection unused
    Map<String, Optional<String>> currentValues = storage.gets(newValues.keySet());


    newValues.forEach((k, v) -> storage.set(k, v.orElse(null)));
  }

  public static Map<String, Optional<String>> changePrefix(Map<String, Optional<String>> currentValues,
                                                           String currentPrefix,
                                                           String newPrefix) {

    Map<String, Optional<String>> ret = new HashMap<>();

    for (final Map.Entry<String, Optional<String>> e : currentValues.entrySet()) {
      String key = e.getKey();
      if (key.startsWith(currentPrefix)) {
        ret.put(newPrefix + key.substring(currentPrefix.length()), e.getValue());
      }
    }

    return ret;
  }

}
