package kz.pompei.v3.storage;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.testng.annotations.BeforeMethod;

public abstract class StoragePathTestParent {

  protected final Date runAt = new Date();

  protected Path rootDir;

  protected StoragePath storage;

  @BeforeMethod
  public void createRootDir(Method testMethod) {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");

    rootDir = Paths.get("build/")
                   .resolve("tst_" + sdf.format(runAt))
                   .resolve(getClass().getSimpleName() + "_$_" + testMethod.getName());

    storage = new StoragePath(rootDir);
  }

}
