package kz.pompei.v3.storage;

import java.util.Set;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StoragePathTest extends StoragePathTestParent {

  @Test
  public void set_get_01() {

    storage.set("tst.lock", "hello");

    assertThat(storage.get("tst.lock")).contains("hello");

  }


  @Test
  public void set_findKeys_01() {

    storage.set("tst.lock.key01", "hello1");
    storage.set("tst.lock.key02", "hello2");
    storage.set("tst.lock.key03", "hello3");
    storage.set("tst.lock.key04", "hello4");

    storage.set("wow", "hello4");

    //
    //
    Set<String> keys = storage.findKeys("tst.lock");
    //
    //

    assertThat(keys).isEqualTo(Set.of("tst.lock.key01", "tst.lock.key02", "tst.lock.key03", "tst.lock.key04"));

  }

  @Test
  public void set_findKeys_02() {

    storage.set("tst.lock.key01", "hello1");
    storage.set("tst.lock.key02", "hello2");
    storage.set("tst.lock.key03", "hello3");
    storage.set("tst.lock.key04", "hello4");
    storage.set("tst.lock", "sun");
    storage.set("tst.hi", "hi1");
    storage.set("tst.by", "by2");

    storage.set("wow", "hello4");

    //
    //
    Set<String> keys = storage.findKeys("tst.lock.");
    //
    //

    assertThat(keys).isEqualTo(Set.of("tst.lock.key01", "tst.lock.key02", "tst.lock.key03", "tst.lock.key04"));

  }

}
