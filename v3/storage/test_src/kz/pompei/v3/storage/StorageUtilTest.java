package kz.pompei.v3.storage;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StorageUtilTest {

  @Test
  public void encodeId_01() {

    final String id = "hello/test%status/found.stone";

    //
    //
    final String encodedId = StorageUtil.encodeId(id);
    //
    //

    assertThat(encodedId).isEqualTo("hello%02test%01status%02found%03stone");
  }

  @Test
  public void decodeId_01() {

    final String encodedId = "hello%02test%01status%02found";

    //
    //
    final String id = StorageUtil.decodeId(encodedId);
    //
    //

    assertThat(id).isEqualTo("hello/test%status/found");
  }
}