package kz.pompei.swing.icons;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.SizeLocationSaver;

public class SvgTestForm {

  public static void main(String[] args) {

    SwingUtilities.invokeLater(() -> {

      JFrame frame = new JFrame("Проверка работы иконок");

      Path build = Paths.get("build");

      SizeLocationSaver.into(build.resolve("sizes_and_locations"))
                       .overJFrame("SvgTestForm", frame, true);

      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setSize(400, 300);

      frame.setContentPane(new JPanel() {

        @Override
        public void paint(Graphics g1) {
          super.paint(g1);
          Graphics2D g = (Graphics2D) g1;
          g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

          int size = 16;

          g.drawImage(Svg.images.get("svg/wsl.svg", size, size), null, 100, 100);
          g.drawImage(Svg.images.get("svg/webModuleGroup.svg", size, size), null, 100, 20);
          g.drawImage(Svg.images.get("svg/module.svg", size, size), null, 100, 150);

        }
      });

      frame.setVisible(true);
    });


  }

}
