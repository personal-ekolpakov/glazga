package kz.pompei.swing.icons;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ConcurrentHashMap;
import kz.pompei.glazga.utils.GuiUtil;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.ImageTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;

import static java.util.Objects.requireNonNull;

public class Svg {

  public static final Svg images = new Svg();


  @EqualsAndHashCode
  @RequiredArgsConstructor
  private static class ImgId {
    private final @NonNull String resourceName;
    private final          int    width;
    private final          int    height;
    private final          Color  backgroundColor;
  }

  private final ConcurrentHashMap<ImgId, BufferedImage> iconMap = new ConcurrentHashMap<>();

  public @NonNull BufferedImage get(@NonNull String name, int width, int height) {
    return get(name, width, height, null);
  }

  public @NonNull BufferedImage get(@NonNull String name, int width, int height, Color backgroundColor) {

    ImgId id = new ImgId(name, width, height, backgroundColor);

    {
      BufferedImage imageIcon = iconMap.get(id);
      if (imageIcon != null) {
        return imageIcon;
      }
    }

    synchronized (iconMap) {
      {
        BufferedImage imageIcon = iconMap.get(id);
        if (imageIcon != null) {
          return imageIcon;
        }
      }

      {
        BufferedImage imageIcon = load(id);
        iconMap.put(id, imageIcon);
        return imageIcon;
      }

    }

  }

  public static final String TRANSPARENT = "__transparent__";

  @SneakyThrows
  private @NonNull BufferedImage load(@NonNull ImgId id) {

    final BufferedImage resourceImage = loadFromResource(id);

    if (id.backgroundColor == null) {
      return resourceImage;
    }

    BufferedImage ret = new BufferedImage(resourceImage.getWidth(), resourceImage.getHeight(), BufferedImage.TYPE_INT_ARGB);

    final Graphics2D g = ret.createGraphics();
    GuiUtil.applyHints(g);

    g.setColor(id.backgroundColor);

    int arcWidth  = Math.round(ret.getWidth() * 0.3f);
    int arcHeight = Math.round(ret.getHeight() * 0.3f);

    g.fillRoundRect(0, 0, resourceImage.getWidth(), resourceImage.getHeight(), arcWidth, arcHeight);

    g.drawImage(resourceImage, 0, 0, null);

    g.dispose();

    return ret;

  }

  private BufferedImage loadFromResource(@NonNull ImgId id) throws IOException, TranscoderException {
    if (TRANSPARENT.equals(id.resourceName)) {
      return new BufferedImage(id.width, id.height, BufferedImage.TYPE_INT_ARGB);
    }

    try (InputStream resource = getClass().getResourceAsStream(id.resourceName)) {
      requireNonNull(resource, "W1zjYhHu4e :: No resource with name " + id.resourceName + " in " + getClass());

      PNGTranscoder transcoder = new PNGTranscoder();

      transcoder.addTranscodingHint(PNGTranscoder.KEY_WIDTH, (float) id.width);
      transcoder.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, (float) id.height);

      TranscoderInput input = new TranscoderInput(resource);

      BufferedImageTranscoder imageTranscoder = new BufferedImageTranscoder();

      imageTranscoder.addTranscodingHint(PNGTranscoder.KEY_WIDTH, (float) id.width);
      imageTranscoder.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, (float) id.height);

      imageTranscoder.transcode(input, null);

      return imageTranscoder.getBufferedImage();
    }
  }

  private static class BufferedImageTranscoder extends ImageTranscoder {
    private BufferedImage img = null;

    @Override
    public BufferedImage createImage(int w, int h) {
      return new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
    }

    @Override
    public void writeImage(BufferedImage img, TranscoderOutput output) {
      this.img = img;
    }

    public BufferedImage getBufferedImage() {
      return img;
    }
  }

}
