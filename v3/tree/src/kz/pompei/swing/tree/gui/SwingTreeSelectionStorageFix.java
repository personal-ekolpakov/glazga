package kz.pompei.swing.tree.gui;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import lombok.NonNull;

public class SwingTreeSelectionStorageFix implements SwingTreeSelectionStorage {

  private       String                             activeNodeId;
  private       String                             activeCellId;
  private final Map<String, LinkedHashSet<String>> selectedCellIds = new LinkedHashMap<>();

  @Override public String activeNodeId() {
    return activeNodeId;
  }

  @Override public void setActiveNodeId(String nodeId) {
    activeNodeId = nodeId;
  }

  @Override public String activeCellId() {
    return activeCellId;
  }

  @Override public void setActiveCellId(String cellId) {
    activeCellId = cellId;
  }

  @Override public void addSelectedNodeId(String nodeId) {
    selectedCellIds.computeIfAbsent(nodeId, k -> new LinkedHashSet<>());
  }

  @Override public void deleteSelectedNodeId(String nodeId) {
    selectedCellIds.remove(nodeId);
  }

  @Override public @NonNull List<String> getSelectedNodeIds() {
    return selectedCellIds.keySet().stream().toList();
  }

  @Override public void addSelectedCellId(String nodeId, String cellId) {
    if (nodeId == null) {
      return;
    }
    selectedCellIds.computeIfAbsent(nodeId, k -> new LinkedHashSet<>()).add(cellId);
  }

  @Override public void deleteSelectedCellId(String nodeId, String cellId) {
    if (nodeId == null) {
      return;
    }
    selectedCellIds.computeIfAbsent(nodeId, k -> new LinkedHashSet<>()).remove(cellId);
  }

  @Override public @NonNull List<String> getSelectedCellIds(String nodeId) {
    final LinkedHashSet<String> set = selectedCellIds.get(nodeId);
    if (set == null) {
      return List.of();
    }
    return set.stream().toList();
  }

  @Override public void flush() {
    // nothing to do...
  }
}
