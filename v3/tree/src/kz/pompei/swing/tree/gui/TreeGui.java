package kz.pompei.swing.tree.gui;

import java.util.Iterator;
import java.util.List;
import kz.pompei.glazga.utils.events.Handler;
import kz.pompei.glazga.utils.events.HandlerObserver;
import lombok.NonNull;

public interface TreeGui<TN extends TreeNodeGui<TN>> {

  @NonNull Iterator<TN> nodeIterator();

  HandlerObserver<TN> chevronMousePressed();

  void repaint();

  void installTrap_OnShowNode(@NonNull String nodeId, @NonNull Handler<String> handler);

  @NonNull List<TN> roots();
}
