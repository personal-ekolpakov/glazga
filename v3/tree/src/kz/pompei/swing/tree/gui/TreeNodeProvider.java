package kz.pompei.swing.tree.gui;

import lombok.NonNull;

public interface TreeNodeProvider<TreeNode extends TreeNodeGui<TreeNode>, DataItem> {

  @NonNull TreeNode newTreeNode(@NonNull String nodeId);

  @NonNull TreeNode newWaitingNode(String parentId, int offset, int pageSize);

  @NonNull TreeNode newMoreNode();

  void assign(@NonNull TreeNode target, @NonNull DataItem source);

}
