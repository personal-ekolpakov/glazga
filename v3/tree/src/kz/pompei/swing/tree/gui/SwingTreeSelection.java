package kz.pompei.swing.tree.gui;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.utils.events.Disconnector;
import kz.pompei.glazga.utils.events.VoidHandler;
import lombok.NonNull;

public interface SwingTreeSelection {

  String activeNodeId();

  void setActiveNodeId(String nodeId);

  String activeCellId();

  void setActiveCellId(String nodeId, String cellId);

  @NonNull Set<String> selectedNodeIds();

  void setSelectedNodeIds(Collection<String> nodeIds);

  @NonNull Map<String, Set<String>> selectedCellIds();

  void setSelectedCellIds(Map<String, Collection<String>> selectedCellIds);

  void addSelection(String nodeId, String cellId);

  void removeSelectedNode(String nodeId);

  void removeSelectedCell(String nodeId, String cellId);

  @NonNull Disconnector addListener(VoidHandler handler);

  boolean isSelectedNode(String nodeId);

  boolean isSelectedCell(String nodeId, String cellId);

  boolean isActiveNode(String nodeId);

  boolean isActiveCell(String nodeId, String cellId);
}
