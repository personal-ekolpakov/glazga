package kz.pompei.swing.tree.gui.model;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class HitTreePoint {
  public final @NonNull SwingTreeNode      node;
  public final          boolean            onChevron;
  public final          SwingTreeNode.Cell cell;
}
