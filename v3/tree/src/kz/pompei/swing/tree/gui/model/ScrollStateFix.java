package kz.pompei.swing.tree.gui.model;

import kz.pompei.swing.tree.gui.ScrollState;

public class ScrollStateFix implements ScrollState {

  private int scrollX=0, scrollY=0;

  @Override public int scrollX() {
    return scrollX;
  }

  @Override public int scrollY() {
    return scrollY;
  }

  @Override public void setScrollX(int scrollX) {
    this.scrollX = scrollX;
  }

  @Override public void setScrollY(int scrollY) {
    this.scrollY = scrollY;
  }
}
