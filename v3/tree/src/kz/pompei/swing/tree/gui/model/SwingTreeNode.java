package kz.pompei.swing.tree.gui.model;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import kz.pompei.glazga.utils.Height;
import kz.pompei.swing.tree.gui.SwingTree;
import kz.pompei.swing.tree.gui.SwingTreeSelection;
import kz.pompei.swing.tree.gui.TreeNodeGui;
import kz.pompei.swing.tree.styles.NodeStatus;
import kz.pompei.swing.tree.styles.Painters;
import kz.pompei.swing.tree.styles.RectStyle;
import kz.pompei.swing.tree.styles.State;
import kz.pompei.swing.tree.styles.SwingTreeNodeStyle;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SwingTreeNode implements TreeNodeGui<SwingTreeNode> {
  private final @NonNull SwingTree owner;
  public final @NonNull  String    id;

  public String     nodeType;
  public NodeStatus status = NodeStatus.LEAF;

  public class Cell {
    public String id;
    public String text;
    public String type;

    private @NonNull String text() {
      var t = text;
      return t != null ? t : "";
    }

    public Rectangle2D.Float boundsCell = new Rectangle2D.Float(0, 0, 0, 0);

    public @NonNull LineMetrics lineMetrics(@NonNull Graphics2D g) {
      return style().cellStyle(type)
                    .font()
                    .getLineMetrics(text(), g.getFontRenderContext());
    }

    public @NonNull Rectangle2D bounds(@NonNull Graphics2D g) {
      return style().cellStyle(type)
                    .font()
                    .getStringBounds(text(), g.getFontRenderContext());
    }

    /**
     * Рисует ячейку
     *
     * @param g     холст для рисования
     * @param x     левая координата, с которой начинать рисование
     * @param y     верхняя координата, с которой начинать рисование
     * @param paint признак рисования. Если false - то самого рисования не будет, но будет сгенерирована геометрия.
     *              Если true - пройдёт рисование
     */
    public void paint(@NonNull Graphics2D g, float x, float y, boolean paint) {

      if (paint) {
        RectStyle rectStyle = cellBackgroundRect();
        if (rectStyle != null) {
          var rect = new Rectangle2D.Float();
          rect.setRect(boundsCell);
          rect.y      = boundsLine.y;
          rect.height = boundsLine.height;
          Painters.rect(rect, g, rectStyle);
        }
      }

      boundsCell.setRect(Painters.text(style().cellStyle(type), g, text(), x, y, paint));
    }

    private RectStyle cellBackgroundRect() {
      String nodeId    = SwingTreeNode.this.id;
      var    selection = owner.selection();

      State state = selection.isActiveCell(nodeId, id)
        ? State.ACTIVE
        : selection.isSelectedCell(nodeId, id)
        ? State.SELECTED
        : State.NORMAL;

      return owner.style.node().cellBackgroundRect(type, state);
    }

    public float width(Graphics2D g) {
      return (float) Painters.text(style().cellStyle(type), g, text(), 0, 0, false).getWidth();
    }
  }

  private @NonNull SwingTreeNodeStyle style() {
    return owner.style.node();
  }

  public int level;

  public final List<Cell>          cells    = new ArrayList<>();
  public final List<SwingTreeNode> children = new ArrayList<>();

  public SwingTreeNode cell(@NonNull String id, String text) {
    return cell(id, text, null);
  }

  public SwingTreeNode cell(@NonNull String id, String text, String type) {
    Cell cell = new Cell();
    cell.id   = id;
    cell.text = text;
    cell.type = type;
    cells.add(cell);
    return this;
  }

  public SwingTreeNode newChild(@NonNull String id) {
    return newChild(id, null);
  }

  public SwingTreeNode newChild(String id, String nodeType) {
    SwingTreeNode ret = new SwingTreeNode(owner, id);
    ret.nodeType = nodeType;
    children.add(ret);
    return ret;
  }

  public @NonNull Height height(@NonNull Graphics2D g) {

    Height ret = null;

    for (final Cell cell : cells) {
      LineMetrics lm     = cell.lineMetrics(g);
      float       center = lm.getStrikethroughOffset();
      Rectangle2D bounds = cell.bounds(g);
      float       height = lm.getHeight();

      if (ret == null || ret.height < height) {
        ret = Height.of(height, (float) -bounds.getY(), center - (float) bounds.getY());
      }

    }

    return ret != null ? ret : Height.of(0, 0, 0);
  }

  private int levelLeftPadding() {
    return style().nodeLevelStep() * level;
  }

  @RequiredArgsConstructor(staticName = "of")
  public static class IconGeo {
    final float iconSizeHalf;
    final int   iconSize;
    final float deltaBaseY;
  }

  private @NonNull IconGeo iconGeo(@NonNull Height height) {
    final float iconSizeHalf = Math.min(height.center, height.height - height.center);
    final int   iconSize     = Math.round(iconSizeHalf * 2);
    final float deltaBaseY   = Math.round(height.center - height.base - iconSizeHalf);

    return IconGeo.of(iconSizeHalf, iconSize, deltaBaseY);
  }

  public Rectangle2D.Float boundsLine        = new Rectangle2D.Float(0, 0, 0, 0);
  public Rectangle2D.Float boundsNode        = new Rectangle2D.Float(0, 0, 0, 0);
  public Rectangle2D.Float boundsChevronIcon = new Rectangle2D.Float(0, 0, 0, 0);
  public Rectangle2D.Float boundsNodeIcon    = new Rectangle2D.Float(0, 0, 0, 0);

  public void paint(@NonNull Graphics2D g, float baseX, float baseY) {

    float   x           = levelLeftPadding() + baseX;
    Height  height      = height(g);
    IconGeo chevronIG   = iconGeo(height.multiply(style().chevronIconSizeFactor()));
    Image   chevronIcon = style().chevronIcon(status, chevronIG.iconSize);
    IconGeo nodeIG      = iconGeo(height.multiply(style().nodeIconSizeFactor()));
    Image   nodeIcon    = style().nodeIcon(nodeType, nodeIG.iconSize);

    boundsChevronIcon.x      = x;
    boundsChevronIcon.y      = baseY + chevronIG.deltaBaseY;
    boundsChevronIcon.width  = chevronIcon.getWidth(null);
    boundsChevronIcon.height = chevronIcon.getHeight(null);

    x += boundsChevronIcon.width + style().chevronIconRight();

    boundsNodeIcon.x      = x;
    boundsNodeIcon.y      = baseY + nodeIG.deltaBaseY;
    boundsNodeIcon.width  = nodeIcon.getWidth(null);
    boundsNodeIcon.height = nodeIcon.getHeight(null);

    x += boundsNodeIcon.width + style().nodeIconRight();

    boundsNode.setRect(boundsChevronIcon);

    {
      float x2 = x;
      for (final Cell cell : cells) {
        cell.paint(g, x2, baseY, false);
        boundsNode.add(cell.boundsCell);
        x2 += (float) cell.boundsCell.getWidth();
      }
    }

    boundsLine.setRect(boundsNode);
    boundsLine.x     = 0;
    boundsLine.width = owner.getWidth();
    boundsLine.y -= owner.style.node().paddingTop();
    boundsLine.height += owner.style.node().paddingTop() + owner.style.node().paddingBottom();

    RectStyle nodeBackgroundRect = nodeBackgroundRect();
    if (nodeBackgroundRect != null) {
      Painters.rect(boundsLine, g, nodeBackgroundRect);
    }

    if (chevronIcon != null) {
      g.drawImage(chevronIcon, Math.round(boundsChevronIcon.x), Math.round(boundsChevronIcon.y), null);
    }

    if (nodeIcon != null) {
      g.drawImage(nodeIcon, Math.round(boundsNodeIcon.x), Math.round(boundsNodeIcon.y), null);
    }

    for (final Cell cell : cells) {
      cell.paint(g, x, baseY, true);
      x += (float) cell.boundsCell.getWidth();
    }

  }

  private RectStyle nodeBackgroundRect() {

    final SwingTreeSelection selection = owner.selection();

    State state = Objects.equals(id, selection.activeNodeId())
      ? State.ACTIVE
      : selection.isSelectedNode(id)
      ? State.SELECTED
      : State.NORMAL;

    return owner.style.node().nodeBackgroundRect(nodeType, state);
  }

  public int width(@NonNull Graphics2D g) {
    final Height  height = height(g);
    final IconGeo ig     = iconGeo(height);
    float         width  = levelLeftPadding() + ig.iconSize;

    for (final Cell cell : cells) {
      width += cell.width(g);
    }
    return Math.round(width);
  }

  public boolean isExpandable() {
    return status == NodeStatus.OPEN || status == NodeStatus.CLOSE;
  }

  @Override public @NonNull String id() {
    return id;
  }

  @Override public NodeStatus status() {
    return status;
  }

  @Override public void setStatus(NodeStatus status) {
    this.status = status;
  }

  @Override public @NonNull List<SwingTreeNode> children() {
    return children;
  }
}
