package kz.pompei.swing.tree.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kz.pompei.glazga.utils.NumUtil;
import kz.pompei.v3.storage.Storage;
import kz.pompei.v3.storage.StorageUtil;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static kz.pompei.v3.storage.StorageUtil.encodeId;

@RequiredArgsConstructor
public class SwingTreeSelectionStorageBridge implements SwingTreeSelectionStorage {
  public static final String SelectedNodes = "selected-nodes";
  public static final String ActiveNodeId  = "activeNodeId";
  public static final String ActiveCellId  = "activeCellId";
  public static final String Order         = "order";
  public static final String Cell          = "cell";

  private final @NonNull Storage storage;

  @Override public String activeNodeId() {
    return storage.get(ActiveNodeId).orElse(null);
  }

  @Override public void setActiveNodeId(String nodeId) {
    storage.set(ActiveNodeId, nodeId);
  }

  @Override public String activeCellId() {
    return storage.get(ActiveCellId).orElse(null);
  }

  @Override public void setActiveCellId(String cellId) {
    storage.set(ActiveCellId, cellId);
  }

  private static final Pattern SEL_NODE_ORDER = Pattern.compile(SelectedNodes + "\\.([^.]+)\\." + Order);

  @Override public void addSelectedNodeId(String nodeId) {

    if (nodeId == null) {
      return;
    }

    String nodeId_ = encodeId(nodeId);

    String nodeKey = SelectedNodes + '.' + nodeId_ + '.' + Order;

    if (storage.get(nodeKey).isPresent()) {
      return;
    }

    List<Double> orders = new ArrayList<>();

    for (final String key : storage.findKeys(SelectedNodes)) {
      Matcher matcher = SEL_NODE_ORDER.matcher(key);
      if (matcher.matches()) {
        orders.add(NumUtil.parseDoubleDef(storage.get(key).orElse("0"), 0));
      }
    }

    double newOrder = orders.stream().mapToDouble(x -> x).max().orElse(0) + 1;

    storage.set(nodeKey, "" + newOrder);
  }

  @Override public void deleteSelectedNodeId(String nodeId) {
    storage.findKeys(SelectedNodes + '.' + encodeId(nodeId) + ".")
           .forEach(storage::remove);
  }

  @Override public @NonNull List<String> getSelectedNodeIds() {

    Map<String, Double> map = new HashMap<>();

    for (final String key : storage.findKeys(SelectedNodes)) {

      Matcher matcher = SEL_NODE_ORDER.matcher(key);
      if (matcher.matches()) {
        map.put(matcher.group(1), NumUtil.parseDoubleDef(storage.get(key).orElse("0"), 0));
      }
    }

    return map.entrySet().stream().sorted(Map.Entry.comparingByValue()).map(Map.Entry::getKey).map(StorageUtil::decodeId).toList();
  }

  private static final Pattern SEL_NODE_CELL_ORDER = Pattern.compile(SelectedNodes + "\\.([^.]+)\\." + Cell + "\\.([^.]+)\\." + Order);

  @Override public void addSelectedCellId(String nodeId, String cellId) {

    if (nodeId == null || cellId == null) {
      return;
    }

    String nodeId_ = encodeId(nodeId);
    String cellId_ = encodeId(cellId);

    addSelectedNodeId(nodeId);

    String cellsKey = SelectedNodes + '.' + nodeId_ + '.' + Cell + '.' + cellId_ + '.' + Order;

    if (storage.get(cellsKey).isPresent()) {
      return;
    }

    List<Double> orders = new ArrayList<>();

    for (final String key : storage.findKeys(SelectedNodes + '.' + nodeId_ + '.' + Cell)) {
      Matcher matcher = SEL_NODE_CELL_ORDER.matcher(key);
      if (matcher.matches()) {
        orders.add(NumUtil.parseDoubleDef(storage.get(key).orElse("0"), 0));
      }
    }

    double newOrder = orders.stream().mapToDouble(x -> x).max().orElse(0) + 1;

    storage.set(cellsKey, "" + newOrder);
  }

  @Override public void deleteSelectedCellId(String nodeId, String cellId) {

    if (nodeId == null || cellId == null) {
      return;
    }

    String nodeId_ = encodeId(nodeId);
    String cellId_ = encodeId(cellId);

    String cellsKey = SelectedNodes + '.' + nodeId_ + '.' + Cell + '.' + cellId_ + '.' + Order;

    storage.set(cellsKey, null);
  }

  @Override public void flush() {
    storage.flush();
  }

  @Override public @NonNull List<String> getSelectedCellIds(String nodeId) {

    if (nodeId == null) {
      return List.of();
    }

    String              nodeId_ = encodeId(nodeId);
    Map<String, Double> map     = new HashMap<>();

    for (final String key : storage.findKeys(SelectedNodes + '.' + nodeId_ + '.' + Cell)) {

      final Matcher matcher = SEL_NODE_CELL_ORDER.matcher(key);
      if (matcher.matches()) {
        map.put(matcher.group(2), NumUtil.parseDoubleDef(storage.get(key).orElse("0"), 0));
      }

    }

    return map.entrySet().stream().sorted(Map.Entry.comparingByValue()).map(Map.Entry::getKey).map(StorageUtil::decodeId).toList();
  }
}
