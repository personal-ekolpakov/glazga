package kz.pompei.swing.tree.gui.model;

import java.awt.Point;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TreeContextMenuEvent {
  public final @NonNull Point        point;
  public final @NonNull Point        screenPoint;
  public final          HitTreePoint hit;
  public final          boolean      controlDown;
  public final          boolean      shiftDown;
}
