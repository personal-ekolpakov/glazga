package kz.pompei.swing.tree.gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JPanel;
import kz.pompei.glazga.utils.ChildrenIterator;
import kz.pompei.glazga.utils.GuiUtil;
import kz.pompei.glazga.utils.Height;
import kz.pompei.glazga.utils.events.Disconnector;
import kz.pompei.glazga.utils.events.Handler;
import kz.pompei.glazga.utils.events.HandlerObserver;
import kz.pompei.swing.tree.gui.model.HitTreePoint;
import kz.pompei.swing.tree.gui.model.IntAccess;
import kz.pompei.swing.tree.gui.model.IntAccessRef;
import kz.pompei.swing.tree.gui.model.ScrollStateFix;
import kz.pompei.swing.tree.gui.model.SwingTreeNode;
import kz.pompei.swing.tree.gui.model.TreeContextMenuEvent;
import kz.pompei.swing.tree.styles.SwingTreeStyle;
import lombok.NonNull;

public class SwingTree extends JPanel implements TreeGui<SwingTreeNode> {

  private       int                            userWidth           = 1;
  private       int                            userHeight          = 1;
  private final IntAccess                      scrollX;
  private final IntAccess                      scrollY;
  private final SwingScroll                    scroll;
  public final  SwingTreeStyle                 style;
  public final  List<SwingTreeNode>            roots               = new ArrayList<>();
  private       String                         directSelectNodeId  = null;
  public final  HandlerObserver<SwingTreeNode> chevronMousePressed = new HandlerObserver<>();
  private       ScrollState                    scrollState         = new ScrollStateFix();
  private       SwingTreeSelection             selection           = new SwingTreeSelectionLogic(new SwingTreeSelectionStorageFix());

  public final HandlerObserver<TreeContextMenuEvent> contextMenuRequested = new HandlerObserver<>();
  public final HandlerObserver<SwingTreeNode>        doubleClicked        = new HandlerObserver<>();

  private Disconnector selectionDisconnector;

  public SwingTree(@NonNull SwingTreeStyle style) {
    this.style   = style;
    this.scrollX = IntAccessRef.of(() -> scrollState.scrollX(), x -> scrollState.setScrollX(x));
    this.scrollY = IntAccessRef.of(() -> scrollState.scrollY(), y -> scrollState.setScrollY(y));
    scroll       = new SwingScroll(this, style::scrollStyle, () -> userWidth, () -> userHeight, scrollX, scrollY);

    scroll.mouse.moved.add(this::mouseMoved);
    scroll.mouse.exited.add(this::mouseExited);
    scroll.mouse.pressed.add(this::mousePressed);
    selectionDisconnector = selection.addListener(this::repaint);
  }

  @Override public @NonNull Iterator<SwingTreeNode> nodeIterator() {
    return new ChildrenIterator<>(roots, x -> x.children, (t, level) -> t.level = level);
  }

  public @NonNull SwingTreeSelection selection() {
    return selection;
  }

  public void setSelection(@NonNull SwingTreeSelection selection) {
    this.selection = selection;
    {
      final Disconnector x = selectionDisconnector;
      if (x != null) {
        x.disconnect();
      }
    }
    selectionDisconnector = selection.addListener(this::repaint);
  }

  public void setScrollState(@NonNull ScrollState scrollState) {
    this.scrollState = scrollState;
  }

  public HitTreePoint findHit(Point point) {
    if (point == null) {
      return null;
    }

    for (final SwingTreeNode node : paintedNodes) {

      if (node.boundsChevronIcon.contains(point)) {
        return new HitTreePoint(node, true, null);
      }

      for (final SwingTreeNode.Cell cell : node.cells) {
        if (cell.boundsCell.contains(point)) {
          return new HitTreePoint(node, false, cell);
        }
      }

      if (node.boundsLine.contains(point)) {
        return new HitTreePoint(node, false, null);
      }
    }

    return null;
  }

  @Override public HandlerObserver<SwingTreeNode> chevronMousePressed() {
    return chevronMousePressed;
  }

  private boolean mousePressed(@NonNull MouseEvent e) {

    HitTreePoint hit = findHit(e.getPoint());

    if (hit != null
        && hit.node.isExpandable()
        && hit.onChevron
        && e.getButton() == MouseEvent.BUTTON1
        && !e.isControlDown()
        && !e.isShiftDown()) {

      chevronMousePressed.fire(hit.node);
    }

    if ((hit == null || !hit.onChevron || !hit.node.isExpandable())
        && e.getButton() == MouseEvent.BUTTON1
        || (e.getButton() == MouseEvent.BUTTON3 && !e.isControlDown() && !e.isShiftDown())) {

      if (!e.isControlDown() && !e.isShiftDown()) {
        directSelectNodeId = null;

        if (hit == null) {
          selection.setSelectedNodeIds(null);
        } else if (hit.cell == null) {
          selection.setSelectedNodeIds(List.of(hit.node.id));
        } else {
          selection.setSelectedCellIds(Map.of(hit.node.id, Set.of(hit.cell.id)));
        }

      } else if (e.isControlDown() && !e.isShiftDown()) {
        directSelectNodeId = null;

        if (hit != null) {

          if (hit.cell == null) {
            if (selection.isSelectedNode(hit.node.id)) {
              selection.removeSelectedNode(hit.node.id);
            } else {
              selection.setActiveNodeId(hit.node.id);
            }
          } else {

            if (selection.isSelectedCell(hit.node.id, hit.cell.id)) {
              selection.removeSelectedCell(hit.node.id, hit.cell.id);
            } else {
              selection.setActiveNodeId(hit.node.id);
              selection.setActiveCellId(hit.node.id, hit.cell.id);
            }

          }

        }


      } else if (!e.isControlDown() && e.isShiftDown() && hit != null) {
        String startId = directSelectNodeId, activeNodeId = selection.activeNodeId();

        if (startId != null && activeNodeId != null) {

          int foundCount = 0;

          Iterator<SwingTreeNode> iterator = nodeIterator();
          while (iterator.hasNext()) {
            SwingTreeNode node = iterator.next();

            if (foundCount == 1) {
              selection.removeSelectedNode(node.id);
            }

            if (startId.equals(node.id) || activeNodeId.equals(node.id)) {
              foundCount++;
            }

            if (foundCount == 1) {
              selection.removeSelectedNode(node.id);
            }

            if (foundCount == 2) {
              break;
            }
          }

        }

        if (startId == null) {
          startId = directSelectNodeId = activeNodeId;
        }
        if (startId == null || startId.equals(hit.node.id)) {
          selection.setActiveNodeId(hit.node.id);
        } else {

          int          foundCount  = 0;
          List<String> foundIds    = new ArrayList<>();
          boolean      startBottom = false;

          Iterator<SwingTreeNode> iterator = nodeIterator();
          while (iterator.hasNext()) {
            SwingTreeNode node = iterator.next();
            if (foundCount == 1) {
              foundIds.add(node.id);
            }

            if (startId.equals(node.id)) {
              foundCount++;
            }
            if (hit.node.id.equals(node.id)) {
              if (foundCount == 0) {
                startBottom = true;
              }
              foundCount++;
            }

            if (foundCount == 1) {
              foundIds.add(node.id);
            }

          }

          if (foundCount == 2 && foundIds.size() > 0) {

            if (startBottom) {
              Collections.reverse(foundIds);
            }

            String lastFoundId = null;

            for (final String foundId : foundIds) {
              lastFoundId = foundId;
              selection.addSelection(foundId, null);
            }

            selection.setActiveNodeId(lastFoundId);

          }

        }
      }

    }

    if (e.getButton() == MouseEvent.BUTTON3) {
      Point pointOnScreen = new Point(e.getXOnScreen(), e.getYOnScreen());
      contextMenuRequested.fire(new TreeContextMenuEvent(e.getPoint(), pointOnScreen, hit, e.isControlDown(), e.isShiftDown()));
    }

    if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 2 && !e.isControlDown() && !e.isShiftDown() && hit != null) {
      doubleClicked.fire(hit.node);
    }

    return true;
  }

  private boolean mouseExited(MouseEvent e) {
    repaint();
    return true;
  }

  @Override public @NonNull List<SwingTreeNode> roots() {
    return roots;
  }

  private boolean mouseMoved(MouseEvent e) {
    return true;
  }

  @Override
  public void paint(Graphics g1) {
    super.paint(g1);
    Graphics2D g = GuiUtil.applyHints(g1);
    paint(g);
    scroll.paint(g);
  }

  private final List<SwingTreeNode> paintedNodes = new ArrayList<>();

  private void paint(@NonNull Graphics2D g) {

    paintedNodes.clear();

    Iterator<SwingTreeNode> iterator            = nodeIterator();
    float                   screenHeight        = getHeight();
    int                     left                = -this.scrollX.get() + style.padding().left();
    List<String>            showNodeTrapNodeIds = new ArrayList<>();
    float                   y                   = -this.scrollY.get() + style.padding().top();
    float                   calcHeight          = style.padding().top();
    int                     maxNodeWidth        = 1;

    while (iterator.hasNext()) {

      final SwingTreeNode node       = iterator.next();
      final int           nodeWidth  = node.width(g);
      final Height        height     = node.height(g);
      final float         lineHeight = height.height + style.node().paddingTop() + style.node().paddingBottom();
      final float         y1         = y;
      final float         y2         = y + lineHeight;

      calcHeight += lineHeight;
      if (maxNodeWidth < nodeWidth) {
        maxNodeWidth = nodeWidth;
      }

      y = y2;

      if (y2 <= 0) {
        showNodeTrapNodeIds.add(node.id);
        continue;
      }

      final float baseY = y1 + height.base + style.node().paddingTop();

      if (y1 < screenHeight) {
        node.paint(g, left, baseY);
        paintedNodes.add(node);
        showNodeTrapNodeIds.add(node.id);
      }

    }

    showNodeTrapNodeIds.forEach(this::fireShowNodeTrap);

    final float moreHeight      = getHeight();
    final float heightIncrement = calcHeight < 2f * moreHeight / 3f ? 0 : moreHeight / 3f;
    final int   newUserHeight   = Math.round(calcHeight + style.padding().bottom() + heightIncrement);
    final int   newUserWidth    = maxNodeWidth + style.padding().right();

    if (userWidth != newUserWidth || userHeight != newUserHeight) {
      userWidth  = newUserWidth;
      userHeight = newUserHeight;
      repaint();
    }

  }

  public SwingTreeNode newRoot(@NonNull String id) {
    return newRoot(id, null);
  }

  public SwingTreeNode newRoot(@NonNull String id, String nodeType) {
    SwingTreeNode ret = new SwingTreeNode(this, id);
    ret.nodeType = nodeType;
    roots.add(ret);
    return ret;
  }

  private final Map<String, List<Handler<String>>> showNodeTraps = new HashMap<>();

  @Override
  public void installTrap_OnShowNode(@NonNull String nodeId, @NonNull Handler<String> handler) {
    showNodeTraps.computeIfAbsent(nodeId, k -> new ArrayList<>()).add(handler);
  }

  private void fireShowNodeTrap(@NonNull String nodeId) {
    List<Handler<String>> handlerList = showNodeTraps.remove(nodeId);
    if (handlerList == null) {
      return;
    }

    List<Exception> errors = new ArrayList<>();

    for (final Handler<String> handler : handlerList) {
      try {
        handler.handle(nodeId);
      } catch (Exception e) {
        errors.add(e);
      }
    }

    if (errors.size() > 0) {
      Exception err = errors.remove(errors.size() - 1);

      for (final Exception error : errors) {
        new Thread(() -> {
          throw error instanceof RuntimeException r ? r : new RuntimeException(error);
        }).start();
      }

      throw err instanceof RuntimeException r ? r : new RuntimeException(err);
    }

  }

}
