package kz.pompei.swing.tree.gui.model;

public interface IntAccess {

  int get();

  void set(int value);

}
