package kz.pompei.swing.tree.gui;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BooleanSupplier;
import kz.pompei.glazga.utils.events.Disconnector;
import kz.pompei.glazga.utils.events.VoidHandler;
import kz.pompei.glazga.utils.events.VoidHandlerObserver;
import lombok.NonNull;

import static java.util.stream.Collectors.toMap;

public class SwingTreeSelectionLogic implements SwingTreeSelection {

  private final @NonNull SwingTreeSelectionStorage storage;
  private final          VoidHandlerObserver       observer = new VoidHandlerObserver();

  public BooleanSupplier multiple    = () -> true;
  public BooleanSupplier selectCells = () -> true;

  public SwingTreeSelectionLogic(@NonNull SwingTreeSelectionStorage storage) {
    this.storage = storage;
  }

  private @NonNull LinkedHashMap<String, LinkedHashSet<String>> extractSelectedCellIds() {
    LinkedHashMap<String, LinkedHashSet<String>> ret = new LinkedHashMap<>();
    for (final String nodeId : storage.getSelectedNodeIds()) {
      ret.put(nodeId, new LinkedHashSet<>(storage.getSelectedCellIds(nodeId)));
    }
    return ret;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" + (multiple.getAsBoolean() ? "MUL" : "SINGLE")
           + " (" + storage.activeNodeId() + "/" + storage.activeCellId() + ") " + extractSelectedCellIds() + "}";
  }

  private class State implements AutoCloseable {

    private final String                             savedActiveNodeId    = storage.activeNodeId();
    private final String                             savedActiveCellId    = storage.activeCellId();
    private final Map<String, LinkedHashSet<String>> savedSelectedCellIds = extractSelectedCellIds();

    private void normalize() {
      if (storage.activeNodeId() == null) {
        final var x = extractSelectedCellIds();
        if (x.isEmpty()) {
          storage.setActiveCellId(null);
          return;
        }

        {
          String[] nodeId = new String[]{null};
          x.keySet().forEach(id -> nodeId[0] = id);
          storage.setActiveNodeId(nodeId[0]);
        }
      }

      assert storage.activeNodeId() != null;

      storage.addSelectedNodeId(storage.activeNodeId());

      final List<String> selectedCellsInActiveNode = storage.getSelectedCellIds(storage.activeNodeId());

      final String activeCellId = storage.activeCellId();
      if (activeCellId == null) {
        String[] cellId = new String[]{null};
        selectedCellsInActiveNode.forEach(id -> cellId[0] = id);
        storage.setActiveCellId(cellId[0]);
      } else {
        storage.addSelectedCellId(storage.activeNodeId(), activeCellId);
      }

    }

    @Override
    public void close() {
      normalize();

      if (!multiple.getAsBoolean()) {
        storage.getSelectedNodeIds().forEach(storage::deleteSelectedNodeId);
      }
      if (!selectCells.getAsBoolean()) {
        storage.setActiveCellId(null);
        for (final String nodeId : storage.getSelectedNodeIds()) {
          storage.getSelectedCellIds(nodeId).forEach(cellId -> storage.deleteSelectedCellId(nodeId, cellId));
        }
      }

      if (Objects.equals(savedActiveNodeId, storage.activeNodeId())
          && Objects.equals(savedActiveCellId, storage.activeCellId())
          && Objects.equals(savedSelectedCellIds, extractSelectedCellIds())) {
        return;
      }

      storage.flush();
      observer.fire();
    }
  }

  @Override
  public String activeNodeId() {
    return storage.activeNodeId();
  }

  @Override
  public void setActiveNodeId(String nodeId) {
    try (State ignore = new State()) {
      storage.setActiveNodeId(nodeId);
    }
  }

  @Override
  public String activeCellId() {
    return storage.activeCellId();
  }

  @Override
  public void setActiveCellId(String nodeId, String cellId) {
    try (State ignore = new State()) {
      if (nodeId != null) {
        storage.setActiveNodeId(nodeId);
      }
      storage.setActiveCellId(cellId);
    }
  }

  @Override
  public @NonNull Set<String> selectedNodeIds() {
    return new LinkedHashSet<>(storage.getSelectedNodeIds());
  }

  @Override
  public void setSelectedNodeIds(Collection<String> nodeIds) {
    try (State ignore = new State()) {

      if (nodeIds == null || nodeIds.isEmpty()) {
        storage.getSelectedNodeIds().forEach(storage::deleteSelectedNodeId);
        storage.setActiveNodeId(null);
        storage.setActiveCellId(null);
        return;
      }

      final Set<String> toRemove = new HashSet<>(storage.getSelectedNodeIds());
      toRemove.removeAll(nodeIds);
      toRemove.forEach(storage::deleteSelectedNodeId);

      nodeIds.forEach(storage::addSelectedNodeId);

      if (storage.activeNodeId() != null && !storage.getSelectedNodeIds().contains(storage.activeNodeId())) {
        storage.setActiveNodeId(null);
        storage.setActiveCellId(null);
      }

      String activeCellId = storage.activeCellId();
      if (activeCellId == null || !storage.getSelectedCellIds(storage.activeNodeId()).contains(activeCellId)) {
        storage.setActiveCellId(null);
      }

    }
  }

  @Override
  public @NonNull Map<String, Set<String>> selectedCellIds() {
    return extractSelectedCellIds().entrySet()
                                   .stream()
                                   .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  @Override
  public void setSelectedCellIds(Map<String, Collection<String>> selectedCellIds) {
    try (State ignore = new State()) {

      storage.getSelectedNodeIds().forEach(storage::deleteSelectedNodeId);

      if (selectedCellIds == null) {
        storage.setActiveNodeId(null);
        storage.setActiveCellId(null);
        return;
      }

      for (final Map.Entry<String, Collection<String>> e : selectedCellIds.entrySet()) {
        String             nodeId  = e.getKey();
        Collection<String> cellIds = e.getValue();
        storage.addSelectedNodeId(nodeId);
        cellIds.forEach(cellId -> storage.addSelectedCellId(nodeId, cellId));
      }

      final String activeNodeId = storage.activeNodeId();

      final Collection<String> selectedCellsInActiveNode = activeNodeId == null ? null : selectedCellIds.get(activeNodeId);
      if (selectedCellsInActiveNode == null) {
        storage.setActiveNodeId(null);
        storage.setActiveCellId(null);
      } else if (storage.activeCellId() == null || !selectedCellsInActiveNode.contains(storage.activeCellId())) {
        storage.setActiveCellId(null);
      }

    }
  }

  @Override
  public void addSelection(String nodeId, String cellId) {
    try (State ignore = new State()) {

      if (nodeId == null) {

        if (cellId == null) {
          return;
        }

        if (storage.activeNodeId() == null) {
          return;
        }

        storage.addSelectedCellId(storage.activeNodeId(), cellId);

        return;
      }

      storage.addSelectedCellId(nodeId, cellId);

    }
  }

  @Override
  public void removeSelectedNode(String nodeId) {
    if (nodeId == null) {
      return;
    }
    try (State ignore = new State()) {

      if (nodeId.equals(storage.activeNodeId())) {
        storage.setActiveNodeId(null);
        storage.setActiveCellId(null);
      }

      storage.deleteSelectedNodeId(nodeId);

    }
  }

  @Override
  public void removeSelectedCell(String nodeId, String cellId) {
    try (State ignore = new State()) {

      if (cellId == null) {
        return;
      }

      if (nodeId == null) {
        nodeId = storage.activeNodeId();
      }

      if (nodeId == null) {
        return;
      }

      storage.deleteSelectedCellId(nodeId, cellId);

      if (cellId.equals(storage.activeCellId())) {
        storage.setActiveCellId(null);
      }

    }
  }

  @Override
  public @NonNull Disconnector addListener(VoidHandler handler) {
    return observer.add(handler);
  }

  @Override
  public boolean isSelectedNode(String nodeId) {
    return multiple.getAsBoolean() ? storage.getSelectedNodeIds().contains(nodeId) : Objects.equals(storage.activeNodeId(), nodeId);
  }

  @Override
  public boolean isSelectedCell(String nodeId, String cellId) {
    return isSelectedNode(nodeId) && storage.getSelectedCellIds(nodeId).contains(cellId);

  }

  @Override public boolean isActiveNode(String nodeId) {
    return Objects.equals(storage.activeNodeId(), nodeId);
  }

  @Override public boolean isActiveCell(String nodeId, String cellId) {
    return (nodeId == null || nodeId.equals(storage.activeNodeId())) && Objects.equals(storage.activeCellId(), cellId);
  }
}
