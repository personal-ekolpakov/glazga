package kz.pompei.swing.tree.gui;

import java.util.List;
import kz.pompei.swing.tree.styles.NodeStatus;
import lombok.NonNull;

public interface TreeNodeGui<TN extends TreeNodeGui<TN>> {

  @NonNull String id();

  NodeStatus status();

  void setStatus(NodeStatus status);

  @NonNull List<TN> children();
}
