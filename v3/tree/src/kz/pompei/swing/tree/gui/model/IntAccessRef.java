package kz.pompei.swing.tree.gui.model;

import java.util.function.IntConsumer;
import java.util.function.IntSupplier;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class IntAccessRef implements IntAccess {
  private final @NonNull IntSupplier getter;
  private final @NonNull IntConsumer setter;

  @Override
  public int get() {
    return getter.getAsInt();
  }

  @Override
  public void set(int value) {
    setter.accept(value);
  }
}
