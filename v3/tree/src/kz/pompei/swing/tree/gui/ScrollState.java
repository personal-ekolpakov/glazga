package kz.pompei.swing.tree.gui;

public interface ScrollState {

  int scrollX();

  int scrollY();

  void setScrollX(int scrollX);

  void setScrollY(int scrollY);

}
