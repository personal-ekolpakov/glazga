package kz.pompei.swing.tree.gui.model;

public class IntHolder implements IntAccess {
  private int value;

  public IntHolder(int value) {
    this.value = value;
  }

  @Override
  public int get() {
    return value;
  }

  @Override
  public void set(int value) {
    this.value = value;
  }
}
