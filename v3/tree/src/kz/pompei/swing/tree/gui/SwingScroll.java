package kz.pompei.swing.tree.gui;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import javax.swing.JComponent;
import kz.pompei.glazga.utils.events.MouseEvents;
import kz.pompei.swing.tree.gui.model.IntAccess;
import kz.pompei.swing.tree.styles.ScrollStyle;
import lombok.NonNull;

public class SwingScroll {

  public final MouseEvents mouse = new MouseEvents();

  private final JComponent  owner;
  private final IntSupplier userWidth;
  private final IntSupplier userHeight;
  private final IntAccess   userDeltaX;
  private final IntAccess   userDeltaY;

  private final Supplier<ScrollStyle> style;

  private int   userDeltaOnMousePressedX;
  private int   userDeltaOnMousePressedY;
  private Point mouseDraggedAt = new Point(0, 0);
  private Point mousePressedAt = new Point(0, 0);

  private boolean lastPressedInVertical    = false;
  private boolean lastPressedInHorizontal  = false;
  private int     lastPressedInSlider      = 0;
  private boolean lastPressedLeftAndFree   = false;
  private boolean lastPressedMiddleAndFree = false;

  private boolean hoverVertical   = false;
  private boolean hoverHorizontal = false;

  public SwingScroll(@NonNull JComponent owner,
                     @NonNull Supplier<ScrollStyle> style,
                     @NonNull IntSupplier userWidth,
                     @NonNull IntSupplier userHeight,
                     @NonNull IntAccess userDeltaX,
                     @NonNull IntAccess userDeltaY) {

    this.owner      = owner;
    this.style      = style;
    this.userWidth  = userWidth;
    this.userHeight = userHeight;
    this.userDeltaX = userDeltaX;
    this.userDeltaY = userDeltaY;

    owner.addMouseListener(mouse);
    owner.addMouseMotionListener(mouse);
    owner.addMouseWheelListener(mouse);

    mouse.moved.add(this::mouseMoved);
    mouse.dragged.add(this::mouseDragged);
    mouse.pressed.add(this::mousePressed);
    mouse.released.add(this::mouseReleased);
    mouse.exited.add(this::mouseExit);
    mouse.wheelMoved.add(this::mouseWheelMoved);
  }

  private void stepWheelY(int step) {
    ScrollCalc sc = new ScrollCalc();
    setAboutHeight(sc);
    applyStepWheel(sc, userDeltaY, step);
  }

  private void stepWheelX(int step) {
    ScrollCalc sc = new ScrollCalc();
    setAboutWidth(sc);
    applyStepWheel(sc, userDeltaX, step);
  }

  private void applyStepWheel(@NonNull ScrollCalc sc, @NonNull IntAccess userDelta, int step) {
    sc.userDeltaOnMousePressed = userDelta.get();
    sc.mousePressedPos         = 0;
    sc.mouseDraggedPos         = step;

    sc.calcUserDeltaScrolled();
    if (sc.needRepaint) {
      userDelta.set(sc.userDelta);
      owner.repaint();
    }
  }

  @SuppressWarnings("DuplicatedCode")
  private boolean mouseWheelMoved(MouseWheelEvent e) {
    if (isFree(e)) {
      stepWheelY(style.get().wheelSliderStep() * (e.getWheelRotation() > 0 ? +1 : -1));
      return false;
    }

    if (isShift(e)) {
      stepWheelX(style.get().wheelSliderStep() * (e.getWheelRotation() > 0 ? +1 : -1));
      return false;
    }

    return true;
  }

  private boolean mouseMoved(@NonNull MouseEvent e) {
    setHover(inVertical(e.getPoint()), inHorizontal(e.getPoint()));
    return true;
  }

  private boolean mouseDragged(@NonNull MouseEvent e) {
    mouseDraggedAt = e.getPoint();

    if (dragScrollEnabled()) {
      if (lastPressedInVertical) {
        ScrollCalc sc = new ScrollCalc();
        setAboutHeight(sc);

        sc.calcUserDeltaScrolled();

        if (sc.needRepaint) {
          userDeltaY.set(sc.userDelta);
          owner.repaint();
        }
        return false;
      }

      if (lastPressedInHorizontal) {
        ScrollCalc sc = new ScrollCalc();
        setAboutWidth(sc);

        sc.calcUserDeltaScrolled();

        if (sc.needRepaint) {
          userDeltaX.set(sc.userDelta);
          owner.repaint();
        }
        return false;
      }

      return false;
    }

    if (dragMiddleEnabled()) {

      ScrollCalc scX = new ScrollCalc();
      ScrollCalc scY = new ScrollCalc();

      setAboutWidth(scX);
      setAboutHeight(scY);

      scX.calcUserDeltaMiddle();
      scY.calcUserDeltaMiddle();

      if (scX.needRepaint) {
        userDeltaX.set(scX.userDelta);
        owner.repaint();
      }
      if (scY.needRepaint) {
        userDeltaY.set(scY.userDelta);
        owner.repaint();
      }

      return false;
    }

    return true;
  }

  private boolean mousePressed(@NonNull MouseEvent e) {
    mousePressedAt           = e.getPoint();
    mouseDraggedAt           = e.getPoint();
    lastPressedInVertical    = inVertical(mousePressedAt);
    lastPressedInHorizontal  = inHorizontal(mousePressedAt);
    lastPressedInSlider      = inSlider();
    lastPressedLeftAndFree   = isLeftAndFree(e);
    lastPressedMiddleAndFree = isMiddleAndFree(e);
    userDeltaOnMousePressedX = userDeltaX.get();
    userDeltaOnMousePressedY = userDeltaY.get();

    if (lastPressedLeftAndFree && lastPressedInVertical && (lastPressedInSlider == +1 || lastPressedInSlider == -1)) {
      stepWheelY(lastPressedInSlider * style.get().underSliderStep());
      return false;
    }

    if (lastPressedLeftAndFree && lastPressedInHorizontal && (lastPressedInSlider == +1 || lastPressedInSlider == -1)) {
      stepWheelX(lastPressedInSlider * style.get().underSliderStep());
      return false;
    }

    return !dragScrollEnabled() && !dragMiddleEnabled();
  }


  private boolean dragScrollEnabled() {
    return lastPressedLeftAndFree && (lastPressedInVertical || lastPressedInHorizontal) && lastPressedInSlider == 0;
  }

  private boolean dragMiddleEnabled() {
    return lastPressedMiddleAndFree;
  }

  private static boolean isLeftAndFree(@NonNull MouseEvent e) {
    return e.getButton() == MouseEvent.BUTTON1 && isFree(e);
  }

  private static boolean isMiddleAndFree(@NonNull MouseEvent e) {
    return e.getButton() == MouseEvent.BUTTON2 && isFree(e);
  }

  private static boolean isFree(@NonNull MouseEvent e) {
    return !e.isShiftDown() && !e.isControlDown();
  }

  private static boolean isShift(@NonNull MouseEvent e) {
    return e.isShiftDown() && !e.isControlDown();
  }

  private boolean mouseReleased(MouseEvent e) {
    return true;
  }

  private boolean mouseExit(MouseEvent e) {
    setHover(false, false);
    return true;
  }

  private void setHover(boolean hoverVertical, boolean hoverHorizontal) {
    if (this.hoverVertical == hoverVertical && this.hoverHorizontal == hoverHorizontal) {
      return;
    }
    this.hoverVertical   = hoverVertical;
    this.hoverHorizontal = hoverHorizontal;
    owner.repaint();
  }

  private boolean inVertical(@NonNull Point point) {
    return viewWidth() - style.get().fatness() <= point.x && point.x <= viewWidth();
  }

  private boolean inHorizontal(@NonNull Point point) {
    return viewHeight() - style.get().fatness() <= point.y && point.y <= viewHeight();
  }

  private int inSlider() {
    if (inVertical(mousePressedAt)) {
      ScrollCalc sc = new ScrollCalc();
      setAboutHeight(sc);
      sc.calcSlider();

      if (sc.sliderSize == 0) {
        return 10;
      }

      //@formatter:off
      if (mousePressedAt.y < sc.sliderPos                ) return -1;
      if (mousePressedAt.y > sc.sliderPos + sc.sliderSize) return +1;
                                                           return  0;
      //@formatter:on
    }
    if (inHorizontal(mousePressedAt)) {
      ScrollCalc sc = new ScrollCalc();
      setAboutWidth(sc);
      sc.calcSlider();

      if (sc.sliderSize == 0) {
        return 10;
      }

      //@formatter:off
      if (mousePressedAt.x < sc.sliderPos                ) return -1;
      if (mousePressedAt.x > sc.sliderPos + sc.sliderSize) return +1;
                                                           return  0;
      //@formatter:on
    }
    return 10;
  }

  private int viewWidth() {
    return owner.getWidth();
  }

  private int viewHeight() {
    return owner.getHeight();
  }

  public void paint(@NonNull Graphics2D g) {
    boolean tailPainted = paintVertical(g);
    paintHorizontal(g, tailPainted);
  }

  private boolean paintVertical(@NonNull Graphics2D g) {
    ScrollCalc sc = new ScrollCalc();
    setAboutHeight(sc);
    sc.calcSlider();

    if (sc.sliderSize == 0) {
      return false;
    }

    final int fatness = style.get().fatness();

    {
      g.setColor(hoverVertical ? style.get().colorScrollHover() : style.get().colorScroll());
      g.fillRect(viewWidth() - fatness, 0, fatness, viewHeight());
      g.setColor(hoverVertical ? style.get().colorSliderHover() : style.get().colorSlider());
      g.fillRect(viewWidth() - fatness, sc.sliderPos, fatness, sc.sliderSize);
    }
    return true;
  }

  private void paintHorizontal(@NonNull Graphics2D g, boolean tailAlreadyPainted) {
    ScrollCalc sc = new ScrollCalc();
    setAboutWidth(sc);
    sc.calcSlider();

    if (sc.sliderSize == 0) {
      return;
    }

    final int fatness = style.get().fatness();

    {
      g.setColor(hoverHorizontal ? style.get().colorScrollHover() : style.get().colorScroll());
      g.fillRect(0, viewHeight() - fatness, tailAlreadyPainted && hoverVertical ? viewWidth() - fatness : viewWidth(), fatness);
      g.setColor(hoverHorizontal ? style.get().colorSliderHover() : style.get().colorSlider());
      g.fillRect(sc.sliderPos, viewHeight() - fatness, sc.sliderSize, fatness);
    }
  }

  private void setAboutHeight(@NonNull ScrollCalc sc) {
    sc.userSize      = this.userHeight.getAsInt() + style.get().deltaUserHeight();
    sc.viewSize      = viewHeight();
    sc.userDelta     = userDeltaY.get();
    sc.minSliderSize = style.get().minSliderSize();

    sc.mousePressedPos         = mousePressedAt.y;
    sc.userDeltaOnMousePressed = userDeltaOnMousePressedY;
    sc.mouseDraggedPos         = mouseDraggedAt.y;
  }

  private void setAboutWidth(@NonNull ScrollCalc sc) {
    sc.userSize      = this.userWidth.getAsInt() + style.get().deltaUserWidth();
    sc.viewSize      = viewWidth();
    sc.userDelta     = userDeltaX.get();
    sc.minSliderSize = style.get().minSliderSize();

    sc.mousePressedPos         = mousePressedAt.x;
    sc.userDeltaOnMousePressed = userDeltaOnMousePressedX;
    sc.mouseDraggedPos         = mouseDraggedAt.x;
  }
}
