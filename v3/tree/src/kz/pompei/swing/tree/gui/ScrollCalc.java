package kz.pompei.swing.tree.gui;

public class ScrollCalc {

  public int userSize;
  public int viewSize;
  public int userDelta;

  public int minSliderSize;
  public int sliderSize;
  public int sliderPos;

  public int mousePressedPos;
  public int mouseDraggedPos;
  public int userDeltaOnMousePressed;

  public boolean needRepaint;

  public void calcSlider() {

    if (userSize <= viewSize) {
      sliderPos = sliderSize = 0;
      return;
    }

    float portion = (float) viewSize / (float) userSize;

    float startImageSize = (float) viewSize * portion;

    sliderSize = Math.round(Math.max((float) minSliderSize, startImageSize));

    float epsPos = (float) userDelta / (float) (userSize - viewSize);
    epsPos = epsPos > 1 ? 1 : epsPos;

    int maxSliderPos = viewSize - sliderSize;

    sliderPos = Math.round((float) maxSliderPos * epsPos);
  }

  public void calcUserDeltaScrolled() {

    int startUserDelta = userDelta;

    userDelta = userDeltaOnMousePressed;
    calcSlider();

    if (sliderSize == 0) {
      needRepaint = false;
      return;
    }

    int mouseMoves = mouseDraggedPos - mousePressedPos;

    int userSpace = userSize - viewSize;
    int viewSpace = viewSize - sliderSize;

    if (viewSpace <= 0) {
      needRepaint = false;
      return;
    }

    int userDeltaMoves = Math.round((float) mouseMoves / (float) viewSpace * (float) userSpace);

    userDelta = userDeltaOnMousePressed + userDeltaMoves;

    if (userDelta < 0) {
      userDelta = 0;
    } else if (userDelta > userSize - viewSize) {
      userDelta = userSize - viewSize;
    }

    needRepaint = startUserDelta != userDelta;
  }

  public void calcUserDeltaMiddle() {
    int startUserDelta = userDelta;
    userDelta   = userDeltaOnMousePressed + mousePressedPos - mouseDraggedPos;
    needRepaint = startUserDelta != userDelta;
  }
}
