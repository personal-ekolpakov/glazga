package kz.pompei.swing.tree.gui;

import kz.pompei.v3.storage.Storage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ScrollStateOverStorage implements ScrollState {
  private final @NonNull Storage storage;

  @Override public int scrollX() {
    try {
      return Integer.parseInt(storage.get("x").orElse("0"));
    } catch (NumberFormatException e) {
      return 0;
    }
  }

  @Override public int scrollY() {
    try {
      return Integer.parseInt(storage.get("y").orElse("0"));
    } catch (NumberFormatException e) {
      return 0;
    }
  }

  @Override public void setScrollX(int scrollX) {
    storage.set("x", "" + scrollX);
  }

  @Override public void setScrollY(int scrollY) {
    storage.set("y", "" + scrollY);
  }
}
