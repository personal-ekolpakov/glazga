package kz.pompei.swing.tree.logic;

import kz.pompei.glazga.utils.Page;
import kz.pompei.swing.tree.logic.model.TreeNodesPortion;
import lombok.NonNull;

public interface TreeLoadServiceSync<DataItem> {

  @SuppressWarnings("unused")
  TreeNodesPortion<DataItem> loadNodeChildren(String parentId, Page page);

  @SuppressWarnings("unused")
  DataItem loadNode(@NonNull String nodeId);

}
