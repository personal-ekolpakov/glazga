package kz.pompei.swing.tree.logic;

import lombok.NonNull;

public interface AsyncBack<T> {

  void accept(T t) throws Throwable;

  default void error(@NonNull Throwable error) throws Throwable {
    throw error;
  }

}
