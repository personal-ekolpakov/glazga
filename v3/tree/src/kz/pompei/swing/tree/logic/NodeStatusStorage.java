package kz.pompei.swing.tree.logic;

import lombok.NonNull;

public interface NodeStatusStorage {

  void setOpen(@NonNull String nodeId, boolean open);

  boolean isOpen(@NonNull String nodeId);

}
