package kz.pompei.swing.tree.logic;

public interface TreeItem {

  String id();

  boolean isFolder();

}
