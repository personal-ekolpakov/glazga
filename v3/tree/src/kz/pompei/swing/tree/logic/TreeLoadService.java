package kz.pompei.swing.tree.logic;

import kz.pompei.glazga.utils.Page;
import kz.pompei.swing.tree.logic.model.TreeNodesPortion;
import lombok.NonNull;

public interface TreeLoadService<DataItem> {

  void loadNodeChildren(String parentId, Page page, AsyncBack<TreeNodesPortion<DataItem>> result);

  void loadNode(@NonNull String nodeId, AsyncBack<DataItem> treeNodeBack);

}
