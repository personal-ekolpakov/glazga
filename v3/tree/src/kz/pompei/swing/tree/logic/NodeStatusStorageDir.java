package kz.pompei.swing.tree.logic;

import java.io.File;
import java.nio.file.Path;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor
public class NodeStatusStorageDir implements NodeStatusStorage {
  private final Path dir;

  private static @NonNull String prepareNodeId(@NonNull String nodeId) {
    return nodeId.replaceAll("/", "--");
  }

  @Override @SneakyThrows public void setOpen(@NonNull String nodeId, boolean open) {
    File file = dir.resolve(prepareNodeId(nodeId)).toFile();

    if (file.exists()) {
      if (!open) {
        file.delete();
      }
    } else {
      if (open) {
        file.getParentFile().mkdirs();
        file.createNewFile();
      }
    }
  }

  @Override public boolean isOpen(@NonNull String nodeId) {
    return dir.resolve(prepareNodeId(nodeId)).toFile().exists();
  }

}
