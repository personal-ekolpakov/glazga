package kz.pompei.swing.tree.logic;

import lombok.NonNull;

public class IllegalNodeId extends RuntimeException {
  public IllegalNodeId(@NonNull String placeId, @NonNull String nodeId) {
    super(placeId + " :: nodeId = " + nodeId);
  }
}
