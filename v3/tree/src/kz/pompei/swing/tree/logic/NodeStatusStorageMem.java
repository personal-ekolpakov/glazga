package kz.pompei.swing.tree.logic;

import java.util.HashSet;
import java.util.Set;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NodeStatusStorageMem implements NodeStatusStorage {

  private final Set<String> nodeIds = new HashSet<>();

  @Override public void setOpen(@NonNull String nodeId, boolean open) {
    if (open) {
      nodeIds.add(nodeId);
    } else {
      nodeIds.remove(nodeId);
    }
  }

  public Set<String> getNodeIds() {
    return Set.copyOf(nodeIds);
  }

  public void setNodeIds(Set<String> nodeIds) {
    this.nodeIds.clear();
    if (nodeIds != null) {
      this.nodeIds.addAll(nodeIds);
    }
  }

  @Override public boolean isOpen(@NonNull String nodeId) {
    return nodeIds.contains(nodeId);
  }
}
