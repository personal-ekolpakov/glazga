package kz.pompei.swing.tree.logic.model;

import java.util.List;
import kz.pompei.glazga.utils.Page;

public class TreeNodesPortion<Item> {
  public List<Item> list;
  public Page       page;
  public boolean    hasMore;
  public Integer    totalCount;

  public static <Item> TreeNodesPortion<Item> empty(Page page) {
    final TreeNodesPortion<Item> ret = new TreeNodesPortion<>();
    ret.page       = page;
    ret.hasMore    = false;
    ret.totalCount = null;
    ret.list       = List.of();
    return ret;
  }
}
