package kz.pompei.swing.tree.logic;

import kz.pompei.v3.storage.Storage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class NodeStatusStorageBridge implements NodeStatusStorage {
  private final @NonNull Storage storage;

  @Override public void setOpen(@NonNull String nodeId, boolean open) {
    storage.set(nodeId, open ? "1" : null);
  }

  @Override public boolean isOpen(@NonNull String nodeId) {
    return storage.get(nodeId).isPresent();
  }
}
