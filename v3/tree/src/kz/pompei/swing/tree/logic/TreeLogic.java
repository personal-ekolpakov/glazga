package kz.pompei.swing.tree.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.IntSupplier;
import java.util.function.Predicate;
import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.utils.events.VoidHandler;
import kz.pompei.swing.tree.gui.TreeGui;
import kz.pompei.swing.tree.gui.TreeNodeGui;
import kz.pompei.swing.tree.gui.TreeNodeProvider;
import kz.pompei.swing.tree.styles.NodeStatus;
import lombok.NonNull;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

public class TreeLogic<TreeNode extends TreeNodeGui<TreeNode>, DataItem extends TreeItem> {
  private final @NonNull TreeGui<TreeNode>                    gui;
  private final @NonNull NodeStatusStorage                    nodeStatusStorage;
  private final @NonNull TreeLoadService<DataItem>            service;
  private final @NonNull TreeNodeProvider<TreeNode, DataItem> treeNodeProvider;

  public IntSupplier pageSize = () -> 4;

  public TreeLogic(@NonNull TreeGui<TreeNode> gui,
                   @NonNull NodeStatusStorage nodeStatusStorage,
                   @NonNull TreeLoadService<DataItem> service,
                   @NonNull TreeNodeProvider<TreeNode, DataItem> treeNodeProvider) {

    this.gui               = gui;
    this.nodeStatusStorage = nodeStatusStorage;
    this.service           = service;
    this.treeNodeProvider  = treeNodeProvider;
    gui.chevronMousePressed().add(this::chevronMousePressed);
  }

  private void chevronMousePressed(@NonNull TreeNode node) {

    boolean isOpen = nodeStatusStorage.isOpen(node.id());
    nodeStatusStorage.setOpen(node.id(), !isOpen);

    syncChevrons();
  }

  private void refreshNode(String parentId,
                           @NonNull List<TreeNode> nodeList,
                           int offset,
                           @NonNull VoidHandler complete) {

    int currentPageSize = offset > 0 ? pageSize.getAsInt() : Math.max(pageSize.getAsInt(), nodeList.size());

    TreeNode waitingNode = treeNodeProvider.newWaitingNode(parentId, offset, currentPageSize);
    waitingNode.setStatus(NodeStatus.LEAF);
    nodeList.add(waitingNode);

    gui.repaint();

    service.loadNodeChildren(parentId, Page.with(offset, currentPageSize), result -> {

      removeNodeFromList(nodeList, waitingNode.id()::equals);

      Map<String, TreeNode> existsMap = nodeList.stream().collect(toMap(TreeNodeGui::id, x -> x));
      HashSet<String>       removeIds = new HashSet<>(existsMap.keySet());

      int resultListSize = result.list.size();

      for (final DataItem dataItem : result.list) {

        String   id         = dataItem.id();
        TreeNode existsNode = existsMap.get(id);

        if (existsNode == null) {
          TreeNode node = treeNodeProvider.newTreeNode(id);
          nodeList.add(node);
          treeNodeProvider.assign(node, dataItem);
          node.setStatus(dataItem.isFolder() ? NodeStatus.CLOSE : NodeStatus.LEAF);
        } else {
          treeNodeProvider.assign(existsNode, dataItem);
        }

        removeIds.remove(id);

      }

      if (offset == 0) {
        removeNodeFromList(nodeList, removeIds::contains);

        Set<String> dbNodeIds  = result.list.stream().map(TreeItem::id).collect(toSet());
        Set<String> guiNodeIds = nodeList.stream().map(TreeNodeGui::id).collect(toSet());

        if (!dbNodeIds.equals(guiNodeIds)) {
          throw new RuntimeException("6oUGKlNlDI :: Logic error, different ids: db" + dbNodeIds + " != gui" + guiNodeIds);
        }

        Map<String, TreeNode> guiMap   = nodeList.stream().collect(toMap(TreeNodeGui::id, x -> x));
        List<String>          dbIdList = result.list.stream().map(TreeItem::id).toList();

        int i = 0;
        for (final String nodeId : dbIdList) {
          TreeNode node = guiMap.get(nodeId);
          nodeList.set(i++, node);
        }

      }

      for (final TreeNode node : nodeList) {

        NodeStatus status = node.status();
        if (status == NodeStatus.LEAF) {
          continue;
        }

        boolean nodeIsOpen = status == NodeStatus.OPEN;

        boolean nodeMustBeOpen = nodeStatusStorage.isOpen(node.id());

        if (nodeIsOpen == nodeMustBeOpen) {
          continue;
        }

        node.setStatus(nodeMustBeOpen ? NodeStatus.OPEN : NodeStatus.CLOSE);

        if (nodeMustBeOpen) {
          refreshNode(node.id(), node.children(), 0, () -> {});
        } else {
          node.children().clear();
        }
      }

      if (result.hasMore && resultListSize > 0) {

        TreeNode nodeMore = treeNodeProvider.newMoreNode();
        nodeMore.setStatus(NodeStatus.LEAF);
        nodeList.add(nodeMore);

        gui.installTrap_OnShowNode(nodeMore.id(), nodeId -> {

          removeNodeFromList(nodeList, nodeMore.id()::equals);

          refreshNode(parentId, nodeList, offset + currentPageSize,
                      () -> removeNodeFromList(nodeList, id -> id.equals(nodeId)));
        });

      }

      complete.handle();
      gui.repaint();

    });
  }

  private void syncChevrons() {

    List<TreeNode> nodesToCleanChildren = new ArrayList<>();

    Iterator<TreeNode> iterator = gui.nodeIterator();
    while (iterator.hasNext()) {
      TreeNode node = iterator.next();

      NodeStatus status = node.status();
      if (status == null || status == NodeStatus.LEAF) {
        continue;
      }

      boolean isOpen = nodeStatusStorage.isOpen(node.id());

      if (isOpen && status == NodeStatus.OPEN) {
        continue;
      }
      if (!isOpen && status == NodeStatus.CLOSE) {
        continue;
      }

      if (isOpen) {
        node.setStatus(NodeStatus.OPEN);

        refreshNode(node.id(), node.children(), 0, () -> {});

      } else {
        node.setStatus(NodeStatus.CLOSE);
        nodesToCleanChildren.add(node);
      }

    }

    nodesToCleanChildren.forEach(n -> n.children().clear());
    if (nodesToCleanChildren.size() > 0) {
      gui.repaint();
    }

  }

  private void removeNodeFromList(@NonNull List<TreeNode> list, @NonNull Predicate<String> predicate) {
    int i = 0;
    while (i < list.size()) {

      if (predicate.test(list.get(i).id())) {
        list.remove(i);
      } else {
        i++;
      }

    }
  }


  public TreeNode findNodeWithId(String nodeId) {
    if (nodeId == null) {
      return null;
    }
    return findNodeWithIdInList(nodeId, gui.roots());
  }

  private TreeNode findNodeWithIdInList(@NonNull String nodeId, List<TreeNode> roots) {

    for (final TreeNode root : roots) {
      if (nodeId.equals(root.id())) {
        return root;
      }
      {
        TreeNode ret = findNodeWithIdInList(nodeId, root.children());
        if (ret != null) {
          return ret;
        }
      }
    }

    return null;
  }


  public void refresh(String parentNodeId) {

    if (parentNodeId == null) {
      refreshNode(null, gui.roots(), 0, () -> {});
      return;
    }

    TreeNode node = findNodeWithId(parentNodeId);

    if (node == null) {
      return;
    }

    service.loadNode(parentNodeId, dataItem -> {

      if (dataItem == null) {
        findAndRemoveNodeById(gui.roots(), parentNodeId);
        return;
      }

      treeNodeProvider.assign(node, dataItem);

      refreshNode(parentNodeId, node.children(), 0, () -> {});

    });

  }

  private boolean findAndRemoveNodeById(@NonNull List<TreeNode> nodeList, @NonNull String nodeId) {

    for (int i = 0; i < nodeList.size(); i++) {
      TreeNode node = nodeList.get(i);
      if (node.id().equals(nodeId)) {
        nodeList.remove(i);
        return true;
      }
      if (findAndRemoveNodeById(node.children(), nodeId)) {
        return true;
      }
    }

    return false;
  }

}
