package kz.pompei.swing.tree.logic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SyncAsyncBridge {
  private final @NonNull Consumer<Runnable> callerInvoker;
  private final @NonNull Consumer<Runnable> taskInvoker;

  public <Async> @NonNull Async createAsync(@NonNull Class<Async> asyncClass, @NonNull Object syncService) {

    Map<String, Method> syncMethods = new HashMap<>();

    for (final Method asyncMethod : asyncClass.getMethods()) {

      Class<?>[] asyncParamTypes = asyncMethod.getParameterTypes();
      if (asyncParamTypes.length > 0 && asyncParamTypes[asyncParamTypes.length - 1] == AsyncBack.class) {

        Class<?>[] syncParamTypes = new Class[asyncParamTypes.length - 1];
        System.arraycopy(asyncParamTypes, 0, syncParamTypes, 0, syncParamTypes.length);

        String methodName = asyncMethod.getName();

        Method syncMethod;
        try {
          syncMethod = syncService.getClass().getMethod(methodName, syncParamTypes);
        } catch (NoSuchMethodException e) {
          throw new RuntimeException("ciOLQjp9fT :: No sync method: was finding in class " + syncService.getClass().getName()
                                     + ", async method " + asyncMethod, e);
        }

        {
          Method existMethod = syncMethods.get(methodName);
          if (existMethod != null) {
            throw new RuntimeException("0yCYkMB1WJ :: Method " + methodName + " already registered for\n\t" + existMethod
                                       + ".\n Second method:\n" + syncMethod);
          }
        }

        syncMethods.put(methodName, syncMethod);
      }

    }

    InvocationHandler invocationHandler = (proxy, asyncMethod, args) -> {

      String methodName = asyncMethod.getName();

      Class<?>[] asyncParamTypes = asyncMethod.getParameterTypes();

      if ("toString".equals(methodName) && asyncParamTypes.length == 0) {
        return asyncClass.getSimpleName() + "##SYNC_PROXY@" + SyncAsyncBridge.this
               + "/over/" + System.identityHashCode(syncService);
      }

      if ("equals".equals(methodName) && asyncParamTypes.length == 1) {
        Object arg0 = args[0];
        //noinspection SimplifiableConditionalExpression
        return arg0 == null ? false : System.identityHashCode(proxy) == System.identityHashCode(arg0);
      }

      if ("hashCode".equals(methodName) && asyncParamTypes.length == 0) {
        return System.identityHashCode(proxy);
      }

      if (asyncParamTypes.length == 0) {
        throw new RuntimeException("TQkYdZQ1dt :: No parameters. Cannot invoke method " + asyncMethod);
      }

      int      lastParamIndex = asyncParamTypes.length - 1;
      Class<?> lastParamType  = asyncParamTypes[lastParamIndex];

      if (lastParamType != AsyncBack.class) {
        throw new RuntimeException("3vK6b1LhqQ :: Illegal last param type " + lastParamType + "."
                                   + " Cannot invoke method " + asyncMethod);
      }

      Method syncMethod = syncMethods.get(methodName);

      if (syncMethod == null) {
        throw new RuntimeException("dM4g88Px1h :: No aync method with name " + methodName + " in ");
      }

      Object[] syncArgs = new Object[asyncParamTypes.length - 1];
      System.arraycopy(args, 0, syncArgs, 0, syncArgs.length);

      //noinspection rawtypes
      AsyncBack asyncBack = (AsyncBack) args[lastParamIndex];

      taskInvoker.accept(() -> {

        Object result;
        try {
          result = syncMethod.invoke(syncService, syncArgs);
        } catch (IllegalAccessException e) {
          throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
          Throwable cause = e.getCause();
          callerInvoker.accept(() -> {
            try {
              asyncBack.error(cause);
            } catch (Throwable ex) {
              throw ex instanceof RuntimeException r ? r : new RuntimeException("2XNnZpGw4r :: Error in caller error block", ex);
            }
          });
          return;
        }

        callerInvoker.accept(() -> {
          try {
            //noinspection unchecked
            asyncBack.accept(result);
          } catch (Throwable e) {
            throw e instanceof RuntimeException r ? r : new RuntimeException("VBuH45DIcB :: Error in caller block", e);
          }
        });

      });

      return null;
    };

    //noinspection unchecked
    return (Async) Proxy.newProxyInstance(syncService.getClass().getClassLoader(), new Class[]{asyncClass}, invocationHandler);
  }

}
