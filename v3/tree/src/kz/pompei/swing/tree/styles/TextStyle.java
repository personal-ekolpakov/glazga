package kz.pompei.swing.tree.styles;

import java.awt.Color;
import java.awt.Font;
import lombok.NonNull;

public interface TextStyle {

  @NonNull Font font();

  @NonNull Color color();

  @NonNull Margin margin();

}
