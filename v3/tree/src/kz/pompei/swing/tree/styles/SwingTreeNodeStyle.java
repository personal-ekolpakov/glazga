package kz.pompei.swing.tree.styles;

import java.awt.Image;
import lombok.NonNull;

public interface SwingTreeNodeStyle {

  int nodeLevelStep();

  int paddingTop();

  int paddingBottom();

  @NonNull Image chevronIcon(NodeStatus nodeStatus, int size);

  float chevronIconSizeFactor();

  int chevronIconRight();

  @NonNull Image nodeIcon(String nodeType, int size);

  float nodeIconSizeFactor();

  int nodeIconRight();

  @NonNull TextStyle cellStyle(String cellType);

  RectStyle nodeBackgroundRect(String nodeType, @NonNull State state);

  RectStyle cellBackgroundRect(String cellType, @NonNull State state);

}
