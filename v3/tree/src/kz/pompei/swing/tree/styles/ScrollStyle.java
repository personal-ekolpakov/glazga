package kz.pompei.swing.tree.styles;

import java.awt.Color;

public interface ScrollStyle {
  //@formatter:off
  int minSliderSize   ();
  int fatness         ();
  int deltaUserHeight ();
  int deltaUserWidth  ();
  int wheelSliderStep ();
  int underSliderStep ();
  //@formatter:on

  //@formatter:off
  Color colorScroll      ();
  Color colorSlider      ();
  Color colorScrollHover ();
  Color colorSliderHover ();
  //@formatter:on
}
