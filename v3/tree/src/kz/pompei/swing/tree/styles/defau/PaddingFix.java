package kz.pompei.swing.tree.styles.defau;

import kz.pompei.swing.tree.styles.Padding;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class PaddingFix implements Padding {
  public final int left;
  public final int top;
  public final int right;
  public final int bottom;

  @Override
  public int left() {
    return left;
  }

  @Override
  public int top() {
    return top;
  }

  @Override
  public int right() {
    return right;
  }

  @Override
  public int bottom() {
    return bottom;
  }
}
