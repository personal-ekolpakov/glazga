package kz.pompei.swing.tree.styles;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import lombok.NonNull;

public class Painters {

  public static @NonNull Rectangle2D text(@NonNull TextStyle style,
                                          @NonNull Graphics2D g,
                                          @NonNull String text,
                                          float x,
                                          float y,
                                          boolean paint) {
    final Margin m = style.margin();

    g.setColor(style.color());
    g.setFont(style.font());

    if (paint) {
      g.drawString(text, x + m.left(), y);
    }

    Rectangle2D ret = g.getFont().getStringBounds(text, g.getFontRenderContext());
    ret.setRect(ret.getX() + x, ret.getY() + y, ret.getWidth() + m.left() + m.right(), ret.getHeight() + m.top() + m.bottom());
    return ret;
  }

  private static class Radius {
    final int arcWidth, arcHeight;

    private Radius(Float radiusX, Float radiusY) {
      if (radiusX == null && radiusY == null) {
        arcWidth  = 0;
        arcHeight = 0;
      } else {
        arcWidth  = radiusX == null ? Math.round(radiusY) : Math.round(radiusX);
        arcHeight = radiusY == null ? Math.round(radiusX) : Math.round(radiusY);
      }
    }

    boolean has() {
      return arcWidth > 0 || arcHeight > 0;
    }
  }

  public static void rect(@NonNull Rectangle2D r, @NonNull Graphics2D g, @NonNull RectStyle style) {

    final Color backgroundColor = style.backgroundColor();
    if (backgroundColor != null) {
      final Radius            radius = new Radius(style.backgroundRadiusX(), style.backgroundRadiusY());
      final Rectangle2D.Float r2     = new Rectangle2D.Float();

      g.setColor(backgroundColor);
      r2.setRect(r);

      if (radius.has()) {

        int x      = Math.round(r2.x);
        int y      = Math.round(r2.y);
        int width  = Math.round(r2.width);
        int height = Math.round(r2.height);

        g.fillRoundRect(x, y, width, height, radius.arcWidth, radius.arcHeight);

      } else {
        g.fill(r);
      }
    }

    final Color borderColor = style.borderColor();
    if (borderColor != null) {
      g.setColor(borderColor);
      final Stroke stroke = style.borderStroke();
      g.setStroke(stroke != null ? stroke : new BasicStroke());

      final Radius radius = new Radius(style.borderRadiusX(), style.borderRadiusY());

      Rectangle2D.Float r2 = new Rectangle2D.Float();
      r2.setRect(r);
      final float offset  = style.borderInlineOffset();
      final float offset2 = offset * 2;
      if (offset2 < r.getWidth() && offset2 < r.getHeight()) {

        r2.x      = (float) r.getX() + offset;
        r2.y      = (float) r.getY() + offset;
        r2.width  = (float) r.getWidth() - offset2 - 0.9f;
        r2.height = (float) r.getHeight() - offset2 - 0.9f;

        if (radius.has()) {

          int x      = Math.round(r2.x);
          int y      = Math.round(r2.y);
          int width  = Math.round(r2.width);
          int height = Math.round(r2.height);

          g.drawRoundRect(x, y, width, height, radius.arcWidth, radius.arcHeight);

        } else {
          g.draw(r2);
        }
      }
    }

  }

}
