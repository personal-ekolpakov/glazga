package kz.pompei.swing.tree.styles.defau;

import kz.pompei.swing.tree.styles.Padding;
import kz.pompei.swing.tree.styles.ScrollStyle;
import kz.pompei.swing.tree.styles.SwingTreeNodeStyle;
import kz.pompei.swing.tree.styles.SwingTreeStyle;
import lombok.NonNull;

public class SwingTreeStyleDefault implements SwingTreeStyle {
  private final ScrollStyleDefault        scrollStyle        = new ScrollStyleDefault();
  private final SwingTreeNodeStyleDefault swingTreeNodeStyle = new SwingTreeNodeStyleDefault();

  private final PaddingFix padding = PaddingFix.of(0, 0, 0, 0);

  @Override
  public @NonNull Padding padding() {
    return padding;
  }

  @Override
  public @NonNull ScrollStyle scrollStyle() {
    return scrollStyle;
  }

  @Override
  public @NonNull SwingTreeNodeStyle node() {
    return swingTreeNodeStyle;
  }

}
