package kz.pompei.swing.tree.styles.defau;

import java.awt.Color;
import java.awt.Font;
import kz.pompei.swing.tree.styles.Margin;
import kz.pompei.swing.tree.styles.TextStyle;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(staticName = "of")
public class TextStyleFix implements TextStyle {
  private final @NonNull Font   font;
  private final @NonNull Color  color;
  private final @NonNull Margin margin;

  @Override
  public @NonNull Font font() {
    return font;
  }

  @Override
  public @NonNull Color color() {
    return color;
  }

  @Override
  public @NonNull Margin margin() {
    return margin;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{"
           + "color=" + color
           + ", font=" + font
           + "}";
  }

}
