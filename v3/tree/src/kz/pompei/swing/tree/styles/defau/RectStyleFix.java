package kz.pompei.swing.tree.styles.defau;

import java.awt.Color;
import java.awt.Stroke;
import kz.pompei.swing.tree.styles.RectStyle;

public class RectStyleFix implements RectStyle {

  public Color  backgroundColor;
  public Float  backgroundRadiusX;
  public Float  backgroundRadiusY;
  public Color  borderColor;
  public float  borderInlineOffset;
  public Float  borderRadiusX;
  public Float  borderRadiusY;
  public Stroke borderStroke;

  @Override public Color backgroundColor() {
    return backgroundColor;
  }

  @Override public Float backgroundRadiusX() {
    return backgroundRadiusX;
  }

  @Override public Float backgroundRadiusY() {
    return backgroundRadiusY;
  }

  @Override public float borderInlineOffset() {
    return borderInlineOffset;
  }

  @Override public Float borderRadiusX() {
    return borderRadiusX;
  }

  @Override public Float borderRadiusY() {
    return borderRadiusY;
  }

  @Override public Stroke borderStroke() {
    return borderStroke;
  }

  @Override public Color borderColor() {
    return borderColor;
  }
}
