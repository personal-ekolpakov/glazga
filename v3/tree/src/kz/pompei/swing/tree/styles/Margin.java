package kz.pompei.swing.tree.styles;

public interface Margin {

  int left();

  int top();

  int right();

  int bottom();

}
