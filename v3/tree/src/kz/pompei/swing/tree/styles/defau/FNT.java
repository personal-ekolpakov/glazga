package kz.pompei.swing.tree.styles.defau;

import kz.pompei.swing.fonts.FontSource;
import kz.pompei.swing.fonts.defau.FontSourceDefault;

public class FNT {
  public final static FontSource FONT_SRC = new FontSourceDefault();
}
