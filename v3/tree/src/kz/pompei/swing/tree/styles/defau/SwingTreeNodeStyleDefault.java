package kz.pompei.swing.tree.styles.defau;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Image;
import kz.pompei.swing.fonts.FontDef;
import kz.pompei.swing.icons.Svg;
import kz.pompei.swing.tree.styles.NodeStatus;
import kz.pompei.swing.tree.styles.RectStyle;
import kz.pompei.swing.tree.styles.State;
import kz.pompei.swing.tree.styles.SwingTreeNodeStyle;
import kz.pompei.swing.tree.styles.TextStyle;
import lombok.NonNull;

import static kz.pompei.swing.icons.Svg.TRANSPARENT;

@SuppressWarnings("DuplicatedCode")
public class SwingTreeNodeStyleDefault implements SwingTreeNodeStyle {

  @Override
  public int nodeLevelStep() {
    return 25;
  }

  @Override
  public int paddingTop() {
    return 0;
  }

  @Override
  public int paddingBottom() {
    return 5;
  }

  @Override
  public @NonNull Image chevronIcon(@NonNull NodeStatus nodeStatus, int size) {
    return Svg.images.get(chevronIconResourceName(nodeStatus), size, size);
  }

  private static String chevronIconResourceName(@NonNull NodeStatus nodeStatus) {
    return switch (nodeStatus) {
      case OPEN -> "svg/treeExpanded.svg";
      case CLOSE -> "svg/treeCollapsed.svg";
      case LEAF -> TRANSPARENT;
    };
  }

  @Override public float chevronIconSizeFactor() {
    return 0.8f;
  }


  @Override public int chevronIconRight() {
    return 0;
  }

  @Override
  public @NonNull Image nodeIcon(String nodeType, int size) {
    return Svg.images.get(nodeIconResourceName(nodeType), size, size);
  }

  private @NonNull String nodeIconResourceName(String nodeType) {
    //@formatter:off
    if ("more"     .equals(nodeType)) return TRANSPARENT;
    if ("waiting"  .equals(nodeType)) return TRANSPARENT;
    if ("folder"   .equals(nodeType)) return "svg/folder.svg";
    if ("txt"      .equals(nodeType)) return "svg/text.svg";
    if ("jpeg"     .equals(nodeType)) return "svg/image.svg";
    if ("jpg"      .equals(nodeType)) return "svg/image.svg";
    if ("ant"      .equals(nodeType)) return "svg/ant.svg";
    if ("cassandra".equals(nodeType)) return "svg/cassandra.svg";
    if ("java"     .equals(nodeType)) return "svg/java.svg";
    return "svg/helpInactive.svg";
    //@formatter:on
  }

  @Override
  public float nodeIconSizeFactor() {
    return 1f;
  }

  @Override
  public int nodeIconRight() {
    return 10;
  }

  private final TextStyleFix def = TextStyleFix.of(
    FNT.FONT_SRC.get(FontDef.of("Playfair Display", 400, 40, false)),
    new Color(16, 1, 162), MarginFix.of(0, 0, 0, 0));

  private final TextStyleFix date = TextStyleFix.of(
    FNT.FONT_SRC.get(FontDef.of("Playfair Display", 400, 30, true)),
    new Color(16, 1, 162), MarginFix.of(10, 0, 0, 0));

  private final TextStyleFix description = TextStyleFix.of(
    FNT.FONT_SRC.get(FontDef.of("Playfair Display", 400, 30, false)),
    new Color(76, 76, 76), MarginFix.of(10, 0, 0, 0));

  private final TextStyleFix grayed = TextStyleFix.of(
    FNT.FONT_SRC.get(FontDef.of("Playfair Display", 400, 20, true)),
    new Color(140, 140, 140), MarginFix.of(10, 0, 0, 0));


  @Override
  public @NonNull TextStyle cellStyle(String cellType) {
    return cellType == null ? def : switch (cellType) {
      default -> def;
      case "date" -> date;
      case "grayed" -> grayed;
      case "description" -> description;
    };
  }

  @Override
  public RectStyle nodeBackgroundRect(String nodeType, @NonNull State state) {
    if (state == State.ACTIVE) {
      RectStyleFix style = new RectStyleFix();
      style.backgroundColor = new Color(34, 253, 76);

      style.borderColor        = new Color(23, 31, 168);
      style.borderInlineOffset = 3;
      style.borderRadiusX      = 13f;
      style.borderStroke       = new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, 1f, new float[]{4f, 3f}, 0f);

      return style;
    }

    if (state == State.SELECTED) {
      RectStyleFix style = new RectStyleFix();
      style.backgroundColor = new Color(34, 253, 76);
      return style;
    }

    return null;
  }

  @Override public RectStyle cellBackgroundRect(String cellType, @NonNull State state) {
    if (state == State.ACTIVE) {
      RectStyleFix style = new RectStyleFix();
      style.backgroundColor = new Color(0, 0, 0, 31);

      style.borderColor        = new Color(23, 31, 168);
      style.borderRadiusX      = 13f;
      style.borderInlineOffset = 3;
      style.borderStroke       = new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, 1f, new float[]{4f, 3f}, 0f);

      return style;
    }

    if (state == State.SELECTED) {
      RectStyleFix style = new RectStyleFix();
      style.backgroundColor = new Color(0, 0, 0, 31);
      return style;
    }

    return null;
  }
}
