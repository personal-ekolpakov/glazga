package kz.pompei.swing.tree.styles;

public interface Padding {
  int left();

  int top();

  int right();

  int bottom();
}
