package kz.pompei.swing.tree.styles;

import lombok.NonNull;

public interface SwingTreeStyle {

  @NonNull ScrollStyle scrollStyle();

  @NonNull SwingTreeNodeStyle node();

  @NonNull Padding padding();

}
