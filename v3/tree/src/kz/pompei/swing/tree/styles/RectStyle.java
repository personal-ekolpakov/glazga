package kz.pompei.swing.tree.styles;

import java.awt.Color;
import java.awt.Stroke;

public interface RectStyle {
  Color backgroundColor();

  Float backgroundRadiusX();

  Float backgroundRadiusY();

  Stroke borderStroke();

  float borderInlineOffset();

  Float borderRadiusX();

  Float borderRadiusY();

  Color borderColor();
}
