package kz.pompei.swing.tree;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.RndUtil;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.swing.icons.Svg;
import kz.pompei.swing.tree.gui.ScrollStateOverStorage;
import kz.pompei.swing.tree.gui.SwingTree;
import kz.pompei.swing.tree.gui.SwingTreeSelectionLogic;
import kz.pompei.swing.tree.gui.SwingTreeSelectionStorageBridge;
import kz.pompei.swing.tree.gui.TreeNodeProvider;
import kz.pompei.swing.tree.gui.model.SwingTreeNode;
import kz.pompei.swing.tree.logic.NodeStatusStorage;
import kz.pompei.swing.tree.logic.NodeStatusStorageDir;
import kz.pompei.swing.tree.logic.SyncAsyncBridge;
import kz.pompei.swing.tree.logic.TreeLoadService;
import kz.pompei.swing.tree.logic.TreeLogic;
import kz.pompei.swing.tree.model.FileHover;
import kz.pompei.swing.tree.styles.NodeStatus;
import kz.pompei.swing.tree.styles.SwingTreeStyle;
import kz.pompei.swing.tree.styles.defau.SwingTreeStyleDefault;
import kz.pompei.v3.storage.StoragePath;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import static kz.pompei.glazga.utils.FileUtils.extractExt;
import static kz.pompei.glazga.utils.GuiUtil.bindKeyAction;

public class SwingTreeLogicForm {

  final Path build = Paths.get("build");
  final Path files = build.resolve("files");

  SwingTree tree;

  TreeLogic<SwingTreeNode, FileHover> logic;

  public static void main(String[] args) {
    SwingTreeLogicForm form = new SwingTreeLogicForm();

    SwingUtilities.invokeLater(form::execute);
  }

  @RequiredArgsConstructor
  public static class FileDo {
    private final @NonNull File file;

    @SuppressWarnings("UnusedReturnValue") @SneakyThrows
    public FileDo con(String content) {
      Files.writeString(file.toPath(), content, StandardCharsets.UTF_8);
      return this;
    }

  }

  @SneakyThrows
  private FileDo file(@NonNull String path) {
    File file = files.resolve(path).toFile();
    file.getParentFile().mkdirs();
    if (!file.exists()) {
      file.createNewFile();
    }
    return new FileDo(file);
  }

  public void execute() {

    JFrame frame = new JFrame("Пример дерева с интерфейсом содержания");

    SizeLocationSaver.into(build.resolve("sizes_and_locations"))
                     .overJFrame("SwingTreeLogicForm", frame, true);

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(400, 300);
    frame.setIconImage(Svg.images.get("svg/cassandra.svg", 128, 128));

    SwingTreeStyle swingTreeStyle = new SwingTreeStyleDefault();

    tree = new SwingTree(swingTreeStyle);
    frame.getContentPane().add(tree);
    frame.pack();

    frame.setVisible(true);

    final String ownerClassSimpleName = getClass().getSimpleName();

    NodeStatusStorage nodeStatusStorage = new NodeStatusStorageDir(build.resolve(ownerClassSimpleName).resolve("opened_nodes"));

    var treeSelectionStorage = new SwingTreeSelectionStorageBridge(new StoragePath(build.resolve(ownerClassSimpleName)
                                                                                        .resolve("selection")));
    tree.setSelection(new SwingTreeSelectionLogic(treeSelectionStorage));

    final ScrollStateOverStorage scrollState = new ScrollStateOverStorage(new StoragePath(build.resolve(ownerClassSimpleName)
                                                                                               .resolve("scroll-state")));

    tree.setScrollState(scrollState);

    final int N      = 100;
    final int MaxLen = ("" + N).length();

    for (int i = 1; i <= N; i++) {
      String I = "" + i;
      while (I.length() < MaxLen) {
        //noinspection StringConcatenationInLoop
        I = "0" + I;
      }
      file("apple/Cloud" + I + ".txt").con("sir");
      file("fallout/YouTube-" + I + ".txt").con("sir");
    }

    file("apple_Linux0.txt").con("sir");
    file("apple_Linux1.txt").con("sir");
    file("apple_Linux2.txt").con("sir");
    file("apple_Linux3.txt").con("sir");
    file("apple_Linux4.txt").con("sir");
    file("apple_Linux5.txt").con("sir");
    file("apple_Linux6.txt").con("sir");
    file("apple_Linux7.txt").con("sir");
    file("apple_Linux8.txt").con("sir");
    file("apple_Linux9.txt").con("sir");
    file("apple_Red0.txt").con("sir");
    file("apple_Red1.txt").con("sir");
    file("apple_Red2.txt").con("sir");
    file("apple_Red3.txt").con("sir");
    file("apple_Red4.txt").con("sir");
    file("apple_Red5.txt").con("sir");
    file("apple_Red6.txt").con("sir");
    file("apple_Red7.txt").con("sir");
    file("apple_Red8.txt").con("sir");
    file("apple_Red9.txt").con("sir");
    file("apple_Status.txt").con("sir");
    file("apple_Stone.txt").con("sir");
    file("apple_Triangle.txt").con("sir");
    file("apple_Sky.txt").con("sir");
    file("apple_Clouds.txt").con("sir");
    file("apple_Simulation.txt").con("sir");
    file("apple_OldCastleLord.txt").con("sir");
    file("apple_Border.txt").con("sir");
    file("apple_Window.txt").con("sir");
    file("status.txt").con("sir");
    file("checkpoint.txt").con("sir");
    file("flower.jpeg").con("sir");
    file("group/stone.jpeg").con("sir");
    file("group/loads.jpeg").con("sir");
    file("group/download/find.jpeg").con("sir");
    file("group/download/lotus.txt").con("sir");
    file("mode/LoadListResult.java").con("sir");
    file("mode/TreeCell.java").con("sir");
    file("mode/TreeCellFix.java").con("sir");
    file("mode/logic/AsyncBlack.java").con("sir");
    file("mode/logic/IllegalNodeId.java").con("sir");
    file("mode/logic/NodeStatusStorage.java").con("sir");
    file("mode/logic/NodeStatusStorageDir.java").con("sir");
    file("mode/logic/SwingTreeLogic.java").con("sir");
    file("zero/Detail.java").con("sir");
    file("zero/DetailType.java").con("sir");
    file("zero/NativeHome.java").con("sir");
    file("zero/SrcFile.java").con("sir");
    file("zero/detail/AlxImport.java").con("sir");
    file("zero/detail/AlxMemory.java").con("sir");
    file("zero/detail/Operation.java").con("sir");
    file("zero/natives/AlxNative.java").con("sir");
    file("zero/natives/AlxNativeArg.java").con("sir");
    file("zero/natives/Arg_STATUS.java").con("sir");
    file("zero/natives/Arg_RED_BOTTLE.java").con("sir");
    file("zero/natives/Arg_SQUARE_ROOT.java").con("sir");
    file("zero/natives/Arg_SYSTEM_TRIANGLE_ROOF.java").con("sir");

    TreeLoadServiceSyncDir serviceSync = new TreeLoadServiceSyncDir(files);
    ExecutorService        executor    = Executors.newFixedThreadPool(1);
    SyncAsyncBridge        bridge      = new SyncAsyncBridge(SwingUtilities::invokeLater, executor::submit);

    @SuppressWarnings("unchecked")
    TreeLoadService<FileHover> service = bridge.createAsync(TreeLoadService.class, serviceSync);

    TreeNodeProvider<SwingTreeNode, FileHover> treeNodeProvider = new TreeNodeProvider<>() {

      @Override public @NonNull SwingTreeNode newTreeNode(@NonNull String nodeId) {
        return new SwingTreeNode(tree, nodeId);
      }

      @Override public @NonNull SwingTreeNode newWaitingNode(String parentId, int offset, int pageSize) {
        SwingTreeNode ret = new SwingTreeNode(tree, RndUtil.newId());
        ret.nodeType = "waiting";
        ret.cell("main", "... waiting", "waiting");
        ret.cell("description", "parentId = " + parentId + ", offset = " + offset, "description");
        return ret;
      }

      @Override public @NonNull SwingTreeNode newMoreNode() {
        SwingTreeNode ret = new SwingTreeNode(tree, RndUtil.newId());
        ret.nodeType = "more";
        ret.cell("main", "...", "more");
        return ret;
      }

      @Override public void assign(@NonNull SwingTreeNode target, @NonNull FileHover source) {

        target.status   = source.isFolder ? NodeStatus.CLOSE : NodeStatus.LEAF;
        target.nodeType = source.isFolder ? "folder" : extractExt(source.name);

        target.cells.clear();

        target.cell("name", source.name, null);
        if (source.description != null) {
          target.cell("description", source.description, "description");
        }
        target.cell("createdAt", source.createdAt, "grayed");

      }

    };

    logic = new TreeLogic<>(tree, nodeStatusStorage, service, treeNodeProvider);

    bindKeyAction(tree, KeyStroke.getKeyStroke("SPACE"), this::actionOnSpaceKey);

    logic.refresh(null);

  }

  private void actionOnSpaceKey() {

    List<String> nodesIds = new ArrayList<>(tree.selection().selectedNodeIds());
    System.out.println("8DwsJoOoVW :: nodesIds = " + nodesIds);

    System.out.println("uDBWp7yurB :: SPACE");
  }

}
