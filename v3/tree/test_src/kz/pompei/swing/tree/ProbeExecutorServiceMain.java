package kz.pompei.swing.tree;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ProbeExecutorServiceMain {

  public static void main(String[] args) {

    ExecutorService executor = Executors.newFixedThreadPool(1);

    executor.submit(new Runnable() {
      @Override public void run() {
        System.out.println("O4C8luPQmi");
      }
    });

  }

}
