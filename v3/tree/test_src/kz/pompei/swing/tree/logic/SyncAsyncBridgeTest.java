package kz.pompei.swing.tree.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import kz.pompei.glazga.utils.RndUtil;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SyncAsyncBridgeTest {

  public static class TestErr extends RuntimeException {
    public TestErr(String message) {super(message);}
  }

  public interface Async {
    void method(String a1, String a2, AsyncBack<List<String>> result);
  }

  public static class SyncImpl {

    String name;
    String callThreadName;

    @SneakyThrows @SuppressWarnings("unused")
    public List<String> method(String a1, String a2) {
      callThreadName = Thread.currentThread().getName();
      if ("err".equals(a1)) {
        throw new TestErr("4Muo6ohG2U :: Current thread `" + Thread.currentThread().getName() + "`");
      }
      return List.of(name, a1, a2);
    }
  }

  private static class TestInvoker implements Consumer<Runnable> {

    public final List<Runnable>          runnableList     = Collections.synchronizedList(new ArrayList<>());
    public final AtomicReference<String> acceptThreadName = new AtomicReference<>(null);

    @Override public void accept(Runnable runnable) {
      acceptThreadName.set(Thread.currentThread().getName());
      runnableList.add(runnable);
    }

    public void runAll() {
      runnableList.forEach(Runnable::run);
    }
  }

  @Test
  public void callServiceOnce() throws Exception {
    System.out.println("D67aDy6Z4B :: Test thread `" + Thread.currentThread().getName() + "`");

    SyncImpl sync = new SyncImpl();
    sync.name = "test " + RndUtil.strEng(20);

    TestInvoker callerInvoker = new TestInvoker();//SwingUtilities.invokeLater();
    TestInvoker taskInvoker   = new TestInvoker();

    SyncAsyncBridge syncAsyncBridge = new SyncAsyncBridge(callerInvoker, taskInvoker);

    Async async = syncAsyncBridge.createAsync(Async.class, sync);

    List<List<String>> resultAccept = new ArrayList<>();
    async.method("v1", "v2", resultAccept::add);

    assertThat(resultAccept).isEmpty();

    assertThat(callerInvoker.runnableList).isEmpty();

    Thread thread = new Thread(taskInvoker::runAll);
    thread.setName("test-invoker-" + RndUtil.strEng(10));
    thread.start();
    thread.join();

    assertThat(callerInvoker.runnableList).hasSize(1);
    assertThat(callerInvoker.acceptThreadName.get()).isEqualTo(thread.getName());
    assertThat(resultAccept).isEmpty();

    callerInvoker.runAll();
    assertThat(resultAccept).hasSize(1);
    assertThat(resultAccept.get(0)).isEqualTo(List.of(sync.name, "v1", "v2"));

  }

  @Test
  public void callServiceWithError() throws Exception {
    System.out.println("Lu1vksSoA3 :: Test thread `" + Thread.currentThread().getName() + "`");

    SyncImpl sync = new SyncImpl();
    sync.name = "test";

    TestInvoker                callerInvoker  = new TestInvoker();//SwingUtilities.invokeLater();
    TestInvoker                taskInvoker     = new TestInvoker();
    SyncAsyncBridge            syncAsyncBridge = new SyncAsyncBridge(callerInvoker, taskInvoker);
    Async                      async           = syncAsyncBridge.createAsync(Async.class, sync);
    AtomicReference<Throwable> error           = new AtomicReference<>(null);
    AtomicBoolean              acceptedResult = new AtomicBoolean(false);

    AsyncBack<List<String>> resultAcceptor = new AsyncBack<>() {
      @Override public void accept(List<String> strings) {
        acceptedResult.set(true);
      }

      @Override public void error(@NonNull Throwable err) {
        error.set(err);
      }
    };

    //
    //
    async.method("err", "v2", resultAcceptor);
    //
    //

    assertThat(taskInvoker.runnableList).hasSize(1);
    assertThat(callerInvoker.runnableList).isEmpty();

    Thread thread = new Thread(taskInvoker::runAll);
    thread.setName("test-invoker-" + RndUtil.strEng(10));
    thread.start();
    thread.join();

    assertThat(callerInvoker.runnableList).hasSize(1);
    assertThat(callerInvoker.acceptThreadName.get()).isEqualTo(thread.getName());
    assertThat(acceptedResult).isFalse();
    assertThat(error.get()).isNull();

    callerInvoker.runAll();
    assertThat(acceptedResult).isFalse();
    assertThat(error.get()).isNotNull();
    assertThat(error.get()).isInstanceOf(TestErr.class);

  }
}
