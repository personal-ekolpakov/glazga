package kz.pompei.swing.tree.logic;

import java.util.List;
import java.util.Set;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TreeLogicTest extends TreeLogicTestParent {

  @Test
  public void oneFileInitShow() {
    pageSize = 10;

    file("tst/system/mine.txt");

    printDb();
    printGui();

    assertThat(jn(listDb())).isEqualTo("""
                                         :tst/
                                         :  system/
                                         :    mine.txt
                                         """);
    assertThat(listGui()).isEmpty();


    logic.refresh(null);
    System.out.println("8iO1UwIePB :: Refresh\n");

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  waiting1
                                          """);

    push().assertEq(1);

    assertThat(jn(listGui())).isEqualTo("""
                                          :> tst
                                          """);

    printGui();

    gui.clickOnChevron("tst");

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :V tst
                                          :     waiting2
                                          """);

    push().assertEq(1);

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :V tst
                                          :   > system
                                          """);

    gui.clickOnChevron("tst/system");

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :V tst
                                          :   V system
                                          :        waiting3
                                          """);

    file("tst/system/mine.txt").description("test");

    push().assertEq(1);

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :V tst
                                          :   V system
                                          :        mine.txt (test)
                                          """);

    assertThat(nodeStatusStorage.getNodeIds()).isEqualTo(Set.of("tst", "tst/system"));
  }

  @Test
  public void openAllLevels_01() {
    pageSize = 10;

    file("tst/system/01-file.txt").description("description 1");
    file("tst/system/02-file.txt").description("description 2");
    file("tst/system/03-file.txt").description("description 3");
    file("tst/system/04-file.txt").description("description 4");

    nodeStatusStorage.setNodeIds(Set.of("tst", "tst/system"));

    logic.refresh(null);
    pushAll().assertEq(List.of(1, 1, 1, 0));

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :V tst
                                          :   V system
                                          :        01-file.txt (description 1)
                                          :        02-file.txt (description 2)
                                          :        03-file.txt (description 3)
                                          :        04-file.txt (description 4)
                                          """);

    assertThat(serviceLog()).isEqualTo(List.of("loadNodeChildren: parentId=NULL, offset=0, limit=10, returns-count=1",
                                               "loadNodeChildren: parentId='tst', offset=0, limit=10, returns-count=1",
                                               "loadNodeChildren: parentId='tst/system', offset=0, limit=10, returns-count=4"));
  }

  @Test
  public void openAllLevels_02() {
    pageSize = 10;

    file("root1/child11.txt");
    file("root1/child12.txt");
    file("root2/child21.txt");
    file("root2/child22.txt");
    file("root3/child31.txt");
    file("root3/child32.txt");

    nodeStatusStorage.setNodeIds(Set.of("root1", "root2", "root3"));

    logic.refresh(null);
    pushAll().assertEq(List.of(1, 3, 0));

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :V root1
                                          :     child11.txt
                                          :     child12.txt
                                          :V root2
                                          :     child21.txt
                                          :     child22.txt
                                          :V root3
                                          :     child31.txt
                                          :     child32.txt
                                          """);


    assertThat(serviceLog()).isEqualTo(List.of("loadNodeChildren: parentId=NULL, offset=0, limit=10, returns-count=3",
                                               "loadNodeChildren: parentId='root1', offset=0, limit=10, returns-count=2",
                                               "loadNodeChildren: parentId='root2', offset=0, limit=10, returns-count=2",
                                               "loadNodeChildren: parentId='root3', offset=0, limit=10, returns-count=2"));
  }

  @Test
  public void paging_on_root() {
    pageSize = 3;

    file("root01.txt");
    file("root02.txt");
    file("root03.txt");
    file("root04.txt");
    file("root05.txt");
    file("root06.txt");
    file("root07.txt");

    logic.refresh(null);

    printGui();
    assertThat(jn(listGui())).isEqualTo("""
                                          :  waiting1
                                          """);


    //
    //
    // THE FIRST PAGE
    //
    //

    push().assertEq(1);
    assertThat(serviceLog()).isEqualTo(List.of("loadNodeChildren: parentId=NULL, offset=0, limit=3, returns-count=3"));

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root01.txt
                                          :  root02.txt
                                          :  root03.txt
                                          :  more1
                                          """);

    push().assertEq(0);

    //
    //
    // THE SECOND PAGE
    //
    //

    gui.fireShowNodeTrap("more1");

    assertThat(serviceLog()).isEqualTo(List.of());

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root01.txt
                                          :  root02.txt
                                          :  root03.txt
                                          :  waiting2
                                          """);

    //
    //
    // THE THIRD PAGE
    //
    //

    push().assertEq(1);

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root01.txt
                                          :  root02.txt
                                          :  root03.txt
                                          :  root04.txt
                                          :  root05.txt
                                          :  root06.txt
                                          :  more2
                                          """);

    assertThat(serviceLog()).isEqualTo(List.of("loadNodeChildren: parentId=NULL, offset=3, limit=3, returns-count=3"));

    gui.fireShowNodeTrap("more2");
    push().assertEq(1);

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root01.txt
                                          :  root02.txt
                                          :  root03.txt
                                          :  root04.txt
                                          :  root05.txt
                                          :  root06.txt
                                          :  root07.txt
                                          """);

    assertThat(serviceLog()).isEqualTo(List.of("loadNodeChildren: parentId=NULL, offset=6, limit=3, returns-count=1"));

  }

  @Test
  public void paging_in_branch() {
    pageSize = 3;

    file("root01.txt");
    file("root02/parent/name1.txt");
    file("root02/parent/name2.txt");
    file("root02/parent/name3.txt");
    file("root02/parent/name4.txt");
    file("root02/parent/name5.txt");
    file("root02/parent/name6.txt");
    file("root02/parent/name7.txt");
    file("root03.txt");

    nodeStatusStorage.setNodeIds(Set.of("root02", "root02/parent"));
    logic.refresh(null);
    pushAll().assertEq(List.of(1, 1, 1, 0));

    printGui();
    assertThat(jn(listGui())).isEqualTo("""
                                          :  root01.txt
                                          :V root02
                                          :   V parent
                                          :        name1.txt
                                          :        name2.txt
                                          :        name3.txt
                                          :        more1
                                          :  root03.txt
                                          """);


    assertThat(serviceLog()).isEqualTo(List.of("loadNodeChildren: parentId=NULL, offset=0, limit=3, returns-count=3",
                                               "loadNodeChildren: parentId='root02', offset=0, limit=3, returns-count=1",
                                               "loadNodeChildren: parentId='root02/parent', offset=0, limit=3, returns-count=3"));

    //
    //
    // THE SECOND PAGE
    //
    //

    gui.fireShowNodeTrap("more1");

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root01.txt
                                          :V root02
                                          :   V parent
                                          :        name1.txt
                                          :        name2.txt
                                          :        name3.txt
                                          :        waiting4
                                          :  root03.txt
                                          """);

    pushAll().assertEq(List.of(1, 0));

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root01.txt
                                          :V root02
                                          :   V parent
                                          :        name1.txt
                                          :        name2.txt
                                          :        name3.txt
                                          :        name4.txt
                                          :        name5.txt
                                          :        name6.txt
                                          :        more2
                                          :  root03.txt
                                          """);

    assertThat(serviceLog()).isEqualTo(List.of("loadNodeChildren: parentId='root02/parent', offset=3, limit=3, returns-count=3"));

    //
    //
    // THE THIRD PAGE
    //
    //

    gui.fireShowNodeTrap("more2");
    pushAll().assertEq(List.of(1, 0));

    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root01.txt
                                          :V root02
                                          :   V parent
                                          :        name1.txt
                                          :        name2.txt
                                          :        name3.txt
                                          :        name4.txt
                                          :        name5.txt
                                          :        name6.txt
                                          :        name7.txt
                                          :  root03.txt
                                          """);

    assertThat(serviceLog()).isEqualTo(List.of("loadNodeChildren: parentId='root02/parent', offset=6, limit=3, returns-count=1"));

  }

  @Test
  public void refreshBranches() {
    pageSize = 30;

    file("root1.txt");
    file("root3/stone1.txt").description("color 1");
    file("root3/stone2.txt").description("color 2");
    file("root3/stone3.txt").description("color 3");
    file("root3/stone4.txt").description("color 4");
    file("root3/stone5.txt").description("color 5");
    dir("root3/stars").description("z in Galactic");
    file("root3/stars/star1.txt").description("Planets 7");
    file("root3/stars/star2.txt").description("Planets 7");
    file("root3/stars/star3.txt").description("Planets 7");
    file("root3/stars/star4.txt").description("Planets 7");
    dir("root2/countries").description("Planets 7");


    nodeStatusStorage.setNodeIds(Set.of("root3", "root3/stars"));
    logic.refresh(null);
    pushAll();
    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt
                                          :> root2
                                          :V root3
                                          :     stone1.txt (color 1)
                                          :     stone2.txt (color 2)
                                          :     stone3.txt (color 3)
                                          :     stone4.txt (color 4)
                                          :     stone5.txt (color 5)
                                          :   V stars (z in Galactic)
                                          :        star1.txt (Planets 7)
                                          :        star2.txt (Planets 7)
                                          :        star3.txt (Planets 7)
                                          :        star4.txt (Planets 7)
                                          """);

    file("root3/stars/star1.txt").description("Planets 8");
    file("root3/stars/star5.txt").description("Planets 8");
    remove("root3/stars/star4.txt");
    remove("root3/stone3.txt");
    remove("root3/stone5.txt");
    file("root3/stone4.txt").description("color 0");
    printDb();

    //
    //
    logic.refresh("root3");
    //
    //

    pushAll();
    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt
                                          :> root2
                                          :V root3
                                          :     stone4.txt (color 0)
                                          :     stone1.txt (color 1)
                                          :     stone2.txt (color 2)
                                          :   V stars (z in Galactic)
                                          :        star2.txt (Planets 7)
                                          :        star3.txt (Planets 7)
                                          :        star1.txt (Planets 8)
                                          :        star5.txt (Planets 8)
                                          """);
  }

  @Test
  public void refreshBranch1() {
    pageSize = 30;

    file("root1/stone1.txt").description("color 1");
    file("root1/stone2.txt").description("color 2");
    file("root1/stone3.txt").description("color 3");
    file("root1/stone4.txt").description("color 4");
    file("root1/stone5.txt").description("color 5");


    nodeStatusStorage.setNodeIds(Set.of("root1"));
    logic.refresh(null);
    pushAll();
    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :V root1
                                          :     stone1.txt (color 1)
                                          :     stone2.txt (color 2)
                                          :     stone3.txt (color 3)
                                          :     stone4.txt (color 4)
                                          :     stone5.txt (color 5)
                                          """);

    remove("root1/stone2.txt");
    dir("root1").description("cool");
    file("root1/stone4.txt").description("color 0");
    file("root1/stone6.txt").description("color 6");

    printDb();

    //
    //
    logic.refresh("root1");
    //
    //

    pushAll();
    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :V root1 (cool)
                                          :     stone4.txt (color 0)
                                          :     stone1.txt (color 1)
                                          :     stone3.txt (color 3)
                                          :     stone5.txt (color 5)
                                          :     stone6.txt (color 6)
                                          """);
  }

  @Test
  public void refreshBranch2() {
    pageSize = 30;

    file("root1/stone1.txt").description("color 1");
    dir("root1/stone2").description("color 2");
    file("root1/stone2/status1").description("look 1");
    file("root1/stone2/status2").description("look 2");
    file("root1/stone2/status3").description("look 3");
    file("root1/stone3.txt").description("color 3");

    nodeStatusStorage.setNodeIds(Set.of("root1", "root1/stone2"));
    logic.refresh(null);
    pushAll();
    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :V root1
                                          :     stone1.txt (color 1)
                                          :   V stone2 (color 2)
                                          :        status1 (look 1)
                                          :        status2 (look 2)
                                          :        status3 (look 3)
                                          :     stone3.txt (color 3)
                                          """);


    file("root1/stone2/status1").description("look 30");
    file("root1/stone2/status2").description("look 20");
    file("root1/stone2/status3").description("look 10");
    printDb();

    //
    //
    logic.refresh("root1");
    //
    //

    pushAll();
    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :V root1
                                          :     stone1.txt (color 1)
                                          :   V stone2 (color 2)
                                          :        status1 (look 1)
                                          :        status2 (look 2)
                                          :        status3 (look 3)
                                          :     stone3.txt (color 3)
                                          """);
  }

  @Test
  public void refreshRoots1() {
    pageSize = 30;

    file("root1.txt").description("color 1");
    file("root2.txt").description("color 2");
    file("root3.txt").description("color 3");
    file("root4.txt").description("color 4");

    logic.refresh(null);
    pushAll();
    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt (color 1)
                                          :  root2.txt (color 2)
                                          :  root3.txt (color 3)
                                          :  root4.txt (color 4)
                                          """);

    file("root5.txt").description("color 5");
    file("root4.txt").description("color 0");
    remove("root1.txt");

    logic.refresh(null);
    pushAll();
    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root4.txt (color 0)
                                          :  root2.txt (color 2)
                                          :  root3.txt (color 3)
                                          :  root5.txt (color 5)
                                          """);
  }

  @Test
  public void refreshRoots2() {
    pageSize = 30;

    file("root1.txt").description("color 1");
    file("root3.txt").description("color 3");
    file("root4.txt").description("color 4");

    logic.refresh(null);
    pushAll();
    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt (color 1)
                                          :  root3.txt (color 3)
                                          :  root4.txt (color 4)
                                          """);

    file("root2/double1.txt").description("sun 1");
    dir("root2").description("color 2");
    file("root2/double2.txt").description("sun 2");
    file("root2/double3.txt").description("sun 3");

    nodeStatusStorage.setNodeIds(Set.of("root2"));

    logic.refresh(null);
    pushAll();
    printGui();

    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt (color 1)
                                          :V root2 (color 2)
                                          :     double1.txt (sun 1)
                                          :     double2.txt (sun 2)
                                          :     double3.txt (sun 3)
                                          :  root3.txt (color 3)
                                          :  root4.txt (color 4)
                                          """);
  }

  @Test
  public void refreshWithPaging1() {
    pageSize = 30;

    file("root1.txt").description("color 1");
    file("root2.txt").description("color 2");
    file("root3.txt").description("color 3");
    file("root4.txt").description("color 4");
    file("root5.txt").description("color 5");
    file("root6.txt").description("color 6");
    file("root7.txt").description("color 7");

    logic.refresh(null);
    pushAll();
    printGui();
    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt (color 1)
                                          :  root2.txt (color 2)
                                          :  root3.txt (color 3)
                                          :  root4.txt (color 4)
                                          :  root5.txt (color 5)
                                          :  root6.txt (color 6)
                                          :  root7.txt (color 7)
                                          """);

    pageSize = 3;

    file("root2.txt").description("color 9");
    logic.refresh(null);
    pushAll();
    printGui();
    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt (color 1)
                                          :  root3.txt (color 3)
                                          :  root4.txt (color 4)
                                          :  root5.txt (color 5)
                                          :  root6.txt (color 6)
                                          :  root7.txt (color 7)
                                          :  root2.txt (color 9)
                                          """);
  }

  @Test
  public void refreshWithPaging2() {
    pageSize = 3;

    file("root1.txt").description("color 1");
    file("root2.txt").description("color 2");
    file("root3.txt").description("color 3");
    file("root4.txt").description("color 3");
    file("root5.txt").description("color 3");
    file("root6.txt").description("color 3");
    file("root7.txt").description("color 3");
    file("root8.txt").description("color 3");
    file("root9.txt").description("color 3");

    logic.refresh(null);
    pushAll();
    printGui();
    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt (color 1)
                                          :  root2.txt (color 2)
                                          :  root3.txt (color 3)
                                          :  more1
                                          """);

    pageSize = 3;

    file("root2.txt").description("color 9");
    logic.refresh(null);
    pushAll();
    printGui();
    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt (color 1)
                                          :  root3.txt (color 3)
                                          :  root4.txt (color 3)
                                          :  root5.txt (color 3)
                                          :  more2
                                          """);

    pageSize = 2;

    gui.fireShowNodeTrap("more2");
    pushAll();
    printGui();
    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt (color 1)
                                          :  root3.txt (color 3)
                                          :  root4.txt (color 3)
                                          :  root5.txt (color 3)
                                          :  root6.txt (color 3)
                                          :  root7.txt (color 3)
                                          :  more3
                                          """);

    pageSize = 3;

    gui.fireShowNodeTrap("more3");
    pushAll();
    printGui();
    assertThat(jn(listGui())).isEqualTo("""
                                          :  root1.txt (color 1)
                                          :  root3.txt (color 3)
                                          :  root4.txt (color 3)
                                          :  root5.txt (color 3)
                                          :  root6.txt (color 3)
                                          :  root7.txt (color 3)
                                          :  root8.txt (color 3)
                                          :  root9.txt (color 3)
                                          :  root2.txt (color 9)
                                          """);
  }

}
