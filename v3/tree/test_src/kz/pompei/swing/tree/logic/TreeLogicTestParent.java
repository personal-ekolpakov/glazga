package kz.pompei.swing.tree.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.utils.ChildrenIterator;
import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.utils.events.Handler;
import kz.pompei.glazga.utils.events.HandlerObserver;
import kz.pompei.swing.tree.gui.TreeGui;
import kz.pompei.swing.tree.gui.TreeNodeGui;
import kz.pompei.swing.tree.gui.TreeNodeProvider;
import kz.pompei.swing.tree.logic.model.TreeNodesPortion;
import kz.pompei.swing.tree.styles.NodeStatus;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.testng.annotations.BeforeMethod;

import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;

public abstract class TreeLogicTestParent {

  final List<DbItem> roots = new ArrayList<>();

  @RequiredArgsConstructor
  protected static class GuiNode implements TreeNodeGui<GuiNode> {
    final @NonNull String id;

    String title;
    String nodeType;

    @Override public @NonNull String id() {
      return id;
    }

    int level;

    private NodeStatus status;

    @Override public NodeStatus status() {
      return status;
    }

    @Override public void setStatus(NodeStatus status) {
      this.status = status;
    }

    final List<GuiNode> children = new ArrayList<>();

    @Override public @NonNull List<GuiNode> children() {
      return children;
    }

    public String line() {
      StringBuilder sb = new StringBuilder();
      sb.append("   ".repeat(level));

      sb.append(status == null ? "? " : switch (status) {
        case LEAF -> "  ";
        case OPEN -> "V ";
        case CLOSE -> "> ";
      });
      sb.append(title);
      if (nodeType != null) {
        sb.append(" (" + nodeType + ")");
      }
      return sb.toString();
    }

    @Override public String toString() {
      return line();
    }
  }

  protected NodeStatusStorageMem nodeStatusStorage;

  @RequiredArgsConstructor
  protected static class DbItem implements TreeItem, Comparable<DbItem> {
    private final @NonNull String       id;
    private final @NonNull String       name;
    private final          boolean      isFolder;
    private final          List<DbItem> children = new ArrayList<>();
    public                 String       description;

    @Override public int compareTo(@NonNull DbItem o) {
      String d1 = this.description;
      String d2 = o.description;

      d1 = d1 == null ? "" : d1;
      d2 = d2 == null ? "" : d2;

      int cmp = d1.compareTo(d2);
      if (cmp != 0) {
        return cmp;
      }

      return this.name.compareTo(o.name);
    }

    @Override public String toString() {
      return name + (isFolder ? "/" : "") + (description == null ? "" : " (" + description + ")");
    }

    @Override public String id() {
      return id;
    }

    @Override public boolean isFolder() {
      return isFolder;
    }

    @SuppressWarnings({"UnusedReturnValue", "SameParameterValue"})
    DbItem description(String description) {
      this.description = description;
      return this;
    }
  }

  protected @NonNull DbItem dir(String path) {
    if (path.startsWith("/")) {
      throw new IllegalArgumentException("8GbBC8wcr7 :: path cannot start with /");
    }
    return getOrCreateDir(0, path.split("/"), roots);
  }

  private @NonNull DbItem getOrCreateDir(int idx, String[] splitPath, @NonNull List<DbItem> children) {
    String id   = Arrays.stream(splitPath).limit(idx + 1).collect(joining("/"));
    DbItem node = null;
    for (final DbItem child : children) {
      if (id.equals(child.id)) {
        node = child;
        break;
      }
    }
    if (node == null) {
      node             = new DbItem(id, splitPath[idx], true);
      node.description = null;
      children.add(node);
    }

    if (splitPath.length == idx + 1) {
      return node;
    }

    return getOrCreateDir(idx + 1, splitPath, node.children);
  }

  protected void remove(@NonNull String nodeId) {
    remove0(roots, nodeId);
  }

  private boolean remove0(List<DbItem> dbItemList, String nodeId) {
    for (int i = 0; i < dbItemList.size(); i++) {
      DbItem item = dbItemList.get(i);
      if (item.id.equals(nodeId)) {
        dbItemList.remove(i);
        return true;
      }
      if (remove0(item.children, nodeId)) {
        return true;
      }
    }
    return false;
  }

  protected DbItem file(String path) {
    if (path.startsWith("/")) {
      throw new IllegalArgumentException("8GbBC8wcr7 :: path cannot start with /");
    }

    String[] split = path.split("/");

    List<DbItem> children;

    if (split.length == 1) {
      children = roots;
    } else {
      String[] splitDir = new String[split.length - 1];
      System.arraycopy(split, 0, splitDir, 0, splitDir.length);
      children = getOrCreateDir(0, splitDir, roots).children;
    }

    DbItem file = null;

    for (final DbItem child : children) {
      if (path.equals(child.id)) {
        file = child;
        if (file.isFolder) {
          throw new RuntimeException("jAi9uuJwtB :: It is folder: " + path);
        }
        break;
      }
    }

    if (file == null) {

      file             = new DbItem(path, split[split.length - 1], false);
      file.description = null;
      children.add(file);

    }

    return file;
  }

  protected static class Gui implements TreeGui<GuiNode> {
    private final List<GuiNode> roots = new ArrayList<>();

    @Override public @NonNull Iterator<GuiNode> nodeIterator() {
      return new ChildrenIterator<>(roots, x -> x.children, (t, level) -> t.level = level);
    }

    public final HandlerObserver<GuiNode> chevronMousePressed = new HandlerObserver<>();

    @Override public HandlerObserver<GuiNode> chevronMousePressed() {
      return chevronMousePressed;
    }

    public int repaintCount = 0;

    @Override public void repaint() {
      repaintCount++;
    }

    private final Map<String, List<Handler<String>>> showNodeTraps = new HashMap<>();

    @Override public void installTrap_OnShowNode(@NonNull String nodeId, @NonNull Handler<String> handler) {
      showNodeTraps.computeIfAbsent(nodeId, k -> new ArrayList<>()).add(handler);
    }

    @SneakyThrows public void fireShowNodeTrap(@NonNull String nodeId) {
      List<Handler<String>> handlerList = showNodeTraps.remove(nodeId);
      if (handlerList == null) {
        throw new RuntimeException("msMf2jafBE :: No trap for nodeId = " + nodeId);
      }

      for (final Handler<String> handler : handlerList) {
        handler.handle(nodeId);
      }
    }

    @Override public @NonNull List<GuiNode> roots() {
      return roots;
    }

    void clickOnChevron(@NonNull String nodeId) {
      Iterator<GuiNode> i = nodeIterator();
      while (i.hasNext()) {
        GuiNode guiNode = i.next();
        if (guiNode.id.equals(nodeId)) {
          chevronMousePressed.fire(guiNode);
          System.out.println("GgjX9IkQyD :: Clicked on chevron `" + nodeId + "`\n");
          return;
        }
      }
      throw new RuntimeException("CORSrAue8D :: Cannot find node with id = `" + nodeId + "`");
    }
  }

  private int nextMoreIndex;
  private int nextWaitingIndex;

  TreeNodeProvider<GuiNode, DbItem> treeNodeProvider = new TreeNodeProvider<>() {

    @Override public @NonNull GuiNode newTreeNode(@NonNull String nodeId) {
      return new GuiNode(nodeId);
    }

    @Override public @NonNull GuiNode newWaitingNode(String parentId, int offset, int pageSize) {
      String id  = "waiting" + nextWaitingIndex++;
      var    ret = new GuiNode(id);
      ret.nodeType = null;
      ret.title    = id;
      return ret;
    }

    @Override public @NonNull GuiNode newMoreNode() {
      String id  = "more" + nextMoreIndex++;
      var    ret = new GuiNode(id);
      ret.nodeType = null;
      ret.title    = id;
      return ret;
    }

    @Override public void assign(@NonNull GuiNode target, @NonNull DbItem source) {
      target.title = source.name + (source.description == null ? "" : " (" + source.description + ")");
    }
  };

  Gui gui;

  protected DbItem findDbNodeById(@NonNull List<DbItem> nodes, String nodeId) {
    for (final DbItem node : nodes) {
      if (node.id.equals(nodeId)) {
        return node;
      }

      {
        DbItem x = findDbNodeById(node.children, nodeId);
        if (x != null) {
          return x;
        }
      }
    }
    return null;
  }

  private final List<Runnable> tasks = new ArrayList<>();

  @RequiredArgsConstructor
  protected static class PushAllReturn {
    public final List<Integer> ret;

    @SuppressWarnings("UnusedReturnValue")
    public PushAllReturn assertEq(List<Integer> expected) {
      assertThat(ret).isEqualTo(expected);
      return this;
    }

  }

  protected PushAllReturn pushAll() {
    List<Integer> ret = new ArrayList<>();
    while (true) {
      int count = push0();
      ret.add(count);
      if (count == 0) {
        System.out.println("GNFnFAQgRI :: PUSHED ALL Tasks Count = " + ret);
        System.out.println();
        return new PushAllReturn(List.copyOf(ret));
      }
    }
  }

  @RequiredArgsConstructor
  protected static class PushReturn {
    public final int pushedCount;

    @SuppressWarnings("UnusedReturnValue")
    public PushReturn assertEq(int expectedCount) {
      assertThat(pushedCount).isEqualTo(expectedCount);
      return this;
    }
  }

  protected PushReturn push() {
    int ret = push0();
    System.out.println("uryOJTje5i :: PUSHED tasks count = " + ret);
    System.out.println();
    return new PushReturn(ret);
  }

  private int push0() {
    List<Runnable> runList = List.copyOf(tasks);
    tasks.clear();
    for (final Runnable task : runList) {
      task.run();
    }

    return runList.size();
  }

  private final List<String> serviceLog = new ArrayList<>();

  protected List<String> serviceLog() {
    List<String> ret = List.copyOf(serviceLog);
    serviceLog.clear();
    return ret;
  }

  final TreeLoadService<DbItem> service = new TreeLoadService<>() {

    @Override public void loadNodeChildren(String parentId,
                                           Page page,
                                           AsyncBack<TreeNodesPortion<DbItem>> result) {

      tasks.add(() -> {

        List<DbItem> nodes = roots;
        if (parentId != null) {

          DbItem foundNode = findDbNodeById(roots, parentId);
          if (foundNode == null) {
            throw new RuntimeException("REaAVoJ7Dn :: No db node with id = " + parentId);
          }

          nodes = foundNode.children;

        }

        nodes = new ArrayList<>(nodes);

        Collections.sort(nodes);

        TreeNodesPortion<DbItem> ret = new TreeNodesPortion<>();

        ret.list    = nodes;
        ret.hasMore = false;

        if (page != null) {
          ret.list    = nodes.stream().skip(page.offset).limit(page.limit).toList();
          ret.hasMore = page.offset + page.limit < nodes.size();
        }

        ret.totalCount = nodes.size();
        ret.page       = page;

        try {

          serviceLog.add("loadNodeChildren: "
                         + "parentId=" + (parentId == null ? "NULL" : "'" + parentId + "'")
                         + (page == null ? "" : ", offset=" + page.offset + ", limit=" + page.limit)
                         + ", returns-count=" + ret.list.size());

          result.accept(ret);
        } catch (Throwable e) {
          throw e instanceof RuntimeException r ? r : new RuntimeException(e);
        }
      });

    }

    @Override public void loadNode(@NonNull String nodeId, AsyncBack<DbItem> treeNodeBack) {
      tasks.add(() -> {

        DbItem foundNode = findDbNodeById(roots, nodeId);

        try {
          serviceLog.add("loadNode: nodeId='" + nodeId + "', returns=" + (foundNode == null ? "NULL" : "ok"));
          treeNodeBack.accept(foundNode);
        } catch (Throwable e) {
          throw e instanceof RuntimeException r ? r : new RuntimeException(e);
        }

      });
    }
  };

  TreeLogic<GuiNode, DbItem> logic;

  int pageSize;


  protected List<String> listGui() {
    List<String>      ret      = new ArrayList<>();
    Iterator<GuiNode> iterator = gui.nodeIterator();
    while (iterator.hasNext()) {
      ret.add(iterator.next().line());
    }
    return ret;
  }

  protected void printGui() {
    List<String> lst = listGui();
    for (final String str : lst) {
      System.out.println("yh1iSpBv9v :: - " + str);
    }
    if (lst.isEmpty()) {
      System.out.println("yh1iSpBv9v :: -------");
    }
    System.out.println();
  }

  protected List<String> listDb() {
    List<String> lines = new ArrayList<>();
    appendListDb(lines, 0, roots);
    return lines;
  }

  private void appendListDb(List<String> lines, int tab, List<DbItem> children) {

    ArrayList<DbItem> list = new ArrayList<>(children);
    Collections.sort(list);

    for (final DbItem item : list) {
      lines.add("  ".repeat(tab) + item);
      appendListDb(lines, tab + 1, item.children);
    }
  }

  protected static String jn(List<String> lines) {
    return lines.stream().map(s -> ":" + s).collect(joining("\n")) + "\n";
  }

  protected void printDb() {
    List<String> list = listDb();
    for (final String line : list) {
      System.out.println("3mRfnDMJa1 :: = " + line);
    }

    if (list.isEmpty()) {
      System.out.println("3mRfnDMJa1 :: =======");
    }
    System.out.println();
  }

  @BeforeMethod
  public void prepare() {
    nextMoreIndex    = 1;
    nextWaitingIndex = 1;
    serviceLog.clear();
    roots.clear();
    gui               = new Gui();
    nodeStatusStorage = new NodeStatusStorageMem();
    logic             = new TreeLogic<>(gui, nodeStatusStorage, service, treeNodeProvider);
    pageSize          = 10;
    logic.pageSize    = () -> pageSize;
  }

}
