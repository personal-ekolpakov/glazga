package kz.pompei.swing.tree;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.swing.icons.Svg;
import kz.pompei.swing.tree.gui.SwingTree;
import kz.pompei.swing.tree.gui.model.SwingTreeNode;
import kz.pompei.swing.tree.styles.NodeStatus;
import kz.pompei.swing.tree.styles.SwingTreeStyle;
import kz.pompei.swing.tree.styles.defau.SwingTreeStyleDefault;

import static kz.pompei.glazga.utils.GuiUtil.bindKeyAction;

public class SwingTreeTestForm {
  private static void printNodes(int tab, List<SwingTreeNode> list) {
    for (final SwingTreeNode node : list) {
      System.out.println("OLDqXjkFjQ :: " + ".  ".repeat(tab) + node.cells.stream().findAny().map(x -> x.text).orElse(""));
      printNodes(tab + 1, node.children);
    }
  }

  public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> {
      SwingTreeTestForm form = new SwingTreeTestForm();
      form.execute();
    });
  }

  private SwingTree tree;

  private void execute() {
    JFrame frame = new JFrame("Пример дерева");

    Path build = Paths.get("build");

    SizeLocationSaver.into(build.resolve("sizes_and_locations"))
                     .overJFrame("SwingTreeTest", frame, true);

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(400, 300);

//      frame.setIconImage(Svg.images.get("svg/enableNewUi.svg", 128, 128));
    frame.setIconImage(Svg.images.get("svg/cassandra.svg", 128, 128));

    SwingTreeStyle swingTreeStyle = new SwingTreeStyleDefault();

    tree = new SwingTree(swingTreeStyle);
    frame.getContentPane().add(tree);
    frame.pack();

    frame.setVisible(true);

    bindKeyAction(tree, KeyStroke.getKeyStroke("SPACE"), this::actionOnSpaceKey);

    //@formatter:off
    SwingTreeNode root1 = tree.newRoot("r1").cell("m", "root1").cell("d", "2024-10-03", "date").cell("ds", "Zero Root", "description");
    SwingTreeNode root2 = tree.newRoot("r2").cell("m", "root2").cell("d", "2024-01-01", "date").cell("ds", "The First Root", "description");

    SwingTreeNode c11 = root1.newChild("c11").cell("m", "child 11").cell("d", "2023-07-11", "date").cell("ds", "Child 1 1", "description");
    SwingTreeNode c12 = root1.newChild("c12").cell("m", "child 12").cell("d", "2023-07-17", "date").cell("ds", "Child 1 2", "description");
    SwingTreeNode c21 = root2.newChild("c21").cell("m", "child 21").cell("d", "2023-07-11", "date").cell("ds", "Child 2 1", "description");
    SwingTreeNode c22 = root2.newChild("c22").cell("m", "child 22").cell("d", "2023-07-17", "date").cell("ds", "Child 2 2", "description");
    //@formatter:on

    c11.newChild("c111").cell("m", "child 111");
    c11.newChild("c112", "cassandra").cell("m", "child 112");
    c11.newChild("c113").cell("m", "child 113");
    c12.newChild("c121").cell("m", "child 121");
    c12.newChild("c122").cell("m", "child 122");
    c12.newChild("c123").cell("m", "child 123");
    c21.newChild("c211").cell("m", "child 211");
    c21.newChild("c212").cell("m", "child 212");
    c21.newChild("c213").cell("m", "child 213");
    c22.newChild("c221").cell("m", "child 221");
    c22.newChild("c222").cell("m", "child 222");
    c22.newChild("c223").cell("m", "child 223");

    c12.nodeType = "ant";

    tree.selection().setActiveNodeId("c11");

    {
      System.out.println();
      System.out.println();
      printNodes(0, tree.roots);
      System.out.println();
      System.out.println();
    }

    Iterator<SwingTreeNode> iter = tree.nodeIterator();
    while (iter.hasNext()) {
      SwingTreeNode node = iter.next();
      if (node.children.size() > 0) {
        node.status = NodeStatus.OPEN;
      } else if (node.cells.get(0).text.endsWith("2")) {
        node.status = NodeStatus.CLOSE;
      } else {
        node.status = NodeStatus.LEAF;
      }
    }
  }

  private void actionOnSpaceKey() {
    System.out.println("5r2J5LIGTg :: selection = " + tree.selection());
  }
}
