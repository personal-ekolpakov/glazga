package kz.pompei.swing.tree.gui;

import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import kz.pompei.v3.storage.StoragePath;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.testng.annotations.BeforeMethod;

public abstract class SwingTreeSelectionStorageBridgeTestParent {

  protected Path                            moduleRoot;
  protected Path                            root;
  protected SwingTreeSelectionStorageBridge storage;

  @BeforeMethod
  public void prepareTestingObject(Method testMethod) {
    if (moduleRoot == null) {
      var sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
      moduleRoot = Paths.get("build").resolve(getClass().getSimpleName()).resolve(sdf.format(new Date()));
    }
    root    = moduleRoot.resolve(testMethod.getName());
    storage = new SwingTreeSelectionStorageBridge(new StoragePath(root));
  }

  @SneakyThrows
  protected String loadFile(@NonNull String fileName) {
    Path file = root.resolve(fileName);
    return !Files.exists(file) ? null : Files.readString(file, StandardCharsets.UTF_8);
  }

  @SneakyThrows
  protected void saveFile(@NonNull String fileName, String content) {
    Path file = root.resolve(fileName);

    if (content == null) {
      if (file.toFile().exists()) {
        file.toFile().delete();
      }
      return;
    }

    file.toFile().getParentFile().mkdirs();
    Files.writeString(file, content, StandardCharsets.UTF_8);
  }
}
