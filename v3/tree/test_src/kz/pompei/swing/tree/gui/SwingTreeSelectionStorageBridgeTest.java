package kz.pompei.swing.tree.gui;

import java.util.List;
import kz.pompei.glazga.utils.RndUtil;
import org.testng.annotations.Test;

import static kz.pompei.swing.tree.gui.SwingTreeSelectionStorageBridge.ActiveCellId;
import static kz.pompei.swing.tree.gui.SwingTreeSelectionStorageBridge.ActiveNodeId;
import static kz.pompei.swing.tree.gui.SwingTreeSelectionStorageBridge.Cell;
import static kz.pompei.swing.tree.gui.SwingTreeSelectionStorageBridge.Order;
import static kz.pompei.swing.tree.gui.SwingTreeSelectionStorageBridge.SelectedNodes;
import static kz.pompei.v3.storage.StoragePath.TXT;
import static kz.pompei.v3.storage.StorageUtil.encodeId;
import static org.assertj.core.api.Assertions.assertThat;

public class SwingTreeSelectionStorageBridgeTest extends SwingTreeSelectionStorageBridgeTestParent {

  @Test
  public void setActiveNodeId() {

    String nodeId = RndUtil.newId();

    //
    //
    storage.setActiveNodeId(nodeId);
    //
    //

    assertThat(loadFile(ActiveNodeId + TXT)).isEqualTo(nodeId);
  }

  @Test
  public void activeNodeId() {

    String nodeId = RndUtil.newId();

    saveFile(ActiveNodeId + TXT, nodeId);

    //
    //
    String actualNodeId = storage.activeNodeId();
    //
    //

    assertThat(actualNodeId).isEqualTo(nodeId);
  }

  @Test
  public void setActiveCellId() {

    String cellId = RndUtil.newId();

    //
    //
    storage.setActiveCellId(cellId);
    //
    //

    assertThat(loadFile(ActiveCellId + TXT)).isEqualTo(cellId);
  }

  @Test
  public void activeCellId() {

    String cellId = RndUtil.newId();

    saveFile(ActiveCellId + TXT, cellId);

    //
    //
    String actualCellId = storage.activeCellId();
    //
    //

    assertThat(actualCellId).isEqualTo(cellId);
  }

  @Test
  public void addSelectedNodeId() {

    String node1 = RndUtil.newId();
    String node2 = RndUtil.newId();
    String node3 = RndUtil.newId();

    //
    //
    //
    storage.addSelectedNodeId(node1);
    storage.addSelectedNodeId(node2);
    storage.addSelectedNodeId(node3);
    //
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node2 + '.' + Order + TXT)).isEqualTo("2.0");
    assertThat(loadFile(SelectedNodes + '.' + node3 + '.' + Order + TXT)).isEqualTo("3.0");

  }

  @Test
  public void deleteSelectedNodeId() {

    String node1 = RndUtil.newId();

    saveFile(SelectedNodes + '.' + node1 + '.' + Order + TXT, "1.0");
    saveFile(SelectedNodes + '.' + node1 + ".tst1" + TXT, "KDRicSYZva");
    saveFile(SelectedNodes + '.' + node1 + ".tst2" + TXT, "EkEm2Jv488");

    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Order + TXT)).isNotNull();
    assertThat(loadFile(SelectedNodes + '.' + node1 + ".tst1" + TXT)).isNotNull();
    assertThat(loadFile(SelectedNodes + '.' + node1 + ".tst2" + TXT)).isNotNull();

    //
    //
    storage.deleteSelectedNodeId(node1);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Order + TXT)).isNull();
    assertThat(loadFile(SelectedNodes + '.' + node1 + ".tst1" + TXT)).isNull();
    assertThat(loadFile(SelectedNodes + '.' + node1 + ".tst2" + TXT)).isNull();

  }

  @Test
  public void getSelectedNodeIds() {

    String node1 = RndUtil.newId();
    String node2 = RndUtil.newId();
    String node3 = RndUtil.newId();

    saveFile(SelectedNodes + '.' + node1 + '.' + Order + TXT, "1.0");
    saveFile(SelectedNodes + '.' + node1 + ".tst1" + TXT, "1");
    saveFile(SelectedNodes + '.' + node1 + ".tst2" + TXT, "1");

    saveFile(SelectedNodes + '.' + node2 + '.' + Order + TXT, "3.0");
    saveFile(SelectedNodes + '.' + node2 + ".tst1" + TXT, "1");
    saveFile(SelectedNodes + '.' + node2 + ".tst2" + TXT, "1");

    saveFile(SelectedNodes + '.' + node3 + '.' + Order + TXT, "2.0");
    saveFile(SelectedNodes + '.' + node3 + ".tst1" + TXT, "1");
    saveFile(SelectedNodes + '.' + node3 + ".tst2" + TXT, "1");

    //
    //
    List<String> actualList = storage.getSelectedNodeIds();
    //
    //

    assertThat(actualList).isEqualTo(List.of(node1, node3, node2));

  }

  @Test
  public void addSelectedCellId() {

    String node1 = RndUtil.newId();
    String node2 = RndUtil.newId();

    String cell11 = RndUtil.newId();
    String cell12 = RndUtil.newId();
    String cell21 = RndUtil.newId();
    String cell22 = RndUtil.newId();

    //
    //
    storage.addSelectedCellId(node1, cell11);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Cell + '.' + cell11 + '.' + Order + TXT)).isEqualTo("1.0");

    //
    //
    storage.addSelectedCellId(node1, cell12);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Cell + '.' + cell11 + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Cell + '.' + cell12 + '.' + Order + TXT)).isEqualTo("2.0");

    //
    //
    storage.addSelectedCellId(node2, cell21);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Cell + '.' + cell11 + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Cell + '.' + cell12 + '.' + Order + TXT)).isEqualTo("2.0");

    assertThat(loadFile(SelectedNodes + '.' + node2 + '.' + Order + TXT)).isEqualTo("2.0");
    assertThat(loadFile(SelectedNodes + '.' + node2 + '.' + Cell + '.' + cell21 + '.' + Order + TXT)).isEqualTo("1.0");

    //
    //
    storage.addSelectedCellId(node2, cell22);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node1 + '.' + Order + TXT)).isEqualTo("1.0");

    assertThat(loadFile(SelectedNodes + '.' + node2 + '.' + Order + TXT)).isEqualTo("2.0");
    assertThat(loadFile(SelectedNodes + '.' + node2 + '.' + Cell + '.' + cell21 + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node2 + '.' + Cell + '.' + cell22 + '.' + Order + TXT)).isEqualTo("2.0");
  }

  @Test
  public void deleteSelectedCellId() {
    String node = RndUtil.newId();
    String cell = RndUtil.newId();

    saveFile(SelectedNodes + '.' + node + '.' + Cell + '.' + cell + '.' + Order + TXT, "1.0");

    assertThat(loadFile(SelectedNodes + '.' + node + '.' + Cell + '.' + cell + '.' + Order + TXT)).isNotNull();

    //
    //
    storage.deleteSelectedCellId(node, cell);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node + '.' + Cell + '.' + cell + '.' + Order + TXT)).isNull();
  }

  @Test
  public void getSelectedCellIds() {

    String node1 = RndUtil.newId();
    String node2 = RndUtil.newId();

    String cell11 = RndUtil.newId();
    String cell12 = RndUtil.newId();
    String cell21 = RndUtil.newId();
    String cell22 = RndUtil.newId();
    String cell23 = RndUtil.newId();
    String cell24 = RndUtil.newId();
    String cell25 = RndUtil.newId();

    saveFile(SelectedNodes + '.' + node1 + '.' + Cell + '.' + cell11 + '.' + Order + TXT, "1.0");
    saveFile(SelectedNodes + '.' + node1 + '.' + Cell + '.' + cell12 + '.' + Order + TXT, "1.0");

    saveFile(SelectedNodes + '.' + node2 + '.' + Cell + '.' + cell24 + '.' + Order + TXT, "10.0");
    saveFile(SelectedNodes + '.' + node2 + '.' + Cell + '.' + cell22 + '.' + Order + TXT, "20.0");
    saveFile(SelectedNodes + '.' + node2 + '.' + Cell + '.' + cell25 + '.' + Order + TXT, "20.1");
    saveFile(SelectedNodes + '.' + node2 + '.' + Cell + '.' + cell21 + '.' + Order + TXT, "30.11");
    saveFile(SelectedNodes + '.' + node2 + '.' + Cell + '.' + cell23 + '.' + Order + TXT, "30.12");

    //
    //
    final List<String> cellIds = storage.getSelectedCellIds(node2);
    //
    //

    assertThat(cellIds).isEqualTo(List.of(cell24, cell22, cell25, cell21, cell23));
  }

  @Test
  public void setActiveNodeId__withSlash() {

    String nodeId = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    //
    //
    storage.setActiveNodeId(nodeId);
    //
    //

    assertThat(loadFile(ActiveNodeId + TXT)).isEqualTo(nodeId);
  }

  @Test
  public void activeNodeId__withSlash() {

    String nodeId = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    saveFile(ActiveNodeId + TXT, nodeId);

    //
    //
    String actualNodeId = storage.activeNodeId();
    //
    //

    assertThat(actualNodeId).isEqualTo(nodeId);
  }

  @Test
  public void setActiveCellId__withSlash() {

    String cellId = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    //
    //
    storage.setActiveCellId(cellId);
    //
    //

    assertThat(loadFile(ActiveCellId + TXT)).isEqualTo(cellId);
  }

  @Test
  public void activeCellId__withSlash() {

    String cellId = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    saveFile(ActiveCellId + TXT, cellId);

    //
    //
    String actualCellId = storage.activeCellId();
    //
    //

    assertThat(actualCellId).isEqualTo(cellId);
  }


  @Test
  public void addSelectedNodeId__withSlash() {

    String node1 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String node2 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String node3 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    //
    //
    //
    storage.addSelectedNodeId(node1);
    storage.addSelectedNodeId(node2);
    storage.addSelectedNodeId(node3);
    //
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + encodeId(node1) + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + encodeId(node2) + '.' + Order + TXT)).isEqualTo("2.0");
    assertThat(loadFile(SelectedNodes + '.' + encodeId(node3) + '.' + Order + TXT)).isEqualTo("3.0");

  }

  @Test
  public void deleteSelectedNodeId__withSlash() {

    String node1 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    saveFile(SelectedNodes + '.' + encodeId(node1) + '.' + Order + TXT, "1.0");
    saveFile(SelectedNodes + '.' + encodeId(node1) + ".tst1" + TXT, "KDRicSYZva");
    saveFile(SelectedNodes + '.' + encodeId(node1) + ".tst2" + TXT, "EkEm2Jv488");

    assertThat(loadFile(SelectedNodes + '.' + encodeId(node1) + '.' + Order + TXT)).isNotNull();
    assertThat(loadFile(SelectedNodes + '.' + encodeId(node1) + ".tst1" + TXT)).isNotNull();
    assertThat(loadFile(SelectedNodes + '.' + encodeId(node1) + ".tst2" + TXT)).isNotNull();

    //
    //
    storage.deleteSelectedNodeId(node1);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + encodeId(node1) + '.' + Order + TXT)).isNull();
    assertThat(loadFile(SelectedNodes + '.' + encodeId(node1) + ".tst1" + TXT)).isNull();
    assertThat(loadFile(SelectedNodes + '.' + encodeId(node1) + ".tst2" + TXT)).isNull();

  }

  @Test
  public void getSelectedNodeIds__withSlash() {

    String node1 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String node2 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String node3 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    saveFile(SelectedNodes + '.' + encodeId(node1) + '.' + Order + TXT, "1.0");
    saveFile(SelectedNodes + '.' + encodeId(node1) + ".tst1" + TXT, "1");
    saveFile(SelectedNodes + '.' + encodeId(node1) + ".tst2" + TXT, "1");

    saveFile(SelectedNodes + '.' + encodeId(node2) + '.' + Order + TXT, "3.0");
    saveFile(SelectedNodes + '.' + encodeId(node2) + ".tst1" + TXT, "1");
    saveFile(SelectedNodes + '.' + encodeId(node2) + ".tst2" + TXT, "1");

    saveFile(SelectedNodes + '.' + encodeId(node3) + '.' + Order + TXT, "2.0");
    saveFile(SelectedNodes + '.' + encodeId(node3) + ".tst1" + TXT, "1");
    saveFile(SelectedNodes + '.' + encodeId(node3) + ".tst2" + TXT, "1");

    //
    //
    List<String> actualList = storage.getSelectedNodeIds();
    //
    //

    assertThat(actualList).isEqualTo(List.of(node1, node3, node2));

  }

  @Test
  public void addSelectedCellId__withSlash() {

    String node1 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String node2 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    String cell11 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell12 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell21 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell22 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    String node1_  = encodeId(node1);
    String node2_  = encodeId(node2);
    String cell11_ = encodeId(cell11);
    String cell12_ = encodeId(cell12);
    String cell21_ = encodeId(cell21);
    String cell22_ = encodeId(cell22);

    //
    //
    storage.addSelectedCellId(node1, cell11);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node1_ + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node1_ + '.' + Cell + '.' + cell11_ + '.' + Order + TXT)).isEqualTo("1.0");

    //
    //
    storage.addSelectedCellId(node1, cell12);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node1_ + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node1_ + '.' + Cell + '.' + cell11_ + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node1_ + '.' + Cell + '.' + cell12_ + '.' + Order + TXT)).isEqualTo("2.0");

    //
    //
    storage.addSelectedCellId(node2, cell21);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node1_ + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node1_ + '.' + Cell + '.' + cell11_ + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node1_ + '.' + Cell + '.' + cell12_ + '.' + Order + TXT)).isEqualTo("2.0");

    assertThat(loadFile(SelectedNodes + '.' + node2_ + '.' + Order + TXT)).isEqualTo("2.0");
    assertThat(loadFile(SelectedNodes + '.' + node2_ + '.' + Cell + '.' + cell21_ + '.' + Order + TXT)).isEqualTo("1.0");

    //
    //
    storage.addSelectedCellId(node2, cell22);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node1_ + '.' + Order + TXT)).isEqualTo("1.0");

    assertThat(loadFile(SelectedNodes + '.' + node2_ + '.' + Order + TXT)).isEqualTo("2.0");
    assertThat(loadFile(SelectedNodes + '.' + node2_ + '.' + Cell + '.' + cell21_ + '.' + Order + TXT)).isEqualTo("1.0");
    assertThat(loadFile(SelectedNodes + '.' + node2_ + '.' + Cell + '.' + cell22_ + '.' + Order + TXT)).isEqualTo("2.0");
  }

  @Test
  public void deleteSelectedCellId__withSlash() {
    String node = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    String node_ = encodeId(node);
    String cell_ = encodeId(cell);

    saveFile(SelectedNodes + '.' + node_ + '.' + Cell + '.' + cell_ + '.' + Order + TXT, "1.0");

    assertThat(loadFile(SelectedNodes + '.' + node_ + '.' + Cell + '.' + cell_ + '.' + Order + TXT)).isNotNull();

    //
    //
    storage.deleteSelectedCellId(node, cell);
    //
    //

    assertThat(loadFile(SelectedNodes + '.' + node_ + '.' + Cell + '.' + cell_ + '.' + Order + TXT)).isNull();
  }

  @Test
  public void getSelectedCellIds__withSlash() {

    String node1  = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String node2  = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell11 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell12 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell21 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell22 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell23 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell24 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String cell25 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    String node1_  = encodeId(node1);
    String node2_  = encodeId(node2);
    String cell11_ = encodeId(cell11);
    String cell12_ = encodeId(cell12);
    String cell21_ = encodeId(cell21);
    String cell22_ = encodeId(cell22);
    String cell23_ = encodeId(cell23);
    String cell24_ = encodeId(cell24);
    String cell25_ = encodeId(cell25);

    saveFile(SelectedNodes + '.' + node1_ + '.' + Cell + '.' + cell11_ + '.' + Order + TXT, "1.0");
    saveFile(SelectedNodes + '.' + node1_ + '.' + Cell + '.' + cell12_ + '.' + Order + TXT, "1.0");
    saveFile(SelectedNodes + '.' + node2_ + '.' + Cell + '.' + cell24_ + '.' + Order + TXT, "10.0");
    saveFile(SelectedNodes + '.' + node2_ + '.' + Cell + '.' + cell22_ + '.' + Order + TXT, "20.0");
    saveFile(SelectedNodes + '.' + node2_ + '.' + Cell + '.' + cell25_ + '.' + Order + TXT, "20.1");
    saveFile(SelectedNodes + '.' + node2_ + '.' + Cell + '.' + cell21_ + '.' + Order + TXT, "30.11");
    saveFile(SelectedNodes + '.' + node2_ + '.' + Cell + '.' + cell23_ + '.' + Order + TXT, "30.12");

    //
    //
    final List<String> cellIds = storage.getSelectedCellIds(node2);
    //
    //

    assertThat(cellIds).isEqualTo(List.of(cell24, cell22, cell25, cell21, cell23));
  }

  @Test
  public void addSelectedNodeId__getSelectedNodeIds() {
    String node1 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String node2 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();
    String node3 = RndUtil.newId() + "/" + RndUtil.newId() + "%" + RndUtil.newId();

    //
    //
    //
    storage.addSelectedNodeId(node1);
    storage.addSelectedNodeId(node2);
    storage.addSelectedNodeId(node3);
    final List<String> loadedNodeIds = storage.getSelectedNodeIds();
    //
    //
    //

    assertThat(loadedNodeIds).isEqualTo(List.of(node1, node2, node3));
  }

  @Test
  public void deleteSelectedNodeId__withParents() {
    String node1 = RndUtil.newId();
    String node2 = node1 + "/" + RndUtil.newId();
    String node3 = node2 + "%" + RndUtil.newId();

    storage.addSelectedNodeId(node1);
    storage.addSelectedNodeId(node2);
    storage.addSelectedNodeId(node3);

    assertThat(storage.getSelectedNodeIds()).isEqualTo(List.of(node1, node2, node3));

    //
    //
    storage.deleteSelectedNodeId(node1);
    //
    //

    assertThat(storage.getSelectedNodeIds()).isEqualTo(List.of(node2, node3));

  }
}
