package kz.pompei.swing.tree;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.SneakyThrows;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TreeLoadServiceSyncDirTest {

  Path                   root;
  TreeLoadServiceSyncDir service;

  @BeforeMethod
  public void prepareRoot() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
    root = Paths.get("build").resolve(getClass().getSimpleName()).resolve(sdf.format(new Date()));

    service = new TreeLoadServiceSyncDir(root);
  }

  @SneakyThrows
  private File file(String id) {
    File file = root.resolve(id).toFile();
    file.getParentFile().mkdirs();
    file.createNewFile();
    return file;
  }

  @Test
  public void idToFile__01() {

    File file = file("asd/dsa/wow");

    //
    //
    File actual = service.idToFile("asd/dsa/wow");
    //
    //

    assertThat(actual.getAbsolutePath()).isEqualTo(file.getAbsolutePath());
  }

  @Test
  public void fileToId__01() {

    File file = file("asd/dsa/stone");

    System.out.println("si6nuiDpEO :: file = " + file);

    //
    //
    String actualId = service.fileToId(file);
    //
    //

    assertThat(actualId).isEqualTo("asd/dsa/stone");
  }
}
