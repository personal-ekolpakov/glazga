package kz.pompei.swing.tree.styles.defau;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.GuiUtil;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.swing.icons.Svg;
import kz.pompei.swing.tree.styles.Painters;

public class RectStyleFixTestForm {

  public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> {

      JFrame frame = new JFrame("Проверка стиля прямоугольной области");

      Path build = Paths.get("build");

      SizeLocationSaver.into(build.resolve("sizes_and_locations"))
                       .overJFrame("RectStyleFixTestForm", frame, true);

      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setSize(400, 300);

      frame.setIconImage(Svg.images.get("svg/apacheDerby.svg", 128, 128));

      final JPanel panel = new JPanel() {
        @Override public void paint(Graphics g1) {
          super.paint(g1);
          final Graphics2D g = GuiUtil.applyHints(g1);

          {
            RectStyleFix style = new RectStyleFix();
            style.backgroundColor   = new Color(34, 253, 76);
            style.backgroundRadiusX = 26f;
            style.backgroundRadiusY = 25f;

            final Rectangle2D.Float rect = new Rectangle2D.Float(100, 70, 200, 70);
            Painters.rect(rect, g, style);
          }
          {
            RectStyleFix style = new RectStyleFix();
            style.backgroundColor = new Color(34, 253, 76);

            style.borderColor        = new Color(14, 106, 213);
            style.borderInlineOffset = 4;
            style.borderRadiusX      = 7f;

            final Rectangle2D.Float rect = new Rectangle2D.Float(100, 200, 400, 70);
            Painters.rect(rect, g, style);
          }
          {
            RectStyleFix style = new RectStyleFix();
            style.backgroundColor = new Color(34, 253, 76);

            style.borderColor        = new Color(23, 31, 168);
            style.borderInlineOffset = 3;
            style.borderRadiusX      = 13f;

            style.borderStroke = new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, 1f, new float[]{3f, 3f}, 0f);

            final Rectangle2D.Float rect = new Rectangle2D.Float(100, 330, 500, 70);
            Painters.rect(rect, g, style);
          }

        }
      };

      frame.getContentPane().setLayout(new BorderLayout());
      frame.getContentPane().add(panel, BorderLayout.CENTER);

      frame.pack();
      frame.setVisible(true);

    });
  }

}
