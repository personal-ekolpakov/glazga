package kz.pompei.swing.tree.model;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import kz.pompei.glazga.utils.FileUtils;
import kz.pompei.swing.tree.logic.TreeItem;
import lombok.NonNull;
import lombok.SneakyThrows;

import static java.nio.charset.StandardCharsets.UTF_8;

public class FileHover implements TreeItem {
  public final String  id;
  public final String  name;
  public final String  description;
  public final boolean isFolder;
  public final String  createdAt;

  @SneakyThrows public FileHover(@NonNull String id, @NonNull File file) {
    this.id   = id;
    this.name = file.getName();
    isFolder  = file.isDirectory();

    description = isFolder ? null : Files.readString(file.toPath(), UTF_8);

    Date             createdAt = FileUtils.fileCreatedAt(file.toPath());
    SimpleDateFormat sdf       = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    this.createdAt = sdf.format(createdAt);
  }

  @Override public String id() {
    return id;
  }

  @Override public boolean isFolder() {
    return isFolder;
  }
}
