package kz.pompei.swing.tree;

import java.awt.Font;
import kz.pompei.glazga.utils.GuiUtil;
import kz.pompei.swing.fonts.FontDef;
import kz.pompei.swing.fonts.FontSource;
import kz.pompei.swing.fonts.defau.FontSourceDefault;

public class TestForm {
  public static void main(String[] args) {
    FontSource fontSource = new FontSourceDefault();


    final Font font = fontSource.get(FontDef.of("Roboto", 400, 17, false));

    GuiUtil.textHeight("asd", font);
  }
}
