package kz.pompei.swing.tree;

import kz.pompei.swing.tree.gui.SwingTreeSelection;
import kz.pompei.swing.tree.gui.SwingTreeSelectionLogic;
import kz.pompei.swing.tree.gui.SwingTreeSelectionStorageFix;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SwingTreeSelectionLogicTest {

  @Test
  public void test__01() {

    SwingTreeSelectionStorageFix storage=new SwingTreeSelectionStorageFix();
    SwingTreeSelection selection = new SwingTreeSelectionLogic(storage);

    selection.setActiveCellId("asd", "asd1");

    selection.addSelection("dsa", "dsa1");
    selection.addSelection("dsa", "dsa2");

    System.out.println("Tk5i4kISmO :: selection = " + selection);

    assertThat(selection.isSelectedNode("asd")).isEqualTo(true);
    assertThat(selection.isSelectedNode("sim")).isEqualTo(false);

    assertThat(selection.isSelectedCell("dsa", "dsa1")).isEqualTo(true);
    assertThat(selection.isSelectedCell("dsa", "dsa2")).isEqualTo(true);
    assertThat(selection.isSelectedCell("dsa", "dsa3")).isEqualTo(false);


  }

  @Test
  public void test__02() {
    SwingTreeSelectionStorageFix storage=new SwingTreeSelectionStorageFix();
    SwingTreeSelection selection = new SwingTreeSelectionLogic(storage);

    selection.addSelection("asd", "asd1");
    selection.addSelection("dsa", "dsa1");
    selection.addSelection("dsa", "dsa2");

    System.out.println("Tk5i4kISmO :: selection = " + selection);

    assertThat(selection.isSelectedNode("asd")).isEqualTo(true);
    assertThat(selection.isSelectedNode("sim")).isEqualTo(false);

    assertThat(selection.isSelectedCell("dsa", "dsa1")).isEqualTo(true);
    assertThat(selection.isSelectedCell("dsa", "dsa2")).isEqualTo(true);
    assertThat(selection.isSelectedCell("dsa", "dsa3")).isEqualTo(false);


  }

  @Test
  public void test__03() {
    SwingTreeSelectionStorageFix storage=new SwingTreeSelectionStorageFix();
    SwingTreeSelection selection = new SwingTreeSelectionLogic(storage);

    selection.setActiveNodeId("asd");

    System.out.println("R7Ey1rM1MT :: selection = " + selection);

  }

  @Test
  public void test__04() {
    SwingTreeSelectionStorageFix storage=new SwingTreeSelectionStorageFix();
    SwingTreeSelection selection = new SwingTreeSelectionLogic(storage);

    selection.setActiveNodeId("asd1");
    selection.setActiveNodeId("asd2");
    selection.setActiveNodeId("asd3");
    selection.setActiveNodeId("asd4");

    assertThat(selection.isSelectedNode("asd1")).isEqualTo(true);
    assertThat(selection.isSelectedNode("asd2")).isEqualTo(true);
    assertThat(selection.isSelectedNode("asd3")).isEqualTo(true);
    assertThat(selection.isSelectedNode("asd4")).isEqualTo(true);
    assertThat(selection.isActiveNode("asd4")).isEqualTo(true);

    System.out.println("0TBBoN6ELs :: selection 01 = " + selection);

    selection.removeSelectedNode("asd4");

    assertThat(selection.isSelectedNode("asd1")).isEqualTo(true);
    assertThat(selection.isSelectedNode("asd2")).isEqualTo(true);
    assertThat(selection.isSelectedNode("asd3")).isEqualTo(true);
    assertThat(selection.isSelectedNode("asd4")).isEqualTo(false);

    assertThat(selection.activeNodeId()).isEqualTo("asd3");


  }
}
