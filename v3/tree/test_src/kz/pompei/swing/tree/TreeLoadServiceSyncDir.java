package kz.pompei.swing.tree;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;
import kz.pompei.glazga.utils.Page;
import kz.pompei.swing.tree.logic.IllegalNodeId;
import kz.pompei.swing.tree.logic.TreeLoadServiceSync;
import kz.pompei.swing.tree.logic.model.TreeNodesPortion;
import kz.pompei.swing.tree.model.FileHover;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor
public class TreeLoadServiceSyncDir implements TreeLoadServiceSync<FileHover> {
  private final @NonNull Path root;

  private static final boolean INFINITY_LOADING = true;

  public File idToFile(@NonNull String id) {
    return root.resolve(id).toAbsolutePath().toFile();
  }

  public String fileToId(@NonNull File file) {
    return root.toAbsolutePath().relativize(file.toPath().toAbsolutePath()).toString();
  }

  @SneakyThrows
  @Override public @NonNull TreeNodesPortion<FileHover> loadNodeChildren(String parentId, @NonNull Page page) {

//    Thread.sleep(3000);

    Path directory = getDirectory(parentId);

    var ret = new TreeNodesPortion<FileHover>();

    List<FileHover> list;
    try (Stream<Path> stream = Files.list(directory)) {
      list = stream.map(Path::toFile)
                   .sorted(Comparator.comparing(File::getName))
                   .map(file -> new FileHover(fileToId(file), file))
                   .toList();
    }

    ret.list = list.stream()
                   .skip(page.offset)
                   .limit(page.limit)
                   .toList();

    if (INFINITY_LOADING) {
      ret.hasMore    = true;
      ret.totalCount = null;
    } else {
      ret.hasMore    = page.offset + page.limit < list.size();
      ret.totalCount = list.size();
    }

//    System.out.println("0Msi3OJ79R :: loadNodeChildren " + (parentId == null ? "<NULL>" : "`" + parentId + "`")
//                       + ", page = " + page
//                       + ", returns count = " + ret.list.size());

    return ret;
  }

  @Override public FileHover loadNode(@NonNull String nodeId) {
    File file = idToFile(nodeId);
    return file.exists() ? new FileHover(fileToId(file), file) : null;
  }

  private @NonNull Path getDirectory(String parentId) {
    if (parentId == null) {
      return root;
    }
    File file = idToFile(parentId);
    if (!file.exists()) {
      throw new IllegalNodeId("ZzjFr3iDDc", parentId);
    }
    return file.toPath();
  }

}
