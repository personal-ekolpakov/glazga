package kz.pompei.glazga.v3.types.engine.script;

import kz.pompei.glazga.v3.types.engine.NowSourceForTests;
import kz.pompei.glazga.v3.types.engine.Translator;
import kz.pompei.glazga.v3.types.engine.TranslatorStorageMem;
import kz.pompei.glazga.v3.types.etc.RndId;
import kz.pompei.glazga.v3.types.ids.script.ProgramGenId;
import kz.pompei.glazga.v3.types.script.ProgramGenerator;
import org.testng.annotations.BeforeMethod;

public abstract class TrTestParent {

  protected TrProgramGen translator;

  @BeforeMethod
  public void createTranslator() {

    ProgramGenerator pg = new ProgramGenerator();

    NowSourceForTests time = new NowSourceForTests();

    ProgramGenId programGenId = ProgramGenId.of(RndId.rndId());

    TranslatorStorageMem storageMem = new TranslatorStorageMem(time);
    storageMem.putProgramGen(programGenId, pg);

    Translator translator = new Translator(storageMem, time);

    this.translator = new TrProgramGen(programGenId, storageMem, translator);
  }

}
