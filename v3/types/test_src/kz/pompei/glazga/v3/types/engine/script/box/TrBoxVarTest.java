package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.script.TrTestParent;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.script.boxes.BoxVar;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrBoxVarTest extends TrTestParent {

  @Test
  public void create() {
    var tb = new TrBoxVar(BoxId.of("Hi67fG52oG"), BoxVar.of("skYiOBbOVv", "H6H7rZRqRi"), Map.of(), translator);

    assertThat(tb).isNotNull();
  }

}
