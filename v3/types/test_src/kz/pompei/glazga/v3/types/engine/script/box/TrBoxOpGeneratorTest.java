package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.script.TrTestParent;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.script.boxes.BoxOpGenerator;
import org.testng.annotations.Test;

public class TrBoxOpGeneratorTest extends TrTestParent {

  @Test
  public void create() {
    new TrBoxOpGenerator(BoxId.of("Hi67fG52oG"), BoxOpGenerator.owner("9MUknIC8ac"), Map.of(), translator);
  }
}
