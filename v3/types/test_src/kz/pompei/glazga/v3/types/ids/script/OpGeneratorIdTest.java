package kz.pompei.glazga.v3.types.ids.script;

import kz.greetgo.util.RND;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class OpGeneratorIdTest {

  @Test
  public void convert() {

    OpGeneratorId origin = new OpGeneratorId(BoxId.of(RND.str(10)), ProgramGenId.of(RND.str(10)), ModuleProfile.MASTER);

    //
    //
    String strValue = origin.strValue();
    //
    //

    System.out.println("fF72P6L1ps :: strValue = " + strValue);

    //
    //
    OpGeneratorId actual = OpGeneratorId.parse(strValue);
    //
    //

    assertThat(actual.programGenId).isEqualTo(origin.programGenId);
    assertThat(actual.opgBoxId).isEqualTo(origin.opgBoxId);
    assertThat(actual).isEqualTo(origin);

  }
}
