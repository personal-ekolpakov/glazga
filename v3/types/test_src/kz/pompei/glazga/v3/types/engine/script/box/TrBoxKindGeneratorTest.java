package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.script.TrTestParent;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.script.boxes.BoxKindGenerator;
import kz.pompei.glazga.v3.types.script.boxes.kind_gen.Destiny;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrBoxKindGeneratorTest extends TrTestParent {

  @Test
  public void create() {
    var tr = new TrBoxKindGenerator(BoxId.of("Hi67fG52oG"), BoxKindGenerator.destiny(Destiny.EMBED_ALIAS), Map.of(), translator);
    assertThat(tr);
  }
}
