package kz.pompei.glazga.v3.types.script.got;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GotKindEmbedConstTest {

  @Test
  public void parseFieldDataDef() {

    byte[] bytes = GotKindEmbedConst.parseFieldDataDef("i4 3465467");



    assertThat(bytes).isEqualTo(new byte[]{-5, -32, 52, 0});

  }
}
