package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.script.TrTestParent;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.script.boxes.BoxGot;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrBoxGotTest extends TrTestParent {

  @Test
  public void create() {
    var tr = new TrBoxGot(BoxId.of("Hi67fG52oG"), BoxGot.of("ja6gA964D3"), Map.of(), translator);
    assertThat(tr);
  }
}
