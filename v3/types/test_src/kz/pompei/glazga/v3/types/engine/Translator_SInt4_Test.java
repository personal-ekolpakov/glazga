package kz.pompei.glazga.v3.types.engine;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import kz.pompei.glazga.utils.FileUtils;
import kz.pompei.glazga.v3.types.engine.dia.DiaMessage;
import kz.pompei.glazga.v3.types.engine.linking.LinkResult;
import kz.pompei.glazga.v3.types.engine.linking.Linking;
import kz.pompei.glazga.v3.types.engine.llvm.data.ModuleLlvm;
import kz.pompei.glazga.v3.types.engine.llvm.data.ProgramTranslateResult;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbed;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbedType;
import kz.pompei.glazga.v3.types.etc.IDV;
import kz.pompei.glazga.v3.types.ids.script.ModuleId;
import kz.pompei.glazga.v3.types.ids.script.ModuleProfile;
import kz.pompei.glazga.v3.types.ids.script.ProgramGenId;
import kz.pompei.glazga.v3.types.script.ProgramGenerator;
import kz.pompei.glazga.v3.types.script.boxes.BoxIf;
import kz.pompei.glazga.v3.types.script.boxes.BoxKindFix;
import kz.pompei.glazga.v3.types.script.boxes.BoxKindGenerator;
import kz.pompei.glazga.v3.types.script.boxes.BoxOpGenerator;
import kz.pompei.glazga.v3.types.script.boxes.BoxReturn;
import kz.pompei.glazga.v3.types.script.boxes.BoxVar;
import kz.pompei.glazga.v3.types.script.boxes.kind_gen.Destiny;
import kz.pompei.glazga.v3.types.script.got.GotCallNative;
import kz.pompei.glazga.v3.types.script.got.GotCallOp;
import kz.pompei.glazga.v3.types.script.got.GotKindEmbedConst;
import kz.pompei.glazga.v3.types.script.got.GotOwnerTypeRef;
import kz.pompei.glazga.v3.types.script.got.GotRefArg;
import kz.pompei.glazga.v3.types.script.got.GotRefKind;
import kz.pompei.glazga.v3.types.script.got.GotRefVar;
import lombok.SneakyThrows;
import org.testng.annotations.Test;

import static kz.pompei.glazga.v3.types.script.arrow.ArrowType.DOWN;
import static kz.pompei.glazga.v3.types.script.arrow.ArrowType.IF_ELSE;
import static kz.pompei.glazga.v3.types.script.arrow.ArrowType.IF_THEN;
import static org.assertj.core.api.Assertions.assertThat;

public class Translator_SInt4_Test {

  // TODO pompei реализовать этот тест
  @Test @SneakyThrows
  public void try_SInt4() {

    ProgramGenId     programGenId = ProgramGenId.of("Probe_SInt4");
    ProgramGenerator pg           = new ProgramGenerator();
    pg.box("SInt32_mL3eeBb1iP", BoxKindGenerator.destiny(Destiny.EMBED_ALIAS)
                                                .kindEmbed(KindEmbed.of(KindEmbedType.INT, 4 * 8))
                                                .name("Целое 4 байта")
                                                .code("SInt32"));

    {

      pg.box("01_PLUS", BoxOpGenerator.owner("SInt32_mL3eeBb1iP")
                                      .name("Плюс")
                                      .code("PLUS")
                                      .arg("LEFT", "Левый аргумент", "01_this1")
                                      .arg("RIGHT", "Правый аргумент", "01_this2")
                                      .out("RESULT", "Результат", "01_this3")
                                      .area(100, 100, 1, 1)

                                      .got("01_this1", GotOwnerTypeRef.of())
                                      .got("01_this2", GotOwnerTypeRef.of())
                                      .got("01_this3", GotOwnerTypeRef.of())
      );

      pg.arrow("01_PLUS", DOWN, "02_return");

      pg.box("02_return", BoxReturn.outCode("RESULT").outGotId("02_resultRef")
                                   .got("02_leftRef", GotRefArg.code("LEFT"))
                                   .got("02_rightRef", GotRefArg.code("RIGHT"))
                                   .got("02_this", GotOwnerTypeRef.of())
                                   .got("02_resultRef", GotCallNative.code("LLVM_ADD")
                                                                     .arg("LEFT", "02_leftRef")
                                                                     .arg("RIGHT", "02_rightRef")
                                                                     .mainOutCode("RESULT")
                                                                     .typeGotId("02_this"))
      );
    }
    {
      pg.box("test_2_plus_3_eq_5_FIX", BoxKindFix.of().test(true));
      pg.arrow("test_2_plus_3_eq_5_FIX", DOWN, "test_2_plus_3_eq_5");
      pg.box("test_2_plus_3_eq_5", BoxOpGenerator.owner("SInt32_mL3eeBb1iP")
                                                 .profile(ModuleProfile.TEST)
                                                 .name("Два плюс три равно пять")
                                                 .out("OK", "Нормальный результат", "SInt4_1")
                                                 .out("ERROR", "Ошибка", "SInt4_2")
                                                 .area(100, 100, 1, 1)
                                                 .got("SInt4_1", GotRefKind.of("SInt32_mL3eeBb1iP"))
                                                 .got("SInt4_2", GotRefKind.of("SInt32_mL3eeBb1iP")));
      pg.arrow("test_2_plus_3_eq_5", DOWN, "03_var2");
      pg.box("03_var2", BoxVar.of("Число 2", "03_const2")
                              .got("03_const2", GotKindEmbedConst.of(KindEmbedType.INT, 32, "2")));
      pg.arrow("03_var2", DOWN, "04_var3");
      pg.box("04_var3", BoxVar.of("Число 3", "04_const3")
                              .got("04_const3", GotKindEmbedConst.of(KindEmbedType.INT, 32, "3")));
      pg.arrow("04_var3", DOWN, "05_varResult");
      pg.box("05_varResult", BoxVar.of("Результат", "05_plus")
                                   .got("05_left", GotRefVar.of("03_var2"))
                                   .got("05_right", GotRefVar.of("04_var3"))
                                   .got("05_plus", GotCallOp.of("01_PLUS", programGenId.id, ModuleProfile.MASTER)
                                                            .arg("LEFT", "05_left")
                                                            .arg("RIGHT", "05_right")
                                                            .mainOutCode("RESULT")));
      pg.arrow("05_varResult", DOWN, "06_expect5");
      pg.box("06_expect5", BoxVar.of("Ожидаемое значение", "06_const5")
                                 .got("06_const5", GotKindEmbedConst.of(KindEmbedType.INT, 32, "5")));
      pg.arrow("06_expect5", DOWN, "07_if");
      pg.box("07_if", BoxIf.condition("07_condition")
                           .got("07_left", GotRefVar.of("05_varResult"))
                           .got("07_right", GotRefVar.of("06_expect5"))
                           .got("07_SInt5", GotRefKind.of("SInt32_mL3eeBb1iP"))
                           .got("07_condition", GotCallNative.code("LLVM_ICMP_EQ")
                                                             .arg("LEFT", "07_left")
                                                             .arg("RIGHT", "07_right")
                                                             .mainOutCode("RESULT")
                                                             .typeGotId("07_SInt5")));
      {
        pg.arrow("07_if", IF_ELSE, "07_1_return_err");
        pg.box("07_1_return_err", BoxReturn.outCode("ERROR")
                                           .outGotId("07_1_result")
                                           .got("07_1_result", GotKindEmbedConst.of(KindEmbedType.INT, 32, "21")));
      }
      {
        pg.arrow("07_if", IF_THEN, "07_2_return_ok");
        pg.box("07_2_return_ok", BoxReturn.outCode("OK")
                                          .outGotId("07_2_result")
                                          .got("07_2_result", GotKindEmbedConst.of(KindEmbedType.INT, 32, "17")));
      }
    }

    NowSourceForTests time = new NowSourceForTests();

    TranslatorStorageMem storageMem = new TranslatorStorageMem(time);
    storageMem.putProgramGen(programGenId, pg);

    Translator translator = new Translator(storageMem, time);

    translator.translateKindFixes(Set.of(programGenId));

    ProgramTranslateResult programTranslateResult = storageMem.loadTranslateResult(programGenId);

    if (programTranslateResult == null) {
      throw new RuntimeException("jU7oAkyXSK :: programTranslateResult == null for programGenId = " + programGenId);
    }

    boolean noErrors = true, hasPrint = false;
    for (final DiaMessage diaMessage : programTranslateResult.diagnostic) {
      System.out.println("b0VRTi1wGr :: " + diaMessage);
      noErrors = noErrors && !diaMessage.isError();
      hasPrint = true;
    }
    if (hasPrint) {
      System.out.println();
      System.out.println();
    }
    if (noErrors) {
      System.out.println("gpFY80aG2D :: При трансляции ошибок нет");
    }

    List<IDV<ModuleId, ModuleLlvm>> modules = programTranslateResult.moduleIds.stream()
                                                                              .map(id -> IDV.idv(id, storageMem.loadModuleLlvm(id)))
                                                                              .toList();

    Path buildDir = Paths.get("build").resolve(getClass().getSimpleName());
    FileUtils.removeFile(buildDir);

    Linking    linking    = new Linking(buildDir);
    LinkResult linkResult = linking.linkTests(modules);

    boolean hasErrors = false;
    for (final DiaMessage message : linkResult.messages) {
      System.out.println(message.messageLines("ZKthJxcCuU " + message.type + " "));
      hasErrors = hasErrors || message.isError();
    }
    if (hasErrors) {
      System.out.println("RhfN0aSGqI :: При связывании есть ошибки");
    } else {
      System.out.println("7x3kvJNBwh :: При связывании ошибок нет");
      System.out.println("HSynWvRyMw :: test soFile : " + linkResult.soFile);
    }

    long errorCount = programTranslateResult.diagnostic.stream().filter(DiaMessage::isError).count();
    assertThat(errorCount).isZero();

    throw new RuntimeException("zbs1bv0ix1 :: Продолжить здесь");
  }
}
