package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.script.TrTestParent;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.script.boxes.BoxIf;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrBoxIfTest  extends TrTestParent {

  @Test
  public void create() {
    var tr = new TrBoxIf(BoxId.of("Hi67fG52oG"), BoxIf.condition("J4bVJgGCvf"), Map.of(), translator);
    assertThat(tr);
  }
}
