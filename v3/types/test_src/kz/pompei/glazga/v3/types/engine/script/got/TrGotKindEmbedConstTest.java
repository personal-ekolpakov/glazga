package kz.pompei.glazga.v3.types.engine.script.got;

import kz.pompei.glazga.v3.types.engine.script.TrTestParent;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbedType;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.got.GotKindEmbedConst;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrGotKindEmbedConstTest extends TrTestParent {

  @Test
  public void create() {
    var g = new TrGotKindEmbedConst(BoxId.of("IVo1RmUJJr"),
                                    GotId.of("xvrAfbS1tV"),
                                    GotKindEmbedConst.of(KindEmbedType.INT, 4, "3"),
                                    translator);
    assertThat(g).isNotNull();
  }
}
