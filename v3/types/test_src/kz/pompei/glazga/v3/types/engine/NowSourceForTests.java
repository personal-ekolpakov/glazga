package kz.pompei.glazga.v3.types.engine;

import java.util.Date;

public class NowSourceForTests implements NowSource {

  public long testNow = new Date().getTime();

  @Override public long now() {
    return testNow++;
  }
}
