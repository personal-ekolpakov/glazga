package kz.pompei.glazga.v3.types.engine.script.got;

import kz.pompei.glazga.v3.types.engine.script.TrTestParent;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.got.GotRefVar;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrGotRefVarTest extends TrTestParent {

  @Test
  public void create() {
    var g = new TrGotRefVar(BoxId.of("IVo1RmUJJr"), GotId.of("xvrAfbS1tV"), GotRefVar.of("PNp1XB9RHk"), translator);
    assertThat(g).isNotNull();
  }
}
