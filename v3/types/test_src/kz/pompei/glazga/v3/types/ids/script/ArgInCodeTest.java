package kz.pompei.glazga.v3.types.ids.script;

import kz.greetgo.util.RND;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ArgInCodeTest {

  @Test
  public void convertToStrAndBack() {

    final ArgInCode source = new ArgInCode(RND.str(10));

    //
    //
    final String string = source.code();
    //
    //

    System.out.println("FpJfjXP1HW :: string = " + string);

    //
    //
    final ArgInCode actual = ArgInCode.of(string);
    //
    //

    assertThat(actual).isEqualTo(source);
    assertThat(actual.code).isEqualTo(source.code);
  }
}
