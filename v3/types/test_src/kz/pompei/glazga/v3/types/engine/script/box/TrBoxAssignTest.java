package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.script.TrTestParent;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.script.boxes.BoxAssign;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrBoxAssignTest extends TrTestParent {

  @Test
  public void create() {
    var tr = new TrBoxAssign(BoxId.of("Hi67fG52oG"), BoxAssign.left("m4ImHoTtEh"), Map.of(), translator);
    assertThat(tr);
  }
}
