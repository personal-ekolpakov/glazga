package kz.pompei.glazga.v3.types.func;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MemDumpTest {

  @Test
  public void fromInt_4bytes() {

    MemDump memDump = MemDump.fromInt_4bytes(-54325345);

    byte[] data = memDump.data();

    assertThat(data).hasSize(4);
    assertThat(data[0]).isEqualTo((byte) -97);
    assertThat(data[1]).isEqualTo((byte) 15);
    assertThat(data[2]).isEqualTo((byte) -61);
    assertThat(data[3]).isEqualTo((byte) -4);

  }
}
