package kz.pompei.glazga.v3.types.engine.script.got;

import kz.pompei.glazga.v3.types.engine.script.TrTestParent;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.script.ModuleProfile;
import kz.pompei.glazga.v3.types.script.got.GotCallOp;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrGotCallOpTest extends TrTestParent {

  @Test
  public void create() {
    var g = new TrGotCallOp(BoxId.of("IVo1RmUJJr"),
                            GotId.of("xvrAfbS1tV"),
                            GotCallOp.of("PNp1XB9RHk", "S5cokP5boY", ModuleProfile.MASTER),
                            translator);
    assertThat(g).isNotNull();
  }
}
