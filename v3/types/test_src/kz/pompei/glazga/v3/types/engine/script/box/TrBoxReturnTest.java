package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.script.TrTestParent;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.script.boxes.BoxReturn;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TrBoxReturnTest extends TrTestParent {
  @Test
  public void create() {
    var tb = new TrBoxReturn(BoxId.of("Hi67fG52oG"), BoxReturn.outCode("ONEYmI8Urd"), Map.of(), translator);

    assertThat(tb).isNotNull();
  }
}
