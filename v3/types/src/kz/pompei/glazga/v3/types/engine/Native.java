package kz.pompei.glazga.v3.types.engine;

import java.util.Map;
import java.util.Objects;
import kz.pompei.glazga.v3.types.ids.script.NativeArgCode;
import kz.pompei.glazga.v3.types.ids.script.NativeCode;
import kz.pompei.glazga.v3.types.ids.script.NativeId;
import lombok.Builder;
import lombok.NonNull;

@Builder
public class Native {
  public final @NonNull NativeCode code;
  public final          String     description;
  public final          int        resultSizeBytes;

  public final @NonNull Map<@NonNull NativeArgCode, @NonNull Integer> argSizesBytes;

  private NativeId id;

  public @NonNull NativeId id() {
    {
      var id = this.id;
      if (id != null) {
        return id;
      }
    }
    return id = NativeId.builder().code(code).argSizesBytes(argSizesBytes).build();
  }

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final Native aNative = (Native) o;
    return resultSizeBytes == aNative.resultSizeBytes && Objects.equals(code, aNative.code) && Objects.equals(
      argSizesBytes, aNative.argSizesBytes);
  }

  @Override public int hashCode() {
    return Objects.hash(code, resultSizeBytes, argSizesBytes);
  }
}
