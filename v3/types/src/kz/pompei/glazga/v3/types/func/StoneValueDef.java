package kz.pompei.glazga.v3.types.func;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class StoneValueDef {
  public final @NonNull String name;
  public final          double order;

  public static StoneValueDef of(double order, @NonNull String name) {
    return new StoneValueDef(name, order);
  }
}
