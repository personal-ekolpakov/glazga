package kz.pompei.glazga.v3.types.engine;

import java.util.Map;
import kz.pompei.glazga.v3.types.func.StoneValue;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Типовые аргументы
 */
@RequiredArgsConstructor
public class Stones {

  /**
   * Набор конкретных типов для генератора
   */
  public final @NonNull Map<StoneKindCode, KindId> kinds;

  /**
   * Набор величин для генератора
   */
  public final @NonNull Map<StoneValueCode, StoneValue> values;
}
