package kz.pompei.glazga.v3.types.engine.script.kind;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kz.pompei.glazga.v3.calc.Once;
import kz.pompei.glazga.v3.types.engine.Translator;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor
public class KindEmbed extends Kind {

  public final @NonNull KindEmbedType type;
  public final          int           sizeBits;

  private final Once<KindId> idOnce = new Once<>(this::calcId);

  @Override @SneakyThrows public KindId id() {
    return idOnce.get();
  }

  private @SneakyThrows KindId calcId() {
    return KindId.of(llvmName());
  }

  @Override public @NonNull String displayStr(@NonNull Translator translator) {
    return toString();
  }

  @Override public String toString() {
    return llvmName();
  }

  public String llvmName() {
    return (switch (type) {
      case INT -> "i";
      case FLOAT -> "f";
    }) + sizeBits;
  }

  private static final Pattern P_INT   = Pattern.compile("i(\\d+)");
  private static final Pattern P_FLOAT = Pattern.compile("f(\\d+)");

  public static @NonNull KindEmbed of(@NonNull KindEmbedType type, int sizeBytes) {
    return new KindEmbed(type, sizeBytes);
  }

  public static Optional<KindEmbed> byId(@NonNull KindId kindId) {

    {
      Matcher matcher = P_INT.matcher(kindId.id);
      if (matcher.matches()) {
        int sizeBits = Integer.parseInt(matcher.group(1));
        return Optional.of(of(KindEmbedType.INT, sizeBits));
      }
    }
    {
      Matcher matcher = P_FLOAT.matcher(kindId.id);
      if (matcher.matches()) {
        int sizeBits = Integer.parseInt(matcher.group(1));
        return Optional.of(of(KindEmbedType.FLOAT, sizeBits));
      }
    }

    return Optional.empty();
  }
}
