package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@StringRepresented
@RequiredArgsConstructor
public class FuncLlvmId implements Comparable<FuncLlvmId> {
  public final @NonNull ProgramGenId  progId;
  public final @NonNull KindId        kindId;
  public final @NonNull ModuleProfile profile;

  public static @NonNull FuncLlvmId of(@NonNull ProgramGenId progId, @NonNull KindId kindId, @NonNull ModuleProfile profile) {
    return new FuncLlvmId(progId, kindId, profile);
  }

  @StringRepresented
  public static FuncLlvmId parse(String id) {
    if (id == null || id.length() <= 3) {
      return null;
    }
    if (id.charAt(1) != '.') {
      return null;
    }

    ModuleProfile profile = ModuleProfile.valueByChar(id.charAt(0));
    if (profile == null) {
      return null;
    }

    int idx = id.indexOf('.', 3);
    if (idx < 0) {
      return null;
    }


    ProgramGenId progId = ProgramGenId.of(id.substring(2, idx));
    KindId       kindId = KindId.of(id.substring(idx + 1));

    return FuncLlvmId.of(progId, kindId, profile);
  }

  @StringRepresented public String id() {
    return profile.c + "." + progId.id() + "." + kindId.id();
  }

  @Override public int compareTo(FuncLlvmId o) {

    int c1 = progId.compareTo(o.progId);
    if (c1 != 0) {
      return c1;
    }

    return kindId.compareTo(o.kindId);
  }

  public String funcName() {
    return "@" + id();
  }
}
