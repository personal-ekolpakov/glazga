package kz.pompei.glazga.v3.types.script.boxes;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.v3.types.func.StoneKindDef;
import kz.pompei.glazga.v3.types.func.StoneValueDef;
import kz.pompei.glazga.v3.types.ids.script.ArgInCode;
import kz.pompei.glazga.v3.types.ids.script.ArgOutCode;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.script.ModuleProfile;
import kz.pompei.glazga.v3.types.ids.type.OpCode;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import kz.pompei.glazga.v3.types.script.boxes.op.ArgInDef;
import kz.pompei.glazga.v3.types.script.boxes.op.ArgOutDef;
import lombok.NonNull;
import lombok.experimental.FieldNameConstants;

/**
 * Генератор операции
 */
@FieldNameConstants
public class BoxOpGenerator extends Box {

  public Map<StoneKindCode, StoneKindDef>   stoneKinds;
  public Map<StoneValueCode, StoneValueDef> stoneValues;

  public BoxId         ownerId;
  public OpCode        code;
  public String        name;
  public ModuleProfile profile;

  public Map<ArgInCode, ArgInDef>   args;
  public Map<ArgOutCode, ArgOutDef> outs;

  public static BoxOpGenerator owner(@NonNull String ownerBoxId) {
    BoxOpGenerator ret = new BoxOpGenerator();
    ret.ownerId = new BoxId(ownerBoxId);
    ret.profile = ModuleProfile.MASTER;
    return ret;
  }

  public BoxOpGenerator name(String name) {
    this.name = name;
    return this;
  }

  public BoxOpGenerator profile(ModuleProfile profile) {
    this.profile = profile;
    return this;
  }

  public BoxOpGenerator code(String code) {
    this.code = OpCode.of(code);
    return this;
  }

  public BoxOpGenerator out(@NonNull String code, String name, String valueTypeGotId) {
    if (outs == null) {
      outs = new HashMap<>();
    }
    ArgOutDef out = new ArgOutDef();
    out.valueKindGotId = GotId.of(valueTypeGotId);
    out.argumentName   = name;
    out.master         = outs.isEmpty();
    out.order          = outs.values().stream().mapToDouble(x -> x.order).max().orElse(0) + 1;
    outs.put(ArgOutCode.of(code), out);
    return this;
  }

  public BoxOpGenerator arg(String code, String name, String typeGotId) {
    if (args == null) {
      args = new HashMap<>();
    }

    ArgInDef def = new ArgInDef();

    def.name      = name;
    def.kindGotId = GotId.of(typeGotId);
    def.order     = args.values().stream().mapToDouble(x -> x.order).max().orElse(0) + 1;
    def.self      = args.isEmpty();

    args.put(ArgInCode.of(code), def);
    return this;
  }

  @Override protected @NonNull List<String> listToDisplay() {
    List<String> list = new ArrayList<>();
    if (profile != null) {
      list.add(profile.name());
    }
    if (ownerId != null) {
      list.add(Fields.ownerId + '=' + ownerId);
    }
    if (code != null) {
      list.add(Fields.code + '=' + code);
    }
    if (name != null) {
      list.add(Fields.name + '=' + name);
    }
    if (stoneKinds != null) {
      List<Map.Entry<StoneKindCode, StoneKindDef>> lst = stoneKinds.entrySet()
                                                                   .stream()
                                                                   .sorted(Comparator.comparing(x -> x.getValue().order))
                                                                   .toList();
      for (final Map.Entry<StoneKindCode, StoneKindDef> x : lst) {
        list.add("[typ:id=" + x.getKey() + ",name=" + x.getValue().name + ']');
      }
    }
    if (stoneValues != null) {
      List<Map.Entry<StoneValueCode, StoneValueDef>> lst = stoneValues.entrySet()
                                                                      .stream()
                                                                      .sorted(Comparator.comparing(x -> x.getValue().order))
                                                                      .toList();
      for (final Map.Entry<StoneValueCode, StoneValueDef> x : lst) {
        list.add("[val:id=" + x.getKey() + ",name=" + x.getValue().name + ']');
      }
    }

    if (args != null) {
      List<Map.Entry<ArgInCode, ArgInDef>> lst = args.entrySet()
                                                     .stream()
                                                     .sorted(Comparator.comparing(x -> x.getValue().order))
                                                     .toList();
      for (final Map.Entry<ArgInCode, ArgInDef> x : lst) {
        ArgInDef v = x.getValue();
        list.add("[val:id=" + x.getKey() + (v.self ? "SELF," : "")
                 + ",name=" + v.name
                 + ',' + ArgInDef.Fields.kindGotId + '=' + v.kindGotId + ']');
      }
    }

    return list;
  }
}
