package kz.pompei.glazga.v3.types.engine.model;

import java.util.List;
import kz.pompei.glazga.v3.types.etc.IDV;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.script.boxes.BoxKindFix;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BoxKindFixList {
  public final @NonNull List<IDV<BoxId, BoxKindFix>> list;
  public final          long                         lastModifiedMs;
}
