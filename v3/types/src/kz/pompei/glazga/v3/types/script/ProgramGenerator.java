package kz.pompei.glazga.v3.types.script;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.v3.types.etc.RndId;
import kz.pompei.glazga.v3.types.ids.script.ArrowId;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.script.arrow.Arrow;
import kz.pompei.glazga.v3.types.script.arrow.ArrowType;
import kz.pompei.glazga.v3.types.script.boxes.Box;

public class ProgramGenerator {

  public Map<BoxId, Box>     boxes  = new HashMap<>();
  public Map<ArrowId, Arrow> arrows = new HashMap<>();


  @SuppressWarnings("UnusedReturnValue")
  public ProgramGenerator box(String id, Box box) {
    boxes.put(BoxId.of(id), box);
    return this;
  }

  @SuppressWarnings("UnusedReturnValue")
  public ProgramGenerator arrow(String sourceBoxId, ArrowType type, String targetBoxId) {
    Arrow arrow = new Arrow();
    arrow.type     = type;
    arrow.sourceId = BoxId.of(sourceBoxId);
    arrow.targetId = BoxId.of(targetBoxId);
    arrows.put(ArrowId.of(RndId.rndId()), arrow);
    return this;
  }
}
