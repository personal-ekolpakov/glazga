package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.List;
import java.util.Map;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.engine.translating.BoxTranslatingOp;
import kz.pompei.glazga.v3.types.engine.translating.BoxWithContinueDown;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.arrow.ArrowType;
import kz.pompei.glazga.v3.types.script.boxes.BoxIf;
import lombok.NonNull;

public class TrBoxIf extends TrBox implements BoxTranslatingOp, BoxWithContinueDown {
  public final @NonNull BoxIf data;

  public TrBoxIf(@NonNull BoxId id,
                 @NonNull BoxIf data, @NonNull Map<GotId, TrGot> gotten,
                 @NonNull TrProgramGen translator) {
    super(id, gotten, translator);
    this.data = data;
  }

  public TrBox continueDownBox;

  @Override public TrBox continueDownBox() {
    return continueDownBox;
  }

  @Override public void translateOp(@NonNull TrFuncLlvm funcLLVM) {

    String myLabel = funcLLVM.getBoxLabel(this);

    funcLLVM.lines().add("");
    funcLLVM.lines().add(myLabel + ": ;Wr446BAVnb " + happenedMsg().message());
    LocalVar varName = translateGotId(data.conditionGotId,
                                      Msg.of("9EuLaFj1yp", "conditionGotId", data.conditionGotId),
                                      gotten::get, 1, funcLLVM);

    if (varName == null) {
      err(Msg.of("itMw1hDQg6"));
      return;
    }

    List<TrBox> arrowsIfThen = program.getAttachedBoxes(id, ArrowType.IF_THEN);
    List<TrBox> arrowsIfElse = program.getAttachedBoxes(id, ArrowType.IF_ELSE);
    List<TrBox> arrowsDown   = program.getAttachedBoxes(id, ArrowType.DOWN);

    if (arrowsIfThen.size() > 1) {
      err(Msg.of("Lk7WSFt02B"));
      return;
    }
    if (arrowsIfElse.size() > 1) {
      err(Msg.of("AnByEAhz1k"));
      return;
    }
    if (arrowsDown.size() > 1) {
      err(Msg.of("WdyMd1WPHx"));
      return;
    }

    TrBox boxThen = null;
    TrBox boxElse = null;
    TrBox boxDown = null;

    if (arrowsIfThen.size() == 1) {
      boxThen = arrowsIfThen.get(0);
    }
    if (arrowsIfElse.size() == 1) {
      boxElse = arrowsIfElse.get(0);
    }
    if (arrowsDown.size() == 1) {
      boxDown = continueDownBox = arrowsDown.get(0);
    }

    String labelThen = null;
    String labelElse = null;

    if (boxThen != null) {
      labelThen = funcLLVM.getBoxLabel(boxThen);
    }

    if (boxElse != null) {
      labelElse = funcLLVM.getBoxLabel(boxElse);
    }

    if (boxDown != null) {
      if (labelElse == null) {
        labelElse = funcLLVM.getBoxLabel(boxDown);
      } else if (labelThen == null) {
        labelThen = funcLLVM.getBoxLabel(boxDown);
      }
    }

    if (labelThen == null) {
      err(Msg.of("lIIuhNSU5T"));
      return;
    }

    if (labelElse == null) {
      err(Msg.of("rAN1W0fxPm"));
      return;
    }

    funcLLVM.lines().add("  br i1 " + varName.name + ", label %" + labelThen + ", label %" + labelElse);

    if (boxThen != null) {
      followTranslateOp(boxThen, Msg.of("0Ug1vgLRwp"), funcLLVM);
    }
    if (boxElse != null) {
      followTranslateOp(boxElse, Msg.of("GuSFRbiRrp"), funcLLVM);
    }
    if (boxDown != null) {
      followTranslateOp(boxDown, Msg.of("oMy5dMM0FP"), funcLLVM);
    }

  }
}
