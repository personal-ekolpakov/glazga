package kz.pompei.glazga.v3.types.engine.llvm.data;

import java.util.List;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.ArgInCode;
import kz.pompei.glazga.v3.types.ids.script.BoxId;

public class FuncLlvm {

  public List<String>             lines;
  public BoxId                    opGeneratorId;
  public Map<ArgInCode, LocalVar> argVars;
  public Map<BoxId, LocalVar>     boxLocalVars;
  public Map<BoxId, String>       boxLabels;
  public Set<String>              variableNames;

}
