package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.boxes.BoxAssign;
import lombok.NonNull;

public class TrBoxAssign extends TrBox {
  public final @NonNull BoxAssign data;

  public TrBoxAssign(@NonNull BoxId id, @NonNull BoxAssign data, @NonNull Map<GotId, TrGot> gotten, @NonNull TrProgramGen translator) {
    super(id, gotten, translator);
    this.data = data;
  }
}
