package kz.pompei.glazga.v3.types.engine.llvm.tr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.v3.types.engine.dia.DiaMessage;
import kz.pompei.glazga.v3.types.engine.dia.DiaType;
import kz.pompei.glazga.v3.types.engine.dia.HappenedObject;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.data.FuncLlvm;
import kz.pompei.glazga.v3.types.engine.llvm.data.ModuleLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.box.TrBox;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxKindGenerator;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxOpGenerator;
import kz.pompei.glazga.v3.types.engine.script.kind.Kind;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbed;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbedAlias;
import kz.pompei.glazga.v3.types.engine.script.kind.KindOp;
import kz.pompei.glazga.v3.types.engine.script.kind.Kinds;
import kz.pompei.glazga.v3.types.ids.script.FuncLlvmId;
import kz.pompei.glazga.v3.types.ids.script.ModuleId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.NonNull;

public class TrModuleLlvm extends TrParentLlvm implements HappenedObject {
  public final @NonNull ModuleId     id;
  public final @NonNull ModuleLlvm   data;
  public final @NonNull TrProgramGen program;

  public final Map<FuncLlvmId, TrFuncLlvm> functions = new HashMap<>();

  public TrModuleLlvm(@NonNull ModuleId id, @NonNull ModuleLlvm data, @NonNull TrProgramGen program) {
    this.id      = id;
    this.data    = data;
    this.program = program;

    if (data.functions != null) {
      for (final Map.Entry<FuncLlvmId, FuncLlvm> e : data.functions.entrySet()) {
        FuncLlvmId funcId = e.getKey();
        FuncLlvm   func   = e.getValue();

        if (func.opGeneratorId == null) {
          err(Msg.of("6vzKBo1gai"));
          continue;
        }

        TrBox box = program.getOrLoadBoxById(func.opGeneratorId);

        if (box == null) {
          err(Msg.of("CD3DVFg8BY", "opGeneratorId", func.opGeneratorId));
          continue;
        }

        if (!(box instanceof TrBoxOpGenerator oopGenerator)) {
          err(Msg.of("f4NAVr1fGB", "class", box.getClass()));
          continue;
        }

        Kind kind = program.storage.getKindById(funcId.kindId);
        if (kind == null) {
          err(Msg.of("hSGrQ0MPQy", "kindId", funcId.kindId));
          continue;
        }

        if (!(kind instanceof KindOp kop)) {
          err(Msg.of("8bz8UCxPdt", "class", kind.getClass()));
          continue;
        }

        functions.put(funcId, new TrFuncLlvm(funcId, func, oopGenerator, this, kop));
      }
    }

  }

  @Override protected @NonNull Set<String> variableNames() {
    if (data.variableNames == null) {
      return data.variableNames = new HashSet<>();
    }
    return data.variableNames;
  }

  public @NonNull String getKindName(@NonNull KindId kindId) {
    return getKindName0(kindId, new HashSet<>());
  }

  private @NonNull String getKindName0(@NonNull KindId kindId, Set<KindId> passed) {
    if (passed.contains(kindId)) {
      throw new RuntimeException("57E3ROB1ts :: kindId infinity circle " + passed);
    }
    passed.add(kindId);

    if (data.kindNames == null) {
      data.kindNames = new HashMap<>();
    }
    {
      String kindVarName = data.kindNames.get(kindId);
      if (kindVarName != null) {
        return kindVarName;
      }
    }

    List<String> defLines = data.typeDefLines();

    {
      Kind kind = program.translator.storage.getKindById(kindId);

      if (kind instanceof KindEmbed kn) {
        return kn.llvmName();
      }

      if (kind instanceof Kinds ks) {

        List<String> subKindNames = new ArrayList<>();

        for (final KindId subKindId : ks.subKinds) {
          subKindNames.add(getKindName0(subKindId, new HashSet<>(passed)));
        }

        String kindVarName = makeNewLocalName("struct");

        defLines.add(kindVarName + " = type {" + String.join(", ", subKindNames) + "};");
        data.kindNames.put(kindId, kindVarName);

        return kindVarName;
      }

      if (kind instanceof KindEmbedAlias kea) {

        @NonNull TrProgramGen       program     = this.program.translator.getOrLoadTrProgramGen(kea.srcId.programGenId);
        @NonNull TrBoxKindGenerator box         = (TrBoxKindGenerator) program.getOrLoadBoxById(kea.srcId.kgBoxId);
        @NonNull String             kindVarName = makeNewLocalName(box.data.code);

        defLines.add(kindVarName + " = type " + kea.kindEmbed.llvmName() + ";");
        data.kindNames().put(kindId, kindVarName);

        return kindVarName;
      }

      throw new RuntimeException("34dIVWu3BZ :: Not impl yet for " + kind.getClass().getSimpleName() + " = " + kind);

    }
  }

  public List<String> lines() {
    List<String> ret = new ArrayList<>();

    //noinspection CollectionAddAllCanBeReplacedWithConstructor
    ret.addAll(data.typeDefLines());
    ret.add("");
    ret.add("");

    functions.entrySet()
             .stream()
             .sorted(Map.Entry.comparingByKey())
             .map(Map.Entry::getValue)
             .forEachOrdered(funcLLVM -> {
               ret.addAll(funcLLVM.lines());
               ret.add("");
             });

    return ret;
  }

  private void putFunc(@NonNull FuncLlvmId funcId, @NonNull TrFuncLlvm funcLLVM) {

    functions.put(funcId, funcLLVM);

    Map<FuncLlvmId, FuncLlvm> x = data.functions;
    if (x == null) {
      x = data.functions = new HashMap<>();
    }

    x.put(funcId, funcLLVM.data);
  }

  public @NonNull TrFuncLlvm newFuncLlvm(@NonNull KindOp kindOp, @NonNull TrBoxOpGenerator trBoxOpGenerator) {

    var funcId = kindOp.funcLlvmId();

    if (functions.containsKey(funcId)) {
      throw new RuntimeException("yaIXZLeH5R :: Function with id = `" + funcId + "` was already registered");
    }

    TrFuncLlvm funcLLVM = new TrFuncLlvm(funcId, new FuncLlvm(), trBoxOpGenerator, this, kindOp);
    putFunc(funcId, funcLLVM);
    return funcLLVM;
  }

  public void err(Msg... msg) {
    program.diagnostic.list.add(DiaMessage.builder()
                                          .type(DiaType.ERR)
                                          .happenedObject(this)
                                          .msgParts(Arrays.stream(msg).toList())
                                          .build());
  }

  @Override public @NonNull Msg happenedMsg() {
    return Msg.of("I2V1rdX2Ll", "moduleId", id);
  }
}
