package kz.pompei.glazga.v3.types.etc;

import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode
@RequiredArgsConstructor
public class IDV<ID, V> {
  public final @NonNull ID id;
  public final @NonNull V  v;

  public static <ID, V> IDV<ID, V> idv(@NonNull ID id, @NonNull V v) {
    return new IDV<>(id, v);
  }

  @Override public String toString() {
    return "id=" + id + ",v=" + v;
  }
}
