package kz.pompei.glazga.v3.types.engine.script.got;

import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.box.TrBox;
import kz.pompei.glazga.v3.types.engine.translating.GotTranslatingExpression;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.got.GotRefVar;
import lombok.NonNull;

public class TrGotRefVar extends TrGot implements GotTranslatingExpression {
  public final @NonNull GotRefVar data;

  public TrGotRefVar(@NonNull BoxId ownerId, @NonNull GotId id, @NonNull GotRefVar data, @NonNull TrProgramGen translator) {
    super(ownerId, id, translator);
    this.data = data;
  }

  @Override public LocalVar translateExpression(int tab, @NonNull TrFuncLlvm funcLLVM) {

    BoxId varBoxId = data.varBoxId;
    if (varBoxId == null) {
      err(Msg.of("hlVmbQe7a4"));
      return null;
    }

    TrBox varDefBox = program.getOrLoadBoxById(varBoxId);
    if (varDefBox == null) {
      err(Msg.of("P1b7BRbD85", "varBoxId", varBoxId));
      return null;
    }

    LocalVar localVar = funcLLVM.boxVarNames().get(varBoxId);
    if (localVar == null) {
      err(Msg.of("IatKb0a1qv", "varBoxId", varBoxId));
      return null;
    }

    return localVar;
  }
}
