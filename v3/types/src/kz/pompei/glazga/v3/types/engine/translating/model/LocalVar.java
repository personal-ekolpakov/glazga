package kz.pompei.glazga.v3.types.engine.translating.model;

import kz.pompei.glazga.v3.types.engine.script.kind.Kind;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@RequiredArgsConstructor
public class LocalVar {
  public final @NonNull Kind   kind;
  public final @NonNull String name;

  public static @NonNull LocalVar of(@NonNull Kind kind, @NonNull String name) {
    return new LocalVar(kind, name);
  }
}
