package kz.pompei.glazga.v3.types.script.boxes.op;

import kz.pompei.glazga.v3.types.ids.script.GotId;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class ArgInDef {
  public GotId   kindGotId;
  public String  name;
  public double  order;
  public boolean self;
}
