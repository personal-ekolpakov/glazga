package kz.pompei.glazga.v3.types.engine.llvm.data;

import java.util.List;
import java.util.Set;
import kz.pompei.glazga.v3.types.engine.dia.DiaMessage;
import kz.pompei.glazga.v3.types.ids.script.ModuleId;
import kz.pompei.glazga.v3.types.ids.script.ProgramGenId;

public class ProgramTranslateResult {

  public Set<ModuleId>     moduleIds;
  public Set<ProgramGenId> dependenciesIds;
  public List<DiaMessage>  diagnostic;

}
