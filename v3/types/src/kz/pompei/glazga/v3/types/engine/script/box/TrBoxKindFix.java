package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.CalcStones;
import kz.pompei.glazga.v3.types.engine.Stones;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.engine.translating.HasStoneGets;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import kz.pompei.glazga.v3.types.script.boxes.BoxKindFix;
import lombok.NonNull;

public class TrBoxKindFix extends TrBox implements HasStoneGets {
  public final @NonNull BoxKindFix data;

  private final CalcStones calcStones = new CalcStones(this);

  public TrBoxKindFix(@NonNull BoxId id,
                      @NonNull BoxKindFix data,
                      @NonNull Map<GotId, TrGot> gotten,
                      @NonNull TrProgramGen program) {
    super(id, gotten, program);
    this.data = data;
  }

  public Stones getStones() {
    return calcStones.getStones();
  }

  @Override public Map<StoneKindCode, GotId> stoneKindGets() {
    return data.genArgTypes;
  }

  @Override public Map<StoneValueCode, GotId> stoneValuesGets() {
    return data.genArgValues;
  }
}
