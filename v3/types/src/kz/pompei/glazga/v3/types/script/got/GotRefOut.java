package kz.pompei.glazga.v3.types.script.got;

import kz.pompei.glazga.v3.types.ids.script.ArgOutCode;
import lombok.NonNull;

public class GotRefOut extends Got {
  public ArgOutCode outCode;

  public static GotRefOut code(@NonNull String outCode) {
    GotRefOut ret = new GotRefOut();
    ret.outCode = new ArgOutCode(outCode);
    return ret;
  }
}
