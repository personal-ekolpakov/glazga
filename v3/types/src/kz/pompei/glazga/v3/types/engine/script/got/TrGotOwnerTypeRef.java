package kz.pompei.glazga.v3.types.engine.script.got;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.CalcStones;
import kz.pompei.glazga.v3.types.engine.Stones;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.box.TrBox;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxKindGenerator;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxOpGenerator;
import kz.pompei.glazga.v3.types.engine.translating.GotExtractingKindId;
import kz.pompei.glazga.v3.types.engine.translating.HasStoneGets;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import kz.pompei.glazga.v3.types.script.got.GotOwnerTypeRef;
import lombok.NonNull;

public class TrGotOwnerTypeRef extends TrGot implements HasStoneGets, GotExtractingKindId {
  public final @NonNull GotOwnerTypeRef data;

  private final CalcStones calcStones = new CalcStones(this);

  public TrGotOwnerTypeRef(@NonNull BoxId ownerId,
                           @NonNull GotId id,
                           @NonNull GotOwnerTypeRef data,
                           @NonNull TrProgramGen program) {
    super(ownerId, id, program);
    this.data = data;
  }

  @Override public Map<StoneKindCode, GotId> stoneKindGets() {
    return data.stoneKindGets;
  }

  @Override public Map<StoneValueCode, GotId> stoneValuesGets() {
    return data.stoneValueGets;
  }

  @Override public KindId extrackKindId() {

    TrBox box = program.getOrLoadBoxById(ownerId);

    if (box == null) {
      err(Msg.of("J4H7L0XtbU", "ownerId", ownerId));
      return null;
    }

    TrBoxOpGenerator opGenerator = box.findMostUp(TrBoxOpGenerator.class);
    if (opGenerator == null) {
      err(Msg.of("z0iJDGiV9Q", "ownerId", ownerId, "class", TrBoxOpGenerator.class));
      return null;
    }

    TrBoxKindGenerator owner = opGenerator.owner();
    if (owner == null) {
      err(Msg.of("Hg1zB4NBEq"));
      return null;
    }

    Stones stones = calcStones.getStones();

    return owner.createKind(stones).id();
  }
}
