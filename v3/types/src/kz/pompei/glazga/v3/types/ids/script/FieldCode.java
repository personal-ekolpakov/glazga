package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.CodeWrapper;
import lombok.NonNull;

@StringRepresented
public class FieldCode extends CodeWrapper {

  public FieldCode(@NonNull String code) {
    super(code);
  }

  @StringRepresented
  public static FieldCode of(String code) {
    return code == null ? null : new FieldCode(code);
  }

}
