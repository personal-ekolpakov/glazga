package kz.pompei.glazga.v3.types.ids.type;


import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.CodeWrapper;
import lombok.NonNull;

@StringRepresented
public class StoneKindCode extends CodeWrapper {

  public StoneKindCode(@NonNull String code) {
    super(code);
  }

  @StringRepresented public static StoneKindCode of(String code) {
    return code == null ? null : new StoneKindCode(code);
  }

}
