package kz.pompei.glazga.v3.types.script.got;

import java.util.Map;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import lombok.NonNull;

/**
 * Это ссылка на тип, который определён только в этом программном генераторе
 */
public class GotRefKind extends Got {

  /**
   * Идентификатор генератора типа, который будет генерировать тип, с указанными в этом классе параметрами
   */
  public BoxId kindGeneratorBoxId;

  /**
   * По {@link BoxId} нужно из БД получить готовую структуру
   */
  public Map<StoneKindCode, GotId> stoneKindGets;

  /**
   * По {@link GotId} нужно получить из текущей детали
   */
  public Map<StoneValueCode, GotId> stoneValueGets;

  public static GotRefKind of(@NonNull String typeGeneratorId) {
    GotRefKind ret = new GotRefKind();
    ret.kindGeneratorBoxId = BoxId.of(typeGeneratorId);
    return ret;
  }

}
