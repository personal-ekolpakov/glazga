package kz.pompei.glazga.v3.types.etc;

import kz.pompei.glazga.v3.types.func.MemDump;
import lombok.NonNull;

public interface MemDumpDisplay {
  @NonNull String display(@NonNull MemDump memDump);
}
