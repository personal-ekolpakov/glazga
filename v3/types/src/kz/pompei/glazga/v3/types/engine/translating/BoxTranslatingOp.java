package kz.pompei.glazga.v3.types.engine.translating;

import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import lombok.NonNull;

/**
 * Данный интерфейс реализуется для элемента, который является исполнительной частью операции, с целью генерации LLVM-кода функции
 */
public interface BoxTranslatingOp {

  /**
   * Метод генерирующий LLVM-код
   *
   * @param funcLLVM объект, куда генерируется LLVM-код
   */
  void translateOp(@NonNull TrFuncLlvm funcLLVM);

}
