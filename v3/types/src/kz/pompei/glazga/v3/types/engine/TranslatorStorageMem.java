package kz.pompei.glazga.v3.types.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import kz.pompei.glazga.v3.types.engine.llvm.data.ModuleLlvm;
import kz.pompei.glazga.v3.types.engine.llvm.data.ProgramTranslateResult;
import kz.pompei.glazga.v3.types.engine.model.BoxKindFixList;
import kz.pompei.glazga.v3.types.engine.script.kind.Kind;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbed;
import kz.pompei.glazga.v3.types.etc.IDV;
import kz.pompei.glazga.v3.types.ids.script.ArrowId;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.ModuleId;
import kz.pompei.glazga.v3.types.ids.script.NativeId;
import kz.pompei.glazga.v3.types.ids.script.ProgramGenId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.script.ProgramGenerator;
import kz.pompei.glazga.v3.types.script.arrow.Arrow;
import kz.pompei.glazga.v3.types.script.arrow.ArrowType;
import kz.pompei.glazga.v3.types.script.boxes.Box;
import kz.pompei.glazga.v3.types.script.boxes.BoxKindFix;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TranslatorStorageMem implements TranslatorStorage {
  private final @NonNull NowSource nowSource;

  private final Map<ProgramGenId, ProgramGenerator>       programGenMap      = new HashMap<>();
  private final Map<ProgramGenId, Long>                   programGenModifies = new HashMap<>();
  private final Map<ModuleId, ModuleLlvm>                 modules            = new HashMap<>();
  private final Map<ProgramGenId, ProgramTranslateResult> programResults     = new HashMap<>();

  public void putProgramGen(ProgramGenId programGenId, ProgramGenerator pg) {
    programGenMap.put(programGenId, pg);
    programGenModifies.put(programGenId, nowSource.now());
  }

  @Override public ModuleLlvm loadModuleLlvm(@NonNull ModuleId id) {
    return modules.get(id);
  }

  @Override public void saveModuleLlvm(@NonNull ModuleId id, @NonNull ModuleLlvm moduleLlvm) {
    modules.put(id, moduleLlvm);
  }

  @Override public ProgramTranslateResult loadTranslateResult(@NonNull ProgramGenId programGenId) {
    return programResults.get(programGenId);
  }

  @Override public void saveTranslateResult(@NonNull ProgramGenId programGenId, @NonNull ProgramTranslateResult ptr) {
    programResults.put(programGenId, ptr);
  }

  @Override public List<IDV<ArrowId, Arrow>> loadTargetArrows(@NonNull BoxId sourceId,
                                                              @NonNull ArrowType arrowType) {

    List<IDV<ArrowId, Arrow>> ret = new ArrayList<>();

    for (final ProgramGenerator pg : programGenMap.values()) {
      if (pg == null) {
        return List.of();
      }
      for (final Map.Entry<ArrowId, Arrow> e : pg.arrows.entrySet()) {
        Arrow arrow = e.getValue();
        if (sourceId.equals(arrow.sourceId) && arrowType == arrow.type) {
          ret.add(new IDV<>(e.getKey(), arrow));
        }
      }
    }
    return ret;
  }

  @Override public BoxKindFixList loadProgramGenFixes(@NonNull ProgramGenId programGenId) {
    ProgramGenerator pg           = programGenMap.get(programGenId);
    Long             modifiedTime = programGenModifies.get(programGenId);
    if (pg == null || modifiedTime == null) {
      return null;
    }

    List<IDV<BoxId, BoxKindFix>> list = new ArrayList<>();

    for (final Map.Entry<BoxId, Box> e : pg.boxes.entrySet()) {
      if (e.getValue() instanceof final BoxKindFix btx) {
        list.add(IDV.idv(e.getKey(), btx));
      }
    }

    return new BoxKindFixList(List.copyOf(list), modifiedTime);
  }

  @Override public boolean existsProgramGen(@NonNull ProgramGenId programGenId) {
    return programGenMap.get(programGenId) != null;
  }

  @Override public @NonNull Map<BoxId, Box> loadBoxesByIds(@NonNull ProgramGenId programGenId, @NonNull Set<BoxId> boxIds) {

    ProgramGenerator pg = programGenMap.get(programGenId);
    if (pg == null) {
      return Map.of();
    }

    Map<BoxId, Box> ret = new HashMap<>();

    for (final BoxId boxId : boxIds) {
      Box box = pg.boxes.get(boxId);
      if (box != null) {
        ret.put(boxId, box);
      }
    }

    return ret;
  }


  public final Map<@NonNull NativeId, @NonNull Native> nativeMap = new HashMap<>();

  @Override public @NonNull Native getNativeById(@NonNull NativeId nativeId) {
    return nativeMap.get(nativeId);
  }

  public void registerNative(@NonNull Native newNative) {
    Native existsNative = nativeMap.get(newNative.id());
    if (existsNative != null) {
      if (Objects.equals(existsNative, newNative)) {
        return;
      }
      throw new RuntimeException("XPWr1akXiV :: Adding different natives with the same ID: " + existsNative + " ; " + newNative);
    }
    nativeMap.put(newNative.id(), newNative);
  }


  private final Map<KindId, Kind> kindsById = new HashMap<>();


  @Override public @NonNull Kind getKindById(@NonNull KindId id) {

    {
      KindEmbed kindEmbed = KindEmbed.byId(id).orElse(null);
      if (kindEmbed != null) {
        return kindEmbed;
      }
    }

    return Optional.of(kindsById).map(x -> x.get(id)).orElseThrow();
  }

  @Override public void ensureSavedKind(@NonNull Kind kind) {
    if (kind instanceof KindEmbed) {
      return;
    }
    kindsById.put(kind.id(), kind);
  }
}
