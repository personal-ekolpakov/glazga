package kz.pompei.glazga.v3.types.engine.dia.lang;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.SneakyThrows;

import static java.util.Objects.requireNonNull;

public class DiaLangMessageTemplates {
  private static final DiaLangMessageTemplates RUS = new DiaLangMessageTemplates("rus.txt");

  private final Map<String, String> messages;

  @SneakyThrows
  private DiaLangMessageTemplates(@NonNull String resourceName) {
    try (InputStream resourceAsStream = getClass().getResourceAsStream(resourceName)) {
      requireNonNull(resourceAsStream);

      messages = new String(resourceAsStream.readAllBytes(), StandardCharsets.UTF_8)
        .lines()
        .map(this::splitToEntry)
        .filter(Objects::nonNull)
        .collect(Collectors.toUnmodifiableMap(Map.Entry::getKey, Map.Entry::getValue));

    }
  }

  private Map.Entry<String, String> splitToEntry(@NonNull String line) {
    int i = line.indexOf('=');
    if (i < 0) {
      return null;
    }
    return Map.entry(line.substring(0, i).trim(), line.substring(i + 1).trim());
  }

  public static DiaLangMessageTemplates msgTemplates() {
    return RUS;
  }

  public @NonNull String get(@NonNull String msgId) {
    return requireNonNull(messages.get(msgId), "6EeZmS3kvr :: No message template with id = " + msgId);
  }
}
