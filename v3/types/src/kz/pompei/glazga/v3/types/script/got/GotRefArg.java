package kz.pompei.glazga.v3.types.script.got;

import kz.pompei.glazga.v3.types.ids.script.ArgInCode;
import lombok.NonNull;

public class GotRefArg extends Got {
  public ArgInCode argCode;

  public static GotRefArg code(@NonNull String argCode) {
    GotRefArg ret = new GotRefArg();
    ret.argCode = ArgInCode.of(argCode);
    return ret;
  }
}
