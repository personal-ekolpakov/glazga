package kz.pompei.glazga.v3.types.script.boxes.kind_gen;

import kz.pompei.glazga.v3.types.ids.script.GotId;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class StoneValueDefine {
  public String name;
  public double order;
  public GotId  valueGotId;
}
