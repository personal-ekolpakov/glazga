package kz.pompei.glazga.v3.types.engine.script.got;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.kind.Kind;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbed;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbedType;
import kz.pompei.glazga.v3.types.engine.script.kind.KindUtil;
import kz.pompei.glazga.v3.types.engine.translating.GotTranslatingExpression;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.script.NativeArgCode;
import kz.pompei.glazga.v3.types.script.got.GotCallNative;
import lombok.NonNull;

public class TrGotCallNative extends TrGot implements GotTranslatingExpression {
  public final @NonNull GotCallNative data;

  public TrGotCallNative(@NonNull BoxId ownerId, @NonNull GotId id, @NonNull GotCallNative data, @NonNull TrProgramGen translator) {
    super(ownerId, id, translator);
    this.data = data;
  }

  @Override public LocalVar translateExpression(int tab, @NonNull TrFuncLlvm funcLLVM) {
    String code = data.code.code;

    if ("LLVM_ADD".equals(code)) {
      return tr__binaryOp__EqTypes(tab, "add", funcLLVM, null);
    }

    if (code.startsWith("LLVM_ICMP_")) {
      String subtype = code.substring("LLVM_ICMP_".length()).toLowerCase();
      return tr__binaryOp__EqTypes(tab, "icmp " + subtype, funcLLVM, KindEmbed.of(KindEmbedType.INT, 1));

    }

    err(Msg.of("jHaa8Rr27s", "code", data.code));
    return null;
  }

  private LocalVar translateArg(int tab, @NonNull String argCodeStr, @NonNull TrFuncLlvm funcLLVM) {
    Map<NativeArgCode, GotId> args = data.args;
    if (args == null) {
      err(Msg.of("goUlPOa9gL"));
      return null;
    }

    NativeArgCode argCode  = NativeArgCode.of(argCodeStr);
    GotId         argGotId = args.get(argCode);
    return translateGotId(argGotId, Msg.of("1jE0CYJ0F0", "code", argCode), this::getTrGotById, tab + 1, funcLLVM);
  }

  @Override public @NonNull Msg happenedMsg() {
    return Msg.of("Ob4OYo1kyg", "class", getClass(), "code", data.code, "id", id);
  }

  private LocalVar tr__binaryOp__EqTypes(int tab, @NonNull String llvmOp, @NonNull TrFuncLlvm funcLLVM, Kind returnKind) {
    Map<NativeArgCode, GotId> args = data.args;
    if (args == null) {
      err(Msg.of("cX9JhYRIbF"));
      return null;
    }

    LocalVar leftVar  = translateArg(tab + 1, "LEFT", funcLLVM);
    LocalVar rightVar = translateArg(tab + 1, "RIGHT", funcLLVM);

    if (leftVar == null) {
      err(Msg.of("Gw1e0N0rIC"));
      return null;
    }
    if (rightVar == null) {
      err(Msg.of("zKZmtDOk9u"));
      return null;
    }

    if (!KindUtil.eq(leftVar.kind, rightVar.kind)) {
      err(Msg.of("qBnO4A3cq9",
                 "code", data.code,
                 "left", leftVar.kind.displayStr(program.translator),
                 "right", rightVar.kind.displayStr(program.translator)));
    }

    @NonNull String kindName = funcLLVM.owner.getKindName(leftVar.kind.id());
    @NonNull String varName  = funcLLVM.makeNewLocalName1(data.code.code + ".");

    funcLLVM.lines().add(ta(tab) + "; 6dAoVBACv2 " + data.code);
    funcLLVM.lines().add(ta(tab) + varName + " = " + llvmOp + " " + kindName + " " + leftVar.name + ", " + rightVar.name);

    return LocalVar.of(returnKind == null ? leftVar.kind : returnKind, varName);
  }

}
