package kz.pompei.glazga.v3.types.script.arrow;

import kz.pompei.glazga.v3.types.script.boxes.BoxIf;
import kz.pompei.glazga.v3.types.script.boxes.BoxReturn;

public enum ArrowType {
  /**
   * Эта стрелка соединяет последовательность исполнения блоков.
   * <p>
   * Если исходный блок {@link BoxIf}, то данная стрелка обозначает выход на случай когда все внутренние условия исполнятся.
   * <p>
   * Из блока {@link BoxReturn} данная стрелка исходить не может
   */
  DOWN,

  /**
   * Эта стрелка соединяет бокс, в котором есть выражение с боксом типа BoxAlter альтернативного исполнения,
   * на случай, если операция завершилась с ошибкой (точнее с альтернативным результатом)
   */
  GOT_OUT,

  /**
   * Исходным боксом данной стрелки может быть только
   */
  IF_THEN,

  IF_ELSE,
}
