package kz.pompei.glazga.v3.types.engine.linking;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kz.pompei.glazga.utils.FileUtils;
import kz.pompei.glazga.utils.RunResult;
import kz.pompei.glazga.v3.types.engine.dia.CmdOutput;
import kz.pompei.glazga.v3.types.engine.dia.DiaMessage;
import kz.pompei.glazga.v3.types.engine.dia.DiaType;
import kz.pompei.glazga.v3.types.engine.dia.HappenedObject;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.data.ModuleLlvm;
import kz.pompei.glazga.v3.types.etc.IDV;
import kz.pompei.glazga.v3.types.ids.script.ModuleId;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.joining;

public class Linking implements HappenedObject {

  private final @NonNull Path buildDir;

  private final List<DiaMessage> messages = new ArrayList<>();

  public Linking(Path buildDir) {
    this.buildDir = buildDir;
  }

  private void err(Msg... msg) {
    messages.add(DiaMessage.builder()
                           .type(DiaType.ERR)
                           .happenedObject(this)
                           .msgParts(Arrays.stream(msg).toList())
                           .build());
  }

  @Override public @NonNull Msg happenedMsg() {
    return Msg.of("6Tbd0QLPvi");
  }

  private LinkResult withErrors() {
    LinkResult ret = new LinkResult();
    ret.messages = List.copyOf(messages);
    if (ret.messages.isEmpty()) {
      throw new RuntimeException("SIIpdJEI6z :: Нет ошибок, а предполагается, что есть");
    }
    return ret;
  }

  @RequiredArgsConstructor
  public static class FilesLO {
    public final String fileLL;
    public final String fileO;
  }

  @SneakyThrows
  public LinkResult linkTests(List<IDV<ModuleId, ModuleLlvm>> modules) {
    FileUtils.removeFile(buildDir);

    List<String> llFiles = new ArrayList<>();

    for (final IDV<ModuleId, ModuleLlvm> module : modules) {
      String name = module.id.progId.id + "-" + module.id.profile.name().toLowerCase() + ".ll";
      llFiles.add(name);
      Path file = buildDir.resolve(name);
      file.toFile().getParentFile().mkdirs();
      Files.write(file, module.v.lines(), UTF_8);
    }

    if (llFiles.isEmpty()) {
      err(Msg.of("6krU45f24z"));
      return withErrors();
    }

    List<FilesLO> files         = llFiles.stream().map(nameLL -> new FilesLO(nameLL, nameLL + ".o")).toList();
    String        oFileNames    = files.stream().map(x -> x.fileO).collect(joining(" "));
    String        outSoFileName = "test.so";

    StringBuilder mf = new StringBuilder();
    mf.append(outSoFileName + ": " + oFileNames + "\n");
    mf.append("\tgcc -shared -o " + outSoFileName + " " + oFileNames + "\n");
    mf.append("\n");

    for (final FilesLO ff : files) {
      mf.append(ff.fileO + ": " + ff.fileLL + "\n");
      mf.append("\tllc -relocation-model=pic -filetype=obj " + ff.fileLL + " -o " + ff.fileO + "\n");
      mf.append("\n");
    }

    Files.writeString(buildDir.resolve("Makefile"), mf, UTF_8);

    final @NonNull RunResult rr = FileUtils.runCmd(buildDir, List.of("make"));

    if (rr.exitCode != 0) {
      err(Msg.of("8mL9pi1x6T", "exitCode", rr.exitCode),
          Msg.of("vu845Ce39F", "output", CmdOutput.of(rr.out)),
          Msg.of("64IH1lyLHk", "output", CmdOutput.of(rr.err))
      );
      return withErrors();
    }

    LinkResult linkResult = new LinkResult();
    linkResult.soFile = buildDir.resolve(outSoFileName);
    return linkResult;
  }
}
