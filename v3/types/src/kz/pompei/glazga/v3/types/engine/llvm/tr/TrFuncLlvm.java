package kz.pompei.glazga.v3.types.engine.llvm.tr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.v3.types.engine.dia.DiaMessage;
import kz.pompei.glazga.v3.types.engine.dia.DiaType;
import kz.pompei.glazga.v3.types.engine.dia.HappenedObject;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.data.FuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.box.TrBox;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxOpGenerator;
import kz.pompei.glazga.v3.types.engine.script.kind.Kind;
import kz.pompei.glazga.v3.types.engine.script.kind.KindOp;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.ArgInCode;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.FuncLlvmId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.NonNull;

public class TrFuncLlvm extends TrParentLlvm implements HappenedObject {
  public final @NonNull FuncLlvmId       funcId;
  public final @NonNull FuncLlvm         data;
  public final @NonNull TrBoxOpGenerator opGenerator;
  public final @NonNull TrModuleLlvm     owner;
  public final @NonNull KindOp           kindOp;

  public TrFuncLlvm(@NonNull FuncLlvmId funcId,
                    @NonNull FuncLlvm data,
                    @NonNull TrBoxOpGenerator opGenerator,
                    @NonNull TrModuleLlvm owner,
                    @NonNull KindOp kindOp) {
    this.funcId      = funcId;
    this.data        = data;
    this.opGenerator = opGenerator;
    this.owner       = owner;
    this.kindOp      = kindOp;
  }

  public LocalVar getArgVar(@NonNull ArgInCode code) {

    if (data.argVars == null) {
      data.argVars = new HashMap<>();
    }

    {
      LocalVar varName = data.argVars.get(code);
      if (varName != null) {
        return varName;
      }
    }

    KindId argKindId = kindOp.args.stream().filter(p -> code.equals(p.code)).findAny().map(x -> x.kindId).orElse(null);

    if (argKindId == null) {
      err(Msg.of("q4Lt535dGq", "code", code));
      return null;
    }

    @NonNull Kind argKind = opGenerator.program.storage.getKindById(argKindId);

    {
      String   varName  = makeNewLocalName("arg_" + code.code);
      LocalVar localVar = LocalVar.of(argKind, varName);
      data.argVars.put(code, localVar);
      return localVar;
    }
  }

  public void err(Msg... msg) {
    opGenerator.program.diagnostic.list.add(DiaMessage.builder()
                                                      .type(DiaType.ERR)
                                                      .happenedObject(this)
                                                      .msgParts(Arrays.stream(msg).toList())
                                                      .build());
  }


  @Override protected @NonNull Set<String> variableNames() {
    if (data.variableNames == null) {
      return data.variableNames = new HashSet<>();
    }
    return data.variableNames;
  }

  public List<String> lines() {
    List<String> lines = data.lines;
    if (lines != null) {
      return lines;
    }
    return data.lines = new ArrayList<>();
  }

  public Map<BoxId, LocalVar> boxVarNames() {
    Map<BoxId, LocalVar> x = data.boxLocalVars;
    if (x != null) {
      return x;
    }
    return data.boxLocalVars = new HashMap<>();
  }

  public void registerBoxLabel(@NonNull BoxId boxId, @NonNull String label) {

    if (data.boxLabels == null) {
      data.boxLabels = new HashMap<>();
    }

    String existLabel = data.boxLabels.get(boxId);

    if (existLabel != null) {
      if (existLabel.equals(label)) {
        return;
      }

      err(Msg.of("fX2I0uHTp3", "newLabel", label, "existLabel", existLabel));
    }

    data.boxLabels.put(boxId, label);
  }

  public @NonNull String getBoxLabel(@NonNull TrBox trBox) {

    {
      String boxLabel = data.boxLabels == null ? null : data.boxLabels.get(trBox.id);
      if (boxLabel != null) {
        return boxLabel;
      }
    }

    @NonNull String label = makeNewLocalName1(trBox.getClass().getSimpleName() + ".").substring(1);
    registerBoxLabel(trBox.id, label);
    return label;
  }

  @Override public @NonNull Msg happenedMsg() {
    return Msg.of("zkP0RvMdOy", "funcId", funcId);
  }
}
