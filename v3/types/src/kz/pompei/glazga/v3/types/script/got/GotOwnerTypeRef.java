package kz.pompei.glazga.v3.types.script.got;

import java.util.Map;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;

/**
 * Ссылка на тип владельца операции, в которой определяется текущая программа
 */
public class GotOwnerTypeRef extends Got {

  /**
   * По {@link GotId} нужно из БД получить готовую структуру
   */
  public Map<StoneKindCode, GotId> stoneKindGets;

  /**
   * По {@link GotId} нужно получить из текущей детали
   */
  public Map<StoneValueCode, GotId> stoneValueGets;

  public static GotOwnerTypeRef of() {
    return new GotOwnerTypeRef();
  }

}
