package kz.pompei.glazga.v3.types.func;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Base64;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode
@RequiredArgsConstructor
public class MemDump {
  @SuppressWarnings("NullableProblems")
  private final @NonNull byte[] data;

  public byte[] data() {
    byte[] ret = new byte[data.length];
    System.arraycopy(data, 0, ret, 0, data.length);
    return ret;
  }

  public ByteBuffer buffer() {
    return ByteBuffer.wrap(data);
  }

  public static MemDump fromInt_4bytes(int source) {
    ByteBuffer buffer = ByteBuffer.allocate(4);
    buffer.order(ByteOrder.LITTLE_ENDIAN);
    buffer.putInt(source);
    buffer.flip();
    return new MemDump(buffer.array());
  }

  @Override public String toString() {
    return "MemDump{" + Base64.getEncoder().encodeToString(data) + '}';
  }
}
