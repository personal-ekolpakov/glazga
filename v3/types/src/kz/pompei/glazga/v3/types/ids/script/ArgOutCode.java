package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.CodeWrapper;
import lombok.NonNull;

@StringRepresented
public class ArgOutCode extends CodeWrapper {

  public ArgOutCode(@NonNull String code) {
    super(code);
  }

  @StringRepresented public static ArgOutCode of(String code) {
    return code == null ? null : new ArgOutCode(code);
  }

}
