package kz.pompei.glazga.v3.types.engine;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.v3.types.engine.llvm.data.ProgramTranslateResult;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrModuleLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.ids.script.ProgramGenId;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static java.util.stream.Collectors.toUnmodifiableSet;

@RequiredArgsConstructor
public class Translator {
  public final @NonNull TranslatorStorage storage;
  public final @NonNull NowSource         nowSource;

  private final Map<ProgramGenId, TrProgramGen> programGens = new HashMap<>();

  public TrProgramGen getOrLoadTrProgramGen(@NonNull ProgramGenId programGenId) {
    {
      TrProgramGen x = programGens.get(programGenId);
      if (x != null) {
        return x;
      }
    }
    if (!storage.existsProgramGen(programGenId)) {
      return null;
    }
    {
      TrProgramGen x = new TrProgramGen(programGenId, storage, this);
      programGens.put(programGenId, x);
      return x;
    }
  }

  public void translateKindFixes(Set<ProgramGenId> programGenIds) {

    for (final ProgramGenId programGenId : programGenIds) {

      TrProgramGen trProgramGen = getOrLoadTrProgramGen(programGenId);

      if (trProgramGen == null) {
        throw new RuntimeException("EyjMD1AgGr :: No ProgramGen found for ProgramGenId " + programGenId);
      }

      trProgramGen.translateKindFixes();

      ProgramTranslateResult ptr = new ProgramTranslateResult();
      ptr.diagnostic = trProgramGen.diagnostic.list;
      ptr.moduleIds  = trProgramGen.modules().stream().map(x -> x.id).collect(toUnmodifiableSet());

      for (final TrModuleLlvm module : trProgramGen.modules()) {
        storage.saveModuleLlvm(module.id, module.data);
      }

      storage.saveTranslateResult(programGenId, ptr);
    }
  }

}
