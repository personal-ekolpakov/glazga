package kz.pompei.glazga.v3.types.engine.script.kind;

import java.security.MessageDigest;
import java.util.List;
import java.util.stream.Collectors;
import kz.pompei.glazga.v3.calc.Once;
import kz.pompei.glazga.v3.types.engine.Translator;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.NonNull;
import lombok.SneakyThrows;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Kinds extends Kind {
  public final @NonNull List<KindId> subKinds;

  public Kinds(@NonNull List<KindId> subKinds) {
    this.subKinds = List.copyOf(subKinds);
  }

  private final Once<KindId> idOnce = new Once<>(this::calcId);

  @Override @SneakyThrows public KindId id() {
    return idOnce.get();
  }

  private @SneakyThrows KindId calcId() {

    MessageDigest md = MessageDigest.getInstance("SHA-1");
    md.update(getClass().getSimpleName().getBytes(UTF_8));

    int i = 1;

    for (final KindId kindId : subKinds) {
      md.update(("" + i++).getBytes(UTF_8));
      md.update(kindId.id.getBytes(UTF_8));
    }

    return KindId.of(KindUtil.bytesToId(md.digest()));
  }

  @Override public @NonNull String displayStr(@NonNull Translator translator) {
    return getClass().getSimpleName()
           + "{"
           + subKinds.stream()
                     .map(kindId -> translator.storage.getKindById(kindId).displayStr(translator))
                     .collect(Collectors.joining(", "))
           + "}";
  }

  @Override public String toString() {
    return getClass().getSimpleName() + "{" + subKinds.size() + "}";
  }
}
