package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrAbstract;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.engine.translating.BoxTranslatingOp;
import kz.pompei.glazga.v3.types.ids.script.BotId;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.arrow.ArrowType;
import lombok.NonNull;

public abstract class TrBox extends TrAbstract {
  public final @NonNull BoxId             id;
  public final @NonNull Map<GotId, TrGot> gotten;

  private TrBox up;

  public TrBox up() {
    return up;
  }

  public final void defineUp(@NonNull TrBox up) {
    this.up = up;
  }

  @Override public @NonNull BotId extractId() {
    return id;
  }

  protected TrBox(@NonNull BoxId id, @NonNull Map<GotId, TrGot> gotten, @NonNull TrProgramGen program) {
    super(program);
    this.id     = id;
    this.gotten = gotten;
  }

  protected void downAndTranslateOp(@NonNull TrFuncLlvm funcLLVM, boolean printBrLabel) {
    List<TrBox> downBoxes = program.getAttachedBoxes(id, ArrowType.DOWN);

    if (downBoxes.isEmpty()) {
      err(Msg.of("ZKUkTaAeL9", "arrowType", ArrowType.DOWN, "fromId", id));
      return;
    }

    if (downBoxes.size() > 1) {
      err(Msg.of("09OViQAHk9", "arrowType", ArrowType.DOWN, "fromId", id, "toIds", "" + downBoxes.stream().map(x -> x.id).toList()));
      return;
    }

    followTranslateOp0(downBoxes.get(0), Msg.of("CGD7VqIYaf"), funcLLVM, printBrLabel);

  }

  private void followTranslateOp0(@NonNull TrBox downBox,
                                        Msg srcPlace,
                                        @NonNull TrFuncLlvm funcLLVM,
                                        boolean printBrLabel) {
    if (downBox.up == null) {
      downBox.defineUp(this);
    } else if (!Objects.equals(downBox.up.id, id)) {
      err(Msg.of("83697M31Dp", "downBoxId", downBox.id, "upBoxId", downBox.up.id, "id", id), srcPlace);
      return;
    }

    if (!(downBox instanceof BoxTranslatingOp trOp)) {
      err(Msg.of("9D1RKAvyPh", "interface", BoxTranslatingOp.class, "class", downBox.getClass()), srcPlace);
      return;
    }

    if (printBrLabel) {
      funcLLVM.lines().add("  br label %" + funcLLVM.getBoxLabel(downBox));
    }

    trOp.translateOp(funcLLVM);
  }

  protected final void followTranslateOp(@NonNull TrBox downBox, Msg srcPlace, @NonNull TrFuncLlvm funcLLVM) {
    followTranslateOp0(downBox, srcPlace, funcLLVM, false);
  }

  public <T extends TrBox> T findMostUp(Class<T> trBoxClass) {

    TrBox current = this;

    Set<BoxId> passedIds = new HashSet<>();

    while (true) {

      if (passedIds.contains(current.id)) {
        throw new RuntimeException("WQgQ1V5NPA :: Inf-circle backDown boxes");
      }
      passedIds.add(current.id);

      if (trBoxClass.isInstance(current)) {
        return trBoxClass.cast(current);
      }

      TrBox back = current.up();

      if (back != null) {
        current = back;
        continue;
      }

      err(Msg.of("gOonYKb66Q", "currentBoxId", current.id, "currentClass", current.getClass()));
      return null;
    }
  }

}
