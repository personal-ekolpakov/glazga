package kz.pompei.glazga.v3.types.script.arrow;

import kz.pompei.glazga.v3.types.ids.script.BoxId;

public class Arrow {
  public BoxId     sourceId;
  public BoxId     targetId;
  public ArrowType type;

}
