package kz.pompei.glazga.v3.types.engine.dia;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.v3.types.etc.IDV;
import kz.pompei.glazga.v3.types.ids.script.ArrowId;
import kz.pompei.glazga.v3.types.script.arrow.Arrow;

public class Diagnostic {
  public final List<DiaMessage> list = new ArrayList<>();

  public void noTargetBoxOfArrow(IDV<ArrowId, Arrow> ignore) {
    list.add(DiaMessage.builder()
                       .type(DiaType.ERR)
                       .build());
  }

}
