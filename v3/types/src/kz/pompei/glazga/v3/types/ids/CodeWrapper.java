package kz.pompei.glazga.v3.types.ids;

import java.util.Objects;
import kz.pompei.glazga.convert.StringRepresented;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class CodeWrapper implements Comparable<CodeWrapper> {
  public final @NonNull String code;

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final CodeWrapper other = (CodeWrapper) o;
    return Objects.equals(code, other.code);
  }

  @Override public int compareTo(CodeWrapper o) {
    return code.compareTo(o.code);
  }

  @Override public int hashCode() {
    return Objects.hashCode(code);
  }

  @Override public String toString() {
    return getClass().getSimpleName() + '(' + code + ')';
  }

  @StringRepresented public String code() {
    return code;
  }
}
