package kz.pompei.glazga.v3.types.engine.script.kind;

import kz.pompei.glazga.v3.types.engine.Translator;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class KindVoid extends Kind {

  @Override public KindId id() {
    return KindId.of("void");
  }

  @Override public @NonNull String displayStr(@NonNull Translator translator) {
    throw new RuntimeException("2024-06-13 20:09 Not impl yet KindVoid.displayStr()");
  }
}
