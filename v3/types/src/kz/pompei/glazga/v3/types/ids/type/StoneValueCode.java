package kz.pompei.glazga.v3.types.ids.type;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.CodeWrapper;
import lombok.NonNull;

@StringRepresented
public class StoneValueCode extends CodeWrapper {

  public StoneValueCode(@NonNull String code) {
    super(code);
  }

  @StringRepresented
  public static StoneValueCode of(String code) {
    return code == null ? null : new StoneValueCode(code);
  }

}
