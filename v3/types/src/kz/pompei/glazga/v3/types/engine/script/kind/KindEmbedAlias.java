package kz.pompei.glazga.v3.types.engine.script.kind;

import java.security.MessageDigest;
import kz.pompei.glazga.v3.calc.Once;
import kz.pompei.glazga.v3.types.engine.Translator;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxKindGenerator;
import kz.pompei.glazga.v3.types.ids.script.KindGeneratorId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.Builder;
import lombok.NonNull;
import lombok.SneakyThrows;

import static java.nio.charset.StandardCharsets.UTF_8;

@Builder
public class KindEmbedAlias extends Kind {
  public final @NonNull KindGeneratorId srcId;
  public final @NonNull KindEmbed       kindEmbed;

  private final Once<KindId> idOnce = new Once<>(this::calcId);

  @Override @SneakyThrows public KindId id() {
    return idOnce.get();
  }

  private @SneakyThrows KindId calcId() {
    MessageDigest md = MessageDigest.getInstance("SHA-1");
    md.update("sShAJ2cLKt".getBytes(UTF_8));
    KindUtil.updateKindGenId(md, srcId);
    KindUtil.updateKindEmbed(md, kindEmbed);
    return KindId.of(KindUtil.bytesToId(md.digest()));
  }

  @Override public @NonNull String displayStr(@NonNull Translator translator) {

    @NonNull TrProgramGen myProgram = translator.getOrLoadTrProgramGen(srcId.programGenId);
    myProgram.getOrLoadBoxById(srcId.kgBoxId);

    var box = (TrBoxKindGenerator) myProgram.getOrLoadBoxById(srcId.kgBoxId);
    return box.data.name;
  }


}
