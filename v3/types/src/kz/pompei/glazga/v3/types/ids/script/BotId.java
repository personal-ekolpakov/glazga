package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.v3.types.ids.IdWrapper;
import lombok.NonNull;

public abstract class BotId extends IdWrapper {
  public BotId(@NonNull String id) {
    super(id);
  }
}
