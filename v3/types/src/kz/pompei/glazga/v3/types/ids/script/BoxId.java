package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import lombok.NonNull;

@StringRepresented
public class BoxId extends BotId {

  public BoxId(@NonNull String id) {
    super(id);
  }

  @StringRepresented
  public static BoxId of(String id) {
    return id == null ? null : new BoxId(id);
  }

}
