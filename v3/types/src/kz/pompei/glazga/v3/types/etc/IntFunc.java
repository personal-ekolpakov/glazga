package kz.pompei.glazga.v3.types.etc;

import java.util.Map;
import kz.pompei.glazga.v3.types.func.StoneValue;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import lombok.NonNull;

public interface IntFunc {
  int apply(@NonNull Map<@NonNull StoneKindCode, @NonNull KindId> argTypes,
            @NonNull Map<@NonNull StoneValueCode, @NonNull StoneValue> argValues);
}
