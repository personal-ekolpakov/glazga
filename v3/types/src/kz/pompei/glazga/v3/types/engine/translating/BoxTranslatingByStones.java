package kz.pompei.glazga.v3.types.engine.translating;

import kz.pompei.glazga.v3.types.engine.Stones;
import kz.pompei.glazga.v3.types.engine.script.kind.KindOp;
import lombok.NonNull;

public interface BoxTranslatingByStones {
  @NonNull KindOp translateByStones(@NonNull Stones stones);
}
