package kz.pompei.glazga.v3.types.script.boxes;

import java.util.List;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import lombok.NonNull;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class BoxGot extends Box {
  public GotId gotId;

  public static BoxGot of(String gotId) {
    BoxGot ret = new BoxGot();
    ret.gotId = GotId.of(gotId);
    return ret;
  }

  @Override protected @NonNull List<String> listToDisplay() {
    return List.of(Fields.gotId + '=' + gotId);
  }
}
