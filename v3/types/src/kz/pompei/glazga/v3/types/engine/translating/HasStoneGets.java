package kz.pompei.glazga.v3.types.engine.translating;

import java.util.Map;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;

public interface HasStoneGets {

  Map<StoneKindCode, GotId> stoneKindGets();

  Map<StoneValueCode, GotId> stoneValuesGets();

}
