package kz.pompei.glazga.v3.types.script.boxes.op;

import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.got.GotCallOp;
import lombok.ToString;

/**
 * Определяет структуру выходного аргумент операции
 */
@ToString
public class ArgOutDef {

  /**
   * Идентификатор выражения определяющего тип величины выходного значения
   * <p>
   * Вычисляет тип на уровне компиляции
   */
  public GotId valueKindGotId;

  /**
   * Имя выходного аргумента
   */
  public String argumentName;

  /**
   * Порядок данного выходного аргумента среди других в этой операции
   */
  public double order;

  /**
   * Признак того, что этот выход приводит к главному пути исполнения.
   * <p>
   * <code>true</code> - это главный путь.
   * <p>
   * <code>false</code> - этот путь должен быть заранее определён в поле {@link GotCallOp#outs}.
   */
  public boolean master;

}
