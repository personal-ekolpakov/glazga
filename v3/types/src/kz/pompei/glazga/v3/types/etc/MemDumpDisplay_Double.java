package kz.pompei.glazga.v3.types.etc;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kz.pompei.glazga.v3.types.func.MemDump;
import lombok.NonNull;

public class MemDumpDisplay_Double implements MemDumpDisplay {

  private MemDumpDisplay_Double() {}

  public static final MemDumpDisplay_Double I = new MemDumpDisplay_Double();

  @Override public @NonNull String display(@NonNull MemDump memDump) {
    return "" + extractDouble(memDump);
  }

  public static double extractDouble(@NonNull MemDump memDump) {
    ByteBuffer buffer = ByteBuffer.wrap(memDump.data());
    buffer.order(ByteOrder.LITTLE_ENDIAN);
    return buffer.getDouble();
  }
}
