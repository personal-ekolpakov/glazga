package kz.pompei.glazga.v3.types.script.boxes;

import java.util.List;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import lombok.NonNull;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class BoxIf extends Box {

  public GotId conditionGotId;

  @Override protected @NonNull List<String> listToDisplay() {
    return List.of(Fields.conditionGotId + '=' + conditionGotId);
  }

  public static BoxIf condition(String conditionGotId) {
    BoxIf ret = new BoxIf();
    ret.conditionGotId = GotId.of(conditionGotId);
    return ret;
  }
}
