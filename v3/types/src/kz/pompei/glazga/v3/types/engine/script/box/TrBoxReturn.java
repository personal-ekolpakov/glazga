package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.FuncLlvmHelper;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.engine.script.kind.ArgOutCode_KindId;
import kz.pompei.glazga.v3.types.engine.translating.BoxTranslatingOp;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.boxes.BoxReturn;
import lombok.NonNull;

public class TrBoxReturn extends TrBox implements BoxTranslatingOp {
  public final @NonNull BoxReturn data;

  public TrBoxReturn(@NonNull BoxId id, @NonNull BoxReturn data, @NonNull Map<GotId, TrGot> gotten, @NonNull TrProgramGen program) {
    super(id, gotten, program);
    this.data = data;
  }

  @Override public void translateOp(@NonNull TrFuncLlvm funcLLVM) {

    List<ArgOutCode_KindId> outs  = funcLLVM.kindOp.outs;
    List<String>            lines = funcLLVM.lines();

    String label = funcLLVM.getBoxLabel(this);
    lines.add("");
    lines.add(label + ":   ; KiwHyOe1sY " + happenedMsg().message());

    if (outs.isEmpty()) {
      lines.add("  ret void;");
      return;
    }

    LocalVar localVar = translateGotId(data.outGotId, Msg.of("vIp0ZtTVmC", "outGotId", data.outGotId), gotten::get, 1, funcLLVM);
    if (localVar == null) {
      err(Msg.of("tUO1W2VRtD"));
      return;
    }

    TrBoxOpGenerator opGenerator = findMostUp(TrBoxOpGenerator.class);
    if (opGenerator == null) {
      err(Msg.of("kZWhBytXCU", "class", TrBoxOpGenerator.class));
      return;
    }

    FuncLlvmHelper helper         = new FuncLlvmHelper(funcLLVM);
    String         returnKindName = helper.returnKindName(outs);

    if (outs.size() == 1) {
      lines.add("  ret " + returnKindName + " " + localVar.name);
      return;
    }

    Integer index = null;

    for (int i = 0; i < outs.size(); i++) {
      if (Objects.equals(outs.get(i).code, data.outCode)) {
        index = i;
        break;
      }
    }

    if (index == null) {
      err(Msg.of("2KXNKbO1IN", "outCode", data.outCode));
      return;
    }

    ArgOutCode_KindId selectedOut         = outs.get(index);
    String            selectedOutKindName = funcLLVM.owner.getKindName(selectedOut.kindId);


    String retVarName1 = funcLLVM.makeNewLocalName1("ret");
    String retVarName2 = funcLLVM.makeNewLocalName1("ret");

    //@formatter:off
    lines.add("  " + retVarName1 + " = insert" + "value " + returnKindName + " undef, i8 " + (index + 1) + ", 0");
    lines.add("  " + retVarName2 + " = insert" + "value " + returnKindName + " " + retVarName1 + ", " + selectedOutKindName + " " + localVar.name + ", " + (index + 1));
    lines.add("  ret " + returnKindName + " " + retVarName2);
    //@formatter:on
  }
}
