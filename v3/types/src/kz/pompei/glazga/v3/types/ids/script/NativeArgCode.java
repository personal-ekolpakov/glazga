package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.CodeWrapper;
import lombok.NonNull;

@StringRepresented
public class NativeArgCode extends CodeWrapper {

  public NativeArgCode(@NonNull String code) {
    super(code);
  }

  @StringRepresented public static NativeArgCode of(String code) {
    return code == null ? null : new NativeArgCode(code);
  }

}
