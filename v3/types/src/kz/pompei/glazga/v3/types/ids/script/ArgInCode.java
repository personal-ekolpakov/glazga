package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.CodeWrapper;
import lombok.NonNull;

@StringRepresented
public class ArgInCode extends CodeWrapper {

  public ArgInCode(@NonNull String code) {
    super(code);
  }

  @StringRepresented public static ArgInCode of(String code) {
    return code == null ? null : new ArgInCode(code);
  }

}
