package kz.pompei.glazga.v3.types.engine.llvm.tr;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.v3.types.engine.TranslatorStorage;
import kz.pompei.glazga.v3.types.engine.script.kind.ArgInCode_KindId;
import kz.pompei.glazga.v3.types.engine.script.kind.ArgOutCode_KindId;
import kz.pompei.glazga.v3.types.engine.script.kind.Kind;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbed;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbedType;
import kz.pompei.glazga.v3.types.engine.script.kind.KindVoid;
import kz.pompei.glazga.v3.types.engine.script.kind.Kinds;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.NonNull;

public class FuncLlvmHelper {

  private final TrFuncLlvm funcLLVM;

  public FuncLlvmHelper(TrFuncLlvm funcLLVM) {
    this.funcLLVM = funcLLVM;
  }

  public String returnKindName(@NonNull List<ArgOutCode_KindId> outs) {
    @NonNull Kind returnKind = returnKind(outs);
    return funcLLVM.owner.getKindName(returnKind.id());
  }

  public @NonNull Kind returnKind(@NonNull List<ArgOutCode_KindId> outs) {
    TranslatorStorage storage = funcLLVM.owner.program.storage;

    if (outs.isEmpty()) {
      KindVoid ret = new KindVoid();
      storage.ensureSavedKind(ret);
      return ret;
    }

    if (outs.size() == 1) {
      return storage.getKindById(outs.get(0).kindId);
    }

    List<KindId> kindIds = new ArrayList<>();

    KindEmbed i8 = new KindEmbed(KindEmbedType.INT, 8);
    storage.ensureSavedKind(i8);

    kindIds.add(i8.id());

    for (final ArgOutCode_KindId out : outs) {
      kindIds.add(out.kindId);
    }

    Kinds kinds = new Kinds(kindIds);
    storage.ensureSavedKind(kinds);

    return kinds;
  }

  public String declareArgs(List<ArgInCode_KindId> args) {

    List<String> types = new ArrayList<>();

    for (final ArgInCode_KindId arg : args) {
      @NonNull String kindVarName = funcLLVM.owner.getKindName(arg.kindId);
      types.add(kindVarName);
    }

    return String.join(", ", types);
  }

  public String defineArgs(List<ArgInCode_KindId> args) {

    List<String> argDefs = new ArrayList<>();

    for (final ArgInCode_KindId arg : args) {
      @NonNull String   kindVarName = funcLLVM.owner.getKindName(arg.kindId);
      @NonNull LocalVar inVarName   = funcLLVM.getArgVar(arg.code);
      argDefs.add(kindVarName + " " + inVarName.name);
    }

    return String.join(", ", argDefs);
  }
}
