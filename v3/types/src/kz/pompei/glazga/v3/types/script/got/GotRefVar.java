package kz.pompei.glazga.v3.types.script.got;

import kz.pompei.glazga.v3.types.ids.script.BoxId;
import lombok.NonNull;

public class GotRefVar extends Got {
  public BoxId varBoxId;

  public static GotRefVar of(@NonNull String varBoxId) {
    GotRefVar ret = new GotRefVar();
    ret.varBoxId = BoxId.of(varBoxId);
    return ret;
  }

}
