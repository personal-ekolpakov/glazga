package kz.pompei.glazga.v3.types.etc;

import java.security.SecureRandom;

public class RndId {

  @SuppressWarnings("SpellCheckingInspection")
  private static final String ENG = "abcdefghijklmnopqrstuvwxyz";
  private static final String DEG = "0123456789";
  private static final char[] ALL = (ENG.toLowerCase() + ENG.toUpperCase() + DEG + "_$").toCharArray();

  private static final SecureRandom RANDOM = new SecureRandom();

  public static String rndId() {
    char[]    all    = ALL;
    int       len    = all.length;
    final int N      = 13;
    char[]    result = new char[N];
    for (int i = 0; i < N; i++) {
      result[i] = all[RANDOM.nextInt(len)];
    }
    return new String(result);
  }

}
