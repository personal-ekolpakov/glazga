package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.v3.calc.Once;
import kz.pompei.glazga.v3.types.engine.Stones;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.FuncLlvmHelper;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrModuleLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.box.op_generator.TrArgIn;
import kz.pompei.glazga.v3.types.engine.script.box.op_generator.TrArgOut;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.engine.script.kind.ArgInCode_KindId;
import kz.pompei.glazga.v3.types.engine.script.kind.ArgOutCode_KindId;
import kz.pompei.glazga.v3.types.engine.script.kind.KindOp;
import kz.pompei.glazga.v3.types.engine.translating.BoxTranslatingByStones;
import kz.pompei.glazga.v3.types.engine.translating.GotExtractingKindId;
import kz.pompei.glazga.v3.types.ids.script.ArgInCode;
import kz.pompei.glazga.v3.types.ids.script.ArgOutCode;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.FuncLlvmId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.script.ModuleProfile;
import kz.pompei.glazga.v3.types.ids.script.OpGeneratorId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.script.boxes.BoxOpGenerator;
import kz.pompei.glazga.v3.types.script.boxes.op.ArgInDef;
import kz.pompei.glazga.v3.types.script.boxes.op.ArgOutDef;
import lombok.NonNull;

public class TrBoxOpGenerator extends TrBox implements BoxTranslatingByStones {
  public final @NonNull BoxOpGenerator data;

  private final Once<List<TrArgIn>>      args        = new Once<>(this::doCalcArgs);
  private final Once<List<TrArgOut>>     outs        = new Once<>(this::doCalcOuts);
  private final Once<ModuleProfile>      profile     = new Once<>(this::doCalcProfile);
  private final Once<ArgOutCode>         mainOutCode = new Once<>(this::doCalcMainOutCode);
  private final Once<TrBoxKindGenerator> owner       = new Once<>(this::doCalcOwner);

  public TrBoxOpGenerator(@NonNull BoxId id,
                          @NonNull BoxOpGenerator data,
                          @NonNull Map<GotId, TrGot> gotten,
                          @NonNull TrProgramGen translator) {
    super(id, gotten, translator);
    this.data = data;
  }

  public TrBoxKindGenerator owner() {
    return owner.get();
  }

  private TrBoxKindGenerator doCalcOwner() {
    BoxId ownerBoxId = data.ownerId;
    if (ownerBoxId == null) {
      err(Msg.of("3BLWjfPkDf"));
      return null;
    }
    TrBox ownerTrBox = program.getOrLoadBoxById(ownerBoxId);
    if (ownerTrBox == null) {
      err(Msg.of("T3BwWVJp1t", "ownerBoxId", ownerBoxId));
      return null;
    }

    if (ownerTrBox instanceof TrBoxKindGenerator ret) {
      return ret;
    }

    err(Msg.of("W8dqSL4Qbu",
               "ownerBoxId", ownerBoxId,
               "expectedClass", TrBoxKindGenerator.class,
               "actualClass", ownerTrBox.getClass()));

    return null;
  }

  private ArgOutCode doCalcMainOutCode() {
    ArgOutCode mainOutCode = null;

    for (final TrArgOut trArgOut : this.outs.get()) {
      if (trArgOut.data.master) {
        if (mainOutCode == null) {
          mainOutCode = trArgOut.code;
        } else {
          err(Msg.of("YBqOUcpLPp"));
          break;
        }
      }
    }
    return mainOutCode;
  }

  private ModuleProfile doCalcProfile() {
    ModuleProfile profile = data.profile;

    if (profile != null) {
      return profile;
    }

    err(Msg.of("ITI2082zN5"));
    return ModuleProfile.MASTER;
  }

  public ModuleProfile profile() {
    return profile.get();
  }

  public ArgOutCode mainOutCode() {
    return mainOutCode.get();
  }

  private List<TrArgIn> doCalcArgs() {

    Map<ArgInCode, ArgInDef> dataArgs = data.args;
    if (dataArgs == null) {
      return List.of();
    }

    List<TrArgIn> ret = new ArrayList<>();

    for (final Map.Entry<ArgInCode, ArgInDef> e : dataArgs.entrySet()) {
      ArgInCode code = e.getKey();
      ArgInDef  def  = e.getValue();

      if (def == null) {
        err(Msg.of("MgTS58sWZs"));
        return List.of();
      }

      if (def.kindGotId == null) {
        err(Msg.of("B5TdRXTlzU"));
        return List.of();
      }

      TrGot trGot = gotten.get(def.kindGotId);

      if (trGot == null) {
        err(Msg.of("6pAoGo1i6v"));
        return List.of();
      }

      if (!(trGot instanceof GotExtractingKindId eki)) {
        err(Msg.of("Pi1c8tQz1f", "gotId", trGot.id, "gotClass", trGot.getClass(),  "interface", GotExtractingKindId.class));
        return List.of();
      }

      KindId kindId = eki.extrackKindId();

      if (kindId == null) {
        err(Msg.of("08T7P6NM8n"));
        return List.of();
      }

      ret.add(new TrArgIn(code, def, this, kindId));
    }

    ret.sort(Comparator.comparing(x -> x.data.order));

    return List.copyOf(ret);
  }

  private List<TrArgOut> doCalcOuts() {
    Map<ArgOutCode, ArgOutDef> dataOuts = data.outs;
    if (dataOuts == null) {
      return List.of();
    }

    List<TrArgOut> ret = new ArrayList<>();

    for (final Map.Entry<ArgOutCode, ArgOutDef> e : dataOuts.entrySet()) {
      ArgOutCode code = e.getKey();
      ArgOutDef  def  = e.getValue();

      if (def == null) {
        err(Msg.of("0T1LUT6aQX"));
        return List.of();
      }

      if (def.valueKindGotId == null) {
        err(Msg.of("h5wqGNX9oU"));
        return List.of();
      }

      TrGot trGot = gotten.get(def.valueKindGotId);
      if (trGot == null) {
        err(Msg.of("sZtBdhJfk8"));
        return List.of();
      }

      if (!(trGot instanceof GotExtractingKindId eki)) {
        err(Msg.of("UjT8jFTXAm", "gotId", trGot.id, "gotClass", trGot.getClass(),  "interface", GotExtractingKindId.class));
        return List.of();
      }

      KindId kindId = eki.extrackKindId();
      if (kindId == null) {
        err(Msg.of("Pg1p3C5XPN"));
        return List.of();
      }

      ret.add(new TrArgOut(code, def, this, kindId));
    }

    ret.sort(Comparator.comparing(x -> x.data.order));

    return List.copyOf(ret);
  }

  private @NonNull KindOp createKindOp(@NonNull Stones stones) {
    if (data.stoneKinds != null && data.stoneKinds.size() > 0 || data.stoneValues != null && data.stoneValues.size() > 0) {
      throw new RuntimeException("XWKcfPlHuJ :: Здесь нужно реализовать аргументы типов");
    }
    if (stones.kinds.size() > 0 || stones.values.size() > 0) {
      throw new RuntimeException("shGfcYK1sw :: Здесь нужно реализовать аргументы типов");
    }

    List<ArgInCode_KindId>  args        = this.args.get().stream().map(TrArgIn::pair).toList();
    List<ArgOutCode_KindId> outs        = this.outs.get().stream().map(TrArgOut::pair).toList();
    ArgOutCode              mainOutCode = mainOutCode();
    ModuleProfile           profile     = profile();

    return ensureSavedKind(KindOp.builder()
                                 .srcId(OpGeneratorId.of(id, program.id, profile))
                                 .stoneKinds(Map.of())// потом надо сделать вычисление этого
                                 .stoneValues(Map.of())// потом надо сделать вычисление этого
                                 .args(args)
                                 .outs(outs)
                                 .mainOutCode(mainOutCode)
                                 .build());

  }

  @Override public @NonNull KindOp translateByStones(@NonNull Stones stones) {

    @NonNull KindOp kindOp = createKindOp(stones);

    FuncLlvmId   funcId     = kindOp.funcLlvmId();
    TrModuleLlvm moduleLLVM = program.getModuleLLVM(profile());

    if (moduleLLVM.functions.containsKey(funcId)) {
      return kindOp;
    }

    TrFuncLlvm     funcLLVM    = moduleLLVM.newFuncLlvm(kindOp, this);
    FuncLlvmHelper helper      = new FuncLlvmHelper(funcLLVM);
    String         definedArgs = helper.defineArgs(kindOp.args);
    String         retKindName = helper.returnKindName(kindOp.outs);

    funcLLVM.lines().add(";pkg6xxWsXu op «" + data.name + "»");
    funcLLVM.lines().add("define " + retKindName + " " + funcId.funcName() + "(" + definedArgs + ") {");

    downAndTranslateOp(funcLLVM, false);

    funcLLVM.lines().add("}");

    return kindOp;
  }

}
