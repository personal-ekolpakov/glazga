package kz.pompei.glazga.v3.types.engine.script.got;

import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.translating.GotTranslatingExpression;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.got.GotRefArg;
import lombok.NonNull;

public class TrGotRefArg extends TrGot implements GotTranslatingExpression {
  public final @NonNull GotRefArg data;

  public TrGotRefArg(@NonNull BoxId ownerId, @NonNull GotId id, @NonNull GotRefArg data, @NonNull TrProgramGen programGen) {
    super(ownerId, id, programGen);
    this.data = data;
  }

  @Override public LocalVar translateExpression(int tab, @NonNull TrFuncLlvm funcLLVM) {

    if (data.argCode == null) {
      err(Msg.of("Y0efWVzOTi"));
      return null;
    }

    LocalVar argVar = funcLLVM.getArgVar(data.argCode);
    if (argVar == null) {
      err(Msg.of("z8ra1v5fKD", "argCode", data.argCode));
      return null;
    }

    return argVar;
  }
}
