package kz.pompei.glazga.v3.types.engine;

import java.util.List;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.v3.types.engine.llvm.data.ModuleLlvm;
import kz.pompei.glazga.v3.types.engine.llvm.data.ProgramTranslateResult;
import kz.pompei.glazga.v3.types.engine.model.BoxKindFixList;
import kz.pompei.glazga.v3.types.engine.script.kind.Kind;
import kz.pompei.glazga.v3.types.etc.IDV;
import kz.pompei.glazga.v3.types.ids.script.ArrowId;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.ModuleId;
import kz.pompei.glazga.v3.types.ids.script.NativeId;
import kz.pompei.glazga.v3.types.ids.script.ProgramGenId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.script.arrow.Arrow;
import kz.pompei.glazga.v3.types.script.arrow.ArrowType;
import kz.pompei.glazga.v3.types.script.boxes.Box;
import lombok.NonNull;

public interface TranslatorStorage {

  BoxKindFixList loadProgramGenFixes(@NonNull ProgramGenId programGenId);

  boolean existsProgramGen(@NonNull ProgramGenId programGenId);

  List<IDV<ArrowId, Arrow>> loadTargetArrows(@NonNull BoxId sourceId, @NonNull ArrowType arrowType);

  @NonNull Map<BoxId, Box> loadBoxesByIds(@NonNull ProgramGenId programGenId, @NonNull Set<BoxId> boxIds);

  ModuleLlvm loadModuleLlvm(@NonNull ModuleId id);

  void saveModuleLlvm(@NonNull ModuleId id, @NonNull ModuleLlvm moduleLlvm);

  ProgramTranslateResult loadTranslateResult(@NonNull ProgramGenId programGenId);

  void saveTranslateResult(@NonNull ProgramGenId programGenId, @NonNull ProgramTranslateResult ptr);

  @NonNull Native getNativeById(@NonNull NativeId nativeId);

  @NonNull Kind getKindById(@NonNull KindId id);

  void ensureSavedKind(@NonNull Kind kind);

}
