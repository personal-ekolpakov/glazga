package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.script.boxes.BoxKindGenerator;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Работает с идентификаторами генераторов типов (Kind)
 */
@ToString
@EqualsAndHashCode
@StringRepresented
@RequiredArgsConstructor
public class KindGeneratorId {

  /**
   * Идентификатор бокса типа {@link BoxKindGenerator}, в котором определяется данная операция
   */
  public final @NonNull BoxId kgBoxId;

  /**
   * Идентификатор детали
   * <p>
   * Если этот идентификатор null, то нужно брать текущую деталь
   * <p>
   * Пока null отключил, вдруг он не понадобиться
   */
  public final @NonNull ProgramGenId programGenId;

  public static @NonNull KindGeneratorId of(@NonNull BoxId kgBoxId, @NonNull ProgramGenId programGenId) {
    return new KindGeneratorId(kgBoxId, programGenId);
  }

  @StringRepresented
  public static KindGeneratorId parse(String strId) {
    if (strId == null) {
      return null;
    }

    String[] split = strId.split("-");


    if (split.length == 2) {
      return new KindGeneratorId(BoxId.of(split[0].trim()), ProgramGenId.of(split[1].trim()));
    }

    return null;
  }

  @StringRepresented
  public String strValue() {
    return kgBoxId.id + '-' + programGenId.id;
  }

}
