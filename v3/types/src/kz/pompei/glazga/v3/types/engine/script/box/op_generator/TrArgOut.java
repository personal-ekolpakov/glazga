package kz.pompei.glazga.v3.types.engine.script.box.op_generator;

import kz.pompei.glazga.v3.types.engine.script.box.TrBoxOpGenerator;
import kz.pompei.glazga.v3.types.engine.script.kind.ArgOutCode_KindId;
import kz.pompei.glazga.v3.types.ids.script.ArgOutCode;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.script.boxes.op.ArgOutDef;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@RequiredArgsConstructor
public class TrArgOut {
  public final @NonNull ArgOutCode       code;
  public final @NonNull ArgOutDef        data;
  public final @NonNull TrBoxOpGenerator owner;
  public final @NonNull KindId           outKindId;

  public @NonNull ArgOutCode_KindId pair() {
    return ArgOutCode_KindId.of(code, outKindId);
  }
}
