package kz.pompei.glazga.v3.types.engine.script.kind;

import java.security.MessageDigest;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.v3.calc.Once;
import kz.pompei.glazga.v3.types.engine.Translator;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxOpGenerator;
import kz.pompei.glazga.v3.types.func.StoneValue;
import kz.pompei.glazga.v3.types.ids.script.ArgOutCode;
import kz.pompei.glazga.v3.types.ids.script.FuncLlvmId;
import kz.pompei.glazga.v3.types.ids.script.OpGeneratorId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import lombok.Builder;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import static java.nio.charset.StandardCharsets.UTF_8;
import static kz.pompei.glazga.v3.types.engine.script.kind.KindUtil.updateByStones;


@Builder
@FieldNameConstants
@ToString(exclude = "idOnce")
public class KindOp extends Kind {
  public final @NonNull OpGeneratorId                   srcId;
  public final @NonNull Map<StoneKindCode, KindId>      stoneKinds;
  public final @NonNull Map<StoneValueCode, StoneValue> stoneValues;
  public final @NonNull List<ArgInCode_KindId>          args;
  public final @NonNull List<ArgOutCode_KindId>         outs;
  public final          ArgOutCode                      mainOutCode;

  private final Once<KindId> idOnce = new Once<>(this::calcId);

  @Override @SneakyThrows public KindId id() {
    return idOnce.get();
  }

  private @SneakyThrows KindId calcId() {

    MessageDigest md = MessageDigest.getInstance("SHA-1");
    md.update(getClass().getSimpleName().getBytes(UTF_8));

    int i = 1;

    md.update(srcId.programGenId.id.getBytes(UTF_8));
    md.update(("" + i++).getBytes(UTF_8));
    md.update(srcId.opgBoxId.id.getBytes(UTF_8));

    updateByStones(md, stoneKinds, stoneValues);

    for (final ArgInCode_KindId arg : args) {
      md.update(("" + i++).getBytes(UTF_8));
      md.update(arg.code.code.getBytes(UTF_8));
      md.update(("" + i++).getBytes(UTF_8));
      md.update(arg.kindId.id.getBytes(UTF_8));
    }

    for (final ArgOutCode_KindId out : outs) {
      md.update(("" + i++).getBytes(UTF_8));
      md.update(out.code.code.getBytes(UTF_8));
      md.update(("" + i++).getBytes(UTF_8));
      md.update(out.kindId.id.getBytes(UTF_8));
    }

    if (mainOutCode != null) {
      //noinspection UnusedAssignment
      md.update(("" + i++).getBytes(UTF_8));
      md.update(mainOutCode.code.getBytes(UTF_8));
    }

    return KindId.of(KindUtil.bytesToId(md.digest()));
  }

  @Override public @NonNull String displayStr(@NonNull Translator translator) {
    @NonNull TrProgramGen program = translator.getOrLoadTrProgramGen(srcId.programGenId);
    TrBoxOpGenerator      box     = (TrBoxOpGenerator) program.getOrLoadBoxById(srcId.opgBoxId);
    return box.data.name;
  }

  public @NonNull FuncLlvmId funcLlvmId() {
    return FuncLlvmId.of(srcId.programGenId, id(), srcId.profile);
  }
}
