package kz.pompei.glazga.v3.types.script.got;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbedType;

/**
 * Прямое заполнение полей указанного типа
 */
public class GotKindEmbedConst extends Got {

  public KindEmbedType kindEmbedType;
  public int           sizeBytes;
  public String        value;

  public static GotKindEmbedConst of(KindEmbedType kindEmbedType, int sizeBytes, String value) {
    GotKindEmbedConst ret = new GotKindEmbedConst();
    ret.kindEmbedType = kindEmbedType;
    ret.sizeBytes     = sizeBytes;
    ret.value         = value;
    return ret;
  }

  public static byte[] parseFieldDataDef(String fieldDataDef) {

    String src = fieldDataDef.trim();
    int    idx = src.indexOf(' ');
    if (idx < 0) {
      throw new IllegalArgumentException("RDyXz8T1FC :: Illegal fieldDataDef = `" + fieldDataDef + "`");
    }

    String typeStr = src.substring(0, idx);
    String content = src.substring(idx).trim();

    if ("i4".equals(typeStr)) {

      int value = Integer.parseInt(content);

      ByteBuffer buffer = ByteBuffer.allocate(4);
      buffer.order(ByteOrder.LITTLE_ENDIAN);
      buffer.putInt(value);

      buffer.flip();

      return buffer.array();
    }

    throw new IllegalArgumentException("BsK6lLf1dR :: Illegal fieldDataDef = `" + fieldDataDef + "`");
  }

  public static String bytesAsDecimalSignedStr_littleEndian(byte[] bytes) {

    if (bytes.length == 4) {
      return "" + ByteBuffer.allocate(4)
                            .order(ByteOrder.LITTLE_ENDIAN)
                            .put(bytes)
                            .flip()
                            .getInt();
    }

    throw new RuntimeException("3nOs1bPU9P :: Not impl yet for length " + bytes.length);
  }
}
