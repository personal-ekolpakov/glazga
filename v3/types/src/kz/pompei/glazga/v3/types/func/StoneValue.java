package kz.pompei.glazga.v3.types.func;

import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class StoneValue {

  public final @NonNull KindId  kindId;
  public final @NonNull MemDump dump;

  public static StoneValue of(@NonNull KindId typeId, @NonNull MemDump dump) {
    return new StoneValue(typeId, dump);
  }

}
