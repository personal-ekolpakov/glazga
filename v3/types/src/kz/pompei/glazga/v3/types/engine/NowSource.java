package kz.pompei.glazga.v3.types.engine;

public interface NowSource {
  long now();
}
