package kz.pompei.glazga.v3.types.engine.script.kind;

import kz.pompei.glazga.v3.types.engine.Translator;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.NonNull;

public abstract class Kind {
  public abstract KindId id();

  public abstract @NonNull String displayStr(@NonNull Translator translator);

  public static @NonNull String displayStr(@NonNull Translator translator, KindId kindId) {
    if (kindId == null) {
      return "<NO>";
    }

    return translator.storage.getKindById(kindId).displayStr(translator);
  }

}
