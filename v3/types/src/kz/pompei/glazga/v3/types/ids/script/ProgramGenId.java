package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.IdWrapper;
import lombok.NonNull;

@StringRepresented
public class ProgramGenId extends IdWrapper {

  public ProgramGenId(@NonNull String id) {
    super(id);
  }

  @StringRepresented
  public static ProgramGenId of(String id) {
    return id == null ? null : new ProgramGenId(id);
  }

}
