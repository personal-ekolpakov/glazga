package kz.pompei.glazga.v3.types.engine.script.kind;

import java.security.MessageDigest;
import java.util.Map;
import kz.pompei.glazga.v3.calc.Once;
import kz.pompei.glazga.v3.types.engine.Translator;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxKindGenerator;
import kz.pompei.glazga.v3.types.func.StoneValue;
import kz.pompei.glazga.v3.types.ids.script.KindGeneratorId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import lombok.Builder;
import lombok.NonNull;
import lombok.SneakyThrows;

import static java.nio.charset.StandardCharsets.UTF_8;
import static kz.pompei.glazga.v3.types.engine.script.kind.KindUtil.updateByStones;
import static kz.pompei.glazga.v3.types.engine.script.kind.KindUtil.updateKindGenId;

@Builder
public class KindFields extends Kind {
  public final @NonNull KindGeneratorId                 srcId;
  public final @NonNull Map<StoneKindCode, KindId>      stoneKinds;
  public final @NonNull Map<StoneValueCode, StoneValue> stoneValues;

  private final Once<KindId> idOnce = new Once<>(this::calcId);

  @Override @SneakyThrows public KindId id() {
    return idOnce.get();
  }

  private @SneakyThrows KindId calcId() {
    MessageDigest md = MessageDigest.getInstance("SHA-1");
    md.update("iBLUkLVj3L".getBytes(UTF_8));
    updateKindGenId(md, srcId);
    updateByStones(md, stoneKinds, stoneValues);
    return KindId.of(KindUtil.bytesToId(md.digest()));
  }

  @Override public @NonNull String displayStr(@NonNull Translator translator) {

    @NonNull TrProgramGen myProgram = translator.getOrLoadTrProgramGen(srcId.programGenId);
    myProgram.getOrLoadBoxById(srcId.kgBoxId);

    var box = (TrBoxKindGenerator) myProgram.getOrLoadBoxById(srcId.kgBoxId);

    if (stoneKinds.isEmpty() && stoneValues.isEmpty()) {
      return box.data.name;
    }

    throw new RuntimeException("yUdpN9KtsE :: Пока с типовыми аргументами не сделано");
  }


}
