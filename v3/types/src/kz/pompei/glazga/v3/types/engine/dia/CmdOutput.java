package kz.pompei.glazga.v3.types.engine.dia;

import java.util.Arrays;
import java.util.List;
import lombok.NonNull;

public class CmdOutput {
  public final @NonNull List<String> lines;

  public CmdOutput(@NonNull List<String> lines) {
    this.lines = List.copyOf(lines);
  }

  public static CmdOutput of(@NonNull String text) {
    return new CmdOutput(Arrays.stream(text.split("\\n")).toList());
  }
}
