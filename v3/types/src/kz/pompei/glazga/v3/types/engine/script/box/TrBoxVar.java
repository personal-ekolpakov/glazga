package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.List;
import java.util.Map;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.engine.translating.BoxTranslatingOp;
import kz.pompei.glazga.v3.types.engine.translating.GotTranslatingExpression;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.boxes.BoxVar;
import lombok.NonNull;

public class TrBoxVar extends TrBox implements BoxTranslatingOp {
  public final @NonNull BoxVar data;

  private String definedLocalVarName;

  public TrBoxVar(@NonNull BoxId id, @NonNull BoxVar data, @NonNull Map<GotId, TrGot> gotten, @NonNull TrProgramGen translator) {
    super(id, gotten, translator);
    this.data = data;
  }

  @Override public void translateOp(@NonNull TrFuncLlvm funcLLVM) {

    if (data.initValueGotId == null) {
      err(Msg.of("4FvXJJ1eJM"));
      return;
    }

    TrGot initValueGot = gotten.get(data.initValueGotId);
    if (initValueGot == null) {
      err(Msg.of("YRBnQ7Ck1D", "initValueGotId", data.initValueGotId));
      return;
    }

    if (!(initValueGot instanceof GotTranslatingExpression te)) {
      err(Msg.of("WNim00tPNJ",
                 "initValueGotId", data.initValueGotId,
                 "gotClass", initValueGot.getClass(),
                 "interface", GotTranslatingExpression.class));
      return;
    }

    List<String> lines = funcLLVM.lines();

    String label = funcLLVM.getBoxLabel(this);
    lines.add("");
    lines.add(label + ":   ; «" + data.varName + "» cWBAFtJWhl");

    LocalVar expressionLocalVar = te.translateExpression(2, funcLLVM);

    if (expressionLocalVar == null) {
      err(Msg.of("ScS8pgC1qO"));
      return;
    }

    if (funcLLVM.boxVarNames().containsKey(id)) {
      err(Msg.of("8yEpJhN1ck"));
      return;
    }

    funcLLVM.boxVarNames().put(id, expressionLocalVar);

    lines.add("  ; vFfR6qbUDo " + happenedMsg().message());

    downAndTranslateOp(funcLLVM, true);
  }

}
