package kz.pompei.glazga.v3.types.script;

import kz.pompei.glazga.convert.StringRepresented;

@StringRepresented
public class Area {
  public double left, top, width, height;


  @StringRepresented
  public static Area parse(String str) {
    if (str == null) {
      return null;
    }

    String[] split = str.split(",");
    if (split.length < 4) {
      return null;
    }

    Area ret = new Area();

    ret.left   = Double.parseDouble(split[0].trim());
    ret.top    = Double.parseDouble(split[1].trim());
    ret.width  = Double.parseDouble(split[2].trim());
    ret.height = Double.parseDouble(split[3].trim());

    return ret;
  }

  @StringRepresented
  public String strValue() {
    return left + "," + top + "," + width + "," + height;
  }
}
