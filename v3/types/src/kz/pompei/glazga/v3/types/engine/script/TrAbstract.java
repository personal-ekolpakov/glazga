package kz.pompei.glazga.v3.types.engine.script;

import java.util.Arrays;
import java.util.function.Function;
import kz.pompei.glazga.v3.types.engine.dia.DiaMessage;
import kz.pompei.glazga.v3.types.engine.dia.DiaType;
import kz.pompei.glazga.v3.types.engine.dia.HappenedObject;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.engine.script.kind.Kind;
import kz.pompei.glazga.v3.types.engine.translating.GotTranslatingExpression;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.BotId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class TrAbstract implements HappenedObject {
  public final @NonNull TrProgramGen program;

  public abstract @NonNull BotId extractId();

  protected final @NonNull <T extends Kind> T ensureSavedKind(@NonNull T kind) {
    program.translator.storage.ensureSavedKind(kind);
    return kind;
  }

  protected static @NonNull String ta(int tab) {
    return "  ".repeat(tab);
  }

  protected final LocalVar translateGotId(GotId gotId,
                                          @NonNull Msg gotIdSourceMsg,
                                          @NonNull Function<GotId, TrGot> getTrGot,
                                          int tab,
                                          @NonNull TrFuncLlvm funcLLVM) {
    if (gotId == null) {
      err(Msg.of("7ENGcNVrDN"), gotIdSourceMsg);
      return null;
    }

    TrGot trGot = getTrGot.apply(gotId);
    if (trGot == null) {
      err(Msg.of("3JOeBWZWh0", "gotId", gotId), gotIdSourceMsg);
      return null;
    }

    if (!(trGot instanceof GotTranslatingExpression gte)) {
      err(Msg.of("sG91QeYZ76", "gotId", gotId, "class", trGot.getClass(), "interface", GotTranslatingExpression.class),
          gotIdSourceMsg);
      return null;
    }

    LocalVar localVar = gte.translateExpression(tab + 1, funcLLVM);

    if (localVar != null) {
      return localVar;
    }

    err(Msg.of("aUWj8fkPo1"), gotIdSourceMsg);
    return null;
  }

  public @Override @NonNull Msg happenedMsg() {
    return Msg.of("tdL6mEqFsS", "class", getClass(), "id", extractId());
  }

  public void err(Msg... msg) {
    program.diagnostic.list.add(DiaMessage.builder()
                                          .type(DiaType.ERR)
                                          .happenedObject(this)
                                          .msgParts(Arrays.stream(msg).toList())
                                          .build());
  }
}
