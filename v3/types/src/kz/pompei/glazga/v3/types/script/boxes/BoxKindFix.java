package kz.pompei.glazga.v3.types.script.boxes;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import lombok.NonNull;
import lombok.experimental.FieldNameConstants;

/**
 * Генератор операции
 */
@FieldNameConstants
public class BoxKindFix extends Box {

  public boolean                    test;
  public Map<StoneKindCode, GotId>  genArgTypes;
  public Map<StoneValueCode, GotId> genArgValues;

  public static BoxKindFix of() {
    return new BoxKindFix();
  }

  public BoxKindFix test(boolean test) {
    this.test = test;
    return this;
  }

  @Override protected @NonNull List<String> listToDisplay() {
    List<String> list = new ArrayList<>();
    if (genArgTypes != null) {
      List<Map.Entry<StoneKindCode, GotId>> lst = genArgTypes.entrySet()
                                                             .stream()
                                                             .sorted(Comparator.comparing(x -> x.getKey().code))
                                                             .toList();
      for (final Map.Entry<StoneKindCode, GotId> x : lst) {
        list.add("[typ:id=" + x.getKey() + ",name=" + x.getValue() + ']');
      }
    }
    if (genArgValues != null) {
      List<Map.Entry<StoneValueCode, GotId>> lst = genArgValues.entrySet()
                                                               .stream()
                                                               .sorted(Comparator.comparing(x -> x.getKey().code))
                                                               .toList();
      for (final Map.Entry<StoneValueCode, GotId> x : lst) {
        list.add("[val:id=" + x.getKey() + ",name=" + x.getValue() + ']');
      }
    }

    return list;
  }

}
