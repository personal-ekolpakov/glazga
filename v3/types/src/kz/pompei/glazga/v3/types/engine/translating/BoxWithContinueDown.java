package kz.pompei.glazga.v3.types.engine.translating;

import kz.pompei.glazga.v3.types.engine.script.box.TrBox;
import kz.pompei.glazga.v3.types.script.boxes.BoxContinue;

/**
 * В этом боксе может быть DOWN-бокс, для продолжения в конце альтернативной ветки с помощью бокса {@link BoxContinue}.
 * <p>
 * {@link BoxContinue} должен ссылаться на бокс, реализующий данный интерфейс и имеющий бокс-продолжения.
 */
public interface BoxWithContinueDown {

  /**
   * Метод, возвращает бокс-продолжение или null, если к этому боксу не прикреплен бокс через стрелку DOWN.
   * В этом случае на этот бокс нельзя ссылаться через {@link BoxContinue}
   *
   * @return бокс-продолжение или null
   */
  TrBox continueDownBox();

}
