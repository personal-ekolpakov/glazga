package kz.pompei.glazga.v3.types.script.boxes;

import java.util.List;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import lombok.NonNull;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class BoxVar extends Box {

  public String varName;
  public GotId  initValueGotId;

  @Override protected @NonNull List<String> listToDisplay() {
    return List.of(Fields.varName + '=' + varName, Fields.initValueGotId + '=' + initValueGotId);
  }

  public static BoxVar of(String varName, String initValueGotId) {
    BoxVar ret = new BoxVar();
    ret.initValueGotId = GotId.of(initValueGotId);
    ret.varName        = varName;
    return ret;
  }
}
