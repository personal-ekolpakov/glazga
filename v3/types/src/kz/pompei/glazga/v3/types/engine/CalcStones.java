package kz.pompei.glazga.v3.types.engine;

import java.util.Map;
import kz.pompei.glazga.v3.calc.Once;
import kz.pompei.glazga.v3.types.engine.translating.HasStoneGets;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CalcStones {
  private final @NonNull HasStoneGets hasStoneGets;

  private final Once<Stones> stones = new Once<>(this::calcKindArgs);

  private Stones calcKindArgs() {

    Map<StoneKindCode, GotId>  genArgTypes  = hasStoneGets.stoneKindGets();
    Map<StoneValueCode, GotId> genArgValues = hasStoneGets.stoneValuesGets();

    if (genArgTypes != null && genArgTypes.size() > 0 || genArgValues != null && genArgValues.size() > 0) {
      throw new RuntimeException("3F9vr1wR1V :: Типовые аргументы пока не сделаны");
    }

    return new Stones(Map.of(), Map.of());
  }

  public Stones getStones() {
    return stones.get();
  }

}
