package kz.pompei.glazga.v3.types.ids;

import java.util.Objects;
import kz.pompei.glazga.convert.StringRepresented;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class IdWrapper implements Comparable<IdWrapper> {
  public final @NonNull String id;

  @Override public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final IdWrapper other = (IdWrapper) o;
    return Objects.equals(id, other.id);
  }

  @Override public int hashCode() {
    return Objects.hashCode(id);
  }

  @Override public String toString() {
    return getClass().getSimpleName() + '(' + id + ')';
  }

  @StringRepresented public String id() {
    return id;
  }

  @Override public int compareTo(IdWrapper o) {
    return id.compareTo(o.id);
  }
}
