package kz.pompei.glazga.v3.types.engine.script.box.op_generator;

import kz.pompei.glazga.v3.types.engine.script.box.TrBoxOpGenerator;
import kz.pompei.glazga.v3.types.engine.script.kind.ArgInCode_KindId;
import kz.pompei.glazga.v3.types.ids.script.ArgInCode;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.script.boxes.op.ArgInDef;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TrArgIn {
  public final @NonNull ArgInCode        code;
  public final @NonNull ArgInDef         data;
  public final @NonNull TrBoxOpGenerator owner;
  public final @NonNull KindId           argKindId;

  public @NonNull ArgInCode_KindId pair() {
    return ArgInCode_KindId.of(code, argKindId);
  }
}
