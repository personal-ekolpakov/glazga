package kz.pompei.glazga.v3.types.script.got;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.script.NativeArgCode;
import kz.pompei.glazga.v3.types.ids.script.NativeCode;
import kz.pompei.glazga.v3.types.ids.script.NativeOutCode;
import lombok.NonNull;

public class GotCallNative extends Got {
  public NativeCode code;

  public Map<NativeArgCode, GotId> args;
  public Map<NativeOutCode, BoxId> outs;

  public GotId         typeGotId;
  public NativeOutCode mainOutCode;

  public static GotCallNative code(@NonNull String code) {
    GotCallNative ret = new GotCallNative();
    ret.code = new NativeCode(code);
    return ret;
  }

  public GotCallNative mainOutCode(String mainOutCode) {
    this.mainOutCode = NativeOutCode.of(mainOutCode);
    return this;
  }

  public GotCallNative typeGotId(String typeGotId) {
    this.typeGotId = GotId.of(typeGotId);
    return this;
  }

  public static GotCallNative code(@NonNull NativeCode code) {
    GotCallNative ret = new GotCallNative();
    ret.code = code;
    return ret;
  }

  public GotCallNative arg(@NonNull String argCode, @NonNull String argGotId) {
    return arg(NativeArgCode.of(argCode), argGotId);
  }

  public GotCallNative arg(@NonNull NativeArgCode argCode, @NonNull String argGotId) {
    if (args == null) {
      args = new HashMap<>();
    }
    args.put(argCode, GotId.of(argGotId));
    return this;
  }

}
