package kz.pompei.glazga.v3.types.script.boxes.op;

import kz.pompei.glazga.v3.types.ids.type.KindId;

public class ArgIn {
  public KindId type;
  public String displayStr;
  public double    order;
}
