package kz.pompei.glazga.v3.types.engine.linking;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import kz.pompei.glazga.v3.types.engine.dia.DiaMessage;
import lombok.ToString;

@ToString
public class LinkResult {
  public Path             soFile;
  public List<DiaMessage> messages = new ArrayList<>();
}
