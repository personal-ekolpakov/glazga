package kz.pompei.glazga.v3.types.script.boxes;

import java.util.List;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import lombok.NonNull;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class BoxAssign extends Box {
  public GotId leftGotId;
  public GotId rightGotId;

  public static BoxAssign left(String leftGotId) {
    BoxAssign ret = new BoxAssign();
    ret.leftGotId = GotId.of(leftGotId);
    return ret;
  }

  public BoxAssign right(String rightGotId) {
    this.rightGotId = GotId.of(rightGotId);
    return this;
  }

  @Override protected @NonNull List<String> listToDisplay() {
    return List.of(
      Fields.leftGotId + '=' + leftGotId,
      Fields.rightGotId + '=' + rightGotId
    );
  }
}
