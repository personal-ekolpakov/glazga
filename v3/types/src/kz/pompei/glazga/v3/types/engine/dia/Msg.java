package kz.pompei.glazga.v3.types.engine.dia;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.v3.types.engine.dia.lang.DiaLangMessageTemplates;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static kz.pompei.glazga.utils.StrUtil.applyStrTemplate;

@RequiredArgsConstructor
public class Msg {
  public final @NonNull String              msgId;
  public final @NonNull Map<String, Object> params;

  public static @NonNull Msg of(@NonNull String msgId, Object... params) {
    if (params.length % 2 != 0) {
      throw new RuntimeException("tKBqP8qd95 :: Illegal arg count for params - it MUST be odd: " + params.length);
    }

    Map<String, Object> pm = new HashMap<>();
    for (int i = 0; i < params.length; i += 2) {
      pm.put((String) params[i], params[i + 1]);
    }
    return new Msg(msgId, Map.copyOf(pm));
  }

  public @NonNull String message() {
    String messageTemplate = DiaLangMessageTemplates.msgTemplates().get(msgId);
    return msgId + " :: " + requireNonNull(applyStrTemplate("{}", messageTemplate, valueToStr(params)));
  }

  private static @NonNull Map<String, String> valueToStr(@NonNull Map<String, Object> params) {
    Map<String, String> ret = new HashMap<>();
    for (final Map.Entry<String, Object> e : params.entrySet()) {
      ret.put(e.getKey(), convertParamValue(e.getValue()));
    }
    return ret;
  }

  private static @NonNull String convertParamValue(Object value) {
    if (value == null) {
      return "!NIL!";
    }
    if (value instanceof String s) {
      return s;
    }
    if (value instanceof Class<?> cl) {
      return cl.getSimpleName();
    }
    if (value instanceof CmdOutput out) {
      return "\n" + out.lines.stream().flatMap(l -> Arrays.stream(l.split("\\n"))).collect(joining("\n"));
    }
    return "" + value;
  }
}
