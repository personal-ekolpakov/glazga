package kz.pompei.glazga.v3.types.ids.script;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ModuleProfile {
  MASTER('m'),
  TEST('t'),
  ;

  public final char c;

  public static ModuleProfile valueByChar(char c) {

    for (final ModuleProfile value : values()) {
      if (value.c == c) {
        return value;
      }
    }

    return null;
  }
}
