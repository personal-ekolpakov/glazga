package kz.pompei.glazga.v3.types.engine.script.got;

import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbed;
import kz.pompei.glazga.v3.types.engine.translating.GotTranslatingExpression;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.got.GotKindEmbedConst;
import lombok.NonNull;

import static kz.pompei.glazga.v3.types.engine.script.TrProgramGen.tab;

public class TrGotKindEmbedConst extends TrGot implements GotTranslatingExpression {
  public final @NonNull GotKindEmbedConst data;

  public TrGotKindEmbedConst(@NonNull BoxId ownerId,
                             @NonNull GotId id,
                             @NonNull GotKindEmbedConst data,
                             @NonNull TrProgramGen translator) {
    super(ownerId, id, translator);
    this.data = data;
  }

  @Override public LocalVar translateExpression(int tab, @NonNull TrFuncLlvm funcLLVM) {

    if (data.kindEmbedType == null) {
      err(Msg.of("Ht9z1xm7fv"));
      return null;
    }
    if (data.sizeBytes <= 0) {
      err(Msg.of("r5tEaGd4ch", "size", data.sizeBytes));
      return null;
    }
    if (data.value == null) {
      err(Msg.of("zFKpLXoYoZ"));
      return null;
    }

    KindEmbed kindEmbed = KindEmbed.of(data.kindEmbedType, data.sizeBytes);
    String    localName = funcLLVM.makeNewLocalName(null);

    funcLLVM.lines().add(tab(tab) + "; nEKRuQPYsy " + getClass().getSimpleName() + ", id=" + id.id);
    funcLLVM.lines().add(tab(tab) + localName + " = add " + kindEmbed.llvmName() + " 0, " + data.value + ";");

    return LocalVar.of(kindEmbed, localName);
  }

}
