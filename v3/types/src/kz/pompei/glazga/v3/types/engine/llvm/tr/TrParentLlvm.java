package kz.pompei.glazga.v3.types.engine.llvm.tr;

import java.util.Set;
import kz.pompei.glazga.utils.StrUtil;
import lombok.NonNull;

public abstract class TrParentLlvm {
  protected abstract @NonNull Set<String> variableNames();

  public String makeNewLocalName(String prefix) {
    return makeNewLocalName(prefix, true);
  }

  public String makeNewLocalName1(String prefix) {
    return makeNewLocalName(prefix, false);
  }

  private String makeNewLocalName(String prefix, boolean mayEmptyPrefix) {

    String usingPrefix = StrUtil.rollUpIllegalChars(prefix);

    String c = "%" + (usingPrefix == null ? "q" : usingPrefix);

    if (mayEmptyPrefix) {
      if (prefix != null && !prefix.isEmpty() && !variableNames().contains(c)) {
        variableNames().add(c);
        return c;
      }
    }

    int i = 1;
    while (true) {
      String ret = c + i++;
      if (!variableNames().contains(ret)) {
        variableNames().add(ret);
        return ret;
      }
    }
  }
}
