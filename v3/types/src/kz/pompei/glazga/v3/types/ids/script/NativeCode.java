package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.CodeWrapper;
import lombok.NonNull;

@StringRepresented
public class NativeCode extends CodeWrapper {

  public NativeCode(@NonNull String code) {
    super(code);
  }

  @StringRepresented public static NativeCode of(String code) {
    return code == null ? null : new NativeCode(code);
  }

}
