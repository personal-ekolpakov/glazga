package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import lombok.NonNull;

@StringRepresented
public class GotId extends BotId {

  public GotId(@NonNull String id) {
    super(id);
  }

  @StringRepresented
  public static GotId of(String id) {
    return id == null ? null : new GotId(id);
  }

}
