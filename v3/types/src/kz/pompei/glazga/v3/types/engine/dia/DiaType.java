package kz.pompei.glazga.v3.types.engine.dia;

public enum DiaType {
  ERR, WRN, INF
}
