package kz.pompei.glazga.v3.types.script.got;

import java.util.HashMap;
import java.util.Map;
import kz.pompei.glazga.v3.types.ids.script.ArgInCode;
import kz.pompei.glazga.v3.types.ids.script.ArgOutCode;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.script.ModuleProfile;
import kz.pompei.glazga.v3.types.ids.script.OpGeneratorId;
import kz.pompei.glazga.v3.types.ids.script.ProgramGenId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import lombok.NonNull;

public class GotCallOp extends Got {
  public OpGeneratorId              opGeneratorId;
  public Map<StoneKindCode, GotId>  stoneKindGets;
  public Map<StoneValueCode, GotId> stoneValueGets;
  public Map<ArgInCode, GotId>      args;
  public Map<ArgOutCode, BoxId>     outs;
  public ArgOutCode                 mainOutCode;

  public static GotCallOp of(String opgBoxId, String progGenId, @NonNull ModuleProfile profile) {
    GotCallOp ret = new GotCallOp();
    ret.opGeneratorId = new OpGeneratorId(BoxId.of(opgBoxId), ProgramGenId.of(progGenId), profile);
    return ret;
  }

  public GotCallOp arg(String argCode, String valueGotId) {
    if (args == null) {
      args = new HashMap<>();
    }

    args.put(ArgInCode.of(argCode), GotId.of(valueGotId));

    return this;
  }

  public GotCallOp mainOutCode(String mainOutCode) {
    this.mainOutCode = ArgOutCode.of(mainOutCode);
    return this;
  }
}
