package kz.pompei.glazga.v3.types.engine.translating;

import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import lombok.NonNull;

/**
 * Выражение, которое генерирует LLVM-код для вычисления этого выражения
 */
public interface GotTranslatingExpression {
  /**
   * Генерирует код для вычисления данного выражения.
   *
   * @param tab      отступ слева
   * @param funcLLVM место, куда генерируется код
   * @return Возвращает тип переменной и имя переменной
   */
  LocalVar translateExpression(int tab, @NonNull TrFuncLlvm funcLLVM);
}
