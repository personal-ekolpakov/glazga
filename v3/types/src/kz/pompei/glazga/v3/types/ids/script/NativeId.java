package kz.pompei.glazga.v3.types.ids.script;

import java.util.Map;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Builder
@EqualsAndHashCode
@RequiredArgsConstructor
public class NativeId {

  public final @NonNull NativeCode code;

  public final @NonNull Map<@NonNull NativeArgCode, @NonNull Integer> argSizesBytes;

}
