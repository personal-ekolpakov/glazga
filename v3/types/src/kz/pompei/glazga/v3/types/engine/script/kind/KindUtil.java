package kz.pompei.glazga.v3.types.engine.script.kind;

import java.security.MessageDigest;
import java.util.Base64;
import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import kz.pompei.glazga.v3.types.func.StoneValue;
import kz.pompei.glazga.v3.types.ids.script.KindGeneratorId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import lombok.NonNull;

import static java.nio.charset.StandardCharsets.UTF_8;

public class KindUtil {

  public static void updateByStones(@NonNull MessageDigest md,
                                    @NonNull Map<StoneKindCode, KindId> stoneKinds,
                                    @NonNull Map<StoneValueCode, StoneValue> stoneValues) {

    md.update("rXBs5VtNIn".getBytes(UTF_8));
    for (final Map.Entry<StoneKindCode, KindId> e : stoneKinds.entrySet()
                                                              .stream()
                                                              .sorted(Comparator.comparing(x -> x.getKey().code))
                                                              .toList()) {
      md.update(e.getKey().code.getBytes(UTF_8));
      md.update(e.getValue().id.getBytes(UTF_8));
    }

    md.update("W0t0d2UdPG".getBytes(UTF_8));
    for (final Map.Entry<StoneValueCode, StoneValue> e : stoneValues.entrySet()
                                                                    .stream()
                                                                    .sorted(Comparator.comparing(x -> x.getKey().code))
                                                                    .toList()) {
      md.update(e.getKey().code.getBytes(UTF_8));
      md.update(e.getValue().kindId.id.getBytes(UTF_8));
      md.update(e.getValue().dump.data());
    }
  }

  public static void updateKindGenId(@NonNull MessageDigest md, @NonNull KindGeneratorId kindGeneratorId) {
    md.update("UHHqL1yoCa".getBytes(UTF_8));
    md.update(kindGeneratorId.kgBoxId.id.getBytes(UTF_8));
    md.update(kindGeneratorId.programGenId.id.getBytes(UTF_8));
  }

  public static void updateKindEmbed(@NonNull MessageDigest md, @NonNull KindEmbed kindEmbed) {
    md.update("WFBCqkSeNt".getBytes(UTF_8));
    md.update((kindEmbed.type.ordinal() + "").getBytes(UTF_8));
    md.update((kindEmbed.sizeBits + "").getBytes(UTF_8));
  }

  public static String bytesToId(byte[] bytes) {
    return Base64.getEncoder()
                 .encodeToString(bytes)
                 .replace('/', '$')
                 .replace('+', '_')
                 .replaceAll("=", "");
  }

  public static boolean eq(Kind a, Kind b) {
    if (a == b) {
      return true;
    }
    if (a == null || b == null) {
      return false;
    }

    Kind a1 = normalizeKind(a);
    Kind b1 = normalizeKind(b);

    return Objects.equals(a1.id(), b1.id());
  }

  private static @NonNull Kind normalizeKind(@NonNull Kind kind) {
    if (kind instanceof KindEmbedAlias kea) {
      return kea.kindEmbed;
    }
    return kind;
  }
}
