package kz.pompei.glazga.v3.types.ids.type;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.IdWrapper;
import lombok.NonNull;

@StringRepresented
public class KindId extends IdWrapper {

  public KindId(@NonNull String id) {
    super(id);
  }

  @StringRepresented public static KindId of(String id) {
    return id == null ? null : new KindId(id);
  }

}
