package kz.pompei.glazga.v3.types.engine.script.kind;

import kz.pompei.glazga.v3.types.ids.script.ArgInCode;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode
@RequiredArgsConstructor
public class ArgInCode_KindId {
  public final @NonNull ArgInCode code;
  public final @NonNull KindId    kindId;

  public static @NonNull ArgInCode_KindId of(@NonNull ArgInCode code, @NonNull KindId kindId) {
    return new ArgInCode_KindId(code, kindId);
  }
}
