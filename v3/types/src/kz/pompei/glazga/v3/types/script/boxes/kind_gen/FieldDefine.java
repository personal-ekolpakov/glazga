package kz.pompei.glazga.v3.types.script.boxes.kind_gen;

import kz.pompei.glazga.v3.types.ids.script.GotId;

public class FieldDefine {
  public String name;
  public GotId  typeGotId;
  public double order;
  public int    alignBytes;
}
