package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.script.boxes.BoxOpGenerator;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Работает с идентификаторами операций
 */
@ToString
@EqualsAndHashCode
@StringRepresented
@RequiredArgsConstructor
public class OpGeneratorId {

  /**
   * Идентификатор бокса типа {@link BoxOpGenerator}, в котором определяется данная операция
   */
  public final @NonNull BoxId opgBoxId;

  /**
   * Идентификатор детали
   * <p>
   * Если этот идентификатор null, то нужно брать текущую деталь
   * <p>
   * Пока null отключил, вдруг он не понадобиться
   */
  public final @NonNull ProgramGenId programGenId;

  /**
   * Профиль операции: тестовый профиль, или мастер-профиль
   */
  public final @NonNull ModuleProfile profile;

  public static @NonNull OpGeneratorId of(@NonNull BoxId opgBoxId,
                                          @NonNull ProgramGenId programGenId,
                                          @NonNull ModuleProfile profile) {
    return new OpGeneratorId(opgBoxId, programGenId, profile);
  }

  public @NonNull ModuleId moduleId() {
    return ModuleId.of(programGenId, profile);
  }

  @StringRepresented
  public static OpGeneratorId parse(String strId) {
    if (strId == null || strId.isEmpty()) {
      return null;
    }

    ModuleProfile profile = ModuleProfile.valueByChar(strId.charAt(0));

    if (profile == null) {
      return null;
    }

    String[] split = strId.substring(1).split("-");


    if (split.length == 2) {
      return new OpGeneratorId(BoxId.of(split[0].trim()), ProgramGenId.of(split[1].trim()), profile);
    }

    return null;
  }

  @StringRepresented
  public String strValue() {
    return profile.c + opgBoxId.id + '-' + programGenId.id;
  }

}
