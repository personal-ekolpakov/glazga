package kz.pompei.glazga.v3.types.ids.type;


import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.CodeWrapper;
import lombok.NonNull;

@StringRepresented
public class OpCode extends CodeWrapper {

  public OpCode(@NonNull String code) {
    super(code);
  }

  @StringRepresented public static OpCode of(String code) {
    return code == null ? null : new OpCode(code);
  }

}
