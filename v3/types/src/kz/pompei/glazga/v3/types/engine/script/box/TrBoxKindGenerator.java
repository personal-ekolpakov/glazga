package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.Stones;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.engine.script.kind.Kind;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbedAlias;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.script.KindGeneratorId;
import kz.pompei.glazga.v3.types.script.boxes.BoxKindGenerator;
import kz.pompei.glazga.v3.types.script.boxes.kind_gen.Destiny;
import lombok.NonNull;

public class TrBoxKindGenerator extends TrBox {
  public final @NonNull BoxKindGenerator data;

  public TrBoxKindGenerator(@NonNull BoxId id,
                            @NonNull BoxKindGenerator data,
                            @NonNull Map<GotId, TrGot> gotten,
                            @NonNull TrProgramGen translator) {
    super(id, gotten, translator);
    this.data = data;
  }

  public @NonNull Kind createKind(@NonNull Stones ignore) {

    if (data.destiny == Destiny.EMBED_ALIAS) {
      return ensureSavedKind(KindEmbedAlias.builder()
                                           .srcId(KindGeneratorId.of(id, program.id))
                                           .kindEmbed(data.kindEmbed)
                                           .build());
    }

    throw new RuntimeException("S68YWf1m6G :: Not impl yet for data.destiny == " + data.destiny);

  }
}
