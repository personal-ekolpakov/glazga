package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.IdWrapper;
import lombok.NonNull;

@StringRepresented
public class ArrowId extends IdWrapper {

  public ArrowId(@NonNull String id) {
    super(id);
  }

  @StringRepresented
  public static ArrowId of(String id) {
    return id == null ? null : new ArrowId(id);
  }

}
