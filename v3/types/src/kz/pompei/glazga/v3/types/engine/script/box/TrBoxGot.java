package kz.pompei.glazga.v3.types.engine.script.box;

import java.util.Map;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.boxes.BoxGot;
import lombok.NonNull;
import lombok.experimental.NonFinal;

public class TrBoxGot extends TrBox {
  public final @NonFinal BoxGot data;

  public TrBoxGot(@NonNull BoxId id, BoxGot data, @NonNull Map<GotId, TrGot> gotten, @NonNull TrProgramGen translator) {
    super(id, gotten, translator);
    this.data = data;
  }
}
