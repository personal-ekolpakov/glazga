package kz.pompei.glazga.v3.types.script.got;

import kz.pompei.glazga.convert.DiscriminatorPostfix;

@DiscriminatorPostfix("type")
public abstract class Got {}
