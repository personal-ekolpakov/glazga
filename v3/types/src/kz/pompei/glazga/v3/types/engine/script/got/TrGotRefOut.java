package kz.pompei.glazga.v3.types.engine.script.got;

import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import lombok.NonNull;

public class TrGotRefOut extends TrGot {
  public TrGotRefOut(@NonNull BoxId ownerId,
                     @NonNull GotId id,
                     @NonNull TrProgramGen translator) {
    super(ownerId, id, translator);
  }
}
