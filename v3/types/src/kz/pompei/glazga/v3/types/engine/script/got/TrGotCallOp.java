package kz.pompei.glazga.v3.types.engine.script.got;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import kz.pompei.glazga.v3.types.engine.CalcStones;
import kz.pompei.glazga.v3.types.engine.Stones;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.tr.FuncLlvmHelper;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrFuncLlvm;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.box.TrBox;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxOpGenerator;
import kz.pompei.glazga.v3.types.engine.script.kind.ArgInCode_KindId;
import kz.pompei.glazga.v3.types.engine.script.kind.Kind;
import kz.pompei.glazga.v3.types.engine.script.kind.KindOp;
import kz.pompei.glazga.v3.types.engine.translating.GotTranslatingExpression;
import kz.pompei.glazga.v3.types.engine.translating.HasStoneGets;
import kz.pompei.glazga.v3.types.engine.translating.model.LocalVar;
import kz.pompei.glazga.v3.types.ids.script.ArgInCode;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.FuncLlvmId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.script.ModuleId;
import kz.pompei.glazga.v3.types.ids.script.OpGeneratorId;
import kz.pompei.glazga.v3.types.ids.script.ProgramGenId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import kz.pompei.glazga.v3.types.script.got.GotCallOp;
import lombok.NonNull;

public class TrGotCallOp extends TrGot implements GotTranslatingExpression, HasStoneGets {
  public final @NonNull GotCallOp data;

  public TrGotCallOp(@NonNull BoxId ownerId, @NonNull GotId id, @NonNull GotCallOp data, @NonNull TrProgramGen program) {
    super(ownerId, id, program);
    this.data = data;
  }

  private final CalcStones calcStones = new CalcStones(this);

  @Override public Map<StoneKindCode, GotId> stoneKindGets() {
    return data.stoneKindGets;
  }

  @Override public Map<StoneValueCode, GotId> stoneValuesGets() {
    return data.stoneValueGets;
  }

  @Override public LocalVar translateExpression(int tab, @NonNull TrFuncLlvm funcLLVM) {

    OpGeneratorId opGeneratorId = data.opGeneratorId;

    if (opGeneratorId == null) {
      err(Msg.of("W8NPjFSreH"));
      return null;
    }

    @NonNull ProgramGenId targetProgramGenId = opGeneratorId.programGenId;

    TrProgramGen trProgramGen = program.translator.getOrLoadTrProgramGen(targetProgramGenId);

    if (trProgramGen == null) {
      err(Msg.of("Oun0QeJn1w", "targetProgramGenId", targetProgramGenId));
      return null;
    }

    TrBox trBox = trProgramGen.getOrLoadBoxById(opGeneratorId.opgBoxId);

    if (trBox == null) {
      err(Msg.of("X0nfi6AfP5", "targetProgramGenId", targetProgramGenId, "opgBoxId", opGeneratorId.opgBoxId));
      return null;
    }

    if (!(trBox instanceof TrBoxOpGenerator opg)) {
      err(Msg.of("pybD04knTZ",
                 "targetProgramGenId", targetProgramGenId,
                 "opgBoxId", opGeneratorId.opgBoxId,
                 "boxClass", trBox.getClass(),
                 "expectedClass", TrBoxOpGenerator.class));
      return null;
    }

    final Stones         stones         = calcStones.getStones();
    final KindOp         kindOp         = opg.translateByStones(stones);
    final FuncLlvmId     funcLlvmId     = kindOp.funcLlvmId();
    final ModuleId       myModuleId     = funcLLVM.owner.id;
    final ModuleId       targetModuleId = opGeneratorId.moduleId();
    final FuncLlvmHelper helper         = new FuncLlvmHelper(funcLLVM);
    final Kind           resKind        = helper.returnKind(kindOp.outs);
    final String         resKindName    = helper.returnKindName(kindOp.outs);

    if (!Objects.equals(myModuleId, targetModuleId)) {
      String             declareArgs = helper.declareArgs(kindOp.args);
      final List<String> defLines    = funcLLVM.owner.data.typeDefLines();
      defLines.add("declare " + resKindName + " " + funcLlvmId.funcName() + "(" + declareArgs + ");");
    }

    List<String> argVars = new ArrayList<>();

    for (final ArgInCode_KindId arg : kindOp.args) {

      Map<ArgInCode, GotId> args = data.args;
      if (args == null) {
        err(Msg.of("1dTaZmCl8S"));
        break;
      }

      GotId gotId = args.get(arg.code);

      LocalVar varName = translateGotId(gotId, Msg.of("afYK3DUmV5", "code", arg.code), this::getTrGotById, tab, funcLLVM);

      if (varName == null) {
        err(Msg.of("g8wPOCm2sd", "argCode", arg.code));
        break;
      }

      String kindVarName = funcLLVM.owner.getKindName(arg.kindId);
      argVars.add(kindVarName + " " + varName.name);
    }

    if (kindOp.args.size() != argVars.size()) {
      err(Msg.of("D4Jr1dEdLd",
                 "opGeneratorId", opGeneratorId,
                 "actualSize", argVars.size(),
                 "requiredSize", kindOp.args.size()));
      return null;
    }

    List<String> lines = funcLLVM.lines();

    String resLocalName = funcLLVM.makeNewLocalName("res_92ehRGeKPz_");

    lines.add("  ".repeat(tab) + "; Pn1au7RKdp call");
    lines.add("  ".repeat(tab) + resLocalName + " = call " + resKindName + " " + funcLlvmId.funcName()
              + "(" + String.join(", ", argVars) + ");");

    return LocalVar.of(resKind, resLocalName);
  }

}
