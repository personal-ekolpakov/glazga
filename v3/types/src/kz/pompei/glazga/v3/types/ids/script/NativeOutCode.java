package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import kz.pompei.glazga.v3.types.ids.CodeWrapper;
import lombok.NonNull;

@StringRepresented
public class NativeOutCode extends CodeWrapper {

  public NativeOutCode(@NonNull String code) {
    super(code);
  }

  @StringRepresented public static NativeOutCode of(String code) {
    return code == null ? null : new NativeOutCode(code);
  }

}
