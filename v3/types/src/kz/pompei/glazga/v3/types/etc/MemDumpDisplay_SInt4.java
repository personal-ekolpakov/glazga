package kz.pompei.glazga.v3.types.etc;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kz.pompei.glazga.v3.types.func.MemDump;
import lombok.NonNull;

public class MemDumpDisplay_SInt4 implements MemDumpDisplay {

  private MemDumpDisplay_SInt4() {}

  public static final MemDumpDisplay_SInt4 I = new MemDumpDisplay_SInt4();

  @Override public @NonNull String display(@NonNull MemDump memDump) {
    return "" + extractInt(memDump);
  }

  public static int extractInt(@NonNull MemDump memDump) {
    ByteBuffer buffer = ByteBuffer.wrap(memDump.data());
    buffer.order(ByteOrder.LITTLE_ENDIAN);
    return buffer.getInt();
  }
}
