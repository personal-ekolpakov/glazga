package kz.pompei.glazga.v3.types.engine.script;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.v3.types.engine.Stones;
import kz.pompei.glazga.v3.types.engine.Translator;
import kz.pompei.glazga.v3.types.engine.TranslatorStorage;
import kz.pompei.glazga.v3.types.engine.dia.DiaMessage;
import kz.pompei.glazga.v3.types.engine.dia.DiaType;
import kz.pompei.glazga.v3.types.engine.dia.Diagnostic;
import kz.pompei.glazga.v3.types.engine.dia.HappenedObject;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.llvm.data.ModuleLlvm;
import kz.pompei.glazga.v3.types.engine.llvm.tr.TrModuleLlvm;
import kz.pompei.glazga.v3.types.engine.model.BoxKindFixList;
import kz.pompei.glazga.v3.types.engine.script.box.TrBox;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxKindFix;
import kz.pompei.glazga.v3.types.engine.script.got.TrGot;
import kz.pompei.glazga.v3.types.engine.translating.BoxTranslatingByStones;
import kz.pompei.glazga.v3.types.etc.IDV;
import kz.pompei.glazga.v3.types.ids.script.ArrowId;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.script.ModuleId;
import kz.pompei.glazga.v3.types.ids.script.ModuleProfile;
import kz.pompei.glazga.v3.types.ids.script.ProgramGenId;
import kz.pompei.glazga.v3.types.script.arrow.Arrow;
import kz.pompei.glazga.v3.types.script.arrow.ArrowType;
import kz.pompei.glazga.v3.types.script.boxes.Box;
import kz.pompei.glazga.v3.types.script.got.Got;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

@RequiredArgsConstructor
public class TrProgramGen implements HappenedObject {
  public final @NonNull ProgramGenId      id;
  public final @NonNull TranslatorStorage storage;
  public final @NonNull Translator        translator;

  public final Diagnostic diagnostic = new Diagnostic();

  private final Map<IDV<BoxId, ArrowType>, List<IDV<ArrowId, Arrow>>> arrowTargets = new HashMap<>();

  public final Map<BoxId, TrBox> boxes = new HashMap<>();

  private final Map<ModuleProfile, TrModuleLlvm> modules = new HashMap<>();

  public @NonNull List<TrModuleLlvm> modules() {
    return modules.entrySet()
                  .stream()
                  .sorted(Comparator.comparing(x -> x.getKey().c))
                  .map(Map.Entry::getValue)
                  .toList();
  }

  @SuppressWarnings("SameParameterValue")
  private List<IDV<ArrowId, Arrow>> getTargetIds(@NonNull BoxId sourceId, @NonNull ArrowType arrowType) {

    IDV<BoxId, ArrowType> idv = IDV.idv(sourceId, arrowType);
    {
      List<IDV<ArrowId, Arrow>> arrows = arrowTargets.get(idv);
      if (arrows != null) {
        return arrows;
      }
    }
    {
      List<IDV<ArrowId, Arrow>> arrows = storage.loadTargetArrows(sourceId, arrowType);
      arrowTargets.put(idv, arrows);
      return arrows;
    }
  }

  public @NonNull List<TrBox> getAttachedBoxes(@NonNull BoxId sourceId, @NonNull ArrowType arrowType) {
    List<IDV<ArrowId, Arrow>> arrows = getTargetIds(sourceId, arrowType);
    if (arrows.isEmpty()) {
      return List.of();
    }

    Map<BoxId, TrBox>         localTargetBoxes = new HashMap<>();
    List<IDV<ArrowId, Arrow>> absentArrows     = new ArrayList<>();
    for (final IDV<ArrowId, Arrow> arrow : arrows) {
      TrBox localBox = boxes.get(arrow.v.targetId);
      if (localBox == null) {
        absentArrows.add(arrow);
      } else {
        localTargetBoxes.put(arrow.v.targetId, localBox);
      }
    }

    Map<BoxId, Box> loadedBoxes = storage.loadBoxesByIds(id, absentArrows.stream()
                                                                         .map(x -> x.v.targetId)
                                                                         .collect(toSet()));
    for (final IDV<ArrowId, Arrow> aa : absentArrows) {
      if (loadedBoxes.get(aa.v.targetId) == null) {
        diagnostic.noTargetBoxOfArrow(aa);
      }
    }

    Map<BoxId, TrBox> trBoxes = createTrBoxes(loadedBoxes);
    boxes.putAll(trBoxes);
    localTargetBoxes.putAll(trBoxes);

    return List.copyOf(localTargetBoxes.values());
  }


  public Map<BoxId, TrBox> getOrLoadBoxesById(@NonNull Set<BoxId> boxIds) {

    Set<BoxId> absentBoxIds = new HashSet<>();

    Map<BoxId, TrBox> ret = new HashMap<>();

    for (final BoxId boxId : boxIds) {
      TrBox trBox = boxes.get(boxId);
      if (trBox == null) {
        absentBoxIds.add(boxId);
      } else {
        ret.put(boxId, trBox);
      }
    }

    if (absentBoxIds.isEmpty()) {
      return ret;
    }

    {
      Map<BoxId, TrBox> trBoxes = createTrBoxes(storage.loadBoxesByIds(id, absentBoxIds));
      ret.putAll(trBoxes);
      boxes.putAll(trBoxes);
    }

    return ret;
  }

  public TrBox getOrLoadBoxById(@NonNull BoxId boxId) {
    return getOrLoadBoxesById(Set.of(boxId)).get(boxId);
  }

  private Map<BoxId, TrBox> createTrBoxes(Map<BoxId, Box> sourceMap) {
    Map<BoxId, TrBox> ret = new HashMap<>();
    for (final Map.Entry<BoxId, Box> e : sourceMap.entrySet()) {
      @NonNull BoxId boxId = e.getKey();
      @NonNull Box   box   = e.getValue();
      @NonNull TrBox trBox = createTrBox(boxId, box);
      ret.put(boxId, trBox);
    }
    return ret;
  }

  @SneakyThrows
  private @NonNull TrBox createTrBox(@NonNull BoxId boxId, @NonNull Box box) {
    Class<?>       boxClass    = box.getClass();
    Class<?>       trBoxClass  = Class.forName(TrBox.class.getPackageName() + ".Tr" + boxClass.getSimpleName());
    Constructor<?> constructor = trBoxClass.getConstructor(BoxId.class, boxClass, Map.class, TrProgramGen.class);

    Map<GotId, TrGot> gotten = new HashMap<>();
    {
      Map<GotId, Got> gg = box.gg;
      if (gg != null) {
        for (final Map.Entry<GotId, Got> e : gg.entrySet()) {
          GotId gotId = e.getKey();
          Got   got   = e.getValue();
          TrGot trGot = createTrGot(boxId, gotId, got);
          gotten.put(gotId, trGot);
        }
      }
    }

    return (TrBox) constructor.newInstance(boxId, box, Map.copyOf(gotten), this);
  }

  @SneakyThrows
  private TrGot createTrGot(@NonNull BoxId boxId, @NonNull GotId gotId, @NonNull Got got) {
    Class<?>       gotClass    = got.getClass();
    Class<?>       trGotClass  = Class.forName(TrGot.class.getPackageName() + ".Tr" + gotClass.getSimpleName());
    Constructor<?> constructor = trGotClass.getConstructor(BoxId.class, GotId.class, gotClass, TrProgramGen.class);
    return (TrGot) constructor.newInstance(boxId, gotId, got, this);
  }

  public static String tab(int tab) {
    return "  ".repeat(tab);
  }

  private void err(Msg... msg) {
    diagnostic.list.add(DiaMessage.builder()
                                  .type(DiaType.ERR)
                                  .happenedObject(this)
                                  .msgParts(Arrays.stream(msg).toList())
                                  .build());
  }

  public void translateKindFixes() {
    BoxKindFixList boxKindFixList = storage.loadProgramGenFixes(id);

    Map<BoxId, TrBox> fixBoxes = createTrBoxes(boxKindFixList.list.stream().collect(toMap(x -> x.id, x -> x.v)));

    for (final TrBox box : fixBoxes.values()) {
      boxes.put(box.id, box);

      if (!(box instanceof TrBoxKindFix fix)) {
        err(Msg.of("ci3PZyNEl4", "box", box));
        continue;
      }

      Stones stones = fix.getStones();

      if (stones == null) {
        err(Msg.of("nPZg71gNPT"));
        continue;
      }

      List<TrBox> attachedBoxes = getAttachedBoxes(fix.id, ArrowType.DOWN);

      if (attachedBoxes.isEmpty()) {
        err(Msg.of("7Ns45pjY81", "id", fix.id));
      }

      for (final TrBox trBox : attachedBoxes) {
        if (trBox instanceof BoxTranslatingByStones bka) {
          bka.translateByStones(stones);
        } else {
          err(Msg.of("IjY5eOuBZ9", "interface", BoxTranslatingByStones.class, "class", fix.getClass()));
        }
      }

    }

  }

  public @NonNull TrModuleLlvm getModuleLLVM(@NonNull ModuleProfile profile) {

    {
      TrModuleLlvm m = modules.get(profile);
      if (m != null) {
        return m;
      }
    }

    {
      ModuleId     moduleId = ModuleId.of(id, profile);
      ModuleLlvm   data     = new ModuleLlvm(id);
      TrModuleLlvm m        = new TrModuleLlvm(moduleId, data, this);
      data.profile = profile;
      modules.put(profile, m);
      return m;
    }
  }

  @Override public @NonNull Msg happenedMsg() {
    return Msg.of("oAmMa20B4R", "id", id);
  }
}
