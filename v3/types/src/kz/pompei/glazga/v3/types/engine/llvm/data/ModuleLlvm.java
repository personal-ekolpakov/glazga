package kz.pompei.glazga.v3.types.engine.llvm.data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.v3.types.ids.script.FuncLlvmId;
import kz.pompei.glazga.v3.types.ids.script.ModuleProfile;
import kz.pompei.glazga.v3.types.ids.script.ProgramGenId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ModuleLlvm {
  public final @NonNull ProgramGenId programGenId;

  public long                      modifiedTime;
  public ModuleProfile             profile;
  public List<String>              typeDefLines;
  public Map<KindId, String>       kindNames;
  public Map<FuncLlvmId, FuncLlvm> functions;
  public Set<String>               variableNames;

  public List<String> typeDefLines() {
    List<String> typeDefLines = this.typeDefLines;
    if (typeDefLines != null) {
      return typeDefLines;
    }
    return this.typeDefLines = new ArrayList<>();
  }


  public Map<KindId, String> kindNames() {
    Map<KindId, String> kindNames = this.kindNames;
    if (kindNames != null) {
      return kindNames;
    }
    return this.kindNames = new HashMap<>();
  }

  public List<String> lines() {
    List<String> ret = new ArrayList<>();

    ret.addAll(typeDefLines());

    var functions = this.functions;
    if (functions != null) {
      functions.entrySet()
               .stream()
               .sorted(Comparator.comparing(x -> x.getKey().id()))
               .map(Map.Entry::getValue)
               .forEachOrdered(func -> {
                 ret.add("");
                 ret.addAll(func.lines);
               });
    }

    return ret;
  }
}
