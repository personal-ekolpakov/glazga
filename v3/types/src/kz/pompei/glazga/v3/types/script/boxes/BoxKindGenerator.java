package kz.pompei.glazga.v3.types.script.boxes;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.v3.types.engine.script.kind.KindEmbed;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.FieldCode;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import kz.pompei.glazga.v3.types.script.boxes.kind_gen.Destiny;
import kz.pompei.glazga.v3.types.script.boxes.kind_gen.FieldDefine;
import kz.pompei.glazga.v3.types.script.boxes.kind_gen.StoneKindDefine;
import kz.pompei.glazga.v3.types.script.boxes.kind_gen.StoneValueDefine;
import lombok.NonNull;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public class BoxKindGenerator extends Box {
  public Destiny   destiny;
  public String    name;
  public String    code;
  public BoxId     ownerId;
  public KindEmbed kindEmbed;

  public Map<StoneKindCode, StoneKindDefine>   stoneKinds;
  public Map<StoneValueCode, StoneValueDefine> stoneValues;
  public Map<FieldCode, FieldDefine>           fields;

  @Override protected @NonNull List<String> listToDisplay() {
    List<String> list = new ArrayList<>();
    list.add(Fields.name + '=' + name);
    if (ownerId != null) {
      list.add(Fields.ownerId + '=' + ownerId);
    }

    if (stoneKinds != null) {
      List<Map.Entry<StoneKindCode, StoneKindDefine>> lst = stoneKinds.entrySet()
                                                                      .stream()
                                                                      .sorted(Comparator.comparing(x -> x.getValue().order))
                                                                      .toList();
      for (final Map.Entry<StoneKindCode, StoneKindDefine> x : lst) {
        list.add("[typ:id=" + x.getKey() + ",name=" + x.getValue().name
                 + ',' + StoneKindDefine.Fields.typeGenGotId + '=' + x.getValue().typeGenGotId + ']');
      }
    }
    if (stoneValues != null) {
      List<Map.Entry<StoneValueCode, StoneValueDefine>> lst = stoneValues.entrySet()
                                                                         .stream()
                                                                         .sorted(Comparator.comparing(x -> x.getValue().order))
                                                                         .toList();
      for (final Map.Entry<StoneValueCode, StoneValueDefine> x : lst) {
        list.add("[val:id=" + x.getKey() + ",name=" + x.getValue().name
                 + "," + StoneValueDefine.Fields.valueGotId + '=' + x.getValue().valueGotId + ']');
      }
    }

    return list;
  }

  public static BoxKindGenerator destiny(Destiny destiny) {
    BoxKindGenerator ret = new BoxKindGenerator();
    ret.destiny = destiny;
    return ret;
  }

  public BoxKindGenerator kindEmbed(KindEmbed kindEmbed) {
    this.kindEmbed = kindEmbed;
    return this;
  }

  public BoxKindGenerator name(String name) {
    this.name = name;
    return this;
  }

  public BoxKindGenerator code(String code) {
    this.code = code;
    return this;
  }

  public BoxKindGenerator field(@NonNull String id, String name, String typeGotId) {
    if (fields == null) {
      fields = new HashMap<>();
    }

    FieldDefine fieldDefine = new FieldDefine();
    fieldDefine.alignBytes = 4;
    fieldDefine.order      = fields.values().stream().mapToDouble(x -> x.order).max().orElse(0) + 1;
    fieldDefine.typeGotId  = GotId.of(typeGotId);
    fieldDefine.name       = name;

    fields.put(FieldCode.of(id), fieldDefine);

    return this;
  }

}
