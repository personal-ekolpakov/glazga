package kz.pompei.glazga.v3.types.ids.script;

import kz.pompei.glazga.convert.StringRepresented;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@StringRepresented
@RequiredArgsConstructor
public class ModuleId {
  public final @NonNull ProgramGenId  progId;
  public final @NonNull ModuleProfile profile;

  @StringRepresented
  public static ModuleId parse(String id) {
    if (id == null || id.isEmpty()) {
      return null;
    }

    var programGenId = ProgramGenId.of(id.substring(1));
    var profile      = ModuleProfile.valueByChar(id.charAt(0));

    if (profile == null) {
      return null;
    }

    return of(programGenId, profile);
  }

  public static ModuleId of(@NonNull ProgramGenId programGenId, @NonNull ModuleProfile profile) {
    return new ModuleId(programGenId, profile);
  }

  @StringRepresented public String strValue() {
    return profile.c + progId.id;
  }

}
