package kz.pompei.glazga.v3.types.engine.script.kind;

public enum KindEmbedType {
  INT, FLOAT
}
