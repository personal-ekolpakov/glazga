package kz.pompei.glazga.v3.types.engine.dia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static java.util.stream.Collectors.joining;

@Builder
@RequiredArgsConstructor
public class DiaMessage {
  public final @NonNull HappenedObject happenedObject;
  public final @NonNull DiaType        type;
  public final @NonNull List<Msg>      msgParts;

  @Override public String toString() {
    return "DIAGNOSTIC: " + type + " " + message();
  }

  public String message() {
    List<Msg> lst = new ArrayList<>(msgParts);
    lst.add(happenedObject.happenedMsg());
    return lst.stream().map(Msg::message).collect(joining(". "));
  }

  public boolean isError() {
    return type == DiaType.ERR;
  }

  public String messageLines(@NonNull String prefix) {
    List<String> lines = new ArrayList<>();

    for (final Msg msgPart : msgParts) {
      lines.addAll(Arrays.asList(msgPart.message().split("\\n")));
    }

    if (lines.isEmpty()) {
      return "";
    }

    StringBuilder sb = new StringBuilder();

    String spaces = " ".repeat(prefix.length());
    String pre    = prefix;
    for (final String line : lines) {
      sb.append(pre).append(line).append("\n");
      pre = spaces;
    }

    return sb.toString();
  }
}
