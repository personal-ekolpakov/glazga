package kz.pompei.glazga.v3.types.script.boxes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kz.pompei.glazga.convert.DiscriminatorPostfix;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.script.Area;
import kz.pompei.glazga.v3.types.script.got.Got;
import lombok.NonNull;

@DiscriminatorPostfix("type")
public abstract class Box {

  public Area area;

  public Map<GotId, Got> gg;

  public Box area(double left, double top, double width, double height) {
    Area area = new Area();
    area.left   = left;
    area.top    = top;
    area.width  = width;
    area.height = height;
    this.area   = area;
    return this;
  }

  protected abstract @NonNull List<String> listToDisplay();

  @Override public String toString() {
    return getClass().getSimpleName() + '{' + String.join(",", listToDisplay()) + '}';
  }

  public Box got(String id, Got got) {
    if (gg == null) {
      gg = new HashMap<>();
    }
    gg.put(GotId.of(id), got);
    return this;
  }

}
