package kz.pompei.glazga.v3.types.engine.script.got;

import java.util.Map;
import kz.pompei.glazga.v3.calc.Once;
import kz.pompei.glazga.v3.types.engine.CalcStones;
import kz.pompei.glazga.v3.types.engine.Stones;
import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.box.TrBox;
import kz.pompei.glazga.v3.types.engine.script.box.TrBoxKindGenerator;
import kz.pompei.glazga.v3.types.engine.translating.GotExtractingKindId;
import kz.pompei.glazga.v3.types.engine.translating.HasStoneGets;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.ids.type.StoneKindCode;
import kz.pompei.glazga.v3.types.ids.type.StoneValueCode;
import kz.pompei.glazga.v3.types.script.got.GotRefKind;
import lombok.NonNull;

/**
 * Ссылка на генератор типа в этой программе-генераторе
 */
public class TrGotRefKind extends TrGot implements GotExtractingKindId, HasStoneGets {
  public final @NonNull GotRefKind data;

  public TrGotRefKind(@NonNull BoxId ownerId, @NonNull GotId id, @NonNull GotRefKind data, @NonNull TrProgramGen program) {
    super(ownerId, id, program);
    this.data = data;
  }

  private final CalcStones               calcStones         = new CalcStones(this);
  private final Once<TrBoxKindGenerator> trBoxKindGenerator = new Once<>(this::doCalc_trBoxKindGenerator);
  private final Once<KindId>             kindIdOnce         = new Once<>(this::doCalcKindId);

  @Override public KindId extrackKindId() {
    return kindIdOnce.get();
  }

  @Override public Map<StoneKindCode, GotId> stoneKindGets() {
    return data.stoneKindGets;
  }

  @Override public Map<StoneValueCode, GotId> stoneValuesGets() {
    return data.stoneValueGets;
  }

  private TrBoxKindGenerator doCalc_trBoxKindGenerator() {

    BoxId kindGeneratorBoxId = data.kindGeneratorBoxId;
    if (kindGeneratorBoxId == null) {
      err(Msg.of("M6H2BhBYuY"));
      return null;
    }

    TrBox trBox = program.getOrLoadBoxById(kindGeneratorBoxId);
    if (trBox == null) {
      err(Msg.of("kVIm1uxj22"));
      return null;
    }

    if (trBox instanceof TrBoxKindGenerator ret) {
      return ret;
    }

    err(Msg.of("nOQ1B9VTY9"));
    return null;
  }

  private KindId doCalcKindId() {

    TrBoxKindGenerator trBoxKindGenerator = this.trBoxKindGenerator.get();

    if (trBoxKindGenerator == null) {
      err(Msg.of("FURTwJIjQZ"));
      return null;
    }

    Stones stones = calcStones.getStones();

    return trBoxKindGenerator.createKind(stones).id();

  }
}
