package kz.pompei.glazga.v3.types.func;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
@RequiredArgsConstructor
public class StoneKindDef {
  public final @NonNull String name;
  public final          double order;

  public static StoneKindDef of(double order, @NonNull String name) {
    return new StoneKindDef(name, order);
  }
}
