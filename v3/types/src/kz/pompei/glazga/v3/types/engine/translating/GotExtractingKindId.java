package kz.pompei.glazga.v3.types.engine.translating;

import kz.pompei.glazga.v3.types.engine.TranslatorStorage;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import kz.pompei.glazga.v3.types.script.got.Got;

public interface GotExtractingKindId {
  /**
   * Вычисляет тип, который должен быть определён данным выражением.
   * <p>
   * Это НЕ тип значение, а само выражение должно вычислять тип на этапе компиляции.
   * <p>
   * Не всякий {@link Got} умеет это делать
   *
   * @return возвращает идентификатор типа, который уже просчитан по структуре и можно получить сам тип по методу
   * {@link TranslatorStorage#getKindById(KindId)} вызванного у <code>translator.storage</code>
   */
  KindId extrackKindId();
}
