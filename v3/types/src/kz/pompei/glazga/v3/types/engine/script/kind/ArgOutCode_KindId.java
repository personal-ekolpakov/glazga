package kz.pompei.glazga.v3.types.engine.script.kind;

import kz.pompei.glazga.v3.types.ids.script.ArgOutCode;
import kz.pompei.glazga.v3.types.ids.type.KindId;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode
@RequiredArgsConstructor
public class ArgOutCode_KindId {
  public final @NonNull ArgOutCode code;
  public final @NonNull KindId     kindId;

  public static @NonNull ArgOutCode_KindId of(@NonNull ArgOutCode code, @NonNull KindId kindId) {
    return new ArgOutCode_KindId(code, kindId);
  }
}
