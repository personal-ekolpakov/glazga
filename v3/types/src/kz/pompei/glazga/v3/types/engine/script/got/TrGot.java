package kz.pompei.glazga.v3.types.engine.script.got;

import kz.pompei.glazga.v3.types.engine.dia.Msg;
import kz.pompei.glazga.v3.types.engine.script.TrAbstract;
import kz.pompei.glazga.v3.types.engine.script.TrProgramGen;
import kz.pompei.glazga.v3.types.engine.script.box.TrBox;
import kz.pompei.glazga.v3.types.ids.script.BotId;
import kz.pompei.glazga.v3.types.ids.script.BoxId;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import lombok.NonNull;

public abstract class TrGot extends TrAbstract {
  public final @NonNull BoxId ownerId;
  public final @NonNull GotId id;

  protected TrGot(@NonNull BoxId ownerId, @NonNull GotId id, @NonNull TrProgramGen program) {
    super(program);
    this.ownerId = ownerId;
    this.id      = id;
  }

  @Override public @NonNull BotId extractId() {
    return id;
  }

  protected final TrGot getTrGotById(@NonNull GotId gotId) {
    TrBox owner = program.getOrLoadBoxById(ownerId);
    if (owner == null) {
      err(Msg.of("7o806z7lMu", "ownerBoxId", ownerId));
      return null;
    }

    TrGot ret = owner.gotten.get(gotId);
    if (ret != null) {
      return ret;
    }

    err(Msg.of("amzLfLIRfK", "gotId", gotId));
    return null;
  }

}
