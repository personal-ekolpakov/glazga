package kz.pompei.glazga.v3.types.script.boxes;

import java.util.List;
import kz.pompei.glazga.v3.types.ids.script.ArgOutCode;
import kz.pompei.glazga.v3.types.ids.script.GotId;
import lombok.NonNull;

public class BoxReturn extends Box {
  public ArgOutCode outCode;
  public GotId      outGotId;

  public static BoxReturn outCode(String outCode) {
    BoxReturn ret = new BoxReturn();
    ret.outCode = ArgOutCode.of(outCode);
    return ret;
  }

  public BoxReturn outGotId(String outGotId) {
    this.outGotId = GotId.of(outGotId);
    return this;
  }

  @Override protected @NonNull List<String> listToDisplay() {
    return List.of("outCode=" + outCode);
  }

}
