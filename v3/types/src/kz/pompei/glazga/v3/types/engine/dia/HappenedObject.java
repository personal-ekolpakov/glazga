package kz.pompei.glazga.v3.types.engine.dia;

import lombok.NonNull;

/**
 * Этот интерфейс определяется у того объекта, который хочет говорить, что какое-то событие произошло в нём
 */
public interface HappenedObject {
  @NonNull Msg happenedMsg();
}
