package kz.pompei.glazga.v3.calc;

import java.util.function.Supplier;

public class Once<Res> {

  private       Res           calculatedValue;
  private final Supplier<Res> supplier;
  private       boolean       inCalculation = false;
  private       boolean       calculated    = false;

  public Once(Supplier<Res> supplier) {
    this.supplier = supplier;
  }

  public Res get() {
    if (calculated) {
      return calculatedValue;
    }
    if (inCalculation) {
      throw new RuntimeException("6AdV7R7Ki7 :: Recursive calculation detected");
    }
    try {
      inCalculation = true;
      return calculatedValue = supplier.get();
    } finally {
      calculated    = true;
      inCalculation = false;
    }
  }

}
