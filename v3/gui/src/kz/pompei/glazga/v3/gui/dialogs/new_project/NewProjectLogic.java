package kz.pompei.glazga.v3.gui.dialogs.new_project;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import kz.pompei.glazga.v3.gui.dialogs.common.DialogLogic;
import lombok.NonNull;

public class NewProjectLogic extends DialogLogic {

  private final NewProjectDialog dialog;

  @Override protected JButton buttonCancel() {
    return dialog.main.buttonCancel;
  }

  @Override protected JButton buttonSave() {
    return dialog.main.buttonSave;
  }

  @Override protected JDialog dialog() {
    return dialog;
  }

  @Override protected JComponent mainPanel() {
    return dialog.main;
  }

  public NewProjectLogic(@NonNull NewProjectDialog dialog) {
    this.dialog = dialog;
    initDialogBasically();
  }

  public String create() {

    dialog.pack();
    dialog.setVisible(true);


    if (cancel) {
      return null;
    }

    return dialog.main.fieldName.getText();
  }
}
