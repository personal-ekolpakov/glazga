package kz.pompei.glazga.v3.gui.projects.pg;

import java.util.Optional;
import java.util.Set;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.utils.db.Db;
import kz.pompei.glazga.v3.gui.projects.ProjectService;
import kz.pompei.glazga.v3.gui.projects.model.DetailDirRecord;
import kz.pompei.glazga.v3.gui.projects.model.DetailDirRecordPortion;
import kz.pompei.v3.storage.Storage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProjectServicePg implements ProjectService {
  private final @NonNull Db     db;
  private final @NonNull String projectId;

  @Override public String getProjectName() {
    return db.query()
             .sql("select value from {.my}.glazga_project_params where id = {id}")
             .param("my", projectId)
             .param("id", "project_name")
             .str();
  }

  @Override public void setProjectName(@NonNull String projectName) {
    db.query()
      .sql("update {.my}.glazga_project_params set value = {projectName} where id = {id}")
      .param("my", projectId)
      .param("id", "project_name")
      .param("projectName", projectName)
      .update();
  }

  @Override public @NonNull Storage getDetailStorage(@NonNull String detailId) {
    return new StorageDetailPg(db, projectId, detailId);
  }

  @Override public void setDetailName(@NonNull String detailId, String detailName) {
    getDetailStorage(detailId).set("name", detailName);
  }

  @Override public String getDetailName(@NonNull String detailId) {
    return getDetailStorage(detailId).get("name").orElse(null);
  }

  @Override public void setDetailType(@NonNull String detailId, String detailType) {
    getDetailStorage(detailId).set("type", detailType);
  }

  @Override public String getDetailType(@NonNull String detailId) {
    return getDetailStorage(detailId).get("type").orElse(null);
  }

  @Override public void setDetailPath(@NonNull String detailId, String path) {
    path = path.trim();

    while (path.startsWith("/")) {
      path = path.substring(1);
    }

    while (path.endsWith("/")) {
      path = path.substring(0, path.length() - 1);
    }

    getDetailStorage(detailId).set("path", path);
  }

  @Override public String getDetailPath(@NonNull String detailId) {
    return getDetailStorage(detailId).get("path").orElse(null);
  }

  @Override public @NonNull DetailDirRecordPortion loadDetailPortion(@NonNull String parentDirId, Page page) {

    DetailDirRecordPortion ret = new DetailDirRecordPortion();

    ret.list = db.query()
                 .page(page)
                 .param("my", projectId)
                 .param("parentDirId", parentDirId)
                 .sql("""
                        select

                                  id as "id"         ,
                        is_directory as "dir"        ,
                                name as "name"       ,
                                type as "type"       ,
                          created_at as "createdAt"  ,
                         modified_at as "modifiedAt"

                        from  {.my}.detail_dir
                        where parent_id = {parentDirId}
                        order by name
                        """)
                 .list(DetailDirRecord.class);

    ret.hasMore = true;

    return ret;
  }

  @Override public @NonNull Optional<DetailDirRecord> loadDetailItem(String detailId) {
    return db.query()
             .param("my", projectId)
             .param("id", detailId)
             .sql("""
                    select

                              id as "id"         ,
                    is_directory as "dir"        ,
                            name as "name"       ,
                            type as "type"       ,
                      created_at as "createdAt"  ,
                     modified_at as "modifiedAt"

                    from  {.my}.detail_dir
                    where id = {id}
                    """)
             .one(DetailDirRecord.class);
  }

  @Override public void deleteDetail(@NonNull String detailId) {

    db.query()
      .param("my", projectId)
      .param("id", detailId)
      .sql("delete from  {.my}.detail where detail_id = {id}")
      .update();

    db.query()
      .param("my", projectId)
      .param("id", detailId)
      .sql("delete from  {.my}.detail_dir where id = {id}")
      .update();

  }

  @Override public @NonNull Set<String> findBoxIdsInRect(@NonNull String detailId, @NonNull Rect rect) {

    return db.query()
             .param("my", projectId)
             .param("detailId", detailId)
             .param("x1", rect.left)
             .param("y1", rect.top)
             .param("x2", rect.right)
             .param("y2", rect.bottom)
             .sql("""
                    select box_id from {.my}.detail_area
                    where detail_id =  {detailId}
                      and area      && BOX(point({x1},{y1}), point({x2},{y2}))
                    """)
             .extractSet(rs -> rs.getString(1));
  }
}
