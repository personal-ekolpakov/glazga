package kz.pompei.glazga.v3.gui.panels;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import javax.swing.JComponent;
import kz.pompei.glazga.v3.gui.context.NodeContext;
import kz.pompei.glazga.v3.gui.context.rain.RainEvent;
import lombok.NonNull;
import lombok.SneakyThrows;

import static java.util.Objects.requireNonNull;

public class PanelNode {

  public                 boolean       using = false;
  public final @NonNull  PanelContent  content;
  private final @NonNull String        nodeType;
  private @NonNull       UpDownConnect up;
  private final          String        panelId;

  private static final Map<String, Class<?>> PANEL_CLASSES = Map.of(
    PanelSplitter.class.getSimpleName(), PanelSplitter.class
    , PanelHolder.class.getSimpleName(), PanelHolder.class
  );

  @SneakyThrows
  public PanelNode(@NonNull NodeContext nodeContext, @NonNull UpDownConnect up) {
    this.up      = up;
    this.panelId = nodeContext.storage().get(up.getDownPanelIdPath()).orElseThrow();

    nodeType = nodeContext.storage().get(panelId + ".nodeType").orElseGet(() -> {
      String t = PanelHolder.class.getSimpleName();
      nodeContext.storage().set(panelId + ".nodeType", t);
      return t;
    });

    Class<?> panelClass = PANEL_CLASSES.get(nodeType);

    if (panelClass == null) {
      String t = PanelHolder.class.getSimpleName();
      nodeContext.storage().set(panelId + '.' + "nodeType", t);
      panelClass = PANEL_CLASSES.get(t);
    }

    requireNonNull(panelClass, "WPgrIFGKtf :: panelClass == null. nodeType = " + nodeType);

    content = (PanelContent) panelClass.getConstructor(NodeContext.class, Supplier.class)
                                       .newInstance(nodeContext, (Supplier<UpDownConnect>) () -> this.up);

  }

  public PanelNode updateUp(@NonNull UpDownConnect up) {
    this.up = up;
    return this;
  }

  public @NonNull JComponent getComponent() {
    return content.getComponent();
  }

  public @NonNull List<UpDownConnect> getChildrenRefs() {
    return content.children();
  }

  public void refreshState() {
    content.refreshState();
  }

  public @NonNull String getNodeType() {
    return nodeType;
  }

  public void destroy() {
    content.destroy();
  }

  public String getPanelId() {
    return panelId;
  }

  public void acceptRainEvent(@NonNull RainEvent rainEvent)  throws Exception {
    PanelContent content = this.content;
    if (content != null) {
      content.acceptRainEvent(rainEvent);
    }
  }
}
