package kz.pompei.glazga.v3.gui.projects.model;

import java.util.Date;
import kz.pompei.glazga.utils.StrUtil;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItem;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemCell;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemId;
import lombok.NonNull;

public class DetailDirRecord {
  public String  id;
  public boolean dir;
  public String  name;
  public String  type;
  public Date    createdAt;
  public Date    modifiedAt;

  public ProjectItem toProjectItem(@NonNull String workspaceId, @NonNull String projectId) {

    ProjectItemId itemId = dir
      ? ProjectItemId.folder(workspaceId, projectId, id)
      : ProjectItemId.detail(workspaceId, projectId, id);

    ProjectItem ret = new ProjectItem(itemId.strId(), dir);
    if (dir) {
      ret.nodeType = "folder";
    } else {
      ret.nodeType = "detail-" + (type == null ? "unknown" : type);
    }

    ret.cells.add(new ProjectItemCell("name", name, "name"));

    String d = StrUtil.displayDelay(new Date(), modifiedAt);
    ret.cells.add(new ProjectItemCell("description", d, "description"));

    return ret;
  }
}
