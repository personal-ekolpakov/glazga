package kz.pompei.glazga.v3.gui.projects.pg;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import kz.pompei.glazga.utils.db.Db;
import kz.pompei.glazga.utils.db.DbQuery;
import kz.pompei.v3.storage.Storage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static java.util.stream.Collectors.toMap;

@RequiredArgsConstructor
public class StorageDetailPg implements Storage {
  private final @NonNull Db     db;
  private final @NonNull String projectId;
  private final @NonNull String detailId;

  private DbQuery query() {
    return db.query().param("my", projectId).param("detail_id", detailId);
  }

  @Override public void set(@NonNull String key, String value) {

    query().param("path", key)
           .param("value", value)
           .sql("""
                  insert into {.my}.detail ( detail_id,   path,   value )
                                    values ({detail_id}, {path}, {value})
                  on conflict (detail_id, path) do update set value = {value}
                  """)
           .update();

  }

  @Override public @NonNull Set<String> findKeys(String keyPrefix) {
    return query().param("prefix", keyPrefix + "%")
                  .sql("select path from {.my}.detail where detail_id = {detail_id} and path like {prefix}")
                  .extractSet(rs -> rs.getString(1));
  }

  @Override public @NonNull Optional<String> get(@NonNull String key) {
    return query().param("path", key)
                  .sql("select value from {.my}.detail where detail_id = {detail_id} and path = {path}")
                  .strOpt();
  }

  @Override public @NonNull Map<String, Optional<String>> gets(@NonNull Set<String> keys) {
    return keys.stream()
               .map(key -> Map.entry(key, get(key)))
               .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  @Override public @NonNull String fullPathOf(@NonNull String key) {
    throw new RuntimeException("2024-04-28 11:42 Not impl yet StorageDetailPg.fullPathOf()");
  }

  @Override public Storage cd(String path) {
    throw new RuntimeException("2024-04-28 11:42 Not impl yet StorageDetailPg.cd()");
  }

  @Override public void flush() {
    // ignore
  }

  @Override public @NonNull Map<String, String> loadOneLevelMap(Set<String> prefixes) {
    return db.withConnection(connection -> {

      connection.setAutoCommit(false);

      try (Statement statement = connection.createStatement()) {
        statement.execute("create temp table tmp_pre (pre varchar(500) not null) on commit drop");
      }

      try (PreparedStatement ps = connection.prepareStatement("insert into tmp_pre (pre) values (?)")) {

        for (final String prefix : prefixes) {
          ps.setString(1, prefix);
          ps.addBatch();
        }

        ps.executeBatch();
      }

      try (PreparedStatement ps = connection.prepareStatement(
        "select d.path, d.value from " + projectId + ".detail d, tmp_pre t" +
        " where d.detail_id = ? and d.path like t.pre || '%'")) {

        ps.setString(1, detailId);

        try (ResultSet rs = ps.executeQuery()) {

          Map<String, String> ret = new HashMap<>();

          while (rs.next()) {
            ret.put(rs.getString("path"), rs.getString("value"));
          }

          return ret;
        }

      }

    });
  }
}
