package kz.pompei.glazga.v3.gui.dialogs.edit_workspace_connect;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import kz.pompei.glazga.utils.JDialogUtil;

public class EditWorkspaceConnectPanel extends JPanel {

  public final JLabel         hostLabel     = new JLabel("Хост:");
  public final JTextField     hostField     = new JTextField();
  public final JLabel         portLabel     = new JLabel("Порт:");
  public final JTextField     portField     = new JTextField();
  public final JLabel         usernameLabel = new JLabel("Имя пользователя:");
  public final JTextField     usernameField = new JTextField();
  public final JLabel         passwordLabel = new JLabel("Пароль:");
  public final JPasswordField passwordField = new JPasswordField();
  public final JLabel         dbNameLabel   = new JLabel("Имя БД:");
  public final JTextField     dbNameField   = new JTextField();

  public final JEditorPane connectInfo = new JEditorPane();


  public final JButton checkButton  = new JButton("Проверить связь");
  public final JButton saveButton   = new JButton("Сохранить");
  public final JButton cancelButton = new JButton("Отменить");

  public final JPanel dialogButtons = new JPanel();

  public EditWorkspaceConnectPanel() {
    setLayout(new GridBagLayout());
    setBorder(new EmptyBorder(10, 10, 10, 10));

    hostField.setPreferredSize(new Dimension(200, 30));
    portField.setPreferredSize(new Dimension(80, 30));
    dbNameField.setPreferredSize(new Dimension(200, 30));
    usernameField.setPreferredSize(new Dimension(200, 30));
    passwordField.setPreferredSize(new Dimension(200, 30));

    hostField.setMinimumSize(new Dimension(200, 30));
    portField.setMinimumSize(new Dimension(80, 30));
    dbNameField.setMinimumSize(new Dimension(200, 30));
    usernameField.setMinimumSize(new Dimension(200, 30));
    passwordField.setMinimumSize(new Dimension(200, 30));

    connectInfo.setBackground(getBackground());
    connectInfo.setEditable(false);
    connectInfo.setText("Hello World");
    connectInfo.setMinimumSize(new Dimension(250, 100));
    connectInfo.setPreferredSize(new Dimension(250, 100));

    JDialogUtil.borders(hostField, 5, 5, 5, 5);
    JDialogUtil.borders(portField, 5, 5, 5, 5);
    JDialogUtil.borders(dbNameField, 5, 5, 5, 5);
    JDialogUtil.borders(usernameField, 5, 5, 5, 5);
    JDialogUtil.borders(passwordField, 5, 5, 5, 5);

    JDialogUtil.borders(portLabel, 0, 5, 0, 0);

    {
      var c = new GridBagConstraints();

      c.weightx = 1;
      c.weighty = 1;
      c.gridy   = 0;

      c.gridx  = 0;
      c.fill   = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.LINE_END;
      add(hostLabel, c);

      c.gridx  = 1;
      c.fill   = GridBagConstraints.HORIZONTAL;
      c.anchor = GridBagConstraints.CENTER;
      add(hostField, c);

      c.gridx  = 2;
      c.fill   = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.LINE_END;
      add(portLabel, c);

      c.gridx  = 3;
      c.fill   = GridBagConstraints.HORIZONTAL;
      c.anchor = GridBagConstraints.CENTER;
      add(portField, c);

      c.gridy++;

      c.gridx  = 0;
      c.fill   = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.LINE_END;
      add(usernameLabel, c);

      c.gridx  = 1;
      c.fill   = GridBagConstraints.HORIZONTAL;
      c.anchor = GridBagConstraints.CENTER;
      add(usernameField, c);

      c.gridy++;

      c.gridx  = 0;
      c.fill   = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.LINE_END;
      add(passwordLabel, c);

      c.gridx  = 1;
      c.fill   = GridBagConstraints.HORIZONTAL;
      c.anchor = GridBagConstraints.CENTER;
      add(passwordField, c);

      c.gridy++;

      c.gridx  = 0;
      c.fill   = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.LINE_END;
      add(dbNameLabel, c);

      c.gridx  = 1;
      c.fill   = GridBagConstraints.HORIZONTAL;
      c.anchor = GridBagConstraints.CENTER;
      add(dbNameField, c);

      c.gridy++;

      JScrollPane connectInfoScroll = new JScrollPane(connectInfo);

      double savedWeightY = c.weighty;
      c.gridx     = 1;
      c.fill      = GridBagConstraints.BOTH;
      c.anchor    = GridBagConstraints.CENTER;
      c.weighty   = 10;
      c.gridwidth = 3;
      add(connectInfoScroll, c);
      c.weighty   = savedWeightY;
      c.gridwidth = 1;

      c.gridy++;

      c.gridx  = 1;
      c.fill   = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.LINE_START;
      add(checkButton, c);

      c.gridy++;

      c.gridx     = 1;
      c.gridwidth = 3;
      c.fill      = GridBagConstraints.NONE;
      add(dialogButtons, c);
    }

    dialogButtons.setLayout(new GridBagLayout());
    dialogButtons.setBorder(new EmptyBorder(10, 0, 0, 0));

    {
      var c = new GridBagConstraints();
      c.gridy = 0;

      c.gridx = 0;
      dialogButtons.add(saveButton, c);

      c.gridx  = 1;
      c.insets = new Insets(0, 10, 0, 0);
      dialogButtons.add(cancelButton, c);
    }

  }
}
