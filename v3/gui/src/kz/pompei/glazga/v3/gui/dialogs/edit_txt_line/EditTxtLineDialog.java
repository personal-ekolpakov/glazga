package kz.pompei.glazga.v3.gui.dialogs.edit_txt_line;

import java.awt.Point;
import java.nio.file.Path;
import javax.swing.JComponent;
import javax.swing.JFrame;
import kz.pompei.glazga.utils.Locations;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.glazga.v3.gui.dialogs.common.DialogParent;

public class EditTxtLineDialog extends DialogParent {

  public final EditTxtLinePanel main = new EditTxtLinePanel();

  @Override protected JComponent mainPanel() {
    return main;
  }

  public EditTxtLineDialog(JFrame owner, Point mousePointOnScreen) {
    super(owner);
    initDialog(mousePointOnScreen);
  }


  public static void main(String[] args) {
    EditTxtLineDialog dialog = new EditTxtLineDialog(null, null);

    dialog.setTitle("Заголовок окна");
    dialog.pack();

    Path rootFolder = Locations.buildConfig();
    SizeLocationSaver.into(rootFolder.resolve("sizes_and_locations"))
                     .overJFrame("EditTxtLine-form", dialog, false);

    dialog.setVisible(true);

    System.exit(0);
  }

}
