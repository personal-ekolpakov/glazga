package kz.pompei.glazga.v3.gui.projects.pg;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.utils.RndUtil;
import kz.pompei.glazga.utils.db.ConnectionCreator;
import kz.pompei.glazga.utils.db.Db;
import kz.pompei.glazga.utils.db.DbIterator;
import kz.pompei.glazga.utils.db.DbQuery;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ConnectParams;
import kz.pompei.glazga.v3.gui.projects.ProjectService;
import kz.pompei.glazga.v3.gui.projects.WorkspaceService;
import kz.pompei.glazga.v3.gui.projects.model.ProjectRecord;
import kz.pompei.glazga.v3.gui.projects.model.ProjectRecordPortion;
import lombok.NonNull;

public class WorkspaceServicePg implements WorkspaceService, AutoCloseable {
  private final ConnectParams    connectParams;
  private final HikariDataSource dataSource;

  public static final String project_id_prefix = "prj_";

  private final Db db = new Db(new ConnectionCreator() {
    @Override public Connection create() throws SQLException {
      return dataSource.getConnection();
    }
  });

  public WorkspaceServicePg(@NonNull ConnectParams connectParams) {
    this.connectParams = connectParams;

    HikariConfig config = new HikariConfig();

    config.setJdbcUrl(connectParams.url());
    config.setUsername(connectParams.username);
    config.setPassword(connectParams.password);

    config.addDataSourceProperty("cachePrepStmts", "true");
    config.addDataSourceProperty("prepStmtCacheSize", "250");
    config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

    dataSource = new HikariDataSource(config);
  }

  @Override public String typeDisplayStr() {
    return "PostgreSQL";
  }

  @Override public String workspaceName() {
    return db.query().sql("select value from public.glazga_workspace_params where id = 'workspace_name'").str();
  }

  @Override public void setWorkspaceName(String workspaceName) {

    String ws_name = Optional.ofNullable(workspaceName)
                             .map(String::trim)
                             .filter(s -> s.length() > 0)
                             .orElse(initialWorkspaceName(connectParams));

    db.query()
      .sql("update public.glazga_workspace_params set value = {ws_name} where id = 'workspace_name'")
      .param("ws_name", ws_name)
      .update()
      .throwIfNotSingle("DPy5SCAaoG");

  }

  @Override public void close() {
    dataSource.close();
  }

  public static String initialWorkspaceName(ConnectParams connectParams) {
    return "PG:" + connectParams.host + ":" + connectParams.port + "/" + connectParams.dbName;
  }

  public void initialize() {
    DbQuery query = db.query().param("glazga_workspace_params", "glazga_workspace_params");

    if (query.copy().sql("""
                           select exists(select from information_schema.tables
                            where table_schema = 'public'
                            and table_name = {glazga_workspace_params})
                           """).bool()) {
      return;
    }


    query.copy().sql("""
                       create table public.{.glazga_workspace_params}
                       (
                          id varchar(500) not null primary key,
                          value varchar(1000) not null
                       )
                       """).update();

    query.copy().sql("""
                       insert into public.{.glazga_workspace_params}
                       (id, value) values ('workspace_name', {name})
                       """)
         .param("name", initialWorkspaceName(connectParams))
         .update();

    //noinspection SpellCheckingInspection
    query.copy().sql("""
                       create or replace function rnd_id() returns text
                           language plpgsql
                       as
                       $$
                       declare
                           ENG  constant TEXT := 'abcdefghijklmnopqrstuvwxyz';
                           DEG  constant TEXT := '0123456789';
                           MORE constant TEXT := '_$';
                           ALL1          TEXT := lower(ENG) || upper(ENG) || DEG || MORE;
                           ALL1_LENGTH   int  := length(ALL1);
                           ret           TEXT;
                       begin
                           ret := '';
                           FOR i IN 1..16
                               LOOP
                                   ret := ret || substring(ALL1 from (1 + floor(random() * ALL1_LENGTH))::int for 1);
                               END LOOP;
                           return ret;
                       end;
                       $$;
                       """).update();
  }

  @Override public String connectParamsStr() {
    return initialWorkspaceName(connectParams);
  }

  @Override public String createProject(@NonNull String name) {

    String projectId = project_id_prefix + RndUtil.strEng(15).toLowerCase();

    //<editor-fold desc="glazga_project_params">

    db.query()
      .param("projectId", projectId)
      .sql("create schema {.projectId}")
      .update();

    DbQuery query = db.query()
                      .param("my", projectId);

    query.copy().sql("""
                       create table {.my}.glazga_project_params (
                         id    varchar(100) primary key,
                         value varchar(500) not null
                       )
                       """).update();

    DbQuery insert = query.copy().sql("""
                                        insert into {.my}.glazga_project_params
                                               ( id,   value )
                                        values ({id}, {value})
                                        """);

    insert.update(Map.of("id", "project_name", "value", name));
    insert.update(Map.of("id", "db_structure_version", "value", "0.1"));

    //</editor-fold>

    query.copy().sql("""
                       create table {.my}.detail (
                         detail_id     varchar(20)   not null,
                         path          varchar(500)  not null,
                         value         varchar(1000) not null,
                         primary key(detail_id, path)
                       )
                       """).update();

    //<editor-fold desc="detail_pos">

    query.copy().sql("""
                       create table {.my}.detail_area (
                            detail_id varchar(20)  not null,
                            box_id    varchar(30)  not null,
                            area      BOX          not null,
                            primary key (detail_id, box_id)
                       )
                       """).update();

    query.copy().sql("create index on {.my}.detail_area using GIST (area)").update();

    query.copy().sql("""
                       CREATE OR REPLACE FUNCTION {.my}.text_to_BOX(area_txt TEXT)
                         RETURNS BOX
                         LANGUAGE PlPgSQL AS
                       $$
                       DECLARE
                         coordinates TEXT[];
                         x           FLOAT;
                         y           FLOAT;
                         w           FLOAT;
                         h           FLOAT;
                       BEGIN
                         coordinates := string_to_array(area_txt, ',');

                         IF array_length(coordinates, 1) < 4 THEN
                           RETURN NULL;
                         END IF;

                         BEGIN
                           x := coordinates[1]::FLOAT;
                           y := coordinates[2]::FLOAT;
                           w := coordinates[3]::FLOAT;
                           h := coordinates[4]::FLOAT;
                         EXCEPTION
                           WHEN others THEN RETURN NULL;
                         END;

                         RETURN BOX(POINT(x, y), POINT(x + w, y + h));
                       END;
                       $$
                       """).update();

    query.copy().sql("""
                       CREATE OR REPLACE FUNCTION {.my}.detail_area_trigger_function()
                         RETURNS TRIGGER
                         LANGUAGE PlPgSQL AS
                       $$
                       DECLARE
                         PTN_BOXES  constant TEXT := 'boxes\\.([\\w_$]+)\\.area';
                         new_detail_id       TEXT;
                         new_box_id          TEXT;
                         new_area            BOX ;
                         do_update           BOOL;
                       BEGIN
                         do_update := false;
                         IF (TG_OP = 'INSERT') THEN
                           IF new.path ~ PTN_BOXES THEN
                             SELECT (regexp_matches(new.path, PTN_BOXES))[1] INTO new_box_id;
                             new_area      := {.my}.text_to_BOX(new.value);
                             new_detail_id := new.detail_id;
                             do_update     := true;
                           END IF;
                         ELSIF (TG_OP = 'UPDATE') THEN
                           IF new.path ~ PTN_BOXES THEN
                             SELECT (regexp_matches(new.path, PTN_BOXES))[1] INTO new_box_id;
                             new_area      := {.my}.text_to_BOX(new.value);
                             new_detail_id := new.detail_id;
                             do_update     := true;
                           END IF;
                         ELSIF (TG_OP = 'DELETE') THEN
                           IF old.path ~ PTN_BOXES THEN
                             SELECT (regexp_matches(old.path, PTN_BOXES))[1] INTO new_box_id;
                             new_area      := null;
                             new_detail_id := old.detail_id;
                             do_update     := true;
                           END IF;
                         END IF;

                         IF do_update THEN
                           IF new_area is null THEN
                             DELETE FROM {.my}.detail_area x
                               WHERE x.detail_id = new_detail_id
                                 AND x.box_id    = new_box_id;
                           ELSE
                             INSERT INTO {.my}.detail_area (detail_id, box_id, area)
                               VALUES (new_detail_id, new_box_id, new_area)
                               ON CONFLICT (detail_id, box_id)
                               DO UPDATE SET area = new_area;
                           END IF;
                         END IF;

                         RETURN NEW;
                       END ;
                       $$
                       """).update();

    query.copy().sql("""
                       CREATE TRIGGER {.my}_detail__to__detail_area
                         AFTER INSERT OR UPDATE OR DELETE
                         ON {.my}.detail
                         FOR EACH ROW
                         EXECUTE FUNCTION {.my}.detail_area_trigger_function();
                       """).update();

    //</editor-fold>

    //<editor-fold desc="detail_dir">

    query.copy().sql("""
                        create table {.my}.detail_dir (
                         id           varchar(20)  not null,
                         parent_id    varchar(20)  not null,
                         is_directory bool         not null,
                         name         varchar(500),
                         name_uniq    varchar(500) not null,
                         type         varchar(100),
                         created_at   timestamp    not null,
                         modified_at  timestamp    not null,
                         primary key (id)
                       )
                       """).update();

    query.copy().sql("""
                       create unique index on {.my}.detail_dir (parent_id, name_uniq);
                       """).update();

    query.copy().sql("""
                       CREATE OR REPLACE FUNCTION {.my}.perform_path(path text[]) RETURNS TEXT
                           language PlPgSQL as
                       $$
                       DECLARE
                           parent_dir_id text;
                           path_element  text;
                       BEGIN
                           IF array_length(path, 1) = 0 THEN
                               return null;
                           END IF;

                           parent_dir_id := null;
                           FOREACH path_element IN ARRAY path
                               LOOP
                                   IF parent_dir_id is null THEN
                                       insert into {.my}.detail_dir (id, parent_id, is_directory, name, name_uniq, created_at, modified_at)
                                       VALUES (rnd_id(), '', true, path_element, path_element, now(), now())
                                       on conflict (parent_id, name_uniq)
                                           do update set modified_at = now()
                                       returning id into parent_dir_id;
                                   ELSE
                                       insert into {.my}.detail_dir (id, parent_id, is_directory, name, name_uniq, created_at, modified_at)
                                       VALUES (rnd_id(), parent_dir_id, true, path_element, path_element, now(), now())
                                       on conflict (parent_id, name_uniq)
                                           do update set modified_at = now()
                                       returning id into parent_dir_id;
                                   END IF;

                               END LOOP;
                           return parent_dir_id;
                       END;
                       $$
                       """).update();

    query.copy().sql("""
                       create or replace function {.my}.clean_path(path text[]) returns void
                           language PlPgSQL as
                       $$
                       declare
                           path_element     text;
                           parent_detail_id text;
                           detail_id        text;
                           is_dir           bool;
                           detail_ids       text[] := array[]::text[];
                       begin
                           parent_detail_id := '';
                           foreach path_element in array path
                               loop
                                   detail_id := null;
                                   select id, is_directory
                                   into detail_id, is_dir
                                   from {.my}.detail_dir
                                   where parent_id = parent_detail_id
                                     and name_uniq = path_element;

                                   if detail_id is null then
                                       exit;
                                   end if;

                                   if not is_dir then
                                       return;
                                   end if;

                                   parent_detail_id := detail_id;

                                   detail_ids := detail_id || detail_ids;

                               end loop;

                           foreach detail_id in array detail_ids
                               loop

                                   if exists(select * from {.my}.detail_dir where parent_id = detail_id) then
                                       return;
                                   end if;

                                   delete from {.my}.detail_dir where id = detail_id;

                               end loop;

                           return;
                       end;
                       $$
                       """).update();

    query.copy().sql("""
                       CREATE OR REPLACE FUNCTION {.my}.split_path(path TEXT)
                           RETURNS TEXT[]
                           LANGUAGE PlPgSQL AS
                       $$
                       BEGIN
                           RETURN ARRAY(
                                   SELECT TRIM(x)
                                   FROM unnest(string_to_array(path, '/')) AS t(x)
                                   WHERE TRIM(x) <> ''
                                  );
                       END;
                       $$;
                       """).update();

    query.copy().sql("""
                       create or replace function {.my}.detail_dir_trigger_function() returns trigger
                           language PlPgSQL
                       as
                       $$
                       DECLARE
                         use_detail_id      text;
                         old_path           text[];
                         new_path           text[];
                         skip_update_path   bool;
                         update_name        bool;
                         use_name           text;
                         use_name_uniq      text;
                         parent_dir_id      text;
                         dir_id             text;
                         update_type        bool;
                         use_type           text;
                       BEGIN
                         IF TG_OP = 'DELETE' THEN
                           use_detail_id := OLD.detail_id;
                         ELSE
                           use_detail_id := NEW.detail_id;
                         END IF;

                         update_name := false;
                         update_type := false;

                         IF TG_OP = 'DELETE' THEN
                           IF OLD.path = 'name' THEN
                             update_name := true;
                             use_name := null;
                           ELSIF OLD.path = 'type' THEN
                             update_type := true;
                             use_type := null;
                           END IF;
                         ELSEIF NEW.path = 'name' THEN
                           update_name := true;
                           use_name := NEW.value;
                         ELSEIF NEW.path = 'type' THEN
                           update_type := true;
                           use_type := NEW.value;
                         END IF;

                         IF update_name AND use_name IS NULL THEN
                           use_name := '';
                         END IF;

                         use_name_uniq := null;
                         IF update_name THEN
                           use_name_uniq := use_name;
                         END IF;

                         IF use_name_uniq IS NULL OR use_name_uniq = '' THEN
                           use_name_uniq := rnd_id();
                         END IF;

                         IF update_name THEN
                           insert into {.my}.detail_dir (id, parent_id, name, name_uniq, is_directory, created_at, modified_at)
                           values (use_detail_id, '', use_name, use_name_uniq, false, now(), now())
                           on conflict (id) do update set modified_at = now(), name = use_name;
                         ELSIF update_type THEN
                           insert into {.my}.detail_dir (id, parent_id, type, name_uniq, is_directory, created_at, modified_at)
                           values (use_detail_id, '', use_type, use_name_uniq, false, now(), now())
                           on conflict (id) do update set modified_at = now(), type = use_type;
                         ELSE
                           insert into {.my}.detail_dir (id, parent_id, name, name_uniq, is_directory, created_at, modified_at)
                           values (use_detail_id, '', '',       use_name_uniq, false, now(), now())
                           on conflict (id) do update set modified_at = now();
                         END IF;

                         skip_update_path := true;
                         IF OLD.path = 'path' OR NEW.path = 'path' THEN
                           skip_update_path := false;
                           old_path := {.my}.split_path(OLD.value);
                           new_path := {.my}.split_path(NEW.value);
                         END IF;

                         IF skip_update_path OR old_path = new_path THEN
                           return NEW;
                         END IF;

                         parent_dir_id := null;
                         dir_id := null;

                         parent_dir_id := {.my}.perform_path(new_path);

                         if parent_dir_id is null then
                           parent_dir_id := '';
                         end if;

                         update {.my}.detail_dir set parent_id = parent_dir_id where id = use_detail_id;

                         perform {.my}.clean_path(old_path);

                         return NEW;
                       END;
                       $$
                       """).update();

    query.copy().sql("""
                       CREATE TRIGGER {.my}_detail__to__detail_dir
                           AFTER INSERT OR UPDATE OR DELETE
                           ON {.my}.detail
                           FOR EACH ROW
                       EXECUTE FUNCTION {.my}.detail_dir_trigger_function()
                       """).update();

    //</editor-fold>

    return projectId;
  }

  @Override public void deleteProject(@NonNull String projectId) {
    db.query().sql("drop schema {.projectId} cascade").param("projectId", projectId).update();
  }

  @Override public ProjectRecord loadProjectRecord(@NonNull String projectId) {
    ProjectRecord ret = new ProjectRecord();
    ret.id   = projectId;
    ret.name = db.query()
                 .sql("""
                        select value
                        from {.projectId}.{.glazga_project_params}
                        where id = {project_name}
                        """)
                 .param("projectId", projectId)
                 .param("project_name", "project_name")
                 .param("glazga_project_params", "glazga_project_params")
                 .str();
    return ret;
  }

  @Override public @NonNull ProjectRecordPortion loadProjectPortion(Page page) {

    List<String> ids = new ArrayList<>();

    try (DbIterator<String> iterate = db.query()
                                        .sql("""
                                               select table_schema
                                               from information_schema.tables
                                               where table_name = {table_name}
                                               """)
                                        .param("table_name", "glazga_project_params")
                                        .iterate(rs -> rs.getString(1))) {
      while (iterate.hasNext()) {
        String schema_name = iterate.next();
        if (schema_name.startsWith(project_id_prefix)) {
          ids.add(schema_name);
        }
      }
    }

    Stream<ProjectRecord> stream = ids.stream()
                                      .map(this::loadProjectRecord)
                                      .sorted(Comparator.comparing(x -> x.name));

    if (page != null) {
      stream = stream.skip(page.offset).limit(page.limit);
    }

    var ret = new ProjectRecordPortion();
    ret.list       = stream.toList();
    ret.hasMore    = page != null && page.offset + page.limit <= ids.size();
    ret.totalCount = ids.size();
    return ret;
  }

  @Override public @NonNull ProjectService getProjectService(@NonNull String projectId) {
    return new ProjectServicePg(db, projectId);
  }
}
