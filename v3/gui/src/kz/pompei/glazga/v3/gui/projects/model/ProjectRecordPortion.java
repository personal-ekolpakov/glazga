package kz.pompei.glazga.v3.gui.projects.model;

import java.util.List;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItem;
import kz.pompei.swing.tree.logic.model.TreeNodesPortion;
import lombok.NonNull;

public class ProjectRecordPortion {
  public List<ProjectRecord> list;
  public boolean             hasMore;
  public int                 totalCount;

  public TreeNodesPortion<ProjectItem> convertToTreeView(@NonNull String workspaceId) {

    TreeNodesPortion<ProjectItem> ret = new TreeNodesPortion<>();
    ret.list       = list.stream().map(r -> r.toProjectItem(workspaceId)).toList();
    ret.hasMore    = hasMore;
    ret.totalCount = totalCount;

    return ret;
  }
}
