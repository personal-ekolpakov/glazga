package kz.pompei.glazga.v3.gui.dialogs.edit_workspace_connect;

import java.awt.Point;
import java.nio.file.Path;
import javax.swing.JComponent;
import javax.swing.JFrame;
import kz.pompei.glazga.utils.Locations;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.glazga.v3.gui.dialogs.common.DialogParent;

public class EditWorkspaceConnectDialog extends DialogParent {

  public final EditWorkspaceConnectPanel main = new EditWorkspaceConnectPanel();

  @Override protected JComponent mainPanel() {
    return main;
  }

  public EditWorkspaceConnectDialog(JFrame owner, Point mousePointOnScreen) {
    super(owner);
    initDialog(mousePointOnScreen);
  }

  public static void main(String[] args) {
    EditWorkspaceConnectDialog dialog = new EditWorkspaceConnectDialog(null, null);
    dialog.pack();

    Path rootFolder = Locations.buildConfig();
    SizeLocationSaver.into(rootFolder.resolve("sizes_and_locations"))
                     .overJFrame("EditWorkspaceConnect-form", dialog, true);

    dialog.setVisible(true);

    System.exit(0);
  }

}
