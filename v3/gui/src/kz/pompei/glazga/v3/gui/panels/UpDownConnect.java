package kz.pompei.glazga.v3.gui.panels;

import java.awt.Container;
import javax.swing.JComponent;
import lombok.NonNull;

public interface UpDownConnect {

  Container ownerContainer();

  void setDownComponent(JComponent child);

  @NonNull String getDownPanelIdPath();

  void deleteDown();

  boolean canDeleteDown();

}
