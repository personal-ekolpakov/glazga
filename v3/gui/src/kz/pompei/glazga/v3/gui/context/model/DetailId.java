package kz.pompei.glazga.v3.gui.context.model;

import java.util.Optional;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class DetailId {
  public final @NonNull String workspaceId;
  public final @NonNull String projectId;
  public final @NonNull String detailId;

  public String strValue() {
    return workspaceId + '-' + projectId + '-' + detailId;
  }

  public static @NonNull Optional<DetailId> parse(String parsedId) {
    if (parsedId == null) {
      return Optional.empty();
    }

    int idx1 = parsedId.indexOf('-');
    if (idx1 < 0) {
      return Optional.empty();
    }

    int idx2 = parsedId.indexOf('-', idx1 + 1);
    if (idx2 < 0) {
      return Optional.empty();
    }

    String workspaceId = parsedId.substring(0, idx1);
    String projectId   = parsedId.substring(idx1 + 1, idx2);
    String detailId    = parsedId.substring(idx2 + 1);

    if (workspaceId.isEmpty() || projectId.isEmpty() || detailId.isEmpty()) {
      return Optional.empty();
    }

    return Optional.of(new DetailId(workspaceId, projectId, detailId));
  }

}
