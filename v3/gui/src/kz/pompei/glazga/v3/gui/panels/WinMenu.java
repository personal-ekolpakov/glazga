package kz.pompei.glazga.v3.gui.panels;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import kz.pompei.glazga.utils.RndUtil;
import kz.pompei.glazga.v3.gui.forms.PanelForm;
import kz.pompei.glazga.v3.gui.forms.PanelFormDef;
import kz.pompei.glazga.v3.gui.icons.Icons;
import lombok.NonNull;
import lombok.SneakyThrows;

public class WinMenu {
  private final JFrame      menu;
  private final PanelHolder owner;

  public static void show(@NonNull PanelHolder owner, @NonNull Point locationOnScreen) {
    new WinMenu(locationOnScreen, owner);
  }

  private WinMenu(@NonNull Point locationOnScreen, @NonNull PanelHolder owner) {
    this.owner = owner;

    menu = new JFrame();
    menu.setLocation(locationOnScreen);
    menu.setSize(200, 300);
    menu.setUndecorated(true);


    menu.addWindowFocusListener(new WindowFocusListener() {
      @Override
      public void windowGainedFocus(WindowEvent e) {}

      @Override
      public void windowLostFocus(WindowEvent e) {
        menu.dispose();
      }
    });

    WinMenuPos winMenuPos = owner.getWinMenuPos();

    JPanel topPanel = new JPanel(new GridLayout(1, 5)); // 1 ряд, 5 колонки

    if (winMenuPos == WinMenuPos.LEFT_TOP) {
      topPanel.add(getMoveButton(MoveWinMenu.TO_DOWN, Icons.arrowDown_100x70(24, 16)));
      topPanel.add(getMoveButton(MoveWinMenu.TO_RIGHT, Icons.arrowRight_100x70(24, 16)));

      topPanel.add(getPlusMenu(PlusWin.DOWN, Icons.plusArrowDown_100x70(24, 16)));
      topPanel.add(getPlusMenu(PlusWin.RIGHT, Icons.plusArrowRight_100x70(20, 16)));
    }
    if (winMenuPos == WinMenuPos.RIGHT_TOP) {
      topPanel.add(getMoveButton(MoveWinMenu.TO_LEFT, Icons.arrowLeft_100x70(24, 16)));
      topPanel.add(getMoveButton(MoveWinMenu.TO_DOWN, Icons.arrowDown_100x70(24, 16)));

      topPanel.add(getPlusMenu(PlusWin.LEFT, Icons.plusArrowLeft_100x70(20, 16)));
      topPanel.add(getPlusMenu(PlusWin.DOWN, Icons.plusArrowDown_100x70(24, 16)));
    }
    if (winMenuPos == WinMenuPos.RIGHT_BOTTOM) {
      topPanel.add(getMoveButton(MoveWinMenu.TO_LEFT, Icons.arrowLeft_100x70(24, 16)));
      topPanel.add(getMoveButton(MoveWinMenu.TO_UP, Icons.arrowUp_100x70(24, 16)));

      topPanel.add(getPlusMenu(PlusWin.LEFT, Icons.plusArrowLeft_100x70(20, 16)));
      topPanel.add(getPlusMenu(PlusWin.UP, Icons.plusArrowUp_100x70(24, 16)));
    }
    if (winMenuPos == WinMenuPos.LEFT_BOTTOM) {
      topPanel.add(getMoveButton(MoveWinMenu.TO_UP, Icons.arrowUp_100x70(24, 16)));
      topPanel.add(getMoveButton(MoveWinMenu.TO_RIGHT, Icons.arrowRight_100x70(24, 16)));

      topPanel.add(getPlusMenu(PlusWin.UP, Icons.plusArrowUp_100x70(24, 16)));
      topPanel.add(getPlusMenu(PlusWin.RIGHT, Icons.plusArrowRight_100x70(20, 16)));
    }

    if (owner.up().canDeleteDown()) {
      JButton button = new JButton(Icons.remove_512x512(8, 8));
      topPanel.add(button);
      button.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          menu.dispose();
          owner.up().deleteDown();
        }
      });
    }

    menu.getContentPane().setLayout(new BorderLayout());
    menu.getContentPane().add(topPanel, BorderLayout.NORTH);

    JPanel forms = new JPanel();
    menu.getContentPane().add(forms, BorderLayout.CENTER);
    initForms(forms);

    menu.setVisible(true);

  }

  @SneakyThrows
  private void initForms(JPanel forms) {
    forms.setLayout(new GridBagLayout());
    int maxGridY = 0;
    for (final Class<?> formClass : PanelForm.formClasses) {
      var def    = (PanelFormDef) formClass.getMethod("definition").invoke(null);
      var button = new JButton(def.menuLabel);
      var gbc    = new GridBagConstraints();
      gbc.gridx   = def.gridX;
      gbc.gridy   = def.gridY;
      gbc.fill    = GridBagConstraints.HORIZONTAL;
      gbc.ipady   = 4;
      gbc.weightx = 1.0;
      //gbc.insets  = new Insets(5, 5, 5, 5); // Отступы вокруг кнопок
      forms.add(button, gbc);
      if (maxGridY < def.gridY) {
        maxGridY = def.gridY;
      }

      button.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          menu.dispose();
          owner.updateForm(formClass);
        }
      });
    }

    {
      JPanel bottom = new JPanel();
      var    gbc    = new GridBagConstraints();
      gbc.gridx     = 1;
      gbc.gridy     = maxGridY + 1;
      gbc.gridwidth = GridBagConstraints.REMAINDER;
      gbc.fill      = GridBagConstraints.BOTH;
      gbc.weightx   = 1.0;
      gbc.weighty   = 1.0;
      forms.add(bottom, gbc);
    }
  }

  private @NonNull JButton getPlusMenu(PlusWin plusWin, ImageIcon icon) {
    JButton ret = new JButton(icon);
    ret.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        doPlusPanel(plusWin);
      }
    });
    return ret;
  }

  private @NonNull JButton getMoveButton(MoveWinMenu moveWinMenu, ImageIcon icon) {
    JButton ret = new JButton(icon);
    ret.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        doMoveWinMenu(moveWinMenu);
      }
    });
    return ret;
  }

  private void doMoveWinMenu(MoveWinMenu moveWinMenu) {
    menu.dispose();

    WinMenuPos newPos = moveWinMenu.move(owner.getWinMenuPos());
    if (newPos == null) {
      return;
    }

    owner.setWinMenuPos(newPos);
    owner.update();
  }


  private void doPlusPanel(@NonNull PlusWin plusWin) {
    menu.dispose();

    String oldCurrentId = owner.panelId();
    String newSplitId   = RndUtil.newId();

    String firstField  = plusWin.firstNew ? "firstId" : "secondId";
    String secondField = plusWin.firstNew ? "secondId" : "firstId";

    owner.context.updating()
                 .set(owner.up().getDownPanelIdPath(), newSplitId)
                 .set(newSplitId + ".nodeType", PanelSplitter.class.getSimpleName())
                 .set(newSplitId + '.' + firstField, RndUtil.newId())
                 .set(newSplitId + '.' + secondField, oldCurrentId)
                 .set(newSplitId + ".splitType", plusWin.splitType.name())
                 .set(newSplitId + ".splitFactor", "" + plusWin.initSplitFactor)
                 .update();

    owner.context.refresh();
  }

}
