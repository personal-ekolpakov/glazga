package kz.pompei.glazga.v3.gui.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.function.Supplier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.events.VoidHandler;
import kz.pompei.glazga.v3.gui.context.ApplicationContext;
import kz.pompei.glazga.v3.gui.context.NodeContext;
import kz.pompei.glazga.v3.gui.context.rain.RainEvent;
import kz.pompei.glazga.v3.gui.forms.PanelForm;
import kz.pompei.glazga.v3.gui.forms.PanelFormDef;
import lombok.NonNull;
import lombok.SneakyThrows;

public class PanelHolder extends PanelContent {

  private final JLayeredPane owner         = new JLayeredPane();
  private final JPanel       faceComponent = new JPanel();
  private final JPanel       menuPanel     = new JPanel();
  private       PanelForm    form;

  public WinMenuPos getWinMenuPos() {
    return WinMenuPos.valueOfDef(context.storage().get(panelId() + ".win-menu-pos").orElse(null));
  }

  public void setWinMenuPos(WinMenuPos winMenuPos) {
    context.storage().set(panelId() + ".win-menu-pos", winMenuPos == null ? null : winMenuPos.name());
  }

  public PanelHolder(NodeContext nodeContext, Supplier<UpDownConnect> up) {
    super(nodeContext, up);
  }

  public @NonNull String panelId() {
    return context.storage().get(up().getDownPanelIdPath()).orElseThrow();
  }

  @Override
  public final @NonNull JComponent getComponent() {
    initialize();
    return owner;
  }

  private void componentRepaint() {
    getComponent().repaint();
  }

  public void update() {

    WinMenuPos winMenuPos = getWinMenuPos();
    winMenuPos.setBounds(menuPanel, owner);
    faceComponent.setBounds(0, 0, owner.getWidth(), owner.getHeight());

    new Thread(() -> {
      try {
        Thread.sleep(100);
      } catch (InterruptedException ignored) {
        return;
      }

      SwingUtilities.invokeLater(() -> {
        JLayeredPane owner = this.owner;
        if (owner != null) {
          owner.revalidate();
          owner.repaint();
        }
      });
    }).start();
  }

  private boolean initialized = false;

  private void initialize() {
    if (initialized) {
      return;
    }
    initialized = true;

    updateFaceComponent();

    menuPanel.setBackground(new Color(185, 0, 227));
    owner.add(faceComponent, Integer.valueOf(1));
    owner.add(menuPanel, Integer.valueOf(2));

    ComponentAdapter componentAdapter = new ComponentAdapter() {
      @Override
      public void componentResized(ComponentEvent e) {
        update();
      }
    };
    owner.addComponentListener(componentAdapter);
    SwingUtilities.invokeLater(() -> componentAdapter.componentResized(null));

    menuPanel.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        WinMenu.show(PanelHolder.this, e.getLocationOnScreen());
      }
    });
  }

  private MouseAdapter click(VoidHandler clickHandler) {
    return new MouseAdapter() {
      @Override
      public void mouseEntered(MouseEvent e) {
        componentRepaint();
      }

      @Override
      public void mouseExited(MouseEvent e) {
        componentRepaint();
      }

      @Override
      public void mouseClicked(MouseEvent e) {

        try {
          clickHandler.handle();
        } catch (Exception ex) {
          throw ex instanceof RuntimeException re ? re : new RuntimeException(ex);
        }
      }
    };
  }

  private Class<?> getStoredFormClass() {
    String form = context.storage().get(id + ".form").orElse(null);
    if (form == null) {
      return null;
    }
    Class<?> formClass = PanelForm.findClass(form);
    if (PanelForm.class.isAssignableFrom(formClass)) {
      return formClass;
    }
    return null;
  }

  private void setStoredFormClass(Class<?> formClass) {
    context.storage().set(id + ".form", formClass == null ? null : formClass.getSimpleName());
  }

  private boolean createdMenu = false;

  @SneakyThrows
  private void updateFaceComponent() {

    final PanelForm form            = this.form;
    final Class<?>  storedFormClass = getStoredFormClass();

    if (storedFormClass == null) {

      if (form != null) {
        form.destroy();
        this.form = null;
      }

      if (!createdMenu) {
        createMenu();
        createdMenu = true;
      }

      return;
    }

    if (form != null) {

      if (form.getClass() == storedFormClass) {
        return;
      }

      form.destroy();
      this.form = null;

    }

    createForm(storedFormClass);
    createdMenu = false;
  }

  @SneakyThrows
  private void createForm(@NonNull Class<?> formClass) {
    var form      = (PanelForm) formClass.getConstructor(ApplicationContext.class).newInstance(context.appContext);
    var component = form.getComponent();

    faceComponent.removeAll();
    faceComponent.setLayout(new BorderLayout());
    faceComponent.add(component, BorderLayout.CENTER);

    this.form = form;
  }

  private void createMenu() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
    faceComponent.setLayout(new GridBagLayout());

    {
      var label = new JLabel("Выберите форму", SwingConstants.CENTER);
      var gbc   = new GridBagConstraints();
      gbc.gridx     = 0;
      gbc.gridy     = 0;
      gbc.gridwidth = GridBagConstraints.REMAINDER;
      gbc.fill      = GridBagConstraints.HORIZONTAL;
      gbc.insets    = new Insets(10, 10, 10, 10); // Внешние отступы
      faceComponent.add(label, gbc);
    }
    int maxGridY = 0;
    for (final Class<?> formClass : PanelForm.formClasses) {
      var def    = (PanelFormDef) formClass.getMethod("definition").invoke(null);
      var button = new JButton(def.menuLabel);
      button.addMouseListener(click(() -> updateForm(formClass)));
      var gbc = new GridBagConstraints();
      gbc.gridx   = def.gridX;
      gbc.gridy   = def.gridY;
      gbc.fill    = GridBagConstraints.HORIZONTAL;
      gbc.ipady   = 10;
      gbc.weightx = 1.0;
      gbc.insets  = new Insets(5, 5, 5, 5); // Отступы вокруг кнопок
      faceComponent.add(button, gbc);
      if (maxGridY < def.gridY) {
        maxGridY = def.gridY;
      }
    }

    {
      JPanel bottom = new JPanel();
      var    gbc    = new GridBagConstraints();
      gbc.gridx     = 1;
      gbc.gridy     = maxGridY + 1;
      gbc.fill      = GridBagConstraints.BOTH;
      gbc.gridwidth = GridBagConstraints.REMAINDER;
      gbc.weightx   = 1.0;
      gbc.weighty   = 1.0;
      faceComponent.add(bottom, gbc);
    }

  }

  public UpDownConnect up() {
    return up.get();
  }

  @Override
  public @NonNull List<UpDownConnect> children() {
    return List.of();
  }

  @Override
  public void refreshState() {
    PanelForm form = this.form;
    if (form != null) {
      form.refreshState();
    }
  }

  public void updateForm(@NonNull Class<?> formClass) {
    setStoredFormClass(formClass);
    updateFaceComponent();
    context.refresh();
  }

  @Override
  public void acceptRainEvent(@NonNull RainEvent rainEvent) throws Exception {
    PanelForm form = this.form;
    if (form != null) {
      form.acceptRainEvent(rainEvent);
    }
  }

}
