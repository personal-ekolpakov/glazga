package kz.pompei.glazga.v3.gui.projects;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import kz.pompei.glazga.utils.NumUtil;
import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.utils.RndUtil;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ConnectParams;
import kz.pompei.glazga.v3.gui.projects.model.WorkspaceRecord;
import kz.pompei.glazga.v3.gui.projects.model.WorkspaceRecordPortion;
import kz.pompei.glazga.v3.gui.projects.pg.WorkspaceServicePg;
import kz.pompei.v3.storage.Storage;
import kz.pompei.v3.storage.update.StorageUpdate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class WorkspaceManager {
  private final @NonNull Storage storage;

  private final ConcurrentHashMap<String, WorkspaceServicePg> services = new ConcurrentHashMap<>();

  private static final Pattern WORKSPACE_ORDER = Pattern.compile("ws\\.([^.\\s]+)\\.order");

  public String addWorkspace(@NonNull ConnectParams connectParams) {

    final String workspaceId = RndUtil.newId();

    double maxOrder = 0;

    for (final String key : storage.findKeys("ws.")) {
      if (WORKSPACE_ORDER.matcher(key).matches()) {
        final double order = NumUtil.parseDoubleDef(storage.get(key).orElse("0"), 0);
        if (maxOrder < order) {
          maxOrder = order;
        }
      }
    }

    final String pre = "ws." + workspaceId;
    StorageUpdate.of(storage).set(pre + ".order", "" + (maxOrder + 1))
                 .set(pre + ".host", connectParams.host)
                 .set(pre + ".port", "" + connectParams.port)
                 .set(pre + ".username", connectParams.username)
                 .set(pre + ".password", connectParams.password)
                 .set(pre + ".dbName", connectParams.dbName)
                 .update();

    return workspaceId;
  }

  public String loadWorkspaceName(@NonNull String workspaceId) {
    return getWsService(workspaceId).workspaceName();
  }

  public void saveWorkspaceName(@NonNull String workspaceId, String endName) {
    getWsService(workspaceId).setWorkspaceName(endName);
  }

  public WorkspaceRecord loadRecord(@NonNull String workspaceId) {

    if (storage.get("ws." + workspaceId + ".order").isEmpty()) {
      return null;
    }

    WorkspaceService service = getWsService(workspaceId);

    String workspaceName = service.workspaceName();
    String description   = service.connectParamsStr();

    return new WorkspaceRecord(workspaceId, workspaceName, description);
  }

  public void updateWorkspaceCP(@NonNull String workspaceId, @NonNull ConnectParams connectParams) {

    final String pre = "ws." + workspaceId;

    StorageUpdate.of(storage).set(pre + ".host", connectParams.host)
                 .set(pre + ".port", "" + connectParams.port)
                 .set(pre + ".username", connectParams.username)
                 .set(pre + ".password", connectParams.password)
                 .set(pre + ".dbName", connectParams.dbName)
                 .update();
  }

  public ConnectParams loadConnectParams(@NonNull String workspaceId) {

    if (storage.get("ws." + workspaceId + ".order").isEmpty()) {
      return null;
    }

    String host     = storage.get("ws." + workspaceId + ".host").orElse(null);
    int    port     = NumUtil.parseIntDef(storage.get("ws." + workspaceId + ".port").orElse(null), 0);
    String username = storage.get("ws." + workspaceId + ".username").orElse(null);
    String password = storage.get("ws." + workspaceId + ".password").orElse(null);
    String dbName   = storage.get("ws." + workspaceId + ".dbName").orElse(null);

    return ConnectParams.builder()
                        .host(host)
                        .port(port)
                        .username(username)
                        .password(password)
                        .dbName(dbName)
                        .build();
  }


  public WorkspaceRecordPortion getWorkspaceRecordList(Page page) {

    Map<String, Double> idAndOrder = new HashMap<>();

    for (final String key : storage.findKeys("ws.")) {
      final Matcher matcher = WORKSPACE_ORDER.matcher(key);
      if (matcher.matches()) {
        final double order = NumUtil.parseDoubleDef(storage.get(key).orElse("0"), 0);
        idAndOrder.put(matcher.group(1), order);
      }
    }

    List<String>   idsAll  = idAndOrder.entrySet().stream().sorted(Map.Entry.comparingByValue()).map(Map.Entry::getKey).toList();
    Stream<String> portion = page == null ? idsAll.stream() : idsAll.stream().skip(page.offset).limit(page.limit);

    WorkspaceRecordPortion ret = new WorkspaceRecordPortion();
    ret.list       = portion.map(this::loadRecord).toList();
    ret.hasMore    = page != null && idsAll.size() > page.offset + page.limit;
    ret.totalCount = idsAll.size();

    for (final WorkspaceRecord workspaceRecord : ret.list) {
      System.out.println("o1CI7OVE1T :: Loaded workspaceId = " + workspaceRecord.id);
    }

    return ret;

  }

  public void close() {
    final HashMap<String, WorkspaceServicePg> map = new HashMap<>(services);
    services.clear();
    map.values().forEach(WorkspaceServicePg::close);
  }

  public @NonNull WorkspaceService getWsService(@NonNull String workspaceId) {
    WorkspaceServicePg service = services.get(workspaceId);
    if (service != null) {
      return service;
    }

    ConnectParams connectParams = loadConnectParams(workspaceId);
    if (connectParams == null) {
      throw new RuntimeException("KM1O8ohsZG :: No connect params for workspaceId = " + workspaceId);
    }

    WorkspaceServicePg spg = new WorkspaceServicePg(connectParams);
    spg.initialize();

    services.put(workspaceId, spg);

    return spg;
  }

}
