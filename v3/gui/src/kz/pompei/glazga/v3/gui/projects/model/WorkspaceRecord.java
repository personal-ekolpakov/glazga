package kz.pompei.glazga.v3.gui.projects.model;

import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItem;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemCell;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemId;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@RequiredArgsConstructor
public class WorkspaceRecord {
  public final String id;
  public final String name;
  public final String description;

  public ProjectItem toProjectItem() {

    final ProjectItemId piId = ProjectItemId.workspace(id);

    final ProjectItem ret = new ProjectItem(piId.strId(), true);
    ret.cells.add(new ProjectItemCell("name", name, "name"));
    ret.cells.add(new ProjectItemCell("description", description, "description"));

    ret.nodeType = "workspace";

    return ret;
  }
}
