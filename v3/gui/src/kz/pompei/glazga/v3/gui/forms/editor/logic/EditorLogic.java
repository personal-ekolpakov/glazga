package kz.pompei.glazga.v3.gui.forms.editor.logic;

import javax.swing.JComponent;

public abstract class EditorLogic {

  public abstract void refresh();

  public abstract JComponent getComponent();

  public void initialize() {}

}
