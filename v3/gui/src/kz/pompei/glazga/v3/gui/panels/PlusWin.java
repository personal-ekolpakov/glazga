package kz.pompei.glazga.v3.gui.panels;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum PlusWin {

  RIGHT(SplitType.Horizontal, 0.1, true),

  DOWN(SplitType.Vertical, 0.1, true),

  LEFT(SplitType.Horizontal, 0.9, false),

  UP(SplitType.Vertical, 0.9, false),

  ;

  public final SplitType splitType;
  public final double    initSplitFactor;
  public final boolean    firstNew;

}
