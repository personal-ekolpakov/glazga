package kz.pompei.glazga.v3.gui.dialogs.new_project;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import kz.pompei.glazga.utils.JDialogUtil;

public class NewProjectPanel extends JPanel {

  public final JLabel     labelName = new JLabel("Имя проекта:");
  public final JTextField fieldName = new JTextField();

  public final JPanel dialogButtons = new JPanel();

  public final JButton buttonSave   = new JButton("Сохранить");
  public final JButton buttonCancel = new JButton("Отменить");


  public NewProjectPanel() {
    setLayout(new GridBagLayout());
    setBorder(new EmptyBorder(10, 10, 10, 10));

    JDialogUtil.borders(fieldName, 5, 5, 5, 5);

    {
      var c = new GridBagConstraints();

      c.weightx = 1;
      c.weighty = 1;
      c.gridy   = 0;

      c.gridx  = 0;
      c.fill   = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.LINE_END;
      add(labelName, c);

      c.gridx  = 1;
      c.fill   = GridBagConstraints.HORIZONTAL;
      c.anchor = GridBagConstraints.CENTER;
      add(fieldName, c);

      c.gridy++;

      c.gridx     = 1;
      c.gridwidth = 2;
      c.fill      = GridBagConstraints.NONE;
      add(dialogButtons, c);
    }

    dialogButtons.setLayout(new GridBagLayout());
    dialogButtons.setBorder(new EmptyBorder(10, 0, 0, 0));

    {
      var c = new GridBagConstraints();
      c.gridy = 0;

      c.gridx = 0;
      dialogButtons.add(buttonSave, c);

      c.gridx  = 1;
      c.insets = new Insets(0, 10, 0, 0);
      dialogButtons.add(buttonCancel, c);
    }

  }
}
