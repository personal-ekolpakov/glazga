package kz.pompei.glazga.v3.gui.forms;

import java.awt.BorderLayout;
import java.util.Objects;
import javax.swing.JComponent;
import javax.swing.JPanel;
import kz.pompei.glazga.v3.gui.context.ApplicationContext;
import kz.pompei.glazga.v3.gui.context.model.DetailId;
import kz.pompei.glazga.v3.gui.context.rain.RainChangeDetailId;
import kz.pompei.glazga.v3.gui.context.rain.RainEvent;
import kz.pompei.glazga.v3.gui.forms.editor.logic.EditorLogic;
import kz.pompei.glazga.v3.gui.forms.editor.logic.EditorLogicDetail;
import kz.pompei.glazga.v3.gui.forms.editor.logic.EditorLogicEmpty;
import kz.pompei.glazga.v3.gui.forms.editor.logic.EditorLogicError;
import kz.pompei.glazga.v3.gui.forms.editor.service.EditorService;
import kz.pompei.glazga.v3.gui.forms.editor.service.EditorServiceSync;
import kz.pompei.glazga.v3.gui.forms.editor.service.EditorServiceSyncImpl;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FormEditor extends PanelForm {
  private final @NonNull ApplicationContext context;

  private final JPanel panel = new JPanel();


  private EditorLogic logic;
  private DetailId    detailId;

  private EditorLogic logic() {
    DetailId detailId = context.getDetailId();

    if (logic != null && Objects.equals(detailId, this.detailId)) {
      return logic;
    }

    this.detailId = detailId;

    if (detailId == null) {
      return revalidate(logic = new EditorLogicEmpty());
    }

    try {
      final EditorServiceSync serviceSync = new EditorServiceSyncImpl(context.workspaceManager, detailId);
      final EditorService     service     = context.syncAsyncBridge.createAsync(EditorService.class, serviceSync);
      return revalidate(logic = new EditorLogicDetail(service, detailId, context.application.cd(detailId.strValue())));
    } catch (Throwable error) {
      //noinspection CallToPrintStackTrace
      error.printStackTrace();
      return revalidate(logic = new EditorLogicError(error));
    }
  }

  private @NonNull EditorLogic revalidate(@NonNull EditorLogic logic) {
    JComponent component = logic.getComponent();
    panel.removeAll();
    panel.setLayout(new BorderLayout());
    panel.add(component, BorderLayout.CENTER);
    panel.revalidate();
    panel.repaint();
    logic.initialize();
    return logic;
  }

  @SuppressWarnings("unused")
  public static PanelFormDef definition() {
    var ret = new PanelFormDef();
    ret.gridX     = 1;
    ret.gridY     = 2;
    ret.menuLabel = "Редактор";
    return ret;
  }

  @Override
  public JComponent getComponent() {
    return panel;
  }

  @Override public void acceptRainEvent(@NonNull RainEvent rainEvent) {
    if (RainChangeDetailId.ID == rainEvent) {
      logic().refresh();
      System.out.println("XZbhUFvtDj :: RainChangeDetailId detailId = " + context.getDetailId());
    }
  }

  @Override public void refreshState() {
    logic().refresh();
  }
}
