package kz.pompei.glazga.v3.gui.context;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.RndUtil;
import kz.pompei.glazga.v3.gui.context.rain.RainEvent;
import kz.pompei.glazga.v3.gui.panels.PanelNode;
import kz.pompei.glazga.v3.gui.panels.UpDownConnect;
import kz.pompei.v3.storage.Storage;
import kz.pompei.v3.storage.update.StorageUpdate;
import lombok.NonNull;
import lombok.SneakyThrows;

import static java.util.Objects.requireNonNull;

public class NodeContext {
  private final Storage            storage;
  public final  ApplicationContext appContext;
  private       UpDownConnect      root;

  public NodeContext(Storage storage, ApplicationContext appContext) {
    this.storage    = storage;
    this.appContext = appContext;

    appContext.rainEventObserver.add(this::handleRainEvent);
  }

  public Storage storage() {
    return storage;
  }

  private final Map<String, PanelNode> nodeMap = new HashMap<>();

  public StorageUpdate updating() {
    return StorageUpdate.over(storage());
  }

  public void setUpDownRoot(UpDownConnect root) {
    this.root = root;
  }

  public UpDownConnect getUpDownRoot() {
    return root;
  }

  public PanelNode getNodeByPanelId(String panelId) {
    return nodeMap.get(panelId);
  }

  public final AtomicBoolean saveState = new AtomicBoolean(true);

  private void handleRainEvent(RainEvent rainEvent) throws Exception {
    if (rainEvent == null) {
      return;
    }
    for (final PanelNode panelNode : nodeMap.values()) {
      panelNode.acceptRainEvent(rainEvent);
    }
  }

  @SneakyThrows
  public void refresh() {
    List<PanelNode> usedPanelNodes = new ArrayList<>();

    saveState.set(false);
    try {
      UpDownConnect root = this.root;
      if (root == null) {
        throw new RuntimeException("tvgQHqLUu0 :: root == null");
      }

      List<UpDownConnect> refList = new ArrayList<>();
      refList.add(root);

      usedPanelNodes.addAll(refresh0(refList, new HashSet<>()));

      nodeMap.values().forEach(x -> x.using = false);
      usedPanelNodes.forEach(x -> x.using = true);

      List<PanelNode> panelsToDelete = nodeMap.values().stream().filter(x -> !x.using).toList();

      for (final PanelNode panelNode : panelsToDelete) {
        nodeMap.remove(panelNode.getPanelId());
        panelNode.destroy();
      }

      root.ownerContainer().revalidate();
      root.ownerContainer().repaint();

    } catch (RuntimeException e) {
      saveState.set(true);
      throw e;
    }

    new Thread(() -> {
      try {

        long sleep = 100;

        for (final PanelNode usedPanelNode : usedPanelNodes) {
          try {
            Thread.sleep(sleep);
          } catch (InterruptedException e) {
            return;
          }

          sleep = 70;

          try {
            SwingUtilities.invokeAndWait(usedPanelNode::refreshState);
          } catch (InterruptedException e) {
            return;
          } catch (InvocationTargetException e) {
            throw new RuntimeException("BkaC6B599V", e);
          }
        }

        try {
          Thread.sleep(80);
        } catch (InterruptedException e) {
          return;
        }
      } finally {
        saveState.set(true);
      }

    }).start();

  }

  public @NonNull PanelNode getOrCreate(@NonNull UpDownConnect up) {

    final String    panelId   = storage.get(up.getDownPanelIdPath()).orElseThrow();
    final PanelNode panelNode = nodeMap.get(panelId);

    if (panelNode != null) {

      final String nodeType  = panelNode.getNodeType();
      final String savedType = storage.get(panelId + ".nodeType").orElse(null);

      requireNonNull(nodeType, "SWUiuLFylU :: nodeType == null");

      if (savedType == null) {
        storage.set(panelId + ".nodeType", nodeType);
      } else if (savedType.equals(nodeType)) {
        return panelNode.updateUp(up);
      }

      panelNode.destroy();
    }

    {
      PanelNode newPanelNode = new PanelNode(this, up);
      nodeMap.put(panelId, newPanelNode);
      return newPanelNode;
    }
  }

  private List<PanelNode> refresh0(List<UpDownConnect> refList, Set<String> usedPanelIds) {

    List<PanelNode> usedPanelNodes = new ArrayList<>();

    while (refList.size() > 0) {

      UpDownConnect ref = refList.remove(0);

      String childPanelId = storage.get(ref.getDownPanelIdPath()).orElse(null);
      if (childPanelId == null) {
        childPanelId = RndUtil.newId();
        storage.set(ref.getDownPanelIdPath(), childPanelId);
      }

      if (usedPanelIds.contains(childPanelId)) {
        System.out.println("H3wWVocIUT :: Happened used panel id = `" + childPanelId + "` : all used panel ids = " + usedPanelIds);
        return usedPanelNodes;
      }
      usedPanelIds.add(childPanelId);

      @NonNull PanelNode panelNode = getOrCreate(ref);

      ref.setDownComponent(panelNode.getComponent());
      usedPanelNodes.add(panelNode);
      refList.addAll(panelNode.getChildrenRefs());
    }

    return usedPanelNodes;
  }
}
