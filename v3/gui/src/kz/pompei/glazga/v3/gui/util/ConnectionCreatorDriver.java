package kz.pompei.glazga.v3.gui.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Arrays;
import java.util.function.Supplier;
import kz.pompei.glazga.utils.db.ConnectionCreator;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ConnectParams;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import static java.util.stream.Collectors.joining;

@RequiredArgsConstructor
public class ConnectionCreatorDriver implements ConnectionCreator {
  private final @NonNull Supplier<ConnectParams> connectParamsSupplier;

  private String appendPort(String host) {
    return host.contains(":") ? host : host + ":" + connectParamsSupplier.get().port;
  }

  @Override @SneakyThrows public Connection create() {
    Class.forName("org.postgresql.Driver");

    final ConnectParams cp        = connectParamsSupplier.get();
    final String        hostsPort = Arrays.stream(cp.host.split(",")).map(this::appendPort).collect(joining(","));
    final String        url       = "jdbc:postgresql://" + hostsPort + "/" + cp.dbName;

    return DriverManager.getConnection(url, cp.username, cp.password);
  }
}
