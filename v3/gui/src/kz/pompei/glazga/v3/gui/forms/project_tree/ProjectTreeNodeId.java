package kz.pompei.glazga.v3.gui.forms.project_tree;

import java.util.Optional;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ProjectTreeNodeId {
  public final ProjectTreeNodeType type;
  public final String              id;

  public static @NonNull ProjectTreeNodeId workspace(@NonNull String workspaceId) {
    return new ProjectTreeNodeId(ProjectTreeNodeType.WORKSPACE, workspaceId);
  }

  public static @NonNull ProjectTreeNodeId project(@NonNull String projectRootId) {
    return new ProjectTreeNodeId(ProjectTreeNodeType.PROJECT, projectRootId);
  }

  public static Optional<ProjectTreeNodeId> parse(String strId) {
    if (strId == null) {
      return Optional.empty();
    }

    final int idx = strId.indexOf('-');
    if (idx < 0) {
      return Optional.empty();
    }

    String typeStr = strId.substring(0, idx);
    String idStr   = strId.substring(idx + 1);
    return Optional.of(new ProjectTreeNodeId(ProjectTreeNodeType.valueOf(typeStr), idStr));
  }

  public @NonNull String strId() {
    return switch (type) {
      case WORKSPACE, PROJECT -> type.name() + '-' + id;
    };
  }

}
