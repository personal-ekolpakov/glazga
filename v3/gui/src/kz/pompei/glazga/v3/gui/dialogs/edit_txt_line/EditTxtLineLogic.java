package kz.pompei.glazga.v3.gui.dialogs.edit_txt_line;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import kz.pompei.glazga.v3.gui.dialogs.common.DialogLogic;
import lombok.NonNull;

public class EditTxtLineLogic extends DialogLogic {

  private final EditTxtLineDialog dialog;

  public EditTxtLineLogic(@NonNull EditTxtLineDialog dialog) {
    this.dialog = dialog;

    initDialogBasically();
  }

  public String edit(String inputText) {

    dialog.main.field.setText(inputText != null ? inputText : "");

    dialog.pack();

    dialog.setVisible(true);

    if (cancel) {
      return null;
    }

    return dialog.main.field.getText();
  }

  @Override protected JButton buttonCancel() {
    return dialog.main.buttonCancel;
  }

  @Override protected JButton buttonSave() {
    return dialog.main.buttonSave;
  }

  @Override protected JDialog dialog() {
    return dialog;
  }

  @Override protected JComponent mainPanel() {
    return dialog.main;
  }

  public void setTitle(String title) {
    dialog.setTitle(title);
  }

  public void setLabel(String label) {
    dialog.main.label.setText(label);
  }
}
