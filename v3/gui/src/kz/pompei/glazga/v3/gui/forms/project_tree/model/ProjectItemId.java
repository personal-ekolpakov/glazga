package kz.pompei.glazga.v3.gui.forms.project_tree.model;

import java.util.Optional;
import lombok.Builder;
import lombok.NonNull;
import lombok.ToString;

import static java.util.Objects.requireNonNull;

@Builder
@ToString
public class ProjectItemId {
  public final @NonNull ProjectItemType type;
  public final          String          workspaceId;
  public final          String          projectId;
  public final          String          id;

  private static final String WORKSPACE = "WS";
  private static final String PROJECT   = "PJ";
  private static final String DETAIL    = "DL";
  private static final String FOLDER    = "FO";

  public String strId() {
    return switch (type) {
      case WORKSPACE -> WORKSPACE + '-' + workspaceId;
      case PROJECT -> PROJECT + '-' + workspaceId + '-' + projectId;
      case DETAIL -> DETAIL + '-' + workspaceId + '-' + projectId + '-' + id;
      case FOLDER -> FOLDER + '-' + workspaceId + '-' + projectId + '-' + id;
    };
  }

  public static Optional<ProjectItemId> parse(String strId) {
    if (strId == null) {
      return Optional.empty();
    }
    final int idx1 = strId.indexOf("-");
    if (idx1 < 0) {
      return Optional.empty();
    }

    final String typeStr = strId.substring(0, idx1);

    switch (typeStr) {
      case WORKSPACE -> {
        String workspaceId = strId.substring(idx1 + 1);
        return Optional.of(ProjectItemId.builder()
                                        .type(ProjectItemType.WORKSPACE)
                                        .workspaceId(workspaceId)
                                        .build());
      }

      case PROJECT -> {
        int idx2 = strId.indexOf('-', idx1 + 1);
        if (idx2 < 0) {
          return Optional.empty();
        }

        String workspaceId = strId.substring(idx1 + 1, idx2);
        String projectId   = strId.substring(idx2 + 1);

        return Optional.of(ProjectItemId.builder()
                                        .type(ProjectItemType.PROJECT)
                                        .workspaceId(workspaceId)
                                        .projectId(projectId)
                                        .build());
      }

      case DETAIL -> {

        int idx2 = strId.indexOf('-', idx1 + 1);
        if (idx2 < 0) {
          return Optional.empty();
        }

        int idx3 = strId.indexOf('-', idx2 + 1);
        if (idx3 < 0) {
          return Optional.empty();
        }

        String workspaceId = strId.substring(idx1 + 1, idx2);
        String projectId   = strId.substring(idx2 + 1, idx3);
        String detailId    = strId.substring(idx3 + 1);

        return Optional.of(ProjectItemId.builder()
                                        .type(ProjectItemType.DETAIL)
                                        .workspaceId(workspaceId)
                                        .projectId(projectId)
                                        .id(detailId)
                                        .build());
      }

      default -> throw new RuntimeException("c3R0UEryGU :: Unknown " + ProjectItemType.class.getSimpleName()
                                            + " value for string = `" + typeStr + "`" + strId);
    }
  }

  public static ProjectItemId workspace(@NonNull String workspaceId) {
    return builder().type(ProjectItemType.WORKSPACE)
                    .workspaceId(workspaceId)
                    .build();
  }

  public static ProjectItemId project(@NonNull String workspaceId, @NonNull String projectId) {
    return builder().type(ProjectItemType.PROJECT)
                    .workspaceId(requireNonNull(workspaceId))
                    .projectId(requireNonNull(projectId))
                    .build();
  }

  public static ProjectItemId detail(@NonNull String workspaceId, @NonNull String projectId, @NonNull String detailId) {
    return builder().type(ProjectItemType.DETAIL)
                    .workspaceId(requireNonNull(workspaceId))
                    .projectId(requireNonNull(projectId))
                    .id(requireNonNull(detailId))
                    .build();
  }

  public static ProjectItemId folder(@NonNull String workspaceId, @NonNull String projectId, @NonNull String folderId) {
    return builder().type(ProjectItemType.FOLDER)
                    .workspaceId(requireNonNull(workspaceId))
                    .projectId(requireNonNull(projectId))
                    .id(requireNonNull(folderId))
                    .build();
  }

}
