package kz.pompei.glazga.v3.gui.forms.project_tree.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Builder
@ToString
@EqualsAndHashCode
public class ConnectParams {
  public final String host;
  public final int    port;
  public final String username;
  public final String password;
  public final String dbName;

  public String url() {
    return "jdbc:postgresql://" + host + ":" + port + "/" + dbName;
  }
}
