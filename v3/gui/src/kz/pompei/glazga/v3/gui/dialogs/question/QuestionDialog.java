package kz.pompei.glazga.v3.gui.dialogs.question;

import java.awt.Point;
import java.nio.file.Path;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import kz.pompei.glazga.utils.Locations;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.glazga.v3.gui.dialogs.common.DialogParent;
import kz.pompei.swing.icons.Svg;

public class QuestionDialog extends DialogParent {

  public final QuestionPanel main = new QuestionPanel();

  public QuestionDialog(JFrame owner, Point mousePointOnScreen) {
    super(owner);
    initDialog(mousePointOnScreen);
  }

  @Override protected JComponent mainPanel() {
    return main;
  }


  public static void main(String[] args) {
    QuestionDialog dialog = new QuestionDialog(null, null);

    dialog.main.init(List.of(

      QuestionVariant.of("010", "Вариант выбора 010"),
      QuestionVariant.of("234", "Вариант выбора 234", new ImageIcon(Svg.images.get("svg/dependencyAnalyzer.svg", 16, 16))),
      QuestionVariant.of("783", "Вариант выбора 783", new ImageIcon(Svg.images.get("svg/deploy.svg", 16, 16)))

    ));

    dialog.setTitle("Заголовок окна");
    dialog.main.labelQuestion.setText("Это основной вопрос?");
    dialog.pack();

    Path rootFolder = Locations.buildConfig();
    SizeLocationSaver.into(rootFolder.resolve("sizes_and_locations"))
                     .overJFrame("QuestionDialog-form", dialog, false);

    dialog.main.buttonClicked.add(() -> System.out.println("L3ZH6UuKCx :: Выбран вариант " + dialog.main.result));

    dialog.pack();

    dialog.setVisible(true);

    System.exit(0);
  }
}
