package kz.pompei.glazga.v3.gui.forms.project_tree;

public enum ProjectTreeNodeType {
  WORKSPACE,
  PROJECT,
}
