package kz.pompei.glazga.v3.gui.forms;

import javax.swing.JComponent;
import javax.swing.JLabel;
import kz.pompei.glazga.v3.gui.context.ApplicationContext;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FormEditorTabs extends PanelForm {
  private final @NonNull ApplicationContext context;

  @SuppressWarnings("unused")
  public static PanelFormDef definition() {
    var ret = new PanelFormDef();
    ret.gridX     = 1;
    ret.gridY     = 3;
    ret.menuLabel = "Редактируемые детали";
    return ret;
  }

  @Override
  public JComponent getComponent() {
    JLabel label = new JLabel();
    label.setText("Редактируемые детали");
    return label;
  }
}
