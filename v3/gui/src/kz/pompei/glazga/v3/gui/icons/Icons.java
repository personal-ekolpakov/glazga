package kz.pompei.glazga.v3.gui.icons;

import java.awt.Image;
import java.io.InputStream;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.ImageIcon;
import lombok.SneakyThrows;

import static java.util.Objects.requireNonNull;

public class Icons {

  private Icons() {}

  private static final Icons ME = new Icons();

  @SneakyThrows
  private ImageIcon loadIcon(String resourceName) {
    try (InputStream resourceAsStream = getClass().getResourceAsStream(resourceName)) {
      requireNonNull(resourceAsStream, "h412WKJIc4 :: No icon in resource `" + resourceName + "` from " + getClass());
      return new ImageIcon(resourceAsStream.readAllBytes());
    }
  }

  private final ConcurrentHashMap<String, ImageIcon> loadedIcons = new ConcurrentHashMap<>();

  private ImageIcon getIcon(String resourceName) {
    {
      ImageIcon ret = loadedIcons.get(resourceName);
      if (ret != null) {
        return ret;
      }
    }
    synchronized (this) {
      {
        ImageIcon ret = loadedIcons.get(resourceName);
        if (ret != null) {
          return ret;
        }
      }
      {
        ImageIcon ret = loadIcon(resourceName);
        loadedIcons.put(resourceName, ret);
        return ret;
      }
    }
  }

  private static ImageIcon getScaledIcon(String resourceName, int width, int height) {
    return new ImageIcon(ME.getIcon(resourceName).getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
  }

  public static ImageIcon arrowLeft_100x70(int width, int height) {
    return getScaledIcon("arrow-left-100x70.png", width, height);
  }

  public static ImageIcon arrowRight_100x70(int width, int height) {
    return getScaledIcon("arrow-right-100x70.png", width, height);
  }

  public static ImageIcon arrowUp_100x70(int width, int height) {
    return getScaledIcon("arrow-up-100x70.png", width, height);
  }

  public static ImageIcon arrowDown_100x70(int width, int height) {
    return getScaledIcon("arrow-down-100x70.png", width, height);
  }

  public static ImageIcon remove_512x512(int width, int height) {
    return getScaledIcon("remove-512x512.png", width, height);
  }

  public static ImageIcon plusArrowDown_100x70(int width, int height) {
    return getScaledIcon("plus-arrow-down-100x70.png", width, height);
  }

  public static ImageIcon plusArrowUp_100x70(int width, int height) {
    return getScaledIcon("plus-arrow-up-100x70.png", width, height);
  }

  public static ImageIcon plusArrowLeft_100x70(int width, int height) {
    return getScaledIcon("plus-arrow-left-100x70.png", width, height);
  }

  public static ImageIcon plusArrowRight_100x70(int width, int height) {
    return getScaledIcon("plus-arrow-right-100x70.png", width, height);
  }

}
