package kz.pompei.glazga.v3.gui.styles;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Image;
import kz.pompei.swing.fonts.FontDef;
import kz.pompei.swing.icons.Svg;
import kz.pompei.swing.tree.styles.NodeStatus;
import kz.pompei.swing.tree.styles.RectStyle;
import kz.pompei.swing.tree.styles.State;
import kz.pompei.swing.tree.styles.SwingTreeNodeStyle;
import kz.pompei.swing.tree.styles.TextStyle;
import kz.pompei.swing.tree.styles.defau.FNT;
import kz.pompei.swing.tree.styles.defau.MarginFix;
import kz.pompei.swing.tree.styles.defau.RectStyleFix;
import kz.pompei.swing.tree.styles.defau.TextStyleFix;
import lombok.NonNull;

import static kz.pompei.swing.icons.Svg.TRANSPARENT;

@SuppressWarnings("DuplicatedCode")
public class SwingTreeNodeStyle_GUI implements SwingTreeNodeStyle {

  @Override
  public int nodeLevelStep() {
    return 25;
  }

  @Override
  public int paddingTop() {
    return 0;
  }

  @Override
  public int paddingBottom() {
    return 5;
  }

  @Override
  public @NonNull Image chevronIcon(@NonNull NodeStatus nodeStatus, int size) {
    return Svg.images.get(chevronIconResourceName(nodeStatus), size, size);
  }

  private static String chevronIconResourceName(@NonNull NodeStatus nodeStatus) {
    return switch (nodeStatus) {
      case OPEN -> "svg/treeExpanded.svg";
      case CLOSE -> "svg/treeCollapsed.svg";
      case LEAF -> TRANSPARENT;
    };
  }

  @Override public float chevronIconSizeFactor() {
    return 0.8f;
  }


  @Override public int chevronIconRight() {
    return 3;
  }

  @Override
  public @NonNull Image nodeIcon(String nodeType, int size) {
    return Svg.images.get(nodeIconResourceName(nodeType), size, size);
  }

  private @NonNull String nodeIconResourceName(String nodeType) {
    //@formatter:off
    if ("more"     .equals(nodeType)) return TRANSPARENT;
    if ("waiting"  .equals(nodeType)) return TRANSPARENT;
    if ("wait"     .equals(nodeType)) return TRANSPARENT;
    if ("folder"   .equals(nodeType)) return "svg/folder.svg";
    if ("txt"      .equals(nodeType)) return "svg/text.svg";
    if ("jpeg"     .equals(nodeType)) return "svg/image.svg";
    if ("jpg"      .equals(nodeType)) return "svg/image.svg";
    if ("ant"      .equals(nodeType)) return "svg/ant.svg";
    if ("cassandra".equals(nodeType)) return "svg/cassandra.svg";
    if ("java"     .equals(nodeType)) return "svg/java.svg";
    if ("workspace".equals(nodeType)) return "svg/jdk.svg";
    if ("project"  .equals(nodeType)) return "svg/project.svg";

    if ("detail-program".equals(nodeType)) return "svg/java.svg";
    if ("detail-unknown".equals(nodeType)) return "svg/helpInactive.svg";
    //@formatter:on

    System.out.println("xGWBvaRUS2 :: Unknown nodeType = " + nodeType);

    return "svg/helpInactive.svg";
  }

  @Override
  public float nodeIconSizeFactor() {
    return 1f;
  }

  @Override
  public int nodeIconRight() {
    return 3;
  }

  private final float BASE_FONT_SIZE = 16;

  private final TextStyleFix def = TextStyleFix.of(
    FNT.FONT_SRC.get(FontDef.of("Playfair Display", 400, BASE_FONT_SIZE, false)),
    new Color(16, 1, 162), MarginFix.of(0, 0, 0, 0));

  private final TextStyleFix date = TextStyleFix.of(
    FNT.FONT_SRC.get(FontDef.of("Playfair Display", 400, BASE_FONT_SIZE * 0.75f, true)),
    new Color(16, 1, 162), MarginFix.of(Math.round(BASE_FONT_SIZE * 0.25f), 0, 0, 0));

  private final TextStyleFix description = TextStyleFix.of(
    FNT.FONT_SRC.get(FontDef.of("Playfair Display", 400, BASE_FONT_SIZE * 0.8f, true)),
    new Color(140, 140, 140), MarginFix.of(Math.round(BASE_FONT_SIZE * 0.25f), 0, 0, 0));

  private final TextStyleFix grayed = TextStyleFix.of(
    FNT.FONT_SRC.get(FontDef.of("Playfair Display", 400, BASE_FONT_SIZE * 0.75f, true)),
    new Color(151, 142, 234), MarginFix.of(Math.round(BASE_FONT_SIZE * 0.25f), 0, 0, 0));


  @Override
  public @NonNull TextStyle cellStyle(String cellType) {
    return cellType == null ? def : switch (cellType) {
      default -> def;
      case "date" -> date;
      case "grayed" -> grayed;
      case "description" -> description;
    };
  }

  @Override
  public RectStyle nodeBackgroundRect(String nodeType, @NonNull State state) {
    if (state == State.ACTIVE) {
      RectStyleFix style = new RectStyleFix();
      style.backgroundColor = new Color(34, 253, 76);

      style.borderColor        = new Color(23, 31, 168);
      style.borderInlineOffset = 0;
      style.borderRadiusX      = 5f;
      style.borderStroke       = new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, 1f, new float[]{0.5f, 4f}, 0f);

      return style;
    }

    if (state == State.SELECTED) {
      RectStyleFix style = new RectStyleFix();
      style.backgroundColor = new Color(34, 253, 76);
      return style;
    }

    return null;
  }

  @Override public RectStyle cellBackgroundRect(String cellType, @NonNull State state) {
    if (state == State.ACTIVE) {
      RectStyleFix style = new RectStyleFix();
      style.backgroundColor = new Color(0, 0, 0, 31);

      style.borderColor        = new Color(23, 31, 168);
      style.borderRadiusX      = 13f;
      style.borderInlineOffset = 3;
      style.borderStroke       = new BasicStroke(1f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER, 1f, new float[]{4f, 3f}, 0f);

      return style;
    }

    if (state == State.SELECTED) {
      RectStyleFix style = new RectStyleFix();
      style.backgroundColor = new Color(0, 0, 0, 31);
      return style;
    }

    return null;
  }
}
