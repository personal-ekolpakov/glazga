package kz.pompei.glazga.v3.gui.launcher;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Path;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.Locations;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.glazga.v3.gui.context.ApplicationContext;
import kz.pompei.glazga.v3.gui.context.NodeContext;
import kz.pompei.glazga.v3.gui.panels.UpDownConnect;
import kz.pompei.glazga.v3.gui.util.Sizes;
import kz.pompei.swing.icons.Svg;
import kz.pompei.v3.storage.Storage;
import kz.pompei.v3.storage.StoragePath;
import lombok.NonNull;

public class GlazgaLauncher {
  public static void main(String[] args) {
    JFrame frame = new JFrame();

//    Storage config = new StoragePath(Locations.userConfigDir());
    Path               rootFolder  = Locations.buildConfig();
    Storage            config      = new StoragePath(rootFolder);
    Storage            mainWindow  = config.cd("main-window");
    Storage            application = config.cd("application");
    ApplicationContext appContext  = new ApplicationContext(frame, application);
    NodeContext        nodeContext = new NodeContext(mainWindow, appContext);

    frame.setTitle("ГлазГа.в3 - Главная форма");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setIconImage(Svg.images.get("svg/cassandra.svg", 128, 128, new Color(255, 255, 255, 226)));
    Sizes.setSizeAndLocation(frame, 0.6f);
    SizeLocationSaver.into(rootFolder.resolve("sizes_and_locations"))
                     .overJFrame("main-form", frame, true);

    UpDownConnect root = new UpDownConnect() {

      @Override
      public Container ownerContainer() {
        return frame;
      }

      @Override
      public void setDownComponent(JComponent child) {
        Container contentPane = frame.getContentPane();
        int       length      = contentPane.getComponents().length;

        if (length == 1) {
          if (contentPane.getComponents()[0] == child) {
            return;
          }
          contentPane.removeAll();
          if (child != null) {
            contentPane.add(child);
          }
          return;
        }

        if (length > 0) {
          contentPane.removeAll();
        }
        if (child != null) {
          contentPane.add(child);
        }
      }

      @Override
      public void deleteDown() {
        throw new RuntimeException("nG1xgP6jAE :: You cannot delete most top element");
      }

      @Override
      public boolean canDeleteDown() {
        return false;
      }

      @Override
      public @NonNull String getDownPanelIdPath() {
        return "root-panel-id";
      }
    };

    nodeContext.setUpDownRoot(root);
    nodeContext.refresh();

    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        frame.dispose();
      }

      @Override
      public void windowClosed(WindowEvent e) {
        System.out.println("l8HCJHcLMU :: windowClosed");
      }
    });

    SwingUtilities.invokeLater(() -> frame.setVisible(true));


  }
}
