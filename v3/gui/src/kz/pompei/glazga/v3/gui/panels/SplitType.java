package kz.pompei.glazga.v3.gui.panels;

import java.awt.Container;
import javax.swing.JSplitPane;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum SplitType {

  Horizontal(JSplitPane.HORIZONTAL_SPLIT) {
    @Override
    public int getSize(@NonNull Container parent) {
      return parent.getWidth();
    }
  },

  Vertical(JSplitPane.VERTICAL_SPLIT) {
    @Override
    public int getSize(@NonNull Container parent) {
      return parent.getHeight();
    }
  },

  ;

  public final int swingId;

  public abstract int getSize(@NonNull Container parent);

  public static @NonNull SplitType valueOfDef(String str) {
    try {
      return valueOf(str);
    } catch (Exception ignore) {
      return Horizontal;
    }
  }

}
