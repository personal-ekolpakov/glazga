package kz.pompei.glazga.v3.gui.panels;

public enum MoveWinMenu {

  TO_RIGHT {
    @Override
    public WinMenuPos move(WinMenuPos current) {
      //@formatter:off
      if (current == WinMenuPos.LEFT_TOP   ) return WinMenuPos.RIGHT_TOP   ;
      if (current == WinMenuPos.LEFT_BOTTOM) return WinMenuPos.RIGHT_BOTTOM;
                                             return null;
      //@formatter:on
    }
  },


  TO_LEFT {
    @Override
    public WinMenuPos move(WinMenuPos current) {
      //@formatter:off
      if (current == WinMenuPos.RIGHT_TOP   ) return WinMenuPos.LEFT_TOP   ;
      if (current == WinMenuPos.RIGHT_BOTTOM) return WinMenuPos.LEFT_BOTTOM;
      return null;
      //@formatter:on
    }
  },


  TO_UP {
    @Override
    public WinMenuPos move(WinMenuPos current) {
      //@formatter:off
      if (current == WinMenuPos. LEFT_BOTTOM) return WinMenuPos. LEFT_TOP;
      if (current == WinMenuPos.RIGHT_BOTTOM) return WinMenuPos.RIGHT_TOP;
      return null;
      //@formatter:on
    }
  },


  TO_DOWN {
    @Override
    public WinMenuPos move(WinMenuPos current) {
      //@formatter:off
      if (current == WinMenuPos. LEFT_TOP) return WinMenuPos. LEFT_BOTTOM;
      if (current == WinMenuPos.RIGHT_TOP) return WinMenuPos.RIGHT_BOTTOM;
      return null;
      //@formatter:on
    }
  };

  public abstract WinMenuPos move(WinMenuPos current);
}
