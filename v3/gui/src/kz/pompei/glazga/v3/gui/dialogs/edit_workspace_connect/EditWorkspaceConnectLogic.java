package kz.pompei.glazga.v3.gui.dialogs.edit_workspace_connect;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.v3.gui.context.ApplicationContext;
import kz.pompei.glazga.v3.gui.dialogs.common.DialogLogic;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ConnectParams;
import lombok.NonNull;
import lombok.SneakyThrows;

import static kz.pompei.glazga.utils.JDialogUtil.handlerToDocumentListenerOver;

public class EditWorkspaceConnectLogic extends DialogLogic {

  private final ApplicationContext         context;
  private final EditWorkspaceConnectDialog dialog;

  public EditWorkspaceConnectLogic(@NonNull ApplicationContext context, @NonNull EditWorkspaceConnectDialog dialog) {
    this.context = context;
    this.dialog  = dialog;
    dialog.main.checkButton.addActionListener(this::checkConnection);

    dialog.main.hostField.getDocument().addDocumentListener(handlerToDocumentListenerOver(this::changedSomeField));
    dialog.main.portField.getDocument().addDocumentListener(handlerToDocumentListenerOver(this::changedSomeField));
    dialog.main.dbNameField.getDocument().addDocumentListener(handlerToDocumentListenerOver(this::changedSomeField));
    dialog.main.usernameField.getDocument().addDocumentListener(handlerToDocumentListenerOver(this::changedSomeField));
    dialog.main.passwordField.getDocument().addDocumentListener(handlerToDocumentListenerOver(this::changedSomeField));

    initDialogBasically();
  }

  private void changedSomeField() {
    initState();
  }

  private @NonNull ConnectParams readConnectParams() {

    String host     = dialog.main.hostField.getText();
    int    port     = Integer.parseInt(dialog.main.portField.getText());
    String username = dialog.main.usernameField.getText();
    String password = new String(dialog.main.passwordField.getPassword());
    String dbName   = dialog.main.dbNameField.getText();

    return ConnectParams.builder()
                        .host(host)
                        .port(port)
                        .username(username)
                        .password(password)
                        .dbName(dbName)
                        .build();
  }

  public ConnectParams edit(ConnectParams connectParams) {

    if (connectParams == null) {

      dialog.main.hostField.setText("localhost");
      dialog.main.portField.setText("5432");
      dialog.main.dbNameField.setText("");
      dialog.main.usernameField.setText("");
      dialog.main.passwordField.setText("");

      dialog.setTitle("Получение доступа к рабочему месту");

    } else {

      dialog.main.hostField.setText(connectParams.host == null ? "" : connectParams.host);
      dialog.main.portField.setText("" + connectParams.port);
      dialog.main.dbNameField.setText(connectParams.dbName == null ? "" : connectParams.dbName);
      dialog.main.usernameField.setText(connectParams.username == null ? "" : connectParams.username);
      dialog.main.passwordField.setText(connectParams.password == null ? "" : connectParams.password);

      dialog.setTitle("Изменение доступа к рабочему месту");
    }

    initState();

    dialog.pack();
    dialog.setVisible(true);

    if (cancel) {
      return null;
    }

    return readConnectParams();
  }

  @Override protected JButton buttonCancel() {
    return dialog.main.cancelButton;
  }

  @Override protected JButton buttonSave() {
    return dialog.main.saveButton;
  }

  @Override protected JDialog dialog() {
    return dialog;
  }

  @Override protected JComponent mainPanel() {
    return dialog.main;
  }

  private void initState() {
    dialog.main.saveButton.setEnabled(false);
    dialog.main.connectInfo.setText("Введите параметры доступа в БД PostgreSQL, где будем работать с проектами.\n" +
                                    "Чтобы отобразить кнопку \"Сохранить\" нежно проверить связь по указанным Вами параметрам.");
    dialog.main.connectInfo.setForeground(new Color(0, 0, 0));
  }

  @SneakyThrows
  private void checkConnection(ActionEvent ignore) {
    dialog.main.connectInfo.setText("Проверка доступа...");
    dialog.main.connectInfo.setForeground(new Color(181, 152, 4));
    context.executorService.submit(this::checkingConnection);
  }

  @SneakyThrows
  private void checkingConnection() {
    final ConnectParams cp = readConnectParams();

    Class.forName("org.postgresql.Driver");

    try (Connection ignore = DriverManager.getConnection(cp.url(), cp.username, cp.password)) {
      SwingUtilities.invokeLater(() -> {
        dialog.main.connectInfo.setText("Доступ получен успешно. Можно сохранить.");
        dialog.main.connectInfo.setForeground(new Color(57, 119, 25));
        dialog.main.saveButton.setEnabled(true);
      });
      return;
    } catch (Exception error) {
      SwingUtilities.invokeLater(() -> {
        dialog.main.connectInfo.setText("Ошибка получения доступа: " + error.getMessage());
        dialog.main.connectInfo.setForeground(new Color(175, 6, 46));
      });
    }
  }
}
