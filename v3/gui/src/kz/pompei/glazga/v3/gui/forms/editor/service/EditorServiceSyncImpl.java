package kz.pompei.glazga.v3.gui.forms.editor.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.v3.gui.context.model.DetailId;
import kz.pompei.glazga.v3.gui.forms.editor.model.storage.StoreCmd;
import kz.pompei.glazga.v3.gui.forms.editor.model.storage.StoreCmdClean;
import kz.pompei.glazga.v3.gui.forms.editor.model.storage.StoreCmdDel;
import kz.pompei.glazga.v3.gui.forms.editor.model.storage.StoreCmdSet;
import kz.pompei.glazga.v3.gui.forms.editor.model.storage.StoreUpd;
import kz.pompei.glazga.v3.gui.projects.ProjectService;
import kz.pompei.glazga.v3.gui.projects.WorkspaceManager;
import kz.pompei.glazga.v3.gui.projects.WorkspaceService;
import kz.pompei.v3.storage.Storage;
import lombok.NonNull;

public class EditorServiceSyncImpl implements EditorServiceSync {
  private final @NonNull DetailId       detailId;
  private final @NonNull ProjectService projectService;
  private final @NonNull Storage        storage;

  public EditorServiceSyncImpl(@NonNull WorkspaceManager workspaceManager, @NonNull DetailId detailId) {

    @NonNull WorkspaceService wsService = workspaceManager.getWsService(detailId.workspaceId);

    this.detailId       = detailId;
    this.projectService = wsService.getProjectService(detailId.projectId);
    this.storage        = projectService.getDetailStorage(detailId.detailId);
  }

  @Override public @NonNull Map<String, String> loadOneLevelMap(Set<String> prefixes) {
    return storage.loadOneLevelMap(prefixes);
  }

  @Override public void updateStore(@NonNull String undoGroup, @NonNull List<StoreCmd> cmdList) {

    for (final StoreCmd storeCmd : cmdList) {

      if (storeCmd instanceof final StoreCmdClean cmd) {
        Set<String> keys = storage.findKeys(cmd.pathPrefix);
        for (final String key : keys) {
          storage.set(key, null);
        }
        continue;
      }

      if (storeCmd instanceof final StoreCmdSet cmd) {
        storage.set(cmd.path, cmd.value);
        continue;
      }

      if (storeCmd instanceof final StoreCmdDel cmd) {
        storage.set(cmd.path, null);
        continue;
      }

      throw new RuntimeException("QWw0cIm1eS :: Unknown cmd class " + storeCmd.getClass().getSimpleName());
    }
  }

  @Override public @NonNull Set<String> findBoxIdsInRect(@NonNull Rect rect) {
    return projectService.findBoxIdsInRect(detailId.detailId, rect);
  }

  @Override public List<StoreUpd> undo() {
    throw new RuntimeException("2024-05-11 17:08 Not impl yet EditorServiceSyncImpl.undo()");
  }

  @Override public List<StoreUpd> redo() {
    throw new RuntimeException("2024-05-11 17:08 Not impl yet EditorServiceSyncImpl.redo()");
  }
}
