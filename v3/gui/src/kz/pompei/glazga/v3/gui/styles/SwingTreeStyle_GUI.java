package kz.pompei.glazga.v3.gui.styles;

import kz.pompei.swing.tree.styles.Padding;
import kz.pompei.swing.tree.styles.ScrollStyle;
import kz.pompei.swing.tree.styles.SwingTreeNodeStyle;
import kz.pompei.swing.tree.styles.SwingTreeStyle;
import kz.pompei.swing.tree.styles.defau.PaddingFix;
import lombok.NonNull;

public class SwingTreeStyle_GUI implements SwingTreeStyle {
  private final ScrollStyle_GUI        scrollStyle        = new ScrollStyle_GUI();
  private final SwingTreeNodeStyle_GUI swingTreeNodeStyle = new SwingTreeNodeStyle_GUI();

  private final PaddingFix padding = PaddingFix.of(0, 4, 0, 0);

  @Override
  public @NonNull Padding padding() {
    return padding;
  }

  @Override
  public @NonNull ScrollStyle scrollStyle() {
    return scrollStyle;
  }

  @Override
  public @NonNull SwingTreeNodeStyle node() {
    return swingTreeNodeStyle;
  }

}
