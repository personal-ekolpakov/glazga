package kz.pompei.glazga.v3.gui.panels;

import java.util.List;
import java.util.function.Supplier;
import javax.swing.JComponent;
import kz.pompei.glazga.v3.gui.context.NodeContext;
import kz.pompei.glazga.v3.gui.context.rain.RainEvent;
import lombok.NonNull;

public abstract class PanelContent {

  public final @NonNull    NodeContext             context;
  protected final @NonNull Supplier<UpDownConnect> up;
  protected final @NonNull String                  id;

  protected PanelContent(@NonNull NodeContext context, @NonNull Supplier<UpDownConnect> up) {
    this.context = context;
    this.up      = up;
    this.id      = context.storage().get(up.get().getDownPanelIdPath()).orElseThrow();
  }

  public abstract @NonNull JComponent getComponent();

  public void destroy()                                                      {}

  public abstract @NonNull List<UpDownConnect> children();

  public abstract void refreshState();

  public void acceptRainEvent(@NonNull RainEvent rainEvent) throws Exception {}

}
