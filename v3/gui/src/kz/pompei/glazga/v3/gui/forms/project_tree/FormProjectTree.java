package kz.pompei.glazga.v3.gui.forms.project_tree;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import kz.pompei.glazga.utils.RndUtil;
import kz.pompei.glazga.v3.gui.context.ApplicationContext;
import kz.pompei.glazga.v3.gui.context.model.DetailId;
import kz.pompei.glazga.v3.gui.context.rain.RainChangeDetailId;
import kz.pompei.glazga.v3.gui.dialogs.edit_txt_line.EditTxtLineDialog;
import kz.pompei.glazga.v3.gui.dialogs.edit_txt_line.EditTxtLineLogic;
import kz.pompei.glazga.v3.gui.dialogs.edit_workspace_connect.EditWorkspaceConnectDialog;
import kz.pompei.glazga.v3.gui.dialogs.edit_workspace_connect.EditWorkspaceConnectLogic;
import kz.pompei.glazga.v3.gui.dialogs.new_project.NewProjectDialog;
import kz.pompei.glazga.v3.gui.dialogs.new_project.NewProjectLogic;
import kz.pompei.glazga.v3.gui.dialogs.question.QuestionDialog;
import kz.pompei.glazga.v3.gui.dialogs.question.QuestionLogic;
import kz.pompei.glazga.v3.gui.dialogs.question.QuestionVariant;
import kz.pompei.glazga.v3.gui.forms.FormProjectTreeService;
import kz.pompei.glazga.v3.gui.forms.PanelForm;
import kz.pompei.glazga.v3.gui.forms.PanelFormDef;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ConnectParams;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItem;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemCell;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemId;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemType;
import kz.pompei.glazga.v3.gui.projects.ProjectService;
import kz.pompei.glazga.v3.gui.projects.WorkspaceManager;
import kz.pompei.glazga.v3.gui.projects.WorkspaceService;
import kz.pompei.glazga.v3.gui.styles.SwingTreeStyle_GUI;
import kz.pompei.glazga.v3.gui.util.ToolIcon;
import kz.pompei.swing.icons.Svg;
import kz.pompei.swing.tree.gui.ScrollStateOverStorage;
import kz.pompei.swing.tree.gui.SwingTree;
import kz.pompei.swing.tree.gui.SwingTreeSelectionLogic;
import kz.pompei.swing.tree.gui.SwingTreeSelectionStorageBridge;
import kz.pompei.swing.tree.gui.TreeNodeProvider;
import kz.pompei.swing.tree.gui.model.SwingTreeNode;
import kz.pompei.swing.tree.gui.model.TreeContextMenuEvent;
import kz.pompei.swing.tree.logic.NodeStatusStorageBridge;
import kz.pompei.swing.tree.logic.TreeLoadService;
import kz.pompei.swing.tree.logic.TreeLogic;
import kz.pompei.swing.tree.styles.SwingTreeStyle;
import kz.pompei.v3.storage.Storage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FormProjectTree extends PanelForm {
  private final @NonNull ApplicationContext appContext;

  private final JPanel                                mainPanel;
  private final SwingTree                             tree;
  private final TreeLogic<SwingTreeNode, ProjectItem> treeLogic;
  private final FormProjectTreeService                service;
  private final NodeStatusStorageBridge               nodeStatusStorage;

  @SuppressWarnings("unused")
  public FormProjectTree(@NonNull ApplicationContext appContext) {
    this.appContext = appContext;

    final SwingTreeStyle         treeStyle        = new SwingTreeStyle_GUI();
    final Storage                storage          = appContext.application.cd("project-tree");
    final WorkspaceManager       workspaceManager = appContext.workspaceManager;
    final FormProjectTreeService service          = new FormProjectTreeService(workspaceManager);

    //noinspection unchecked
    final TreeLoadService<ProjectItem>    async             = appContext.syncAsyncBridge.createAsync(TreeLoadService.class, service);
    final NodeStatusStorageBridge         nodeStatusStorage = new NodeStatusStorageBridge(storage.cd("node-statuses"));
    final SwingTree                       tree              = new SwingTree(treeStyle);
    final SwingTreeSelectionStorageBridge selectionBridge   = new SwingTreeSelectionStorageBridge(storage.cd("selection"));
    final SwingTreeSelectionLogic         selectionLogic    = new SwingTreeSelectionLogic(selectionBridge);

    selectionLogic.multiple    = () -> true;
    selectionLogic.selectCells = () -> false;
    tree.setScrollState(new ScrollStateOverStorage(storage.cd("scroll")));
    tree.setSelection(selectionLogic);
    tree.contextMenuRequested.add(this::contextMenuRequested);
    final TreeLogic<SwingTreeNode, ProjectItem> treeLogic = new TreeLogic<>(tree, nodeStatusStorage, async, treeNodeProvider);

    final JPanel mainPanel     = new JPanel(new BorderLayout());
    final JPanel topPanel      = new JPanel(new BorderLayout());
    final JPanel topLeftPanel  = new JPanel(new FlowLayout(FlowLayout.LEFT, 3, 3));
    final JPanel topRightPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 3, 3));

    mainPanel.add(topPanel, BorderLayout.NORTH);
    mainPanel.add(tree, BorderLayout.CENTER);
    topPanel.add(topLeftPanel, BorderLayout.WEST);
    topPanel.add(topRightPanel, BorderLayout.EAST);

    topPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY));
    topLeftPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));

    final ToolIcon i1 = new ToolIcon(5, Svg.images.get(Svg.TRANSPARENT, 16, 16), null);
    final ToolIcon i2 = new ToolIcon(5, Svg.images.get("svg/addJdk.svg", 16, 16), this::clickAddWorkspaceToolIcon);
    final ToolIcon i3 = new ToolIcon(5, Svg.images.get("svg/locate.svg", 16, 16), null);

    i3.setEnabled(false);

    topLeftPanel.add(i1);
    topRightPanel.add(i2);
    topRightPanel.add(i3);

    tree.doubleClicked.add(this::doubleClicked);

    this.service           = service;
    this.tree              = tree;
    this.treeLogic         = treeLogic;
    this.mainPanel         = mainPanel;
    this.nodeStatusStorage = nodeStatusStorage;

    treeLogic.refresh(null);
  }

  @SuppressWarnings("FieldCanBeLocal")
  private final TreeNodeProvider<SwingTreeNode, ProjectItem> treeNodeProvider = new TreeNodeProvider<>() {
    @Override public @NonNull SwingTreeNode newTreeNode(@NonNull String nodeId) {
      return new SwingTreeNode(tree, nodeId);
    }

    @Override public @NonNull SwingTreeNode newWaitingNode(String parentId, int offset, int pageSize) {
      SwingTreeNode node = new SwingTreeNode(tree, RndUtil.newId());
      node.nodeType = "wait";
      node.cell("wait", "wait", "wait");
      return node;
    }

    @Override public @NonNull SwingTreeNode newMoreNode() {
      SwingTreeNode node = new SwingTreeNode(tree, RndUtil.newId());
      node.nodeType = "more";
      node.cell("more", "more", "more");
      return node;
    }

    @Override public void assign(@NonNull SwingTreeNode target, @NonNull ProjectItem source) {
      target.cells.clear();
      target.nodeType = source.nodeType;
      for (final ProjectItemCell cell : source.cells) {
        target.cell(cell.id, cell.name, cell.type);
      }
    }
  };

  @SuppressWarnings("unused")
  public static @NonNull PanelFormDef definition() {
    var ret = new PanelFormDef();
    ret.gridX     = 1;
    ret.gridY     = 1;
    ret.menuLabel = "Структура проекта";
    return ret;
  }

  @Override
  public JComponent getComponent() {
    return mainPanel;
  }

  private void clickAddWorkspaceToolIcon(MouseEvent e) {
    showDialog__editWorkspaceCP(new Point(e.getXOnScreen(), e.getYOnScreen()), null);
  }

  private void contextMenuRequested(@NonNull TreeContextMenuEvent e) {

    final ProjectItemId   itemId   = ProjectItemId.parse(tree.selection().activeNodeId()).orElse(null);
    final ProjectItemType itemType = itemId == null ? null : itemId.type;

    JPopupMenu contextMenu = new JPopupMenu();
    if (itemType == ProjectItemType.WORKSPACE) {
      {// Workspace EDIT connectParams
        JMenuItem miAddWorkspace = new JMenuItem("Изменить параметры доступа...");
        miAddWorkspace.setIcon(new ImageIcon(Svg.images.get("svg/edit.svg", 16, 16)));
        contextMenu.add(miAddWorkspace);
        miAddWorkspace.addActionListener(__ -> showDialog__editWorkspaceCP(e.screenPoint, itemId.workspaceId));
      }
      {// Workspace EDIT name
        JMenuItem miAddWorkspace = new JMenuItem("Изменить имя...");
        miAddWorkspace.setIcon(new ImageIcon(Svg.images.get("svg/javadocEdit.svg", 16, 16)));
        contextMenu.add(miAddWorkspace);
        miAddWorkspace.addActionListener(__ -> showDialog__editWorkspaceName(e.screenPoint, itemId.workspaceId));
      }
      contextMenu.addSeparator();
      {// Workspace Add Project
        JMenuItem miAddWorkspace = new JMenuItem("Создать новый проект...");
        miAddWorkspace.setIcon(new ImageIcon(Svg.images.get("svg/project.svg", 16, 16)));
        contextMenu.add(miAddWorkspace);
        miAddWorkspace.addActionListener(__ -> showDialog_createNewProject(e.screenPoint, itemId.workspaceId));
      }
    }
    if (itemType == ProjectItemType.PROJECT) {
      {// Add Detail CLASS
        JMenuItem miAddWorkspace = new JMenuItem("Добавить программу...");
        miAddWorkspace.setIcon(new ImageIcon(Svg.images.get("svg/java.svg", 16, 16)));
        contextMenu.add(miAddWorkspace);
        miAddWorkspace.addActionListener(__ -> showDialog__addNewProgram(e.screenPoint, itemId.workspaceId, itemId.projectId));
      }

      contextMenu.addSeparator();

      {// Workspace EDIT project name
        JMenuItem miAddWorkspace = new JMenuItem("Изменить имя проекта...");
        miAddWorkspace.setIcon(new ImageIcon(Svg.images.get("svg/edit.svg", 16, 16)));
        contextMenu.add(miAddWorkspace);
        miAddWorkspace.addActionListener(__ -> showDialog__editProjectName(e.screenPoint, itemId.workspaceId, itemId.projectId));
      }
    }
    if (itemType == ProjectItemType.DETAIL) {
      {// Detail EDIT
        JMenuItem miAddWorkspace = new JMenuItem("Изменить имя...");
        miAddWorkspace.setIcon(new ImageIcon(Svg.images.get("svg/edit.svg", 16, 16)));
        contextMenu.add(miAddWorkspace);
        miAddWorkspace.addActionListener(__ -> showDialog__editDetail(e.screenPoint, itemId.workspaceId, itemId.projectId, itemId.id));
      }
      {// Detail Delete
        JMenuItem miAddWorkspace = new JMenuItem("Удалить");
        miAddWorkspace.setIcon(new ImageIcon(Svg.images.get("svg/delete.svg", 16, 16)));
        contextMenu.add(miAddWorkspace);
        miAddWorkspace.addActionListener(
          __ -> showDialog__deleteDetail(e.screenPoint, itemId.workspaceId, itemId.projectId, itemId.id));
      }
    }

    if (contextMenu.getComponentCount() > 0) {
      contextMenu.show(tree, e.point.x, e.point.y);
    }
  }

  private void showDialog__deleteDetail(Point screenPoint,
                                        @NonNull String workspaceId,
                                        @NonNull String projectId,
                                        @NonNull String detailId) {

    final WorkspaceService wsService      = service.workspaceManager.getWsService(workspaceId);
    final ProjectService   projectService = wsService.getProjectService(projectId);
    final String           name           = projectService.getDetailName(detailId);
    QuestionDialog         dialog         = new QuestionDialog(appContext.mainFrame, screenPoint);
    QuestionLogic          logic          = new QuestionLogic(dialog);

    dialog.setTitle("Производиться опасная операция");
    dialog.main.setPreferredSize(new Dimension(300, 100));

    String result = logic.show("Удаление детали `" + name + "`", List.of(
      QuestionVariant.of("ok", "Подтверждаю удаление", new ImageIcon(Svg.images.get("svg/delete.svg", 16, 16))),
      QuestionVariant.of(null, "Отменя удаления", new ImageIcon(Svg.images.get("svg/adm-info.svg", 16, 16)))
    ));

    if (!"ok".equals(result)) {
      return;
    }

    projectService.deleteDetail(detailId);
    var selectingItemId = ProjectItemId.detail(workspaceId, projectId, detailId);
    treeLogic.refresh(selectingItemId.strId());
  }

  private void showDialog__editDetail(Point screenPoint,
                                      @NonNull String workspaceId,
                                      @NonNull String projectId,
                                      @NonNull String detailId) {

    final WorkspaceService  wsService      = service.workspaceManager.getWsService(workspaceId);
    final ProjectService    projectService = wsService.getProjectService(projectId);
    final String            name           = projectService.getDetailName(detailId);
    final EditTxtLineDialog dialog         = new EditTxtLineDialog(appContext.mainFrame, screenPoint);
    final EditTxtLineLogic  logic          = new EditTxtLineLogic(dialog);
    final String            newName        = logic.edit(name);

    if (newName == null || newName.equals(name)) {
      return;
    }

    projectService.setDetailName(detailId, newName);
    final ProjectItemId selectingItemId = ProjectItemId.detail(workspaceId, projectId, detailId);
    treeLogic.refresh(selectingItemId.strId());
  }

  private void showDialog__addNewProgram(Point screenPoint, @NonNull String workspaceId, @NonNull String projectId) {

    final EditTxtLineDialog dialog  = new EditTxtLineDialog(appContext.mainFrame, screenPoint);
    final EditTxtLineLogic  logic   = new EditTxtLineLogic(dialog);
    final String            newName = logic.edit(null);

    if (newName == null) {
      return;
    }

    final WorkspaceService wsService      = service.workspaceManager.getWsService(workspaceId);
    final ProjectService   projectService = wsService.getProjectService(projectId);
    final String           detailId       = RndUtil.newId();

    projectService.setDetailName(detailId, newName);
    projectService.setDetailType(detailId, "program");

    final ProjectItemId parentItemId    = ProjectItemId.project(workspaceId, projectId);
    final ProjectItemId selectingItemId = ProjectItemId.detail(workspaceId, projectId, detailId);

    nodeStatusStorage.setOpen(parentItemId.strId(), true);
    tree.selection().setSelectedNodeIds(Set.of(selectingItemId.strId()));
    treeLogic.refresh(parentItemId.strId());
  }

  private void showDialog__editProjectName(Point screenPoint, @NonNull String workspaceId, @NonNull String projectId) {
    final EditTxtLineDialog dialog         = new EditTxtLineDialog(appContext.mainFrame, screenPoint);
    final EditTxtLineLogic  logic          = new EditTxtLineLogic(dialog);
    final WorkspaceService  wsService      = service.workspaceManager.getWsService(workspaceId);
    final ProjectService    projectService = wsService.getProjectService(projectId);
    final String            newName        = logic.edit(projectService.getProjectName());

    if (newName == null) {
      return;
    }

    projectService.setProjectName(newName);

    ProjectItemId itemId = ProjectItemId.project(workspaceId, projectId);

    treeLogic.refresh(itemId.strId());
  }

  private void showDialog_createNewProject(@NonNull Point screentPoint, @NonNull String workspaceId) {

    final NewProjectDialog dialog      = new NewProjectDialog(appContext.mainFrame, screentPoint);
    final NewProjectLogic  logic       = new NewProjectLogic(dialog);
    final String           projectName = logic.create();

    if (projectName == null) {
      return;
    }

    final WorkspaceService wsService = service.workspaceManager.getWsService(workspaceId);
    final String           projectId = wsService.createProject(projectName);

    nodeStatusStorage.setOpen(ProjectItemId.workspace(workspaceId).strId(), true);
    treeLogic.refresh(ProjectItemId.workspace(workspaceId).strId());
    tree.selection().setSelectedNodeIds(Set.of(ProjectItemId.project(workspaceId, projectId).strId()));
  }

  private void showDialog__editWorkspaceName(@NonNull Point screentPoint, @NonNull String workspaceId) {

    final String startName = service.workspaceManager.loadWorkspaceName(workspaceId);
    final var    dialog    = new EditTxtLineDialog(appContext.mainFrame, screentPoint);
    final var    logic     = new EditTxtLineLogic(dialog);
    final String endName   = logic.edit(startName);

    if (endName == null || endName.equals(startName)) {
      return;
    }

    service.workspaceManager.saveWorkspaceName(workspaceId, endName);
    treeLogic.refresh(ProjectItemId.workspace(workspaceId).strId());
  }

  private void showDialog__editWorkspaceCP(@NonNull Point screentPoint, String workspaceId) {

    ConnectParams inputCP = null;

    if (workspaceId != null) {
      inputCP = service.workspaceManager.loadConnectParams(workspaceId);

      if (inputCP == null) {
        treeLogic.refresh(null);
        JOptionPane.showMessageDialog(tree, "Данной рабочей области уже нет", "Сообщение", JOptionPane.INFORMATION_MESSAGE);
        return;
      }
    }

    final var           dialog = new EditWorkspaceConnectDialog(appContext.mainFrame, screentPoint);
    final var           logic  = new EditWorkspaceConnectLogic(appContext, dialog);
    final ConnectParams outCP  = logic.edit(inputCP);

    if (outCP == null) {
      return;
    }

    if (outCP.equals(inputCP)) {
      return;
    }

    if (inputCP == null) {
      workspaceId = service.workspaceManager.addWorkspace(outCP);
      final String nodeId = ProjectItemId.workspace(workspaceId).strId();

      tree.selection().setSelectedNodeIds(Set.of(nodeId));
      this.treeLogic.refresh(null);
    } else {
      service.workspaceManager.updateWorkspaceCP(workspaceId, outCP);
      this.treeLogic.refresh(ProjectItemId.workspace(workspaceId).strId());
    }

  }

  private void doubleClicked(@NonNull SwingTreeNode node) {
    ProjectItemId itemId = ProjectItemId.parse(node.id).orElse(null);
    if (itemId != null && itemId.type == ProjectItemType.DETAIL) {

      String workspaceId = itemId.workspaceId;
      String projectId   = itemId.projectId;
      String detailId    = itemId.id;

      appContext.setDetailId(new DetailId(workspaceId, projectId, detailId));
      appContext.rainEventObserver.fire(RainChangeDetailId.ID);
    }
  }

}
