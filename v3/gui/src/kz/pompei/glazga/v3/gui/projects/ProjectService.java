package kz.pompei.glazga.v3.gui.projects;

import java.util.Optional;
import java.util.Set;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.v3.gui.projects.model.DetailDirRecord;
import kz.pompei.glazga.v3.gui.projects.model.DetailDirRecordPortion;
import kz.pompei.v3.storage.Storage;
import lombok.NonNull;

public interface ProjectService {

  String getProjectName();

  void setProjectName(@NonNull String projectName);

  @NonNull Storage getDetailStorage(@NonNull String detailId);

  void deleteDetail(@NonNull String detailId);

  void setDetailName(@NonNull String detailId, String detailName);

  String getDetailName(@NonNull String detailId);

  void setDetailType(@NonNull String detailId, String detailType);

  String getDetailType(@NonNull String detailId);

  void setDetailPath(@NonNull String detailId, String path);

  String getDetailPath(@NonNull String detailId);

  @NonNull DetailDirRecordPortion loadDetailPortion(@NonNull String parentDirId, Page page);

  @NonNull Optional<DetailDirRecord> loadDetailItem(String detailId);

  @NonNull Set<String> findBoxIdsInRect(@NonNull String detailId, @NonNull Rect rect);
}
