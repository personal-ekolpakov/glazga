package kz.pompei.glazga.v3.gui.forms.editor.logic;

import javax.swing.JComponent;
import javax.swing.JPanel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EditorLogicError extends EditorLogic {
  private final @NonNull Throwable error;

  private final JPanel panel = new JPanel();

  @Override public void refresh() {
    System.out.println("jHN0Z2Z7ED :: refresh");
  }

  @Override public JComponent getComponent() {
    return panel;
  }
}
