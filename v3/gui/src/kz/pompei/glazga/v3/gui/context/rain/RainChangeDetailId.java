package kz.pompei.glazga.v3.gui.context.rain;

public class RainChangeDetailId implements RainEvent {
  private RainChangeDetailId() {}

  public static final RainChangeDetailId ID = new RainChangeDetailId();
}
