package kz.pompei.glazga.v3.gui.util;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;

public class Sizes {

  public static void setSizeAndLocation(JFrame frame, float displayPart) {

    Toolkit   toolkit    = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();

    frame.setSize(Math.round(screenSize.width * displayPart),
                   Math.round(screenSize.height * displayPart));

    frame.setLocationRelativeTo(null);

  }

}
