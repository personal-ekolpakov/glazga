package kz.pompei.glazga.v3.gui.panels;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import lombok.NonNull;

public enum WinMenuPos {


  LEFT_TOP {
    @Override
    public void setBounds(@NonNull JPanel menuPanel, JLayeredPane owner) {
      menuPanel.setBounds(PADDING, PADDING, WIDTH, HEIGHT);
    }
  },


  RIGHT_TOP {
    @Override
    public void setBounds(@NonNull JPanel menuPanel, JLayeredPane owner) {
      menuPanel.setBounds(owner.getWidth() - PADDING - WIDTH, PADDING, WIDTH, HEIGHT);
    }
  },


  RIGHT_BOTTOM {
    @Override
    public void setBounds(@NonNull JPanel menuPanel, JLayeredPane owner) {
      menuPanel.setBounds(owner.getWidth() - PADDING - WIDTH, owner.getHeight() - PADDING - HEIGHT, WIDTH, HEIGHT);
    }
  },


  LEFT_BOTTOM {
    @Override
    public void setBounds(@NonNull JPanel menuPanel, JLayeredPane owner) {
      menuPanel.setBounds(PADDING, owner.getHeight() - PADDING - HEIGHT, WIDTH, HEIGHT);
    }
  };

  private static final int PADDING = 3;
  private static final int WIDTH   = 10;
  private static final int HEIGHT  = 7;


  public static @NonNull WinMenuPos valueOfDef(String value) {
    try {
      return valueOf(value);
    } catch (Exception ignore) {
      return LEFT_TOP;
    }
  }

  public abstract void setBounds(@NonNull JPanel menuPanel, JLayeredPane owner);
}
