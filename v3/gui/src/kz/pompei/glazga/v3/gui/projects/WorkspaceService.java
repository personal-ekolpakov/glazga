package kz.pompei.glazga.v3.gui.projects;

import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.v3.gui.projects.model.ProjectRecord;
import kz.pompei.glazga.v3.gui.projects.model.ProjectRecordPortion;
import lombok.NonNull;

public interface WorkspaceService {

  /**
   * Предоставляет название типа рабочего пространства
   *
   * @return название типа рабочего пространства. Например: PostgreSQL
   */
  String typeDisplayStr();

  /**
   * Получает наименование рабочего пространства
   *
   * @return наименование рабочего пространства
   */
  String workspaceName();

  /**
   * Меняет наименование рабочего пространства
   *
   * @param workspaceName новое наименование рабочего пространства
   */
  void setWorkspaceName(String workspaceName);

  String connectParamsStr();

  /**
   * Создаёт проект
   *
   * @param name имя нового проекта
   * @return идентификатор нового проекта
   */
  String createProject(@NonNull String name);

  /**
   * Удаляет проект по идентификатору
   *
   * @param projectId идентификатор проекта
   */
  void deleteProject(@NonNull String projectId);

  /**
   * Получает список проектов отсортированных по имени
   *
   * @param page страница списка
   * @return список проекта
   */
  @NonNull ProjectRecordPortion loadProjectPortion(Page page);

  /**
   * Загружает запись проекта по идентификатору
   *
   * @param projectId идентификатор проекта
   * @return запись проекта
   */
  ProjectRecord loadProjectRecord(@NonNull String projectId);

  @NonNull ProjectService getProjectService(@NonNull String projectId);

}
