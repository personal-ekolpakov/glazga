package kz.pompei.glazga.v3.gui.panels;

import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.v3.gui.context.NodeContext;
import lombok.NonNull;

import static kz.pompei.glazga.utils.NumUtil.parseDoubleDef;

public class PanelSplitter extends PanelContent {

  private final JSplitPane faceComponent;
  private final String     id;

  public PanelSplitter(NodeContext context, Supplier<UpDownConnect> up) {
    super(context, up);
    this.id = context.storage().get(up.get().getDownPanelIdPath()).orElseThrow();

    SplitType splitType = SplitType.valueOfDef(context.storage().get(id + ".splitType").orElse(null));
    faceComponent = new JSplitPane(splitType.swingId);
    faceComponent.setDividerSize(3);

    faceComponent.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, evt -> {

      if (!context.saveState.get()) {
        return;
      }

      int    newValue    = (Integer) evt.getNewValue();
      int    fieldSize   = splitType.getSize(faceComponent) - faceComponent.getDividerSize();
      double splitFactor = fieldSize <= 0 ? 0.5 : (double) newValue / (double) fieldSize;

      if (0.05 < splitFactor && splitFactor < 0.95) {
        context.storage().set(id + ".splitFactor", "" + splitFactor);
        context.saveState.set(false);
        SwingUtilities.invokeLater(this::restoreSplitFactorAll);
      }
    });

  }

  private void restoreSplitFactorAll() {
    context.saveState.set(false);
    try {

      UpDownConnect upDownRoot = context.getUpDownRoot();
      if (upDownRoot == null) {
        return;
      }

      List<UpDownConnect> udList = new ArrayList<>();
      udList.add(upDownRoot);

      while (udList.size() > 0) {

        UpDownConnect ud = udList.remove(0);

        String panelId = context.storage().get(ud.getDownPanelIdPath()).orElse(null);
        if (panelId == null) {
          continue;
        }

        PanelNode node = context.getNodeByPanelId(panelId);
        if (node == null) {
          continue;
        }

        PanelContent panel = node.content;
        udList.addAll(panel.children());

        if (panel instanceof PanelSplitter split) {
          split.restoreSplitFactor1();
        }

      }

    } finally {
      new Thread(() -> {
        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {
          return;
        }
        context.saveState.set(true);
      }).start();
    }

  }

  private void restoreSplitFactor1() {
    JSplitPane faceComponent = this.faceComponent;
    if (faceComponent == null) {
      return;
    }

    String id = context.storage().get(up.get().getDownPanelIdPath()).orElseThrow();

    double splitFactor = parseDoubleDef(context.storage().get(id + ".splitFactor").orElse(null), 0.1);

    faceComponent.setDividerLocation(splitFactor);
  }

  @Override
  public @NonNull JComponent getComponent() {
    return faceComponent;
  }

  private final UpDownConnect first = new UpDownConnect() {

    @Override
    public Container ownerContainer() {
      return faceComponent;
    }

    @Override
    public void setDownComponent(JComponent child) {
      if (faceComponent.getTopComponent() != child) {
        faceComponent.setTopComponent(child);
      }
    }

    @Override
    public @NonNull String getDownPanelIdPath() {
      return id + ".firstId";
    }

    @Override
    public boolean canDeleteDown() {
      return true;
    }

    @Override
    public void deleteDown() {
      String myDownPanelId     = context.storage().get(getDownPanelIdPath()).orElseThrow();
      String secondDownPanelId = context.storage().get(second.getDownPanelIdPath()).orElseThrow();

      context.updating()
             .cleaning(id)
             .cleaning(myDownPanelId)
             .set(up.get().getDownPanelIdPath(), secondDownPanelId)
             .update();

      context.refresh();

    }
  };

  private final UpDownConnect second = new UpDownConnect() {

    @Override
    public Container ownerContainer() {
      return faceComponent;
    }

    @Override
    public void setDownComponent(JComponent child) {
      if (faceComponent.getBottomComponent() != child) {
        faceComponent.setBottomComponent(child);
      }
    }

    @Override
    public @NonNull String getDownPanelIdPath() {
      return id + ".secondId";
    }

    @Override
    public void deleteDown() {
      String myDownPanelId    = context.storage().get(getDownPanelIdPath()).orElseThrow();
      String firstDownPanelId = context.storage().get(first.getDownPanelIdPath()).orElseThrow();

      context.updating()
             .cleaning(id)
             .cleaning(myDownPanelId)
             .set(up.get().getDownPanelIdPath(), firstDownPanelId)
             .update();

      context.refresh();
    }

    @Override
    public boolean canDeleteDown() {
      return true;
    }
  };

  @Override
  public @NonNull List<UpDownConnect> children() {
    return List.of(first, second);
  }

  @Override
  public void refreshState() {
    restoreSplitFactor1();
  }
}
