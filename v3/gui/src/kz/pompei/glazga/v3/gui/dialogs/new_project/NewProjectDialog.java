package kz.pompei.glazga.v3.gui.dialogs.new_project;

import java.awt.Point;
import java.nio.file.Path;
import javax.swing.JComponent;
import javax.swing.JFrame;
import kz.pompei.glazga.utils.Locations;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.glazga.v3.gui.dialogs.common.DialogParent;

public class NewProjectDialog extends DialogParent {
  public final NewProjectPanel main = new NewProjectPanel();

  public NewProjectDialog(JFrame owner, Point mousePointOnScreen) {
    super(owner);
    initDialog(mousePointOnScreen);
    setTitle("Создание нового проекта");
  }

  @Override protected JComponent mainPanel() {
    return main;
  }

  public static void main(String[] args) {
    NewProjectDialog dialog = new NewProjectDialog(null, null);
    dialog.pack();

    Path rootFolder = Locations.buildConfig();
    SizeLocationSaver.into(rootFolder.resolve("sizes_and_locations"))
                     .overJFrame("NewProjectDialog-form", dialog, false);

    dialog.setVisible(true);

    System.exit(0);
  }
}
