package kz.pompei.glazga.v3.gui.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import kz.pompei.glazga.utils.GuiUtil;
import kz.pompei.glazga.utils.events.Handler;
import lombok.NonNull;
import lombok.SneakyThrows;

public class ToolIcon extends JComponent {

  private final Dimension fixedSize;
  private final Image     image;
  private final Image     imageGray;
  private final int       padding;

  private boolean mouseEntered = false;

  public ToolIcon(int padding, @NonNull Image image, Handler<MouseEvent> clickHandler) {
    this.image   = image;
    fixedSize    = new Dimension(image.getWidth(null) + padding * 2, image.getHeight(null) + padding * 2);
    imageGray    = GuiUtil.createGrayImage(image, 0.5f);
    this.padding = padding;
    addMouseListener(new MouseAdapter() {
      @Override public void mouseEntered(MouseEvent e) {
        mouseEntered = true;
        revalidate();
        repaint();
      }

      @Override public void mouseExited(MouseEvent e) {
        mouseEntered = false;
        revalidate();
        repaint();
      }

      @Override @SneakyThrows public void mouseClicked(MouseEvent e) {
        if (clickHandler != null) {
          clickHandler.handle(e);
        }
      }
    });
  }


  @Override
  public Dimension getMinimumSize() {
    return fixedSize;
  }

  @Override
  public Dimension getPreferredSize() {
    return fixedSize;
  }

  @Override
  public Dimension getMaximumSize() {
    return fixedSize;
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    GuiUtil.applyHints(g);
    if (mouseEntered && isEnabled()) {
      g.setColor(new Color(0, 0, 0, 27));
      g.fillRoundRect(0, 0, getWidth(), getHeight(), 10, 10);
    }
    g.drawImage(isEnabled() ? image : imageGray, padding, padding, this);
  }

}
