package kz.pompei.glazga.v3.gui.forms.editor.logic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;
import javax.swing.JPanel;
import lombok.NonNull;

public class EditorLogicEmpty extends EditorLogic {

  private final JPanel panel = new JPanel() {
    @Override protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      paintPanel((Graphics2D) g);
    }
  };

  private void paintPanel(@NonNull Graphics2D g) {
    g.setColor(new Color(162, 39, 189));
    g.drawString("ERROR", 100, 100);
  }

  @Override public void refresh() {
    System.out.println("SIr1qcWvPA :: Refresh");
  }

  @Override public JComponent getComponent() {
    return panel;
  }
}
