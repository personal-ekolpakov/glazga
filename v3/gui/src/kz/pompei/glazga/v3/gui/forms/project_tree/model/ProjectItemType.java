package kz.pompei.glazga.v3.gui.forms.project_tree.model;

public enum ProjectItemType {
  WORKSPACE, PROJECT, DETAIL, FOLDER,
}
