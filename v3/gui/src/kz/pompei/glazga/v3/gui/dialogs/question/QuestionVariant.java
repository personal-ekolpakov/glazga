package kz.pompei.glazga.v3.gui.dialogs.question;

import javax.swing.Icon;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class QuestionVariant {
  public final          String result;
  public final @NonNull String text;
  public final          Icon   icon;

  public static QuestionVariant of(String result, @NonNull String text, Icon icon) {
    return new QuestionVariant(result, text, icon);
  }

  public static QuestionVariant of(String result, @NonNull String text) {
    return of(result, text, null);
  }
}
