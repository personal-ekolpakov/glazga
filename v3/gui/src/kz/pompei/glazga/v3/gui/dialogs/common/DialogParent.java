package kz.pompei.glazga.v3.gui.dialogs.common;

import java.awt.BorderLayout;
import java.awt.Point;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;

public abstract class DialogParent extends JDialog {
  public DialogParent(JFrame owner) {
    super(owner);
  }

  protected abstract JComponent mainPanel();

  protected void initDialog(Point mousePointOnScreen) {
    setModal(true);

    setLayout(new BorderLayout());

    add(mainPanel(), BorderLayout.CENTER);

    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

    if (mousePointOnScreen != null) {
      setLocation(mousePointOnScreen);
    }
  }
}
