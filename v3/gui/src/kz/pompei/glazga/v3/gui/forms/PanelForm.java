package kz.pompei.glazga.v3.gui.forms;

import java.util.List;
import javax.swing.JComponent;
import kz.pompei.glazga.v3.gui.context.rain.RainEvent;
import kz.pompei.glazga.v3.gui.forms.project_tree.FormProjectTree;
import lombok.NonNull;

public abstract class PanelForm {
  public static final List<Class<?>> formClasses = List.of(
    FormProjectTree.class, FormEditor.class, FormEditorTabs.class
  );

  public abstract JComponent getComponent();

  public void destroy()      {}

  public void refreshState() {}

  public static @NonNull Class<?> findClass(String classSimpleName) {
    for (final Class<?> formClass : formClasses) {
      if (formClass.getSimpleName().equals(classSimpleName)) {
        return formClass;
      }
    }
    throw new RuntimeException("1QqU4nVw7H :: No such class " + classSimpleName + " in " + formClasses);
  }

  @SuppressWarnings("RedundantThrows")
  public void acceptRainEvent(@NonNull RainEvent rainEvent) throws Exception {
    // Override it, if you want the rain
  }
}
