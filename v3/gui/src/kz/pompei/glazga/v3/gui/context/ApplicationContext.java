package kz.pompei.glazga.v3.gui.context;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.events.HandlerObserver;
import kz.pompei.glazga.v3.gui.context.model.DetailId;
import kz.pompei.glazga.v3.gui.context.rain.RainEvent;
import kz.pompei.glazga.v3.gui.projects.WorkspaceManager;
import kz.pompei.swing.tree.logic.SyncAsyncBridge;
import kz.pompei.v3.storage.Storage;
import lombok.NonNull;

public class ApplicationContext {
  public final @NonNull  JFrame           mainFrame;
  public final @NonNull  Storage          application;
  public final @NonNull  SyncAsyncBridge  syncAsyncBridge;
  public final @NonNull  ExecutorService  executorService;
  public final @NonNull  WorkspaceManager workspaceManager;
  private final @NonNull Storage          context;

  public final HandlerObserver<RainEvent> rainEventObserver = new HandlerObserver<>();

  public void setDetailId(DetailId detailId) {
    context.set("detailId", detailId == null ? null : detailId.strValue());
  }

  public DetailId getDetailId() {
    return context.get("detailId").flatMap(DetailId::parse).orElse(null);
  }

  public ApplicationContext(@NonNull JFrame mainFrame, @NonNull Storage application) {
    this.mainFrame   = mainFrame;
    this.application = application;
    this.context     = application.cd("context");

    workspaceManager = new WorkspaceManager(application.cd("workspaces"));
    executorService  = new ThreadPoolExecutor(1, 1, 10L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
    syncAsyncBridge  = new SyncAsyncBridge(SwingUtilities::invokeLater, executorService::submit);
  }
}
