package kz.pompei.glazga.v3.gui.forms.editor.logic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import kz.pompei.glazga.v3.gui.context.model.DetailId;
import kz.pompei.glazga.v3.gui.forms.editor.service.EditorService;
import kz.pompei.swing.icons.Svg;
import kz.pompei.v3.storage.Storage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EditorLogicDetail extends EditorLogic {
  private final @NonNull EditorService service;
  private final @NonNull DetailId      detailId;
  private final @NonNull Storage       userStorage;

  private final JPanel panel = new JPanel() {
    @Override protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      paintPanel((Graphics2D) g);
    }
  };

  @Override public void initialize() {
    panel.addMouseListener(new MouseAdapter() {
      @Override public void mousePressed(MouseEvent e) {

        System.out.println("85ta7Mw1ec :: mousePressed " + e);

        if (e.getButton() == MouseEvent.BUTTON3 && e.getClickCount() == 1) {
          showContextMenu(e);
          return;
        }

      }
    });
  }

  private void showContextMenu(MouseEvent e) {
    JPopupMenu contextMenu = new JPopupMenu();

    {// Workspace EDIT connectParams
      JMenuItem miAddWorkspace = new JMenuItem("Элемент меню");
      miAddWorkspace.setIcon(new ImageIcon(Svg.images.get("svg/edit.svg", 16, 16)));
      contextMenu.add(miAddWorkspace);
      miAddWorkspace.addActionListener(__ -> selectedMenuElement(e));
    }

    contextMenu.show(panel, e.getX(), e.getY());
  }

  private void selectedMenuElement(MouseEvent e) {
    System.out.println("UeML8IITl1 :: selectedMenuElement");
  }

  @Override public void refresh() {
    panel.repaint();
  }

  @Override public JComponent getComponent() {
    return panel;
  }

  private void paintPanel(@NonNull Graphics2D g) {
    g.setColor(new Color(218, 15, 15));
    g.drawRect(0, 0, panel.getWidth() - 1, panel.getHeight() - 1);
    g.drawRect(5, 5, panel.getWidth() - 10, panel.getHeight() - 10);
    g.drawString(detailId.strValue(), 50, 50);
  }

}
