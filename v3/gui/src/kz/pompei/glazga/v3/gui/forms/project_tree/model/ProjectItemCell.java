package kz.pompei.glazga.v3.gui.forms.project_tree.model;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProjectItemCell {
  public final String id;
  public final String name;
  public final String type;
}
