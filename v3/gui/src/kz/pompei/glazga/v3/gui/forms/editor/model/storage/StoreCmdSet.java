package kz.pompei.glazga.v3.gui.forms.editor.model.storage;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class StoreCmdSet extends StoreCmd {
  public final @NonNull String path;
  public final @NonNull String value;
}
