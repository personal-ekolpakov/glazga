package kz.pompei.glazga.v3.gui.forms.editor.model.storage;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class StoreCmdClean extends StoreCmd {
  public final @NonNull String pathPrefix;
}
