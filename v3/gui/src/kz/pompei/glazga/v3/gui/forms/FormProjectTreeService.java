package kz.pompei.glazga.v3.gui.forms;

import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItem;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemId;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemType;
import kz.pompei.glazga.v3.gui.projects.ProjectService;
import kz.pompei.glazga.v3.gui.projects.WorkspaceManager;
import kz.pompei.glazga.v3.gui.projects.WorkspaceService;
import kz.pompei.glazga.v3.gui.projects.model.DetailDirRecordPortion;
import kz.pompei.glazga.v3.gui.projects.model.ProjectRecordPortion;
import kz.pompei.glazga.v3.gui.projects.model.WorkspaceRecordPortion;
import kz.pompei.swing.tree.logic.TreeLoadServiceSync;
import kz.pompei.swing.tree.logic.model.TreeNodesPortion;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FormProjectTreeService implements TreeLoadServiceSync<ProjectItem> {
  public final WorkspaceManager workspaceManager;

  @Override public TreeNodesPortion<ProjectItem> loadNodeChildren(String parentId, Page page) {
    if (parentId == null) {
      final WorkspaceRecordPortion portion = workspaceManager.getWorkspaceRecordList(page);
      return portion.convertToTreeView();
    }

    ProjectItemId itemId = ProjectItemId.parse(parentId).orElse(null);

    if (itemId == null) {
      return TreeNodesPortion.empty(page);
    }

    if (itemId.type == ProjectItemType.WORKSPACE) {
      WorkspaceService     service = workspaceManager.getWsService(itemId.workspaceId);
      ProjectRecordPortion portion = service.loadProjectPortion(page);
      return portion.convertToTreeView(itemId.workspaceId);
    }

    if (itemId.type == ProjectItemType.PROJECT) {
      WorkspaceService       wsService  = workspaceManager.getWsService(itemId.workspaceId);
      ProjectService         prjService = wsService.getProjectService(itemId.projectId);
      DetailDirRecordPortion portion    = prjService.loadDetailPortion("", page);
      return portion.convertToTreeView(itemId.workspaceId, itemId.projectId);
    }

    return TreeNodesPortion.empty(page);
  }

  @Override public ProjectItem loadNode(@NonNull String nodeId) {
    final ProjectItemId itemId = ProjectItemId.parse(nodeId).orElse(null);
    if (itemId == null) {
      return null;
    }

    ProjectItemType itemType = itemId.type;

    if (itemType == ProjectItemType.WORKSPACE) {
      return workspaceManager.loadRecord(itemId.workspaceId).toProjectItem();
    }

    if (itemType == ProjectItemType.PROJECT) {

      String workspaceId = itemId.workspaceId;
      String projectId   = itemId.projectId;

      return workspaceManager.getWsService(workspaceId).loadProjectRecord(projectId).toProjectItem(workspaceId);
    }

    if (itemType == ProjectItemType.DETAIL || itemType == ProjectItemType.FOLDER) {

      String workspaceId = itemId.workspaceId;
      String projectId   = itemId.projectId;
      String detailId    = itemId.id;

      return workspaceManager.getWsService(workspaceId)
                             .getProjectService(projectId)
                             .loadDetailItem(detailId)
                             .map(x -> x.toProjectItem(workspaceId, projectId))
                             .orElse(null);
    }

    return null;
  }
}
