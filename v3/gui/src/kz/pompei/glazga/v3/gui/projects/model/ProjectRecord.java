package kz.pompei.glazga.v3.gui.projects.model;

import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItem;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemCell;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ProjectItemId;

public class ProjectRecord {
  public String id;
  public String name;

  public ProjectItem toProjectItem(String workspaceId) {

    final ProjectItemId piId = ProjectItemId.project(workspaceId, id);
    final ProjectItem   ret  = new ProjectItem(piId.strId(), true);

    ret.cells.add(new ProjectItemCell("name", name, "name"));
    ret.nodeType = "project";

    return ret;
  }
}
