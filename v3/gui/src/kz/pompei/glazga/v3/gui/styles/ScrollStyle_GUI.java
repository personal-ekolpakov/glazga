package kz.pompei.glazga.v3.gui.styles;

import java.awt.Color;
import kz.pompei.swing.tree.styles.ScrollStyle;

public class ScrollStyle_GUI implements ScrollStyle {

  //@formatter:off
  @Override public int minSliderSize   () { return 20;  }
  @Override public int fatness         () { return 10;  }
  @Override public int deltaUserHeight () { return 300; }
  @Override public int deltaUserWidth  () { return 200; }
  @Override public int wheelSliderStep () { return 30;  }
  @Override public int underSliderStep () { return 120; }
  //@formatter:on

  //@formatter:off
  @Override public Color colorScroll      () { return new Color(0, 0, 0, 0);  }
  @Override public Color colorSlider      () { return new Color(0, 0, 0, 45); }
  @Override public Color colorScrollHover () { return new Color(0, 0, 0, 45); }
  @Override public Color colorSliderHover () { return new Color(0, 0, 0, 45); }
  //@formatter:on

}
