package kz.pompei.glazga.v3.gui.dialogs.common;

import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.KeyStroke;

public abstract class DialogLogic {

  protected boolean cancel = true;

  protected abstract JButton buttonCancel();

  protected abstract JButton buttonSave();

  protected abstract JDialog dialog();

  protected abstract JComponent mainPanel();

  protected void initDialogBasically() {

    dialog().addWindowListener(new WindowAdapter() {
      @Override public void windowClosed(WindowEvent e) {
        onCancel();
      }

      @Override public void windowClosing(WindowEvent e) {
        onCancel();
      }
    });

    JButton buttonCancel = buttonCancel();
    if (buttonCancel != null) {
      buttonCancel.addActionListener(e -> onCancel());
    }

    JButton buttonSave = buttonSave();
    if (buttonSave != null) {
      buttonSave.addActionListener(e -> onSave());
    }

    mainPanel().registerKeyboardAction(e -> onCancel(),
                                       KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                                       JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    mainPanel().registerKeyboardAction(e -> onSave(),
                                       KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, KeyEvent.CTRL_DOWN_MASK),
                                       JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
  }

  protected void onCancel() {
    cancel = true;
    dialog().dispose();
  }

  protected void onSave() {
    cancel = false;
    dialog().dispose();
  }

}
