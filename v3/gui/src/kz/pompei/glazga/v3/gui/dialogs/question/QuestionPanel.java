package kz.pompei.glazga.v3.gui.dialogs.question;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import kz.pompei.glazga.utils.events.VoidHandlerObserver;
import lombok.NonNull;

public class QuestionPanel extends JPanel {
  public final JLabel labelQuestion = new JLabel("Задаваемый вопрос");

  public String result;

  public VoidHandlerObserver buttonClicked = new VoidHandlerObserver();

  public void init(@NonNull List<QuestionVariant> questionVariants) {
    setLayout(new GridBagLayout());
    setBorder(new EmptyBorder(10, 10, 10, 10));

    {
      var c = new GridBagConstraints();

      c.weightx = 1;
      c.weighty = 1;
      c.gridy   = 0;

      c.gridx  = 0;
      c.fill   = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.LINE_START;
      add(labelQuestion, c);

      c.insets = new Insets(5, 0, 5, 5);

      for (final QuestionVariant questionVariant : questionVariants) {
        c.gridy++;

        String text = questionVariant.text;
        Icon   icon = questionVariant.icon;

        JButton button = icon == null ? new JButton(text) : new JButton(text, icon);
        button.addActionListener(e -> {
          result = questionVariant.result;
          buttonClicked.fire();
        });


        c.gridx  = 0;
        c.fill   = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_START;

        add(button, c);
      }
    }
  }
}
