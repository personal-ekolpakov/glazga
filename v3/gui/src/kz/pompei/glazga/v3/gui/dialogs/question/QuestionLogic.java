package kz.pompei.glazga.v3.gui.dialogs.question;

import java.util.List;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import kz.pompei.glazga.v3.gui.dialogs.common.DialogLogic;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class QuestionLogic extends DialogLogic {
  private final QuestionDialog dialog;

  public String show(@NonNull String question, @NonNull List<QuestionVariant> variants) {
    dialog.main.init(variants);
    dialog.main.labelQuestion.setText(question);

    dialog.pack();

    dialog.main.buttonClicked.add(() -> {
      cancel = false;
      dialog.dispose();
    });

    cancel = true;

    initDialogBasically();

    dialog.setVisible(true);

    if (cancel) {
      return null;
    }

    return dialog.main.result;
  }

  @Override protected JButton buttonCancel() {
    return null;
  }

  @Override protected JButton buttonSave() {
    return null;
  }

  @Override protected JDialog dialog() {
    return dialog;
  }

  @Override protected JComponent mainPanel() {
    return dialog.main;
  }
}
