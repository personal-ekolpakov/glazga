package kz.pompei.glazga.v3.gui.forms.project_tree.model;

import java.util.ArrayList;
import java.util.List;
import kz.pompei.swing.tree.logic.TreeItem;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProjectItem implements TreeItem {

  public final String  id;
  public final boolean isFolder;

  public String nodeType;

  public final List<ProjectItemCell> cells = new ArrayList<>();


  @Override public String id() {
    return id;
  }

  @Override public boolean isFolder() {
    return isFolder;
  }
}
