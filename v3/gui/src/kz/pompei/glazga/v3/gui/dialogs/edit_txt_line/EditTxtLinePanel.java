package kz.pompei.glazga.v3.gui.dialogs.edit_txt_line;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import kz.pompei.glazga.utils.JDialogUtil;

public class EditTxtLinePanel extends JPanel {

  public final JLabel     label = new JLabel("Изменяемый текст:");
  public final JTextField field = new JTextField();

  public final JPanel  dialogButtons = new JPanel();
  public final JButton buttonSave    = new JButton("Сохранить");
  public final JButton buttonCancel  = new JButton("Отменить");

  public EditTxtLinePanel() {
    setLayout(new GridBagLayout());
    setBorder(new EmptyBorder(10, 10, 10, 10));

    JDialogUtil.borders(field, 5, 5, 5, 5);

    {
      var c = new GridBagConstraints();

      c.weightx = 1;
      c.weighty = 1;
      c.gridy   = 0;

      c.gridx  = 0;
      c.fill   = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.LINE_END;
      add(label, c);

      c.gridx  = 1;
      c.fill   = GridBagConstraints.HORIZONTAL;
      c.anchor = GridBagConstraints.CENTER;
      add(field, c);

      c.gridy++;

      c.gridx     = 1;
      c.gridwidth = 3;
      c.fill      = GridBagConstraints.BOTH;
      add(dialogButtons, c);
    }

    dialogButtons.setLayout(new GridBagLayout());
    dialogButtons.setBorder(new EmptyBorder(10, 0, 0, 0));

    {
      var c = new GridBagConstraints();
      c.gridy   = 0;
      c.anchor  = GridBagConstraints.LINE_START;
      c.weightx = 1;

      c.gridx = 0;
      dialogButtons.add(buttonSave, c);

      c.gridx  = 1;
      c.insets = new Insets(0, 10, 0, 0);
      dialogButtons.add(buttonCancel, c);
    }
  }
}
