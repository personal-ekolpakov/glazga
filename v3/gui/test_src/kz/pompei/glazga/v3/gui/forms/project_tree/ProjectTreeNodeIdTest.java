package kz.pompei.glazga.v3.gui.forms.project_tree;

import kz.pompei.glazga.utils.RndUtil;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ProjectTreeNodeIdTest {

  @Test
  public void workspace__strId__parse() {

    final ProjectTreeNodeId ptnId = ProjectTreeNodeId.workspace(RndUtil.newId());

    assertThat(ptnId.type).isEqualTo(ProjectTreeNodeType.WORKSPACE);

    final String strId = ptnId.strId();

    final ProjectTreeNodeId actualPtnId = ProjectTreeNodeId.parse(strId).orElseThrow();

    assertThat(actualPtnId.type).isEqualTo(ProjectTreeNodeType.WORKSPACE);
    assertThat(actualPtnId.id).isEqualTo(ptnId.id);

  }

  @Test
  public void project__strId__parse() {

    final ProjectTreeNodeId ptnId = ProjectTreeNodeId.project(RndUtil.newId());

    assertThat(ptnId.type).isEqualTo(ProjectTreeNodeType.PROJECT);

    final String strId = ptnId.strId();

    final ProjectTreeNodeId actualPtnId = ProjectTreeNodeId.parse(strId).orElseThrow();

    assertThat(actualPtnId.type).isEqualTo(ProjectTreeNodeType.PROJECT);
    assertThat(actualPtnId.id).isEqualTo(ptnId.id);

  }
}