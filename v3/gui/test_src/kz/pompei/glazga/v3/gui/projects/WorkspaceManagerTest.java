package kz.pompei.glazga.v3.gui.projects;

import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ConnectParams;
import kz.pompei.glazga.v3.gui.projects.model.WorkspaceRecordPortion;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class WorkspaceManagerTest extends WorkspaceManagerTestParent {

  @Test
  public void addWorkspace__getWorkspaceRecordList() {

    workspaceManager.addWorkspace(ConnectParams.builder()
                                               .host("localhost")
                                               .port(17381)
                                               .username("pompei")
                                               .password("dMx1q3hS4QMiJbh08VfRc1uDamDQgZ")
                                               .dbName("stella")
                                               .build());

    final WorkspaceRecordPortion portion = workspaceManager.getWorkspaceRecordList(Page.with(0, 4));

    System.out.println("PV30bHysRM :: " + portion);

    assertThat(portion.list).hasSize(1);
    assertThat(portion.list.get(0).name).isEqualTo("PG:localhost:17381/stella");
    assertThat(portion.list.get(0).description).isEqualTo("PG:localhost:17381/stella");

  }
}
