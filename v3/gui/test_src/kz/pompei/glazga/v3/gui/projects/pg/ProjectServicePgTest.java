package kz.pompei.glazga.v3.gui.projects.pg;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.mat.Rect;
import kz.pompei.glazga.utils.Page;
import kz.pompei.glazga.utils.RndUtil;
import kz.pompei.glazga.v3.gui.projects.PgTestParent;
import kz.pompei.glazga.v3.gui.projects.ProjectService;
import kz.pompei.glazga.v3.gui.projects.model.DetailDirRecordPortion;
import kz.pompei.v3.storage.Storage;
import lombok.NonNull;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ProjectServicePgTest extends PgTestParent {

  @Override protected boolean newDbForEveryTest() {
    return false;
  }

  @Test
  public void getProjectName() {

    String         name           = RndUtil.strEng(10);
    String         projectId      = workspaceService.createProject(name);
    ProjectService projectService = workspaceService.getProjectService(projectId);

    String actualName = projectService.getProjectName();

    assertThat(actualName).isEqualTo(name);
  }

  @Test
  public void setDetailName__getDetailName() {
    String         projectId      = workspaceService.createProject(RndUtil.strEng(10));
    ProjectService projectService = workspaceService.getProjectService(projectId);

    String detailId = RndUtil.newId();
    String name     = RndUtil.strEng(10);

    //
    //
    projectService.setDetailName(detailId, name);
    //
    //

    //
    //
    String actualName = projectService.getDetailName(detailId);
    //
    //

    assertThat(actualName).isEqualTo(name);

  }

  @Test
  public void loadDetailPortion() {
    String         projectId      = workspaceService.createProject(RndUtil.strEng(10));
    ProjectService projectService = workspaceService.getProjectService(projectId);

    String d1 = RndUtil.newId();
    String d2 = RndUtil.newId();
    String d3 = RndUtil.newId();
    String d4 = RndUtil.newId();

    Storage s1 = projectService.getDetailStorage(d1);
    Storage s2 = projectService.getDetailStorage(d2);
    Storage s3 = projectService.getDetailStorage(d3);
    Storage s4 = projectService.getDetailStorage(d4);

    s1.set("name", "det001");
    s2.set("name", "det002");
    s3.set("name", "det003");
    s4.set("name", "det004");

    s1.set("path", "root/main/src");
    s2.set("path", "root/main/src");
    s3.set("path", "root/main/src");
    s4.set("path", "root/main/src");

    DetailDirRecordPortion topLevel = projectService.loadDetailPortion("", Page.with(0, 10));

    assertThat(topLevel.list).hasSize(1);
    assertThat(topLevel.list.get(0).id).isNotNull();
    assertThat(topLevel.list.get(0).name).isEqualTo("root");
    assertThat(topLevel.list.get(0).dir).isEqualTo(true);
    assertThat(topLevel.list.get(0).createdAt).isNotNull();
    assertThat(topLevel.list.get(0).modifiedAt).isNotNull();

    DetailDirRecordPortion level2 = projectService.loadDetailPortion(topLevel.list.get(0).id, Page.with(0, 10));

    assertThat(level2.list).hasSize(1);
    assertThat(level2.list.get(0).id).isNotNull();
    assertThat(level2.list.get(0).name).isEqualTo("main");
    assertThat(level2.list.get(0).dir).isEqualTo(true);
    assertThat(level2.list.get(0).createdAt).isNotNull();
    assertThat(level2.list.get(0).modifiedAt).isNotNull();

    DetailDirRecordPortion level3 = projectService.loadDetailPortion(level2.list.get(0).id, Page.with(0, 10));

    assertThat(level3.list).hasSize(1);
    assertThat(level3.list.get(0).id).isNotNull();
    assertThat(level3.list.get(0).name).isEqualTo("src");
    assertThat(level3.list.get(0).dir).isEqualTo(true);
    assertThat(level3.list.get(0).createdAt).isNotNull();
    assertThat(level3.list.get(0).modifiedAt).isNotNull();

    DetailDirRecordPortion level4 = projectService.loadDetailPortion(level3.list.get(0).id, Page.with(0, 10));

    assertThat(level4.list).hasSize(4);

    assertThat(level4.list.get(0).name).isEqualTo("det001");
    assertThat(level4.list.get(1).name).isEqualTo("det002");
    assertThat(level4.list.get(2).name).isEqualTo("det003");
    assertThat(level4.list.get(3).name).isEqualTo("det004");

    assertThat(level4.list.get(0).dir).isEqualTo(false);
    assertThat(level4.list.get(1).dir).isEqualTo(false);
    assertThat(level4.list.get(2).dir).isEqualTo(false);
    assertThat(level4.list.get(3).dir).isEqualTo(false);

    assertThat(level4.list.get(0).id).isEqualTo(d1);
    assertThat(level4.list.get(1).id).isEqualTo(d2);
    assertThat(level4.list.get(2).id).isEqualTo(d3);
    assertThat(level4.list.get(3).id).isEqualTo(d4);

    assertThat(level4.list.get(0).createdAt).isNotNull();
    assertThat(level4.list.get(1).createdAt).isNotNull();
    assertThat(level4.list.get(2).createdAt).isNotNull();
    assertThat(level4.list.get(3).createdAt).isNotNull();

    assertThat(level4.list.get(0).modifiedAt).isNotNull();
    assertThat(level4.list.get(1).modifiedAt).isNotNull();
    assertThat(level4.list.get(2).modifiedAt).isNotNull();
    assertThat(level4.list.get(3).modifiedAt).isNotNull();

  }

  @Test
  public void deleteDetail() {
    String         projectId      = workspaceService.createProject(RndUtil.strEng(10));
    ProjectService projectService = workspaceService.getProjectService(projectId);

    String detailId1 = RndUtil.newId();
    String detailId2 = RndUtil.newId();

    projectService.setDetailName(detailId1, "Hello1");
    projectService.setDetailPath(detailId1, "root/main/src/parent01");

    projectService.setDetailName(detailId2, "Hello2");
    projectService.setDetailPath(detailId2, "root/main/src/parent02");

    Storage s1 = projectService.getDetailStorage(detailId1);
    Storage s2 = projectService.getDetailStorage(detailId2);

    s1.set("boxes.VnMts2k1ib.area", "123,345,10,10");
    s2.set("boxes.VnMts2k1ib.area", "321,321,10,10");
    s1.set("boxes.X7aOJnzNVi.area", "1.23,34.5,10,10");
    s2.set("boxes.X7aOJnzNVi.area", "32.1,3.21,10,10");

    assertThat(detail_dir_name_count(projectService, "parent01")).isEqualTo(1);
    assertThat(detail_dir_name_count(projectService, "parent02")).isEqualTo(1);

    assertThat(detail_dir_name_count(projectService, "root")).isEqualTo(1);
    assertThat(detail_dir_name_count(projectService, "main")).isEqualTo(1);
    assertThat(detail_dir_name_count(projectService, "src")).isEqualTo(1);

    assertThat(detail_area_count(projectService, detailId1)).isEqualTo(2);
    assertThat(detail_area_count(projectService, detailId2)).isEqualTo(2);

    //
    //
    projectService.deleteDetail(detailId1);
    //
    //

    assertThat(detail_dir_name_count(projectService, "parent01")).isEqualTo(0);
    assertThat(detail_dir_name_count(projectService, "parent02")).isEqualTo(1);

    assertThat(detail_dir_name_count(projectService, "root")).isEqualTo(1);
    assertThat(detail_dir_name_count(projectService, "main")).isEqualTo(1);
    assertThat(detail_dir_name_count(projectService, "src")).isEqualTo(1);

    assertThat(detail_area_count(projectService, detailId1)).isEqualTo(0);
    assertThat(detail_area_count(projectService, detailId2)).isEqualTo(2);

    //
    //
    projectService.deleteDetail(detailId2);
    //
    //

    assertThat(detail_dir_name_count(projectService, "parent01")).isEqualTo(0);
    assertThat(detail_dir_name_count(projectService, "parent02")).isEqualTo(0);

    assertThat(detail_dir_name_count(projectService, "root")).isEqualTo(0);
    assertThat(detail_dir_name_count(projectService, "main")).isEqualTo(0);
    assertThat(detail_dir_name_count(projectService, "src")).isEqualTo(0);

    assertThat(detail_area_count(projectService, detailId1)).isEqualTo(0);
    assertThat(detail_area_count(projectService, detailId2)).isEqualTo(0);

  }

  @Test
  public void findBoxIdsInRect() {

    String         projectId      = workspaceService.createProject(RndUtil.strEng(10));
    ProjectService projectService = workspaceService.getProjectService(projectId);

    String detailId1 = RndUtil.newId();
    String detailId2 = RndUtil.newId();

    Storage s1 = projectService.getDetailStorage(detailId1);
    Storage s2 = projectService.getDetailStorage(detailId2);

    s1.set("boxes.a1.area", "2,2,2,2");
    s1.set("boxes.a2.area", "9,3,2,2");
    s1.set("boxes.a3.area", "5,4,3,3");
    s1.set("boxes.a4.area", "9,6,1,3");
    s1.set("boxes.a5.area", "5,8,1,1");
    s1.set("boxes.a6.area", "11,8,2,3");
    s1.set("boxes.a7.area", "5,11,2,2");
    s1.set("boxes.a8.area", "9,12,3,1");
    s1.set("boxes.a9.area", "5,14,1,2");

    s2.set("boxes.b1.area", "2,2,2,2");
    s2.set("boxes.b2.area", "9,3,2,2");
    s2.set("boxes.b3.area", "5,4,3,3");
    s2.set("boxes.b4.area", "9,6,1,3");
    s2.set("boxes.b5.area", "5,8,1,1");

    //
    //
    Set<String> boxIds = projectService.findBoxIdsInRect(detailId1, Rect.of(3, 3, 12, 10));
    //
    //

    assertThat(boxIds).isEqualTo(Set.of("a1", "a2", "a3", "a4", "a5", "a6"));

  }

  private static void putTwice(@NonNull Storage storage, @NonNull Map<String, String> expectedMap,
                               @NonNull String key, @NonNull String value) {
    storage.set(key, value);
    expectedMap.put(key, value);
  }

  @Test
  public void getOneLevelMap() {

    String         projectId      = workspaceService.createProject(RndUtil.strEng(10));
    ProjectService projectService = workspaceService.getProjectService(projectId);

    String detailId1 = RndUtil.newId();
    String detailId2 = RndUtil.newId();

    Storage s1 = projectService.getDetailStorage(detailId1);
    Storage s2 = projectService.getDetailStorage(detailId2);

    Map<String, String> expectedMap = new HashMap<>();

    // s1

    putTwice(s1, expectedMap, "boxes.a1.area", "2,2,2,2");
    putTwice(s1, expectedMap, "boxes.a1.type", "Test1");
    putTwice(s1, expectedMap, "boxes.a1.valueId", "KdhW1eUtO7");
    putTwice(s1, expectedMap, "boxes.a2.area", "2.1,2.1,2.1,2.1");
    putTwice(s1, expectedMap, "boxes.a2.type", "Test2");
    putTwice(s1, expectedMap, "boxes.a2.valueId", "tzUgJB3ebY");

    s1.set("boxes.a3.area", "3.1,3.1,3.1,3.1");
    s1.set("boxes.a3.type", "Test3");
    s1.set("boxes.a3.valueId", "tTyCN1PXzq");

    // s2

    s2.set("boxes.a1.area", "23,23,23,23");
    s2.set("boxes.a1.type", "Test11");
    s2.set("boxes.a1.valueId", "ck11es1wFB");
    s2.set("boxes.a2.area", "23.1,23.1,23.1,23.1");
    s2.set("boxes.a2.type", "Test21");
    s2.set("boxes.a2.valueId", "rfV5p0UYwk");
    s2.set("boxes.a3.area", "33.1,33.1,33.1,33.1");
    s2.set("boxes.a3.type", "Test31");
    s2.set("boxes.a3.valueId", "gklM7WoJCl");
    s2.set("boxes.b3.area", "44.1,44.1,44.1,44.1");
    s2.set("boxes.b3.type", "Test41");
    s2.set("boxes.b3.valueId", "gklM74oJCl");

    //
    //
    Map<String, String> oneLevelMap = s1.loadOneLevelMap(Set.of("boxes.a1.", "boxes.a2.", "boxes.b3."));
    //
    //

    for (final Map.Entry<String, String> e : oneLevelMap.entrySet()) {
      System.out.println("pjdL4MW8ZE :: " + e);
    }

    assertThat(oneLevelMap).isEqualTo(expectedMap);

  }
}
