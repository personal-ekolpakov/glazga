package kz.pompei.glazga.v3.gui.projects;

import java.util.Map;
import java.util.Set;
import kz.pompei.glazga.utils.RndUtil;
import kz.pompei.glazga.utils.db.DbQuery;
import kz.pompei.glazga.utils.err.SqlNoRelation;
import kz.pompei.glazga.v3.gui.projects.model.ProjectRecord;
import kz.pompei.glazga.v3.gui.projects.model.ProjectRecordPortion;
import kz.pompei.v3.storage.Storage;
import org.testng.annotations.Test;

import static java.util.stream.Collectors.toMap;
import static org.assertj.core.api.Assertions.assertThat;

public class WorkspaceServicePgTest2 extends PgTestParent {

  @Override protected boolean newDbForEveryTest() {
    return false;
  }

  @Test
  public void createProject() {

    String name = RndUtil.strEng(25);

    //
    //
    String projectId = workspaceService.createProject(name);
    //
    //

    assertThat(projectId).isNotNull();

    DbQuery params = workspaceDb.query()
                                .sql("select value from {.projectId}.{.glazga_project_params} where id = {param_id}")
                                .param("glazga_project_params", "glazga_project_params")
                                .param("projectId", projectId);

    String actualName = params.copy()
                              .param("param_id", "project_name")
                              .str();

    assertThat(actualName).isEqualTo(name);

    String structureVersion = params.copy()
                                    .param("param_id", "db_structure_version")
                                    .str();

    assertThat(structureVersion).isEqualTo("0.1");
  }

  @Test(expectedExceptions = SqlNoRelation.class)
  public void deleteProject() {

    String projectId = workspaceService.createProject(RndUtil.strEng(10));


    //
    //
    workspaceService.deleteProject(projectId);
    //
    //

    workspaceDb.query()
               .sql("select value from {.my}.glazga_project_params where id = {param_id}")
               .param("my", projectId)
               .param("param_id", "db_structure_version")
               .str();
  }

  @Test
  public void loadProjectPortion() {

    String name1 = "hello 001 " + RndUtil.strEng(10);
    String name2 = "hello 001 " + RndUtil.strEng(10);
    String name3 = "hello 001 " + RndUtil.strEng(10);
    String name4 = "hello 001 " + RndUtil.strEng(10);
    String name5 = "hello 001 " + RndUtil.strEng(10);

    String id1 = workspaceService.createProject(name1);
    String id2 = workspaceService.createProject(name2);
    String id3 = workspaceService.createProject(name3);
    String id4 = workspaceService.createProject(name4);
    String id5 = workspaceService.createProject(name5);

    //
    //
    ProjectRecordPortion portion = workspaceService.loadProjectPortion(null);
    //
    //

    assertThat(portion).isNotNull();
    Map<String, ProjectRecord> map = portion.list.stream().collect(toMap(x -> x.id, x -> x));

    assertThat(map).containsKeys(id1, id2, id3, id4, id5);
    assertThat(map.get(id1).name).isEqualTo(name1);
    assertThat(map.get(id2).name).isEqualTo(name2);
    assertThat(map.get(id3).name).isEqualTo(name3);
    assertThat(map.get(id4).name).isEqualTo(name4);
    assertThat(map.get(id5).name).isEqualTo(name5);

  }

  @Test
  public void storage_set_get() {

    String         projectId      = workspaceService.createProject(RndUtil.strEng(17));
    String         detailId       = RndUtil.newId();
    ProjectService projectService = workspaceService.getProjectService(projectId);
    Storage        storage        = projectService.getDetailStorage(detailId);

    String value1 = RndUtil.strEng(20);
    String value2 = RndUtil.strEng(20);
    String value3 = RndUtil.strEng(20);

    storage.set("some.path.to.value1", value1);
    storage.set("some.path.to.value2", value2);
    storage.set("some.path.to.value3", value3);

    String actual1 = storage.get("some.path.to.value1").orElseThrow();
    String actual2 = storage.get("some.path.to.value2").orElseThrow();
    String actual3 = storage.get("some.path.to.value3").orElseThrow();

    assertThat(actual1).isEqualTo(value1);
    assertThat(actual2).isEqualTo(value2);
    assertThat(actual3).isEqualTo(value3);
  }

  @Test
  public void findKeys() {
    String         projectId      = workspaceService.createProject(RndUtil.strEng(17));
    String         detailId       = RndUtil.newId();
    String         detailId2      = RndUtil.newId();
    ProjectService projectService = workspaceService.getProjectService(projectId);
    Storage        storage        = projectService.getDetailStorage(detailId);
    Storage        storage2       = projectService.getDetailStorage(detailId2);

    storage.set("status.lock.stone", RndUtil.newId());
    storage.set("status.lock.look", RndUtil.newId());
    storage.set("status.lock.feel", RndUtil.newId());

    storage.set("status.path.val1", RndUtil.newId());
    storage.set("status.path.val2", RndUtil.newId());
    storage.set("left.sinus.val3", RndUtil.newId());

    storage2.set("status.lock.stone1", RndUtil.newId());
    storage2.set("status.lock.look1", RndUtil.newId());
    storage2.set("status.lock.feel1", RndUtil.newId());

    //
    //
    Set<String> keys = storage.findKeys("status.lock");
    //
    //

    assertThat(keys).isEqualTo(Set.of("status.lock.stone", "status.lock.look", "status.lock.feel"));
  }
}
