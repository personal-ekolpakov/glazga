package kz.pompei.glazga.v3.gui.projects;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import kz.pompei.v3.storage.StoragePath;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public abstract class WorkspaceManagerTestParent {
  Path             root;
  WorkspaceManager workspaceManager;

  @BeforeMethod
  public void prepareWorkspaceManager(Method testMethod) {
    if (root == null) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
      root = Paths.get("build").resolve(getClass().getSimpleName()).resolve(sdf.format(new Date()));
    }

    final StoragePath storage = new StoragePath(root.resolve(testMethod.getName()));
    workspaceManager = new WorkspaceManager(storage);
  }

  @AfterMethod
  public void closeWorkspaceManager() {
    workspaceManager.close();
  }

}
