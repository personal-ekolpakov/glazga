package kz.pompei.glazga.v3.gui.forms.editor.model;

import kz.greetgo.util.RND;
import kz.pompei.glazga.v3.types.script.Area;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AreaTest {

  @Test
  public void twiceConvert() {

    Area area = new Area();
    area.left   = RND.plusDouble(1_000_000_000_000d, 7);
    area.top    = RND.plusDouble(1_000_000_000_000d, 7);
    area.width  = RND.plusDouble(1_000_000_000_000d, 7);
    area.height = RND.plusDouble(1_000_000_000_000d, 7);

    //
    //
    String str = area.strValue();
    //
    //

    Area actual = Area.parse(str);

    assertThat(actual).isNotNull();
    assertThat(actual.left).isEqualTo(area.left);
    assertThat(actual.top).isEqualTo(area.top);
    assertThat(actual.width).isEqualTo(area.width);
    assertThat(actual.height).isEqualTo(area.height);

  }
}
