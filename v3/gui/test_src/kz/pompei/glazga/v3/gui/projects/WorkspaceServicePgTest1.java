package kz.pompei.glazga.v3.gui.projects;

import kz.pompei.glazga.utils.RndUtil;
import org.testng.annotations.Test;

import static kz.pompei.glazga.v3.gui.projects.pg.WorkspaceServicePg.initialWorkspaceName;
import static org.assertj.core.api.Assertions.assertThat;

public class WorkspaceServicePgTest1 extends PgTestParent {

  @Override protected boolean newDbForEveryTest() {
    return true;
  }

  @Test
  public void typeDisplayStr() {
    //
    //
    String typeDisplayStr = workspaceService.typeDisplayStr();
    //
    //

    assertThat(typeDisplayStr).isEqualTo("PostgreSQL");
  }

  @Test
  public void initialize() {

    workspaceService.initialize();


    String workspaceName = workspaceDb.query()
                                      .sql("select value from public.glazga_workspace_params where id = 'workspace_name'")
                                      .str();

    System.out.println("SvtWTZUmQR :: workspaceName: " + workspaceName);

    assertThat(workspaceName).startsWith("PG:localhost:17382/tst_");
  }

  @Test
  public void workspaceName() {

    String workspaceName = initialWorkspaceName(workspaceConnectParams);

    //
    //
    String actualWorkspaceName = workspaceService.workspaceName();
    //
    //

    System.out.println("mMkzP6Mq6z :: actualWorkspaceName: " + actualWorkspaceName);

    assertThat(actualWorkspaceName).isEqualTo(workspaceName);
  }

  @Test
  public void setWorkspaceName() {

    String workspaceName = RndUtil.newId();

    //
    //
    workspaceService.setWorkspaceName(workspaceName);
    //
    //

    String dbWorkspaceName = workspaceDb.query()
                                        .sql("select value from public.glazga_workspace_params where id = 'workspace_name'")
                                        .str();

    assertThat(dbWorkspaceName).isEqualTo(workspaceName);
  }

}
