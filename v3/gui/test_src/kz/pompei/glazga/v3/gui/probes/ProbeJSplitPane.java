package kz.pompei.glazga.v3.gui.probes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.nio.file.Paths;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import kz.pompei.glazga.utils.SizeLocationSaver;
import kz.pompei.glazga.utils.events.VoidHandler;
import kz.pompei.glazga.v3.gui.util.Sizes;
import lombok.NonNull;
import lombok.SneakyThrows;

public class ProbeJSplitPane {
  public static void main(String[] args) {
    new ProbeJSplitPane().exec();
  }

  private void exec() {
    JFrame frame = new JFrame();
    frame.setTitle("Проба - " + getClass().getSimpleName());
    frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    Sizes.setSizeAndLocation(frame, 0.6f);
    SizeLocationSaver.into(Paths.get("build").resolve(getClass().getSimpleName()).resolve("sizes_and_locations"))
                     .overJFrame("main-form", frame, true);

    initFrame(frame);

    SwingUtilities.invokeLater(() -> frame.setVisible(true));
  }

  private static JButton newButton(@NonNull String title, @NonNull VoidHandler handler) {
    JButton ret = new JButton(title);
    ret.addMouseListener(new MouseAdapter() {
      @Override
      @SneakyThrows
      public void mouseClicked(MouseEvent e) {
        handler.handle();
      }
    });
    return ret;
  }

  private void initFrame(JFrame frame) {
    JSplitPane body = new JSplitPane();

    JPanel top = new JPanel();
    top.setLayout(new GridLayout(1, 3));
    top.add(newButton("init-split", () -> {

      JPanel panel1 = new JPanel();
      JPanel panel2 = new JPanel();

      panel1.setBackground(new Color(50, 182, 45));
      panel2.setBackground(new Color(33, 91, 166));

      body.setTopComponent(panel1);
      body.setBottomComponent(panel2);

      body.setDividerLocation(0.5);

    }));

    frame.getContentPane().setLayout(new BorderLayout());
    frame.getContentPane().add(top, BorderLayout.NORTH);
    frame.getContentPane().add(body, BorderLayout.CENTER);

  }
}
