package kz.pompei.glazga.v3.gui.context.model;

import kz.pompei.glazga.utils.RndUtil;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DetailIdTest {

  @Test
  public void strValue__parse() {

    DetailId id1 = new DetailId(RndUtil.newId(), RndUtil.newId(), RndUtil.newId());

    String str = id1.strValue();
    System.out.println("5bhYiaV32k :: str = " + str);

    DetailId id2 = DetailId.parse(str).orElseThrow();

    assertThat(id2).isEqualTo(id1);
    assertThat(id2.workspaceId).isEqualTo(id1.workspaceId);
    assertThat(id2.projectId).isEqualTo(id1.projectId);
    assertThat(id2.detailId).isEqualTo(id1.detailId);

  }
}
