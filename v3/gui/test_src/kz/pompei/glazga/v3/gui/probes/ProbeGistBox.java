package kz.pompei.glazga.v3.gui.probes;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.util.concurrent.atomic.AtomicBoolean;
import kz.greetgo.util.RND;
import kz.greetgo.util.fui.FUI;
import kz.greetgo.util.fui.IntAccessor;
import kz.pompei.glazga.utils.RndUtil;
import org.testng.annotations.Test;

public class ProbeGistBox extends ProbeDatabaseParent {

  @Test(enabled = false)
  public void createTable() {

    db.query().sql("""
                     create table box (
                        id varchar(20) primary key,
                        name varchar(500),
                        rect BOX
                     )
                     """).update();

    db.query().sql("""
                     create index on box using GIST (rect)
                     """).update();

  }

  @Test(enabled = false)
  public void insertManyData() {

    final double FIELD_WIDTH   = 1_000_000, FIELD_HEIGHT = 1_000_000;
    final double BOX_MAX_WIDTH = 10, BOX_MAX_HEIGHT = 10;
    final double BOX_MIN_WIDTH = 1, BOX_MIN_HEIGHT = 1;

    Path appDir = Paths.get("build").resolve(getClass().getSimpleName());

    FUI fui = new FUI(appDir);

    AtomicBoolean insertWork       = new AtomicBoolean(true);
    AtomicBoolean showCurrentState = new AtomicBoolean(false);

    IntAccessor maxBatchSizeEntry = fui.entryInt("batch-size", 3000);
    IntAccessor sleep             = fui.entryInt("sleep", 2400);

    fui.button("insertion-stop", () -> insertWork.set(false));

    fui.button("insertion-run", () -> {
      insertWork.set(true);

      new Thread(() -> {

        while (insertWork.get()) {
          try {
            Thread.sleep(sleep.get());
          } catch (InterruptedException e) {
            return;
          }

          showCurrentState.set(true);
        }

      }).start();

      new Thread(() -> db.withConnectionVoid(connection -> {

        connection.setAutoCommit(false);

        try (PreparedStatement ps = connection.prepareStatement(
          """
            insert into box (id, name, rect) values (?, ?, BOX(point(?, ?), point(?, ?)))
            """)) {

          int insertedRecords = 0;
          int batchSize       = 0;

          while (insertWork.get()) {

            double x = RND.rnd.nextDouble() * FIELD_WIDTH;
            double y = RND.rnd.nextDouble() * FIELD_HEIGHT;
            double w = RND.rnd.nextDouble() * (BOX_MAX_WIDTH - BOX_MIN_WIDTH) + BOX_MIN_WIDTH;
            double h = RND.rnd.nextDouble() * (BOX_MAX_HEIGHT - BOX_MIN_HEIGHT) + BOX_MIN_HEIGHT;

            ps.setString(1, RndUtil.newId());
            ps.setString(2, "Name " + RND.str(10));
            ps.setDouble(3, x);
            ps.setDouble(4, y);
            ps.setDouble(5, x + w);
            ps.setDouble(6, y + h);
            ps.addBatch();
            batchSize++;
            insertedRecords++;

            if (batchSize >= maxBatchSizeEntry.get()) {
              ps.executeBatch();
              connection.commit();
              batchSize = 0;
            }

            if (showCurrentState.get()) {
              showCurrentState.set(false);

              System.out.println("pgcUUFnfHs :: insertedRecords = " + insertedRecords);
            }

          }

          if (batchSize > 0) {
            ps.executeBatch();
            connection.commit();
          }

          connection.setAutoCommit(true);

          System.out.println("q9T327oj19 :: TOTAL insertedRecords = " + insertedRecords);


        }

      })).start();


    });

    System.out.println("ESLFolCszY :: Application has been run");
    fui.go();

    insertWork.set(false);

  }

}
