package kz.pompei.glazga.v3.gui.probes;

import java.sql.PreparedStatement;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import kz.pompei.glazga.utils.RndUtil;
import kz.pompei.glazga.utils.db.Db;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ConnectParams;
import kz.pompei.glazga.v3.gui.util.ConnectionCreatorDriver;
import org.testng.annotations.Test;

public class DetailProbes {

  protected final ConnectParams connectParams = ConnectParams.builder()
                                                             .host("localhost")
                                                             .port(17381)
                                                             .username("john")
                                                             .password("QMt3wmA5L186PdMkEK3IVjQMzS8pqQ")
                                                             .dbName("cloud")
                                                             .build();

  protected final Db db = new Db(new ConnectionCreatorDriver(() -> connectParams));

  @Test
  public void createTable_detail() {

    db.query().sql("""
                     create table detail (
                      detail_id   varchar(20)   not null,
                      path        varchar(300)  not null,
                      value       varchar(1000) not null,
                      primary key(detail_id, path)
                     )
                     """).update();

    db.query().sql("""
                     create table detail_pos (
                          detail_id varchar(20)           not null,
                          block_id  varchar(30)           not null,
                          pos       GEOMETRY(Point, 3857) not null,
                          primary key (detail_id, block_id)
                     )
                     """).update();

    db.query().sql("""
                     CREATE OR REPLACE FUNCTION text_to_Point3857(pos_txt TEXT)
                       RETURNS GEOMETRY(Point, 3857) AS
                     $$
                     DECLARE
                       coordinates TEXT[];
                       x           FLOAT;
                       y           FLOAT;
                     BEGIN
                       coordinates := string_to_array(pos_txt, ',');

                       IF array_length(coordinates, 1) < 2 THEN
                         RETURN NULL;
                       END IF;

                       BEGIN
                         x := coordinates[1]::FLOAT;
                         y := coordinates[2]::FLOAT;
                       EXCEPTION
                         WHEN others THEN RETURN NULL;
                       END;

                       RETURN ST_SetSrId(ST_MakePoint(x, y), 3857);
                     END;
                     $$ LANGUAGE PlPgSQL
                     """).update();

    db.query().sql("""
                     CREATE OR REPLACE FUNCTION detail_pos_trigger_function()
                       RETURNS TRIGGER AS
                     $$
                     declare
                       PTN_BLOCKS constant text := 'blocks\\.([\\w_$]+)\\.pos';
                       new_detail_id       text;
                       new_block_id        text;
                       new_pos             geometry(Point, 3857);
                       do_update           bool;
                     BEGIN
                       do_update := false;

                       IF (TG_OP = 'INSERT') THEN
                         if new.path ~ PTN_BLOCKS then
                           SELECT (regexp_matches(new.path, PTN_BLOCKS))[1] INTO new_block_id;
                           new_pos       := text_to_Point3857(new.value);
                           new_detail_id := new.detail_id;
                           do_update     := true;
                         end if;
                       ELSIF (TG_OP = 'UPDATE') THEN
                         if new.path ~ PTN_BLOCKS then
                           SELECT (regexp_matches(new.path, PTN_BLOCKS))[1] INTO new_block_id;
                           new_pos       := text_to_Point3857(new.value);
                           new_detail_id := new.detail_id;
                           do_update     := true;
                         end if;
                         ELSIF (TG_OP = 'DELETE') THEN
                           if old.path ~ PTN_BLOCKS then
                             SELECT (regexp_matches(old.path, PTN_BLOCKS))[1] INTO new_block_id;
                             new_pos       := null;
                             new_detail_id := old.detail_id;
                             do_update     := true;
                           end if;
                         END IF;

                         if do_update then
                           if new_pos is null then
                             delete
                               from detail_pos x
                               where x.detail_id = new_detail_id
                                 and x.block_id  = new_block_id;
                           else
                             insert into detail_pos (detail_id, block_id, pos)
                               values (new_detail_id, new_block_id, new_pos)
                               on conflict (detail_id, block_id)
                               do update set pos = new_pos;
                           end if;
                         end if;

                       RETURN NEW;
                     END;
                     $$ LANGUAGE PlPgSQL
                     """).update();

    db.query().sql("""
                     CREATE TRIGGER detail__to__detail_pos
                         AFTER INSERT OR UPDATE OR DELETE
                         ON detail
                         FOR EACH ROW
                     EXECUTE FUNCTION detail_pos_trigger_function();
                     """).update();
  }

  @Test
  public void insertManyData() {
    int detailCount = 10;
    int blockCount  = 10;

    db.withConnectionVoid(connection -> {

      connection.setAutoCommit(false);
      final int maxBatchSize = 1000;

      final AtomicInteger insertCount = new AtomicInteger(0);
      final AtomicBoolean working     = new AtomicBoolean(true);

      Thread lookingThread = new Thread(() -> {

        int        prev       = 0;
        final long start_time = System.currentTimeMillis();
        long       time       = start_time;

        while (working.get()) {

          try {
            Thread.sleep(1000);
          } catch (InterruptedException e) {
            break;
          }

          long time2          = System.currentTimeMillis();
          int  current        = insertCount.get();
          int  delta          = current - prev;
          long deltaTime      = time2 - time;
          long deltaTimeTotal = time2 - start_time;

          prev = current;
          time = time2;

          double velocity      = deltaTime == 0 ? 0 : (double) delta / (double) deltaTime;
          double velocityTotal = deltaTimeTotal == 0 ? 0 : (double) current / (double) deltaTimeTotal;

          System.out.println("q5DTba799b :: inserted " + current + " records, current velocity " + velocity + " rec/sec,"
                             + " total velocity " + velocityTotal + " rec/sec");

        }

        int    current        = insertCount.get();
        long   deltaTimeTotal = System.currentTimeMillis() - start_time;
        double velocityTotal  = deltaTimeTotal == 0 ? 0 : (double) current / (double) deltaTimeTotal;

        System.out.println("3TjbFoThpY :: FINISHED inserted " + current + " records, total velocity " + velocityTotal + " rec/sec");

      });
      lookingThread.start();

      try (PreparedStatement ps = connection.prepareStatement("insert into detail (detail_id, path, value) values (?, ?, ?)")) {
        int batchSize = 0;
        for (int d = 0; d < detailCount && working.get(); d++) {
          String detailId = RndUtil.newId();
          for (int b = 0; b < blockCount && working.get(); b++) {

            String blockId = RndUtil.newId();
            double x       = RndUtil.RND.nextDouble() * 200 - 100;
            double y       = RndUtil.RND.nextDouble() * 200 - 100;
            int    i       = 0;

            {
              ps.setString(1, detailId);
              ps.setString(2, "blocks." + blockId + ".pos");
              ps.setString(3, "" + x + "," + y);
              ps.addBatch();
              i++;
            }
            {
              ps.setString(1, detailId);
              ps.setString(2, "blocks." + blockId + ".type");
              ps.setString(3, "FOREACH");
              ps.addBatch();
              i++;
            }
            {
              ps.setString(1, detailId);
              ps.setString(2, "blocks." + blockId + ".exprId");
              ps.setString(3, RndUtil.strEng(10));
              ps.addBatch();
              i++;
            }
            {
              ps.setString(1, detailId);
              ps.setString(2, "blocks." + blockId + ".rightExprId");
              ps.setString(3, RndUtil.strEng(10));
              ps.addBatch();
              i++;
            }
            {
              ps.setString(1, detailId);
              ps.setString(2, "blocks." + blockId + ".status");
              ps.setString(3, "true");
              ps.addBatch();
              i++;
            }

            String exprId = RndUtil.newId();
            {
              ps.setString(1, detailId);
              ps.setString(2, "expressions." + exprId + ".type");
              ps.setString(3, "ValueConst");
              ps.addBatch();
              i++;
            }
            {
              ps.setString(1, detailId);
              ps.setString(2, "expressions." + exprId + ".leftExprId");
              ps.setString(3, RndUtil.newId());
              ps.addBatch();
              i++;
            }
            {
              ps.setString(1, detailId);
              ps.setString(2, "expressions." + exprId + ".rightExprId");
              ps.setString(3, RndUtil.newId());
              ps.addBatch();
              i++;
            }
            {
              ps.setString(1, detailId);
              ps.setString(2, "expressions." + exprId + ".status");
              ps.setString(3, "STONE");
              ps.addBatch();
              i++;
            }
            {
              ps.setString(1, detailId);
              ps.setString(2, "expressions." + exprId + ".kind");
              ps.setString(3, "FOUND");
              ps.addBatch();
              i++;
            }


            batchSize += i;
            insertCount.addAndGet(i);

            if (batchSize >= maxBatchSize) {
              ps.executeBatch();
              connection.commit();
              batchSize = 0;
            }

          }
        }
        if (batchSize > 0) {
          ps.executeBatch();
          connection.commit();
        }
        working.set(false);
        lookingThread.join();
      }
    });

    System.out.println("Zra1q1hsQF :: FINISHED");

  }
}
