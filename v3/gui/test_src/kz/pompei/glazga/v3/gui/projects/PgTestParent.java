package kz.pompei.glazga.v3.gui.projects;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import kz.pompei.glazga.utils.RndUtil;
import kz.pompei.glazga.utils.db.Db;
import kz.pompei.glazga.utils.db.UsingQuerySource;
import kz.pompei.glazga.utils.err.ErrFilter;
import kz.pompei.glazga.utils.err.SqlErrorUtil;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ConnectParams;
import kz.pompei.glazga.v3.gui.projects.pg.WorkspaceServicePg;
import kz.pompei.glazga.v3.gui.util.ConnectionCreatorDriver;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static kz.pompei.glazga.utils.ReflectUtil.getField;

public abstract class PgTestParent {

  private static final String postgresPassword = "koxCJiVxLCYqNWsqRQidWRHAdg8gUA";

  protected final ConnectParams adminConnectParams = ConnectParams.builder()
                                                                  .host("localhost")
                                                                  .port(17382)
                                                                  .username("test_user")
                                                                  .password("j1eBouLkl9QN2O1AX04RqmXQy5P1pL")
                                                                  .dbName("test_db")
                                                                  .build();

  protected ConnectParams      workspaceConnectParams;
  protected WorkspaceServicePg workspaceService;

  protected final Db adminDb     = new Db(new ConnectionCreatorDriver(() -> adminConnectParams));
  protected final Db workspaceDb = new Db(new ConnectionCreatorDriver(() -> workspaceConnectParams));

  protected abstract boolean newDbForEveryTest();

  private boolean dbAlreadyCreated = false;

  @BeforeMethod @SneakyThrows
  public void prepareWorkspaceService() {
    prepareDb();
    initializeWorkspaceService();
    System.out.println("FfD5Q0e1na :: workspaceConnectParams " + workspaceConnectParams);
  }

  private void prepareDb() {
    if (newDbForEveryTest()) {
      createWorkspaceConnectParams_random();
      createDb();
      return;
    }

    if (dbAlreadyCreated) {
      return;
    }

    for (int num = 1; num < 100; num++) {
      createWorkspaceConnectParams_Test(num);
      try {
        checkDbConnection();
        break;
      } catch (Exception e) {
        String check = ErrFilter.of(e)
                                .ignorePasswordFailed(true, null)
                                .ignorePermissionDenied(true, "pd")
                                .ignoreNoType(true, null)
                                .ignoreNoDb(true, "no-db")
                                .check("yPVsJz8v7i");

        if ("pd".equals(check)) {
          continue;
        }
      }

      try {
        createDb();
      } catch (Exception e) {
        ErrFilter.of(e)
                 .ignoreUserAlreadyExists(true, null)
                 .ignoreNoType(true, null)
                 .check("j3DmQnM9hY");
        continue;
      }
      break;
    }


    dbAlreadyCreated = true;
  }

  private void checkDbConnection() {
    workspaceDb.query().sql("create table test_name_k0wd1efr8a(id int, pos GEOMETRY(Point, 3857))").update();
    workspaceDb.query().sql("drop table test_name_k0wd1efr8a").update();
  }

  private void createWorkspaceConnectParams_Test(int num) {
    workspaceConnectParams = ConnectParams.builder()
                                          .host(adminConnectParams.host)
                                          .port(adminConnectParams.port)
                                          .username("test_user_" + num)
                                          .password("UP1XC4Mnq2ZGWa1h983N")
                                          .dbName("test_db_" + num)
                                          .build();
  }

  private void createWorkspaceConnectParams_random() {
    SimpleDateFormat sdf      = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
    String           dbName   = "tst_" + sdf.format(new Date());
    String           userName = "tst_user_" + sdf.format(new Date());
    String           password = RndUtil.strEng(23);

    workspaceConnectParams = ConnectParams.builder()
                                          .host(adminConnectParams.host)
                                          .port(adminConnectParams.port)
                                          .username(userName)
                                          .password(password)
                                          .dbName(dbName)
                                          .build();
  }

  private void createDb() {

    adminDb.execAutoCommiting((UsingQuerySource<Void>) qs -> {

      String username      = workspaceConnectParams.username;
      String password      = workspaceConnectParams.password;
      String dbName        = workspaceConnectParams.dbName;
      String adminUsername = adminConnectParams.username;

      qs.query().sql("create user " + username + " WITH ENCRYPTED PASSWORD '" + password + "'").update();
      qs.query().sql("grant " + username + " to " + adminUsername).update();
      qs.query().sql("create database " + dbName + " with owner " + username).update();

      return null;
    });

    final String hostsPort = workspaceConnectParams.host + ":" + workspaceConnectParams.port;
    final String url       = "jdbc:postgresql://" + hostsPort + "/" + workspaceConnectParams.dbName;

    try (Connection connection = DriverManager.getConnection(url, "postgres", postgresPassword)) {

      try (Statement statement = connection.createStatement()) {
        statement.execute("CREATE EXTENSION PostGIS");
        statement.execute("CREATE EXTENSION PostGIS_topology");
        statement.execute("CREATE EXTENSION BTree_GIST");
      }

    } catch (SQLException e) {
      throw SqlErrorUtil.convertError(e);
    }

  }

  private void initializeWorkspaceService() {
    workspaceService = new WorkspaceServicePg(workspaceConnectParams);
    workspaceService.initialize();
  }

  @AfterMethod
  public void closeWorkspaceService() {
    workspaceService.close();
  }


  protected int detail_dir_name_count(ProjectService projectService, String name) {
    Db     db        = getField(projectService, "db");
    String projectId = getField(projectService, "projectId");

    return db.query()
             .param("my", projectId)
             .param("name", name)
             .sql("select count(1) from {.my}.detail_dir where name = {name}")
             .integer();
  }

  protected int detail_area_count(@NonNull ProjectService projectService, @NonNull String detailId) {
    Db     db        = getField(projectService, "db");
    String projectId = getField(projectService, "projectId");

    return db.query()
             .param("my", projectId)
             .param("detailId", detailId)
             .sql("select count(1) from {.my}.detail_area where detail_id = {detailId}")
             .integer();
  }
}
