package kz.pompei.glazga.v3.gui.probes;

import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.util.concurrent.atomic.AtomicBoolean;
import kz.greetgo.util.RND;
import kz.greetgo.util.fui.FUI;
import kz.greetgo.util.fui.IntAccessor;
import kz.pompei.glazga.utils.RndUtil;
import org.testng.annotations.Test;

public class ProbeManySelectLike extends ProbeDatabaseParent {


  @Test
  public void prepareTables() {
    db.query().sql("create table client (id varchar(200) primary key, name varchar(500) not null)").update();
    db.query().sql("create table prefix (pre varchar(200) primary key, status int not null)").update();
  }

  @Test
  public void insertData() throws Exception {

    FUI fui = new FUI(Paths.get("build").resolve(getClass().getSimpleName()));


    AtomicBoolean working   = new AtomicBoolean(true);
    AtomicBoolean showState = new AtomicBoolean(false);

    Thread threadLooking = new Thread(() -> {

      while (working.get()) {
        try {
          Thread.sleep(1500);
        } catch (InterruptedException e) {
          return;
        }

        showState.set(true);
      }

    });

    IntAccessor maxBatchSize      = fui.entryInt("max-batch-size", 5000);
    IntAccessor clientsWithPre    = fui.entryInt("insert/clients-with-pre", 10);
    IntAccessor clientsWithoutPre = fui.entryInt("insert/client-without-pre", 1000);
    IntAccessor statusMax         = fui.entryInt("insert/status-max", 10);

    Thread threadWorking = new Thread(() -> db.withConnectionVoid(connection -> {
      connection.setAutoCommit(false);

      try (PreparedStatement psClient = connection.prepareStatement("insert into client (id, name) values (?, ?)");
           PreparedStatement psPrefix = connection.prepareStatement("insert into prefix (pre, status) values (?, ?)")) {

        int batchSizeClient = 0;
        int batchSizePrefix = 0;
        int insertClients   = 0;
        int insertPrefixes  = 0;

        boolean needCommit;

        while (working.get()) {


          {
            String preId = RndUtil.newId();

            {
              psPrefix.setString(1, preId);
              psPrefix.setInt(2, RND.rnd.nextInt(statusMax.getInt()));
              psPrefix.addBatch();
              batchSizePrefix++;
              insertPrefixes++;
            }


            int count = clientsWithPre.getInt();
            for (int i = 0; i < count; i++) {
              String id = preId + "-" + RndUtil.newId();

              {
                psClient.setString(1, id);
                psClient.setString(2, "With PRE " + RND.str(10));
                psClient.addBatch();
                batchSizeClient++;
                insertClients++;
              }


            }


          }
          {
            String preId = RndUtil.newId();
            int    count = clientsWithoutPre.getInt();
            for (int i = 0; i < count; i++) {
              String id = preId + "-" + RndUtil.newId();

              {
                psClient.setString(1, id);
                psClient.setString(2, "Without PRE " + RND.str(10));
                psClient.addBatch();
                batchSizeClient++;
                insertClients++;
              }

            }
          }

          needCommit = false;
          if (batchSizeClient >= maxBatchSize.get()) {
            psClient.executeBatch();
            batchSizeClient = 0;
            needCommit      = true;
          }
          if (batchSizePrefix >= maxBatchSize.get()) {
            psPrefix.executeBatch();
            batchSizePrefix = 0;
            needCommit      = true;
          }
          if (needCommit) {
            connection.commit();
          }

          if (showState.get()) {
            showState.set(false);

            System.out.println("kF7q1Yr2sr :: Inserted: clients " + insertClients + ", prefixes " + insertPrefixes);
          }
        }

        needCommit = false;
        if (batchSizeClient > 0) {
          psClient.executeBatch();
          needCommit = true;
        }
        if (batchSizePrefix > 0) {
          psPrefix.executeBatch();
          needCommit = true;
        }
        if (needCommit) {
          connection.commit();
        }

        System.out.println("J7nQ0dl1rN :: TOTAL Inserted: clients " + insertClients + ", prefixes " + insertPrefixes);
      } finally {
        working.set(false);
        fui.shutdown();
      }

    }));

    threadWorking.start();
    threadLooking.start();

    System.out.println("97fF3Ov1yc :: Application started");
    fui.go();
    working.set(false);

    threadLooking.interrupt();
    threadLooking.join();
    threadWorking.join();

    System.out.println("TZP34gP1DB :: Application FINISHED");

  }
}
