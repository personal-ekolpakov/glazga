package kz.pompei.glazga.v3.gui.probes;

import kz.pompei.glazga.utils.db.Db;
import kz.pompei.glazga.v3.gui.forms.project_tree.model.ConnectParams;
import kz.pompei.glazga.v3.gui.util.ConnectionCreatorDriver;

public abstract class ProbeDatabaseParent {
  protected final ConnectParams testDbConParams = ConnectParams.builder()
                                                               .host("localhost")
                                                               .port(17382)
                                                               .username("test_user")
                                                               .password("j1eBouLkl9QN2O1AX04RqmXQy5P1pL")
                                                               .dbName("test_db")
                                                               .build();

  protected final Db db = new Db(new ConnectionCreatorDriver(() -> testDbConParams));
}
