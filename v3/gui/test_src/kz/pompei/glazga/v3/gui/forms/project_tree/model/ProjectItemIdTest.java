package kz.pompei.glazga.v3.gui.forms.project_tree.model;

import kz.pompei.glazga.utils.RndUtil;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ProjectItemIdTest {

  @Test
  public void strId__parseId__WORKSPACE() {
    final ProjectItemId pii = ProjectItemId.builder()
                                           .type(ProjectItemType.WORKSPACE)
                                           .workspaceId(RndUtil.newId())
                                           .build();

    //
    //
    final String strId = pii.strId();
    //
    //

    final ProjectItemId pii2 = ProjectItemId.parse(strId).orElseThrow();

    assertThat(pii2.workspaceId).isEqualTo(pii.workspaceId);
    assertThat(pii2.type).isEqualTo(ProjectItemType.WORKSPACE);

  }

  @Test
  public void strId__parseId__PROJECT() {
    final ProjectItemId pii = ProjectItemId.builder()
                                           .type(ProjectItemType.PROJECT)
                                           .workspaceId(RndUtil.newId())
                                           .projectId(RndUtil.newId())
                                           .build();

    //
    //
    final String strId = pii.strId();
    //
    //

    final ProjectItemId pii2 = ProjectItemId.parse(strId).orElseThrow();

    assertThat(pii2.workspaceId).isEqualTo(pii.workspaceId);
    assertThat(pii2.projectId).isEqualTo(pii.projectId);
    assertThat(pii2.type).isEqualTo(ProjectItemType.PROJECT);

  }

  @Test
  public void strId__parseId__DETAIL() {
    final ProjectItemId pii = ProjectItemId.builder()
                                           .type(ProjectItemType.DETAIL)
                                           .workspaceId(RndUtil.newId())
                                           .projectId(RndUtil.newId())
                                           .id(RndUtil.newId())
                                           .build();

    //
    //
    final String strId = pii.strId();
    //
    //

    System.out.println("xa7DL94fzK :: strId = " + strId);

    final ProjectItemId pii2 = ProjectItemId.parse(strId).orElseThrow();

    assertThat(pii2.workspaceId).isEqualTo(pii.workspaceId);
    assertThat(pii2.projectId).isEqualTo(pii.projectId);
    assertThat(pii2.id).isEqualTo(pii.id);
    assertThat(pii2.type).isEqualTo(ProjectItemType.DETAIL);

  }

}
