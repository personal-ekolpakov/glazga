package kz.pompei.glazga.for_tests;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import kz.pompei.glazga.store.Storage;

public class StorageForTests implements Storage {

  public final Map<String, Map<String, String>> datum = new HashMap<>();

  @Override
  public Map<String, String> loadAll(String filePath) {
    return Optional.ofNullable(datum.get(filePath)).orElseGet(HashMap::new);
  }

  @Override
  public Map<String, String> loadPart(String filePath, String keyPrefix) {
    return loadAll(filePath).entrySet()
                            .stream()
                            .filter(e -> e.getKey().startsWith(keyPrefix))
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  @Override
  public void save(String filePath, Map<String, String> updateData) {

    Map<String, String> map = datum.compute(filePath, (k, v) -> v != null ? v : new HashMap<>());

    for (final Map.Entry<String, String> e : updateData.entrySet()) {
      String key   = e.getKey();
      String value = e.getValue();
      if (value == null) {
        map.remove(key);
      } else {
        map.put(key, value);
      }
    }

  }
}
